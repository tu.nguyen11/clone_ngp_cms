export default {
  PRODUCT: "PRODUCT",
  PRODUCT_TYPE: "PRODUCT_TYPE",
  CATE: "CATE",
  USER: "USER",
  SHIPPER: "SHIPPER",
  SALEMAN: "SALEMAN",
  EVENT: "EVENT",
  NEWS: "NEWS",
  NOTIFY: "NOTIFY",
  BANNER: "BANNER",
  BRAND: "BRAND",
};
