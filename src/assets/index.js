import images from "./images";
import colors from "./colors";
import strings from "./strings";

export { images, colors, strings };
