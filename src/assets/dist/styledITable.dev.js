"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true,
});
exports.StyledPagination = exports.StyledTable = void 0;

function _templateObject2() {
  var data = _taggedTemplateLiteral([
    "\n  & > li {\n    height: 42px;\n    border: none;\n    font-size: 13px;\n    line-height: 18px;\n    color: #383b48;\n    background: #f3f7f5;\n    font-family: Avenir;\n  }\n  & .ant-pagination-item-active {\n    border: none;\n    background: #004C91;\n    font-size: 13px;\n    line-height: 18px;\n    border-radius: 0px;\n  }\n  .ant-pagination-item-active span {\n    color: #ffffff;\n  }\n",
  ]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral([
    "\n  & .ant-table-thead > tr > th {\n    background: #f3f7f5;\n    font-style: normal;\n    font-weight: bold;\n    font-size: 14px;\n    line-height: 19px;\n    border-bottom: 1px solid rgba(122, 123, 123, 0.3);\n    font-family: Avenir Next;\n  }\n\n  & .ant-table-tbody > tr > td {\n    background: #f3f7f5;\n    border-bottom: 1px solid rgba(122, 123, 123, 0.3);\n    font-size: clamp(8px, 4vw, 14px);\n    font-style: normal;\n    font-weight: normal;\n    color: #22232b;\n    line-height: 20px;\n    font-family: Avenir Next;\n  }\n  & .ant-table-footer {\n    background: #f3f7f5;\n  }\n",
  ]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) {
  if (!raw) {
    raw = strings.slice(0);
  }
  return Object.freeze(
    Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })
  );
}

var _require = require("styled-components"),
  styled = _require["default"];

var _require2 = require("antd"),
  Table = _require2.Table,
  Pagination = _require2.Pagination;

var StyledTable = styled(Table)(_templateObject());
exports.StyledTable = StyledTable;
var StyledPagination = styled(Pagination)(_templateObject2());
exports.StyledPagination = StyledPagination;
