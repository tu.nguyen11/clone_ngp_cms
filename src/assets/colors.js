class colors {
  constructor() {
    if (!colors.instance) {
      colors.instance = this;
    }
    return colors.instance;
  }

  set(colors) {
    try {
      this.main = colors.main;
    } catch (error) {}
  }

  main = "#004C91";
  white = "#fff";
  oranges = "#E35F4B";
  darkRed = "#D01717";
  icon = {
    default: "#9a9a9d",
  };
  background = "#F5F3F6";
  mainDark = "#004C91";
  text = {
    black: "#3C3F3D",
    gray: "#939495",
  };
  line = {
    black: "#E9F2F4",
    gray: "#CDD0D9",
  };
  blackChart = "#22232B";
  red = "#E35F4B";
  line = "#cdcece";
  line_2 = "#7A7B7B";
  transparent = "#00000000";
  gray = {
    _50: "#FAFAFA",
    _100: "#F5F5F5",
    _200: "#EEEEEE",
    _300: "#E0E0E0",
    _400: "#BDBDBD",
    _500: "#9E9E9E",
    _600: "#757575",
    _700: "#616161",
    _800: "#424242",
    _900: "#212121",
  };
  title = "#004C91";

  green = {
    _3: "#E4EFFF",
  };
}

const instance = new colors();
//Object.freeze(instance);
export default instance;
