class images {
  constructor() {
    if (!images.instance) {
      images.instance = this;
    }
    return images.instance;
  }

  set(images) {
    try {
      this.logo = images.logo;
    } catch (error) {}
  }
  logo = require("./images/logo.png");
  logo_form = require("./images/logo_form.png");
  imgError = require("./img/img_error.png");
  icCloseModal = require("./img/ic_close_modal.png");
  icSearch = require("./img/ic_search.png");
  remove = require("./img/Remove.png");
  avatar = require("./images/avtdefault.jpg");
  check = require("./images/checkmark.png");
  // icOrder = require("./images");
}

const instance = new images();
//Object.freeze(instance);
export default instance;
