const DefineKeyEvent = {
  agency_S1: 1,
  agency_S2: 2,
  event_type_assigned: 1,
  event_type_S1: 2,
  event_type_S2: 3,
  filter_sales: 2, // Doanh số
  filter_money: 1, // số lượng
  big_size: 1, // quy cách lớn
  small_size: 2, // quy cách nhỏ
  event_running: 2, //đang chạy,
  event_initialization: 1, // khởi tạo
  event_end: 3, //kết thúc
  percent_discount: 1, // % doanh số tích lũy
  money_specifications: 2, // đ/ quy cách tích lũy
  priori_prioritize: 1, // event ưu tiên
  priori_parallel: 0, // event song song
  event_product_present: 1, // quà tặng sản phẩm
  event_discount_present: 2, // giảm giá quà tặng
  conditon_nguyen_don: 0, // nguyên đơn
  condition_type_sales: 1, // Doanh số sản phẩm
  condition_type_count: 2, // Số lượng sản phẩm
  tung_san_pham: 2,
  nhom_san_pham: 3,
  song_song: 0,
  uu_tien: 1
}

export { DefineKeyEvent }
