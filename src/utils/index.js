import { message } from "antd";
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => {
    if (!callback) {
      return;
    }
    callback(reader.result);
  });
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.warn("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 < 500;
  if (!isLt2M) {
    message.warn("Image must smaller than 500kb!");
  }
  return isJpgOrPng && isLt2M;
}
const priceFormat = (price) => {
  if (price == undefined) {
    return "";
  } else {
    let str = typeof price == "string" ? price : price.toString();
    return str.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "";
  }
};
const priceShortFormat = (price) => {
  if (!price) {
    return "-";
  } else {
    return priceFormat(Math.round(price / 1000000));
  }
};
const priceChartFormat = (price) => {
  return priceFormat(Math.round(price / 1000000));
};

const unPriceFormat = (price) => {
  try {
    var data = price.replace(/\,/g, "");
    return data;
  } catch (error) {}
  return price;
};

const dayInMonth = (month, year) => {
  return new Date(year, month, 0).getDate();
};

function truncate(str, n) {
  return str.length > n ? str.substr(0, n - 1) + "..." : str;
}

// const findPathTree = (node, attr, value) => {
//   if (node[attr] == value) {
//     return [];
//   }
//   if (Array.isArray(node.children)) {
//     for (var treeNode of node.children) {
//       const childResult = findPathTree(treeNode, value);

//       if (Array.isArray(childResult)) {
//         return [treeNode[attr]].concat(childResult);
//       }
//     }
//   }
// };
const renderAddress = (string) => {
  let match = ["", null, undefined, ", ", ",,,"];
  if (!string || match.includes(string)) return "-";
  return string.replace(/^,/g, "");
};
export {
  //   showToash,
  //   parseColumnName,
  //   getBase64,
  //   beforeUpload,
  priceFormat,
  unPriceFormat,
  dayInMonth,
  priceShortFormat,
  priceChartFormat,
  truncate,
  renderAddress,
  //   findPathTree
};
