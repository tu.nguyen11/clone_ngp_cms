export const getStatusColor = type => {
  switch (type) {
    case 1:
      return '#fffb8f'
    case 2:
      return '#36cfc9'
    case 3:
      return '#FF9900'
    case 4:
      return '#33CCFF'
    default:
      return '#b7eb8f'
  }
}
export function convertHex (hex, opacity) {
  hex = hex.replace('#', '')
  let r = parseInt(hex.substring(0, 2), 16)
  let g = parseInt(hex.substring(2, 4), 16)
  let b = parseInt(hex.substring(4, 6), 16)
  let result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')'
  return result
}
