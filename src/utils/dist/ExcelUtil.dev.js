"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ExcelToJson = void 0;

var _xlsx = _interopRequireDefault(require("xlsx"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//startline: số dòng bắt đầu đọc
//arrkey: Mảng key mapping tới server
var ExcelToJson = function ExcelToJson(file, startLine, arrKey) {
  return new Promise(function (resolve, reject) {
    try {
      var reader = new FileReader(); // var result = {};

      reader.onload = function (e) {
        var data = e.target.result;
        data = new Uint8Array(data);

        var workbook = _xlsx["default"].read(data, {
          type: "array"
        });

        console.log(workbook);
        var result = {};
        workbook.SheetNames.forEach(function (sheetName, index) {
          var roa = _xlsx["default"].utils.sheet_to_row_object_array(workbook.Sheets[sheetName], {
            header: 1
          });

          if (roa.length > 0) {
            result = roa;
          }
        }); //console.log(attrTypes);

        var arr = [];

        for (var i = startLine; i < result.length; i++) {
          var obj = result[i]; //console.log(obj);

          var jsonObject = {};

          for (var key in obj) {
            // var attrName = result[0][key];
            var attrName = arrKey[key];
            var attrValue = obj[key];
            jsonObject[attrName] = attrValue;
          }

          arr.push(jsonObject);
        } //   // see the result, caution: it works after reader event is done.
        //   console.log(result);


        resolve(arr);
      };

      reader.readAsArrayBuffer(file);
    } catch (error) {
      reject(error);
    } //reader.readAsBinaryString(file)

  });
};

exports.ExcelToJson = ExcelToJson;