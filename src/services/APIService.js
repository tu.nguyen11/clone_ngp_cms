import {message} from "antd";

import {HistoryService} from ".";
import UserService from "./UserService";

const time = new Date();
const domainCall = `http://config.hyundai-nguyengiaphat.vn/ngp/cms/cms.json?${time.getTime()}`;

class APIService {
  constructor() {
    if (!APIService.instance) {
      APIService.instance = this;
    }
    let domain = window.location.hostname;
    if (
      domain.search("localhost") >= 0 ||
      domain.search("ngpdev.ipicorp.co") >= 0
    ) {
      fetch(domainCall)
        .then((response) => response.json())
        .then((data) => {
          localStorage.setItem(
            "local_domain_server_NGP_ADMIN",
            data.admin.dev.restful
          );
        });
    } else if (domain.search("ngptest.ipicorp.co") >= 0) {
      fetch(domainCall)
        .then((response) => response.json())
        .then((data) => {
          localStorage.setItem(
            "local_domain_server_NGP_ADMIN",
            data.admin.test.restful
          );
        });
    } else if (domain.search("cms.hyundai-nguyengiaphat.vn") >= 0) {
      fetch(domainCall)
        .then((response) => response.json())
        .then((data) => {
          localStorage.setItem(
            "local_domain_server_NGP_ADMIN",
            data.admin.product.restful
          );
        });
    }
    return APIService.instance;
  }

  adminService = "adminservice/";
  permissionService = "permissionservice/";
  warehouseservice = "warehouseservice/";
  dashboardService = "dashboardservice/";

  queryParams(params) {
    return Object.keys(params)
      .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
      .join("&");
  }

  get(func, params = {}) {
    if (!("user_id_login" in params) && !("user_name_login" in params)) {
      params.user_id_login = !localStorage.getItem("user_id_login")
        ? ""
        : localStorage.getItem("user_id_login");
      params.user_name_login = !localStorage.getItem("user_name_login")
        ? ""
        : localStorage.getItem("user_name_login");
    }
    var query = this.queryParams(params);
    var url = localStorage.getItem("local_domain_server_NGP_ADMIN") + func;

    if (query) {
      url += "?" + query;
    }

    let arrFunc = func.split("/");
    if (arrFunc[0].indexOf("service") > -1) {
      arrFunc.splice(0, 1);
    }
    let arrFuncNew = arrFunc.filter((item) => {
      return item !== "";
    });
    let linkAPI = arrFuncNew.join("/");

    console.log("GET: " + url);
    var token = UserService._getToken();
    return new Request(url, {
      method: "GET",
      headers: {
        Authorization: "Bearer ",
        // token: !token || token === "" ? token : token + "-/" + linkAPI,
        token,
      },
    });
  }

  post(func, params = {}, isLogin = false) {
    var url = localStorage.getItem("local_domain_server_NGP_ADMIN") + func;
    console.log("POST: " + url);
    console.log(params);

    if (!("user_id_login" in params) && !("user_name_login" in params)) {
      params.user_id_login = !localStorage.getItem("user_id_login")
        ? ""
        : localStorage.getItem("user_id_login");
      params.user_name_login = !localStorage.getItem("user_name_login")
        ? ""
        : localStorage.getItem("user_name_login");
    }

    let linkAPI;
    if (!isLogin) {
      let arrFunc = func.split("/");
      if (arrFunc[0].indexOf("service") > -1) {
        arrFunc.splice(0, 1);
      }
      let arrFuncNew = arrFunc.filter((item) => {
        return item !== "";
      });
      linkAPI = arrFuncNew.join("/");
    }

    var token = UserService._getToken();

    return new Request(url, {
      method: "POST",
      body: JSON.stringify(params),
      headers: {
        "Content-Type": "application/json",
        token: isLogin ? "" : token,
        // : !token || token === ""
        // ? token
        // : token + "-/" + linkAPI,
      },
    });
  }

  multipart = (func, body) => {
    try {
      var url = localStorage.getItem("local_domain_server_NGP_ADMIN") + func;
      let formData = new FormData(); //formdata object

      //   formData.append("name", "ABC"); //append the values with key, value pair
      //   formData.append("age", 20);
      if (body) {
        if (!("user_id_login" in body) && !("user_name_login" in body)) {
          body.user_id_login = !localStorage.getItem("user_id_login")
            ? ""
            : localStorage.getItem("user_id_login");
          body.user_name_login = !localStorage.getItem("user_name_login")
            ? ""
            : localStorage.getItem("user_name_login");
        }
        Object.keys(body).forEach((item) => {
          var obj = body[item];
          if (Array.isArray(obj)) {
            for (let i = 0; i < obj.length; i++) {
              formData.append(item, obj[i]);
            }
          } else {
            formData.append(item, body[item]);
          }
        });
      }

      let arrFunc = func.split("/");
      if (arrFunc[0].indexOf("service") > -1) {
        arrFunc.splice(0, 1);
      }
      let arrFuncNew = arrFunc.filter((item) => {
        return item !== "";
      });
      let linkAPI = arrFuncNew.join("/");

      var token = UserService._getToken();

      var request = new Request(url, {
        method: "POST",
        body: formData,
        headers: {
          Authorization: "Bearer " + token,
          // token: !token ? token || token === "" : token + "-/" + linkAPI,
          token,
        },
      });
      return request;
    } catch (error) {
      //this.handleError(error.code, error.message);
      throw error;
    }
  };

  execute = async (request) => {
    try {
      const response = await fetch(request);

      const responseJson = await response.json();
      console.log(response);
      if (responseJson.code == 1) {
        return responseJson.data;
      }
      if (responseJson.code == -1) {
        UserService._setToken("");
        HistoryService.replace("/login");
        return;
      }

      throw responseJson;
    } catch (error) {
      // console.log(JSON.stringify(request));
      // message.error(error);
      // throw error;
      console.log(JSON.stringify(request));
      if (error.code == 0 || error.code == -125) {
        if (!error.error) {
          message.error(error.error_message.vn);
          throw error;
        }

        message.error(error.error);
        throw error;
      }

      if (error.code == -2) {
        HistoryService.replace("/404");
        throw error.error;
      }
      throw error;
    }
  };

  getDefaultParams = () => {
    var params = new Object();
    params.user_id = 1;
    params.user_id_login = !localStorage.getItem("user_id_login")
      ? ""
      : localStorage.getItem("user_id_login");
    params.user_name_login = !localStorage.getItem("user_name_login")
      ? ""
      : localStorage.getItem("user_name_login");
    return params;
  };

  //   _uploadImage = async (file, type) => {
  //     try {
  //       var params = this.getDefaultParams();
  //       params.images = file;
  //       params.type = type;
  //       let request = this.multipart("product/upload_image", params);
  //       let data = await this.execute(request);
  //       return data;
  //     } catch (error) {
  //       throw error;
  //     }
  //   };

  _login = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "admincms/login",
        params,
        true
      );

      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListOrder = async (status, page, filter) => {
    try {
      var params = this.getDefaultParams();
      params.status = status;
      params.page = page;
      params.filter = filter;
      let request = this.get(this.adminService + "order/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailOrder = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.order_id = id;
      let request = this.get(this.adminService + "order/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSuppliers = async (city_id, agency_id, key, page, status) => {
    try {
      var params = this.getDefaultParams();

      params.city_id = city_id;
      params.agency_id = agency_id;
      // params.district_id = district_id;
      params.key = key;
      params.page = page;
      params.status = status;

      let request = this.get(this.adminService + "supplier/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailSuppliers = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.supplier_id = id;
      let request = this.get(this.adminService + "supplier/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListProduct = async (page, status, key, category_id, type) => {
    try {
      var params = this.getDefaultParams();
      params.page = page;
      params.status = status;
      params.key = key;
      params.category_id = category_id;
      params.type = type; // type là caterory cha
      let request = this.get(this.adminService + "product/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListTypeProduct = async (status, page, keySearch) => {
    try {
      var params = this.getDefaultParams();
      params.status = status;
      params.page = page;
      params.key = keySearch;
      let request = this.get(
        this.adminService + "brand_category/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  // _getListStock = async (status, page) => {
  //   //status 1: hoat dong , 0 : all
  //   try {
  //     var params = this.getDefaultParams();
  //     params.status = status;
  //     params.page = page;
  //     let request = this.get(this.adminService + "stock/get_all", params);
  //     let data = await this.execute(request);
  //     return data;
  //   } catch (error) {
  //     throw error;
  //   }
  // };

  _updateStock = async (warehouse_id, order_id) => {
    try {
      var params = this.getDefaultParams();

      params.warehouse_id = warehouse_id;
      params.order_id = order_id;

      let request = this.multipart("order/assigned_stock", params);

      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailProduct = async (product_id) => {
    try {
      var params = this.getDefaultParams();
      params.product_id = product_id;
      let request = this.get(
        this.adminService + "product/detail_product",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailEvent = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.event_id = id;
      let request = this.get(
        this.adminService + "event/news/get_detail_event_increment",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailSalesPolicyEvent = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.event_id = id;
      let request = this.get(
        this.adminService + "event/news/get_detail_event",
        params
      );

      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailTypeProduct = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.category_id = id;
      let request = this.get(
        this.adminService + "brand_category/get_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListCities = async (key) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      let request = this.get(this.adminService + "cities/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListCitiesEmpty = async (supplier_id) => {
    try {
      var params = this.getDefaultParams();
      params.supplier_id = supplier_id;
      let request = this.get(
        this.adminService + "warehouse/get_list_region_empty",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListCountyAll = async (city_id, key) => {
    try {
      var params = this.getDefaultParams();
      params.lst_city_id = city_id.length === 0 ? [-1] : city_id;
      params.key = key;
      let request = this.get(
        this.adminService + "districts/get_list_district_by_list_city",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListBusinessRegion = async (key) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      let request = this.get(
        this.adminService + "business_region/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListWardAll = async (district_id, key) => {
    try {
      var params = this.getDefaultParams();
      params.lst_district_id = district_id.length === 0 ? [-1] : district_id;

      params.key = key;
      let request = this.get(
        this.adminService + "/ward/get_list_ward_by_list_district",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAgencyByListBussinessRegion = async (key_word, list_region) => {
    try {
      var params = this.getDefaultParams();
      params.key_word = key_word;
      params.list_region = [...list_region];

      let request = this.get(
        this.adminService + "agency/get_agency_by_list_bussiness_region",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListCounty = async (city_id, key) => {
    try {
      var params = this.getDefaultParams();
      params.city_id = city_id;
      params.key = key;
      let request = this.get(
        this.adminService + "districts/get_list_by_city",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListDistrictByCity = async (listRegion) => {
    try {
      var params = this.getDefaultParams();
      params.list_region = [...listRegion];
      let request = this.get(
        this.adminService + "cities/get_list_district_by_cities",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListWard = async (district_id, key) => {
    try {
      var params = this.getDefaultParams();
      params.district_id = district_id;
      params.key = key;
      let request = this.get(this.adminService + "ward/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListAgencyS1Name = async (key) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      let request = this.get(
        this.adminService + "supplier/get_list_supplier_name",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddNewAgencyS2 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(this.adminService + "/supplier/add_new", params);

      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };
  _postAddNewAgencyS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;

      let request = this.post(
        this.adminService + "user_agency/create_user_agency",
        params
      );

      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListBrandCategory = async (productTypeId = 0, search = "") => {
    try {
      var params = this.getDefaultParams();
      params.productTypeId = productTypeId;
      params.search = search;
      // let request = this.get(
      //   this.adminService + "product/get_list_product_type",
      //   params
      // );
      let request = this.get(
        this.adminService + "/product/get_list_product_type_parent",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSubCategpry = async (product_tye_parent, search = "") => {
    try {
      var params = this.getDefaultParams();
      params.product_tye_parent = product_tye_parent;
      params.search = search;
      // let request = this.get(
      //   this.adminService + "product/get_list_cate",
      //   params
      // );
      let request = this.get(
        this.adminService + "/product/get_list_product_type",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListNewBrand = async (product_type_id) => {
    try {
      var params = this.getDefaultParams();
      params.product_type_id = product_type_id;
      let request = this.get(
        this.adminService + "/product/get_list_cate",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListOutStandingBrand = async (search = "") => {
    try {
      var params = this.getDefaultParams();
      params.search = search;
      let request = this.get(
        this.adminService + "/product/get_list_brand",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddTypeProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params.data = JSON.stringify(obj);
      let request = this.multipart("/brand_category/add_new", params);
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _postEditSuppliers = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(this.adminService + "/supplier/update", params);
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _searchSuppliers = async (status, page, key) => {
    try {
      var params = this.getDefaultParams();
      params.status = status;
      params.page = page;
      params.key = key;
      let request = this.get(this.adminService + "supplier/search", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _filterSuppliers = async (status, page, city_id, district_id) => {
    try {
      var params = this.getDefaultParams();
      params.status = status;
      params.page = page;
      params.city_id = city_id;
      params.district_id = district_id;
      let request = this.get(this.adminService + "supplier/filter", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _updateStatusSuppliers = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/supplier/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _UpdateOrder = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/order/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListOrder = async (status, page, key, start_date, end_date) => {
    try {
      var params = this.getDefaultParams();
      params.status = status;
      params.page = page;
      params.key = key;
      params.start_date = start_date;
      params.end_date = end_date;

      let request = this.get(this.adminService + "/order/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListStatus = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(this.adminService + "/order/get_status", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postExportExecl = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(this.adminService + "/order/export", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postEventExportExcel = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(this.adminService + "/order/export", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postExportFileInput = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.post(
        this.adminService + "warehouse/import/export_file",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postEditTypeProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params.data = JSON.stringify(obj);
      let request = this.multipart(
        this.adminService + "brand_category/update",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveTypeProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/brand_category/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
    }
  };

  _postRemoveShipperAssign = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "warehouse/shipper/delete_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
    }
  };
  _postRemoveEvent = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/event/delete_event",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
    }
  };

  _postAddProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "/product/add_new", params);
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAttribute = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "/product/get_list_attribute",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailStockInput = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "/warehouse/import/detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailStockOnput = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "/warehouse/export/detail_export",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postEditProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/edit_product",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _uploadImage = async (type, file) => {
    try {
      var params = this.getDefaultParams();
      params.files = file;
      params.type = type;
      let request = this.multipart(
        this.adminService + "user/upload_image",
        params
      );
      console.log("Image upload request", request);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _uploadFile = async (type, file, name_file) => {
    try {
      var params = this.getDefaultParams();
      params.files = file;
      params.type = type;
      params.name_file = name_file;
      let request = this.multipart(
        this.adminService + "user/upload_file",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveProduct = async (objRemove) => {
    try {
      var params = this.getDefaultParams();
      params = {...objRemove};
      let request = this.post(
        this.adminService + "product/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getSelectListWarehouse = async (keySearch) => {
    try {
      var params = this.getDefaultParams();
      params.keyword = keySearch;
      let request = this.get(
        this.adminService + "warehouse/get_list_type",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListTypeShipper = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "warehouse/shipper/list_type_shipper",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListShipperSelect = async (keySearch, warehouse_id) => {
    try {
      var params = this.getDefaultParams();
      params.key = keySearch;
      params.warehouse_id = warehouse_id;
      let request = this.get(
        this.adminService + "warehouse/shipper/get_list_shipper",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postShipperAssign = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "warehouse/shipper/add_shipper_order",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListWarehouseExport = async (
    from_date,
    to_date,
    warehouse_id,
    type,
    keyword,
    page
  ) => {
    try {
      var params = this.getDefaultParams();
      params.warehouse_id = warehouse_id;
      params.from_date = from_date;
      params.to_date = to_date;
      params.type = type;
      params.page = page;
      params.page = page;
      params.keyword = keyword;

      let request = this.get(
        this.adminService + "warehouse/export/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListInventory = async (
    warehouse_type,
    type_product,
    category_id,
    page,
    fromDate,
    toDate,
    keyword
  ) => {
    try {
      var params = this.getDefaultParams();
      params.warehouse_type = warehouse_type;
      params.type_product = type_product;
      params.category_id = category_id;
      params.page = page;
      params.fromDate = fromDate;
      params.toDate = toDate;
      params.keyword = keyword;

      let request = this.get(
        this.adminService + "warehouse/inventory/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListWarehouse = async (type, region, keyword, page) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      params.region = region;
      params.keyword = keyword;
      params.page = page;
      let request = this.get(
        this.adminService + "/warehouse/warehouses",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getTypeWarehouse = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "/warehouse/list_type_warehouse",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailWH = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "warehouse/detail_warehouse",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postEditWarehouse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "/warehouse/update_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddWarehouse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "/warehouse/add_new", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailWarehouse = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.supplier_id = id;
      let request = this.get(this.adminService + "supplier/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListWarehouseImport = async (
    warehouse_id,
    start_date,
    end_date,
    status,
    key,
    page,
    type
  ) => {
    try {
      var params = this.getDefaultParams();
      params.warehouse_id = warehouse_id;
      params.start_date = start_date;
      params.end_date = end_date;
      params.status = status;
      params.key = key;
      params.page = page;
      params.type = type;

      let request = this.get(
        this.adminService + "warehouse/import/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListShipper = async (warehouse_id, status, key, page) => {
    try {
      var params = this.getDefaultParams();
      params.warehouse_id = warehouse_id;
      params.status = status;
      params.key = key;
      params.page = page;

      let request = this.get(this.adminService + "shipper/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailShipper = async (shipper_id) => {
    try {
      var params = this.getDefaultParams();
      params.shipper_id = shipper_id;

      let request = this.get(this.adminService + "shipper/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddShipper = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "/shipper/add_new", params);
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveShipper = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/shipper/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
    }
  };

  _getListHistoryShipper = async (
    id,
    start_date,
    end_date,
    status,
    key,
    page
  ) => {
    try {
      var params = this.getDefaultParams();
      params.shipper_phone = id;
      params.start_date = start_date;
      params.end_date = end_date;
      params.status = status;
      params.key = key;
      params.page = page;
      let request = this.get(
        this.adminService + "shipper/history/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListHistoryShipperDetail = async (order_code_child) => {
    try {
      var params = this.getDefaultParams();
      params.order_code_child = order_code_child;
      let request = this.get(
        this.adminService + "shipper/history/detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListHistoryWarehouse = async (from_date, to_date, page, warehouse_id) => {
    try {
      var params = this.getDefaultParams();
      params.from_date = from_date;
      params.to_date = to_date;
      params.page = page;
      params.warehouse_id = warehouse_id;
      let request = this.get(this.adminService + "warehouse/history", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postEditShipper = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "shipper/update", params);
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListShipperAssgin = async (
    warehouse_id,
    shipper_type,
    fromDate,
    toDate,
    page,
    keyword
  ) => {
    try {
      var params = this.getDefaultParams();
      params.warehouse_id = warehouse_id;
      params.shipper_type = shipper_type;
      params.fromDate = fromDate;
      params.toDate = toDate;
      params.page = page;
      params.keyword = keyword;

      let request = this.get(
        this.adminService + "warehouse/shipper/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListStock = async (keyword = "") => {
    try {
      var params = this.getDefaultParams();
      params.keyword = keyword;
      let request = this.get(
        this.adminService + "/warehouse/get_list_type",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  // _postRemoveProduct = async objRemove => {
  //   try {
  //     var params = this.getDefaultParams();
  //     params = {...objRemove};
  //     let request = this.post('product/update_status', params);
  //     let data = await this.execute(request);
  //     return data;
  //   } catch (error) {
  //     throw error;
  //   }
  // };
  _postRemoveWarehouse = async (objRemove) => {
    try {
      var params = this.getDefaultParams();
      params = {...objRemove};

      let request = this.post(
        this.adminService + "warehouse/delete_warehouse",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };
  _postImportAddNew = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/warehouse/import/add_new",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };
  _postExportAddNew = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/warehouse/import/add_new_export",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSalesMan = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "/saleman/get_all_saleman",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveSalesman = async (objRemove) => {
    try {
      var params = this.getDefaultParams();
      params = {...objRemove};
      let request = this.post(
        this.adminService + "saleman/delete_salesman",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailSalesman = async (id) => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "saleman/detail_saleman/" + id,
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListHistoryVisit = async (id, start_date, end_date, toLimit) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      params.start_date = start_date;
      params.end_date = end_date;
      params.fromLimit = 0;
      params.toLimit = toLimit;

      let request = this.get(
        this.adminService + "saleman/get_history_saleman",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListRouterSalesman = async (fromLimit, toLimit) => {
    try {
      var params = this.getDefaultParams();
      params.fromLimit = fromLimit;
      params.toLimit = toLimit;
      let request = this.get(
        this.adminService + "saleman/get_list_route",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postEditSalesman = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "saleman/update_saleman",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddSalesman = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "saleman/add_new_saleman",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListRouter = async (warehouse_id, status, key, page) => {
    try {
      var params = this.getDefaultParams();
      params.warehouse_id = warehouse_id;
      params.status = status;
      params.key = key;
      params.page = page;
      let request = this.get(this.adminService + "route/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveRouter = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(this.adminService + "route/delete_route", params);
      let data = await this.execute(request);
      return data;
    } catch (err) {
    }
  };
  _getDetailRouter = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.route_id = id;
      let request = this.get(this.adminService + "route/detail_route", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddRouter = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "route/add_new_route",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getSelectFrequency = async (from_limit, to_limit) => {
    try {
      var params = this.getDefaultParams();
      params.from_limit = from_limit;
      params.to_limit = to_limit;
      let request = this.get(
        this.adminService + "route/get_list_frequency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getSelectSaleman = async (route_id, fromLimit, toLimit) => {
    try {
      var params = this.getDefaultParams();
      params.route_id = route_id;
      params.fromLimit = fromLimit;
      params.toLimit = toLimit;
      let request = this.get(
        this.adminService + "route/get_list_saleman_not_route",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSupplierName = async (key, fromLimit, toLimit) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      params.fromLimit = fromLimit;
      params.toLimit = toLimit;
      let request = this.get(
        this.adminService + "route/get_list_supplier",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateRouter = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "route/update_route_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getSelectGift = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(this.adminService + "event/get_config", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getModalListProduct = async (keyword) => {
    try {
      var params = this.getDefaultParams();
      params.keyword = keyword;
      let request = this.get(
        this.adminService + "event/product/search",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getModalListGroupProduct = async (keyword) => {
    try {
      var params = this.getDefaultParams();
      params.keyword = keyword;
      let request = this.get(
        this.adminService + "event/product_group/search",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postAddEvent = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      console.log(params);
      let request = this.post(this.adminService + "event/create", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postEditEvent = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "event/update_info", params);
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getSalesDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_sale",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getOrderDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_count_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getOrderDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_count_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getAgencyDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        "dashboard/get_chart_count_order_by_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getAverageSaleOrderDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_average_sale_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getOrderByAgencyDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        "dashboard/get_chart_average_order_by_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  //ChartApplication
  _getChartCountNewAgencyDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_count_new_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getChartCountNewAgencyWorkDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_count_agency_work",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getChartCountNewAgencyNotWorkDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        "dashboard/get_chart_count_agency_not_work",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  //SalesReport

  _getChartAverageSaleStaffDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_average_sale_staff",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getChartCountOrderByStaffDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        "dashboard/get_chart_count_order_by_staff",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getChartRatioOrderForVisitDashboard = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        "dashboard/get_chart_ratio_order_for_visit",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getCountStaffWord = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_count_staff_work",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAverageTimeVisit = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_average_time_visit",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getCountAgencyVisited = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        "dashboard/get_chart_count_agency_visitted",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAverageSaleGrowthOrder = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_average_sale_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getSaleGrowthOrder = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_sale_growth_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getTopProductSodl = async (month, year, city_id, sort) => {
    // sort : 1 . SKU , 2.nhóm , 3.Nhóm
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      params.sort = sort;
      let request = this.get(
        this.adminService + "dashboard/get_top_product_sold",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getInventoryValueToday = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_inventory_value_today",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getChartSaleStaff = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        this.adminService + "dashboard/get_chart_sale_staff",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getChartCountStaffFinishVisit = async (month, year, city_id) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.city_id = city_id;
      let request = this.get(
        "dashboard/get_chart_count_staff_finish_visit",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAllNews = async (
    start_date,
    end_date,
    cate_id,
    type_code,
    status,
    page,
    key
  ) => {
    try {
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      params.cate_id = cate_id;
      params.type_code = type_code;
      params.status = status;
      params.page = page;
      params.key = key;

      let request = this.get(this.adminService + "news/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDropListCateNews = async (type) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      let request = this.get(
        this.adminService + "news/cate/get_drop_list_cate_news",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveNews = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(this.adminService + "news/update_status", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveNoti = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "notification/delete_notification",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateNews = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(this.adminService + "news/update", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailNews = async (idNews) => {
    try {
      var params = this.getDefaultParams();
      params.id = idNews;
      let request = this.get(this.adminService + "news/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddNews = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "news/add_new", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListComment = async (news_id, page) => {
    try {
      var params = this.getDefaultParams();
      params.news_id = news_id;
      params.page = page;

      let request = this.get(
        this.adminService + "news/comment/get_list_comment",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListCateNews = async (key, page, type) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      params.page = page;
      params.type = type;

      let request = this.get(this.adminService + "news/cate/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailCateNews = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(this.adminService + "news/cate/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddCateNews = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "news/cate/add_new", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateCateNews = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "news/cate/update", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveCateNews = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "news/cate/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListPrioritizeNews = async (type) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      let request = this.get(
        this.adminService + "news/prioritize/get_list",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postSatusCommentNews = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "news/comment/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getSelectListPrioritizeNews = async (type, key, page) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      params.key = key;
      params.page = page;

      let request = this.get(
        this.adminService + "news/prioritize/select_list",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postStatusPrioritizeNews = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "/news/prioritize/update_list_news_prioritize",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAllNotification = async (
    distribute,
    status,
    start_date,
    end_date,
    key,
    page
  ) => {
    try {
      var params = this.getDefaultParams();
      params.distribute = distribute;
      params.status = status;
      params.start_date = start_date;
      params.end_date = end_date;
      params.key = key;
      params.page = page;

      let request = this.get(
        this.adminService + "notification/get_all_notification",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailNotification = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;

      let request = this.get(
        this.adminService + "notification/get_detail_notification",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getConfigNotification = async () => {
    try {
      var params = this.getDefaultParams();

      let request = this.get(
        this.adminService + "notification/get_config",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddNotification = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "notification/add_new_notification",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postEditNotification = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "notification/update_notification",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateSatatusCateNew = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "news/cate/update_status_news_cate",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSortCategoryPriorityNews = async (type) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      let request = this.get(
        this.adminService + "news/cate/sort_category_priority",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateSortCate = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "news/cate/update_news_cate",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListCentralTower = async (page, city_id, agency_id, key, status = 1) => {
    try {
      var params = this.getDefaultParams();
      params.page = page;
      params.city_id = city_id;
      params.key = key;
      params.agency_id = -1; // Không lọc theo chi nhánh, số lượng chi nhánh nhiều hơn ttpp thì lọc làm gì.
      params.status = status;
      let request = this.get(
        this.adminService + "center_tower/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailDistribution = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "/center_tower/detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListBranch = async (page, search) => {
    try {
      var params = this.getDefaultParams();
      params.page = page;
      params.search = search;
      let request = this.get(
        this.adminService + "agency/get_list_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getBranchDropList = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "agency/get_all_agency_active",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailBranch = async (id_agency) => {
    try {
      var params = this.getDefaultParams();
      params.id_agency = id_agency;
      let request = this.get(
        this.adminService + "agency/get_detail_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListS1 = async (id_agency) => {
    try {
      var params = this.getDefaultParams();
      params.id_agency = id_agency;
      let request = this.get(this.adminService + "agency/get_list_s1", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAgencyS1 = async (
    status,
    page,
    city_id,
    agency_id,
    key,
    membership_id
  ) => {
    try {
      var params = this.getDefaultParams();
      params.status = status;
      params.page = page;
      params.city_id = city_id;
      // params.district_id = district_id;
      params.key = key;
      params.agency_id = agency_id;
      params.membership_id = membership_id;

      let request = this.get(this.adminService + "user_agency/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAllAgencyS1 = async (
    status,
    page,
    city_id,
    agency_id,
    key,
    membership_id
  ) => {
    try {
      var params = this.getDefaultParams();
      params.status = status;
      params.page = page;
      params.city_id = city_id;
      // params.district_id = district_id;
      params.key = key;
      params.agency_id = agency_id;
      params.membership_id = membership_id;

      let request = this.get(
        this.adminService + "user_agency/get_droplist_user_agency_membership",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAgencyS1Droplist = async (page, key) => {
    try {
      var params = this.getDefaultParams();

      params.page = page;
      params.key = key;

      let request = this.get(
        this.adminService + "user_agency/get_droplist_user_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailAgencyS1 = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "/user_agency/get_detail_user_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListS2InS1 = async (id_s1) => {
    try {
      var params = this.getDefaultParams();
      params.id_s1 = id_s1;
      let request = this.get(
        this.adminService + "user_agency/get_list_s2_in_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListShopType = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "user_agency/get_list_shop_type",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postUpdateAgencyS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();

      params = obj;

      let request = this.post(
        this.adminService + "user_agency/update_user_agency",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };
  _getRevenueDashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.regionlv2_id = id_region;
      params.regionlv3_id = id_area;
      params.city_id = city_id;
      params.shop_channel_id = id_chanel;
      params.product_type_id = id_product_type;
      params.product_type_pk_id = id_product_group;
      params.pk_id = 0;
      params.product_packsize_id = 0;

      let request = this.get(
        this.dashboardService + "dashboard/get_revenue_table",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getASODashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.regionlv2_id = id_region;
      params.regionlv3_id = id_area;
      params.city_id = city_id;
      params.shop_channel_id = id_chanel;
      params.product_type_id = id_product_type;
      params.product_type_pk_id = id_product_group;
      params.pk_id = 0;
      params.product_packsize_id = 0;

      let request = this.get(
        this.dashboardService + "dashboard/get_aso_table",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getOrderDashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.regionlv2_id = id_region;
      params.regionlv3_id = id_area;
      params.city_id = city_id;
      params.shop_channel_id = id_chanel;
      params.product_type_id = id_product_type;
      params.product_type_pk_id = id_product_group;
      params.pk_id = 0;
      params.product_packsize_id = 0;

      let request = this.get(
        this.dashboardService + "dashboard/get_order_table",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getVPODashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.regionlv2_id = id_region;
      params.regionlv3_id = id_area;
      params.city_id = city_id;
      params.shop_channel_id = id_chanel;
      params.product_type_id = id_product_type;
      params.product_type_pk_id = id_product_group;
      params.pk_id = 0;
      params.product_packsize_id = 0;

      let request = this.get(
        this.dashboardService + "dashboard/get_vpo_table",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropsizeDashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      var params = this.getDefaultParams();
      params.month = month;
      params.year = year;
      params.regionlv2_id = id_region;
      params.regionlv3_id = id_area;
      params.city_id = city_id;
      params.shop_channel_id = id_chanel;
      params.product_type_id = id_product_type;
      params.product_type_pk_id = id_product_group;
      params.pk_id = 0;
      params.product_packsize_id = 0;

      let request = this.get(
        this.dashboardService + "dashboard/get_drop_size_table",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getTopProductSell = async (
    month,
    year,
    city_id,
    type,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      params.month = month;
      params.year = year;
      params.regionlv2_id = id_region;
      params.regionlv3_id = id_area;
      params.city_id = city_id;
      params.shop_channel_id = id_chanel;
      params.product_type_id = id_product_type;
      params.product_type_pk_id = id_product_group;
      params.pk_id = 0;
      params.product_packsize_id = 0;

      let request = this.get(
        this.dashboardService + "dashboard/get_top_product_sell",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropListRegion = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.dashboardService + "dashboard/get_drop_list_region",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropListArea = async (id_region) => {
    try {
      var params = this.getDefaultParams();
      params.id_region = id_region;
      let request = this.get(
        this.dashboardService + "dashboard/get_drop_list_area",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropListCity = async (id_area) => {
    try {
      var params = this.getDefaultParams();
      params.id_area = id_area;
      let request = this.get(
        this.dashboardService + "dashboard/get_drop_list_city",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropListChanel = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.dashboardService + "dashboard/get_drop_list_shop_chanel",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropListProductType = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.dashboardService + "dashboard/get_drop_list_product_type",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropListProductGroup = async (product_type) => {
    try {
      var params = this.getDefaultParams();
      params.product_type = product_type;
      let request = this.get(
        this.dashboardService + "dashboard/get_drop_list_cate_level_1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAllAgencyGetRegion = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "/agency/get_region_level2",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListCityByRegion = async (arrayRegion) => {
    try {
      var params = this.getDefaultParams();
      params.list_region = [...arrayRegion];
      let request = this.get(
        this.adminService + "/cities/get_list_city_by_region",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListTreeProduct = async (company) => {
    try {
      var params = this.getDefaultParams();
      params.company = company;
      let request = this.get(
        this.adminService + "/product/get_list_tree_product",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  // _getListTreeProductWarehouse = async (search) => {
  //   try {
  //     var params = this.getDefaultParams();
  //     params.search = search;
  //     let request = this.get(
  //       this.adminservice + "product/get_list_tree_product_type_cate_product",
  //       params
  //     );
  //     let data = await this.execute(request);
  //     return data;
  //   } catch (error) {
  //     throw error;
  //   }
  // };

  _postCreateEventIncrement = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "/event/news/post_create_event_increment",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListEvent = async (obj) => {
    try {
      const {start_date, end_date, status, type, key, page} = obj;
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      params.status = status;
      params.type = type;
      params.key = key;
      params.page = page;

      let request = this.get(
        this.adminService + "/event/news/get_all_event_increment",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateEventIncrement = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "/event/news/post_update_event_increment",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getStatisticalEvent = async (event_id) => {
    try {
      var params = this.getDefaultParams();
      params.event_id = event_id;

      let request = this.get(
        this.dashboardService + "/dashboard/get_report_event_increment",
        params
      );

      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getProductGetCateByCompany = async () => {
    try {
      var params = this.getDefaultParams();

      let request = this.get(
        this.adminService + "/product/get_cate_by_company",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDropListProductGroup = async (product_type) => {
    try {
      var params = this.getDefaultParams();
      params.product_type = product_type;
      let request = this.get(
        this.dashboardService + "dashboard/get_drop_list_cate_level_1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDataReportSell = async (start_date, end_date) => {
    try {
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      let request = this.get(
        this.adminService + "export_excel/admin/report_sell",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDataReportRouteSell = async (start_date, end_date) => {
    try {
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      let request = this.get(
        this.adminService + "export_excel/admin/report_route_sell",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDataReportTrandingS1 = async (start_date, end_date) => {
    try {
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      let request = this.get(
        this.adminService + "export_excel/admin/report_trading_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDataReportTrandingS2 = async (start_date, end_date) => {
    try {
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      let request = this.get(
        this.adminService + "export_excel/admin/report_trading_s2",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDataReportDetailOrderS1 = async (start_date, end_date) => {
    try {
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      let request = this.get(
        this.adminService + "export_excel/admin/report_detail_order_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDataReportDetailOrderS2 = async (start_date, end_date) => {
    try {
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      let request = this.get(
        this.adminService + "export_excel/admin/report_detail_order_s2",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDataReportNvbhVisitS2 = async (
    start_date,
    end_date,
    list_agency_id,
    list_route_id
  ) => {
    try {
      var params = this.getDefaultParams();
      params = {
        start_date: start_date,
        end_date: end_date,
        list_agency_id: list_agency_id,
        list_route_id: list_route_id,
      };
      let request = this.post(
        this.adminService + "export_excel/admin/report_nvbh_vitsit_s2",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropListBranch = async (page, key) => {
    try {
      var params = this.getDefaultParams();
      params.page = page;
      params.key = key;
      let request = this.get(
        this.adminService + "export_excel/admin/get_droplist_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDropListRoute = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;

      let request = this.post(
        this.adminService + "export_excel/admin/get_droplist_route",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDataReportDetailEvent = async (event_id) => {
    try {
      var params = this.getDefaultParams();
      params.event_id = event_id;
      let request = this.get(
        this.adminService + "export_excel/admin/report_detail_event_increment",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAPIDetailDebtAgencyS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "user_agency/detail_intergation_by_bill",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateUserAgencyDebt = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;

      let request = this.post(
        this.adminService + "/user_agency/update_user_agency_debt",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };
  _postSyncDataReportEvent = async (event_id) => {
    try {
      var params = this.getDefaultParams();
      params = {event_id};
      let request = this.post(
        this.dashboardService + "/dashboard/sync_data_report_event",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListOrderS1 = async (
    page,
    from_date,
    to_date,
    user_agency_id,
    status,
    search
  ) => {
    try {
      var params = this.getDefaultParams();
      params.page = page;
      params.from_date_temp = from_date;
      params.to_date_temp = to_date;
      params.status = status;
      params.search = search;
      params.user_agency_id = user_agency_id;
      let request = this.get(
        this.adminService + "order/list_order_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  //Call API get data using in OrderDLC1PageNew.js page
  _getListOrderS1New = async (
    date_from,
    date_to,
    user_agency_id,
    key_word,
    page,
    note
  ) => {
    try {
      var params = this.getDefaultParams();
      params.date_from = date_from;
      params.date_to = date_to;
      params.user_agency_id = user_agency_id;
      params.key_word = key_word;
      params.page = page;
      params.note = note;

      let request = this.get(
        this.adminService + "order_s1/get_list_order_product_provisional_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListRouteSell = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(this.adminService + "route/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailMember = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id
      let request = this.get(
        this.adminService + `saleman/detail`,
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAction = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService +
        "/agency_route_dvnn/get_list_service_dvnn_for_admin",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListStatusAppSerice = async (status) => {
    try {
      var params = this.getDefaultParams();
      params.status = status;

      let request = this.get(this.adminService + "/order/get_status", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _detailRouteSell = async (route_id) => {
    try {
      var params = this.getDefaultParams();
      params.route_id = route_id;
      let request = this.get(this.adminService + "route/detail_route", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListCustomInRoute = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "route/get_list_user_of_route",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListServiceRoute = async (key, agency_id, page) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      params.agency_id = agency_id;
      params.page = page;

      let request = this.get(
        this.adminService + "/admin_route_dvnn/get_all_for_admin",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailServiceRoute = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;

      let request = this.get(
        this.adminService + "/admin_route_dvnn/get_detail_route_for_admin",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailCoorDinatiorOrderS1 = async (order_id) => {
    try {
      var params = this.getDefaultParams();
      params.order_id = order_id;

      let request = this.get(
        this.adminService + "order/get_detail_coordinate",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getOrderFarmer = async (
    agency_id,
    city_id,
    district_id,
    start_date,
    end_date,
    keyword,
    page
  ) => {
    try {
      var params = this.getDefaultParams();
      params.agency_id = agency_id;
      params.city_id = city_id;
      params.district_id = district_id;
      params.start_date = start_date;
      params.end_date = end_date;
      params.keyword = keyword;
      params.page = page;

      let request = this.get(
        this.adminService + "order_farmer/admin/get_all",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailOrderFarm = async (order_farmer_id) => {
    try {
      var params = this.getDefaultParams();
      params.order_farmer_id = order_farmer_id;

      let request = this.get(
        this.adminService + "order_farmer/admin/get_detail_order_farmer",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailTacVu = async (action_id) => {
    try {
      var params = this.getDefaultParams();
      params.action_id = action_id;

      let request = this.get(
        this.adminService + "agency_action/detail_action_for_admin",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailOrderS1 = async (order_id) => {
    try {
      var params = this.getDefaultParams();
      params.order_id = order_id;
      let request = this.get(
        this.adminService + "order/get_order_agency_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  // get API detail order agency Temporary
  _getDetailOrderTemporary = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;

      let request = this.get(
        this.adminService + "order_s1/get_info_order_product_provisional_s1",
        params
      );

      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailCodeOrderS2 = async (order_code_child) => {
    try {
      var params = this.getDefaultParams();
      params.order_code_child = order_code_child;

      let request = this.get(
        this.adminService + "order/get_order_agency_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailCodeOrderS2 = async (order_s2) => {
    try {
      var params = this.getDefaultParams();
      params.order_s2 = order_s2;
      let request = this.get(
        this.adminService + "agency_order/get_detail_order_s2_in_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  /**
   * Old func
   * @param usertype
   * @param userId
   * @returns {Promise<*>}
   * @private
   */
  _getListAdminAccount = async ({
                                  groupPermissionId,
                                  managementUnitId,
                                  type,
                                  page,
                                  key,
                                }) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      params.key = key;
      params.managementUnitId = managementUnitId;
      params.groupPermissionId = groupPermissionId;
      params.page = page;

      let request = this.get(
        this.adminService + "admincms/get_list_admin_account",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _detailAccount = async ({usertype, userId}) => {
    try {
      var params = this.getDefaultParams();
      params.usertype = usertype;
      params.userId = userId;

      let request = this.get(
        this.adminService + "admincms/get_admin_account_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postCreateAccount = async (dataAdd) => {
    try {
      var params = this.getDefaultParams();
      params = {...dataAdd};

      let request = this.post(
        this.adminService + "admincms/create_admin_account",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListAdminAccountNGP = async ({key, page}) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      params.page = page;
      let request = this.get(this.adminService + "admincms/list", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _detailAccountNGP = async (userId) => {
    try {
      var params = this.getDefaultParams();
      params.id_admin_cms = userId;

      let request = this.get(this.adminService + "admincms/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postCreateAccountAdminNgp = async (obj) => {
    try {
      try {
        var params = this.getDefaultParams();
        params = {...obj};

        let request = this.post(this.adminService + "admincms/create", params);
        let data = await this.execute(request);
        return data;
      } catch (error) {
        throw error;
      }
    } catch (err) {
      throw err;
    }
  };
  _postEditAccountAdminNgp = async (obj) => {
    try {
      try {
        var params = this.getDefaultParams();
        params = {...obj};

        let request = this.post(this.adminService + "admincms/edit", params);
        let data = await this.execute(request);
        return data;
      } catch (error) {
        throw error;
      }
    } catch (err) {
      throw err;
    }
  };
  _getListManagementUnit = async ({type, key}) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      params.key = key;

      let request = this.get(
        this.adminService + "admincms/list_management_unit",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAllCodeOrderS2 = async (id_order_s1, search) => {
    try {
      var params = this.getDefaultParams();
      params.id_order_s1 = id_order_s1;
      params.search = search;
      let request = this.get(
        this.adminService + "agency_order/get_list_order_s2_in_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getCheckExistPriotyInEvent = async (index, type_user) => {
    try {
      var params = this.getDefaultParams();
      params.index = index;
      params.type_user = type_user;
      let request = this.get(
        this.adminService + "/event/news/check_exist_prioty_in_event",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateEventNow = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "/event/news/post_create_event_now",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListEventNow = async (obj) => {
    try {
      const {start_date, end_date, status, type, key, page, group_id} = obj;
      var params = this.getDefaultParams();
      params.start_date = start_date;
      params.end_date = end_date;
      params.status = status;
      params.type = type;
      params.key = key;
      params.page = page;
      params.group_id = group_id;

      let request = this.get(
        this.adminService + "event/news/get_all_event_return",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailEventImmediate = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.event_id = id;

      let request = this.get(
        this.adminService + "/event/news/get_detail_event_return",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListStatisticalEventGiftPage = async (page, id) => {
    try {
      var params = this.getDefaultParams();
      params.page = page;
      params.id = id;

      let request = this.get(
        this.adminService + "/event/get_list_statistical_gift",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  // _getListHistoryStatisticalEventGift = async(obj) => {
  //   try{
  //     var params = this.getDefaultParams();
  //     params = {...obj}
  //     let request = this.get(this.adminService + "/event/get_list_history_update_event", params);
  //     let data = await this.execute(request);
  //     return data;
  //   }
  //   catch (error) {
  //     throw error;
  //   }
  // }

  _getListHistoryStatisticalEventGift = async (page, id) => {
    try {
      var params = this.getDefaultParams();
      params.page = page;
      params.id = id;

      let request = this.get(
        this.adminService + "/event/get_list_history_update_event",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListHistoryStatisticalEventGiftAndLevel = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "/event/get_history_event_object_and_level",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postStopEventImmediate = async (ids) => {
    try {
      var params = this.getDefaultParams();
      params.ids = ids;
      let request = this.post(
        this.adminService + "/event/news/update_status_pause",
        params
      );

      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListProductsC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(this.adminService + "/product/c1/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailProductC1 = async (product_id) => {
    try {
      var params = this.getDefaultParams();
      params.product_id = product_id;

      let request = this.get(this.adminService + "/product/c1/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getAPIDetailDebtAgencyS2 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "/supplier/detail_intergation_by_bill",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getAPIListHotProducts = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "product/get_list_product_hot",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListHotProductsNew = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "product/get_list_product_hot_new",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAPIListHotBrand = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "product/get_list_brand_hot",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateHotBrand = async (arrUpdate) => {
    try {
      let request = this.post(
        this.adminService + "/product/update_brand_hot",
        arrUpdate
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAPIListBrand = async (search) => {
    try {
      var params = this.getDefaultParams();
      params.search = search;

      let request = this.get(
        this.adminService + "product/get_list_brand",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAPIListSelectHotProducts = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "/product/get_droplist_product_hot",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAPIAllListHotProducts = async () => {
    try {
      var params = this.getDefaultParams();
      params.type = -1;
      let request = this.get(
        this.adminService + "/product/get_select_product_hot",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAPIAllListHotProductsNew = async () => {
    try {
      var params = this.getDefaultParams();
      params.type = -1;
      let request = this.get(
        this.adminService + "/product/get_select_product_hot_new",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveHotProduct = async (objRemove) => {
    try {
      let request = this.post(
        this.adminService + "/product/remove_product_hot",
        objRemove
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveHotProductNew = async (objRemove) => {
    try {
      let request = this.post(
        this.adminService + "/product/remove_product_hot_new",
        objRemove
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateHotProducts = async (arrUpdate) => {
    try {
      let request = this.post(
        this.adminService + "/product/update_list_product_hot",
        arrUpdate
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateHotProductsNew = async (arrUpdate) => {
    try {
      let request = this.post(
        this.adminService + "/product/create_list_product_hot_new",
        arrUpdate
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateUserAgencyS2Debt = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;

      let request = this.post(
        this.adminService + "/user/update_user_debt",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailEndUser = async (farmer_id) => {
    try {
      var params = this.getDefaultParams();
      params.farmer_id = farmer_id;
      let request = this.get(this.adminService + "farmer/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _getListEndUser = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(this.adminService + "farmer/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _postRemoveEndUser = async (objRemove) => {
    try {
      let request = this.post(
        this.adminService + "farmer/post_update_status_farmer",
        objRemove
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postUpdateEndUser = async (obj) => {
    try {
      let request = this.post(
        this.adminService + "/farmer/post_update_farmer",
        obj
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListEventBanner = async (search) => {
    try {
      var params = this.getDefaultParams();
      params.search = search;
      let request = this.get(
        this.adminService + "event/news/get_list_event",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _postCreateBanner = async (obj) => {
    try {
      let request = this.post(this.adminService + "/banner/create_banner", obj);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListBanner = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(this.adminService + "/banner/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _getAPIAllListHotBanner = async () => {
    try {
      let request = this.get(
        this.adminService + "banner/get_select_banner_sort"
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _getAPIListSelectHotBanner = async (search) => {
    try {
      var params = this.getDefaultParams();
      params.search = search;
      let request = this.get(
        this.adminService + "banner/list_banner_by_sort_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _getDetailBanner = async (banner_id) => {
    try {
      var params = this.getDefaultParams();
      params.banner_id = banner_id;
      let request = this.get(
        this.adminService + "banner/banner_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _postUpdateHotBanner = async (arrUpdate) => {
    try {
      let request = this.post(
        this.adminService + "/banner/update_list_banner",
        arrUpdate
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListMemberships = async () => {
    try {
      let request = this.get(
        this.adminService + "user_agency/get_droplist_membership"
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListProductType = async () => {
    try {
      let request = this.get(this.adminService + "product/get_list_group");
      // let request = this.get(this.adminService + "/product/get_list_product_type_parent")
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListColorProductVersion = async () => {
    try {
      let request = this.get(this.adminService + "product/get_list_color");
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListUnitProductVersion = async () => {
    try {
      let request = this.get(this.adminService + "product/get_list_unit");
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListAllProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "product/get_all_product",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getDetailProductVersion = async (version_product_id) => {
    try {
      var params = this.getDefaultParams();
      params.version_product_id = version_product_id;
      let request = this.get(
        this.adminService + "product/get_detail_version_product",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListWarrantyPeriod = async () => {
    try {
      let request = this.get(
        this.adminService + "product/get_list_warranty_period"
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListAttributeMax = async () => {
    try {
      let request = this.get(
        this.adminService + "product/get_list_attribute_max"
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListAttributeMin = async () => {
    try {
      let request = this.get(
        this.adminService + "product/get_list_attribute_min"
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _postUpdateStatusProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListWarehouse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/get_list_warehouse"
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _postAddNewWarehouse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(this.adminService + "warehouse/add_new", params);
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateWarehouse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(this.adminService + "warehouse/update", params);
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailWarehouse = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "warehouse/detail_warehouse",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListLevel = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "user_agency/get_droplist_membership",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAllWarehouse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/get_list_warehouse",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListStockReceipt = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/get_list_receipt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailStockReceipt = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "warehouse/get_detail_receipt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getReponsitoryCenterTower = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "center_tower/get_all_reponsitory_center_tower",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListStaff = async (key, page) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      params.page = page;
      let request = this.get(
        this.adminService + "warehouse/get_droplist_staff",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListStaffByShowRoom = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.idAgency = id;
      let request = this.get(
        this.adminService + "saleman/get_sale_man_filter_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListPaymentTransaction = async (rank, status, page, search) => {
    try {
      var params = this.getDefaultParams();
      params.rank = rank;
      params.status = status;
      params.page = page;
      params.search = search;
      let request = this.get(this.adminService + "debt/get_list_debt", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  // An

  _getListReturnDebt = async (membership_id, status, key, page) => {
    try {
      var params = this.getDefaultParams();
      params.membership_id = membership_id;
      params.status = status;
      params.key = key;
      params.page = page;
      let request = this.get(
        this.adminService + "debt/get_all_return_purchased",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListReportReleasesStock = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/report/get_list_report_delivery_bill",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListReportReleasesStockBuild112 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.warehouseservice + "warehouse/get_list_report_delivery_bill",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  // _postCreatePayment = async (user_agency_id, money_payment) => {
  //   try {
  //     var params = this.getDefaultParams();
  //     params.user_agency_id = user_agency_id;
  //     params.money_payment = money_payment;
  //     params.type = 2;
  //     let request = this.post(
  //       "useragencyservice/debt/post_payment_debt",
  //       params
  //     );
  //     let data = await this.execute(request);
  //     return data;
  //   } catch (err) {
  //     throw err;
  //   }
  // };
  // add
  _postReturnDebt = async (dataAdd) => {
    try {
      var params = this.getDefaultParams();
      params = {...dataAdd};
      let request = this.post(
        this.adminService + "debt/add_new_return_purchased",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postConfirmReturnDebt = async (array) => {
    try {
      var params = this.getDefaultParams();
      params = array;
      let request = this.post(
        this.adminService + "debt/confirm_return_purchased",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _postRejectReturnDebt = async (arr) => {
    try {
      var param = this.getDefaultParams();
      param = [...arr];
      let request = this.post(
        this.adminService + "debt/reject_return_purchased",
        param
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListReportReleasesStock = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/report/get_list_report_delivery_bill",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRejectPayment = async (array) => {
    try {
      var params = this.getDefaultParams();
      params = array;
      let request = this.post(
        this.adminService + "debt/post_reject_debt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListReportInputStock = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/report/get_list_report_receipt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListOrderDebit = async (user_agency_id, from_date, to_date, page) => {
    try {
      var params = this.getDefaultParams();
      params.user_agency_id = user_agency_id;
      params.from_date = from_date;
      params.to_date = to_date;
      params.page = page;
      let request = this.get(
        this.adminService + "debt/get_list_history_update_debt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateStockReceipt = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      params.user_name_admin = localStorage.getItem("user_name_login")
        ? localStorage.getItem("user_name_login")
        : "";
      let request = this.post(
        this.adminService + "warehouse/add_new_receipt_new",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postUpdateStockReceipt = async (obj) => {
    let params = this.getDefaultParams();
    params = {...obj};
    params.user_name_admin = localStorage.getItem("user_name_login")
      ? localStorage.getItem("user_name_login")
      : "";
    let request = this.post(
      this.adminService + "warehouse/update_receipt",
      params
    );
    let data = await this.execute(request);
    return data;
  };
  _getListReportInventory = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/report/get_list_report_inventory",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailOrderDebit = async (user_agency_id, from_date, to_date, page) => {
    try {
      var params = this.getDefaultParams();
      params.user_agency_id = user_agency_id;
      params.from_date = from_date;
      params.to_date = to_date;
      params.page = page;
      let request = this.get(
        this.adminService + "debt/get_list_detail_debt_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postConfirmPayment = async (array) => {
    try {
      var params = this.getDefaultParams();
      params = array;
      let request = this.post(
        this.adminService + "debt/confirm_payment_debt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _postCreatePayment = async (user_agency_id, money_payment) => {
    try {
      var params = this.getDefaultParams();
      params.user_agency_id = user_agency_id;
      params.money_payment = money_payment;
      params.type = 2;
      let request = this.post(
        "useragencyservice/debt/post_payment_debt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _getListImportDebit = async (search, page, file) => {
    try {
      var params = this.getDefaultParams();
      params.search = search;
      params.page = page;
      params.file = file;
      let request = this.get(
        this.adminService + "debt/get_list_import_debt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusAgencyC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "user_agency/update_status_user_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _importFileDebit = async (file) => {
    try {
      var params = this.getDefaultParams();
      params.file = file;
      let request = this.multipart(
        this.adminService + "debt/import_debt",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListKeyIndustry = async () => {
    try {
      let request = this.get(
        this.adminService + "user_agency/get_droplist_type_main"
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRejectOrderS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService +
        "order_child_s1_receipt/admin_reject_order_child_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postApprovalOrderS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "order_child_s1_receipt/approve_order_child_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postApprovalListOrderS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService +
        "order_child_s1_receipt/approve_list_order_child_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRejectListOrderS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService +
        "order_child_s1_receipt/admin_reject_list_order_child_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListTreeProductEventGift = async (search, product_type) => {
    try {
      var params = this.getDefaultParams();
      params.search = search;
      params.product_type = product_type;
      let request = this.get(
        this.adminService + "product/get_list_tree_product_type_cate_product",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListEventGift = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(this.adminService + "event/get_all", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateEventGift = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "/event/news/post_create_event",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailEventGift = async (event_id) => {
    try {
      var params = this.getDefaultParams();
      params.event_id = event_id;

      let request = this.get(
        this.adminService + "event/news/get_detail_event",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postActiveOrStopEvent = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "event/news/post_toggle_event",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreatePermisstion = async (dataAdd) => {
    try {
      var params = this.getDefaultParams();
      params = {...dataAdd};

      let request = this.post(
        this.permissionService + "/permission/post_create_group_permission",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListGroupDropdown = async (type, key = "") => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      params.key = key;
      let request = this.get(
        this.permissionService + "permission/get_all_list_group_permission",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListGroupPermisstion = async ({type, page, key}) => {
    try {
      var params = this.getDefaultParams();
      params.type = type;
      params.page = page;
      params.key = key;

      let request = this.get(
        this.permissionService + "permission/get_list_group_permission",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdatePermisstion = async (dataUpdate) => {
    try {
      var params = this.getDefaultParams();
      params = {...dataUpdate};

      let request = this.post(
        this.permissionService + "/permission/post_update_group_permission",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailGroupPermisstion = async (id, type) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      params.user_type = type;

      let request = this.get(
        this.permissionService + "permission/get_detail_group_permission",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateListBrandHot = async (arr) => {
    try {
      var params = this.getDefaultParams();
      params = [...arr];

      let request = this.post(
        this.adminService + "product/update_list_brand_hot",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusStockReceipt = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/warehouse/admin_update_status_receipt",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusStockReceiptForBuild112 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "/warehouse/update_status_receipt",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreatePriceByObject = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "product/add_product_multi_price",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListProductByObject = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "product/get_list_product_multi_price",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _getListProductCategory = async (obj) => {
    try {
      let params = this.getDefaultParams();
      let {key} = obj;
      params.key = key;
      let request = this.get(
        this.adminService + "/product_type_cate/get_list",
        params
      );

      const data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _getDetailProductCategory = async (obj) => {
    try {
      let {id, type} = obj;
      let params = this.getDefaultParams();
      params.id = id;
      params.type = type;

      const request = this.get(
        this.adminService + "/product_type_cate/detail",
        params
      );
      const data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _postAddNewProductCategory = async (obj) => {
    try {
      let {
        type,
        status,
        product_type_parent_id,
        product_type_id,
        name,
        image_path,
      } = obj;
      let params = this.getDefaultParams();
      params.type = type;
      params.status = status;
      params.product_type_parent_id = product_type_parent_id;
      params.product_type_id = product_type_id;
      params.name = name;
      params.image_path = image_path;

      let request = this.post(
        this.adminService + "/product_type_cate/add_new",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postStopProductByObject = async (arr) => {
    try {
      var params = this.getDefaultParams();
      params = [...arr];
      let request = this.post(
        this.adminService + "product/update_status_product_multi_price",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getTreePermission = async (typeUser) => {
    try {
      var params = this.getDefaultParams();
      params.typeUser = typeUser;

      let request = this.get(
        this.permissionService + "permission/get_list_group_permission_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateProductCategory = async (obj) => {
    try {
      let {
        type,
        status,
        product_type_parent_id,
        product_type_id,
        name,
        image_path,
        cate_id,
      } = obj;
      let params = this.getDefaultParams();
      params.type = type;
      params.status = status;
      params.product_type_parent_id = product_type_parent_id;
      params.product_type_id = product_type_id;
      params.name = name;
      params.image_path = image_path;
      params.cate_id = cate_id;
      let request = this.post(
        this.adminService + "product_type_cate/update",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListProductBanner = async (key, product_type_parent) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      params.product_type_parent = product_type_parent;
      let request = this.get(
        this.adminService + "product/get_droplist_product_banner",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListEventAgencyS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "event/get_all_event_user_agency_join_admin",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListHideEventAgencyS1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "event/get_list_event_user_join_and_block",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListDropListNote = async () => {
    try {
      let request = this.get(
        this.adminService + "order_s1/get_list_order_note"
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListOrderC1NoteResponse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService +
        "order_child_s1_receipt/get_list_note_order_child_s1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListHistoryInventory = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/report/list_log_warehouse",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAllVersionProduct = async (search, page) => {
    try {
      var params = this.getDefaultParams();
      params.key = search;
      params.page = page;
      let request = this.get(
        this.adminService + "product/drop_list_product_version",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAllEventRunning = async (search) => {
    try {
      var params = this.getDefaultParams();
      params.search = search;
      let request = this.get(
        this.adminService + "event/news/get_droplist_event_running",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAccountAgencyC1 = async (user_agency_id) => {
    try {
      var params = this.getDefaultParams();
      params.user_agency_id = user_agency_id;
      let request = this.get(
        this.adminService + "user_agency/list_user_agency_register_multi_login",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListCateBlock = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "product/get_list_product_type_parent_block",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSubCateBlock = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "product/get_list_product_type_block",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateEventNowNew = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "/event/news/post_create_event_now_by_parent",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListProductBlock = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "product/get_list_product_block",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateExtraAccount = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "user_agency/user_agency_register_multi_login",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _listEventChildParentID = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.event_parent_id = id;
      let request = this.get(
        this.adminService +
        "event/news/get_list_event_child_by_event_parent_id",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListBrandBlock = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "product/get_list_brand_block",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateInfoAndStatusExtraAccount = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService +
        "user_agency/update_user_agency_password_multi_login",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListEventBlock = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "event/get_list_event_block",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateCateBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/create_product_type_parent_block",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateSubCateBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/create_product_type_block",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateProductBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/create_product_block",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateBrandBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/create_brand_block",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateEventBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "event/create_event_bolck",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusCateBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/update_product_type_parent_block",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusSubCateBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/update_product_type_block",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusProductBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/update_product_block",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusBrandBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/update_brand_block",
        params
      );
      let data = await this.execute(request);

      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusEventBlock = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "event/update_event_block",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdatePriorityEvent = async (dataUpdate) => {
    try {
      var params = this.getDefaultParams();
      params = [...dataUpdate];

      let request = this.post(
        this.adminService + "event/news/update_priority_event",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListEventNowParent = async (key = "") => {
    try {
      var params = this.getDefaultParams();
      params.key = key;

      let request = this.get(
        this.adminService + "/event/news/get_list_event_now_parent",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusDecentralizationUserAdmin = async (arr) => {
    try {
      let params = this.getDefaultParams();
      params = [...arr];
      let request = this.post(
        this.adminService + "admincms/update_active",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusDecentralizationUserPKD = async (arr) => {
    try {
      let params = this.getDefaultParams();
      params = [...arr];
      let request = this.post(
        this.adminService + "agency/update_active",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusDecentralizationUserPDV = async (arr) => {
    try {
      let params = this.getDefaultParams();
      params = [...arr];
      let request = this.post(
        this.adminService + "center_tower/update_active",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postDeActiveDecentralizationUserAdmin = async (arr) => {
    try {
      let params = this.getDefaultParams();
      params = [...arr];
      let request = this.post(
        this.adminService + "admincms/update_deactive",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postDeActiveDecentralizationUserPKD = async (arr) => {
    try {
      let params = this.getDefaultParams();
      params = [...arr];
      let request = this.post(
        this.adminService + "agency/update_deactive",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postDeActiveDecentralizationUserPDV = async (arr) => {
    try {
      let params = this.getDefaultParams();
      params = [...arr];
      let request = this.post(
        this.adminService + "center_tower/update_deactive",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusDecentralizationGroup = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.permissionService +
        "permission/post_update_status_group_permission",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postApproveProductMultiPrice = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "product/approve_product_multi_price",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusGroupPermissionAdmin = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.permissionService +
        "permission/post_update_status_group_permission_admin",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusGroupPermissionPKD = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.permissionService +
        "permission/post_update_status_group_permission_pkd",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusGroupPermissionPDV = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.permissionService +
        "permission/post_update_status_group_permission_pdv",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postDeActiveUserAgencyC1 = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "user_agency/update_status_deactivate_user_agency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListEventRunningByObject = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "event/get_list_event_running",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListPermissionAdminByAgencyC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "permission/list_admin_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListPermissionPKDByAgencyC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "permission/list_agency_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAccount = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "admincms/get_list_account",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateAdminPermissionsDLC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "permission/create_admin_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };

  _postUpdateUnStatusGroupPermissionAdmin = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.permissionService +
        "permission/post_update_un_status_group_permission_admin",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreatePKDPermissionsDLC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "permission/create_agency_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateUnStatusGroupPermissionPKD = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.permissionService +
        "permission/post_update_un_status_group_permission_pkd",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postUpdateAdminPermissionsDLC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "permission/edit_admin_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateUnStatusGroupPermissionPDV = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.permissionService +
        "permission/post_update_un_status_group_permission_pdv",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postUpdatePKDPermissionsDLC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "permission/edit_agency_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postActiveAdminPermissionsDLC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "permission/activate_admin_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postActivePKDPermissionsDLC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "permission/activate_agency_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postDeActiveAdminPermissionsDLC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "permission/deactivate_admin_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postDeActivePKDPermissionsDLC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.post(
        this.adminService + "permission/deactivate_agency_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailPermissionAdminByAgencyC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "permission/detail_admin_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailPermissionPKDByAgencyC1 = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "permission/detail_agency_permission_dlc1",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListAccountCheckExist = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};

      let request = this.get(
        this.adminService + "permission/list_account_check_exist",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveSell = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "route/delete_route", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSalesmanNotRoute = async (show_rom_id, bussiness_id) => {
    try {
      var params = this.getDefaultParams();
      params.show_rom_id = show_rom_id;
      params.business_channel_id = bussiness_id;
      let request = this.get(
        this.adminService + "route/get_list_saleman_not_have_route",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateRouteSell = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "route/update_route_detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddRouteSell = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "route/add_new_route",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDrowpAgencyS2 = async (obj) => {
    try {
      const {key_word, page} = obj;
      var params = this.getDefaultParams();
      params.key_word = key_word;
      params.page = page;
      let request = this.get(this.adminService + "userS2/get_list", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getSelectFrequency = async (from_limit, to_limit) => {
    try {
      var params = this.getDefaultParams();
      params.from_limit = from_limit;
      params.to_limit = to_limit;
      let request = this.get(
        this.adminService + "admin_route/get_list_frequency",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateAction = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "agency_action/edit_action",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddAction = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "agency_action/add_action",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListRoute = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "agency_route_dvnn/get_list_route",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _detailRouteSell = async (route_id) => {
    try {
      var params = this.getDefaultParams();
      params.route_id = route_id;
      let request = this.get(this.adminService + "route/detail_route", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRemoveRouterService = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "agency_route_dvnn/update_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDroplistSalesman = async (key, id, agencyId) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      params.route_dvnn_id = id;
      params.agencyId = agencyId;
      let request = this.get(
        this.adminService + "admin_route_dvnn/get_droplist_saleman_dvnn",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListLocality = async (obj) => {
    try {
      const {route_dvnn_id, key, district_id, city_id, page, agencyId} = obj;
      var params = this.getDefaultParams();
      params.key = key;
      params.page = page;
      params.route_dvnn_id = route_dvnn_id;
      params.district_id = district_id;
      params.city_id = city_id;
      params.agencyId = agencyId;
      let request = this.get(
        this.adminService + "admin_route_dvnn/get_list_region",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postAddSalesman = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "saleman/add_new_saleman",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateSalesman = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "saleman/update_saleman",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailMemberNew = async (id) => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "admin_saleman/detail_saleman/" + id,
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _deleteNhanVien = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "admin_saleman/delete_salesman",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _deleteNhanVienSale = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "saleman/delete", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDropListSuppliers = async (obj) => {
    try {
      var params = this.getDefaultParams();

      params = {...obj};

      let request = this.get(
        this.adminService + "admin_route/get_list_supplier",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListReleaseStockReceipts = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/get_list_delivery_bill",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListOutPutWarehouse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "warehouse/list_output",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailReleasesStockReceipt = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "warehouse/get_detail_delivery_bill",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailOutPut = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "warehouse/detail_output",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postOutPutWarehouse = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "warehouse/output", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postConvertQuantityAllProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "product/convert_to_quantity_min_unit_list_product",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postConvertQuantityProduct = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "product/convert_to_quantity_min_unit_product",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getAPITemplateReceipt = async () => {
    try {
      let request = this.get(
        this.adminService + "warehouse/export_template_receipt"
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postRequestApprovalReceipt = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = obj;
      let request = this.post(
        this.adminService + "warehouse/update_receipt_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSalesManFilter = async () => {
    try {
      let request = this.get(this.adminService + "saleman/get_sale_man_filter");
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListOrderRequest = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "order_child_s1/get_list",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSalesManType = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "saleman/list_sale_man_type",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailOrderRequest = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "order_child_s1/detail",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postUpdateStatusOrderRequest = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "order_child_s1/edit",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListShowroom = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "route/get_list_show_rom",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListManager = async (show_room_id) => {
    try {
      var params = this.getDefaultParams();
      params.list_show_room_id = [...show_room_id];
      let request = this.get(
        this.adminService + "saleman/list_manager",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListManagerWithBussinessChanel = async (show_room_id, channel_id) => {
    try {
      var params = this.getDefaultParams();
      params.showroom_id = [...show_room_id];
      params.channel_id = [...channel_id];
      let request = this.get(
        this.adminService + "saleman/manager_filter_showroom_channel",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListSalesManDropList = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "user/get_drop_down_sale_man",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListSalesManDropListHasRouter = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "user/get_drop_down_sale_man_have_route",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListCustomer = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(this.adminService + "user/get_list_user", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListQuestion = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "user/get_list_question",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSource = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "user/get_list_source",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCreateCustomer = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "user/create_user", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  async _postEditSaleManOfCustomer(obj) {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "user/edit", params);
      const data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  }

  async _editCustomer(obj) {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "user/update_user", params);
      const data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  }

  _getDetailCustomer = async (id_user) => {
    try {
      var params = this.getDefaultParams();
      params.id_user = id_user;
      let request = this.get(
        this.adminService + `user/get_detail_user`,
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListSurveyCustomer = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + `user/get_history_survey`,
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListProductForCustomer = async (key) => {
    try {
      var params = this.getDefaultParams();
      params.key = key;
      let request = this.get(
        this.adminService + `product/list_product_pop_up`,
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListStatusDropListContract = async () => {
    try {
      var params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "contract/filter_status_contract",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListContract = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      params.key = obj.search;
      let request = this.get(
        this.adminService + "contract/get_list_contract",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getDetailContract = async (id) => {
    try {
      var params = this.getDefaultParams();
      params.id = id;
      let request = this.get(
        this.adminService + "contract/get_detail_contract",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postCancelContract = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "contract/delete_contract",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _postEditContract = async (editFilePaper, editHoSoGiaiNgan) => {
    try {
      var params = this.getDefaultParams();
      params.editFilePaper = [...editFilePaper];
      params.editHoSoGiaiNgan = [...editHoSoGiaiNgan];
      let request = this.post(
        this.adminService + "contract/edit_contract",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postImportExcelProductStock = async (file) => {
    let params = this.getDefaultParams();
    params.file = file;
    let request = this.multipart(
      this.adminService + "warehouse/import_file_product_receipt",
      params
    );
    let data = await this.execute(request);
    return data;
  };

  _getListCustomerSurvey = async (page) => {
    try {
      let params = this.getDefaultParams();
      params.page = 1;
      let request = this.get(
        this.adminService + "customer_survery/get_all",
        params
      );
      let result = await this.execute(request);
      return result;
    } catch (err) {
      throw err;
    }
  };
  _getDetailCustomerSurvey = async (id) => {
    try {
      let params = this.getDefaultParams();
      params.id = id;
      let response = this.get(
        this.adminService + "customer_survery/detail",
        params
      );
      let data = await this.execute(response);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _updateCustomerSurvey = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let response = this.post(
        this.adminService + "customer_survery/update_customer_survey",
        params
      );
      let data = await this.execute(response);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _getListKpiManagement = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(this.adminService + "target/list", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListShowroomKpiManagement = async () => {
    try {
      let params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "target/dropdown_show_room",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };

  _getListStatusKpiManagement = async () => {
    try {
      let params = this.getDefaultParams();
      let request = this.get(
        this.adminService + "target/dropdown_status",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postKpiApproves = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      params.username = !localStorage.getItem("user_name_login")
        ? ""
        : localStorage.getItem("user_name_login");
      let request = this.post(this.adminService + "target/approves", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListCreaterKpimanagement = async () => {
    try {
      let params = this.getDefaultParams();
      params.username = !localStorage.getItem("user_name_login")
        ? ""
        : localStorage.getItem("user_name_login");
      let request = this.get(this.adminService + "target/get_name", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailKpiManagement = async (id) => {
    try {
      let params = this.getDefaultParams();
      params.id = id;
      let request = this.get(this.adminService + "target/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _filterShowroomForDate = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "target/dropdown_show_room_target",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getListShowRoomEdit = async (objDateTime, id) => {
    try {
      let params = this.getDefaultParams();
      params = {...objDateTime};
      params.id = id;
      let request = this.get(
        this.adminService + "target/dropdown_show_room_target_edit",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (err) {
      throw err;
    }
  };
  _getListTargetKpi = async () => {
    try {
      let params = this.getDefaultParams();
      let request = this.get(this.adminService + "target/list_target", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _createTargetKpi = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(this.adminService + "target/create", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _editTargetKpi = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      params.username = !localStorage.getItem("user_name_login")
        ? ""
        : localStorage.getItem("user_name_login");
      let request = this.post(this.adminService + "target/edit", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _postFileNameOrder = async (obj) => {
    try {
      var params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "order_child_s1/save_pdf_order",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (e) {
      throw e;
    }
  };
  _getListTypeOfPayment = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.get(
        this.adminService + "payment/get_list_payment",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getDetailTypeOfPayment = async (id) => {
    try {
      let params = this.getDefaultParams();
      params.id = id;
      let request = this.get(this.adminService + "payment/detail", params);
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _updateTypeOfPayment = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let request = this.post(
        this.adminService + "payment/update_payment",
        params
      );
      let data = await this.execute(request);
      return data;
    } catch (error) {
      throw error;
    }
  };
  _getScheduleCar = async (id) => {
    try {
      let params = this.getDefaultParams();
      params.order_child_s1_id = id;
      let req = this.get(this.adminService + "contract/schedule_car", params);
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e;
    }
  };
  _updateScheduleCar = async (arr) => {
    try {
      let params = this.getDefaultParams();
      params = [...arr];
      let req = this.post(
        this.adminService + "contract/edit_schedule_car",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e;
    }
  };
  _getListAllFeatureCheckbox = async () => {
    try {
      let params = this.getDefaultParams();
      let req = this.get(
        this.adminService + "admincms/list_all_feature",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e;
    }
  };
  _deleteAccount = async (arr) => {
    try {
      let params = this.getDefaultParams();
      params.list_id = [...arr]
      let req = this.post(
        this.adminService + "admincms/delete",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e;
    }
  }
  _getListFeatureLeftMenu = async () => {
    try {
      let params = this.getDefaultParams();
      let req = this.get(
        this.adminService + "admincms/list_feature",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _accessPrint = async () => {
    try {
      let params = this.getDefaultParams();
      let req = this.post(
        this.adminService + "contract/print",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _accessApproveContract = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let req = this.post(
        this.adminService + "order_child_s1/approve",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _accessCloseContract = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let req = this.post(
        this.adminService + "order_child_s1/close",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _accessCancelContract = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj};
      let req = this.post(
        this.adminService + "order_child_s1/cancel",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _getLstBussinesChanel = async () => {
    try {
      let params = this.getDefaultParams();
      let req = this.get(
        this.adminService + "saleman/business_channel",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _getLstRouterCreateUser = async (business_id) => {
    try {
      let params = this.getDefaultParams();
      params.business_chanel_id = business_id
      let req = this.get(
        this.adminService + "user/route_when_create_user",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _seeFramesNumber = async (code) => {
    try {
      let params = this.getDefaultParams();
      params.product_code = code
      params.status = 1
      let req = this.get(
        this.adminService + "warehouse/get_detail_product_frame_seri",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _getListFramesbyProduct = async (obj) => {
    try {
      let params = this.getDefaultParams();
      params = {...obj}
      let req = this.get(
        this.adminService + "warehouse/get_frames_by_product",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _getListSeriByFrames = async obj => {
    try {
      let params = this.getDefaultParams();
      params = {...obj}
      let req = this.get(
        this.adminService + "warehouse/filter_seri_by_frames_and_product",
        params
      );
      let data = await this.execute(req);
      return data;
    } catch (e) {
      throw e
    }
  }
  _getListFramesInHistoryInventory = async (obj) => {
    try {
      let params = this.getDefaultParams()
      params = {...obj}
      let rq = this.get(this.warehouseservice + "warehouse/get_detail_receipt_product_frame_seri", params);
      let data = await this.execute(rq);
      return data;
    } catch (e) {
      throw  e
    }
  }
}

const instance = new APIService();
Object.freeze(instance);
export default instance;
