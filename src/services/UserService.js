import { STORAGE } from "../constant";

class UserService {
  constructor() {
    if (!UserService.instance) {
      UserService.instance = this;
    }
    return UserService.instance;
  }

  _getToken = () => {
    return localStorage.getItem(STORAGE.TOKEN) || "";
  };

  _setToken = token => {
    localStorage.setItem(STORAGE.TOKEN, token);
  };
}
const instance = new UserService();
Object.freeze(instance);
export default instance;
