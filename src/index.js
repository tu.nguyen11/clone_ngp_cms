import React from "react";
import ReactDOM from "react-dom";
import { LocaleProvider } from "antd";
import App from "./App";
import { Provider } from "react-redux";

import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.css";
import "antd/dist/antd.css";
import "./index.css";
import "./theme.less";
import vi_VN from "antd/es/locale-provider/vi_VN";
import moment from "moment";
import "moment/locale/vi";
import "./views/components/style.css";  
import store, { persistor } from "./views/store/store";
import { PersistGate } from "redux-persist/integration/react";

moment.locale("vi");
ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <LocaleProvider locale={vi_VN}>
        <App />
      </LocaleProvider>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
