"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reducerProduct = exports.initialProduct = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var initialProduct = {
  productName: "",
  productNameOther: "",
  //ten khac
  different_name: "",
  //ten rut gon
  productCode: "",
  productType: 0,
  cate: 0,
  // nganh hang
  brand: 0,
  // old brand -> nhom hang
  newBrand: 0,
  //cthuong hieu
  outStandingBrand: 0,
  // thuong hieu noi bat
  arrProductVersion: []
};
exports.initialProduct = initialProduct;
var reducerProduct = {
  CREATE_PRODUCT_INFO: function CREATE_PRODUCT_INFO(state, action) {
    state.product[action.payload.key] = action.payload.value;
  },
  CREATE_PRODUCT_VERSION: function CREATE_PRODUCT_VERSION(state, action) {
    state.product[action.payload.key].unshift(action.payload.value);
  },
  SORT_LIST_VERSION: function SORT_LIST_VERSION(state, action) {
    var arrNotStatus = [];
    var arrShow = [];
    var arrHide = [];
    state.product.arrProductVersion.forEach(function (item) {
      if (item.showProductVersion === -1) {
        arrNotStatus.push(item);
      }

      if (item.showProductVersion === 0) {
        arrHide.push(item);
      }

      if (item.showProductVersion === 1) {
        arrShow.push(item);
      }
    });
    state.product.arrProductVersion = [].concat(arrShow, arrNotStatus, arrHide);
  },
  CHECK_PRODUCT_DEFAULT: function CHECK_PRODUCT_DEFAULT(state, action) {
    state.product[action.payload.key] = _toConsumableArray(action.payload.value);
  },
  CREATE_PRODUCT_VERSION_INFO: function CREATE_PRODUCT_VERSION_INFO(state, action) {
    state.product[action.payload.keyRoot][action.payload.idx][action.payload.key] = action.payload.value;
  },
  ADD_SPECIFICATION_INFO: function ADD_SPECIFICATION_INFO(state, action) {
    state.product[action.payload.keyRoot][action.payload.idx][action.payload.key].push(action.payload.value);
  },
  REMOVE_SPECIFICATION_INFO: function REMOVE_SPECIFICATION_INFO(state, action) {
    state.product[action.payload.keyRoot][action.payload.idx][action.payload.key].splice(action.payload.idx1, 1);
  },
  CREATE_PRODUCT_SPECIFICATION_INFO: function CREATE_PRODUCT_SPECIFICATION_INFO(state, action) {
    state.product[action.payload.keyRoot][action.payload.idx][action.payload.keyArr][action.payload.idx1][action.payload.key] = action.payload.value;
  },
  CLEAR_REDUX_PRODUCT_VERSION: function CLEAR_REDUX_PRODUCT_VERSION(state, action) {
    state.product[action.payload.keyRoot].splice(action.payload.idx, 1, _objectSpread({}, state.product[action.payload.keyRoot][action.payload.idx], {}, action.payload.value));
  },
  CHECK_NULL_VALUE_LIST_SPECIFICATION: function CHECK_NULL_VALUE_LIST_SPECIFICATION(state, action) {
    var arrTmp = state.product.arrProductVersion[action.payload].specificationsInfo.filter(function (item) {
      if (item.name !== "" || item.description !== "") {
        return item;
      }
    });
    state.product.arrProductVersion[action.payload].specificationsInfo = arrTmp;
  },
  ADD_ITEM_SPEC_INFO: function ADD_ITEM_SPEC_INFO(state, action) {
    var arSpecificationsInfo = JSON.parse(JSON.stringify(state.product[action.payload.keyRoot][action.payload.idxKeyRoot].specificationsInfo));
    arSpecificationsInfo.push(action.payload.value);
    state.product[action.payload.keyRoot][action.payload.idxKeyRoot].specificationsInfo = arSpecificationsInfo;
    return state;
  },
  ASSIGN_MIN_MAX_UNIT: function ASSIGN_MIN_MAX_UNIT(state, action) {
    state.product.arrProductVersion[action.payload.index].minUnitCode = action.payload.minUnit;
    state.product.arrProductVersion[action.payload.index].maxUnitCode = action.payload.maxUnit;
    return state;
  }
};
exports.reducerProduct = reducerProduct;