const initialProductsByObject = {
  startDate: "",
  type_way: 0,
  arrMembership: [],
  arrayRegion: [],
  arrayCity: [],
  arrayCityClone: [],
  id_agency: 1,
  arrProduct: [],
};

const reducerProductsByObject = {
  CREATE_PRODUCT_BY_OBJECT_INFO: (state, action) => {
    state.productsByObject[action.payload.key] = action.payload.value;
  },

  CHANGE_PRICE_PRODUCT_BY_OBJECT: (state, action) => {
    state.productsByObject[action.payload.key][action.payload.idxProduct][
      action.payload.keyData
    ] = action.payload.valueData;
  },

  CHECK_VALIDATE_PRICE_BY_OBJECT: (state, action) => {
    state.productsByObject[action.payload.key][action.payload.idxProduct][
      action.payload.keyData
    ] = action.payload.check;
  },
};

export { initialProductsByObject, reducerProductsByObject };
