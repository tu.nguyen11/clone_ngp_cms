const initialProduct = {
  productName: "",
  productCode: "",
  pdf_url: "",
  pdf_name: "",
  file_name: "",
  cate: 0, // nganh hang
  brand: 0, // dòng xe
  outStandingBrand: 0, // thương hiệu
  newBrand: 0, // loại xe
  arrProductVersion: [],
};

const reducerProduct = {
  CREATE_PRODUCT_INFO: (state, action) => {
    state.product[action.payload.key] = action.payload.value;
  },

  CREATE_PRODUCT_VERSION: (state, action) => {
    state.product[action.payload.key].unshift(action.payload.value);
  },

  SORT_LIST_VERSION: (state, action) => {
    let arrNotStatus = [];
    let arrShow = [];
    let arrHide = [];
    state.product.arrProductVersion.forEach((item) => {
      if (item.showProductVersion === -1) {
        arrNotStatus.push(item);
      }

      if (item.showProductVersion === 0) {
        arrHide.push(item);
      }

      if (item.showProductVersion === 1) {
        arrShow.push(item);
      }
    });

    state.product.arrProductVersion = [...arrShow, ...arrNotStatus, ...arrHide];
  },

  CHECK_PRODUCT_DEFAULT: (state, action) => {
    state.product[action.payload.key] = [...action.payload.value];
  },

  CREATE_PRODUCT_VERSION_INFO: (state, action) => {
    state.product[action.payload.keyRoot][action.payload.idx][
      action.payload.key
    ] = action.payload.value;
  },

  ADD_SPECIFICATION_INFO: (state, action) => {
    state.product[action.payload.keyRoot][action.payload.idx][
      action.payload.key
    ].push(action.payload.value);
  },

  REMOVE_SPECIFICATION_INFO: (state, action) => {
    state.product[action.payload.keyRoot][action.payload.idx][
      action.payload.key
    ].splice(action.payload.idx1, 1);
  },

  CREATE_PRODUCT_SPECIFICATION_INFO: (state, action) => {
    state.product[action.payload.keyRoot][action.payload.idx][
      action.payload.keyArr
    ][action.payload.idx1][action.payload.key] = action.payload.value;
  },

  CLEAR_REDUX_PRODUCT_VERSION: (state, action) => {
    state.product[action.payload.keyRoot].splice(action.payload.idx, 1, {
      ...state.product[action.payload.keyRoot][action.payload.idx],
      ...action.payload.value,
    });
  },

  CHECK_NULL_VALUE_LIST_SPECIFICATION: (state, action) => {
    let arrTmp = state.product.arrProductVersion[
      action.payload
    ].specificationsInfo.filter((item) => {
      if (item.name !== "" || item.description !== "") {
        return item;
      }
    });

    state.product.arrProductVersion[action.payload].specificationsInfo = arrTmp;
  },

  ADD_ITEM_SPEC_INFO: (state, action) => {
    let arSpecificationsInfo = JSON.parse(
      JSON.stringify(
        state.product[action.payload.keyRoot][action.payload.idxKeyRoot]
          .specificationsInfo
      )
    );

    arSpecificationsInfo.push(action.payload.value);

    state.product[action.payload.keyRoot][
      action.payload.idxKeyRoot
    ].specificationsInfo = arSpecificationsInfo;

    return state;
  },

  ASSIGN_MIN_MAX_UNIT: (state, action) => {
    state.product.arrProductVersion[action.payload.index].minUnitCode =
      action.payload.minUnit;
    state.product.arrProductVersion[action.payload.index].maxUnitCode =
      action.payload.maxUnit;
    return state;
  },
};

export { initialProduct, reducerProduct };
