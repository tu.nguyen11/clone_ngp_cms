const initialEventGift = {
  recommended_time: 0,
  type_event_status: 0,
  type_way: 0,
  conditions_apply: 2,
  show_name_event: false,
  event_filter: {
    arrayRegion: [],
    arrayCity: [],
    arrayCityClone: [],
    id_agency: 1,
    membership: [],
  },
  event_design: {
    key_type_product: 1,
    reward: [],
    rewardQuantity: [],
  },
  id_count_save_item: 0,
  content_event: "",
  code_event: "",
  name_event: "",
  img_event: "",
  IsEdit: false,
  URl_img: "",
};

const reducerEventGift = {
  CREATE_INFO_EVENT_GIFT: (state, action) => {
    state.eventGift[action.payload.key] = action.payload.value;
    return state;
  },
  CREATE_EVENT_GIFT_FILTER: (state, action) => {
    state.eventGift.event_filter[action.payload.key] = action.payload.value;
    return state;
  },
  ADD_EVENT_GIFT_LEVEL: (state, action) => {
    state.eventGift.event_design[action.payload.key].push(action.payload.data);
    return state;
  },
  REMOVE_EVENT_GIFT_LEVEL: (state, action) => {
    state.eventGift.event_design[action.payload.key].splice(
      action.payload.idx,
      1
    );
    return state;
  },
  UPDATE_KEY_CATE_EVENT_GIFT: (state, action) => {
    state.eventGift.event_design.key_type_product = action.payload;
    state.treeProductEventGiftGroup.listArrListTreeConfirm = [];
    state.treeProductEventGiftGroup.listArrDisabled = [];
    state.treeProductEventGiftGroup.listKeyCloneDisabled = [];
    state.treeProductEventGiftGroup2.listArrListTreeConfirm = [];
    state.treeProductEventGiftGroup2.listArrDisabled = [];
    state.treeProductEventGiftGroup2.listKeyCloneDisabled = [];
    state.eventGift.event_design.rewardQuantity = [];
    state.eventGift.event_design.reward = [];
  },
  UPDATE_DESIGN_EVENT_GIFT: (state, action) => {
    state.eventGift.event_design[action.payload.key][action.payload.idx][
      action.payload.idxProduct
    ][action.payload.keyData] = action.payload.valueData;
  },
  CHECK_VALIDATE_EVENT_GIFT: (state, action) => {
    state.eventGift.event_design[action.payload.key][action.payload.idxRoot][
      action.payload.idx
    ][action.payload.validate] = action.payload.check;
  },

  UPDATE_ID_COUNT_SAVE_ITEM: (state, action) => {
    state.eventGift[action.payload.key] = action.payload.value;
    return state;
  },

  ADD_PRODUCT_GROUP_LEVEL: (state, action) => {
    if (action.payload.conditions_apply === 2) {
      if (state.eventGift.event_design.reward.length === 0) {
        state.eventGift.event_design.reward.push([action.payload.dataSaleNew]);
      } else {
        state.eventGift.event_design.reward.map((item) => {
          item.push(action.payload.dataSaleNew);
        });
      }
    } else {
      if (state.eventGift.event_design.rewardQuantity.length === 0) {
        state.eventGift.event_design.rewardQuantity.push([
          action.payload.dataQuantityNew,
        ]);
      } else {
        state.eventGift.event_design.rewardQuantity.map((item) => {
          item.push(action.payload.dataQuantityNew);
        });
      }
    }
    return state;
  },

  ADD_DESIGN_EVENT_LEVEL_GROUP: (state, action) => {
    if (action.payload.conditions_apply === 2) {
      let dataSaleNew = [];
      const length = state.eventGift.event_design.reward.length;
      if (length > 0) {
        state.eventGift.event_design.reward[length - 1].map((item, index) => {
          dataSaleNew.push({
            ...item,
            number_to: 0,
            discount: 0,
            valid_number_to: false,
            valid_discount: false,
          });
        });
      }
      state.eventGift.event_design.reward.push(dataSaleNew);
    } else {
      let dataQuantityNew = [];
      const lengthQuantity = state.eventGift.event_design.rewardQuantity.length;
      if (lengthQuantity > 0) {
        state.eventGift.event_design.rewardQuantity[lengthQuantity - 1].map(
          (item, index) => {
            dataQuantityNew.push({
              ...item,
              number_to: 0,
              discount: 0,
              conversion_rate: 0,
              valid_number_to: false,
              valid_discount: false,
              valid_conversion_rate: false,
            });
          }
        );

        state.eventGift.event_design.rewardQuantity.push(dataQuantityNew);
      }
    }
  },

  REMOVE_LEVEL: (state, action) => {
    let index = action.payload;

    let rewardClone = JSON.parse(
      JSON.stringify(state.eventGift.event_design.reward)
    );

    let rewardQuantityClone = JSON.parse(
      JSON.stringify(state.eventGift.event_design.rewardQuantity)
    );

    rewardClone.splice(index, 1);
    rewardQuantityClone.splice(index, 1);

    state.eventGift.event_design.reward = rewardClone;
    state.eventGift.event_design.rewardQuantity = rewardQuantityClone;
    return state;
  },

  CHANGE_CONDITION_APPLY: (state, action) => {
    if (action.payload.value === 2) {
      state.eventGift.event_design.rewardQuantity = [];
      if (state.treeProductEventGiftGroup.listArrListTreeConfirm.length > 0) {
        state.eventGift.event_design.reward.push([]);
        state.treeProductEventGiftGroup.listArrListTreeConfirm.forEach(
          (item) => {
            state.eventGift.event_design.reward[0].push({
              number_to: 0,
              discount: 0,
              valid_number_to: false,
              valid_discount: false,
            });
          }
        );
      }
    } else {
      state.eventGift.event_design.reward = [];
      if (state.treeProductEventGiftGroup.listArrListTreeConfirm.length > 0) {
        state.eventGift.event_design.rewardQuantity.push([]);
        state.treeProductEventGiftGroup.listArrListTreeConfirm.forEach(
          (item) => {
            state.eventGift.event_design.rewardQuantity[0].push({
              number_to: 0,
              discount: 0,
              conversion_rate: 0,
              valid_number_to: false,
              valid_discount: false,
              valid_conversion_rate: false,
            });
          }
        );
      }
    }
    return state;
  },
};

export { initialEventGift, reducerEventGift };
