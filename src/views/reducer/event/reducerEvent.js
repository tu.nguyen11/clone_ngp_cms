import { DefineKeyEvent } from "../../../utils/DefineKey";

const initialEvent = {
  from_time_apply: 0,
  to_time_apply: 0,
  from_time_paythereward: 0,
  to_time_paythereward: 0,
  type_event_status: 0,
  type_way: 0,
  event_filter: {
    arrayRegion: [],
    arrayCity: [],
    arrayCityClone: [],
    id_agency: 0,
    key_condition_select: false,
  },
  event_degsin: {
    product: [],
    keyProduct: [],
    key_cate: 1,
    reward_sales: [
      {
        money_min: 0,
        money_max: 0,
        quantity_max: 0,
        quantity_min: 0,
        event_reward_type: 0,
        discount: 0,
        discountShow: 0,
        valid_quantity_max: false,
        valid_quantity_min: false,
        valid_discount: false,
      },
    ],
    reward_count: [
      {
        money_min: 0,
        money_max: 0,
        quantity_max: 0,
        quantity_min: 0,
        event_reward_type: 0,
        discount: 0,
        discountShow: 0,
        valid_quantity_max: false,
        valid_quantity_min: false,
        valid_discount: false,
      },
    ],
  },
  content_event: "",
  code_event: "",
  name_event: "",
  img_event: "",
  ISedit: false,
  URl_img: "",
  key_filter_promotion: DefineKeyEvent.filter_sales,
  key_specifications: DefineKeyEvent.big_size,
  key_discount: DefineKeyEvent.percent_discount,
};

const reducerEvent = {
  CREATE_DATE_EVENT: (state, action) => {
    state.event[action.payload.key] = action.payload.value;

    return state;
  },
  CREATE_DATE_EVENT_FILTER: (state, action) => {
    state.event.event_filter[action.payload.key] = action.payload.value;
    return state;
  },
  ADD_DESGIN_EVENT: (state, action) => {
    state.event.event_degsin[action.payload.key].push(action.payload.data);
    return state;
  },
  REMOVE_DESGIN_EVENT: (state, action) => {
    state.event.event_degsin[action.payload.key].splice(action.payload.idx, 1);
    return state;
  },
  UPDATE_DESGIN_EVENT: (state, action) => {
    state.event.event_degsin[action.payload.key][action.payload.idx][
      action.payload.keyData
    ] = action.payload.valueData;
  },
  UPDATE_DESGIN_EVENT_DISCOUNT: (state, action) => {
    if (action.payload.type === "sales") {
      state.event.event_degsin[action.payload.key][action.payload.idx][
        action.payload.keyData
      ] = action.payload.valueData / 100;
      state.event.event_degsin[action.payload.key][action.payload.idx][
        action.payload.keyDataShow
      ] = action.payload.valueData;
    } else {
      if (action.payload.type_count === "sales") {
        state.event.event_degsin[action.payload.key][action.payload.idx][
          action.payload.keyData
        ] = action.payload.valueData / 100;
        state.event.event_degsin[action.payload.key][action.payload.idx][
          action.payload.keyDataShow
        ] = action.payload.valueData;
      } else {
        state.event.event_degsin[action.payload.key][action.payload.idx][
          action.payload.keyData
        ] = action.payload.valueData;
        state.event.event_degsin[action.payload.key][action.payload.idx][
          action.payload.keyDataShow
        ] = action.payload.valueData;
      }
    }
  },
  UPDATE_KEY_REWARD: (state, action) => {
    state.event.event_degsin.reward_count.map((item) => {
      item.event_reward_type = action.payload;
    });
    return state;
  },

  UPDATE_KEY_CATE: (state, action) => {
    state.event.event_degsin.key_cate = action.payload;
    state.treeProduct.listTreeShow = [];
    state.treeProduct.listConfirm = [];
  },
  CHECK_VALIDATE: (state, action) => {
    state.event.event_degsin[action.payload.key][action.payload.idx][
      action.payload.validate
    ] = action.payload.check;
  },
};

export { initialEvent, reducerEvent };
