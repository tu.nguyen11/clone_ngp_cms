import { DefineKeyEvent } from "../../../utils/DefineKey";

const initialEventImmediateNew = {
  from_time_apply: 0,
  to_time_apply: 0,
  type_way: 0,
  check_dependent: 1,
  arr_event_tmp: [],
  arr_event_confirm: [],
  event_dependent_tmp: {},
  event_dependent_confirm: {},
  condition_promotion: 1,
  gift_promotion: 1,
  attribute_promotion: 1,
  check_condition_attach: 0,
  arr_product_confirm: [],
  event_birthday: false,
  show_name_event: false,
  arr_bonus_level: [
    {
      from: 0,
      to: 0,
      quantity_sale: 0,
      arr_product: [],
    },
  ],

  id_count_save_item: 0,

  count_save_group: 1,
  arr_gift_group: [
    {
      from: 0,
      to: 0,
    },
  ],

  arr_product_gift_group: [],

  id_count_save_item_condition_attach: 0,
  arr_condition_attach_group: [],

  key_cate_attach: 0,
  key_condition_attach: 1,
  key_condition_attach_product: 1,
  arr_product_condition_attach: [],

  event_filter: {
    arrayRegion: [],
    arrayCity: [],
    arrayCityClone: [],
    id_agency: 1,
    membership: [],
  },
  event_degsin: {
    product: [],
    keyProduct: [],
    key_type: 1,
    key_cate: 1,
    key_condition: 0,
    key_reward: 0,
    count_save_group: 1,
    id_count_save_item: 0,
    reward: [
      {
        listReward: [],
      },
    ],
    rewardCombo: [],
    reward_condition: [],
  },
  content_event: "",
  code_event: "",
  name_event: "",
  img_event: "",
  ISedit: false,
  URl_img: "",
  pdf_name: "",
  pdf_file: "",
  pdf_url: "",
  sum_sales_order: 0,
  condition_type: DefineKeyEvent.conditon_nguyen_don,
  condition_order_style: DefineKeyEvent.tung_san_pham,
  dataDetail: { event_response: {} },
  is_evet_parent: 1,
  event_priority_type: DefineKeyEvent.song_song,
  value_prioty: 0,
  excommunicate_event_ids: [],
  id_parent: 0,
  loading: false,
};

const reducerEventImmediateNew = {
  CREATE_DATE_EVENT_IMMEDIATE_NEW: (state, action) => {
    state.eventImmediateNew[action.payload.key] = action.payload.value;
    return state;
  },
  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW: (state, action) => {
    state.eventImmediateNew.event_filter[action.payload.key] =
      action.payload.value;
    return state;
  },
  CREATE_KEY_DEGSIN_IMMEDIATE_NEW: (state, action) => {
    state.eventImmediateNew[action.payload.keyRoot][action.payload.key] =
      action.payload.value;
    return state;
  },
  ADD_FIRST_BONUS_LEVEL_EVENT_IMMEDIATE_NEW: (state, action) => {
    let arrBonusLevel = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_bonus_level)
    );

    let arrProduct = JSON.parse(JSON.stringify(action.payload.value));

    let arrBonusLevelNew = arrBonusLevel.map((item) => {
      let arrProductNew = [];
      arrProduct.forEach((itemProduct) => {
        let objProductExist = item.arr_product.find(
          (itemArrProduct) => itemProduct.id === itemArrProduct.id
        );

        if (objProductExist) {
          arrProductNew.push(objProductExist);
        } else {
          arrProductNew.push(itemProduct);
        }
      });
      return { ...item, arr_product: arrProductNew };
    });
    state.eventImmediateNew.arr_bonus_level = arrBonusLevelNew;
    return state;
  },

  ADD_BONUS_LEVEL_EVENT_IMMEDIATE_NEW: (state, action) => {
    let arrBonusLevel = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_bonus_level)
    );

    arrBonusLevel.push(action.payload);

    state.eventImmediateNew.arr_bonus_level = arrBonusLevel;
    return state;
  },

  UPDATE_ARR_ATTACH: (state, action) => {
    let arrBonusLevelAttach = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_product_condition_attach)
    );

    arrBonusLevelAttach[action.payload.index].salesOrQuantity =
      action.payload.value;
    state.eventImmediateNew.arr_product_condition_attach = [
      ...arrBonusLevelAttach,
    ];
    return state;
  },

  ADD_BONUS_LEVEL_ATTACH_EVENT_IMMEDIATE_NEW: (state, action) => {
    let arrBonusLevelAttach = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_product_condition_attach)
    );

    let arrNew = [...action.payload.value];
    let arrBonusLevelAttachNew = [];

    if (arrBonusLevelAttach.length === 0) {
      arrBonusLevelAttachNew = arrNew;
    } else {
      arrNew.forEach((item) => {
        const itemAttach = arrBonusLevelAttach.find(
          (itemAttach) => itemAttach.id === item.id
        );

        if (!itemAttach) {
          arrBonusLevelAttachNew.push(item);
        } else {
          arrBonusLevelAttachNew.push(itemAttach);
        }
      });
    }

    state.eventImmediateNew.arr_product_condition_attach =
      arrBonusLevelAttachNew;
    return state;
  },

  ADD_GIFT_LEVEL_EVENT_IMMEDIATE_NEW: (state, action) => {
    let arrBonusLevel = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_gift_group)
    );

    arrBonusLevel.push(action.payload);

    state.eventImmediateNew.arr_gift_group = arrBonusLevel;
    return state;
  },

  ADD_PRODUCT_GROUP_LEVEL_EVENT_IMMEDIATE: (state, action) => {
    let arr = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_condition_attach_group)
    );
    arr.push({
      salesOrQuantityGroup: 0,
    });

    state.eventImmediateNew.arr_condition_attach_group = [...arr];

    return state;
  },

  REMOVE_BONUS_LEVEL: (state, action) => {
    let arrBonusLevel = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_bonus_level)
    );

    arrBonusLevel.splice(action.payload.idx, 1);

    state.eventImmediateNew.arr_bonus_level = [...arrBonusLevel];
    return state;
  },

  REMOVE_GIFT_LEVEL: (state, action) => {
    let arrGiftGroup = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_gift_group)
    );

    let listArrListTreeConfirm = JSON.parse(
      JSON.stringify(state.treeProductImmediateGiftGroup.listArrListTreeConfirm)
    );

    let arrProductGiftGroup = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_product_gift_group)
    );

    arrGiftGroup.splice(action.payload.idx, 1);
    listArrListTreeConfirm.splice(action.payload.idx, 1);
    arrProductGiftGroup.splice(action.payload.idx, 1);

    state.eventImmediateNew.arr_gift_group = [...arrGiftGroup];
    state.treeProductImmediateGiftGroup.listArrListTreeConfirm =
      listArrListTreeConfirm;
    state.eventImmediateNew.arr_product_gift_group = arrProductGiftGroup;
    return state;
  },

  UPDATE_REWARD_PERFORMANCE: (state, action) => {
    state.eventImmediateNew[action.payload.keyRoot][action.payload.idx][
      action.payload.key
    ] = action.payload.value;

    return state;
  },

  UPDATE_REWARD_PERFORMANCE_PRODUCT: (state, action) => {
    state.eventImmediateNew.arr_bonus_level[
      action.payload.idxArrProduct
    ].arr_product[action.payload.idx][action.payload.key] =
      action.payload.value;
    return state;
  },

  UPDATE_GIFT_GROUP_PRODUCT: (state, action) => {
    state.eventImmediateNew.arr_product_gift_group[
      action.payload.idxArrProduct
    ].arr_product[action.payload.idx][action.payload.key] =
      action.payload.value;
    return state;
  },

  UPDATE_FROM_VALUE_LIST_PRODUCT_CONDITION_ATTACH: (state, action) => {
    state.eventImmediateNew.arr_product_condition_attach[action.payload.idx][
      action.payload.key
    ] = action.payload.value;
    return state;
  },

  ADD_PRODUCT_CONDITION_ATTACH: (state, action) => {
    let arrProductConditionAttach = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_product_condition_attach)
    );

    let arrProduct = JSON.parse(JSON.stringify(action.payload.value));
    if (arrProductConditionAttach.length > 0) {
      let arrProductConditionAttachNew = [];

      arrProduct.forEach((itemProduct) => {
        let objExist = arrProductConditionAttach.find(
          (itemProductExist) => itemProductExist.id === itemProduct.id
        );
        if (objExist) {
          arrProductConditionAttachNew.push(objExist);
        } else {
          arrProductConditionAttachNew.push(itemProduct);
        }
      });
      state.eventImmediateNew.arr_product_condition_attach =
        arrProductConditionAttachNew;
    } else {
      state.eventImmediateNew.arr_product_condition_attach =
        action.payload.value;
    }

    return state;
  },

  UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE: (state, action) => {
    state.eventImmediateNew[action.payload.key] = action.payload.value;
    return state;
  },

  ADD_ARR_PRODUCT_GIFT_GROUP: (state, action) => {
    let objOld = {
      ...state.eventImmediateNew.arr_product_gift_group[action.payload.index],
    };

    if (Object.keys(objOld).length > 0) {
      let objNew = JSON.parse(JSON.stringify(action.payload.value));
      let arrProductNew = [];
      objNew.arr_product.forEach((itemNew) => {
        let itemExist = objOld.arr_product.find(
          (itemOld) => itemOld.id === itemNew.id
        );

        if (itemExist) {
          arrProductNew.push(itemExist);
        } else {
          arrProductNew.push(itemNew);
        }
      });

      let arrProductGiftGroup = JSON.parse(
        JSON.stringify(state.eventImmediateNew.arr_product_gift_group)
      );

      let obj = { arr_product: arrProductNew };

      arrProductGiftGroup.splice(action.payload.index, 1, obj);
      state.eventImmediateNew.arr_product_gift_group = arrProductGiftGroup;
    } else {
      state.eventImmediateNew.arr_product_gift_group.push(action.payload.value);
    }

    return state;
  },

  ASSIGNED_EVENT_INFO: (state, action) => {
    let eventParent = { ...state.eventImmediateNew.event_dependent_tmp };
    state.eventImmediateNew.from_time_apply = eventParent.start_date;
    state.eventImmediateNew.to_time_apply = eventParent.end_date;
    state.eventImmediateNew.type_way =
      eventParent.lst_city.length > 0
        ? 1
        : eventParent.user_s1 !== null
        ? 2
        : 3;
    state.eventImmediateNew.event_filter.arrayCity = eventParent.lst_city.map(
      (item) => item.id
    );
    state.eventImmediateNew.event_filter.arrayRegion =
      eventParent.lst_region.map((item) => item.id);
    state.eventImmediateNew.event_filter.id_agency = eventParent.level_id;
    state.eventImmediateNew.event_degsin.key_cate = eventParent.cate_id;
    state.eventImmediateNew.condition_promotion =
      eventParent.application_name === "EVENT_PRODUCT_COUNT" ? 2 : 1;
    state.eventImmediateNew.gift_promotion =
      eventParent.reward_name === "EVENT_REWARD_PRODUCT_DISCOUNT"
        ? 1
        : eventParent.reward_name === "EVENT_REWARD_PRODUCT_MONEY"
        ? 2
        : 3;
    state.eventImmediateNew.content_event = eventParent.description;
    state.eventImmediateNew.img_event = eventParent.image;
    state.modalS1.listS1Show =
      eventParent.user_s1 === null ? [] : eventParent.user_s1;
    state.modalS2.listS2Show =
      eventParent.user_s2 === null ? [] : eventParent.user_s2;
    state.eventImmediateNew.name_event = eventParent.name_event;
    state.eventImmediateNew.pdf_name = eventParent.pdf_name;
    state.eventImmediateNew.code_event = eventParent.code_event;
    state.eventImmediateNew.pdf_url = eventParent.pdf_url;
    state.eventImmediateNew.URl_img = eventParent.url_image;
    return state;
  },

  ASSIGNED_EVENT_INFO_DETAIL: (state, action) => {
    const data = action.payload;
    state.eventImmediateNew.id_parent = data.event_response.id;
    state.eventImmediateNew.dataDetail = { ...data };
    state.eventImmediateNew.is_evet_parent = 2;
    state.eventImmediateNew.from_time_apply = data.event_response.start_date;
    state.eventImmediateNew.to_time_apply = data.event_response.end_date;
    state.eventImmediateNew.type_way =
      data.event_response.lst_city.length > 0 ? 3 : 1;
    state.eventImmediateNew.event_filter.arrayCity =
      data.event_response.lst_city.map((item) => item.id);
    state.eventImmediateNew.attribute_promotion =
      data.event_response.product_atrtibute_type;

    state.eventImmediateNew.event_filter.id_agency =
      data.event_response.level_id;
    state.eventImmediateNew.event_degsin.key_cate =
      data.event_response.company_id;
    state.eventImmediateNew.condition_promotion =
      data.event_response.application_type_id;
    state.eventImmediateNew.gift_promotion = data.event_response.reward_type_id;
    state.eventImmediateNew.content_event = data.event_response.description;
    state.eventImmediateNew.img_event = data.event_response.image;
    state.modalS1.listS1Show =
      data.event_response.type_entry_condition === 2
        ? data.event_response.list_application_object.map((item) => item.id)
        : [];
    state.modalS2.listS2Show =
      data.event_response.type_entry_condition === 3
        ? data.event_response.list_application_object.map((item) => item.id)
        : [];

    state.eventImmediateNew.name_event = data.event_response.name;
    state.eventImmediateNew.pdf_name = data.event_response.pdf_name;
    state.eventImmediateNew.code_event = data.event_response.code;
    state.eventImmediateNew.pdf_url = data.pdf_url;
    state.eventImmediateNew.URl_img = data.image_url;
    return state;
  },

  CLEAR_VALUE_ATTRIBUTE: (state, action) => {
    if (action.payload.gift_promotion !== 3) {
      let arrBonusLevel = JSON.parse(
        JSON.stringify(state.eventImmediateNew.arr_bonus_level)
      );

      let arrBonusLevelNew = arrBonusLevel.map((item) => {
        let arrProductNew = item.arr_product.map((item1) => {
          item1[action.payload.key] = 0;
          return item1;
        });
        return {
          ...item,
          arr_product: arrProductNew,
        };
      });

      state.eventImmediateNew.arr_bonus_level = arrBonusLevelNew;
    } else {
      let arrGiftGroup = JSON.parse(
        JSON.stringify(state.eventImmediateNew.arr_product_gift_group)
      );

      let arrGiftGroupNew = arrGiftGroup.map((item) => {
        let arrProductNew = item.arr_product.map((item1) => {
          item1[action.payload.key] = 0;
          return item1;
        });

        return {
          ...item,
          arr_product: arrProductNew,
        };
      });

      state.eventImmediateNew.arr_product_gift_group = arrGiftGroupNew;
    }

    return state;
  },

  ADD_DESIGN_EVENT_IMMEDIATE_CONDITION_ATTACH_GROUP: (state, action) => {
    let dataNew = [];
    const length = state.eventImmediateNew.arr_condition_attach_group.length;
    if (length > 0) {
      state.eventImmediateNew.arr_condition_attach_group[length - 1].map(
        (item, index) => {
          dataNew.push({
            ...item,
            from: item.from + 1,
          });
        }
      );
    }
    state.eventImmediateNew.arr_condition_attach_group.push(dataNew);
  },

  ADD_PRODUCT_LEVEL_CONDITION_ATTACH_GROUP: (state, action) => {
    if (state.eventImmediateNew.arr_condition_attach_group.length === 0) {
      state.eventImmediateNew.arr_condition_attach_group.push([action.payload]);
    } else {
      state.eventImmediateNew.arr_condition_attach_group.map((item) => {
        item.push(action.payload);
      });
    }
    return state;
  },

  UPDATE_FROM_VALUE_LIST_PRODUCT_CONDITION_ATTACH_GROUP: (state, action) => {
    state.eventImmediateNew.arr_condition_attach_group[action.payload.idx][
      action.payload.idxGroup
    ][action.payload.key] = action.payload.value;
    return state;
  },

  CLEAR_ARR_SETUP_CONDITION_ATTACH: (state, action) => {
    state.eventImmediateNew[action.payload.key] = [];
    if (action.payload.keyTree === "listArrListTreeConfirm") {
      state.treeProductImmediateConditionAttachGroup.listArrListTreeConfirm =
        [];
      state.treeProductImmediateConditionAttachGroup.listArrDisabled = [];
    } else {
      state.treeProductPresentIImmediateConditionAttach.listConfirm = [];
      state.treeProductPresentIImmediateConditionAttach.listTreeShow = [];
      state.treeProductPresentIImmediateConditionAttach.listTreeClone = [];
      state.treeProductPresentIImmediateConditionAttach.listKeyClone = [];
    }

    return state;
  },

  // DELETE_PRODUCT_LEVEL_CONDITION_ATTACH_GROUP: (state, action) => {
  //   state.eventCombo.event_design.reward = state.eventCombo.event_design.reward.map(
  //     (item) => {
  //       let arr = item.listReward.filter((item1) => {
  //         return item1.product_id !== action.payload.product_id;
  //       });
  //       return { listReward: arr };
  //     }
  //   );
  //   return state;
  // },
};

export { initialEventImmediateNew, reducerEventImmediateNew };
