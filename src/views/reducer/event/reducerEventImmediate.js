const initialEventImmediate = {
  from_time_apply: 0,
  to_time_apply: 0,
  type_event_status: 0,
  type_way: 0,
  priori: 0,
  valuePriori: 1,
  condition_product_quantity_min: 0,
  event_filter: {
    arrayRegion: [],
    arrayCity: [],
    arrayCityClone: [],
    id_agency: 0,
  },
  event_degsin: {
    product: [],
    keyProduct: [],
    key_cate: 0,
    key_condition: 0,
    key_reward: 0,
  },
  content_event: "",
  code_event: "",
  name_event: "",
  img_event: "",
  ISedit: false,
  URl_img: "",
};

const reducerEventImmediate = {
  CREATE_DATE_EVENT_IMMEDIATE: (state, action) => {
    state.eventImmediate[action.payload.key] = action.payload.value;
    return state;
  },
  CREATE_DATE_EVENT_FILTER_IMMEDIATE: (state, action) => {
    state.eventImmediate.event_filter[action.payload.key] =
      action.payload.value;
    return state;
  },
  CREATE_KEY_DEGSIN_IMMEDIATE: (state, action) => {
    state.eventImmediate[action.payload.keyRoot][action.payload.key] =
      action.payload.value;
    return state;
  },
};

export { initialEventImmediate, reducerEventImmediate };
