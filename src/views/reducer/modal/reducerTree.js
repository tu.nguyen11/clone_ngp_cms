import { DefineKeyEvent } from "../../../utils/DefineKey";

const initialTreeProduct = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listKeyConfirm: [],
  arrConfirm: [],
  objUpdateShow: {},
  objUpdateConfirm: {},
  keyDefaultProduct: 0,
};

const initialTreeProductImmediate = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
};
const initialTreeProductPresentImmediateDiscount = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
};

const initialTreeProductPresentImmediate = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
};

const initialTreeProductComboGroup = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listArrListTreeConfirm: [],
  listArrDisabled: [],
};

const initialTreeProductImmediateGiftGroup = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listArrListTreeConfirm: [],
  listArrDisabled: [],
};

const initialTreeProductEventGiftGroup = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listKeyConfirm: [],
  listArrListTreeConfirm: [],
  listArrDisabled: [],
};

const initialTreeProductEventGiftGroup2 = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listKeyConfirm: [],
  listArrListTreeConfirm: [],
  listArrDisabled: [],
};
const initialTreeProductConditionGroup = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listArrListTreeConfirm: [],
  listArrDisabled: [],
};

const initialTreeProductConditionPage = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
};

const initialTreeProductImmediateConditionAttachGroup = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listArrListTreeConfirm: [],
  listArrDisabled: [],
};

const initialTreeProductImmediateConditionAttach = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
};

const reducerTreeProduct = {
  REMOVE_PRODUCT_CONFIRM_NEW: (state, action) => {
    const listTreeClone = action.payload.listTreeClone;
    const listTreeShow = action.payload.listTreeShow;
    const arrConfirm = action.payload.arrConfirm;
    const listConfirm = action.payload.listConfirm;
    let listKeyClone = action.payload.listKeyClone;
    let listKeyConfirm = action.payload.listKeyConfirm;
    let objUpdateShow = action.payload.objUpdateShow;
    const product = action.payload.product;

    const ids = product.tree_id.split("-");

    const [lv1, lv2, lv3] = ids;

    // List tree clone
    let objTreeCloneLv1 = listTreeClone.find((item) => item.id === Number(lv1));

    let objTreeCloneLv2 = objTreeCloneLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeCloneLv3 = objTreeCloneLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeCloneTmp = [];

    if (arrTreeCloneLv3.length > 0) {
      let objTreeCloneLv2New = {
        ...objTreeCloneLv2,
        children: arrTreeCloneLv3,
      };

      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1, objTreeCloneLv2New);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      listTreeCloneTmp.splice(idxObjLv1, 1, {
        ...objTreeCloneLv1,
        children: listTreeCloneLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      if (listTreeCloneLv1ChildTmp.length === 0) {
        listTreeCloneTmp.splice(idxObjLv1, 1);
      } else {
        listTreeCloneTmp.splice(idxObjLv1, 1, {
          ...objTreeCloneLv1,
          children: listTreeCloneLv1ChildTmp,
        });
      }
    }

    //List tree show
    let objTreeShowLv1 = listTreeShow.find((item) => item.id === Number(lv1));

    let objTreeShowLv2 = objTreeShowLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeShowLv3 = objTreeShowLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeShowTmp = [];

    if (arrTreeShowLv3.length > 0) {
      let objTreeShowLv2New = {
        ...objTreeShowLv2,
        children: arrTreeShowLv3,
      };

      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1, objTreeShowLv2New);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      listTreeShowTmp.splice(idxObjLv1, 1, {
        ...objTreeShowLv1,
        children: listTreeShowLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      if (listTreeShowLv1ChildTmp.length === 0) {
        listTreeShowTmp.splice(idxObjLv1, 1);
      } else {
        listTreeShowTmp.splice(idxObjLv1, 1, {
          ...objTreeShowLv1,
          children: listTreeShowLv1ChildTmp,
        });
      }
    }

    //List tree confirm
    let objListConfirmLv1 = listConfirm.find((item) => item.id === Number(lv1));

    let objListConfirmLv2 = objListConfirmLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrListConfirmLv3 = objListConfirmLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listConfirmTmp = [];

    if (arrListConfirmLv3.length > 0) {
      let objListConfirmLv2New = {
        ...objListConfirmLv2,
        children: arrListConfirmLv3,
      };

      let idxObjLv2 = objListConfirmLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listConfirmLv1ChildTmp = [...objListConfirmLv1.children];

      listConfirmLv1ChildTmp.splice(idxObjLv2, 1, objListConfirmLv2New);

      let idxObjLv1 = listConfirm.findIndex((item) => item.id === Number(lv1));

      listConfirmTmp = [...listConfirm];

      listConfirmTmp.splice(idxObjLv1, 1, {
        ...objListConfirmLv1,
        children: listConfirmLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objListConfirmLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listConfirmLv1ChildTmp = [...objListConfirmLv1.children];

      listConfirmLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listConfirm.findIndex((item) => item.id === Number(lv1));

      listConfirmTmp = [...listConfirm];

      if (listConfirmLv1ChildTmp.length === 0) {
        listConfirmTmp.splice(idxObjLv1, 1);
      } else {
        listConfirmTmp.splice(idxObjLv1, 1, {
          ...objListConfirmLv1,
          children: listConfirmLv1ChildTmp,
        });
      }
    }

    const arrConfirmNew = arrConfirm.filter((item) => item.id !== product.id);

    const arrConfirmNew1 = arrConfirmNew.map((item, index) => {
      let stt = index + 1;
      return {
        ...item,
        stt: stt,
      };
    });

    const listKeyCloneNew1 = listKeyClone.filter(
      (item) => item !== product.tree_id
    );

    const listKeyCloneNew2 = listKeyCloneNew1.filter(
      (item) => item !== [lv1, lv2].join("-")
    );

    const listKeyCloneNew3 = listKeyCloneNew2.filter((item) => item !== lv1);

    const listKeyConfirmNew1 = listKeyConfirm.filter(
      (item) => item !== product.tree_id
    );
    const idConfirms = product.tree_id.split("-");

    const [lv1Confirm, lv2Confirm, lv3Confirm] = idConfirms;

    const listKeyConfirmNew2 = listKeyConfirmNew1.filter(
      (item) => item !== [lv1Confirm, lv2Confirm].join("-")
    );

    const listKeyConfirmNew3 = listKeyConfirmNew2.filter(
      (item) => item !== lv1Confirm
    );

    let objUpdateShowNew = {};
    if (objUpdateShow) {
      for (let key in objUpdateShow) {
        if (Number(key) !== Number(product.id)) {
          objUpdateShowNew = { ...objUpdateShowNew, [key]: objUpdateShow[key] };
        }
      }
    }

    state[action.payload.keyRoot][action.payload.key_listTreeClone] =
      listTreeCloneTmp;
    state[action.payload.keyRoot][action.payload.key_listTreeShow] =
      listTreeShowTmp;
    state[action.payload.keyRoot][action.payload.key_listConfirm] =
      listConfirmTmp;
    state[action.payload.keyRoot][action.payload.key_arrConfirm] =
      arrConfirmNew1;
    state[action.payload.keyRoot][action.payload.key_listKeyClone] =
      listKeyCloneNew3;
    state[action.payload.keyRoot][action.payload.key_listKeyConfirm] =
      listKeyConfirmNew3;
    state[action.payload.keyRoot][action.payload.key_objUpdateShow] =
      objUpdateShowNew;

    return state;
  },
  REMOVE_OBJ_UNCHECK: (state, action) => {
    const objUpdateShow = JSON.parse(
      JSON.stringify(state[action.payload.keyRoot][action.payload.key])
    );

    const listUnCheck = action.payload.listUnCheck;

    listUnCheck.forEach((id) => {
      delete objUpdateShow[id];
    });

    state[action.payload.keyRoot][action.payload.key] = objUpdateShow;
  },
  ADD_OBJ_UPDATE_CONFIRMED: (state, action) => {
    const objUpdateShow = JSON.parse(
      JSON.stringify(state[action.payload.keyRoot][action.payload.key])
    );

    const arrId = action.payload.arrId;

    arrId.forEach((item) => {
      if ([item.id] in objUpdateShow) return;

      objUpdateShow[item.id] = {
        attributesId: item.attributesId,
        quantity: 1,
      };
    });

    state[action.payload.keyRoot][action.payload.key] = objUpdateShow;
    state[action.payload.keyRoot][action.payload.key_objConfirm] =
      objUpdateShow;
    return state;
  },
  ADD_NEW_OBJ_FROM_SERVER: (state, action) => {
    state[action.payload.keyRoot][action.payload.key] = action.payload.value;
    state[action.payload.keyRoot][action.payload.key_objConfirm] =
      action.payload.value;
    return state;
  },
  ADD_ARR_CONFIRMED: (state, action) => {
    state[action.payload.keyRoot][action.payload.key] =
      action.payload.arrConfirm;
    return state;
  },
  CHANGE_ITEM: (state, action) => {
    const objOld =
      state[action.payload.keyRoot][action.payload.objUpdateShow][
        action.payload.idProduct
      ];

    if (objOld) {
      state[action.payload.keyRoot][action.payload.objUpdateShow][
        action.payload.idProduct
      ] = { ...objOld, ...action.payload.valueUpdate };
      state[action.payload.keyRoot][action.payload.objUpdateConfirm][
        action.payload.idProduct
      ] = { ...objOld, ...action.payload.valueUpdate };
    } else {
      state[action.payload.keyRoot][action.payload.objUpdateShow][
        action.payload.idProduct
      ] = action.payload.valueUpdate;
      state[action.payload.keyRoot][action.payload.objUpdateConfirm][
        action.payload.idProduct
      ] = action.payload.valueUpdate;
    }

    return state;
  },

  ADD_CONTENT_PRODUCT: (state, action) => {
    let tree = JSON.parse(JSON.stringify(action.payload.tree));
    let idProduct = action.payload.id;
    let idxRoot = action.payload.idxRoot;

    let treeNew = tree[idxRoot].treeProduct.map((itemLv1) => {
      let arrLv1Child = itemLv1.children.map((itemLv2) => {
        let arrLv2Child = itemLv2.children.map((itemLv3) => {
          if (itemLv3.id === idProduct) {
            itemLv3.description = action.payload.value;
            return itemLv3;
          } else {
            return itemLv3;
          }
        });
        return {
          ...itemLv2,
          children: [...arrLv2Child],
        };
      });
      return {
        ...itemLv1,
        children: [...arrLv1Child],
      };
    });

    if (action.payload.keyTypeProduct === 1) {
      state.treeProductEventGiftGroup.listArrListTreeConfirm[
        idxRoot
      ].treeProduct = [...treeNew];
    } else {
      state.treeProductEventGiftGroup2.listArrListTreeConfirm[
        idxRoot
      ].treeProduct = [...treeNew];
    }
  },

  ADD_CONTENT_PRODUCT_NEW: (state, action) => {
    let tree = JSON.parse(JSON.stringify(action.payload.tree));
    let idProduct = action.payload.id;

    let treeNew = tree.map((itemLv1) => {
      let arrLv1Child = itemLv1.children.map((itemLv2) => {
        let arrLv2Child = itemLv2.children.map((itemLv3) => {
          if (itemLv3.id === idProduct) {
            itemLv3.description = action.payload.value;
            return itemLv3;
          } else {
            return itemLv3;
          }
        });
        return {
          ...itemLv2,
          children: [...arrLv2Child],
        };
      });
      return {
        ...itemLv1,
        children: [...arrLv1Child],
      };
    });

    state[action.payload.keyRoot].listConfirm = [...treeNew];
    state[action.payload.keyRoot].listTreeShow = [...treeNew];
    state[action.payload.keyRoot].listTreeClone = [...treeNew];
  },

  CLOSE_MODAL_COMBO_CONDITION: (state, action) => {
    if (!action.payload) {
      state.treeProductConditionGroup.listArrDisabled = [
        ...state.treeProductConditionGroup.listArrDisabled,
        // ...state.treeProductConditionGroup.listKeyCloneDisabled
      ];
      state.treeProductConditionGroup.listArrDisabled = [
        ...new Set(state.treeProductConditionGroup.listArrDisabled),
      ];

      state.treeProductConditionGroup.listTreeClone = [];
      state.treeProductConditionGroup.listTreeShow = [];
      state.treeProductConditionGroup.listConfirm = [];
      state.treeProductConditionGroup.listKeyClone = [];
    } else {
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listArrDisabled = [
        ...state.treeProductConditionGroup.listArrDisabled,
        // ...state.treeProductConditionGroup.listKeyCloneDisabled
      ];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listArrDisabled = [
        ...new Set(state.treeProductConditionGroup.listArrDisabled),
      ];

      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listTreeClone = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listTreeShow = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listConfirm = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listKeyClone = [];
    }
  },

  SAVE_TREE_GROUP_COMBO_CONDITION: (state, action) => {
    if (action.payload.checkArrayCondition) {
      state.dataArrayCondition[action.payload.idx][action.payload.keyRoot][
        action.payload.initial_listConfirm
      ] =
        state.dataArrayCondition[action.payload.idx][action.payload.keyRoot][
          action.payload.initial_listShow
        ];
      state.dataArrayCondition[action.payload.idx][
        action.payload.keyRoot
      ].listArrListTreeConfirm.push({
        keyDefaultProduct: `${
          state.dataArrayCondition[action.payload.idx][action.payload.keyRoot][
            action.payload.initial_listShow
          ][0].children[0].id
        }`,
        treeProduct: [
          ...state.dataArrayCondition[action.payload.idx][
            action.payload.keyRoot
          ][action.payload.initial_listShow],
        ],
      });
      if (state.event.condition_order_style === DefineKeyEvent.nhom_san_pham) {
        state.dataArrayCondition[
          action.payload.idx
        ].event.event_degsin.reward_condition.push({
          value_count: 0,
          value_sales: 0,
        });
      }

      if (
        state.dataArrayCondition[action.payload.idx].condition_order_style ===
        DefineKeyEvent.nhom_san_pham
      ) {
        state.dataArrayCondition[
          action.payload.idx
        ].eventCombo.event_design.reward_condition.push({
          value_count: 0,
          value_sales: 0,
        });
      }

      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listArrDisabled = [
        ...state.dataArrayCondition[action.payload.idx]
          .treeProductConditionGroup.listArrDisabled,
        ...state.dataArrayCondition[action.payload.idx]
          .treeProductConditionGroup.listKeyClone,
      ];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listArrDisabled = [
        ...new Set(
          state.dataArrayCondition[
            action.payload.idx
          ].treeProductConditionGroup.listArrDisabled
        ),
      ];

      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listKeyCloneDisabled = [
        ...state.dataArrayCondition[action.payload.idx]
          .treeProductConditionGroup.listKeyClone,
      ];

      // state.event.event_degsin.reward =
      // RESTART CÂY LẠI
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listTreeClone = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listTreeShow = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listConfirm = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listKeyClone = [];

      return state;
    } else {
      state[action.payload.keyRoot][action.payload.initial_listConfirm] =
        state[action.payload.keyRoot][action.payload.initial_listShow];
      state[action.payload.keyRoot].listArrListTreeConfirm.push({
        keyDefaultProduct: `${
          state[action.payload.keyRoot][action.payload.initial_listShow][0]
            .children[0].id
        }`,
        treeProduct: [
          ...state[action.payload.keyRoot][action.payload.initial_listShow],
        ],
      });
      if (state.event.condition_order_style === DefineKeyEvent.nhom_san_pham) {
        state.event.event_degsin.reward_condition.push({
          value_count: 0,
          value_sales: 0,
        });
      }

      if (
        state.eventCombo.condition_order_style === DefineKeyEvent.nhom_san_pham
      ) {
        state.eventCombo.event_design.reward_condition.push({
          value_count: 0,
          value_sales: 0,
        });
      }

      state.treeProductConditionGroup.listArrDisabled = [
        ...state.treeProductConditionGroup.listArrDisabled,
        ...state.treeProductConditionGroup.listKeyClone,
      ];
      state.treeProductConditionGroup.listArrDisabled = [
        ...new Set(state.treeProductConditionGroup.listArrDisabled),
      ];

      state.treeProductConditionGroup.listKeyCloneDisabled = [
        ...state.treeProductConditionGroup.listKeyClone,
      ];

      // state.event.event_degsin.reward =
      // RESTART CÂY LẠI
      state.treeProductConditionGroup.listTreeClone = [];
      state.treeProductConditionGroup.listTreeShow = [];
      state.treeProductConditionGroup.listConfirm = [];
      state.treeProductConditionGroup.listKeyClone = [];

      return state;
    }
  },
  SAVE_TREE_GROUP_COMBO_OVER_CONDITION: (state, action) => {
    if (action.payload.checkArrayCondition) {
      state.dataArrayCondition[action.payload.idx][action.payload.keyRoot][
        action.payload.initial_listConfirm
      ] =
        state.dataArrayCondition[action.payload.idx][action.payload.keyRoot][
          action.payload.initial_listShow
        ];
      state.dataArrayCondition[action.payload.idx][
        action.payload.keyRoot
      ].listArrListTreeConfirm[
        state.dataArrayCondition[
          action.payload.idx
        ].eventCombo.event_design.id_count_save_item
      ].treeProduct = [
        ...state.dataArrayCondition[action.payload.idx][action.payload.keyRoot][
          action.payload.initial_listShow
        ],
      ];

      const idx = state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listKeyClone.indexOf(
        state.dataArrayCondition[action.payload.idx][action.payload.keyRoot]
          .listArrListTreeConfirm[
          state.dataArrayCondition[action.payload.idx].eventCombo.event_design
            .id_count_save_item
        ].keyDefaultProduct
      );
      if (idx === -1) {
        state.dataArrayCondition[action.payload.idx][
          action.payload.keyRoot
        ].listArrListTreeConfirm[
          state.dataArrayCondition[
            action.payload.idx
          ].eventCombo.event_design.id_count_save_item
        ].keyDefaultProduct =
          state.dataArrayCondition[
            action.payload.idx
          ].treeProductConditionGroup.listKeyClone[0];
      }
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listArrDisabled = [
        ...state.dataArrayCondition[action.payload.idx]
          .treeProductConditionGroup.listArrDisabled,
        ...state.dataArrayCondition[action.payload.idx]
          .treeProductConditionGroup.listKeyClone,
      ];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listArrDisabled = [
        ...new Set(
          state.dataArrayCondition[
            action.payload.idx
          ].treeProductConditionGroup.listArrDisabled
        ),
      ];

      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listKeyCloneDisabled = [
        ...state.dataArrayCondition[action.payload.idx]
          .treeProductConditionGroup.listKeyClone,
      ];

      // RESTART CÂY LẠI
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listTreeClone = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listTreeShow = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listConfirm = [];
      state.dataArrayCondition[
        action.payload.idx
      ].treeProductConditionGroup.listKeyClone = [];
    } else {
      state[action.payload.keyRoot][action.payload.initial_listConfirm] =
        state[action.payload.keyRoot][action.payload.initial_listShow];
      state[action.payload.keyRoot].listArrListTreeConfirm[
        state.eventCombo.event_design.id_count_save_item
      ].treeProduct = [
        ...state[action.payload.keyRoot][action.payload.initial_listShow],
      ];

      const idx = state.treeProductConditionGroup.listKeyClone.indexOf(
        state[action.payload.keyRoot].listArrListTreeConfirm[
          state.eventCombo.event_design.id_count_save_item
        ].keyDefaultProduct
      );
      if (idx === -1) {
        state[action.payload.keyRoot].listArrListTreeConfirm[
          state.eventCombo.event_design.id_count_save_item
        ].keyDefaultProduct = state.treeProductConditionGroup.listKeyClone[0];
      }
      state.treeProductConditionGroup.listArrDisabled = [
        ...state.treeProductConditionGroup.listArrDisabled,
        ...state.treeProductConditionGroup.listKeyClone,
      ];
      state.treeProductConditionGroup.listArrDisabled = [
        ...new Set(state.treeProductConditionGroup.listArrDisabled),
      ];

      state.treeProductConditionGroup.listKeyCloneDisabled = [
        ...state.treeProductConditionGroup.listKeyClone,
      ];

      // RESTART CÂY LẠI
      state.treeProductConditionGroup.listTreeClone = [];
      state.treeProductConditionGroup.listTreeShow = [];
      state.treeProductConditionGroup.listConfirm = [];
      state.treeProductConditionGroup.listKeyClone = [];
    }

    return state;
  },
  CLOSE_MODAL_COMBO: (state, action) => {
    state.treeProductComboGroup.listArrDisabled = [
      ...state.treeProductComboGroup.listArrDisabled,
      // ...state.treeProductComboGroup.listKeyCloneDisabled
    ];
    state.treeProductComboGroup.listArrDisabled = [
      ...new Set(state.treeProductComboGroup.listArrDisabled),
    ];

    state.treeProductComboGroup.listTreeClone = [];
    state.treeProductComboGroup.listTreeShow = [];
    state.treeProductComboGroup.listConfirm = [];
    state.treeProductComboGroup.listKeyClone = [];
  },

  SAVE_TREE_GIFT_GROUP_IMMEDIATE: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] =
      state[action.payload.keyRoot][action.payload.initial_listShow];
    state[action.payload.keyRoot].listArrListTreeConfirm.push({
      // keyDefaultProduct: `${
      //   state[action.payload.keyRoot][action.payload.initial_listShow][0]
      //     .children[0].id
      // }`,
      treeProduct: [
        ...state[action.payload.keyRoot][action.payload.initial_listShow],
      ],
    });
    // state.treeProductComboGroup.listArrDisabled = [
    //   ...state.treeProductComboGroup.listArrDisabled,
    //   ...state.treeProductComboGroup.listKeyClone,
    // ];
    // state.treeProductComboGroup.listArrDisabled = [
    //   ...new Set(state.treeProductComboGroup.listArrDisabled),
    // ];

    // state.treeProductComboGroup.listKeyCloneDisabled = [
    //   ...state.treeProductComboGroup.listKeyClone,
    // ];

    // RESTART CÂY LẠI
    state[action.payload.keyRoot].listTreeClone = [];
    state[action.payload.keyRoot].listTreeShow = [];
    state[action.payload.keyRoot].listConfirm = [];
    state[action.payload.keyRoot].listKeyClone = [];

    return state;
  },

  ADD_TREE: (state, action) => {
    let listTreeClone = JSON.parse(
      JSON.stringify(
        state[action.payload.keyRoot][action.payload.initial_listClone]
      )
    );
    let checked = action.payload.data.checked;
    let listTreeCloneNew = [...action.payload.data.tree];

    if (checked) {
      if (listTreeClone.length === 0) {
        state[action.payload.keyRoot][action.payload.initial_listClone] =
          listTreeCloneNew;
      } else {
        listTreeCloneNew.forEach((itemNewLv1) => {
          let isExist = false;
          listTreeClone.forEach((itemLv1, indexLv1) => {
            if (itemLv1.id === itemNewLv1.id) {
              isExist = true;
              itemNewLv1.children.forEach((itemNewLv2) => {
                let isExist2 = false;
                itemLv1.children.forEach((itemLv2, indexLv2) => {
                  if (itemLv2.id === itemNewLv2.id) {
                    isExist2 = true;
                    itemNewLv2.children.forEach((itemNewLv3) => {
                      let index = itemLv2.children.findIndex(
                        (itemCheck) => itemCheck.id === itemNewLv3.id
                      );
                      if (index === -1) {
                        listTreeClone[indexLv1].children[
                          indexLv2
                        ].children.push(itemNewLv3);
                      }
                    });
                  }
                });
                if (isExist2 === false) {
                  listTreeClone[indexLv1].children.push(itemNewLv2);
                }
              });
            }
          });
          if (isExist === false) {
            listTreeClone.push(itemNewLv1);
          }
        });

        state[action.payload.keyRoot][action.payload.initial_listClone] =
          listTreeClone;
      }
    } else {
      if (listTreeCloneNew.length === 0) {
        let dataProduct = JSON.parse(JSON.stringify(state.dataProduct));
        if (dataProduct.length > 1) {
          let listConfirm = JSON.parse(
            JSON.stringify(state.treeProductEventGiftGroup2.listConfirm)
          );

          if (listConfirm.length === 0) {
            state[action.payload.keyRoot][action.payload.initial_listClone] =
              [];
            return;
          }

          listTreeClone.forEach((itemLv1, indexLv1) => {
            itemLv1.children.forEach((itemLv2, indexLv2) => {
              const index = itemLv2.children.findIndex((itemLv3) => {
                return itemLv3.id === listConfirm[0].children[0].children[0].id;
              });
              if (index !== -1) {
                listTreeClone[indexLv1].children[indexLv2].children.splice(
                  index,
                  1
                );

                if (
                  listTreeClone[indexLv1].children[indexLv2].children.length ===
                  0
                ) {
                  listTreeClone[indexLv1].children.splice(indexLv2, 1);
                }

                if (listTreeClone[indexLv1].children.length === 0) {
                  listTreeClone.splice(indexLv1, 1);
                }
              }
            });
          });
        } else {
          listTreeClone.forEach((itemLv1, indexLv1) => {
            itemLv1.children.forEach((itemLv2, indexLv2) => {
              const index = itemLv2.children.findIndex(
                (itemLv3) =>
                  itemLv3.id === dataProduct[0].children[0].children[0].id
              );
              if (index !== -1) {
                listTreeClone[indexLv1].children[indexLv2].children.splice(
                  index,
                  1
                );

                if (
                  listTreeClone[indexLv1].children[indexLv2].children.length ===
                  0
                ) {
                  listTreeClone[indexLv1].children.splice(indexLv2, 1);
                }

                if (listTreeClone[indexLv1].children.length === 0) {
                  listTreeClone.splice(indexLv1, 1);
                }
              }
            });
          });
        }
      } else {
        // if (listTreeCloneNew[0].children[0].children.length === 1) {
        //   listTreeClone.forEach((itemLv1, indexLv1) => {
        //     itemLv1.children.forEach((itemLv2, indexLv2) => {
        //       const index = itemLv2.children.findIndex(
        //         (itemLv3) =>
        //           itemLv3.id === listTreeCloneNew[0].children[0].children[0].id
        //       );
        //       if (index !== -1) {
        //         listTreeClone[indexLv1].children[indexLv2].children.splice(
        //           index + 1,
        //           1
        //         );
        //       }
        //     });
        //   });
        // } else {
        listTreeClone.forEach((itemLv1, indexLv1) => {
          let isExist = true;
          listTreeCloneNew.forEach((itemNewLv1) => {
            if (itemLv1.id === itemNewLv1.id) {
              isExist = false;
              itemLv1.children.forEach((itemLv2, indexLv2) => {
                let isExist2 = true;
                itemNewLv1.children.forEach((itemNewLv2) => {
                  if (itemLv2.id === itemNewLv2.id) {
                    isExist2 = false;
                    itemLv2.children.forEach((itemLv3) => {
                      let index = itemNewLv2.children.findIndex(
                        (itemCheck) => itemCheck.id === itemLv3.id
                      );
                      if (index === -1) {
                        let indexItemDelete = itemLv2.children.findIndex(
                          (itemCheck) => itemCheck.id === itemLv3.id
                        );
                        listTreeClone[indexLv1].children[
                          indexLv2
                        ].children.splice(indexItemDelete, 1);
                      }
                    });
                  }
                });
                if (isExist2 === true) {
                  const index = itemLv1.children.findIndex(
                    (item) => item.id === itemLv2.id
                  );
                  listTreeClone[indexLv1].children.splice(index, 1);
                }
              });
            }
          });
          if (isExist === true) {
            const index = listTreeClone.findIndex(
              (item) => item.id === itemLv1.id
            );
            listTreeClone.splice(index, 1);
          }
        });
        // }
      }

      state[action.payload.keyRoot][action.payload.initial_listClone] =
        listTreeClone;
    }
    return state;
  },

  ADD_TREE_KEY_CLONE: (state, action) => {
    let listKeyClone = JSON.parse(
      JSON.stringify(
        state[action.payload.keyRoot][action.payload.initial_listClone]
      )
    );

    let checked = action.payload.data.checked;
    let arrKeyCloneNew = [...action.payload.data.arr];

    const listKeyCloneNew = arrKeyCloneNew.filter((item) => {
      const ids = item.split("-");
      if (ids.length === 3) {
        return item;
      }
    });

    if (checked) {
      if (listKeyClone.length === 0) {
        state[action.payload.keyRoot][action.payload.initial_listClone] =
          listKeyCloneNew;
      } else {
        listKeyCloneNew.forEach((itemNew) => {
          const index = listKeyClone.indexOf(itemNew);
          if (index === -1) {
            listKeyClone.push(itemNew);
          }
        });

        state[action.payload.keyRoot][action.payload.initial_listClone] =
          listKeyClone;
      }
    } else {
      listKeyCloneNew.forEach((itemNew) => {
        const index = listKeyClone.indexOf(itemNew);
        if (index !== -1) {
          listKeyClone.splice(index, 1);
        }
      });

      state[action.payload.keyRoot][action.payload.initial_listClone] =
        listKeyClone;
    }

    return state;
  },

  ADD_TREE_CLONE_NOT_DISABLE: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listClone] =
      action.payload.listTreeCloneNew;
    return state;
  },

  SHOW_TREE: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listShow] =
      state[action.payload.keyRoot][action.payload.initial_listClone];
    return state;
  },
  SAVE_TREE: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] =
      state[action.payload.keyRoot][action.payload.initial_listShow];
    /*Add more state save confirm key has selected  */
      state[action.payload.keyRoot][action.payload.initial_listKeyConfirm] =
          action.payload.dataListKeyConfirm;
    if (action.payload.assigned_condition) {
      if (state.treeProduct.keyDefaultProduct === 0) {
        state.treeProduct.keyDefaultProduct = `${state.treeProduct.listConfirm[0].children[0].id}`;
      }
    }
    return state;
  },

  SAVE_TREE_NEW: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] =
      state[action.payload.keyRoot][action.payload.initial_listShow];

    state[action.payload.keyRoot][action.payload.initial_listKeyConfirm] =
      action.payload.dataListKeyConfirm;
    return state;
  },

  CANCEL_TREE: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listClone] = [];
    return state;
  },

  CANCEL_TREE_NEW: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listClone] =
      action.payload.dataListConfirm;
    state[action.payload.keyRoot][action.payload.initial_listShow] =
      action.payload.dataListConfirm;
    state[action.payload.keyRoot][action.payload.initial_listKeyClone] =
      action.payload.dataListKeyConfirm;
    return state;
  },

  UPDATE_TREE_PRIORITY: (state, action) => {
    state[action.payload.keyRoot][action.payload.key][
      action.payload.idxRoot
    ].children[action.payload.idxChildren][action.payload.keyPriority] =
      action.payload.valuePayload;
    return state;
  },

  ASSIGNED_KEY: (state, action) => {
    let keyArr = [];
    state[action.payload.keyRoot][action.payload.keyTree].map((item) => {
      item.children.map((item1) => {
        keyArr.push(item1.id);
      });
    });
    state[action.payload.keyRoot][action.payload.keyListID] = keyArr;
    return state;
  },
  REMOVE_TREE: (state, action) => {
    if (
      state[action.payload.keyRoot][action.payload.key][
        action.payload.keyLocationRoot
      ][action.payload.keyObject].length -
        1 ===
      0
    ) {
      state[action.payload.keyRoot][action.payload.key].splice(
        action.payload.keyLocationRoot,
        1
      );
    } else {
      state[action.payload.keyRoot][action.payload.key][
        action.payload.keyLocationRoot
      ][action.payload.keyObject].splice(action.payload.idx, 1);
    }

    return state;
  },
  UPDATE_TREE: (state, action) => {
    state[action.payload.keyRoot][action.payload.key][
      action.payload.keyLocationRoot
    ][action.payload.keyObject][action.payload.idx][action.payload.keyValue] =
      action.payload.value;
    return state;
  },
  UPDATE_TREE_KEYDEFAULT: (state, action) => {
    state[action.payload.keyRoot][action.payload.key] =
      action.payload.valuePayload;
    return state;
  },

  REMOVE_PRODUCT: (state, action) => {
    const listTreeClone = action.payload.listTreeClone;
    const listTreeShow = action.payload.listTreeShow;
    let listKeyClone = action.payload.listKeyClone;
    const product = action.payload.product;

    const ids = product.tree_id.split("-");

    const [lv1, lv2, lv3] = ids;

    // List tree clone
    let objTreeCloneLv1 = listTreeClone.find((item) => item.id === Number(lv1));

    let objTreeCloneLv2 = objTreeCloneLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeCloneLv3 = objTreeCloneLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeCloneTmp = [];

    if (arrTreeCloneLv3.length > 0) {
      let objTreeCloneLv2New = {
        ...objTreeCloneLv2,
        children: arrTreeCloneLv3,
      };

      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1, objTreeCloneLv2New);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      listTreeCloneTmp.splice(idxObjLv1, 1, {
        ...objTreeCloneLv1,
        children: listTreeCloneLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      if (listTreeCloneLv1ChildTmp.length === 0) {
        listTreeCloneTmp.splice(idxObjLv1, 1);
      } else {
        listTreeCloneTmp.splice(idxObjLv1, 1, {
          ...objTreeCloneLv1,
          children: listTreeCloneLv1ChildTmp,
        });
      }
    }

    //List tree show
    let objTreeShowLv1 = listTreeShow.find((item) => item.id === Number(lv1));

    let objTreeShowLv2 = objTreeShowLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeShowLv3 = objTreeShowLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeShowTmp = [];

    if (arrTreeShowLv3.length > 0) {
      let objTreeShowLv2New = {
        ...objTreeShowLv2,
        children: arrTreeShowLv3,
      };

      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1, objTreeShowLv2New);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      listTreeShowTmp.splice(idxObjLv1, 1, {
        ...objTreeShowLv1,
        children: listTreeShowLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      if (listTreeShowLv1ChildTmp.length === 0) {
        listTreeShowTmp.splice(idxObjLv1, 1);
      } else {
        listTreeShowTmp.splice(idxObjLv1, 1, {
          ...objTreeShowLv1,
          children: listTreeShowLv1ChildTmp,
        });
      }
    }

    const listKeyCloneNew1 = listKeyClone.filter(
      (item) => item !== product.tree_id
    );

    const listKeyCloneNew2 = listKeyCloneNew1.filter(
      (item) => item !== [lv1, lv2].join("-")
    );

    const listKeyCloneNew3 = listKeyCloneNew2.filter((item) => item !== lv1);

    state[action.payload.keyRoot][action.payload.key_listTreeClone] =
      listTreeCloneTmp;
    state[action.payload.keyRoot][action.payload.key_listTreeShow] =
      listTreeShowTmp;
    state[action.payload.keyRoot][action.payload.key_listKeyClone] =
      listKeyCloneNew3;

    return state;
  },

  REMOVE_PRODUCT_CONFIRM: (state, action) => {
    const listTreeClone = action.payload.listTreeClone;
    const listTreeShow = action.payload.listTreeShow;
    const listConfirm = action.payload.listConfirm;
    let listKeyClone = action.payload.listKeyClone;
    let listKeyConfirm = action.payload.listKeyConfirm;
    const product = action.payload.product;

    const ids = product.tree_id.split("-");

    const [lv1, lv2, lv3] = ids;

    // List tree clone
    let objTreeCloneLv1 = listTreeShow.find((item) => item.id === Number(lv1));

    let objTreeCloneLv2 = objTreeCloneLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeCloneLv3 = objTreeCloneLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeCloneTmp = [];

    if (arrTreeCloneLv3.length > 0) {
      let objTreeCloneLv2New = {
        ...objTreeCloneLv2,
        children: arrTreeCloneLv3,
      };

      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1, objTreeCloneLv2New);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      listTreeCloneTmp.splice(idxObjLv1, 1, {
        ...objTreeCloneLv1,
        children: listTreeCloneLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      if (listTreeCloneLv1ChildTmp.length === 0) {
        listTreeCloneTmp.splice(idxObjLv1, 1);
      } else {
        listTreeCloneTmp.splice(idxObjLv1, 1, {
          ...objTreeCloneLv1,
          children: listTreeCloneLv1ChildTmp,
        });
      }
    }

    //List tree show
    let objTreeShowLv1 = listTreeShow.find((item) => item.id === Number(lv1));

    let objTreeShowLv2 = objTreeShowLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeShowLv3 = objTreeShowLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeShowTmp = [];

    if (arrTreeShowLv3.length > 0) {
      let objTreeShowLv2New = {
        ...objTreeShowLv2,
        children: arrTreeShowLv3,
      };

      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1, objTreeShowLv2New);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      listTreeShowTmp.splice(idxObjLv1, 1, {
        ...objTreeShowLv1,
        children: listTreeShowLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      if (listTreeShowLv1ChildTmp.length === 0) {
        listTreeShowTmp.splice(idxObjLv1, 1);
      } else {
        listTreeShowTmp.splice(idxObjLv1, 1, {
          ...objTreeShowLv1,
          children: listTreeShowLv1ChildTmp,
        });
      }
    }

    //List tree confirm
    let objListConfirmLv1 = listConfirm.find((item) => item.id === Number(lv1));

    let objListConfirmLv2 = objListConfirmLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrListConfirmLv3 = objListConfirmLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listConfirmTmp = [];

    if (arrListConfirmLv3.length > 0) {
      let objListConfirmLv2New = {
        ...objListConfirmLv2,
        children: arrListConfirmLv3,
      };

      let idxObjLv2 = objListConfirmLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listConfirmLv1ChildTmp = [...objListConfirmLv1.children];

      listConfirmLv1ChildTmp.splice(idxObjLv2, 1, objListConfirmLv2New);

      let idxObjLv1 = listConfirm.findIndex((item) => item.id === Number(lv1));

      listConfirmTmp = [...listConfirm];

      listConfirmTmp.splice(idxObjLv1, 1, {
        ...objListConfirmLv1,
        children: listConfirmLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objListConfirmLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listConfirmLv1ChildTmp = [...objListConfirmLv1.children];

      listConfirmLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listConfirm.findIndex((item) => item.id === Number(lv1));

      listConfirmTmp = [...listConfirm];

      if (listConfirmLv1ChildTmp.length === 0) {
        listConfirmTmp.splice(idxObjLv1, 1);
      } else {
        listConfirmTmp.splice(idxObjLv1, 1, {
          ...objListConfirmLv1,
          children: listConfirmLv1ChildTmp,
        });
      }
    }

    const listKeyCloneNew1 = listKeyClone.filter(
      (item) => item !== product.tree_id
    );

    const listKeyCloneNew2 = listKeyCloneNew1.filter(
      (item) => item !== [lv1, lv2].join("-")
    );

    const listKeyCloneNew3 = listKeyCloneNew2.filter((item) => item !== lv1);

    const listKeyConfirmNew1 = listKeyConfirm.filter(
      (item) => item !== product.tree_id
    );
    const idConfirms = product.tree_id.split("-");

    const [lv1Confirm, lv2Confirm, lv3Confirm] = idConfirms;

    const listKeyConfirmNew2 = listKeyConfirmNew1.filter(
      (item) => item !== [lv1Confirm, lv2Confirm].join("-")
    );

    const listKeyConfirmNew3 = listKeyConfirmNew2.filter(
      (item) => item !== lv1Confirm
    );

    state[action.payload.keyRoot][action.payload.key_listTreeClone] =
      listTreeCloneTmp;
    state[action.payload.keyRoot][action.payload.key_listTreeShow] =
      listTreeShowTmp;
    state[action.payload.keyRoot][action.payload.key_listConfirm] =
      listConfirmTmp;
    state[action.payload.keyRoot][action.payload.key_listKeyClone] =
      listKeyCloneNew3;
    state[action.payload.keyRoot][action.payload.key_listKeyConfirm] =
      listKeyConfirmNew3;

    return state;
  },

  ASSIGN_LIST_KEY_CLONE: (state, action) => {
    state[action.payload.keyRoot][action.payload.key] = action.payload.value;
    return state;
  },

  SAVE_TREE_GROUP: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] =
      state[action.payload.keyRoot][action.payload.initial_listShow];

    state[action.payload.keyRoot].listArrListTreeConfirm.push({
      treeProduct: [
        ...state[action.payload.keyRoot][action.payload.initial_listShow],
      ],
    });

    if (state.eventGift.event_design.key_type_product === 1) {
      state.treeProductEventGiftGroup.listArrDisabled = [
        ...state.treeProductEventGiftGroup.listArrDisabled,
        ...state.treeProductEventGiftGroup.listKeyClone,
      ];
      state.treeProductEventGiftGroup.listArrDisabled = [
        ...new Set(state.treeProductEventGiftGroup.listArrDisabled),
      ];

      state.treeProductEventGiftGroup.listKeyCloneDisabled = [
        ...new Set(state.treeProductEventGiftGroup.listArrDisabled),
      ];

      state.treeProductEventGiftGroup.listTreeClone = [];
      state.treeProductEventGiftGroup.listTreeShow = [];
      state.treeProductEventGiftGroup.listConfirm = [];
      state.treeProductEventGiftGroup.listKeyClone = [];
      state.treeProductEventGiftGroup.listKeyConfirm = [];
    } else {
      state.treeProductEventGiftGroup2.listArrDisabled = [
        ...state.treeProductEventGiftGroup2.listArrDisabled,
        ...state.treeProductEventGiftGroup2.listKeyClone,
      ];
      state.treeProductEventGiftGroup2.listArrDisabled = [
        ...new Set(state.treeProductEventGiftGroup2.listArrDisabled),
      ];

      state.treeProductEventGiftGroup2.listKeyCloneDisabled = [
        ...new Set(state.treeProductEventGiftGroup2.listArrDisabled),
      ];

      state.treeProductEventGiftGroup2.listTreeClone = [];
      state.treeProductEventGiftGroup2.listTreeShow = [];
      state.treeProductEventGiftGroup2.listConfirm = [];
      state.treeProductEventGiftGroup2.listKeyClone = [];
      state.treeProductEventGiftGroup2.listKeyConfirm = [];
    }

    return state;
  },

  SAVE_TREE_GROUP_OVER: (state, action) => {
    // RESTART CÂY LẠI

    if (state.eventGift.event_design.key_type_product === 1) {
      let listConfirm = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup.listConfirm)
      );
      let listArrDisabled = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup.listArrDisabled)
      );

      let listKeyClone = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup.listKeyClone)
      );

      let treeIdConfirm;
      listConfirm.map((itemLv1) => {
        itemLv1.children.forEach((itemLv2) => {
          itemLv2.children.forEach((itemLv3) => {
            if ((treeIdConfirm = itemLv3.tree_id)) {
              const index = listArrDisabled.indexOf(treeIdConfirm);
              listArrDisabled.splice(index, 1);
            }
          });
        });
      });

      state.treeProductEventGiftGroup.listArrDisabled = [
        ...listArrDisabled,
        ...listKeyClone,
      ];

      state.treeProductEventGiftGroup.listArrDisabled = [
        ...new Set(state.treeProductEventGiftGroup.listArrDisabled),
      ];

      state.treeProductEventGiftGroup.listKeyCloneDisabled = [
        ...new Set(state.treeProductEventGiftGroup.listArrDisabled),
      ];

      state[action.payload.keyRoot][action.payload.initial_listConfirm] =
        state[action.payload.keyRoot][action.payload.initial_listShow];
      state[action.payload.keyRoot].listArrListTreeConfirm[
        state.eventGift.id_count_save_item
      ].treeProduct = [
        ...state[action.payload.keyRoot][action.payload.initial_listShow],
      ];

      state.treeProductEventGiftGroup.listTreeClone = [];
      state.treeProductEventGiftGroup.listTreeShow = [];
      state.treeProductEventGiftGroup.listConfirm = [];
      state.treeProductEventGiftGroup.listKeyClone = [];
      state.treeProductEventGiftGroup.listKeyConfirm = [];
    } else {
      let listConfirm = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup2.listConfirm)
      );
      let listArrDisabled = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup2.listArrDisabled)
      );

      let listKeyClone = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup2.listKeyClone)
      );

      let treeIdConfirm;
      listConfirm.map((itemLv1) => {
        itemLv1.children.forEach((itemLv2) => {
          itemLv2.children.forEach((itemLv3) => {
            treeIdConfirm = itemLv3.tree_id;
          });
        });
      });

      const index = listArrDisabled.indexOf(treeIdConfirm);

      listArrDisabled.splice(index, 1);

      state.treeProductEventGiftGroup2.listArrDisabled = [
        ...listArrDisabled,
        ...listKeyClone,
      ];

      state.treeProductEventGiftGroup2.listArrDisabled = [
        ...new Set(state.treeProductEventGiftGroup2.listArrDisabled),
      ];

      state.treeProductEventGiftGroup2.listKeyCloneDisabled = [
        ...new Set(state.treeProductEventGiftGroup2.listArrDisabled),
      ];

      state[action.payload.keyRoot][action.payload.initial_listConfirm] =
        state[action.payload.keyRoot][action.payload.initial_listShow];
      state[action.payload.keyRoot].listArrListTreeConfirm[
        state.eventGift.id_count_save_item
      ].treeProduct = [
        ...state[action.payload.keyRoot][action.payload.initial_listShow],
      ];

      state.treeProductEventGiftGroup2.listTreeClone = [];
      state.treeProductEventGiftGroup2.listTreeShow = [];
      state.treeProductEventGiftGroup2.listConfirm = [];
      state.treeProductEventGiftGroup2.listKeyClone = [];
      state.treeProductEventGiftGroup2.listKeyConfirm = [];
    }

    return state;
  },

  SAVE_TREE_GROUP_NEW: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] =
      state[action.payload.keyRoot][action.payload.initial_listShow];

    state[action.payload.keyRoot].listArrListTreeConfirm.push({
      treeProduct: [
        ...state[action.payload.keyRoot][action.payload.initial_listShow],
      ],
    });

    state[action.payload.keyRoot].listArrDisabled = [
      ...state[action.payload.keyRoot].listArrDisabled,
      ...state[action.payload.keyRoot].listKeyClone,
    ];
    state[action.payload.keyRoot].listArrDisabled = [
      ...new Set(state[action.payload.keyRoot].listArrDisabled),
    ];

    state[action.payload.keyRoot].listKeyCloneDisabled = [
      ...new Set(state[action.payload.keyRoot].listArrDisabled),
    ];

    state[action.payload.keyRoot].listTreeClone = [];
    state[action.payload.keyRoot].listTreeShow = [];
    state[action.payload.keyRoot].listConfirm = [];
    state[action.payload.keyRoot].listKeyClone = [];
    state[action.payload.keyRoot].listKeyConfirm = [];

    return state;
  },

  SAVE_TREE_GROUP_OVER_NEW: (state, action) => {
    // RESTART CÂY LẠI

    let listConfirm = JSON.parse(
      JSON.stringify(state[action.payload.keyRoot].listConfirm)
    );
    let listArrDisabled = JSON.parse(
      JSON.stringify(state[action.payload.keyRoot].listArrDisabled)
    );

    let listKeyClone = JSON.parse(
      JSON.stringify(state[action.payload.keyRoot].listKeyClone)
    );

    let treeIdConfirm;
    listConfirm.map((itemLv1) => {
      itemLv1.children.forEach((itemLv2) => {
        itemLv2.children.forEach((itemLv3) => {
          if ((treeIdConfirm = itemLv3.tree_id)) {
            const index = listArrDisabled.indexOf(treeIdConfirm);
            listArrDisabled.splice(index, 1);
          }
        });
      });
    });

    state[action.payload.keyRoot].listArrDisabled = [
      ...listArrDisabled,
      ...listKeyClone,
    ];

    state[action.payload.keyRoot].listArrDisabled = [
      ...new Set(state[action.payload.keyRoot].listArrDisabled),
    ];

    state[action.payload.keyRoot].listKeyCloneDisabled = [
      ...new Set(state[action.payload.keyRoot].listArrDisabled),
    ];

    state[action.payload.keyRoot][action.payload.initial_listConfirm] =
      state[action.payload.keyRoot][action.payload.initial_listShow];
    state[action.payload.keyRoot].listArrListTreeConfirm[
      state.eventGift.id_count_save_item
    ].treeProduct = [
      ...state[action.payload.keyRoot][action.payload.initial_listShow],
    ];

    state[action.payload.keyRoot].listTreeClone = [];
    state[action.payload.keyRoot].listTreeShow = [];
    state[action.payload.keyRoot].listConfirm = [];
    state[action.payload.keyRoot].listKeyClone = [];
    state[action.payload.keyRoot].listKeyConfirm = [];

    return state;
  },

  CLOSE_MODAL_TREE: (state, action) => {
    if (state.eventGift.event_design.key_type_product === 1) {
      state.treeProductEventGiftGroup.listTreeClone = [];
      state.treeProductEventGiftGroup.listTreeShow = [];
      state.treeProductEventGiftGroup.listConfirm = [];
      state.treeProductEventGiftGroup.listKeyClone = [];
      state.treeProductEventGiftGroup.listKeyCloneDisabled =
        state.treeProductEventGiftGroup.listArrDisabled;
    } else {
      state.treeProductEventGiftGroup2.listTreeClone = [];
      state.treeProductEventGiftGroup2.listTreeShow = [];
      state.treeProductEventGiftGroup2.listConfirm = [];
      state.treeProductEventGiftGroup2.listKeyClone = [];
      state.treeProductEventGiftGroup2.listKeyCloneDisabled =
        state.treeProductEventGiftGroup2.listArrDisabled;
    }
  },

  CLOSE_MODAL_TREE_NEW: (state, action) => {
    state[action.payload.keyRoot].listTreeClone = [];
    state[action.payload.keyRoot].listTreeShow = [];
    state[action.payload.keyRoot].listConfirm = [];
    state[action.payload.keyRoot].listKeyClone = [];
    state[action.payload.keyRoot].listKeyCloneDisabled =
      state[action.payload.keyRoot].listArrDisabled;
  },

  ASSIGNED_TREE_CLICK_GROUP: (state, action) => {
    const listKeyNew = [];
    action.payload[0].children.forEach((itemLv2) => {
      itemLv2.children.forEach((itemLv3) => {
        listKeyNew.push(itemLv3.tree_id);
      });
    });

    let listArrDisabledTmp = JSON.parse(
      JSON.stringify(state.treeProductEventGiftGroup.listArrDisabled)
    );

    if (state.eventGift.event_design.key_type_product === 1) {
      listKeyNew.forEach((item, index) => {
        const idx = listArrDisabledTmp.indexOf(item);
        state.treeProductEventGiftGroup.listKeyCloneDisabled.splice(idx, 1);
        listArrDisabledTmp.splice(idx, 1);
      });
      state.treeProductEventGiftGroup.listTreeClone = [...action.payload];
      state.treeProductEventGiftGroup.listTreeShow = [...action.payload];
      state.treeProductEventGiftGroup.listConfirm = [...action.payload];
      state.treeProductEventGiftGroup.listKeyClone = [...listKeyNew];
    } else {
      listKeyNew.forEach((item, index) => {
        const idx =
          state.treeProductEventGiftGroup2.listArrDisabled.indexOf(item);
        state.treeProductEventGiftGroup2.listKeyCloneDisabled.splice(idx, 1);
      });

      // state.treeProductEventGiftGroup2.listKeyCloneDisabled = [...listKeyNew];
      state.treeProductEventGiftGroup2.listTreeClone = [...action.payload];
      state.treeProductEventGiftGroup2.listTreeShow = [...action.payload];
      state.treeProductEventGiftGroup2.listConfirm = [...action.payload];
      state.treeProductEventGiftGroup2.listKeyClone = [...listKeyNew];

      // state.treeProductEventGiftGroup2.listArrDisabled = [
      //   ...state.treeProductEventGiftGroup2.listArrDisabled,
      // ];
    }
  },

  REMOVE_PRODUCT_GROUP: (state, action) => {
    let index = action.payload.idxTreeItem;

    if (state.eventGift.event_design.key_type_product === 1) {
      const listKeyNew = [];
      action.payload.treeProduct[0].children.forEach((itemLv2) => {
        itemLv2.children.forEach((itemLv3) => {
          listKeyNew.push(itemLv3.tree_id);
        });
      });

      let listArrDisabledTmp = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup.listArrDisabled)
      );

      listKeyNew.forEach((item, index) => {
        const idx = listArrDisabledTmp.indexOf(item);
        state.treeProductEventGiftGroup.listKeyCloneDisabled.splice(idx, 1);
        listArrDisabledTmp.splice(idx, 1);
      });

      let listArrListTreeConfirmClone = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup.listArrListTreeConfirm)
      );

      let listArrDisabledClone = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup.listArrDisabled)
      );

      listArrListTreeConfirmClone.splice(index, 1);
      listKeyNew.forEach((item) => {
        const idx = listArrDisabledClone.indexOf(item);
        listArrDisabledClone.splice(idx, 1);
      });

      state.treeProductEventGiftGroup.listArrListTreeConfirm =
        listArrListTreeConfirmClone;
      state.treeProductEventGiftGroup.listArrDisabled = listArrDisabledClone;
      state.treeProductEventGiftGroup.listKeyCloneDisabled =
        listArrDisabledClone;
    } else {
      let listArrListTreeConfirmClone2 = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup2.listArrListTreeConfirm)
      );

      let listArrDisabledClone = JSON.parse(
        JSON.stringify(state.treeProductEventGiftGroup2.listArrDisabled)
      );

      listArrListTreeConfirmClone2.splice(index, 1);
      listArrDisabledClone.splice(index, 1);

      state.treeProductEventGiftGroup2.listArrListTreeConfirm =
        listArrListTreeConfirmClone2;
      state.treeProductEventGiftGroup2.listArrDisabled = listArrDisabledClone;
      state.treeProductEventGiftGroup2.listKeyCloneDisabled =
        listArrDisabledClone;
    }

    let rewardClone = JSON.parse(
      JSON.stringify(state.eventGift.event_design.reward)
    );

    let rewardQuantityClone = JSON.parse(
      JSON.stringify(state.eventGift.event_design.rewardQuantity)
    );

    let rewardNew = rewardClone.map((item) => {
      item.splice(index, 1);
      return item;
    });

    let rewardQuantityNew = rewardQuantityClone.map((item) => {
      item.splice(index, 1);
      return item;
    });

    state.eventGift.event_design.reward = rewardNew;
    state.eventGift.event_design.rewardQuantity = rewardQuantityNew;
    return state;
  },

  REMOVE_PRODUCT_GROUP_IMMEDIATE: (state, action) => {
    let index = action.payload.idxTreeItem;

    const listKeyNew = [];
    action.payload.treeProduct[0].children.forEach((itemLv2) => {
      itemLv2.children.forEach((itemLv3) => {
        listKeyNew.push(itemLv3.tree_id);
      });
    });

    let listArrDisabledTmp = JSON.parse(
      JSON.stringify(
        state.treeProductImmediateConditionAttachGroup.listArrDisabled
      )
    );

    listKeyNew.forEach((item, index) => {
      const idx = listArrDisabledTmp.indexOf(item);
      state.treeProductImmediateConditionAttachGroup.listKeyCloneDisabled.splice(
        idx,
        1
      );
      listArrDisabledTmp.splice(idx, 1);
    });

    let listArrListTreeConfirmClone = JSON.parse(
      JSON.stringify(
        state.treeProductImmediateConditionAttachGroup.listArrListTreeConfirm
      )
    );

    let listArrDisabledClone = JSON.parse(
      JSON.stringify(
        state.treeProductImmediateConditionAttachGroup.listArrDisabled
      )
    );

    listArrListTreeConfirmClone.splice(index, 1);
    listKeyNew.forEach((item) => {
      const idx = listArrDisabledClone.indexOf(item);
      listArrDisabledClone.splice(idx, 1);
    });

    state.treeProductImmediateConditionAttachGroup.listArrListTreeConfirm =
      listArrListTreeConfirmClone;
    state.treeProductImmediateConditionAttachGroup.listArrDisabled =
      listArrDisabledClone;
    state.treeProductImmediateConditionAttachGroup.listKeyCloneDisabled =
      listArrDisabledClone;

    let rewardClone = JSON.parse(
      JSON.stringify(state.eventImmediateNew.arr_condition_attach_group)
    );

    rewardClone.splice(index, 1);

    state.eventImmediateNew.arr_condition_attach_group = rewardClone;
    return state;
  },

  REMOVE_PRODUCT_BY_OBJECT: (state, action) => {
    const listTreeClone = action.payload.listTreeClone;
    const listTreeShow = action.payload.listTreeShow;
    let listKeyClone = action.payload.listKeyClone;
    const product = action.payload.product;

    const ids = product.tree_id.split("-");

    const [lv1, lv2, lv3] = ids;

    // List tree clone
    let objTreeCloneLv1 = listTreeClone.find((item) => item.id === Number(lv1));

    let objTreeCloneLv2 = objTreeCloneLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeCloneLv3 = objTreeCloneLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeCloneTmp = [];

    if (arrTreeCloneLv3.length > 0) {
      let objTreeCloneLv2New = {
        ...objTreeCloneLv2,
        children: arrTreeCloneLv3,
      };

      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1, objTreeCloneLv2New);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      listTreeCloneTmp.splice(idxObjLv1, 1, {
        ...objTreeCloneLv1,
        children: listTreeCloneLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      if (listTreeCloneLv1ChildTmp.length === 0) {
        listTreeCloneTmp.splice(idxObjLv1, 1);
      } else {
        listTreeCloneTmp.splice(idxObjLv1, 1, {
          ...objTreeCloneLv1,
          children: listTreeCloneLv1ChildTmp,
        });
      }
    }

    //List tree show
    let objTreeShowLv1 = listTreeShow.find((item) => item.id === Number(lv1));

    let objTreeShowLv2 = objTreeShowLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeShowLv3 = objTreeShowLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeShowTmp = [];

    if (arrTreeShowLv3.length > 0) {
      let objTreeShowLv2New = {
        ...objTreeShowLv2,
        children: arrTreeShowLv3,
      };

      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1, objTreeShowLv2New);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      listTreeShowTmp.splice(idxObjLv1, 1, {
        ...objTreeShowLv1,
        children: listTreeShowLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      if (listTreeShowLv1ChildTmp.length === 0) {
        listTreeShowTmp.splice(idxObjLv1, 1);
      } else {
        listTreeShowTmp.splice(idxObjLv1, 1, {
          ...objTreeShowLv1,
          children: listTreeShowLv1ChildTmp,
        });
      }
    }

    const listKeyCloneNew1 = listKeyClone.filter(
      (item) => item !== product.tree_id
    );

    const listKeyCloneNew2 = listKeyCloneNew1.filter(
      (item) => item !== [lv1, lv2].join("-")
    );

    const listKeyCloneNew3 = listKeyCloneNew2.filter((item) => item !== lv1);

    state[action.payload.keyRoot][action.payload.key_listTreeClone] =
      listTreeCloneTmp;
    state[action.payload.keyRoot][action.payload.key_listTreeShow] =
      listTreeShowTmp;
    state[action.payload.keyRoot][action.payload.key_listKeyClone] =
      listKeyCloneNew3;

    return state;
  },

  REMOVE_PRODUCT_BY_OBJECT_CONFIRM: (state, action) => {
    const listTreeClone = action.payload.listTreeClone;
    const listTreeShow = action.payload.listTreeShow;
    const arrConfirm = action.payload.arrConfirm;
    const listConfirm = action.payload.listConfirm;
    let listKeyClone = action.payload.listKeyClone;
    let listKeyConfirm = action.payload.listKeyConfirm;
    const product = action.payload.product;

    const ids = product.tree_id.split("-");

    const [lv1, lv2, lv3] = ids;

    // List tree clone
    let objTreeCloneLv1 = listTreeClone.find((item) => item.id === Number(lv1));

    let objTreeCloneLv2 = objTreeCloneLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeCloneLv3 = objTreeCloneLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeCloneTmp = [];

    if (arrTreeCloneLv3.length > 0) {
      let objTreeCloneLv2New = {
        ...objTreeCloneLv2,
        children: arrTreeCloneLv3,
      };

      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1, objTreeCloneLv2New);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      listTreeCloneTmp.splice(idxObjLv1, 1, {
        ...objTreeCloneLv1,
        children: listTreeCloneLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeCloneLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeCloneLv1ChildTmp = [...objTreeCloneLv1.children];

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeClone.findIndex(
        (item) => item.id === Number(lv1)
      );

      listTreeCloneTmp = [...listTreeClone];

      if (listTreeCloneLv1ChildTmp.length === 0) {
        listTreeCloneTmp.splice(idxObjLv1, 1);
      } else {
        listTreeCloneTmp.splice(idxObjLv1, 1, {
          ...objTreeCloneLv1,
          children: listTreeCloneLv1ChildTmp,
        });
      }
    }

    //List tree show
    let objTreeShowLv1 = listTreeShow.find((item) => item.id === Number(lv1));

    let objTreeShowLv2 = objTreeShowLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrTreeShowLv3 = objTreeShowLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listTreeShowTmp = [];

    if (arrTreeShowLv3.length > 0) {
      let objTreeShowLv2New = {
        ...objTreeShowLv2,
        children: arrTreeShowLv3,
      };

      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1, objTreeShowLv2New);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      listTreeShowTmp.splice(idxObjLv1, 1, {
        ...objTreeShowLv1,
        children: listTreeShowLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objTreeShowLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listTreeShowLv1ChildTmp = [...objTreeShowLv1.children];

      listTreeShowLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listTreeShow.findIndex((item) => item.id === Number(lv1));

      listTreeShowTmp = [...listTreeShow];

      if (listTreeShowLv1ChildTmp.length === 0) {
        listTreeShowTmp.splice(idxObjLv1, 1);
      } else {
        listTreeShowTmp.splice(idxObjLv1, 1, {
          ...objTreeShowLv1,
          children: listTreeShowLv1ChildTmp,
        });
      }
    }

    //List tree confirm
    let objListConfirmLv1 = listConfirm.find((item) => item.id === Number(lv1));

    let objListConfirmLv2 = objListConfirmLv1.children.find(
      (item) => item.id === Number(lv2)
    );

    let arrListConfirmLv3 = objListConfirmLv2.children.filter(
      (item) => item.id !== Number(lv3)
    );

    let listConfirmTmp = [];

    if (arrListConfirmLv3.length > 0) {
      let objListConfirmLv2New = {
        ...objListConfirmLv2,
        children: arrListConfirmLv3,
      };

      let idxObjLv2 = objListConfirmLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listConfirmLv1ChildTmp = [...objListConfirmLv1.children];

      listConfirmLv1ChildTmp.splice(idxObjLv2, 1, objListConfirmLv2New);

      let idxObjLv1 = listConfirm.findIndex((item) => item.id === Number(lv1));

      listConfirmTmp = [...listConfirm];

      listConfirmTmp.splice(idxObjLv1, 1, {
        ...objListConfirmLv1,
        children: listConfirmLv1ChildTmp,
      });
    } else {
      let idxObjLv2 = objListConfirmLv1.children.findIndex(
        (item) => item.id === Number(lv2)
      );

      let listConfirmLv1ChildTmp = [...objListConfirmLv1.children];

      listConfirmLv1ChildTmp.splice(idxObjLv2, 1);

      let idxObjLv1 = listConfirm.findIndex((item) => item.id === Number(lv1));

      listConfirmTmp = [...listConfirm];

      if (listConfirmLv1ChildTmp.length === 0) {
        listConfirmTmp.splice(idxObjLv1, 1);
      } else {
        listConfirmTmp.splice(idxObjLv1, 1, {
          ...objListConfirmLv1,
          children: listConfirmLv1ChildTmp,
        });
      }
    }

    const arrConfirmNew = arrConfirm.filter((item) => item.id !== product.id);

    const arrConfirmNew1 = arrConfirmNew.map((item, index) => {
      let stt = index + 1;
      return {
        ...item,
        stt: stt,
      };
    });

    const listKeyCloneNew1 = listKeyClone.filter(
      (item) => item !== product.tree_id
    );

    const listKeyCloneNew2 = listKeyCloneNew1.filter(
      (item) => item !== [lv1, lv2].join("-")
    );

    const listKeyCloneNew3 = listKeyCloneNew2.filter((item) => item !== lv1);

    const listKeyConfirmNew1 = listKeyConfirm.filter(
      (item) => item !== product.tree_id
    );
    const idConfirms = product.tree_id.split("-");

    const [lv1Confirm, lv2Confirm, lv3Confirm] = idConfirms;

    const listKeyConfirmNew2 = listKeyConfirmNew1.filter(
      (item) => item !== [lv1Confirm, lv2Confirm].join("-")
    );

    const listKeyConfirmNew3 = listKeyConfirmNew2.filter(
      (item) => item !== lv1Confirm
    );

    state[action.payload.keyRoot][action.payload.key_listTreeClone] =
      listTreeCloneTmp;
    state[action.payload.keyRoot][action.payload.key_listTreeShow] =
      listTreeShowTmp;
    state[action.payload.keyRoot][action.payload.key_listConfirm] =
      listConfirmTmp;
    state.productsByObject[action.payload.key_arrConfirm] = arrConfirmNew1;
    state[action.payload.keyRoot][action.payload.key_listKeyClone] =
      listKeyCloneNew3;
    state[action.payload.keyRoot][action.payload.key_listKeyConfirm] =
      listKeyConfirmNew3;

    return state;
  },

  ASSIGNED_TREE_CLICK_GROUP: (state, aciton) => {
    let listKeyNew = [];
    aciton.payload.forEach((item) => {
      item.children.forEach((item1) => {
        listKeyNew.push(item1.id);
      });
    });
    // aciton.payload[0].children.map((item) => item.id);
    listKeyNew.forEach((item, index) => {
      const idx = state.treeProductComboGroup.listArrDisabled.indexOf(item);
      state.treeProductComboGroup.listArrDisabled.splice(idx, 1);
    });

    state.treeProductComboGroup.listKeyCloneDisabled = [...listKeyNew];

    state.treeProductComboGroup.listTreeClone = [...aciton.payload];
    state.treeProductComboGroup.listTreeShow = [...aciton.payload];
    state.treeProductComboGroup.listKeyClone = [...listKeyNew];
    state.treeProductComboGroup.listArrDisabled = [
      ...state.treeProductComboGroup.listArrDisabled,
    ];
  },

  ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE_CONDITION_ATTACH: (state, action) => {
    const listKeyNew = [];
    action.payload[0].children.forEach((itemLv2) => {
      itemLv2.children.forEach((itemLv3) => {
        listKeyNew.push(itemLv3.tree_id);
      });
    });

    let listArrDisabledTmp = JSON.parse(
      JSON.stringify(
        state.treeProductImmediateConditionAttachGroup.listArrDisabled
      )
    );

    listKeyNew.forEach((item, index) => {
      const idx = listArrDisabledTmp.indexOf(item);
      state.treeProductImmediateConditionAttachGroup.listKeyCloneDisabled.splice(
        idx,
        1
      );
      listArrDisabledTmp.splice(idx, 1);
    });
    state.treeProductImmediateConditionAttachGroup.listTreeClone = [
      ...action.payload,
    ];
    state.treeProductImmediateConditionAttachGroup.listTreeShow = [
      ...action.payload,
    ];
    state.treeProductImmediateConditionAttachGroup.listConfirm = [
      ...action.payload,
    ];
    state.treeProductImmediateConditionAttachGroup.listKeyClone = [
      ...listKeyNew,
    ];
  },

  ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE: (state, aciton) => {
    const listKeyNew = [];
    aciton.payload.forEach((itemLv1) => {
      itemLv1.children.forEach((itemLv2) => {
        itemLv2.children.forEach((itemLv3) => {
          listKeyNew.push(itemLv3.tree_id);
        });
      });
    });
    // listKeyNew.forEach((item, index) => {
    //   const idx = state.treeProductComboGroup.listArrDisabled.indexOf(item);
    //   state.treeProductImmediateGiftGroup.listArrDisabled.splice(idx, 1);
    // });

    // state.treeProductImmediateGiftGroup.listKeyCloneDisabled = [...listKeyNew];

    state.treeProductImmediateGiftGroup.listTreeClone = [...aciton.payload];
    state.treeProductImmediateGiftGroup.listTreeShow = [...aciton.payload];
    state.treeProductImmediateGiftGroup.listKeyClone = [...listKeyNew];
    // state.treeProductImmediateGiftGroup.listArrDisabled = [
    //   ...state.treeProductImmediateGiftGroup.listArrDisabled,
    // ];
  },

  ASSIGNED_TREE_CLICK_GROUP_CONDITION: (state, aciton) => {
    const listKeyNew = aciton.payload[0].children.map((item) => item.id);
    listKeyNew.forEach((item, index) => {
      const idx = state.treeProductConditionGroup.listArrDisabled.indexOf(item);
      state.treeProductConditionGroup.listArrDisabled.splice(idx, 1);
    });

    state.treeProductConditionGroup.listKeyCloneDisabled = [...listKeyNew];

    state.treeProductConditionGroup.listTreeClone = [...aciton.payload];
    state.treeProductConditionGroup.listTreeShow = [...aciton.payload];
    state.treeProductConditionGroup.listKeyClone = [...listKeyNew];
    state.treeProductConditionGroup.listArrDisabled = [
      ...state.treeProductConditionGroup.listArrDisabled,
    ];
  },

  SAVE_TREE_GIFT_GROUP_IMMEDIATE_OVER: (state, action) => {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] =
      state[action.payload.keyRoot][action.payload.initial_listShow];
    state[action.payload.keyRoot].listArrListTreeConfirm[
      state.eventImmediateNew.id_count_save_item_condition_attach
    ].treeProduct = [
      ...state[action.payload.keyRoot][action.payload.initial_listShow],
    ];

    // const idx = state.treeProductImmediateGiftGroup.listKeyClone.indexOf(
    //   state[action.payload.keyRoot].listArrListTreeConfirm[
    //     state.eventImmediateNew.id_count_save_item
    //   ].keyDefaultProduct
    // );
    // if (idx === -1) {
    //   state[action.payload.keyRoot].listArrListTreeConfirm[
    //     state.eventImmediateNew.id_count_save_item
    //   ].keyDefaultProduct = state.treeProductImmediateGiftGroup.listKeyClone[0];
    // }
    // state.treeProductComboGroup.listArrDisabled = [
    //   ...state.treeProductComboGroup.listArrDisabled,
    //   ...state.treeProductComboGroup.listKeyClone,
    // ];
    // state.treeProductComboGroup.listArrDisabled = [
    //   ...new Set(state.treeProductComboGroup.listArrDisabled),
    // ];

    // state.treeProductComboGroup.listKeyCloneDisabled = [
    //   ...state.treeProductComboGroup.listKeyClone,
    // ];

    // RESTART CÂY LẠI
    state[action.payload.keyRoot].listTreeClone = [];
    state[action.payload.keyRoot].listTreeShow = [];
    state[action.payload.keyRoot].listConfirm = [];
    state[action.payload.keyRoot].listKeyClone = [];

    return state;
  },
};

export {
  initialTreeProduct,
  initialTreeProductEventGiftGroup,
  initialTreeProductEventGiftGroup2,
  reducerTreeProduct,
  initialTreeProductImmediate,
  initialTreeProductPresentImmediate,
  initialTreeProductImmediateGiftGroup,
  initialTreeProductPresentImmediateDiscount,
  initialTreeProductComboGroup,
  initialTreeProductConditionGroup,
  initialTreeProductConditionPage,
  initialTreeProductImmediateConditionAttachGroup,
  initialTreeProductImmediateConditionAttach,
};
