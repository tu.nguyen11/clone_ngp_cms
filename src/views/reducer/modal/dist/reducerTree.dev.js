"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initialTreeProductPresentImmediate = exports.initialTreeProductImmediate = exports.reducerTreeProduct = exports.initialTreeProductEventGiftGroup2 = exports.initialTreeProductEventGiftGroup = exports.initialTreeProduct = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var initialTreeProduct = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listKeyConfirm: [],
  keyDefaultProduct: 0
};
exports.initialTreeProduct = initialTreeProduct;
var initialTreeProductImmediate = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: []
};
exports.initialTreeProductImmediate = initialTreeProductImmediate;
var initialTreeProductPresentImmediate = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: []
};
exports.initialTreeProductPresentImmediate = initialTreeProductPresentImmediate;
var initialTreeProductEventGiftGroup = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listKeyConfirm: [],
  listArrListTreeConfirm: []
};
exports.initialTreeProductEventGiftGroup = initialTreeProductEventGiftGroup;
var initialTreeProductEventGiftGroup2 = {
  listTreeClone: [],
  listTreeShow: [],
  listConfirm: [],
  listKeyClone: [],
  listKeyConfirm: [],
  listArrListTreeConfirm: [],
  listArrDisabled: []
};
exports.initialTreeProductEventGiftGroup2 = initialTreeProductEventGiftGroup2;
var reducerTreeProduct = {
  ADD_TREE: function ADD_TREE(state, action) {
    var listTreeClone = JSON.parse(JSON.stringify(state[action.payload.keyRoot][action.payload.initial_listClone]));
    var checked = action.payload.data.checked;

    var listTreeCloneNew = _toConsumableArray(action.payload.data.tree);

    if (checked) {
      if (listTreeClone.length === 0) {
        state[action.payload.keyRoot][action.payload.initial_listClone] = listTreeCloneNew;
      } else {
        listTreeCloneNew.forEach(function (itemNewLv1) {
          var isExist = false;
          listTreeClone.forEach(function (itemLv1, indexLv1) {
            if (itemLv1.id === itemNewLv1.id) {
              isExist = true;
              itemNewLv1.children.forEach(function (itemNewLv2) {
                var isExist2 = false;
                itemLv1.children.forEach(function (itemLv2, indexLv2) {
                  if (itemLv2.id === itemNewLv2.id) {
                    isExist2 = true;
                    itemNewLv2.children.forEach(function (itemNewLv3) {
                      var index = itemLv2.children.findIndex(function (itemCheck) {
                        return itemCheck.id === itemNewLv3.id;
                      });

                      if (index === -1) {
                        listTreeClone[indexLv1].children[indexLv2].children.push(itemNewLv3);
                      }
                    });
                  }
                });

                if (isExist2 === false) {
                  listTreeClone[indexLv1].children.push(itemNewLv2);
                }
              });
            }
          });

          if (isExist === false) {
            listTreeClone.push(itemNewLv1);
          }
        });
        state[action.payload.keyRoot][action.payload.initial_listClone] = listTreeClone;
      }
    } else {
      if (listTreeCloneNew.length === 0) {
        var dataProduct = JSON.parse(JSON.stringify(state.dataProduct));

        if (dataProduct.length > 1) {
          var listConfirm = JSON.parse(JSON.stringify(state.treeProductEventGiftGroup2.listConfirm));

          if (listConfirm.length === 0) {
            state[action.payload.keyRoot][action.payload.initial_listClone] = [];
            return;
          }

          listTreeClone.forEach(function (itemLv1, indexLv1) {
            itemLv1.children.forEach(function (itemLv2, indexLv2) {
              var index = itemLv2.children.findIndex(function (itemLv3) {
                return itemLv3.id === listConfirm[0].children[0].children[0].id;
              });

              if (index !== -1) {
                listTreeClone[indexLv1].children[indexLv2].children.splice(index, 1);
              }
            });
          });
        } else {
          listTreeClone.forEach(function (itemLv1, indexLv1) {
            itemLv1.children.forEach(function (itemLv2, indexLv2) {
              var index = itemLv2.children.findIndex(function (itemLv3) {
                return itemLv3.id === dataProduct[0].children[0].children[0].id;
              });

              if (index !== -1) {
                listTreeClone[indexLv1].children[indexLv2].children.splice(index, 1);
              }
            });
          });
        }
      } else {
        // if (listTreeCloneNew[0].children[0].children.length === 1) {
        //   listTreeClone.forEach((itemLv1, indexLv1) => {
        //     itemLv1.children.forEach((itemLv2, indexLv2) => {
        //       const index = itemLv2.children.findIndex(
        //         (itemLv3) =>
        //           itemLv3.id === listTreeCloneNew[0].children[0].children[0].id
        //       );
        //       if (index !== -1) {
        //         listTreeClone[indexLv1].children[indexLv2].children.splice(
        //           index + 1,
        //           1
        //         );
        //       }
        //     });
        //   });
        // } else {
        listTreeClone.forEach(function (itemLv1, indexLv1) {
          var isExist = true;
          listTreeCloneNew.forEach(function (itemNewLv1) {
            if (itemLv1.id === itemNewLv1.id) {
              isExist = false;
              itemLv1.children.forEach(function (itemLv2, indexLv2) {
                var isExist2 = true;
                itemNewLv1.children.forEach(function (itemNewLv2) {
                  if (itemLv2.id === itemNewLv2.id) {
                    isExist2 = false;
                    itemLv2.children.forEach(function (itemLv3) {
                      var index = itemNewLv2.children.findIndex(function (itemCheck) {
                        return itemCheck.id === itemLv3.id;
                      });

                      if (index === -1) {
                        var indexItemDelete = itemLv2.children.findIndex(function (itemCheck) {
                          return itemCheck.id === itemLv3.id;
                        });
                        listTreeClone[indexLv1].children[indexLv2].children.splice(indexItemDelete, 1);
                      }
                    });
                  }
                });

                if (isExist2 === true) {
                  var index = itemLv1.children.findIndex(function (item) {
                    return item.id === itemLv2.id;
                  });
                  listTreeClone[indexLv1].children.splice(index, 1);
                }
              });
            }
          });

          if (isExist === true) {
            var index = listTreeClone.findIndex(function (item) {
              return item.id === itemLv1.id;
            });
            listTreeClone.splice(index, 1);
          }
        }); // }
      }

      state[action.payload.keyRoot][action.payload.initial_listClone] = listTreeClone;
    }

    return state;
  },
  ADD_TREE_KEY_CLONE: function ADD_TREE_KEY_CLONE(state, action) {
    var listKeyClone = JSON.parse(JSON.stringify(state[action.payload.keyRoot][action.payload.initial_listClone]));
    var checked = action.payload.data.checked;

    var arrKeyCloneNew = _toConsumableArray(action.payload.data.arr);

    var listKeyCloneNew = arrKeyCloneNew.filter(function (item) {
      var ids = item.split("-");

      if (ids.length === 3) {
        return item;
      }
    });

    if (checked) {
      if (listKeyClone.length === 0) {
        state[action.payload.keyRoot][action.payload.initial_listClone] = listKeyCloneNew;
      } else {
        listKeyCloneNew.forEach(function (itemNew) {
          var index = listKeyClone.indexOf(itemNew);

          if (index === -1) {
            listKeyClone.push(itemNew);
          }
        });
        state[action.payload.keyRoot][action.payload.initial_listClone] = listKeyClone;
      }
    } else {
      listKeyCloneNew.forEach(function (itemNew) {
        var index = listKeyClone.indexOf(itemNew);

        if (index !== -1) {
          listKeyClone.splice(index, 1);
        }
      });
      state[action.payload.keyRoot][action.payload.initial_listClone] = listKeyClone;
    }

    return state;
  },
  SHOW_TREE: function SHOW_TREE(state, action) {
    state[action.payload.keyRoot][action.payload.initial_listShow] = state[action.payload.keyRoot][action.payload.initial_listClone];
    return state;
  },
  SAVE_TREE: function SAVE_TREE(state, action) {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] = state[action.payload.keyRoot][action.payload.initial_listShow];

    if (action.payload.assigned_condition) {
      if (state.treeProduct.keyDefaultProduct === 0) {
        state.treeProduct.keyDefaultProduct = "".concat(state.treeProduct.listConfirm[0].children[0].id);
      }
    }

    return state;
  },
  SAVE_TREE_NEW: function SAVE_TREE_NEW(state, action) {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] = state[action.payload.keyRoot][action.payload.initial_listShow];
    state[action.payload.keyRoot][action.payload.initial_listKeyConfirm] = action.payload.dataListKeyConfirm;
    return state;
  },
  CANCEL_TREE: function CANCEL_TREE(state, action) {
    state[action.payload.keyRoot][action.payload.initial_listClone] = [];
    return state;
  },
  CANCEL_TREE_NEW: function CANCEL_TREE_NEW(state, action) {
    state[action.payload.keyRoot][action.payload.initial_listClone] = action.payload.dataListConfirm;
    state[action.payload.keyRoot][action.payload.initial_listShow] = action.payload.dataListConfirm;
    state[action.payload.keyRoot][action.payload.initial_listKeyClone] = action.payload.dataListKeyConfirm;
    return state;
  },
  UPDATE_TREE_PRIORITY: function UPDATE_TREE_PRIORITY(state, action) {
    state[action.payload.keyRoot][action.payload.key][action.payload.idxRoot].children[action.payload.idxChildren][action.payload.keyPriority] = action.payload.valuePayload;
    return state;
  },
  ASSIGNED_KEY: function ASSIGNED_KEY(state, action) {
    var keyArr = [];
    state[action.payload.keyRoot][action.payload.keyTree].map(function (item) {
      item.children.map(function (item1) {
        keyArr.push(item1.id);
      });
    });
    state[action.payload.keyRoot][action.payload.keyListID] = keyArr;
    return state;
  },
  REMOVE_TREE: function REMOVE_TREE(state, action) {
    if (state[action.payload.keyRoot][action.payload.key][action.payload.keyLocationRoot][action.payload.keyObject].length - 1 === 0) {
      state[action.payload.keyRoot][action.payload.key].splice(action.payload.keyLocationRoot, 1);
    } else {
      state[action.payload.keyRoot][action.payload.key][action.payload.keyLocationRoot][action.payload.keyObject].splice(action.payload.idx, 1);
    }

    return state;
  },
  UPDATE_TREE: function UPDATE_TREE(state, action) {
    state[action.payload.keyRoot][action.payload.key][action.payload.keyLocationRoot][action.payload.keyObject][action.payload.idx][action.payload.keyValue] = action.payload.value;
    return state;
  },
  UPDATE_TREE_KEYDEFAULT: function UPDATE_TREE_KEYDEFAULT(state, action) {
    state[action.payload.keyRoot][action.payload.key] = action.payload.valuePayload;
    return state;
  },
  REMOVE_PRODUCT: function REMOVE_PRODUCT(state, action) {
    var listTreeClone = action.payload.listTreeClone;
    var listTreeShow = action.payload.listTreeShow;
    var listKeyClone = action.payload.listKeyClone;
    var product = action.payload.product;
    var ids = product.tree_id.split("-");

    var _ids = _slicedToArray(ids, 3),
        lv1 = _ids[0],
        lv2 = _ids[1],
        lv3 = _ids[2]; // List tree clone


    var objTreeCloneLv1 = listTreeClone.find(function (item) {
      return item.id === Number(lv1);
    });
    var objTreeCloneLv2 = objTreeCloneLv1.children.find(function (item) {
      return item.id === Number(lv2);
    });
    var arrTreeCloneLv3 = objTreeCloneLv2.children.filter(function (item) {
      return item.id !== Number(lv3);
    });
    var listTreeCloneTmp = [];

    if (arrTreeCloneLv3.length > 0) {
      var objTreeCloneLv2New = _objectSpread({}, objTreeCloneLv2, {
        children: arrTreeCloneLv3
      });

      var idxObjLv2 = objTreeCloneLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var listTreeCloneLv1ChildTmp = _toConsumableArray(objTreeCloneLv1.children);

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1, objTreeCloneLv2New);
      var idxObjLv1 = listTreeClone.findIndex(function (item) {
        return item.id === Number(lv1);
      });
      listTreeCloneTmp = _toConsumableArray(listTreeClone);
      listTreeCloneTmp.splice(idxObjLv1, 1, _objectSpread({}, objTreeCloneLv1, {
        children: listTreeCloneLv1ChildTmp
      }));
    } else {
      var _idxObjLv = objTreeCloneLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var _listTreeCloneLv1ChildTmp = _toConsumableArray(objTreeCloneLv1.children);

      _listTreeCloneLv1ChildTmp.splice(_idxObjLv, 1);

      var _idxObjLv2 = listTreeClone.findIndex(function (item) {
        return item.id === Number(lv1);
      });

      listTreeCloneTmp = _toConsumableArray(listTreeClone);

      if (_listTreeCloneLv1ChildTmp.length === 0) {
        listTreeCloneTmp.splice(_idxObjLv2, 1);
      } else {
        listTreeCloneTmp.splice(_idxObjLv2, 1, _objectSpread({}, objTreeCloneLv1, {
          children: _listTreeCloneLv1ChildTmp
        }));
      }
    } //List tree show


    var objTreeShowLv1 = listTreeShow.find(function (item) {
      return item.id === Number(lv1);
    });
    var objTreeShowLv2 = objTreeShowLv1.children.find(function (item) {
      return item.id === Number(lv2);
    });
    var arrTreeShowLv3 = objTreeShowLv2.children.filter(function (item) {
      return item.id !== Number(lv3);
    });
    var listTreeShowTmp = [];

    if (arrTreeShowLv3.length > 0) {
      var objTreeShowLv2New = _objectSpread({}, objTreeShowLv2, {
        children: arrTreeShowLv3
      });

      var _idxObjLv3 = objTreeShowLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var listTreeShowLv1ChildTmp = _toConsumableArray(objTreeShowLv1.children);

      listTreeShowLv1ChildTmp.splice(_idxObjLv3, 1, objTreeShowLv2New);

      var _idxObjLv4 = listTreeShow.findIndex(function (item) {
        return item.id === Number(lv1);
      });

      listTreeShowTmp = _toConsumableArray(listTreeShow);
      listTreeShowTmp.splice(_idxObjLv4, 1, _objectSpread({}, objTreeShowLv1, {
        children: listTreeShowLv1ChildTmp
      }));
    } else {
      var _idxObjLv5 = objTreeShowLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var _listTreeShowLv1ChildTmp = _toConsumableArray(objTreeShowLv1.children);

      _listTreeShowLv1ChildTmp.splice(_idxObjLv5, 1);

      var _idxObjLv6 = listTreeShow.findIndex(function (item) {
        return item.id === Number(lv1);
      });

      listTreeShowTmp = _toConsumableArray(listTreeShow);

      if (_listTreeShowLv1ChildTmp.length === 0) {
        listTreeShowTmp.splice(_idxObjLv6, 1);
      } else {
        listTreeShowTmp.splice(_idxObjLv6, 1, _objectSpread({}, objTreeShowLv1, {
          children: _listTreeShowLv1ChildTmp
        }));
      }
    }

    var listKeyCloneNew1 = listKeyClone.filter(function (item) {
      return item !== product.tree_id;
    });
    var listKeyCloneNew2 = listKeyCloneNew1.filter(function (item) {
      return item !== [lv1, lv2].join("-");
    });
    var listKeyCloneNew3 = listKeyCloneNew2.filter(function (item) {
      return item !== lv1;
    });
    state[action.payload.keyRoot][action.payload.key_listTreeClone] = listTreeCloneTmp;
    state[action.payload.keyRoot][action.payload.key_listTreeShow] = listTreeShowTmp;
    state[action.payload.keyRoot][action.payload.key_listKeyClone] = listKeyCloneNew3;
    return state;
  },
  REMOVE_PRODUCT_CONFIRM: function REMOVE_PRODUCT_CONFIRM(state, action) {
    var listTreeClone = action.payload.listTreeClone;
    var listTreeShow = action.payload.listTreeShow;
    var listConfirm = action.payload.listConfirm;
    var listKeyClone = action.payload.listKeyClone;
    var listKeyConfirm = action.payload.listKeyConfirm;
    var product = action.payload.product;
    var ids = product.tree_id.split("-");

    var _ids2 = _slicedToArray(ids, 3),
        lv1 = _ids2[0],
        lv2 = _ids2[1],
        lv3 = _ids2[2]; // List tree clone


    var objTreeCloneLv1 = listTreeClone.find(function (item) {
      return item.id === Number(lv1);
    });
    var objTreeCloneLv2 = objTreeCloneLv1.children.find(function (item) {
      return item.id === Number(lv2);
    });
    var arrTreeCloneLv3 = objTreeCloneLv2.children.filter(function (item) {
      return item.id !== Number(lv3);
    });
    var listTreeCloneTmp = [];

    if (arrTreeCloneLv3.length > 0) {
      var objTreeCloneLv2New = _objectSpread({}, objTreeCloneLv2, {
        children: arrTreeCloneLv3
      });

      var idxObjLv2 = objTreeCloneLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var listTreeCloneLv1ChildTmp = _toConsumableArray(objTreeCloneLv1.children);

      listTreeCloneLv1ChildTmp.splice(idxObjLv2, 1, objTreeCloneLv2New);
      var idxObjLv1 = listTreeClone.findIndex(function (item) {
        return item.id === Number(lv1);
      });
      listTreeCloneTmp = _toConsumableArray(listTreeClone);
      listTreeCloneTmp.splice(idxObjLv1, 1, _objectSpread({}, objTreeCloneLv1, {
        children: listTreeCloneLv1ChildTmp
      }));
    } else {
      var _idxObjLv7 = objTreeCloneLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var _listTreeCloneLv1ChildTmp2 = _toConsumableArray(objTreeCloneLv1.children);

      _listTreeCloneLv1ChildTmp2.splice(_idxObjLv7, 1);

      var _idxObjLv8 = listTreeClone.findIndex(function (item) {
        return item.id === Number(lv1);
      });

      listTreeCloneTmp = _toConsumableArray(listTreeClone);

      if (_listTreeCloneLv1ChildTmp2.length === 0) {
        listTreeCloneTmp.splice(_idxObjLv8, 1);
      } else {
        listTreeCloneTmp.splice(_idxObjLv8, 1, _objectSpread({}, objTreeCloneLv1, {
          children: _listTreeCloneLv1ChildTmp2
        }));
      }
    } //List tree show


    var objTreeShowLv1 = listTreeShow.find(function (item) {
      return item.id === Number(lv1);
    });
    var objTreeShowLv2 = objTreeShowLv1.children.find(function (item) {
      return item.id === Number(lv2);
    });
    var arrTreeShowLv3 = objTreeShowLv2.children.filter(function (item) {
      return item.id !== Number(lv3);
    });
    var listTreeShowTmp = [];

    if (arrTreeShowLv3.length > 0) {
      var objTreeShowLv2New = _objectSpread({}, objTreeShowLv2, {
        children: arrTreeShowLv3
      });

      var _idxObjLv9 = objTreeShowLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var listTreeShowLv1ChildTmp = _toConsumableArray(objTreeShowLv1.children);

      listTreeShowLv1ChildTmp.splice(_idxObjLv9, 1, objTreeShowLv2New);

      var _idxObjLv10 = listTreeShow.findIndex(function (item) {
        return item.id === Number(lv1);
      });

      listTreeShowTmp = _toConsumableArray(listTreeShow);
      listTreeShowTmp.splice(_idxObjLv10, 1, _objectSpread({}, objTreeShowLv1, {
        children: listTreeShowLv1ChildTmp
      }));
    } else {
      var _idxObjLv11 = objTreeShowLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var _listTreeShowLv1ChildTmp2 = _toConsumableArray(objTreeShowLv1.children);

      _listTreeShowLv1ChildTmp2.splice(_idxObjLv11, 1);

      var _idxObjLv12 = listTreeShow.findIndex(function (item) {
        return item.id === Number(lv1);
      });

      listTreeShowTmp = _toConsumableArray(listTreeShow);

      if (_listTreeShowLv1ChildTmp2.length === 0) {
        listTreeShowTmp.splice(_idxObjLv12, 1);
      } else {
        listTreeShowTmp.splice(_idxObjLv12, 1, _objectSpread({}, objTreeShowLv1, {
          children: _listTreeShowLv1ChildTmp2
        }));
      }
    } //List tree confirm


    var objListConfirmLv1 = listConfirm.find(function (item) {
      return item.id === Number(lv1);
    });
    var objListConfirmLv2 = objListConfirmLv1.children.find(function (item) {
      return item.id === Number(lv2);
    });
    var arrListConfirmLv3 = objListConfirmLv2.children.filter(function (item) {
      return item.id !== Number(lv3);
    });
    var listConfirmTmp = [];

    if (arrListConfirmLv3.length > 0) {
      var objListConfirmLv2New = _objectSpread({}, objListConfirmLv2, {
        children: arrListConfirmLv3
      });

      var _idxObjLv13 = objListConfirmLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var listConfirmLv1ChildTmp = _toConsumableArray(objListConfirmLv1.children);

      listConfirmLv1ChildTmp.splice(_idxObjLv13, 1, objListConfirmLv2New);

      var _idxObjLv14 = listConfirm.findIndex(function (item) {
        return item.id === Number(lv1);
      });

      listConfirmTmp = _toConsumableArray(listConfirm);
      listConfirmTmp.splice(_idxObjLv14, 1, _objectSpread({}, objListConfirmLv1, {
        children: listConfirmLv1ChildTmp
      }));
    } else {
      var _idxObjLv15 = objListConfirmLv1.children.findIndex(function (item) {
        return item.id === Number(lv2);
      });

      var _listConfirmLv1ChildTmp = _toConsumableArray(objListConfirmLv1.children);

      _listConfirmLv1ChildTmp.splice(_idxObjLv15, 1);

      var _idxObjLv16 = listConfirm.findIndex(function (item) {
        return item.id === Number(lv1);
      });

      listConfirmTmp = _toConsumableArray(listConfirm);

      if (_listConfirmLv1ChildTmp.length === 0) {
        listConfirmTmp.splice(_idxObjLv16, 1);
      } else {
        listConfirmTmp.splice(_idxObjLv16, 1, _objectSpread({}, objListConfirmLv1, {
          children: _listConfirmLv1ChildTmp
        }));
      }
    }

    var listKeyCloneNew1 = listKeyClone.filter(function (item) {
      return item !== product.tree_id;
    });
    var listKeyCloneNew2 = listKeyCloneNew1.filter(function (item) {
      return item !== [lv1, lv2].join("-");
    });
    var listKeyCloneNew3 = listKeyCloneNew2.filter(function (item) {
      return item !== lv1;
    });
    var listKeyConfirmNew1 = listKeyConfirm.filter(function (item) {
      return item !== product.tree_id;
    });
    var idConfirms = product.tree_id.split("-");

    var _idConfirms = _slicedToArray(idConfirms, 3),
        lv1Confirm = _idConfirms[0],
        lv2Confirm = _idConfirms[1],
        lv3Confirm = _idConfirms[2];

    var listKeyConfirmNew2 = listKeyConfirmNew1.filter(function (item) {
      return item !== [lv1Confirm, lv2Confirm].join("-");
    });
    var listKeyConfirmNew3 = listKeyConfirmNew2.filter(function (item) {
      return item !== lv1Confirm;
    });
    state[action.payload.keyRoot][action.payload.key_listTreeClone] = listTreeCloneTmp;
    state[action.payload.keyRoot][action.payload.key_listTreeShow] = listTreeShowTmp;
    state[action.payload.keyRoot][action.payload.key_listConfirm] = listConfirmTmp;
    state[action.payload.keyRoot][action.payload.key_listKeyClone] = listKeyCloneNew3;
    state[action.payload.keyRoot][action.payload.key_listKeyConfirm] = listKeyConfirmNew3;
    return state;
  },
  ASSIGN_LIST_KEY_CLONE: function ASSIGN_LIST_KEY_CLONE(state, action) {
    state[action.payload.keyRoot][action.payload.key] = action.payload.value;
    return state;
  },
  SAVE_TREE_GROUP: function SAVE_TREE_GROUP(state, action) {
    state[action.payload.keyRoot][action.payload.initial_listConfirm] = state[action.payload.keyRoot][action.payload.initial_listShow];
    state[action.payload.keyRoot].listArrListTreeConfirm.push({
      treeProduct: _toConsumableArray(state[action.payload.keyRoot][action.payload.initial_listShow])
    });

    if (state.eventGift.event_design.key_type_product === 1) {
      state.treeProductEventGiftGroup.listTreeClone = [];
      state.treeProductEventGiftGroup.listTreeShow = [];
      state.treeProductEventGiftGroup.listConfirm = [];
      state.treeProductEventGiftGroup.listKeyClone = [];
      state.treeProductEventGiftGroup.listKeyConfirm = [];
    } else {
      state.treeProductEventGiftGroup2.listArrDisabled = [].concat(_toConsumableArray(state.treeProductEventGiftGroup2.listArrDisabled), _toConsumableArray(state.treeProductEventGiftGroup2.listKeyClone));
      state.treeProductEventGiftGroup2.listArrDisabled = _toConsumableArray(new Set(state.treeProductEventGiftGroup2.listArrDisabled));
      state.treeProductEventGiftGroup2.listKeyCloneDisabled = _toConsumableArray(new Set(state.treeProductEventGiftGroup2.listArrDisabled));
      state.treeProductEventGiftGroup2.listTreeClone = [];
      state.treeProductEventGiftGroup2.listTreeShow = [];
      state.treeProductEventGiftGroup2.listConfirm = [];
      state.treeProductEventGiftGroup2.listKeyClone = [];
      state.treeProductEventGiftGroup2.listKeyConfirm = [];
    }

    return state;
  },
  SAVE_TREE_GROUP_OVER: function SAVE_TREE_GROUP_OVER(state, action) {
    // RESTART CÂY LẠI
    if (state.eventGift.event_design.key_type_product === 1) {
      state[action.payload.keyRoot][action.payload.initial_listConfirm] = state[action.payload.keyRoot][action.payload.initial_listShow];
      state[action.payload.keyRoot].listArrListTreeConfirm[state.eventGift.id_count_save_item].treeProduct = _toConsumableArray(state[action.payload.keyRoot][action.payload.initial_listShow]);
      state.treeProductEventGiftGroup.listTreeClone = [];
      state.treeProductEventGiftGroup.listTreeShow = [];
      state.treeProductEventGiftGroup.listConfirm = [];
      state.treeProductEventGiftGroup.listKeyClone = [];
      state.treeProductEventGiftGroup.listKeyConfirm = [];
    } else {
      var listConfirm = JSON.parse(JSON.stringify(state.treeProductEventGiftGroup2.listConfirm));
      var listArrDisabled = JSON.parse(JSON.stringify(state.treeProductEventGiftGroup2.listArrDisabled));
      var listKeyClone = JSON.parse(JSON.stringify(state.treeProductEventGiftGroup2.listKeyClone));
      var treeIdConfirm;
      listConfirm.map(function (itemLv1) {
        itemLv1.children.forEach(function (itemLv2) {
          itemLv2.children.forEach(function (itemLv3) {
            treeIdConfirm = itemLv3.tree_id;
          });
        });
      });
      var index = listArrDisabled.indexOf(treeIdConfirm);
      listArrDisabled.splice(index, 1);
      state.treeProductEventGiftGroup2.listArrDisabled = [].concat(_toConsumableArray(listArrDisabled), _toConsumableArray(listKeyClone));
      state.treeProductEventGiftGroup2.listArrDisabled = _toConsumableArray(new Set(state.treeProductEventGiftGroup2.listArrDisabled));
      state.treeProductEventGiftGroup2.listKeyCloneDisabled = _toConsumableArray(new Set(state.treeProductEventGiftGroup2.listArrDisabled));
      state[action.payload.keyRoot][action.payload.initial_listConfirm] = state[action.payload.keyRoot][action.payload.initial_listShow];
      state[action.payload.keyRoot].listArrListTreeConfirm[state.eventGift.id_count_save_item].treeProduct = _toConsumableArray(state[action.payload.keyRoot][action.payload.initial_listShow]);
      state.treeProductEventGiftGroup2.listTreeClone = [];
      state.treeProductEventGiftGroup2.listTreeShow = [];
      state.treeProductEventGiftGroup2.listConfirm = [];
      state.treeProductEventGiftGroup2.listKeyClone = [];
      state.treeProductEventGiftGroup2.listKeyConfirm = [];
    }

    return state;
  },
  CLOSE_MODAL_TREE: function CLOSE_MODAL_TREE(state, action) {
    if (state.eventGift.event_design.key_type_product === 1) {
      state.treeProductEventGiftGroup.listTreeClone = [];
      state.treeProductEventGiftGroup.listTreeShow = [];
      state.treeProductEventGiftGroup.listConfirm = [];
      state.treeProductEventGiftGroup.listKeyClone = [];
    } else {
      state.treeProductEventGiftGroup2.listTreeClone = [];
      state.treeProductEventGiftGroup2.listTreeShow = [];
      state.treeProductEventGiftGroup2.listConfirm = [];
      state.treeProductEventGiftGroup2.listKeyClone = [];
      state.treeProductEventGiftGroup2.listKeyCloneDisabled = state.treeProductEventGiftGroup2.listArrDisabled;
    }
  },
  ASSIGNED_TREE_CLICK_GROUP: function ASSIGNED_TREE_CLICK_GROUP(state, action) {
    var listKeyNew = [];
    action.payload[0].children.forEach(function (itemLv2) {
      itemLv2.children.forEach(function (itemLv3) {
        listKeyNew.push(itemLv3.tree_id);
      });
    });

    if (state.eventGift.event_design.key_type_product === 1) {
      state.treeProductEventGiftGroup.listTreeClone = _toConsumableArray(action.payload);
      state.treeProductEventGiftGroup.listTreeShow = _toConsumableArray(action.payload);
      state.treeProductEventGiftGroup.listKeyClone = [].concat(listKeyNew);
    } else {
      listKeyNew.forEach(function (item, index) {
        var idx = state.treeProductEventGiftGroup2.listArrDisabled.indexOf(item);
        state.treeProductEventGiftGroup2.listKeyCloneDisabled.splice(idx, 1);
      }); // state.treeProductEventGiftGroup2.listKeyCloneDisabled = [...listKeyNew];

      state.treeProductEventGiftGroup2.listTreeClone = _toConsumableArray(action.payload);
      state.treeProductEventGiftGroup2.listTreeShow = _toConsumableArray(action.payload);
      state.treeProductEventGiftGroup2.listConfirm = _toConsumableArray(action.payload);
      state.treeProductEventGiftGroup2.listKeyClone = [].concat(listKeyNew); // state.treeProductEventGiftGroup2.listArrDisabled = [
      //   ...state.treeProductEventGiftGroup2.listArrDisabled,
      // ];
    }
  },
  REMOVE_PRODUCT_GROUP: function REMOVE_PRODUCT_GROUP(state, action) {
    var index = action.payload;

    if (state.eventGift.event_design.key_type_product === 1) {
      var listArrListTreeConfirmClone = JSON.parse(JSON.stringify(state.treeProductEventGiftGroup.listArrListTreeConfirm));
      listArrListTreeConfirmClone.splice(index, 1);
      state.treeProductEventGiftGroup.listArrListTreeConfirm = listArrListTreeConfirmClone;
    } else {
      var listArrListTreeConfirmClone2 = JSON.parse(JSON.stringify(state.treeProductEventGiftGroup2.listArrListTreeConfirm));
      var listArrDisabledClone = JSON.parse(JSON.stringify(state.treeProductEventGiftGroup2.listArrDisabled));
      listArrListTreeConfirmClone2.splice(index, 1);
      listArrDisabledClone.splice(index, 1);
      state.treeProductEventGiftGroup2.listArrListTreeConfirm = listArrListTreeConfirmClone2;
      state.treeProductEventGiftGroup2.listArrDisabled = listArrDisabledClone;
      state.treeProductEventGiftGroup2.listKeyCloneDisabled = listArrDisabledClone;
    }

    var rewardClone = JSON.parse(JSON.stringify(state.eventGift.event_design.reward));
    var rewardQuantityClone = JSON.parse(JSON.stringify(state.eventGift.event_design.rewardQuantity));
    var rewardNew = rewardClone.map(function (item) {
      item.splice(index, 1);
      return item;
    });
    var rewardQuantityNew = rewardQuantityClone.map(function (item) {
      item.splice(index, 1);
      return item;
    });
    state.eventGift.event_design.reward = rewardNew;
    state.eventGift.event_design.rewardQuantity = rewardQuantityNew;
    return state;
  }
};
exports.reducerTreeProduct = reducerTreeProduct;