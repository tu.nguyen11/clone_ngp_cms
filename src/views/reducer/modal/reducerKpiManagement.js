const initListTargetKpi = {
    listTarget: [],
    listTargetClone: [],
    listTargetShow: [],
    listTargetApprove: [],
    countTagrgetShow: { },
    countTagrgetApprove: { }
}

const reducerKpiTarget = {
    ASSIGN_LIST_TARGET_KPI: (state, action) => {
        state[action.payload.keyRoot][action.payload.key] = action.payload.value
    },
    ADD_LIST_CLONE_CHOOSE: (state, action) => {
        let check = state[action.payload.keyRoot][action.payload.key].find(item => item.id == action.payload.value.id)
        if (!check) {
            state[action.payload.keyRoot][action.payload.key] = [...state[action.payload.keyRoot][action.payload.key], action.payload.value]
        } else {
            state[action.payload.keyRoot][action.payload.key] = state[action.payload.keyRoot][action.payload.key].filter(item => item.id !== action.payload.value.id)
        }
    },
    ADD_LIST_SHOW_CHOOSE: (state, action) => {
        state[action.payload.keyRoot][action.payload.key] = action.payload.value
    },
    EDIT_COUNT: (state, action) => {
        const objOld =
            state[action.payload.keyRoot][action.payload.countTagrgetShow][
            action.payload.idProduct
            ];

        if (objOld) {
            state[action.payload.keyRoot][action.payload.countTagrgetShow][
                action.payload.idProduct
            ] = { ...objOld, ...action.payload.valueUpdate };
            state[action.payload.keyRoot][action.payload.countTagrgetApprove][
                action.payload.idProduct
            ] = { ...objOld, ...action.payload.valueUpdate };
        } else {
            state[action.payload.keyRoot][action.payload.countTagrgetShow][
                action.payload.idProduct
            ] = action.payload.valueUpdate;
            state[action.payload.keyRoot][action.payload.countTagrgetApprove][
                action.payload.idProduct
            ] = action.payload.valueUpdate;
        }

        return state;
    },
    SAVE_LIST_APPROVE: (state, action) => {
        state[action.payload.keyRoot][action.payload.key] = action.payload.value
    },
    CANCEL_LIST_KPI_MODAL: (state, action) => {
        state[action.payload.keyRoot][action.payload.keyShow] = action.payload.value
        state[action.payload.keyRoot][action.payload.keyTargetClone] = action.payload.value
    },
    REMOVE_ITEM_SHOW: (state, action) => {
        state[action.payload.keyRoot][action.payload.key] = state[action.payload.keyRoot][action.payload.key].filter(item => item.id !== action.payload.id)
        state[action.payload.keyRoot][action.payload.keyClone] = state[action.payload.keyRoot][action.payload.key].filter(item => item.id !== action.payload.id)
    },
    REMOVE_ITEM_APPROVE: (state, action) => {
        state[action.payload.keyRoot][action.payload.keyApprove] = state[action.payload.keyRoot][action.payload.keyApprove].filter(item => item.id !== action.payload.id)
        state[action.payload.keyRoot][action.payload.keyShow] = state[action.payload.keyRoot][action.payload.keyApprove]
        state[action.payload.keyRoot][action.payload.keyTargetClone] = state[action.payload.keyRoot][action.payload.keyApprove]
    },
    ASSIGNED_DATA_TARGET_KPI_EDIT: (state, action) => {
        let dataListTarget = action.payload.value.map((item, index) => {
            return {
                "name": item.name,
                "description": item.description,
                "id": item.target_id
            }
        })
        let countTagrgetShow = { };
        action.payload.value.forEach((item) => {
            countTagrgetShow[item.target_id] = {
                quantity: item.target,
            };
        });
        state[action.payload.keyRoot][action.payload.keyApprove] = dataListTarget
        state[action.payload.keyRoot][action.payload.keyShow] = dataListTarget
        state[action.payload.keyRoot][action.payload.keyTargetClone] = dataListTarget
        state[action.payload.keyRoot][action.payload.keyCountTagrgetApprove] = countTagrgetShow
        state[action.payload.keyRoot][action.payload.keyCountTagrgetShow] = countTagrgetShow
    },
}
export { initListTargetKpi, reducerKpiTarget }
