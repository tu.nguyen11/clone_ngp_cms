const initialModalS1 = {
  listS1AddClone: [],
  listS1Add: [],
  listS1Show: [],
  listS1: [],
  listS1Clone: [],
  listS1Before: []
}

const initialModalS2 = {
  listS2AddClone: [],
  listS2Add: [],
  listS2Show: [],
  listS2: [],
  listS2Clone: [],
  listS2Before: []
}

const initialBusiness = {
  listBusinessAdd: [],
  listBusinessShow: [],
  listBusiness: [],
  listBusinessConfirm: [],

  loading: false
}

const reducerModal = {
  ADD_MODAL: (state, action) => {
    let arrActive = state[action.payload.keyInitial_root][
      action.payload.keyInitial_Add
    ].map(item => item.id)
    if (action.payload.status) {
      if (arrActive.indexOf(action.payload.data.id) === -1) {
        let obj = {
          ...action.payload.data
        }
        state[action.payload.keyInitial_root][
          action.payload.keyInitial_AddClone
        ].push(obj)
      }
    } else {
      let arrActiveClone = state[action.payload.keyInitial_root][
        action.payload.keyInitial_AddClone
      ].map(item => item.id)
      let idx = arrActiveClone.indexOf(action.payload.data.id)
      state[action.payload.keyInitial_root][
        action.payload.keyInitial_AddClone
      ].splice(idx, 1)
      state[action.payload.keyInitial_root][
        action.payload.keyInitial_ListS1Add
      ].splice(idx, 1)
    }
    return state
  },

  REMOVE_MODAL: (state, action) => {
    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listShow
    ].splice(action.payload.idx, 1)
    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listAddClone
    ].splice(action.payload.idx, 1)
    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listS1Add
    ].splice(action.payload.idx, 1)
    return state
  },
  // RESET MODAL S1
  RESET_MODAL: (state, action) => {
    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listShow
    ] = []

    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listAddClone
    ] = []

    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listAdd
    ] = []

    let listS1Tmp = JSON.parse(
      JSON.stringify(state[action.payload.keyInitial_root].listS1Clone)
    )

    let listS1New = listS1Tmp.map(item => {
      item.checked = false
      return item
    })

    state[action.payload.keyInitial_root][
      action.payload.keyInitial_list
    ] = listS1New

    return state
  },

  UPDATE_ACTIVELISTAGENCY_MODAL: (state, action) => {
    let arrActive = state[action.payload.keyInitial_root][
      action.payload.keyInitial_list
    ].map(item => item.id)
    let idx = arrActive.indexOf(action.payload.id)
    if (idx === -1) {
      return state
    }
    state[action.payload.keyInitial_root][action.payload.keyInitial_list][
      idx
    ].checked = action.payload.status
    return state
  },

  UPDATE_ACTIVELIST_MODAL: (state, action) => {
    let arrActive = state[action.payload.keyInitial_root][
      action.payload.keyInitial_list
    ].map(item => item.id)
    let idx = arrActive.indexOf(action.payload.id)
    state[action.payload.keyInitial_root][action.payload.keyInitial_list][
      idx
    ].checked = action.payload.status
    return state
  },

  SAVE_MODAL: (state, action) => {
    state[action.payload.keyInitial_root][action.payload.keyInitial_listShow] =
      state[action.payload.keyInitial_root][
        action.payload.keyInitial_listAddClone
      ]

    return state
  },

  CONFIRM_MODAL: (state, action) => {
    state[action.payload.keyInitial_root][action.payload.keyInitial_Add] =
      state[action.payload.keyInitial_root][action.payload.keyInitial_listShow]

    return state
  },

  ARRAY_MODAL: (state, action) => {
    state.listS2Add = action.payload
    state.listS2Show = action.payload
    return state
  },

  REST_MODAL: (state, action) => {
    state.listS2AddClone = []
    state.listS2Add = []
    state.listS2Show = []
    return state
  },

  DEAFUAT_MODAL: (state, action) => {
    let arrActive = state[action.payload.keyInitial_root][
      action.payload.keyInitial_listShow
    ].map(item => item.id)
    action.payload.data.map(item => {
      if (arrActive.indexOf(item.id) !== -1) {
        item.checked = true
      }
    })
    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listAddClone
    ] =
      state[action.payload.keyInitial_root][action.payload.keyInitial_listShow]
    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listBefore
    ] = action.payload.data

    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listBefore
    ] = action.payload.data
    state[action.payload.keyInitial_root][action.payload.keyInitial_list] =
      action.payload.data
    state[action.payload.keyInitial_root][action.payload.keyInitial_listClone] =
      action.payload.data

    return state
  },

  CANCEL_MODAL: (state, action) => {
    state[action.payload.keyInitial_root][action.payload.keyInitial_listShow] =
      state[action.payload.keyInitial_root][action.payload.keyInitial_Add]

    state[action.payload.keyInitial_root][action.payload.keyInitial_list] =
      state[action.payload.keyInitial_root][action.payload.keyInitial_listClone]

    state[action.payload.keyInitial_root][
      action.payload.keyInitial_listAddClone
    ] = []
    return state
  }
}

export { reducerModal, initialModalS1, initialModalS2, initialBusiness }
