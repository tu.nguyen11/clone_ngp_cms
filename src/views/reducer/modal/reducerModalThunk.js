const initialBusiness = {
  listBusinessAdd: [],
  listBusinessShow: [],
  listBusiness: [],
  listBusinessConfirm: [],

  loading: false
}

const initialDistrict = {
  listDistrictsAdd: [],
  listDistrictShow: [],
  listDistrict: [],
  listDistrictConfirm: [],

  loading: false
}

const initialCity = {
  listCityAdd: [],
  listCityShow: [],
  listCity: [],
  listCityConfirm: [],

  loading: false
}

const initialWard = {
  listWardAdd: [],
  listWardShow: [],
  listWard: [],
  listWardConfirm: [],

  loading: false
}

const reducerModalThunk = {
  ADD_MODAL_THUNK: (state, action) => {
    state[action.payload.key_root][action.payload.key].push(
      action.payload.value
    )
  },
  REMOVE_MODAL_THUNK: (state, action) => {
    const idx = state[action.payload.key_root][action.payload.key]
      .map(item => item[action.payload.keyItem])
      .indexOf(action.payload.value[action.payload.keyItem])
    state[action.payload.key_root][action.payload.key].splice(idx, 1)
    if (action.payload.isShow) {
      state[action.payload.key_root][action.payload.key1] =
        state[action.payload.key_root][action.payload.key]
    }
  },
  CLEAR_MODAL_ALL_KD: (state, action) => {
    state[action.payload.key_root][action.payload.key_confirm] = []
    state[action.payload.key_root][action.payload.key_add] = []
    state[action.payload.key_root][action.payload.key_list] = []
    state[action.payload.key_root][action.payload.key_show] = []
  },
  SHOW_MODAL_THUNK: (state, action) => {
    state[action.payload.key_root][action.payload.key_show] =
      state[action.payload.key_root][action.payload.key]
  },
  COFIRM_MODAL_THUNK: (state, action) => {
    state[action.payload.key_root][action.payload.key_show] =
      state[action.payload.key_root][action.payload.key]
  },
  REMOVE_COFIRM_MODAL_THUNK: (state, action) => {
    const idx = state[action.payload.key_root][action.payload.key]
      .map(item => item.agency_id)
      .indexOf(action.payload.value.agency_id)
    state[action.payload.key_root][action.payload.key].splice(idx, 1)
    state[action.payload.key_root][action.payload.key_add] =
      state[action.payload.key_root][action.payload.key]
    state[action.payload.key_root][action.payload.key_show] =
      state[action.payload.key_root][action.payload.key]
  },
  ADD_ALL_MODAL_THUNK: (state, action) => {
    state[action.payload.key_root][action.payload.key_add] =
      state[action.payload.key_root][action.payload.list]
    state[action.payload.key_root][action.payload.key_show] =
      state[action.payload.key_root][action.payload.list]
  },
  REMOVE_ALL_MODAL_THUNK: (state, action) => {
    state[action.payload.key_root][action.payload.key_add] = []
    state[action.payload.key_root][action.payload.key_show] = []
  }
}

export { reducerModalThunk, initialDistrict, initialCity, initialWard }
