import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectAttrubie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    this._APIgetListAttribute();
  }

  _APIgetListAttribute = async () => {
    const { type } = this.props;
    const data = await APIService._getListAttribute();
    const attributes = data.attributes;
    let listAttributes = [];
    if (type == "min") {
      attributes.map((item, index) => {
        if (item.type == 1)
          listAttributes.push({ key: item.id, value: item.name });
      });
      this.setState({
        data: listAttributes
      });
    }
    if (type == "max") {
      attributes.map((item, index) => {
        if (item.type == 3)
          listAttributes.push({ key: item.id, value: item.name });
      });

      this.setState({
        data: listAttributes
      });
    }
  };
  render() {
    const { style } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%"
        }}
      >
        <Select {...this.props} defaultValue size="large">
          {this.state.data.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
