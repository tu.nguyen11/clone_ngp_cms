import { Pagination, Icon } from "antd";
import React, { Component } from "react";
import ITitle from "./ITitle";
import locale from "antd/es/date-picker/locale/zh_CN";
import moment from "moment";
import "moment/locale/zh-cn";
moment.locale("zh-cn");
function itemRender(current, type, originalElement) {
  if (type === "prev") {
    return <Icon type="left" />;
  }
  if (type === "next") {
    return <Icon type="right" />;
  }
  return (
    <div
      className="center"
      style={{
        width: 42
      }}
    >
      <ITitle level={4} title={current} />
    </div>
  );
}
class IPagination extends Component {
  render() {
    const { totalItem, indexPage = 1, sizeItem } = this.props; // totalItem là tổng số lượng item , indexPage là vị trí trang hiện tại ,sizeItem là số lượng item trong 1 page
    const PageMax = Math.ceil(totalItem / sizeItem) * 10;
    return (
      <Pagination
        {...this.props}
        // itemRender={itemRender}
        total={PageMax}
        current={indexPage}
        // showQuickJumper
        locale={locale}
        // showSizeChanger
      />
    );
  }
}
export default IPagination;
