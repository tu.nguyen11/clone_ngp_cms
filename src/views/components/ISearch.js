import React, { Component } from "react";
import styled from "styled-components";
import { IStyledSearch } from "./styled/search/styledSearch";
import { colors } from "../../assets";

const IDiv = styled.div`
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  cursor: pointer;

  :hover ${IStyledSearch} {
    width: 400px;

    transform: 0.2s;
    border-bottom: 1px solid #f5f5f5;
    background-color: "f5f5f5";
    padding: 2px;
  }
`;

export default class ISearch extends Component {
  render() {
    const { icon, style } = this.props;
    return (
      <IDiv
        style={{
          backgroundColor: colors.white,
          borderRadius: 21,
          display: "flex",
          flexDirection: "row",
        }}
      >
        {icon}
        <IStyledSearch required {...this.props} />
      </IDiv>
    );
  }
}
