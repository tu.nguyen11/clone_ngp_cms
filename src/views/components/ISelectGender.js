import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import "./style.css";
const { Option } = Select;
let data = [
  { key: 0, value: "Nam" },
  { key: 1, value: "Nữ" }
];
export default class ISelectGender extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { style } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%"
        }}
      >
        <Select
          {...this.props}
          defaultValue
          style={{
            width: "100%",
            fontSize: "1em",
            height: 42,
            // maxWidth: 235,
            minWidth: 200,
            color: "black",
            // borderLeftWidth: 0,
            // borderRightWidth: 0,
            // borderTopWidth: 0,
            // borderBottomWidth: 0,
            // borderColor: "black",
            // borderStyle: "solid"

            ...style
          }}
          size="large"
        >
          {data.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
