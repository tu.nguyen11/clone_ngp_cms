import { DatePicker } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import "./ISvg.css";
import locale from "antd/es/date-picker/locale/vi_VN";
import moment from "moment";
import "moment/locale/vi";
import { colors } from "../../assets";
const { MonthPicker, RangePicker, WeekPicker, style } = DatePicker;

class IDatePicker extends Component {
  render() {
    const { format = "DD/MM/YYYY", from = 0, to = 0, style } = this.props;
    if (from == 0) {
      return null;
    }
    const { isBackground = true, width = 280 } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: width,
        }}
      >
        <RangePicker
          {...this.props}
          locale={locale}
          size="large"
          format={format}
          defaultValue={[
            moment(new Date(from), format),
            moment(new Date(to), format),
          ]}
          style={
            {
              // background:"white !important"
              // height:42
              // ...style,
              // width: "16vw",
              // background: colors.white,
              // borderWidth: 1,
              // borderColor: colors.line,
              // borderStyle: "solid"
            }
          }
          allowClear={false}
          separator={"-"}
        />
      </div>
    );
  }
}
export default IDatePicker;
