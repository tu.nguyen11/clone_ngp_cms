import React from "react";
import styled from "styled-components";
import "./styleTableHtml.css";

const DivCustom = styled.div`
  height: ${(props) => props.height} !important;
`;

export default function ITableHtml(props) {
  const {
    childrenHeader,
    childrenBody,
    isfoot = false,
    isBorder = true,
    childrenTitle,
    childrenFoot,
  } = props;
  return (
    <>
      <DivCustom className="tableFixHead cursor-scroll" {...props}>
        <table
          style={{
            borderLeft: isBorder
              ? "1px solid rgba(122, 123, 123, 0.5)"
              : "unset",
            borderRight: isBorder
              ? "1px solid rgba(122, 123, 123, 0.5)"
              : "unset",
            borderBottom: isBorder
              ? "1px solid rgba(122, 123, 123, 0.5)"
              : "unset",
            width: "100%",
          }}
        >
          <thead className="thead-table ">{childrenTitle}</thead>
          <thead className="thead-table">{childrenHeader}</thead>
          <tbody style={{ overflow: "hidden" }}>{childrenBody}</tbody>
        </table>
      </DivCustom>
      {!isfoot ? null : (
        <div>
          <table>{isfoot ? childrenFoot : null}</table>
        </div>
      )}
    </>
  );
}
