import React from "react";
import { ISelect } from ".";

export default function ISelectBranch(props) {
  let dataStore = [{ key: 0, value: "Tất cả" }];
  return (
    <div>
      <ISelect data={dataStore} {...props} />
    </div>
  );
}
