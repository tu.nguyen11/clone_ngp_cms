import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";
import { Container, Col, Row } from "reactstrap";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectCity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCity: [],
      search: "",
    };
  }

  componentDidMount() {
    this._getAPIListCities(this.state.search);
  }

  _getAPIListCities = async (search) => {
    var { fillerAll = false } = this.props;
    const data = await APIService._getListCities(search);
    const dataNew = data.cities.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    if (fillerAll) {
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
    }

    this.setState({
      listCity: dataNew,
    });
  };

  render() {
    const { style, select = false } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%",
        }}
      >
        <Select
          {...this.props}
          style={{
            ...style,
            width: "100%",
            fontSize: "1em",
            height: 40,
            color: "black",
            borderWidth: 0,
          }}
          dropdownStyle={{ minWidth: 200 }}
          showSearch
          optionFilterProp="children"
          onSearch={(keySearch) => {
            this.setState(
              {
                search: keySearch,
              },
              () => this._getAPIListCities(this.state.search)
            );
          }}
          size="large"
          disabled={!select}
        >
          {this.state.listCity.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
