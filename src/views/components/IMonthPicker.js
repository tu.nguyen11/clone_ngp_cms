import React, {Component} from "react";
import locale from "antd/es/date-picker/locale/vi_VN";
import moment from "moment";
import {DatePicker} from 'antd'

class IMonthPicker extends Component {
    render() {
        const {MonthPicker, RangePicker} = DatePicker;
        const {format = 'MM/YYYY', from = 0, style, picker = "month"} = this.props;
        if (from == 0) {
            return null;
        }
        const {isBackground = true, width = 170} = this.props;
        return (
            <div
                className={isBackground ? "" : "clear-pad"}
                style={{
                    background: isBackground ? "white" : "transparent",
                    border: isBackground ? "1px solid #cdcece" : "none",
                    borderBottom: "1px solid #cdcece",
                    width: width,
                }}
            >
                <MonthPicker
                    {...this.props}
                    locale={locale}
                    size="large"
                    format={format}
                    // defaultValue={
                    //     moment(from, format)
                    // }
                    picker={picker}
                />
            </div>
        );
    }
}

export default IMonthPicker;
