import React, { Component } from "react";
import { Upload, Icon, message, Modal } from "antd";
import { APIService } from "../../services";
import { ImageType } from "../../constant";
import ISvg from "./ISvg";
import { SVGAddUpload } from "../../assets/svg";
import { images } from "../../assets";
import { IError } from "./common";
class IUpload extends Component {
  constructor(props) {
    super(props);
    const { name, url, callback, typeAgency, className = "border1" } = props;
    this.state = {
      loading: false,
      imageUrl: name ? url + name : "",
      className: className,
      file: null,
    };

    this.beforeUpload = this.beforeUpload.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.name !== this.props.name ||
      nextProps.url !== this.props.url
    ) {
      //Perform some operation
      let imageUrl = nextProps.url + nextProps.name;
      this.setState({ imageUrl: imageUrl });
      // this.classMethod();
    }
  }
  onRemove = (file) => {
    try {
      var index = file;
      this.setState({
        imageUrl: false,
      });
    } catch (error) {}
  };
  render() {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? "loading" : "plus"} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const uploadButtonAgency = (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          width: this.props.width,
        }}
      >
        <ISvg
          name={ISvg.NAME.ADDUPLOAD}
          width={30}
          height={30}
          // fill={colors.icon.default}
        />
      </div>
    );
    const { name, url, callback, sizeImg, className = "border1" } = this.props;

    let { imageUrl } = this.state;
    return (
      <div style={{}}>
        <Upload
          {...this.props}
          name="avatar"
          listType="picture-card"
          className={this.state.className}
          showUploadList={false}
          beforeUpload={this.beforeUpload}
          // onChange={this.handleChange}
          // onRemove={this.onRemove}
          accept=".jpg,.jpeg,.png"
        >
          {imageUrl ? (
            <div>
              <img
                src={imageUrl}
                alt="avatar"
                style={{
                  width: sizeImg ? 235 : 180,
                  height: sizeImg ? 100 : 180,
                }}
              />
              {/* <div
                onClick={() => this.onRemove(this.state.file)}
                style={{
                  position: "absolute",
                  width: 20,
                  height: 20,
                  top: 0,
                  right: 5,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <img
                  src={images.remove}
                  style={{ width: "100%", height: "100%" }}
                />
              </div> */}
            </div>
          ) : (
            uploadButtonAgency
          )}
        </Upload>
        <Modal visible={false} footer={null} onCancel={this.handleCancel}>
          <img alt="example" style={{ width: "100%" }} />
        </Modal>
      </div>
    );
  }

  // handleChange = async info => {
  //   if (info.file.status === "uploading") {
  //     return;
  //   }

  //   if (info.file.status === "done") {
  //
  //   }
  // };
  beforeUpload(file) {
    // const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    // if (!isJpgOrPng) {
    //   message.error("You can only upload JPG/PNG file!");
    // }
    // const isLt2M = file.size / 1024 < 512;
    // if (!isLt2M) {
    //   message.error("Image must smaller than 512Kb!");
    // }
    // if (!isJpgOrPng || !isLt2M) {
    //   return false;
    // }

    if (file.size > 10000000) {
      return IError("Hình ảnh không upload trên 10mb");
    }

    const { callback, type } = this.props;
    return new Promise(async (resolve, reject) => {
      try {
        this.setState({ loading: true });

        const response = await APIService._uploadImage(type, file);

        this.setState({
          loading: false,
          imageUrl: response.image_url + response.images[0],
          className: "hideboder",
          file: file,
        });

        if (callback) {
          callback(response.images, response.image_url);
        }
        this.setState({});
        resolve();
      } catch (error) {
        this.setState({ loading: false });
        reject();
      }
    });
  }

  getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }
}

export default IUpload;
