import React, { Component } from "react";
import { Modal } from "antd";

import "./ILoaingstyle.css";
export default class ILoading extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { onLoad = true } = this.props;
    return onLoad ? (
      <div class="loading">
        <div class="obj"></div>
        <div class="obj"></div>
        <div class="obj"></div>
        <div class="obj"></div>
        <div class="obj"></div>
        <div class="obj"></div>
        <div class="obj"></div>
        <div class="obj"></div>
      </div>
    ) : null;
  }
}
