"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ISvg", {
  enumerable: true,
  get: function get() {
    return _ISvg["default"];
  }
});
Object.defineProperty(exports, "ISearch", {
  enumerable: true,
  get: function get() {
    return _ISearch["default"];
  }
});
Object.defineProperty(exports, "IButton", {
  enumerable: true,
  get: function get() {
    return _IButton["default"];
  }
});
Object.defineProperty(exports, "ITitle", {
  enumerable: true,
  get: function get() {
    return _ITitle["default"];
  }
});
Object.defineProperty(exports, "ISelect", {
  enumerable: true,
  get: function get() {
    return _ISelect["default"];
  }
});
Object.defineProperty(exports, "ITable", {
  enumerable: true,
  get: function get() {
    return _ITable["default"];
  }
});
Object.defineProperty(exports, "IPagination", {
  enumerable: true,
  get: function get() {
    return _IPagination["default"];
  }
});
Object.defineProperty(exports, "IInput", {
  enumerable: true,
  get: function get() {
    return _IInput["default"];
  }
});
Object.defineProperty(exports, "IList", {
  enumerable: true,
  get: function get() {
    return _IList["default"];
  }
});
Object.defineProperty(exports, "IUpload", {
  enumerable: true,
  get: function get() {
    return _IUpload["default"];
  }
});
Object.defineProperty(exports, "ISelectMultiple", {
  enumerable: true,
  get: function get() {
    return _ISelectMultiple["default"];
  }
});
Object.defineProperty(exports, "ICascader", {
  enumerable: true,
  get: function get() {
    return _ICascader["default"];
  }
});
Object.defineProperty(exports, "IImage", {
  enumerable: true,
  get: function get() {
    return _IImage["default"];
  }
});
Object.defineProperty(exports, "ISelectCity", {
  enumerable: true,
  get: function get() {
    return _ISelectCity["default"];
  }
});
Object.defineProperty(exports, "ISelectCounty", {
  enumerable: true,
  get: function get() {
    return _ISelectCounty["default"];
  }
});
Object.defineProperty(exports, "IExportExecl", {
  enumerable: true,
  get: function get() {
    return _IExportExecl["default"];
  }
});
Object.defineProperty(exports, "ISelectStatus", {
  enumerable: true,
  get: function get() {
    return _ISelectStatus["default"];
  }
});
Object.defineProperty(exports, "IDatePicker", {
  enumerable: true,
  get: function get() {
    return _IDatePicker["default"];
  }
});
Object.defineProperty(exports, "ISelectAttrubie", {
  enumerable: true,
  get: function get() {
    return _ISelectAttrubie["default"];
  }
});
Object.defineProperty(exports, "IUploadMultiple", {
  enumerable: true,
  get: function get() {
    return _IUploadMultiple["default"];
  }
});
Object.defineProperty(exports, "ISelectWareHouse", {
  enumerable: true,
  get: function get() {
    return _ISelectWareHouse["default"];
  }
});
Object.defineProperty(exports, "ISelectTypeWareHouse", {
  enumerable: true,
  get: function get() {
    return _ISelectTypeWareHouse["default"];
  }
});
Object.defineProperty(exports, "ISelectAllStatus", {
  enumerable: true,
  get: function get() {
    return _ISelectAllStatus["default"];
  }
});
Object.defineProperty(exports, "ISelectAllWareHouse", {
  enumerable: true,
  get: function get() {
    return _ISelectAllWareHouse["default"];
  }
});
Object.defineProperty(exports, "ISelectAllTypeShipper", {
  enumerable: true,
  get: function get() {
    return _ISelectAllTypeShipper["default"];
  }
});
Object.defineProperty(exports, "ISelectShipperAssagin", {
  enumerable: true,
  get: function get() {
    return _ISelectShipperAssagin["default"];
  }
});
Object.defineProperty(exports, "ILoading", {
  enumerable: true,
  get: function get() {
    return _ILoading["default"];
  }
});
Object.defineProperty(exports, "IVinhHoang", {
  enumerable: true,
  get: function get() {
    return _IVinhHoang["default"];
  }
});
Object.defineProperty(exports, "ISelectGender", {
  enumerable: true,
  get: function get() {
    return _ISelectGender["default"];
  }
});
Object.defineProperty(exports, "ISelectRouterSalesman", {
  enumerable: true,
  get: function get() {
    return _ISelectRouterSalesman["default"];
  }
});
Object.defineProperty(exports, "IBannerIMG", {
  enumerable: true,
  get: function get() {
    return _IBannerIMG["default"];
  }
});
Object.defineProperty(exports, "ITableTransfer", {
  enumerable: true,
  get: function get() {
    return _ITableTransfer["default"];
  }
});
Object.defineProperty(exports, "ISelectSaleman", {
  enumerable: true,
  get: function get() {
    return _ISelectSaleman["default"];
  }
});
Object.defineProperty(exports, "IChart", {
  enumerable: true,
  get: function get() {
    return _IChart["default"];
  }
});
Object.defineProperty(exports, "IEditor", {
  enumerable: true,
  get: function get() {
    return _IEditor["default"];
  }
});
Object.defineProperty(exports, "ISelectTypeNews", {
  enumerable: true,
  get: function get() {
    return _ISelectTypeNews["default"];
  }
});
Object.defineProperty(exports, "ISelectStatusNews", {
  enumerable: true,
  get: function get() {
    return _ISelectStatusNews["default"];
  }
});
Object.defineProperty(exports, "ISelectCateNews", {
  enumerable: true,
  get: function get() {
    return _ISelectCateNews["default"];
  }
});
Object.defineProperty(exports, "ISelectBranch", {
  enumerable: true,
  get: function get() {
    return _ISelectBranch["default"];
  }
});

var _ISvg = _interopRequireDefault(require("./ISvg"));

var _ISearch = _interopRequireDefault(require("./ISearch"));

var _IButton = _interopRequireDefault(require("./IButton"));

var _ITitle = _interopRequireDefault(require("./ITitle"));

var _ISelect = _interopRequireDefault(require("./ISelect"));

var _ITable = _interopRequireDefault(require("./ITable"));

var _IPagination = _interopRequireDefault(require("./IPagination"));

var _IInput = _interopRequireDefault(require("./IInput"));

var _IList = _interopRequireDefault(require("./IList"));

var _IUpload = _interopRequireDefault(require("./IUpload"));

var _ISelectMultiple = _interopRequireDefault(require("./ISelectMultiple"));

var _ICascader = _interopRequireDefault(require("./ICascader"));

var _IImage = _interopRequireDefault(require("./IImage"));

var _ISelectCity = _interopRequireDefault(require("./ISelectCity"));

var _ISelectCounty = _interopRequireDefault(require("./ISelectCounty"));

var _IExportExecl = _interopRequireDefault(require("./IExportExecl"));

var _ISelectStatus = _interopRequireDefault(require("./ISelectStatus"));

var _IDatePicker = _interopRequireDefault(require("./IDatePicker"));

var _ISelectAttrubie = _interopRequireDefault(require("./ISelectAttrubie"));

var _IUploadMultiple = _interopRequireDefault(require("./IUploadMultiple"));

var _ISelectWareHouse = _interopRequireDefault(require("./ISelectWareHouse"));

var _ISelectTypeWareHouse = _interopRequireDefault(require("./ISelectTypeWareHouse"));

var _ISelectAllStatus = _interopRequireDefault(require("./ISelectAllStatus"));

var _ISelectAllWareHouse = _interopRequireDefault(require("./ISelectAllWareHouse"));

var _ISelectAllTypeShipper = _interopRequireDefault(require("./ISelectAllTypeShipper"));

var _ISelectShipperAssagin = _interopRequireDefault(require("./ISelectShipperAssagin"));

var _ILoading = _interopRequireDefault(require("./ILoading"));

var _IVinhHoang = _interopRequireDefault(require("./IVinhHoang"));

var _ISelectGender = _interopRequireDefault(require("./ISelectGender"));

var _ISelectRouterSalesman = _interopRequireDefault(require("./ISelectRouterSalesman"));

var _IBannerIMG = _interopRequireDefault(require("./IBannerIMG"));

var _ITableTransfer = _interopRequireDefault(require("./ITableTransfer"));

var _ISelectSaleman = _interopRequireDefault(require("./ISelectSaleman"));

var _IChart = _interopRequireDefault(require("./IChart"));

var _IEditor = _interopRequireDefault(require("./IEditor"));

var _ISelectTypeNews = _interopRequireDefault(require("./ISelectTypeNews"));

var _ISelectStatusNews = _interopRequireDefault(require("./ISelectStatusNews"));

var _ISelectCateNews = _interopRequireDefault(require("./ISelectCateNews"));

var _ISelectBranch = _interopRequireDefault(require("./ISelectBranch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }