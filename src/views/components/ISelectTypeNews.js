import { Select } from "antd";
import React, { Component } from "react";
import "./style.css";
const { Option } = Select;

export default class ISelectTypeNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typeNews: [
        { key: "", value: "Tất cả" },
        { key: "NEWS", value: "Bài viết" },
        { key: "VIDEO", value: "Video" }
      ]
    };
  }

  render() {
    const { isBackground = true, style } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%"
        }}
      >
        <Select
          {...this.props}
          style={{
            width: "100%",
            fontSize: "1em",
            height: 42,

            color: "black",
            ...style,
            borderBotom: "1px solid #ccc"
          }}
          size="large"
        >
          {this.state.typeNews.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
