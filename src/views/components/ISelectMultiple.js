import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";
import "./style.css";

import { Container, Col, Row } from "reactstrap";
const { Option } = Select;

function handleChange(value) {
  console.log(`selected ${value}`);
}
class ISelectMultiple extends Component {
  render() {
    const { data } = this.props;
    return (
      <Select
        mode="multiple"
        style={{ width: "100%", height: 32 }}
        size="large"
        placeholder="Please select"
        // defaultValue={data}
        onChange={handleChange}
      >
        {data.map((item, index) => (
          <Option value={item.id}>{item.value}</Option>
        ))}
      </Select>
    );
  }
}

export default ISelectMultiple;
