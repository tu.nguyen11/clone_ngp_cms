"use strict";
function _templateObject2() {
  var e = _taggedTemplateLiteral([
    "\n  & > li {\n    height: 42px;\n    border: none;\n    font-size: 13px;\n    line-height: 18px;\n    color: #383b48;\n    background: #f3f7f5;\n    font-family: Avenir;\n  }\n  & .ant-pagination-item-active {\n    border: none;\n    background: #004C91;\n    font-size: 13px;\n    line-height: 18px;\n    border-radius: 0px;\n  }\n  .ant-pagination-item-active span {\n    color: #ffffff;\n  }\n",
  ]);
  return (
    (_templateObject2 = function () {
      return e;
    }),
    e
  );
}
function _templateObject() {
  var e = _taggedTemplateLiteral([
    "\n  & .ant-table-thead > tr > th {\n    background: #f3f7f5;\n    font-style: normal;\n    font-weight: bold;\n    font-size: 14px;\n    line-height: 19px;\n    border-bottom: 1px solid rgba(122, 123, 123, 0.3);\n    font-family: Avenir Next;\n  }\n\n  & .ant-table-tbody > tr > td {\n    background: #f3f7f5;\n    border-bottom: 1px solid rgba(122, 123, 123, 0.3);\n    font-size: clamp(8px, 4vw, 14px);\n    font-style: normal;\n    font-weight: normal;\n    color: #22232b;\n    line-height: 20px;\n    font-family: Avenir Next;\n  }\n  & .ant-table-footer {\n    background: #f3f7f5;\n  }\n",
  ]);
  return (
    (_templateObject = function () {
      return e;
    }),
    e
  );
}
function _taggedTemplateLiteral(e, t) {
  return (
    (t = t || e.slice(0)),
    Object.freeze(
      Object.defineProperties(e, { raw: { value: Object.freeze(t) } })
    )
  );
}
Object.defineProperty(exports, "__esModule", { value: !0 }),
  (exports.StyledPagination = exports.StyledTable = void 0);
var _require = require("styled-components"),
  styled = _require.default,
  _require2 = require("antd"),
  Table = _require2.Table,
  Pagination = _require2.Pagination,
  StyledTable = styled(Table)(_templateObject());
exports.StyledTable = StyledTable;
var StyledPagination = styled(Pagination)(_templateObject2());
exports.StyledPagination = StyledPagination;
