const { default: styled } = require("styled-components");

const { Table, Pagination } = require("antd");
// const StyledTable = styled(Table)`
//   .ant-table-thead {
//     background: #f5f3f6;
//   }
//   & .ant-table-thead > tr > th {
//     font-style: normal;
//     font-weight: bold;
//     font-size: 14px;
//     line-height: 19px;
//     border-bottom: 1px solid rgba(122, 123, 123, 0.3);
//   }
//   .ant-table-column-title {
//     font-weight: bold;
//   }

//   & .ant-table-tbody > tr > td {
//     border-bottom: 1px solid rgba(122, 123, 123, 0.3);
//     font-size: clamp(8px, 4vw, 14px);
//     font-style: normal;
//     font-weight: 400;
//     color: #22232b;
//     line-height: 20px;
//   }

//   & .ant-table-tbody > tr > td > span {
//     display: inline-block;
//     display: -webkit-box;
//     -webkit-line-clamp: 1;
//     -webkit-box-orient: vertical;
//     overflow: hidden;
//   }

//   & .ant-table-footer {
//     background: #f5f3f6;
//   }
// `;

// const StyledPagination = styled(Pagination)`
//   & > li {
//     height: 42px;
//     border: none;
//     font-size: 13px;
//     line-height: 18px;
//     color: #383b48;
//     // background: #f3f7f5;
//   }
//   & .ant-pagination-item-active {
//     border: none;
//     background: #004c91;
//     font-size: 13px;
//     line-height: 18px;
//     border-radius: 0px;
//   }
//   .ant-pagination-item-active span {
//     color: #ffffff;
//   }
// `;

const StyledTable = styled(Table)`
// box-shadow: 0 0px 27px 0 rgb(0 0 0 / 5%) !important;
& .ant-table-tbody > tr > td > span  {
  display: inline-block;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
}
  .ant-table-column-title {
        font-weight: bold;
      }
    .ant-table-thead {
      background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};
    }
    & .ant-table-thead > tr > th {
      background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};
      font-style: normal;
      font-weight: bold;
      border-radius: 0px !important;
      font-size: 14px;
      line-height: 19px;
      border-bottom: ${(props) =>
        props.bordered === true
          ? "0px solid rgba(122, 123, 123, 0.3)"
          : "1px solid rgba(122, 123, 123, 0.3)"};
    }

    & .ant-table.ant-table-bordered thead > tr > th {
      border-right: 1px solid rgba(122, 123, 123, 0.3) !important;
      border-bottom: ${(props) =>
        props.bordered === false
          ? "0px solid rgba(122, 123, 123, 0.3)"
          : "1px solid rgba(122, 123, 123, 0.3)"};
    }

    & .ant-table.ant-table-bordered thead > tr > th:last-child {
      border-right: 0px solid rgba(122, 123, 123, 0.3) !important;
    }

    & .ant-table-tbody > tr > td {
      background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};

      border-bottom: ${(props) =>
        props.bordered === false
          ? "0px solid rgba(122, 123, 123, 0.3) !important"
          : "1px solid rgba(122, 123, 123, 0.3) !important"};
    }

    & .ant-table.ant-table-bordered tbody > tr > td {
      border-right: ${(props) =>
        props.bordered === false
          ? "0px solid rgba(122, 123, 123, 0.3) !important"
          : "1px solid rgba(122, 123, 123, 0.3) !important"};
    }

    & .ant-table.ant-table-bordered tbody > tr > td:last-child {
      border-right: 0px solid rgba(122, 123, 123, 0.3) !important;
    }

    background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};
    font-size: clamp(8px, 4vw, 14px);
    font-style: normal;
    font-weight: normal;
    color: #22232b;
    line-height: 20px;
  }

  & .ant-table-tbody > tr > td > div {
    display: inline-block;
    display: -webkit-box;
    -webkit-line-clamp: ${(props) => (!props.rows ? 1 : props.rows)};
    -webkit-box-orient: vertical;
    overflow: hidden;
  }

  & .ant-table-tbody > tr > td > span {
    display: inline-block;
    display: -webkit-box;
    -webkit-line-clamp: ${(props) => (!props.rows ? 1 : props.rows)};
    -webkit-box-orient: vertical;
    overflow: hidden;
  }

  & .ant-table-footer {
    background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};
  }
`;

const StyledPagination = styled(Pagination)`
  /* & > li {
    height: 42px;
    border: none;
    font-size: 13px;
    line-height: 18px;
    color: #383b48;
    background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};
  ;
  }
  & .ant-pagination-item-active {
    border: none;
    background: #004C91;
    font-size: 13px;
    line-height: 18px;
    border-radius: 0px;
  }
  .ant-pagination-item-active span {
    color: #ffffff;
  } */
`;

export { StyledTable, StyledPagination };
