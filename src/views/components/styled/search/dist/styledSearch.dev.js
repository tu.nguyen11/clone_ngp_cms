"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IStyledSearch = void 0;

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  border: none !important;\n  height: 42px;\n  width: 0px;\n  padding: 0px;\n  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),\n    0 4px 6px -2px rgba(0, 0, 0, 0.05);\n  :focus {\n    width: 400px;\n    transform: 0.4s;\n    background: \"f5f5f5\";\n    padding: 2px;\n  }\n\n  :valid {\n    width: 400px;\n    transform: 0.4s;\n    border-bottom: 1px solid #f5f5f5;\n    background: \"f5f5f5\";\n    padding: 2px;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _require = require("styled-components"),
    styled = _require["default"];

var _require2 = require("antd"),
    Input = _require2.Input;

var IStyledSearch = styled(Input)(_templateObject());
exports.IStyledSearch = IStyledSearch;