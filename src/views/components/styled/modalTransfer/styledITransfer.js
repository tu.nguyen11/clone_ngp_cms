const { default: styled } = require("styled-components");
const { Input } = require("antd");

const StyledInput = styled(Input)`
  height: 42px;
`;

export { StyledInput };
