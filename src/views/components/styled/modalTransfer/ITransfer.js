import React, { useState } from "react";
import { Row, Col, Checkbox, List } from "antd";
import ITitle from "../../ITitle";
import { images, colors } from "../../../../assets";
import { StyledInput } from "./styledITransfer";
import InfiniteScroll from "react-infinite-scroller";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import "./cssITransfer.css";
function ITransfer(props) {
  const {
    title,
    dataList,
    dataHeader,
    dataBody,
    callbackClose,
    keyId,
    align,
  } = props;

  const [dataCheck, setDateCheck] = useState([...dataList]);
  const headerTable = (headerTable) => {
    return (
      <tr className="tr-table">
        {headerTable.map((item, index) => (
          <th
            className="th-table"
            style={{ textAlign: item.align }}
            width={item.width}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (dataBody, dataHeader, keyId, align) => {
    return dataBody.map((item, index) => (
      <tr className="tr-table">
        {dataHeader.map((item1, index1) => {
          return index1 === dataHeader.length - 1 ? (
            <td className="td-table" style={{ textAlign: "center" }}>
              {/* <div
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  background: " rgba(227, 95, 75, 0.1)",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
                className="cursor scale"
              >
                <ISvg
                  name={ISvg.NAME.CROSS}
                  width={7}
                  height={7}
                  fill={colors.oranges}
                />
              </div> */}
            </td>
          ) : (
            <td className="td-table" style={{ textAlign: align[index1] }}>
              {item[keyId[index1]]}
            </td>
          );
        })}
      </tr>
    ));
  };

  return (
    <div>
      <div style={{ padding: "30px 40px" }}>
        <Row gutter={[26, 12]}>
          <Col span={24}>
            <ITitle title={title} level={3} style={{ fontWeight: "bold" }} />
          </Col>
          <Col span={24}>
            <Row gutter={[24, 12]} type="flex">
              {/* <Col span={8}>
                <div>
                  <Col>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <img
                        src={images.icSearch}
                        style={{ width: 16, height: 16, marginRight: 12 }}
                        className="cursor scale"
                      />
                      <StyledInput />
                    </div>
                  </Col>
                  <Col>
                    <div
                      style={{
                        height: 350,
                        overflow: "auto",
                        border: "1px solid rgba(122, 123, 123, 0.5)",
                        paddingLeft: 18,
                        paddingRight: 18,
                      }}
                    >
                      <InfiniteScroll
                        initialLoad={false}
                        pageStart={0}
                        //   loadMore={this.handleInfiniteOnLoad}
                        hasMore={true}
                        useWindow={false}
                      >
                        <List
                          dataSource={dataCheck}
                          bordered={false}
                          renderItem={(item, index) => (
                            <List.Item>
                              <Checkbox
                                checked={item.active}
                                style={{ marginRight: 35 }}
                                defaultChecked={item.active}
                                onChange={(e) => {
                                  dataCheck[index].active = e.target.checked;
                                  setDateCheck([...dataCheck]);
                                  // this.state.dataList[index].active =
                                  //   e.target.checked;
                                  // this.setState({
                                  //   dataList: this.state.dataList.slice(0),
                                  // });
                                }}
                              />
                              <ITitle level={4} title={item.title} />
                            </List.Item>
                          )}
                        />
                      </InfiniteScroll>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        marginTop: 30,
                      }}
                    >
                      <IButton
                        icon={ISvg.NAME.BUTTONRIGHT}
                        title="Thêm"
                        color={colors.main}
                        iconRight={true}
                        onClick={() => {}}
                      />
                    </div>
                  </Col>
                </div>
              </Col>
               */}
              <Col span={24}>
                <div
                  style={{
                    overflow: "auto",
                    height: 480,
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <table
                    className="table-auto "
                    style={{
                      // border: "1px solid #7A7B7B",
                      width: "100%",
                    }}
                  >
                    <thead className="thead-table">
                      {headerTable(dataHeader)}
                    </thead>
                    <tbody>
                      {bodyTable(dataBody, dataHeader, keyId, align)}
                    </tbody>
                  </table>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      {/* <div
        style={{
          display: "flex",
          flexDirection: "row",
          position: "absolute",
          right: 0,
          marginTop: 30,
        }}
      >
        <IButton
          icon={ISvg.NAME.SAVE}
          width={20}
          height={20}
          color={colors.main}
          title="Lưu"
          style={{ marginRight: 20 }}
          onClick={() => alert("abc")}
        />
        <IButton
          icon={ISvg.NAME.CROSS}
          width={20}
          height={20}
          color={"#E35F4B"}
          title="Hủy bỏ"
          onClick={() => callbackClose()}
        />
      </div>
      */}
      <div
        style={{
          width: 20,
          height: 20,
          borderRadius: 10,
          background: "#FCEFED",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          position: "absolute",
          top: -6,
          right: -6,
        }}
        onClick={() => callbackClose()}
        className="cursor scale"
      >
        <ISvg
          name={ISvg.NAME.CROSS}
          width={7}
          height={7}
          fill={colors.oranges}
        />
      </div>
    </div>
  );
}

export default ITransfer;
