"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StyledForm = void 0;

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .ant-form-item {\n    margin-bottom: 0px !important;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _require = require("styled-components"),
    styled = _require["default"];

var _require2 = require("antd"),
    Form = _require2.Form;

var StyledForm = styled(Form)(_templateObject());
exports.StyledForm = StyledForm;