const { default: styled } = require("styled-components");
const { Form } = require("antd");

const StyledForm = styled(Form)`
  .ant-form-item {
    margin-bottom: 0px !important;
  }
`;

export { StyledForm };
