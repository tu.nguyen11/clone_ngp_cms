import React, { Component } from "react";
import { Carousel } from "antd";
import "./styleIBanner.css";
import IImage from "./IImage";
class IBannerIMG extends Component {
  render() {
    const { arr, url } = this.props;
    return (
      <Carousel effect="fade" autoplay>
        {arr.map((item, index) => {
          return (
            <IImage src={url + item} style={{ width: 400, height: 400 }} />
          );
        })}
      </Carousel>
    );
  }
}

export default IBannerIMG;
