import React, { Component } from "react";
import ReactDOM from "react-dom";
import Draft, { EditorState, convertToRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";

import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
class IEditor extends React.Component {
  constructor(props) {
    super(props);
    const draft = htmlToDraft(this.props.html);
    this.state = {
      editorState: EditorState.createWithContent(
        ContentState.createFromBlockArray(draft.contentBlocks)
      )
    };
  }

  onEditorStateChange = editorState => {
    try {
      this.state.editorState = editorState;
      this.setState({});
      if (this.props.callback) {
        const html = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        this.props.callback(html);
      }
    } catch (error) {}
  };

  render() {
    const { html = "", callback = null } = this.props;
    return (
      <Editor
        // ref={this.setEditor}
        editorState={this.state.editorState}
        toolbarClassName="toolbarClassName"
        wrapperClassName="wrapperClassName"
        editorClassName="editorClassName"
        onEditorStateChange={this.onEditorStateChange}
        {...this.props}
      />
    );
  }
}

const styles = {
  editor: {
    border: "1px solid gray",
    minHeight: "6em"
  }
};

export default IEditor;
