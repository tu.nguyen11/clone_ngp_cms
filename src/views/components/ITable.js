import React, { Component } from "react";
import IPagination from "./IPagination.js";
import { StyledTable } from "./styled/table/styledITable.js";
export default class ITable extends Component {
  render() {
    const {
      columns,
      data,
      style,
      defaultCurrent,
      maxpage = 1,
      onChangePage,
      sizeItem = 10,
      indexPage,
      hidePage = false,
      footer = () => {},
      rows = 1,
    } = this.props;

    return (
      <StyledTable
        columns={columns}
        dataSource={data}
        style={{ ...style }}
        pagination={false}
        rowClassName="cursor"
        size="middle"
        backgroundWhite={false}
        isBorderBottom={false}
        rows={rows}
        scroll={{ x: 1400 }}
        {...this.props}
        footer={() =>
          hidePage ? (
            footer()
          ) : (
            <IPagination
              defaultCurrent={defaultCurrent}
              sizeItem={sizeItem}
              totalItem={maxpage}
              indexPage={indexPage}
              onChange={onChangePage}
            />
          )
        }
      />
    );
  }
}
