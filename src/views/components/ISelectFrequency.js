import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";
import { Container, Col, Row } from "reactstrap";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectFrequency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      frequency_plan: [],
      from_limit: 0,
      to_limit: 10,
    };
    this.handleScrollSelect = this.handleScrollSelect.bind(this);
  }

  componentDidMount() {
    this._APIgetSelectFrequency(this.state.from_limit, this.state.to_limit);
  }

  handleScrollSelect = async (e) => {
    const element = e.target;
    console.log(element.scrollHeight);
    if (element.scrollHeight == element.clientHeight + element.scrollTop) {
      // let pageNew = this.state.page + 1;
      // if (pageNew <= this.state.page_size) {
      //   this.setState({
      //     page: pageNew
      //   });
      //   const data = await APIService._getListProductSelect(
      //     this.state.data.merchantId,
      //     pageNew
      //   );
      //
      //   const dataNew = [...this.state.listProduct, ...data.products];
      //   this.setState({
      //     listProduct: dataNew
      //   });
    }
  };

  _APIgetSelectFrequency = async (from_limit, to_limit) => {
    const data = await APIService._getSelectFrequency(from_limit, to_limit);
    const dataNew = data.frequency_plan.map((item, index) => {
      const key = item.id;
      const value = item.code;
      return {
        key,
        value,
      };
    });
    this.setState({
      frequency_plan: dataNew,
    });
  };

  render() {
    const { style, isBackground = true } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%",
        }}
      >
        <Select
          {...this.props}
          onPopupScroll={this.handleScrollSelect}
          // defaultValue
          style={{
            ...style,
            width: "100%",
            fontSize: "1em",
            height: 42,
            color: "black",
            borderWidth: 0,
          }}
          size="large"
        >
          {this.state.frequency_plan.map((item, index) => (
            <Option value={JSON.stringify(item)}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
