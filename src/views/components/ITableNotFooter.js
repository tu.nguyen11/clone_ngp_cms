import React from "react";
import { ITable } from "../components";
import styled from "styled-components";
import { colors } from "../../assets";
const ITableNotFooterStyle = styled(ITable)`
  & .ant-table-footer {
    display: none !important;
  }

  & .ant-table-header,
  .ant-table-hide-scrollbar {
    display: ${(props) =>
      props.data.length === 0 ? "unset" : "normal"} !important;
    background: ${(props) =>
      !props.backgroundHeader ? "white" : props.backgroundHeader} !important;
  }

  border: 1px solid #ccc;

  & .ant-table-thead {
    background: ${(props) =>
      !props.backgroundHeader ? "white" : props.backgroundHeader} !important;
  }
`;

export default function ITableNotFooter(props) {
  return <ITableNotFooterStyle {...props} />;
}
