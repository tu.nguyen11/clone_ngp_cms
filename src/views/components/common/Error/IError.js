import { Modal } from "antd";
import React from "react";

function IError(content, typeModal = "error") {
  return Modal[typeModal]({
    title: <span style={{ fontWeight: "bold", fontSize: 14 }}>THÔNG BÁO</span>,
    content: content,
    onOk() {},
  });
}

export { IError };
