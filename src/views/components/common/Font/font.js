const { default: styled } = require("styled-components");

const StyledITitle = styled.h1`
  font-size: clamp(14px, 5vw, 22px);
  font-style: normal;
  font-weight: bold;
  line-height: 24px;
`;

const StyledITileHeading = styled.h3`
  font-size: clamp(
    ${(props) => props.minFont},
    3vw,
    ${(props) => props.maxFont}
  );
  font-style: normal;
  font-weight: bold;
`;

export { StyledITitle, StyledITileHeading };
