import { InputNumber } from "antd";
import React from "react";
import "./styleIInputNumber.css";
import styled from "styled-components";

const InputNumberStyle = styled(InputNumber)`
  .ant-input-number-input {
    text-align: ${(props) =>
      !props.textAlign ? "left" : props.textAlign} !important;
    padding-right: ${(props) => (!props.suffix ? "0px" : "40px")} !important;
  }
`;

export default function IInputNumber(props) {
  const { suffix = "" } = props;
  return (
    <div>
      <InputNumberStyle className="style-input-number" {...props} />
      {suffix === "" ? null : <span className="suffix">VNĐ</span>}
    </div>
  );
}
