import IDatePickerFrom from "./DatePickerFrom/IDatePickerFrom";
import StyledISelect from "./Select/StyledSelect";
import IInputText from "./Input/IInputText";
import IInputNumber from "./InputNumber/IInputNumber";
import IUploadHook from "./Upload/IUploadHook";
import ITree from "./Tree/ITree";
import { IError } from "./Error/IError";

import { Form, InputNumber, Input } from "antd";
import styled from "styled-components";
import Search from "antd/lib/input/Search";
import useModalAgencyS1 from "./ModalAgency/useModalAgencyS1";
import useModalAgencyS1Filter from "./ModalAgency/useModalAgencyS1Filter";
import useModalAgencyS1FilterState from "./ModalAgency/useModalAgencyS1FilterState";
import useModalAgencyS2 from "./ModalAgency/useModalAgencyS2";
import useModalTreeProduct from "./ModalAgency/useModalTreeProduct";
import Fancybox from "./LightBoxImage";
const { TextArea } = Input;

const StyledInputNumber = styled(InputNumber)`
  .ant-input-number-input {
    text-align: center;
  }
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  max-width: ${(props) => props.maxWidth};
`;

const IFormItem = styled(Form.Item)`
  margin-bottom: 0px !important;
`;

const IInputTextArea = styled(TextArea)`
  color: black;
  border-bottom: 1px solid #bfbfbf;
  border-radius: 0px !important;
  padding: 12px;
  border: 1px solid rgba(122, 123, 123, 0.5) !important;
  :focus {
    -webkit-box-shadow: 0 0 0 0 white !important;
  }
`;

const TagP = styled.p`
  line-height: 0;
`;

const StyledSearchCustom = styled(Search)`
  height: 42px;
  padding-right: 6px;
  width: 100%;
  border: 0px !important;
  .ant-input-suffix {
    opacity: 0 !important;
  }
  .ant-input:focus {
    box-shadow: none !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
`;

export {
  IDatePickerFrom,
  StyledISelect,
  StyledInputNumber,
  IInputText,
  IInputNumber,
  IUploadHook,
  IFormItem,
  IInputTextArea,
  TagP,
  ITree,
  StyledSearchCustom,
  useModalAgencyS1,
  useModalAgencyS1Filter,
  useModalAgencyS1FilterState,
  useModalAgencyS2,
  useModalTreeProduct,
  IError,
  Fancybox
};
