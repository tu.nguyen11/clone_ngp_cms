import { Input } from "antd";
import React from "react";
import styled from "styled-components";

const StyledInputText = styled(Input)`
  height: 42px;
  width: 100%;
  border: unset;
  border-bottom: 1px solid #bfbfbf;
  border-radius: 0px !important;
  padding: 0px !important;
  color: black;

  :focus {
    -webkit-box-shadow: 0 0 0 0 white !important;
  }
`;

export default function IInputText(props) {
  return <StyledInputText {...props} />;
}
