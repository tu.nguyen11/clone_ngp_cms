import React, { useState, useEffect } from "react";
import { Upload } from "antd";
import styled from "styled-components";
import ImgCrop from "antd-img-crop";
import ISvg from "../../ISvg";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import { ImageType } from "../../../../constant";
import { IError } from "../Error/IError";

const UploadCustom = styled(Upload)`
  width: ${(props) => props.width} !important;
  height: ${(props) => props.height} !important;
  .ant-upload.ant-upload-select-picture-card {
    width: ${(props) => props.width} !important;
    height: ${(props) => props.height} !important;
    background: white !important;
    margin: 0px !important;
    border: 2px dashed rgba(0, 113, 197, 0.3);
  }
  .ant-upload-list-picture-card .ant-upload-list-item {
    width: ${(props) => props.width} !important;
    height: ${(props) => props.height} !important;
  }
`;

const IUploadHook = (props) => {
  const {
    callback = () => {},
    removeCallback = () => {},
    dataProps,
    type = ImageType.EVENT,
  } = props;

  const [fileList, setFileList] = useState(dataProps);

  const onRemove = async (file) => {
    setFileList([]);
    removeCallback();
  };

  useEffect(() => {
    if (dataProps.length === 0) {
      setFileList([]);
    } else {
      setFileList(dataProps);
    }
  }, [dataProps]);

  const onChange = async ({ fileList: newFileList }) => {
    try {
      let error = false;
      newFileList.map((item) => {
        if (item.size > 100000) {
          error = true;
          return IError("Hình ảnh không upload trên 100KB");
        }
      });

      if (error) return;

      if (newFileList.length > 1) {
        newFileList.splice(0, 1);
      }

      const data = await APIService._uploadImage(
        type,
        newFileList[0].originFileObj
      );

      setFileList([
        {
          uid: -1,
          name: data.images[0],
          status: "done",
          url: data.image_url + data.images[0],
        },
      ]);
      callback(data.images[0], data.image_url);
    } catch (error) {
      console.log(error);
    }
  };

  const onPreview = async (file) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  return (
    // <ImgCrop rotate>
    <UploadCustom
      {...props}
      listType="picture-card"
      fileList={fileList}
      onChange={onChange}
      onPreview={onPreview}
      onRemove={onRemove}
      className="cursor-push"
      style={{ marginBottom: 24 }}
    >
      {fileList.length < 1 && (
        <ISvg
          name={ISvg.NAME.ADDUPLOAD}
          fill={colors.main}
          width={40}
          height={40}
        />
      )}
    </UploadCustom>
    // </ImgCrop>
  );
};

export default IUploadHook;
