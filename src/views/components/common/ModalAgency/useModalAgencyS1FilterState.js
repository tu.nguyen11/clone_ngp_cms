import { Checkbox, Col, List, message, Row, Input, Spin } from "antd";
import React, { useEffect, useState } from "react";
import { StyledITileHeading } from "../Font/font";

import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import ISelect from "../../ISelect";
import ITableHtml from "../../ITableHtml";
import { APIService } from "../../../../services";
import styled from "styled-components";

const { Search } = Input;

const StyledSearchCustomNew = styled(Search)`
  height: 42px;

  width: 100%;
  border: 0px !important;

  .ant-input:focus {
    box-shadow: none !important;
  }

  .ant-input-affix-wrapper .ant-input {
    padding-left: 0px !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
  .ant-input-search: not(.ant-input-search-enter-button) {
    padding-right: 6px !important;
  }
  .ant-input-suffix {
    font-size: 20px;
  }

  .ant-input {
    padding-left: 0 !important;
  }
`;

export default function useModalAgencyS1FilterState(
  callbackSave = () => {},
  callbackCancel = () => {},
  listS1Confirm,
  clearS1 = false,
  agency_id,
  visiblePopup
) {
  const [filterList, setfilterList] = useState({
    status: 1,
    page: 0,
    city_id: 0,
    agency_id: !agency_id ? -1 : agency_id,
    keySearch: "",
    membership_id: 0,
  });
  const [listMembership, setListMembership] = useState([]);
  const [loadingList, setLoadingList] = useState(true);

  const [listS1, setListS1] = useState([]);
  const [listS1Clone, setListS1Clone] = useState([]);
  const [listS1Show, setListS1Show] = useState([]);

  const _getAPIListAgencyS1 = async () => {
    try {
      setLoadingList(true);
      const data = await APIService._getListAllAgencyS1(
        filterList.status,
        filterList.page,
        filterList.city_id,
        !agency_id ? -1 : agency_id,
        filterList.keySearch,
        filterList.membership_id
      );

      let listS1Clone = [];
      if (listS1Confirm.length > 0) {
        listS1Clone = [...listS1Confirm];
        setListS1Clone([...listS1Confirm]);
        setListS1Show([...listS1Confirm]);
      } else {
        listS1Clone = [...listS1Clone];
      }
      let useragency = [...data.useragency];
      useragency.map((item, index) => {
        const idx = listS1Clone.findIndex(
          (itemClone) => itemClone.dms_code === item.dms_code
        );
        if (idx === -1) {
          useragency[index].checked = false;
        } else {
          useragency[index].checked = true;
        }
      });

      setListS1(useragency);

      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  const _getAPIListMembership = async () => {
    try {
      const data = await APIService._getListMemberships();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setListMembership([...dataNew]);
    } catch (error) {}
  };

  useEffect(() => {
    _getAPIListMembership();
  }, []);

  useEffect(() => {
    if (clearS1) {
      setLoadingList(true);
      let listS1Tmp = [...listS1];
      listS1Tmp.map((item) => {
        item.checked = false;
        return item;
      });
      setListS1Clone([]);
      setListS1Show([]);
      setListS1([...listS1Tmp]);
      setLoadingList(false);
    }
  }, [clearS1]);

  useEffect(() => {
    if (visiblePopup === true) {
      _getAPIListAgencyS1(filterList);
    }
  }, [filterList, visiblePopup]);

  const headerTable = [
    {
      name: "STT",
      align: "center",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Cấp bậc",
      align: "left",
    },
    {
      name: "",
      align: "center",
    },
  ];

  const headerTableProduct = (headerTable) => {
    return (
      <tr>
        {headerTable.map((item, index) => (
          <th
            className={"th-table"}
            style={{ textAlign: item.align, fontWeight: 600 }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return loadingList ? (
      <tr height="400px">
        <td colspan="5" style={{ padding: 20 }}>
          <div
            style={{
              width: "100%",
              height: "100%",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Spin />
          </div>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{
              textAlign: "center",
              fontWeight: 400,
              fontSize: "clamp(8px, 4vw, 14px)",
            }}
          >
            {index + 1}
          </td>
          <td
            className="td-table"
            style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
          >
            {item.dms_code}
          </td>
          <td
            className="td-table"
            style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
          >
            {item.shop_name}
          </td>
          <td
            className="td-table"
            style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
          >
            {item.membership}
          </td>
          <td className="td-table">
            <div
              style={{
                background: "rgba(227, 95, 75, 0.1)",
                width: 20,
                height: 20,
                borderRadius: 10,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
              onClick={() => {
                let listS1Tmp = [...listS1];
                let listS1ShowTmp = [...listS1Show];

                const idxS1Remove = listS1Tmp.findIndex((itemS1) => {
                  return itemS1.dms_code === item.dms_code;
                });

                listS1ShowTmp.splice(index, 1);
                setListS1Show([...listS1ShowTmp]);
                setListS1Clone([...listS1ShowTmp]);

                listS1Tmp[idxS1Remove].checked = false;
                setListS1(listS1Tmp);
              }}
              className="cursor"
            >
              <ISvg
                name={ISvg.NAME.CROSS}
                width={8}
                height={8}
                fill={colors.oranges}
              />
            </div>
          </td>
        </tr>
      ))
    );
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Lựa chọn đại lý
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={12}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <Row gutter={[15, 0]}>
                        <Col span={17}>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              width: "100%",
                            }}
                          >
                            <StyledSearchCustomNew
                              style={{ padding: 0 }}
                              onPressEnter={(e) => {
                                setLoadingList(true);
                                setfilterList({
                                  ...filterList,
                                  keySearch: e.target.value,
                                });
                              }}
                              placeholder="Tìm kiếm theo mã, tên, SĐT ĐLC1"
                            />
                          </div>
                        </Col>
                        <Col span={7}>
                          <ISelect
                            defaultValue="Cấp bậc"
                            data={listMembership}
                            select={true}
                            onChange={(value) => {
                              setLoadingList(true);
                              setfilterList({
                                ...filterList,
                                membership_id: value,
                              });
                            }}
                          />
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                        }}
                      >
                        <Row>
                          <Col span={24}>
                            <Row
                              style={{
                                background: colors.green._3,
                                borderBottom:
                                  "1px solid rgba(122, 123, 123, 0.5)",
                              }}
                            >
                              <Col span={2}></Col>
                              <Col span={7}>
                                <div
                                  style={{
                                    fontWeight: 600,
                                    textAlign: "left",
                                    color: colors.blackChart,
                                    padding: "8px 0px",
                                  }}
                                >
                                  Mã đại lý
                                </div>
                              </Col>
                              <Col span={10}>
                                <div
                                  style={{
                                    fontWeight: 600,
                                    textAlign: "left",
                                    color: colors.blackChart,
                                    padding: "8px 0px",
                                  }}
                                >
                                  Tên đại lý
                                </div>
                              </Col>
                              <Col span={5}>
                                <div
                                  style={{
                                    fontWeight: 600,
                                    textAlign: "left",
                                    color: colors.blackChart,
                                    padding: "8px 0px",
                                  }}
                                >
                                  Cấp bậc
                                </div>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={24}>
                            <div style={{ height: 342, overflow: "auto" }}>
                              <List
                                dataSource={listS1}
                                bordered={false}
                                loading={loadingList}
                                renderItem={(item, index) => {
                                  return (
                                    <List.Item>
                                      <div
                                        style={{
                                          width: "100%",
                                          height: "100%",
                                        }}
                                      >
                                        <Row>
                                          <Col span={2}>
                                            <div
                                              style={{ textAlign: "center" }}
                                            >
                                              <Checkbox
                                                defaultChecked={item.checked}
                                                checked={item.checked}
                                                onChange={(e) => {
                                                  let checked =
                                                    e.target.checked;

                                                  let listS1Tmp = [...listS1];
                                                  listS1Tmp[index].checked =
                                                    checked;
                                                  setListS1(listS1Tmp);
                                                  let listS1CloneTmp = [
                                                    ...listS1Clone,
                                                  ];
                                                  if (checked) {
                                                    listS1CloneTmp.push(
                                                      listS1Tmp[index]
                                                    );
                                                    setListS1Clone([
                                                      ...listS1CloneTmp,
                                                    ]);
                                                  } else {
                                                    const idx =
                                                      listS1CloneTmp.findIndex(
                                                        (item) =>
                                                          item.dms_code ===
                                                          listS1Tmp[index]
                                                            .dms_code
                                                      );
                                                    listS1CloneTmp.splice(
                                                      idx,
                                                      1
                                                    );
                                                    setListS1Clone([
                                                      ...listS1CloneTmp,
                                                    ]);
                                                  }
                                                }}
                                              ></Checkbox>
                                            </div>
                                          </Col>
                                          <Col span={7}>
                                            <div
                                              style={{
                                                textAlign: "left",
                                                color: colors.blackChart,
                                                padding: "0px 5px",
                                              }}
                                            >
                                              {item.dms_code}
                                            </div>
                                          </Col>
                                          <Col span={10}>
                                            <div
                                              style={{
                                                textAlign: "left",
                                                color: colors.blackChart,
                                                padding: "0px 5px",
                                              }}
                                            >
                                              {item.shop_name}
                                            </div>
                                          </Col>
                                          <Col span={5}>
                                            <div
                                              style={{
                                                textAlign: "left",
                                                color: colors.blackChart,
                                                padding: "0px 5px",
                                              }}
                                            >
                                              {item.membership}
                                            </div>
                                          </Col>
                                        </Row>
                                      </div>
                                    </List.Item>
                                  );
                                }}
                              />
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            if (listS1Clone.length === 0) {
                              message.error("Vui lòng chọn đại lý cấp 1");
                              return;
                            }

                            setListS1Show([...listS1Clone]);
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={12}>
                <div
                  style={{
                    height: 490,
                    borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                    borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <ITableHtml
                    style={{ maxHeight: 489 }}
                    childrenBody={bodyTableProduct(listS1Show)}
                    childrenHeader={headerTableProduct(headerTable)}
                    isBorder={false}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24} style={{ padding: 0 }}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 30,
              right: -25,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 15,
              }}
              onClick={() => {
                callbackSave(listS1Show);
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                let listS1Tmp = [...listS1];
                let listS1New = listS1Tmp.map((item) => {
                  const idx = listS1Confirm.findIndex(
                    (itemS1Confirm) => itemS1Confirm.id === item.id
                  );

                  if (idx === -1) {
                    item.checked = false;
                  } else {
                    item.checked = true;
                  }

                  return item;
                });
                setListS1Show([...listS1Confirm]);
                setListS1Clone([...listS1Confirm]);
                setListS1([...listS1New]);
                callbackCancel();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
