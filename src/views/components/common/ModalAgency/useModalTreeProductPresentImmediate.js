import { Col, InputNumber, Row } from 'antd'
import React, { useEffect, useState } from 'react'

import { colors } from '../../../../assets'
import IButton from '../../IButton'
import ISvg from '../../ISvg'
import { APIService } from '../../../../services'
import { useDispatch, useSelector } from 'react-redux'
import {
  SAVE_TREE,
  ADD_TREE,
  SHOW_TREE,
  ASSIGNED_KEY,
  CREATE_DATA_PRODUCT,
  UPDATE_DATA_PRODUCT,
  REMOVE_TREE
} from '../../../store/reducers'
import ITree from '../Tree/ITree'
import { StyledITileHeading } from '../Font/font'
import { priceFormat } from '../../../../utils'
import { DefineKeyEvent } from '../../../../utils/DefineKey'

export default function useModalTreeProductPresentImmediate (
  callbackClose = () => {},
  id,
  title
) {
  const dispatch = useDispatch()
  const dataRoot = useSelector(state => state)
  const {
    dataProductPresent,
    treeProductPresentIImmediate = {
      listTreeClone: [],
      listTreeShow: [],
      listConfirm: []
    },
    eventImmediate
  } = dataRoot
  const [loadingList, setLoadingList] = useState(true)

  const _getListTreeProduct = async id => {
    try {
      const data = await APIService._getListTreeProduct(id)
      data.cate.lstCate.forEach(item => {
        item.children.map(item1 => {
          item1.quantity_sale = 0
          item1.quantity_specifications = 0
          item1.percent_sale = 0
          item1.quantity_product_present = 1
        })
      })
      const keyDispatch = {
        key: 'dataProductPresent',
        value: data.cate.lstCate
      }
      dispatch(CREATE_DATA_PRODUCT(keyDispatch))
    } catch (error) {
      console.log(error)
      setLoadingList(false)
    }
  }

  useEffect(() => {
    if (dataProductPresent.length === 0) {
      _getListTreeProduct(id)
    }
  }, [id])
  const treeProct = (dataProduct, idx) => {
    let keyRootTemp = undefined
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            keyRootTemp = item.id
            return (
              <ul
                style={{
                  listStyleType: 'none',
                  padding: '6px 0px'
                }}
              >
                <li
                  style={{
                    fontWeight: 'bold',
                    borderBottom: '0.5px dashed #7A7B7B',
                    padding: '6px 6px'
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children)}
              </ul>
            )
          } else {
            return (
              <ul
                style={{
                  listStyleType: 'none',
                  borderBottom: '0.5px dashed #7A7B7B'
                }}
              >
                <li style={{ padding: '6px 0px' }}>
                  <Row gutter={[12, 0]} type='flex'>
                    <Col
                      span={
                        eventImmediate.event_degsin.key_reward ===
                        DefineKeyEvent.event_discount_present
                          ? 13
                          : 21
                      }
                    >
                      <div
                        style={{
                          wordWrap: 'break-word',
                          wordBreak: 'break-word',
                          overflow: 'hidden',
                          height: '100%',
                          display: 'flex',
                          alignItems: 'center'
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    {eventImmediate.event_degsin.key_reward ===
                    DefineKeyEvent.event_discount_present ? (
                      <Col span={8}>
                        <div
                          style={{
                            wordWrap: 'break-word',
                            wordBreak: 'break-word',
                            overflow: 'hidden',
                            height: '100%',
                            display: 'flex',
                            alignItems: 'center',
                            textAlign: 'center'
                          }}
                        >
                          <span style={{ textAlign: 'center' }}>
                            {priceFormat(item.priceMin) + 'đ'}
                          </span>
                        </div>
                      </Col>
                    ) : null}

                    <Col span={3}>
                      <div
                        style={{
                          height: 32,
                          display: 'flex',
                          alignItems: 'center'
                        }}
                      >
                        <div
                          style={{
                            background: 'rgba(227, 95, 75, 0.1)',
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center'
                          }}
                          onClick={() => {
                            const dataRemovePresentShow = {
                              key: 'listTreeShow',
                              keyRoot: 'treeProductPresentIImmediate',
                              keyObject: 'children',
                              keyValue: 'quantity_product_present',
                              keyLocationRoot: item.locationRoot,
                              idx: index
                            }

                            const dataRemovePresentClone = {
                              key: 'listTreeClone',
                              keyRoot: 'treeProductPresentIImmediate',
                              keyObject: 'children',
                              keyValue: 'quantity_product_present',
                              keyLocationRoot: item.locationRoot,
                              idx: index
                            }
                            const dataAssigned = {
                              keyRoot: 'treeProductPresentIImmediate',
                              keyTree: 'listTreeShow',
                              keyListID: 'listKeyClone'
                            }

                            const dataProductPercentQuantity = {
                              keyRoot: 'dataProductPresent',
                              keyObject: 'children',
                              keyValue: 'quantity_product_present',
                              keyLocationRoot: item.locationRootReal,
                              value: 0,
                              idx: item.locationChildrenReal
                            }
                            dispatch(
                              UPDATE_DATA_PRODUCT(dataProductPercentQuantity)
                            )
                            dispatch(REMOVE_TREE(dataRemovePresentShow))
                            dispatch(REMOVE_TREE(dataRemovePresentClone))
                            dispatch(ASSIGNED_KEY(dataAssigned))
                          }}
                          className='cursor'
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill={colors.oranges}
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            )
          }
        })}
      </ul>
    )
  }

  return (
    <div style={{ width: '100%', height: '100%' }}>
      <div style={{ marginBottom: 24 }}>
        <StyledITileHeading minFont='10px' maxFont='16px'>
          {title}
        </StyledITileHeading>
      </div>

      <Row gutter={[0, 16]}>
        <Col span={24}>
          <div style={{ width: '100%', height: '100%' }}>
            <Row gutter={[26, 0]}>
              <Col span={12}>
                <div style={{ width: '100%', height: '100%' }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <div
                        style={{
                          height: 455,
                          overflow: 'auto',
                          border: '1px solid rgba(122, 123, 123, 0.5)',
                          paddingLeft: 18,
                          paddingRight: 18
                        }}
                      >
                        <ITree
                          checkable
                          checkedKeys={
                            treeProductPresentIImmediate.listKeyClone
                          }
                          dataLoop={dataProductPresent}
                          defaultExpandAll
                          loadingList
                          // blockNode={true}
                          // checkStrictly={true}
                          onCheck={(checkedKeys, e) => {
                            let idxRoot = 0
                            const dataTree = dataProductPresent.map(
                              (item, index) => {
                                let keyRoot
                                let titleRoot
                                const children = e.checkedNodes.map(item1 => {
                                  let splitKey = item1.key.split('-')
                                  if (splitKey.length === 2) {
                                    if (
                                      Number(splitKey[0]) === Number(item.id)
                                    ) {
                                      keyRoot = item.id
                                      titleRoot = item.name
                                      const priceMin =
                                        item1.props.dataProps.price_max_unit
                                      const locationRootReal =
                                        item1.props.locationRoot
                                      const locationChildrenReal =
                                        item1.props.locationChildren

                                      const id = item1.key
                                      const name = item1.props.title
                                      const quantity_sale =
                                        item1.props.dataProps.quantity_sale
                                      const percent_sale =
                                        item1.props.dataProps.percent_sale
                                      const quantity_product_present =
                                        item1.props.dataProps
                                          .quantity_product_present
                                      const quantity_specifications =
                                        item1.props.dataProps
                                          .quantity_specifications
                                      const sale_type =
                                        item1.props.dataProps.sale_type
                                      return {
                                        id,
                                        name,
                                        quantity_sale,
                                        percent_sale,
                                        priceMin,
                                        locationRootReal,
                                        quantity_specifications,
                                        locationChildrenReal,
                                        quantity_product_present,
                                        sale_type
                                      }
                                    }
                                  }
                                  return
                                })
                                if (
                                  typeof keyRoot === 'undefined' ||
                                  typeof titleRoot === 'undefined'
                                ) {
                                  return
                                }
                                const id = keyRoot
                                const name = titleRoot

                                return { id, name, children }
                              }
                            )

                            let dataTreeSucces = dataTree.filter(function (
                              element
                            ) {
                              return element !== undefined
                            })
                            dataTreeSucces.map(item => {
                              item.children = item.children.filter(item1 => {
                                return item1 !== undefined
                              })
                            })
                            dataTreeSucces.map((item, index) => {
                              item.children.map((item1, index1) => {
                                item1.locationRoot = index
                                item1.locationChildren = index1
                              })
                            })
                            let dataKey = []
                            dataTreeSucces.map(item => {
                              item.children.map(item1 => {
                                dataKey.push(item1.id)
                              })
                            })
                            const dataAction = {
                              keyRoot: 'treeProductPresentIImmediate',
                              initial_listClone: 'listTreeClone',
                              data: dataTreeSucces
                            }
                            const dataActionKey = {
                              keyRoot: 'treeProductPresentIImmediate',
                              initial_listClone: 'listKeyClone',
                              data: dataKey
                            }
                            dispatch(ADD_TREE(dataAction))
                            dispatch(ADD_TREE(dataActionKey))
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: '100%',
                          display: 'flex',
                          justifyContent: 'flex-end'
                        }}
                      >
                        <IButton
                          title='Thêm'
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140
                          }}
                          isRight={true}
                          onClick={() => {
                            const dataAction = {
                              keyRoot: 'treeProductPresentIImmediate',
                              initial_listClone: 'listTreeClone',
                              initial_listShow: 'listTreeShow'
                            }

                            dispatch(SHOW_TREE(dataAction))
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={12}>
                <div
                  style={{
                    height: 510,
                    border: '1px solid rgba(122, 123, 123, 0.5)',
                    overflowY: 'auto',
                    overflowX: 'hidden'
                  }}
                >
                  <div
                    style={{
                      padding: '12px',
                      borderBottom: '0.5px solid #d9d9d9'
                    }}
                  >
                    <Row gutter={[12, 0]} type='flex'>
                      <Col span={13}>
                        <div
                          style={{
                            height: '100%',
                            display: 'flex',
                            alignItems: 'center'
                          }}
                        >
                          <StyledITileHeading minFont='14px' maxFont='16px'>
                            Nhóm sản phẩm
                          </StyledITileHeading>
                        </div>
                      </Col>
                      {eventImmediate.event_degsin.key_reward ===
                      DefineKeyEvent.event_discount_present ? (
                        <Col span={8}>
                          <div
                            style={{
                              height: '100%',
                              display: 'flex',
                              alignItems: 'center'
                            }}
                          >
                            <StyledITileHeading
                              minFont='10px'
                              maxFont='14px'
                              style={{ textAlign: 'center' }}
                            >
                              Đơn giá <br /> Quy cách nhỏ nhất
                            </StyledITileHeading>
                          </div>
                        </Col>
                      ) : null}
                    </Row>
                  </div>
                  <div style={{ padding: '0px 12px' }}>
                    {treeProct(treeProductPresentIImmediate.listTreeShow)}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              position: 'absolute',
              top: 60,
              right: 0
            }}
          >
            <IButton
              title='Lưu'
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 24
              }}
              onClick={() => {
                const dataAction = {
                  keyRoot: 'treeProductPresentIImmediate',
                  initial_listShow: 'listTreeShow',
                  initial_listConfirm: 'listConfirm',
                  assigned_condition: false
                }

                dispatch(SAVE_TREE(dataAction))

                callbackClose()
              }}
            />
            <IButton
              title='Hủy bỏ'
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140
              }}
              onClick={() => {
                // dispatch(
                //   CANCEL_MODAL({
                //     keyInitial_root: "modalS2",
                //     keyInitial_listShow: "listS2Show",
                //     keyInitial_Add: "listS2Add",
                //     keyInitial_listAddClone: "listS2AddClone",
                //     keyInitial_listClone: "listS2Clone",
                //     keyInitial_list: "listS2",
                //   })
                // );
                // const dataAction = {
                //   keyRoot: "treeProduct",
                //   initial_listClone: "listTreeClone",
                // };
                // dispatch(CANCEL_TREE(dataAction));

                callbackClose()
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  )
}
