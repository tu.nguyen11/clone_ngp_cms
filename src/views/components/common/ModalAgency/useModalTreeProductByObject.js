import { Col, Row, Spin, Input } from "antd";
import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { colors } from "../../../../assets";
import ISvg from "../../ISvg";

import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import {
  SAVE_TREE_NEW,
  ADD_TREE,
  SHOW_TREE,
  REMOVE_PRODUCT_BY_OBJECT,
  CREATE_DATA_PRODUCT,
  CANCEL_TREE_NEW,
  ASSIGN_LIST_KEY_CLONE,
  ADD_TREE_KEY_CLONE,
  CREATE_PRODUCT_BY_OBJECT_INFO,
} from "../../../store/reducers";
import { StyledITileHeading } from "../Font/font";
import IButton from "../../IButton";
import ITreeNew from "../Tree/ITreeNew";

const { Search } = Input;

const StyledSearchCustomNew = styled(Search)`
  height: 42px;

  width: 100%;
  border: 0px !important;

  .ant-input:focus {
    box-shadow: none !important;
  }

  .ant-input-affix-wrapper .ant-input {
    padding-left: 0px !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
  .ant-input-search: not(.ant-input-search-enter-button) {
    padding-right: 6px !important;
  }
  .ant-input-suffix {
    font-size: 20px;
  }

  .ant-input {
    padding-left: 0 !important;
  }
`;

export default function useModalTreeProductByObject(callbackClose = () => {}) {
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { treeProduct, dataProduct, productsByObject } = dataRoot;
  const [loadingList, setLoadingList] = useState(false);
  const [listUnCheck, setListUnCheck] = useState([]);
  const [search, setSearch] = useState("");

  const getListTreeProduct = async (search) => {
    try {
      const data = await APIService._getListTreeProductEventGift(search, 0);

      let listKeyCloneNew = [];

      treeProduct.listKeyClone.forEach((item) => {
        let ids = item.split("-");
        if (ids.length === 3) {
          listKeyCloneNew.push(item);
        }
      });

      const dataAction = {
        keyRoot: "treeProduct",
        key: "listKeyClone",
        value: listKeyCloneNew,
      };

      dispatch(ASSIGN_LIST_KEY_CLONE(dataAction));

      const keyDispatch = {
        key: "dataProduct",
        value: data.product_type,
      };
      dispatch(CREATE_DATA_PRODUCT(keyDispatch));
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    getListTreeProduct(search);
  }, [search]);

  const treeProct = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={22}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 40,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    <Col span={2}>
                      <div
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "flex-end",
                          paddingRight: 10,
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            const dataAction = {
                              keyRoot: "treeProduct",
                              key_listTreeClone: "listTreeClone",
                              key_listTreeShow: "listTreeShow",
                              key_listKeyClone: "listKeyClone",
                              indexProduct: index,
                              product: item,
                              listTreeClone: treeProduct.listTreeClone,
                              listTreeShow: treeProduct.listTreeShow,
                              listKeyClone: treeProduct.listKeyClone,
                            };

                            dispatch(REMOVE_PRODUCT_BY_OBJECT(dataAction));
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill="#E35F4B"
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Chọn phiên bản
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={10}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <StyledSearchCustomNew
                          style={{ width: 465, padding: 0 }}
                          onPressEnter={(e) => {
                            setLoadingList(true);
                            setSearch(e.target.value);
                          }}
                          placeholder="Tìm kiếm theo mã, tên phiên bản"
                        />
                      </div>
                    </Col>

                    <Col span={24}>
                      <div
                        style={{
                          height: 490,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          // paddingLeft: 10,
                          paddingRight: 10,
                        }}
                      >
                        {loadingList ? (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              height: "100%",
                            }}
                          >
                            <Spin />
                          </div>
                        ) : (
                          <ITreeNew
                            checkable
                            checkedKeys={treeProduct.listKeyClone}
                            dataLoop={dataProduct}
                            defaultExpandAll
                            loadingList
                            onCheck={(checkedKeys, e) => {
                              console.log("checkedKeys: ", checkedKeys);
                              const tree = [];
                              let arrUnCheck = [];

                              if (e.checked === false) {
                                const id = e.node.props.eventKey;
                                const arrId = id.split("-");
                                switch (arrId.length) {
                                  case 1:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          item.children.forEach((item2) => {
                                            arrUnCheck.push(item2.tree_id);
                                          });
                                        }
                                      );
                                    }
                                    break;
                                  case 2:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          arrUnCheck.push(item.tree_id);
                                        }
                                      );
                                    }
                                    break;
                                  case 3:
                                    {
                                      arrUnCheck.push(
                                        e.node.props.dataProps.tree_id
                                      );
                                    }
                                    break;
                                  default:
                                    break;
                                }

                                setListUnCheck(arrUnCheck);
                              }
                              checkedKeys.forEach((key, _, itself) => {
                                const keys = key.split("-");
                                const [id1, id2, id3] = keys;
                                const selectedItem1 = dataProduct.find(
                                  (item) => item.id.toString() === id1
                                );

                                const selectedItem2 =
                                  selectedItem1 &&
                                  selectedItem1.children.find(
                                    (item) => item.id.toString() === id2
                                  );

                                const selectedItem3 =
                                  selectedItem2 &&
                                  selectedItem2.children.find(
                                    (item) => item.id.toString() === id3
                                  );

                                switch (keys.length) {
                                  case 1: {
                                    if (selectedItem1) {
                                      tree.push(selectedItem1);
                                    }
                                    break;
                                  }

                                  case 2: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );
                                    if (!selectedItem1InShow) {
                                      tree.unshift({
                                        ...selectedItem1,
                                        children: [selectedItem2],
                                      });
                                    } else {
                                      selectedItem1InShow.children.unshift(
                                        selectedItem2
                                      );
                                    }
                                    break;
                                  }
                                  case 3: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    if (
                                      itself.find(
                                        (item) => item === [id1, id2].join("-")
                                      )
                                    )
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );

                                    if (!selectedItem1InShow) {
                                      selectedItem1InShow = {
                                        ...selectedItem1,
                                        children: [
                                          { ...selectedItem2, children: [] },
                                        ],
                                      };
                                      tree.push(selectedItem1InShow);
                                    }

                                    const selectedItem2InShow = selectedItem1InShow.children.find(
                                      (item) => item.id.toString() === id2
                                    );

                                    if (!selectedItem2InShow) {
                                      selectedItem1InShow.children.push({
                                        ...selectedItem2,
                                        children: [selectedItem3],
                                      });
                                    } else {
                                      selectedItem2InShow.children.push(
                                        selectedItem3
                                      );
                                    }
                                  }
                                  default:
                                    break;
                                }
                              });

                              const dataActionKey = {
                                keyRoot: "treeProduct",
                                initial_listClone: "listKeyClone",
                                data:
                                  e.checked === true
                                    ? { checked: true, arr: [...checkedKeys] }
                                    : { checked: false, arr: [...arrUnCheck] },
                              };

                              const dataAction = {
                                keyRoot: "treeProduct",
                                initial_listClone: "listTreeClone",
                                data: {
                                  checked: e.checked,
                                  tree: tree,
                                },
                              };

                              dispatch(ADD_TREE(dataAction));
                              dispatch(ADD_TREE_KEY_CLONE(dataActionKey));
                            }}
                          />
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.ARROWRIGHT}
                          style={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            const dataAction = {
                              keyRoot: "treeProduct",
                              initial_listClone: "listTreeClone",
                              initial_listShow: "listTreeShow",
                            };

                            dispatch(SHOW_TREE(dataAction));
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={14}>
                <Row>
                  <Col span={24}>
                    <Row
                      style={{
                        borderTop: "1px solid rgba(122, 123, 123, 0.5)",
                        borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                        borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                        padding: "9px 6px",
                        color: "#000",
                        fontWeight: 600,
                      }}
                    >
                      <Col span={22}>
                        <div>Phiên bản</div>
                      </Col>
                      <Col span={2}></Col>
                    </Row>
                  </Col>
                  <Col span={24}>
                    <div
                      style={{
                        height: 555,
                        border: "1px solid rgba(122, 123, 123, 0.5)",
                        overflowY: "auto",
                        overflowX: "hidden",
                      }}
                    >
                      {treeProduct.listTreeShow.length > 0
                        ? treeProct(treeProduct.listTreeShow)
                        : null}
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 60,
              right: -25,
            }}
          >
            <div style={{ display: "flex" }}>
              <IButton
                title="Lưu"
                color={colors.main}
                icon={ISvg.NAME.SAVE}
                styleHeight={{
                  width: 140,
                }}
                onClick={() => {
                  let arrProductConfirm = [];
                  let arrProduct = JSON.parse(
                    JSON.stringify(productsByObject.arrProduct)
                  );

                  treeProduct.listTreeShow.forEach((itemLv1) => {
                    itemLv1.children.forEach((itemLv2) => {
                      itemLv2.children.forEach((itemLv3) => {
                        let productExist = arrProduct.find(
                          (item) => item.id === itemLv3.id
                        );
                        if (productExist) {
                          arrProductConfirm.push({
                            ...itemLv3,
                            priceByObject: productExist.priceByObject,
                            checkPriceByObject: productExist.checkPriceByObject,
                          });
                        } else {
                          arrProductConfirm.push({
                            ...itemLv3,
                            priceByObject: itemLv3.price,
                            checkPriceByObject: false,
                          });
                        }
                      });
                    });
                  });

                  const dataAction = {
                    keyRoot: "treeProduct",
                    initial_listShow: "listTreeShow",
                    initial_listConfirm: "listConfirm",
                    initial_listKeyConfirm: "listKeyConfirm",
                    dataListKeyConfirm: treeProduct.listKeyClone,
                    assigned_condition: true,
                  };

                  dispatch(SAVE_TREE_NEW(dataAction));

                  const dataAction1 = {
                    key: "arrProduct",
                    value: arrProductConfirm,
                  };

                  dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(dataAction1));

                  callbackClose();
                }}
              />
              <IButton
                title="Hủy bỏ"
                color={colors.oranges}
                icon={ISvg.NAME.CROSS}
                styleHeight={{
                  width: 140,
                  marginLeft: 15,
                }}
                onClick={() => {
                  const dataAction = {
                    keyRoot: "treeProduct",
                    initial_listClone: "listTreeClone",
                    initial_listShow: "listTreeShow",
                    dataListConfirm: treeProduct.listConfirm,
                    initial_listKeyClone: "listKeyClone",
                    dataListKeyConfirm: treeProduct.listKeyConfirm,
                  };

                  dispatch(CANCEL_TREE_NEW(dataAction));
                  callbackClose();
                }}
              />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}
