import { Checkbox, Col, Icon, List, message, Row } from "antd";
import React, { useEffect, useState } from "react";
import { StyledITileHeading } from "../Font/font";
import { StyledSearchCustom } from "..";
import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import ITableHtml from "../../ITableHtml";
import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import {
  ADD_MODAL,
  DEAFUAT_MODAL,
  REMOVE_MODAL,
  SAVE_MODAL,
  UPDATE_ACTIVELIST_MODAL,
  UPDATE_ACTIVELISTAGENCY_MODAL,
  CONFIRM_MODAL,
  CANCEL_MODAL,
} from "../../../store/reducers";

export default function useModalAgencyS2(
  callbackClose = () => {},
  historyPush = () => {}
) {
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { modalS2 } = dataRoot;
  const [filterList, setfilterList] = useState({
    status: 2,
    page: 1,
    city_id: 0,
    supplier_id: 0,
    keySearch: "",
  });
  const [loadingList, setLoadingList] = useState(true);
  const _getAPIListAgencyS1 = async () => {
    try {
      const data = await APIService._getListSuppliers(
        filterList.city_id,
        filterList.supplier_id,
        filterList.keySearch,
        filterList.page,
        filterList.status
      );
      let suppliers = [...data.suppliers];
      suppliers.map((item, index) => {
        suppliers[index].stt = (filterList.page - 1) * 10 + index + 1;
        suppliers[index].checked = false;
      });
      dispatch(
        DEAFUAT_MODAL({
          data: suppliers,
          keyInitial_listShow: "listS2Show",
          keyInitial_listAddClone: "listS2AddClone",
          keyInitial_listBefore: "listS2Before",
          keyInitial_list: "listS2",
          keyInitial_listClone: "listS2Clone",
          keyInitial_root: "modalS2",
        })
      );

      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    _getAPIListAgencyS1(filterList);
  }, [filterList]);

  const headerTable = [
    {
      name: "STT",
      align: "center",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Địa chỉ",
      align: "left",
    },
    {
      name: "ĐL Cấp 1 trực thuộc",
      align: "left",
    },
    {
      name: "Phòng kinh doanh",
      align: "left",
    },
    {
      name: "",
      align: "center",
    },
  ];

  const headerTableProduct = (headerTable) => {
    return (
      <tr>
        {headerTable.map((item, index) => (
          <th className={"th-table"} style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{
            textAlign: "center",
            fontWeight: 400,
            fontSize: "clamp(8px, 4vw, 14px)",
          }}
        >
          {index + 1}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.supplierCode}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.supplierName}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.address}, {item.districtsName}, {item.cityName}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.supplierNameFull}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.agencyName}
        </td>
        <td className="td-table">
          <div
            style={{
              background: "rgba(227, 95, 75, 0.1)",
              width: 20,
              height: 20,
              borderRadius: 10,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            onClick={() => {
              dispatch(
                REMOVE_MODAL({
                  idx: index,
                  keyInitial_root: "modalS2",
                  keyInitial_listShow: "listS2Show",
                  keyInitial_listAddClone: "listS2AddClone",
                })
              );
              dispatch(
                UPDATE_ACTIVELISTAGENCY_MODAL({
                  id: item.id,
                  status: false,
                  keyInitial_root: "modalS2",
                  keyInitial_list: "listS2",
                })
              );
            }}
            className="cursor"
          >
            <ISvg
              name={ISvg.NAME.CROSS}
              width={8}
              height={8}
              fill={colors.oranges}
            />
          </div>
        </td>
      </tr>
    ));
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Chọn Đại lý cấp 2 chỉ định áp dụng chính sách
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={7}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <Icon
                          type="search"
                          style={{
                            fontSize: 20,
                            color: colors.icon,
                          }}
                        />
                        <StyledSearchCustom
                          style={{ width: 400 }}
                          suffix={() => {
                            return null;
                          }}
                          onPressEnter={(e) => {
                            setLoadingList(true);
                            setfilterList({
                              ...filterList,
                              keySearch: e.target.value,
                            });
                          }}
                          placeholder="nhập kí tự tìm kiếm"
                          prefix={() => {
                            return null;
                          }}
                        />
                      </div>
                    </Col>

                    <Col span={24}>
                      <div
                        style={{
                          height: 400,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        <List
                          dataSource={modalS2.listS2}
                          bordered={false}
                          loading={loadingList}
                          renderItem={(item, index) => {
                            return (
                              <List.Item>
                                <div style={{ width: "100%", height: "100%" }}>
                                  <Row>
                                    <Col span={3}>
                                      <Checkbox
                                        defaultChecked={item.checked}
                                        checked={item.checked}
                                        onChange={(e) => {
                                          let checked = e.target.checked;
                                          dispatch(
                                            ADD_MODAL({
                                              keyInitial_Add: "listS2Add",
                                              keyInitial_AddClone:
                                                "listS2AddClone",
                                              data: item,
                                              status: checked,
                                              keyInitial_root: "modalS2",
                                            })
                                          );
                                          dispatch(
                                            UPDATE_ACTIVELIST_MODAL({
                                              keyInitial_root: "modalS2",
                                              keyInitial_list: "listS2",
                                              status: checked,
                                              id: item.id,
                                            })
                                          );
                                        }}
                                      />
                                    </Col>
                                    <Col span={21}>
                                      <span>{item.supplierName}</span>
                                    </Col>
                                  </Row>
                                </div>
                              </List.Item>
                            );
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            if (modalS2.listS2AddClone.length === 0) {
                              message.error("Chưa chọn đại lý cấp 2");
                              return;
                            }
                            dispatch(
                              SAVE_MODAL({
                                keyInitial_root: "modalS2",
                                keyInitial_listShow: "listS2Show",
                                keyInitial_listAddClone: "listS2AddClone",
                              })
                            );
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={17}>
                <div
                  style={{
                    height: 510,
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <ITableHtml
                    childrenBody={bodyTableProduct(modalS2.listS2Show)}
                    childrenHeader={headerTableProduct(headerTable)}
                    isBorder={false}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 60,
              right: 0,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 24,
              }}
              onClick={() => {
                dispatch(
                  CONFIRM_MODAL({
                    keyInitial_root: "modalS2",
                    keyInitial_listShow: "listS2Show",
                    keyInitial_Add: "listS2Add",
                  })
                );
                callbackClose();
                historyPush();
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                dispatch(
                  CANCEL_MODAL({
                    keyInitial_root: "modalS2",
                    keyInitial_listShow: "listS2Show",
                    keyInitial_Add: "listS2Add",
                    keyInitial_listAddClone: "listS2AddClone",
                    keyInitial_listClone: "listS2Clone",
                    keyInitial_list: "listS2",
                  })
                );
                callbackClose();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
