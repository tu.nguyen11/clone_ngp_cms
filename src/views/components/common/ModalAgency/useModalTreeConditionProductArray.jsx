import { Col, message, Row, Spin } from 'antd';
import React, { useEffect, useState } from 'react';
import { colors } from '../../../../assets';
import IButton from '../../IButton';
import ISvg from '../../ISvg';
import { APIService } from '../../../../services';
import { useDispatch, useSelector } from 'react-redux';
import {
	SAVE_TREE,
	ADD_TREE,
	SHOW_TREE,
	CANCEL_TREE,
	REMOVE_TREE,
	ASSIGNED_KEY,
	CREATE_DATA_PRODUCT,
	CLOSE_MODAL_COMBO_CONDITION,
	SAVE_TREE_GROUP_COMBO_CONDITION,
	SAVE_TREE_GROUP_COMBO_OVER_CONDITION,
	ASSIGN_LIST_KEY_CLONE,
	ADD_TREE_KEY_CLONE
} from '../../../store/reducers';
import ITreeNew from "../Tree/ITreeNew";
import { IError } from '../Error/IError';
import StyledSearchCustomNew from "./useModalTreeProductImmediate"
import useWindowSize from "../../../../utils/useWindowSize";


export default function useModalTreeConditionProductArray(
	callbackClose = () => {},
	id,
	checkGroup,
	count_save,
	idxRoot,
	type_product = 1
) {
	let size = useWindowSize();
	const dispatch = useDispatch();
	const dataRoot = useSelector((state) => state);
	const { dataArrayCondition, dataProduct } = dataRoot;

	const treeProductRedux = checkGroup
		? dataArrayCondition[idxRoot].treeProductConditionGroup
		: dataArrayCondition[idxRoot].treeProductCondition;
	const acitonRedux = checkGroup ? `treeProductConditionGroup` : `treeProductCondition`;

	const [loadingList, setLoadingList] = useState(true);
	const [search, setSearch] = useState("")

	  const getListTreeProduct = async (search, type_product) => {
    try {
      setLoadingList(true);
      const data = await APIService._getListTreeProductEventGift(
        search,
        type_product
      );
      let listKeyCloneNew = [];

      treeProductRedux.listKeyClone.forEach((item) => {
        let ids = item.split("-");
        if (ids.length === 3) {
          listKeyCloneNew.push(item);
        }
      });

      const dataAction = {
        keyRoot: "treeProductCondition",
        key: "listKeyClone",
        value: listKeyCloneNew,
      };

      dispatch(ASSIGN_LIST_KEY_CLONE(dataAction));

      let tree = data.product_type.map((itemLv1) => {
        let arrLv1Child = itemLv1.children.map((itemLv2) => {
          let arrLv2Child = itemLv2.children.map((itemLv3) => {
            if (!itemLv3.content) {
              itemLv3.description = "";
              return itemLv3;
            } else {
              return itemLv3;
            }
          });
          return {
            ...itemLv2,
            children: [...arrLv2Child],
          };
        });
        return {
          ...itemLv1,
          children: [...arrLv1Child],
        };
      });

      const keyDispatch = {
        key: "dataProduct",
        value: tree,
      };

      dispatch(CREATE_DATA_PRODUCT(keyDispatch));
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    getListTreeProduct(search, type_product);
  }, [search, type_product]);

  const treeProct = (dataProduct, idxRoot) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children, idxRoot)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={24}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 30,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    {/* <Col span={24}>
                      <div style={{ paddingLeft: 30 }}>
                        <IInputTextArea
                          style={{
                            minHeight: 35,
                            height: 35,
                            padding: 6,
                            marginTop: 8,
                            border: "none",
                          }}
                          placeholder="Mô tả chính sách bán hàng của sản phẩm"
                          value={item.description}
                          onChange={(e) => {
                            const data = {
                              key: "description",
                              tree:
                                eventGift.event_design.key_type_product === 1
                                  ? treeProductEventGiftGroup.listArrListTreeConfirm
                                  : treeProductEventGiftGroup2.listArrListTreeConfirm,
                              value: e.target.value,
                              idxRoot: idxRoot,
                              id: item.id,
                              keyTypeProduct:
                                eventGift.event_design.key_type_product,
                            };
                            dispatch(ADD_CONTENT_PRODUCT(data));
                          }}
                        />
                      </div>
                    </Col> */}
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };


	const treeProctGroup = (dataProduct, idx) => {
		return (
			<ul style={{ padding: 0 }}>
				{dataProduct.map((item, index) => {
					if (item.children && item.children.length) {
						return (
							<ul
								style={{
									listStyleType: 'none',
									padding: '6px 0px',
								}}
							>
								<li
									style={{
										fontWeight: 'bold',
										borderBottom: '0.5px dashed #7A7B7B',
										padding: '6px 6px',
									}}
								>
									{item.name}
								</li>
								{treeProct(item.children)}
							</ul>
						);
					} else {
						return (
							<ul
								style={{
									listStyleType: 'none',
									borderBottom: '0.5px dashed #7A7B7B',
								}}
							>
								<li style={{ padding: '6px 0px' }}>
									<Row gutter={[12, 0]} type="flex">
										<Col span={21}>
											<div
												style={{
													wordWrap: 'break-word',
													wordBreak: 'break-word',
													overflow: 'hidden',
													height: '100%',
													display: 'flex',
													alignItems: 'center',
												}}
											>
												<span>{item.name}</span>
											</div>
										</Col>

										<Col span={3}>
											<div
												style={{
													height: 32,
													display: 'flex',
													alignItems: 'center',
												}}
											>
												<div
													style={{
														background: 'rgba(227, 95, 75, 0.1)',
														width: 20,
														height: 20,
														borderRadius: 10,
														display: 'flex',
														justifyContent: 'center',
														alignItems: 'center',
													}}
													onClick={() => {
														const dataRemovePresentShow = {
															key: 'listTreeShow',
															keyRoot: 'treeProductCombo',
															keyObject: 'children',
															keyLocationRoot: item.locationRoot,
															idx: index,
														};

														const dataRemovePresentClone = {
															key: 'listTreeClone',
															keyRoot: 'treeProductCombo',
															keyObject: 'children',
															keyLocationRoot: item.locationRoot,
															idx: index,
														};
														const dataAssigned = {
															keyRoot: 'treeProductCombo',
															keyTree: 'listTreeShow',
															keyListID: 'listKeyClone',
														};

														dispatch(REMOVE_TREE(dataRemovePresentShow));
														dispatch(REMOVE_TREE(dataRemovePresentClone));
														dispatch(ASSIGNED_KEY(dataAssigned));
													}}
													className="cursor"
												>
													<ISvg
														name={ISvg.NAME.CROSS}
														width={8}
														height={8}
														fill={colors.oranges}
													/>
												</div>
											</div>
										</Col>
									</Row>
								</li>
							</ul>
						);
					}
				})}
			</ul>
		);
	};

	return (
		<div style={{ width: '100%', height: '100%' }}>
			<Row gutter={[0, 16]}>
				<Col span={24}>
					<div style={{ width: '100%', height: '100%' }}>
						<Row gutter={[26, 0]}>
							<Col span={12}>
								<div style={{ width: '100%', height: '100%' }}>
									<Row gutter={[0, 12]}>
										 <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <StyledSearchCustomNew
                          style={{ width: 500, padding: 0 }}
                          onPressEnter={(e) => {
                            setLoadingList(true);
                            setSearch(e.target.value);
                          }}
                          placeholder="Tìm kiếm theo mã, tên phiên bản"
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          height: size.width > 1600 ? 532 : 332,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingRight: 10,
                        }}
                      >
                        {loadingList ? (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              width: "100%",
                              height: "100%",
                            }}
                          >
                            <Spin />
                          </div>
                        ) : (
                          <ITreeNew
                            checkable
                            checkedKeys={treeProductRedux.listKeyClone}
							dataLoop={dataProduct}
							defaultExpandAll
							loadingList
							arrDisabled={treeProductRedux.listArrDisabled}
                            onCheck={(checkedKeys, e) => {
                              console.log("checkedKeys: ", checkedKeys);
                              const tree = [];
                              let arrUnCheck = [];
                              if (e.checked === false) {
                                const id = e.node.props.eventKey;
                                const arrId = id.split("-");
                                switch (arrId.length) {
                                  case 1:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          item.children.forEach((item2) => {
                                            arrUnCheck.push(item2.tree_id);
                                          });
                                        }
                                      );
                                    }
                                    break;
                                  case 2:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          arrUnCheck.push(item.tree_id);
                                        }
                                      );
                                    }
                                    break;
                                  case 3:
                                    {
                                      arrUnCheck.push(
                                        e.node.props.dataProps.tree_id
                                      );
                                    }
                                    break;
                                  default:
                                    break;
                                }
                              }

                              checkedKeys.forEach((key, _, itself) => {
                                const keys = key.split("-");
                                const [id1, id2, id3] = keys;
                                //
                                const selectedItem1 = dataProduct.find(
                                  (item) => item.id.toString() === id1
                                );

                                const selectedItem2 =
                                  selectedItem1 &&
                                  selectedItem1.children.find(
                                    (item) => item.id.toString() === id2
                                  );

                                const selectedItem3 =
                                  selectedItem2 &&
                                  selectedItem2.children.find(
                                    (item) => item.id.toString() === id3
                                  );

                                switch (keys.length) {
                                  case 1: {
                                    if (selectedItem1) {
                                      tree.push(selectedItem1);
                                    }
                                    break;
                                  }

                                  case 2: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );
                                  
                                    if (!selectedItem1InShow) {
                                      tree.unshift({
                                        ...selectedItem1,
                                        children: [
                                          selectedItem2,
                                        
                                        ],
                                      });
                                    } else {
                                      selectedItem1InShow.children.unshift(
                                        selectedItem2
                                      );
                                    }
                                    break;
                                  }
                                  case 3: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    if (
                                      itself.find(
                                        (item) => item === [id1, id2].join("-")
                                      )
                                    )
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );

                                    if (!selectedItem1InShow) {
                                      selectedItem1InShow = {
                                        ...selectedItem1,
                                        children: [
                                          { ...selectedItem2, children: [] },
                                        ],
                                      };
                                      tree.push(selectedItem1InShow);
                                    }

                                    const selectedItem2InShow =
                                      selectedItem1InShow.children.find(
                                        (item) => item.id.toString() === id2
                                      );

                                    if (!selectedItem2InShow) {
                                      selectedItem1InShow.children.push({
                                        ...selectedItem2,
                                        children: [selectedItem3],
                                      });
                                    } else {
                                      selectedItem2InShow.children.push(
                                        selectedItem3
                                      );
                                    }
                                  }
                                  default:
                                    break;
                                }
                              });

                              const dataActionKey = {
                                keyRoot: "treeProductIImmediate",
                                initial_listClone: "listKeyClone",
                                data:
                                  e.checked === true
                                    ? { checked: true, arr: [...checkedKeys] }
                                    : { checked: false, arr: [...arrUnCheck] },
                              };

                              const dataAction = {
                                keyRoot: "treeProductIImmediate",
                                initial_listClone: "listTreeClone",
                                data: {
                                  checked: e.checked,
                                  tree: tree,
                                },
                              };
                              dispatch(ADD_TREE(dataAction));
                              dispatch(ADD_TREE_KEY_CLONE(dataActionKey));
                            }}
                          />
                        )}
                      </div>
                    </Col>
                   
										{/* <Col span={24}>
											<div
												style={{
													height: 455,
													overflow: 'auto',
													border: '1px solid rgba(122, 123, 123, 0.5)',
													paddingLeft: 18,
													paddingRight: 18,
												}}
											>
												<ITree
													checkable
													checkedKeys={treeProductRedux.listKeyClone}
													dataLoop={dataProduct}
													defaultExpandAll
													loadingList
													arrDisabled={treeProductRedux.listArrDisabled}
													onCheck={(checkedKeys, e) => {
														const dataTree = dataProduct.map((item) => {
															let keyRoot;
															let titleRoot;
															const children = e.checkedNodes.map((item1) => {
																let splitKey = item1.key.split('-');
																if (splitKey.length === 2) {
																	if (Number(splitKey[0]) === Number(item.id)) {
																		keyRoot = item.id;
																		titleRoot = item.name;
																		const id = item1.key;
																		const name = item1.props.title;
																		const locationChildrenReal =
																			item1.props.locationChildren;
																		const locationRootReal =
																			item1.props.locationRoot;
																		const attibute_max_value =
																			item1.props.dataProps.attibute_max_value;
																		const price = item1.props.dataProps.price;
																		const priority = item1.props.dataProps.priority;
																		const quantity_sales = 0;
																		const quantity_count = 0;
																		return {
																			id,
																			name,
																			priority,
																			locationChildrenReal,
																			locationRootReal,
																			attibute_max_value,
																			price,
																			quantity_sales,
																			quantity_count,
																		};
																	}
																}
																return;
															});
															if (
																typeof keyRoot === 'undefined' ||
																typeof titleRoot === 'undefined'
															) {
																return;
															}
															const id = keyRoot;
															const name = titleRoot;
															return { id, name, children };
														});
														let dataTreeSucces = dataTree.filter(function (element) {
															return element !== undefined;
														});
														dataTreeSucces.map((item) => {
															item.children = item.children.filter((item1) => {
																return item1 !== undefined;
															});
														});

														dataTreeSucces.map((item, index) => {
															item.children.map((item1, index1) => {
																item1.locationRoot = index;
																item1.locationChildren = index1;
															});
														});

														let dataKey = [];
														dataTreeSucces.map((item) => {
															item.children.map((item1) => {
																dataKey.push(item1.id);
															});
														});

														const dataActionKey = {
															keyRoot: acitonRedux,
															initial_listClone: 'listKeyClone',
															data: dataKey,
															checkArrayCondition: true,
															idx: idxRoot,
														};

														const dataAction = {
															keyRoot: acitonRedux,
															initial_listClone: 'listTreeClone',
															data: dataTreeSucces,
															checkArrayCondition: true,
															idx: idxRoot,
														};

														if (checkGroup) {
															if (dataTreeSucces.length > 1) {
																const content = (
																	<span>
																		Khai báo các sản phẩm thuộc
																		<span style={{ fontWeight: 600 }}>
																			{' '}
																			1 nhóm.
																		</span>
																	</span>
																);
																return IError(content);
															}
														}

														dispatch(ADD_TREE(dataAction));
														dispatch(ADD_TREE(dataActionKey));
													}}
												/>
											</div>
										</Col>
										 */}
										<Col span={24}>
											<div
												style={{
													width: '100%',
													display: 'flex',
													justifyContent: 'flex-end',
												}}
											>
												<IButton
													title="Thêm"
													color={colors.main}
													icon={ISvg.NAME.BUTTONRIGHT}
													styleHeight={{
														width: 140,
													}}
													isRight={true}
													onClick={() => {
														const dataAction = {
															keyRoot: `${acitonRedux}`,
															initial_listClone: 'listTreeClone',
															initial_listShow: 'listTreeShow',
															checkArrayCondition: true,
															idx: idxRoot,
														};

														dispatch(SHOW_TREE(dataAction));
													}}
												/>
											</div>
										</Col>
									</Row>
								</div>
							</Col>
							<Col span={12}>
								<div
									style={{
										height: size.width > 1600 ? 640 : 439,
										border: '1px solid rgba(122, 123, 123, 0.5)',
										overflow: 'auto',
									}}
								>
									{checkGroup
										? treeProctGroup(treeProductRedux.listTreeShow)
										: treeProct(treeProductRedux.listTreeShow)}
								</div>
							</Col>
						</Row>
					</div>
				</Col>
				<Col span={24}>
					<div
						style={{
							display: 'flex',
							flexDirection: 'row',
							position: 'absolute',
							top: 60,
							right: 0,
						}}
					>
						<IButton
							title="Lưu"
							color={colors.main}
							icon={ISvg.NAME.SAVE}
							styleHeight={{
								width: 140,
								marginRight: 24,
							}}
							onClick={() => {
								const dataAction = {
									keyRoot: `${acitonRedux}`,
									initial_listShow: 'listTreeShow',
									initial_listConfirm: 'listConfirm',
									checkArrayCondition: true,
									idx: idxRoot,
								};

								let arrProductShow = [];
								treeProductRedux.listTreeShow.map((item, index) => {
									if (item.children && item.children.length) {
										item.children.map((item) => {
											const id = item.id;
											const name = item.name;
											arrProductShow.push({ id, name });
										});
									}
								});

								if (arrProductShow.length <= 0) {
									const content = <span>Vui lòng chọn từ 1 sản phẩm trở lên</span>;
									return IError(content);
								} else {
									if (checkGroup) {
										if (count_save === -1 || treeProductRedux.listArrListTreeConfirm.length === 0) {
											const dataGroup = {
												keyRoot: `${acitonRedux}`,
												initial_listShow: 'listTreeShow',
												initial_listConfirm: 'listConfirm',
												conditon_reward: true,
												checkArrayCondition: true,
												idx: idxRoot,
											};
											dispatch(SAVE_TREE_GROUP_COMBO_CONDITION(dataGroup));
										} else {
											const dataGroup1 = {
												keyRoot: `${acitonRedux}`,
												initial_listShow: 'listTreeShow',
												initial_listConfirm: 'listConfirm',
												conditon_reward: true,
												checkArrayCondition: true,
												idx: idxRoot,
											};
											dispatch(SAVE_TREE_GROUP_COMBO_OVER_CONDITION(dataGroup1));
										}
									} else {
										dispatch(SAVE_TREE(dataAction));
									}

									callbackClose();
								}
							}}
						/>
						<IButton
							title="Hủy bỏ"
							color={colors.oranges}
							icon={ISvg.NAME.CROSS}
							styleHeight={{
								width: 140,
							}}
							onClick={() => {
								if (count_save === -1) {
									callbackClose();
								} else {
									if (checkGroup) {
										dispatch(
											CLOSE_MODAL_COMBO_CONDITION({
												idx: idxRoot,
											})
										);
										callbackClose();
									} else {
										callbackClose();
									}
								}
							}}
						/>
					</div>
				</Col>
			</Row>
		</div>
	);
}
