import { Col, message, Row, Spin } from "antd";
import React, { useEffect, useState } from "react";
import PropType from "prop-types";
import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import {
  ADD_TREE,
  SHOW_TREE,
  REMOVE_TREE,
  ASSIGNED_KEY,
  CREATE_DATA_PRODUCT,
  CLOSE_MODAL_COMBO,
  SAVE_TREE_GIFT_GROUP_IMMEDIATE,
  SAVE_TREE_GIFT_GROUP_IMMEDIATE_OVER,
  ADD_ARR_PRODUCT_GIFT_GROUP,
  ADD_TREE_KEY_CLONE,
  SAVE_TREE_NEW,
  REMOVE_PRODUCT,
} from "../../../store/reducers";
import ITreeNew from "../Tree/ITreeNew";
import { IError } from "../../../components/common";

export default function useModalTreeProductImmediateNewGiftGroup(
  callbackClose = () => {},
  id,
  checkGroup,
  count_save,
  indexArrProductGiftGroup,
  type_product,
  isVisibleTreeGift
) {
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const {
    treeProductIImmediate,
    eventImmediateNew,
    dataProductImmediatePresent,
    treeProductImmediateGiftGroup,
  } = dataRoot;
  const treeProductRedux = checkGroup
    ? treeProductImmediateGiftGroup
    : treeProductIImmediate;
  const acitonRedux = checkGroup
    ? "treeProductImmediateGiftGroup"
    : "treeProductIImmediate";
  const [loadingList, setLoadingList] = useState(true);
  // const [search, setSearch] = useState("");

  const getListTreeProduct = async (search, type_product) => {
    try {
      setLoadingList(true);
      const data = await APIService._getListTreeProductEventGift(
        search,
        type_product
      );
      // let listKeyCloneNew = [];

      // treeProductIImmediate.listKeyClone.forEach((item) => {
      //   let ids = item.split("-");
      //   if (ids.length === 3) {
      //     listKeyCloneNew.push(item);
      //   }
      // });

      // const dataAction = {
      //   keyRoot: "treeProductIImmediate",
      //   key: "listKeyClone",
      //   value: listKeyCloneNew,
      // };

      // dispatch(ASSIGN_LIST_KEY_CLONE(dataAction));

      // let tree = data.product_type.map((itemLv1) => {
      //   let arrLv1Child = itemLv1.children.map((itemLv2) => {
      //     let arrLv2Child = itemLv2.children.map((itemLv3) => {
      //       if (!itemLv3.content) {
      //         itemLv3.description = "";
      //         return itemLv3;
      //       } else {
      //         return itemLv3;
      //       }
      //     });
      //     return {
      //       ...itemLv2,
      //       children: [...arrLv2Child],
      //     };
      //   });
      //   return {
      //     ...itemLv1,
      //     children: [...arrLv1Child],
      //   };
      // });

      const keyDispatch = {
        key: "dataProductImmediatePresent",
        value: data.product_type,
      };

      dispatch(CREATE_DATA_PRODUCT(keyDispatch));
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    getListTreeProduct("", type_product);
  }, [isVisibleTreeGift]);

  const treeProct = (dataProduct, idxRoot) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children, idxRoot)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={21}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 30,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    <Col span={3}>
                      <div
                        style={{
                          height: 32,
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                          onClick={() => {
                            const dataAction = {
                              keyRoot: `treeProductImmediateGiftGroup`,
                              key_listTreeClone: "listTreeClone",
                              key_listTreeShow: "listTreeShow",
                              key_listKeyClone: "listKeyClone",
                              indexProduct: index,
                              product: item,
                              listTreeClone: treeProductRedux.listTreeClone,
                              listTreeShow: treeProductRedux.listTreeShow,
                              listKeyClone: treeProductRedux.listKeyClone,
                            };

                            dispatch(REMOVE_PRODUCT(dataAction));
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill={colors.oranges}
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  const treeProctGroup = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding: "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[12, 0]} type="flex">
                    <Col span={21}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };
  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={12}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <div
                        style={{
                          height: 455,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        {loadingList ? (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              width: "100%",
                              height: "100%",
                            }}
                          >
                            <Spin />
                          </div>
                        ) : (
                          <ITreeNew
                            checkable
                            checkedKeys={treeProductRedux.listKeyClone}
                            dataLoop={dataProductImmediatePresent}
                            defaultExpandAll
                            arrDisabled={treeProductRedux.listArrDisabled}
                            loadingList
                            onCheck={(checkedKeys, e) => {
                              console.log("checkedKeys: ", checkedKeys);
                              const tree = [];
                              let arrUnCheck = [];
                              if (e.checked === false) {
                                const id = e.node.props.eventKey;
                                const arrId = id.split("-");
                                switch (arrId.length) {
                                  case 1:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          item.children.forEach((item2) => {
                                            arrUnCheck.push(item2.tree_id);
                                          });
                                        }
                                      );
                                    }
                                    break;
                                  case 2:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          arrUnCheck.push(item.tree_id);
                                        }
                                      );
                                    }
                                    break;
                                  case 3:
                                    {
                                      arrUnCheck.push(
                                        e.node.props.dataProps.tree_id
                                      );
                                    }
                                    break;
                                  default:
                                    break;
                                }
                              }

                              checkedKeys.forEach((key, _, itself) => {
                                const keys = key.split("-");
                                const [id1, id2, id3] = keys;
                                //
                                const selectedItem1 =
                                  dataProductImmediatePresent.find(
                                    (item) => item.id.toString() === id1
                                  );

                                const selectedItem2 =
                                  selectedItem1 &&
                                  selectedItem1.children.find(
                                    (item) => item.id.toString() === id2
                                  );

                                const selectedItem3 =
                                  selectedItem2 &&
                                  selectedItem2.children.find(
                                    (item) => item.id.toString() === id3
                                  );

                                switch (keys.length) {
                                  case 1: {
                                    if (selectedItem1) {
                                      tree.push(selectedItem1);
                                    }
                                    break;
                                  }

                                  case 2: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );
                                    // let childSelectedItem2Tmp = [
                                    //   ...selectedItem2.children,
                                    // ];
                                    //
                                    // treeProductRedux.listKeyCloneDisabled.forEach(
                                    //   (itemDisabled) => {
                                    //
                                    //     let idx = childSelectedItem2Tmp.findIndex(
                                    //       (item) =>
                                    //         item.tree_id === itemDisabled
                                    //     );
                                    //     if (idx !== -1) {
                                    //       childSelectedItem2Tmp.splice(idx, 1);
                                    //     }
                                    //   }
                                    // );
                                    if (!selectedItem1InShow) {
                                      tree.unshift({
                                        ...selectedItem1,
                                        children: [
                                          selectedItem2,
                                          // {
                                          //   ...selectedItem2,
                                          //   children: childSelectedItem2Tmp,
                                          // },
                                        ],
                                      });
                                    } else {
                                      selectedItem1InShow.children.unshift(
                                        selectedItem2
                                      );
                                    }
                                    break;
                                  }
                                  case 3: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    if (
                                      itself.find(
                                        (item) => item === [id1, id2].join("-")
                                      )
                                    )
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );

                                    if (!selectedItem1InShow) {
                                      selectedItem1InShow = {
                                        ...selectedItem1,
                                        children: [
                                          { ...selectedItem2, children: [] },
                                        ],
                                      };
                                      tree.push(selectedItem1InShow);
                                    }

                                    const selectedItem2InShow =
                                      selectedItem1InShow.children.find(
                                        (item) => item.id.toString() === id2
                                      );

                                    if (!selectedItem2InShow) {
                                      selectedItem1InShow.children.push({
                                        ...selectedItem2,
                                        children: [selectedItem3],
                                      });
                                    } else {
                                      selectedItem2InShow.children.push(
                                        selectedItem3
                                      );
                                    }
                                  }
                                  default:
                                    break;
                                }
                              });

                              const dataActionKey = {
                                keyRoot: `${acitonRedux}`,
                                initial_listClone: "listKeyClone",
                                data:
                                  e.checked === true
                                    ? { checked: true, arr: [...checkedKeys] }
                                    : { checked: false, arr: [...arrUnCheck] },
                              };

                              const dataAction = {
                                keyRoot: `${acitonRedux}`,
                                initial_listClone: "listTreeClone",
                                data: {
                                  checked: e.checked,
                                  tree: tree,
                                },
                              };
                              dispatch(ADD_TREE(dataAction));
                              dispatch(ADD_TREE_KEY_CLONE(dataActionKey));
                            }}
                          />
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            if (treeProductRedux.listTreeClone.length === 0) {
                              message.error("Vui lòng chọn sản phẩm");
                            } else {
                              const dataAction = {
                                keyRoot: `${acitonRedux}`,
                                initial_listClone: "listTreeClone",
                                initial_listShow: "listTreeShow",
                              };

                              dispatch(SHOW_TREE(dataAction));
                            }
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={12}>
                <div
                  style={{
                    height: 510,
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                    overflow: "auto",
                  }}
                >
                  {checkGroup
                    ? treeProctGroup(treeProductRedux.listTreeShow)
                    : treeProct(treeProductRedux.listTreeShow)}
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 60,
              right: 0,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 24,
              }}
              onClick={() => {
                const dataAction = {
                  keyRoot: `${acitonRedux}`,
                  initial_listShow: "listTreeShow",
                  initial_listConfirm: "listConfirm",
                };

                let arrProductShow = [];
                treeProductRedux.listTreeShow.map((itemLv1, index) => {
                  if (itemLv1.children && itemLv1.children.length) {
                    itemLv1.children.map((itemLv2) => {
                      if (itemLv2.children && itemLv2.children.length) {
                        itemLv2.children.map((itemLv3) => {
                          const id = itemLv3.id;
                          const name = itemLv3.name;
                          arrProductShow.push({ id, name });
                        });
                      }
                    });
                  }
                });

                if (arrProductShow.length <= 0) {
                  const content = (
                    <span>Vui lòng chọn tối thiểu 1 sản phẩm trở lên</span>
                  );
                  return IError(content);
                } else {
                  if (checkGroup) {
                    if (
                      count_save === -1 ||
                      treeProductImmediateGiftGroup.listArrListTreeConfirm
                        .length === 0
                    ) {
                      let arrProductGift = [];

                      treeProductImmediateGiftGroup.listTreeShow.forEach(
                        (itemLv1) => {
                          itemLv1.children.forEach((itemLv2) => {
                            itemLv2.children.forEach((itemLv3) => {
                              arrProductGift.push({
                                ...itemLv3,
                                minUnitValue: 0,
                                maxUnitValue: 0,
                                quantity_gif_min: 0,
                                quantity_gif_max: 0,
                              });
                            });
                          });
                        }
                      );

                      const dataAction = {
                        key: "arr_product_gift_group",
                        index: indexArrProductGiftGroup,
                        value: { arr_product: arrProductGift },
                      };

                      dispatch(ADD_ARR_PRODUCT_GIFT_GROUP(dataAction));

                      const dataGroup = {
                        keyRoot: `${acitonRedux}`,
                        initial_listShow: "listTreeShow",
                        initial_listConfirm: "listConfirm",
                      };
                      dispatch(SAVE_TREE_GIFT_GROUP_IMMEDIATE(dataGroup));
                    } else {
                      let arrProductGift = [];

                      treeProductImmediateGiftGroup.listTreeShow.forEach(
                        (itemLv1) => {
                          itemLv1.children.forEach((itemLv2) => {
                            itemLv2.children.map((itemLv3) => {
                              arrProductGift.push({
                                ...itemLv3,
                                minUnitValue: 0,
                                maxUnitValue: 0,
                              });
                            });
                          });
                        }
                      );

                      const dataAction = {
                        key: "arr_product_gift_group",
                        index: indexArrProductGiftGroup,
                        value: { arr_product: arrProductGift },
                      };

                      dispatch(ADD_ARR_PRODUCT_GIFT_GROUP(dataAction));

                      const dataGroup1 = {
                        keyRoot: `${acitonRedux}`,
                        initial_listShow: "listTreeShow",
                        initial_listConfirm: "listConfirm",
                      };
                      dispatch(SAVE_TREE_GIFT_GROUP_IMMEDIATE_OVER(dataGroup1));
                    }
                  } else {
                    dispatch(SAVE_TREE_NEW(dataAction));
                  }

                  callbackClose();
                }
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                if (count_save === -1) {
                  callbackClose();
                } else {
                  if (checkGroup) {
                    dispatch(CLOSE_MODAL_COMBO());
                    callbackClose();
                  } else {
                    callbackClose();
                  }
                }
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
