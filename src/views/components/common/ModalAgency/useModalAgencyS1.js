import { Checkbox, Col, Icon, List, message, Row, Input } from "antd";
import React, { useEffect, useState } from "react";
import { StyledITileHeading } from "../Font/font";
import { StyledSearchCustom } from "..";
import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import ITableHtml from "../../ITableHtml";
import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { SearchOutlined } from "@ant-design/icons";
import {
  ADD_MODAL,
  DEAFUAT_MODAL,
  REMOVE_MODAL,
  SAVE_MODAL,
  UPDATE_ACTIVELIST_MODAL,
  UPDATE_ACTIVELISTAGENCY_MODAL,
  CONFIRM_MODAL,
  CANCEL_MODAL,
} from "../../../store/reducers";

const { Search } = Input;

const StyledSearchCustomNew = styled(Search)`
  height: 42px;

  width: 100%;
  border: 0px !important;

  .ant-input:focus {
    box-shadow: none !important;
  }

  .ant-input-affix-wrapper .ant-input {
    padding-left: 0px !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
  .ant-input-search: not(.ant-input-search-enter-button) {
    padding-right: 6px !important;
  }
  .ant-input-suffix {
    font-size: 20px;
  }

  .ant-input {
    padding-left: 0 !important;
  }
`;

export default function useModalAgencyS1(
  callbackClose = () => {},
  historyPush = () => {}
) {
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { modalS1 } = dataRoot;
  const [filterList, setfilterList] = useState({
    status: 1,
    page: 0,
    city_id: 0,
    // district_id: 0,
    agency_id: -1,
    keySearch: "",
    membership_id: 0,
  });
  const [loadingList, setLoadingList] = useState(true);
  const _getAPIListAgencyS1 = async () => {
    try {
      const data = await APIService._getListAllAgencyS1(
        filterList.status,
        filterList.page,
        filterList.city_id,
        // filterList.district_id,
        filterList.agency_id,
        filterList.keySearch,
        filterList.membership_id
      );
      let useragency = [...data.useragency];
      useragency.map((item, index) => {
        useragency[index].stt = (filterList.page - 1) * 10 + index + 1;
        useragency[index].checked = false;
      });
      dispatch(
        DEAFUAT_MODAL({
          data: useragency,
          keyInitial_listShow: "listS1Show",
          keyInitial_listAddClone: "listS1AddClone",
          keyInitial_listBefore: "listS1Before",
          keyInitial_list: "listS1",
          keyInitial_listClone: "listS1Clone",
          keyInitial_root: "modalS1",
        })
      );

      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    _getAPIListAgencyS1(filterList);
  }, [filterList]);

  const headerTable = [
    {
      name: "STT",
      align: "center",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Địa chỉ",
      align: "left",
    },
    {
      name: "Phòng kinh doanh",
      align: "left",
    },
    {
      name: "",
      align: "center",
    },
  ];

  const headerTableProduct = (headerTable) => {
    return (
      <tr>
        {headerTable.map((item, index) => (
          <th className={"th-table"} style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{
            textAlign: "center",
            fontWeight: 400,
            fontSize: "clamp(8px, 4vw, 14px)",
          }}
        >
          {index + 1}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.dms_code}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.shop_name}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.address}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.agency_name}
        </td>
        <td className="td-table">
          <div
            style={{
              background: "rgba(227, 95, 75, 0.1)",
              width: 20,
              height: 20,
              borderRadius: 10,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            onClick={() => {
              dispatch(
                REMOVE_MODAL({
                  idx: index,
                  keyInitial_root: "modalS1",
                  keyInitial_listShow: "listS1Show",
                  keyInitial_listAddClone: "listS1AddClone",
                  keyInitial_listS1Add: "listS1Add",
                })
              );
              dispatch(
                UPDATE_ACTIVELISTAGENCY_MODAL({
                  id: item.id,
                  status: false,
                  keyInitial_root: "modalS1",
                  keyInitial_list: "listS1",
                })
              );
            }}
            className="cursor"
          >
            <ISvg
              name={ISvg.NAME.CROSS}
              width={8}
              height={8}
              fill={colors.oranges}
            />
          </div>
        </td>
      </tr>
    ));
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Chọn Đại lý cấp 1 chỉ định áp dụng chương trình
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={7}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <StyledSearchCustomNew
                          style={{ width: 450, padding: 0 }}
                          onPressEnter={(e) => {
                            setLoadingList(true);
                            setfilterList({
                              ...filterList,
                              keySearch: e.target.value,
                            });
                          }}
                          placeholder="Tìm kiếm theo mã, tên, SĐT ĐLC1"
                          // suffix={
                          //   <SearchOutlined
                          //     style={{
                          //       fontSize: 20,
                          //       color: colors.svg_default,
                          //       paddingRight: 5,
                          //     }}
                          //   />
                          // }
                        />
                      </div>
                    </Col>

                    <Col span={24}>
                      <div
                        style={{
                          height: 342,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        <List
                          dataSource={modalS1.listS1}
                          bordered={false}
                          loading={loadingList}
                          renderItem={(item, index) => {
                            return (
                              <List.Item>
                                <div style={{ width: "100%", height: "100%" }}>
                                  <Row>
                                    <Col span={24}>
                                      <Checkbox
                                        defaultChecked={item.checked}
                                        checked={item.checked}
                                        onChange={(e) => {
                                          let checked = e.target.checked;

                                          dispatch(
                                            UPDATE_ACTIVELIST_MODAL({
                                              keyInitial_root: "modalS1",
                                              keyInitial_list: "listS1",
                                              status: checked,
                                              id: item.id,
                                            })
                                          );
                                          dispatch(
                                            ADD_MODAL({
                                              keyInitial_Add: "listS1Add",
                                              keyInitial_AddClone:
                                                "listS1AddClone",
                                              keyInitial_ListS1Add: "listS1Add",
                                              data: item,
                                              status: checked,
                                              keyInitial_root: "modalS1",
                                            })
                                          );
                                        }}
                                      >
                                        <span>{item.shop_name}</span>
                                      </Checkbox>
                                    </Col>
                                  </Row>
                                </div>
                              </List.Item>
                            );
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            if (modalS1.listS1AddClone.length === 0) {
                              message.error("Chưa chọn đại lý cấp 1");
                              return;
                            }
                            dispatch(
                              SAVE_MODAL({
                                keyInitial_root: "modalS1",
                                keyInitial_listShow: "listS1Show",
                                keyInitial_listAddClone: "listS1AddClone",
                              })
                            );
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={17}>
                <div
                  style={{
                    height: 450,
                    borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                    borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <ITableHtml
                    style={{ maxHeight: 449 }}
                    childrenBody={bodyTableProduct(modalS1.listS1Show)}
                    childrenHeader={headerTableProduct(headerTable)}
                    isBorder={false}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 40,
              right: -25,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 15,
              }}
              onClick={() => {
                dispatch(
                  CONFIRM_MODAL({
                    keyInitial_root: "modalS1",
                    keyInitial_listShow: "listS1Show",
                    keyInitial_Add: "listS1Add",
                  })
                );
                callbackClose();
                historyPush();
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                dispatch(
                  CANCEL_MODAL({
                    keyInitial_root: "modalS1",
                    keyInitial_listShow: "listS1Show",
                    keyInitial_Add: "listS1Add",
                    keyInitial_listAddClone: "listS1AddClone",
                    keyInitial_listClone: "listS1Clone",
                    keyInitial_list: "listS1",
                  })
                );
                callbackClose();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
