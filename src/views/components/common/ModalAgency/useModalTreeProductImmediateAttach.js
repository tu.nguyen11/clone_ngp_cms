import { Col, Input, Row, Spin } from "antd";
import React, { useEffect, useState } from "react";

import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import {
  ADD_TREE,
  SHOW_TREE,
  CREATE_DATA_PRODUCT,
  ASSIGN_LIST_KEY_CLONE,
  ADD_TREE_KEY_CLONE,
  ADD_BONUS_LEVEL_ATTACH_EVENT_IMMEDIATE_NEW,
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
  SAVE_TREE_NEW,
} from "../../../store/reducers";
import { StyledITileHeading } from "../Font/font";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import styled from "styled-components";
import ITreeNew from "../Tree/ITreeNew";
import useWindowSize from "../../../../utils/useWindowSize";

const { Search } = Input;

export const StyledSearchCustomNew = styled(Search)`
  height: 42px;

  width: 100%;
  border: 0px !important;

  .ant-input:focus {
    box-shadow: none !important;
  }

  .ant-input-affix-wrapper .ant-input {
    padding-left: 0px !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
  .ant-input-search: not(.ant-input-search-enter-button) {
    padding-right: 6px !important;
  }
  .ant-input-suffix {
    font-size: 20px;
  }

  .ant-input {
    padding-left: 0 !important;
  }
`;

export default function useModalTreeProductImmediateAttach(
  callbackClose = () => {},
  type_product,
  title
) {
  let size = useWindowSize();
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const {
    dataProductImmediateConditionAttach,
    treeProductPresentIImmediateConditionAttach = {
      listTreeClone: [],
      listTreeShow: [],
      listConfirm: [],
    },
  } = dataRoot;

  const [loadingList, setLoadingList] = useState(true);
  const [search, setSearch] = useState("");

  const getListTreeProduct = async (search, type_product) => {
    try {
      setLoadingList(true);
      const data = await APIService._getListTreeProductEventGift(
        search,
        type_product
      );
      let listKeyCloneNew = [];

      treeProductPresentIImmediateConditionAttach.listKeyClone.forEach(
        (item) => {
          let ids = item.split("-");
          if (ids.length === 3) {
            listKeyCloneNew.push(item);
          }
        }
      );

      const dataAction = {
        keyRoot: "treeProductPresentIImmediateConditionAttach",
        key: "listKeyClone",
        value: listKeyCloneNew,
      };

      dispatch(ASSIGN_LIST_KEY_CLONE(dataAction));

      let tree = data.product_type.map((itemLv1) => {
        let arrLv1Child = itemLv1.children.map((itemLv2) => {
          let arrLv2Child = itemLv2.children.map((itemLv3) => {
            if (!itemLv3.content) {
              itemLv3.description = "";
              return itemLv3;
            } else {
              return itemLv3;
            }
          });
          return {
            ...itemLv2,
            children: [...arrLv2Child],
          };
        });
        return {
          ...itemLv1,
          children: [...arrLv1Child],
        };
      });

      const keyDispatch = {
        key: "dataProductImmediateConditionAttach",
        value: tree,
      };

      dispatch(CREATE_DATA_PRODUCT(keyDispatch));
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    getListTreeProduct(search, type_product);
  }, [search, type_product]);

  const treeProct = (dataProduct, idxRoot) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children, idxRoot)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={24}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 30,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    {/* <Col span={24}>
                      <div style={{ paddingLeft: 30 }}>
                        <IInputTextArea
                          style={{
                            minHeight: 35,
                            height: 35,
                            padding: 6,
                            marginTop: 8,
                            border: "none",
                          }}
                          placeholder="Mô tả chính sách bán hàng của sản phẩm"
                          value={item.description}
                          onChange={(e) => {
                            const data = {
                              key: "description",
                              tree:
                                eventGift.event_design.key_type_product === 1
                                  ? treeProductEventGiftGroup.listArrListTreeConfirm
                                  : treeProductEventGiftGroup2.listArrListTreeConfirm,
                              value: e.target.value,
                              idxRoot: idxRoot,
                              id: item.id,
                              keyTypeProduct:
                                eventGift.event_design.key_type_product,
                            };
                            dispatch(ADD_CONTENT_PRODUCT(data));
                          }}
                        />
                      </div>
                    </Col> */}
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <div style={{ marginBottom: 24 }}>
        <StyledITileHeading minFont="10px" maxFont="16px">
          {title}
        </StyledITileHeading>
      </div>

      <Row gutter={[0, 16]}>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={11}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <StyledSearchCustomNew
                          style={{ width: 500, padding: 0 }}
                          onPressEnter={(e) => {
                            setLoadingList(true);
                            setSearch(e.target.value);
                          }}
                          // value={search}
                          // placeholder="Tìm kiếm theo mã, tên phiên bản"
                          placeholder="Tìm kiếm theo mã, tên phiên bản"
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          height: size.width > 1600 ? 532 : 332,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingRight: 10,
                        }}
                      >
                        {loadingList ? (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              width: "100%",
                              height: "100%",
                            }}
                          >
                            <Spin />
                          </div>
                        ) : (
                          <ITreeNew
                            checkable
                            checkedKeys={
                              treeProductPresentIImmediateConditionAttach.listKeyClone
                            }
                            dataLoop={dataProductImmediateConditionAttach}
                            defaultExpandAll
                            loadingList
                            onCheck={(checkedKeys, e) => {
                              console.log("checkedKeys: ", checkedKeys);
                              const tree = [];
                              let arrUnCheck = [];
                              if (e.checked === false) {
                                const id = e.node.props.eventKey;
                                const arrId = id.split("-");
                                switch (arrId.length) {
                                  case 1:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          item.children.forEach((item2) => {
                                            arrUnCheck.push(item2.tree_id);
                                          });
                                        }
                                      );
                                    }
                                    break;
                                  case 2:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          arrUnCheck.push(item.tree_id);
                                        }
                                      );
                                    }
                                    break;
                                  case 3:
                                    {
                                      arrUnCheck.push(
                                        e.node.props.dataProps.tree_id
                                      );
                                    }
                                    break;
                                  default:
                                    break;
                                }
                              }

                              checkedKeys.forEach((key, _, itself) => {
                                const keys = key.split("-");
                                const [id1, id2, id3] = keys;
                                //
                                const selectedItem1 =
                                  dataProductImmediateConditionAttach.find(
                                    (item) => item.id.toString() === id1
                                  );

                                const selectedItem2 =
                                  selectedItem1 &&
                                  selectedItem1.children.find(
                                    (item) => item.id.toString() === id2
                                  );

                                const selectedItem3 =
                                  selectedItem2 &&
                                  selectedItem2.children.find(
                                    (item) => item.id.toString() === id3
                                  );

                                switch (keys.length) {
                                  case 1: {
                                    if (selectedItem1) {
                                      tree.push(selectedItem1);
                                    }
                                    break;
                                  }

                                  case 2: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );
                                    if (!selectedItem1InShow) {
                                      tree.unshift({
                                        ...selectedItem1,
                                        children: [selectedItem2],
                                      });
                                    } else {
                                      selectedItem1InShow.children.unshift(
                                        selectedItem2
                                      );
                                    }
                                    break;
                                  }
                                  case 3: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    if (
                                      itself.find(
                                        (item) => item === [id1, id2].join("-")
                                      )
                                    )
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );

                                    if (!selectedItem1InShow) {
                                      selectedItem1InShow = {
                                        ...selectedItem1,
                                        children: [
                                          { ...selectedItem2, children: [] },
                                        ],
                                      };
                                      tree.push(selectedItem1InShow);
                                    }

                                    const selectedItem2InShow =
                                      selectedItem1InShow.children.find(
                                        (item) => item.id.toString() === id2
                                      );

                                    if (!selectedItem2InShow) {
                                      selectedItem1InShow.children.push({
                                        ...selectedItem2,
                                        children: [selectedItem3],
                                      });
                                    } else {
                                      selectedItem2InShow.children.push(
                                        selectedItem3
                                      );
                                    }
                                  }
                                  default:
                                    break;
                                }
                              });

                              const dataActionKey = {
                                keyRoot:
                                  "treeProductPresentIImmediateConditionAttach",
                                initial_listClone: "listKeyClone",
                                data:
                                  e.checked === true
                                    ? { checked: true, arr: [...checkedKeys] }
                                    : { checked: false, arr: [...arrUnCheck] },
                              };

                              const dataAction = {
                                keyRoot:
                                  "treeProductPresentIImmediateConditionAttach",
                                initial_listClone: "listTreeClone",
                                data: {
                                  checked: e.checked,
                                  tree: tree,
                                },
                              };
                              dispatch(ADD_TREE(dataAction));
                              dispatch(ADD_TREE_KEY_CLONE(dataActionKey));
                            }}
                          />
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                        arr_bonus_level
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            const dataAction = {
                              keyRoot:
                                "treeProductPresentIImmediateConditionAttach",
                              initial_listClone: "listTreeClone",
                              initial_listShow: "listTreeShow",
                            };

                            dispatch(SHOW_TREE(dataAction));
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={13}>
                <div
                  style={{
                    height: size.width > 1600 ? 640 : 439,
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                    overflowY: "auto",
                    overflowX: "hidden",
                  }}
                >
                  <div
                    style={{
                      padding: "12px",
                      borderBottom: "0.5px solid #d9d9d9",
                    }}
                  >
                    <Row gutter={[12, 0]} type="flex">
                      <Col span={24}>
                        <div
                          style={{
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <StyledITileHeading minFont="14px" maxFont="16px">
                            Nhóm phiên bản
                          </StyledITileHeading>
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <div style={{ padding: "0px 12px" }}>
                    {treeProct(
                      treeProductPresentIImmediateConditionAttach.listTreeShow
                    )}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 45,
              right: -25,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 24,
              }}
              onClick={() => {
                let arrProduct = JSON.parse(
                  JSON.stringify(
                    treeProductPresentIImmediateConditionAttach.listTreeShow
                  )
                );

                let arrProductConfirm = [];
                arrProduct.forEach((itemLv1) => {
                  itemLv1.children.forEach((itemLv2) => {
                    itemLv2.children.forEach((itemLv3) => {
                      arrProductConfirm.push({
                        ...itemLv3,
                        salesOrQuantity: 0,
                      });
                    });
                  });
                });

                // let arrProductNew = arrProductConfirm.map((item) => {
                //   return {
                //     ...item,
                //     salesOrQuantity: 0,
                //   };
                // });

                const dataAction3 = {
                  value: arrProductConfirm,
                };

                dispatch(
                  ADD_BONUS_LEVEL_ATTACH_EVENT_IMMEDIATE_NEW(dataAction3)
                );

                const dataAction1 = {
                  keyRoot: "treeProductPresentIImmediateConditionAttach",
                  initial_listShow: "listTreeShow",
                  initial_listConfirm: "listConfirm",
                  assigned_condition: true,
                };

                dispatch(SAVE_TREE_NEW(dataAction1));

                callbackClose();
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                callbackClose();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
