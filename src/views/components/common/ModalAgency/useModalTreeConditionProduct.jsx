import { Col, message, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { colors } from '../../../../assets';
import IButton from '../../IButton';
import ISvg from '../../ISvg';
import { APIService } from '../../../../services';
import { useDispatch, useSelector } from 'react-redux';
import {
	SAVE_TREE,
	ADD_TREE,
	SHOW_TREE,
	CANCEL_TREE,
	REMOVE_TREE,
	ASSIGNED_KEY,
	CREATE_DATA_PRODUCT,
	CLOSE_MODAL_COMBO_CONDITION,
	SAVE_TREE_GROUP_COMBO_CONDITION,
	SAVE_TREE_GROUP_COMBO_OVER_CONDITION,
} from '../../../store/reducers';
import ITree from '../Tree/ITree';
import { IError } from '../Error/IError';
export default function useModalTreeConditionProduct(callbackClose = () => {}, id, checkGroup, count_save) {
	const dispatch = useDispatch();
	const dataRoot = useSelector((state) => state);
	const { treeProductCondition, treeProductConditionGroup, event, dataProduct } = dataRoot;

	const treeProductRedux = checkGroup ? treeProductConditionGroup : treeProductCondition;
	const acitonRedux = checkGroup ? 'treeProductConditionGroup' : 'treeProductCondition';

	const [loadingList, setLoadingList] = useState(true);

	const _getListTreeProduct = async (id) => {
		try {
			const data = await APIService._getListTreeProduct(id);
			data.cate.lstCate.forEach((item) => {
				item.children.map((item1) => {
					item1.priority = 1;
				});
			});
			const keyDispatch = {
				key: 'dataProduct',
				value: data.cate.lstCate,
			};
			dispatch(CREATE_DATA_PRODUCT(keyDispatch));
		} catch (error) {
			console.log(error);
			setLoadingList(false);
		}
	};

	useEffect(() => {
		_getListTreeProduct(id);
	}, [id]);

	const treeProct = (dataProduct, idx) => {
		return (
			<ul style={{ padding: 0 }}>
				{dataProduct.map((item, index) => {
					if (item.children && item.children.length) {
						return (
							<ul
								style={{
									listStyleType: 'none',
									padding: '6px 0px',
								}}
							>
								<li
									style={{
										fontWeight: 'bold',
										borderBottom: '0.5px dashed #7A7B7B',
										padding: '6px 6px',
									}}
								>
									{item.name}
								</li>
								{treeProct(item.children)}
							</ul>
						);
					} else {
						return (
							<ul
								style={{
									listStyleType: 'none',
									borderBottom: '0.5px dashed #7A7B7B',
								}}
							>
								<li style={{ padding: '6px 0px' }}>
									<Row gutter={[12, 0]} type="flex">
										<Col span={21}>
											<div
												style={{
													wordWrap: 'break-word',
													wordBreak: 'break-word',
													overflow: 'hidden',
													height: '100%',
													display: 'flex',
													alignItems: 'center',
												}}
											>
												<span>{item.name}</span>
											</div>
										</Col>

										<Col span={3}>
											<div
												style={{
													height: 32,
													display: 'flex',
													alignItems: 'center',
												}}
											>
												<div
													style={{
														background: 'rgba(227, 95, 75, 0.1)',
														width: 20,
														height: 20,
														borderRadius: 10,
														display: 'flex',
														justifyContent: 'center',
														alignItems: 'center',
													}}
													onClick={() => {
														if (checkGroup) {
															const dataRemovePresentShow = {
																key: 'listTreeShow',
																keyRoot: 'treeProductConditionGroup',
																keyObject: 'children',
																keyLocationRoot: item.locationRoot,
																idx: index,
															};

															const dataRemovePresentClone = {
																key: 'listTreeClone',
																keyRoot: 'treeProductConditionGroup',
																keyObject: 'children',
																keyLocationRoot: item.locationRoot,
																idx: index,
															};
															const dataAssigned = {
																keyRoot: 'treeProductConditionGroup',
																keyTree: 'listTreeShow',
																keyListID: 'listKeyClone',
															};

															dispatch(REMOVE_TREE(dataRemovePresentShow));
															dispatch(REMOVE_TREE(dataRemovePresentClone));
															dispatch(ASSIGNED_KEY(dataAssigned));
														} else {
															const dataRemovePresentShow = {
																key: 'listTreeShow',
																keyRoot: acitonRedux,
																keyObject: 'children',
																keyLocationRoot: item.locationRoot,
																idx: index,
															};

															const dataRemovePresentClone = {
																key: 'listTreeClone',
																keyRoot: acitonRedux,
																keyObject: 'children',
																keyLocationRoot: item.locationRoot,
																idx: index,
															};
															const dataAssigned = {
																keyRoot: 'treeProductCondition',
																keyTree: 'listTreeShow',
																keyListID: 'listKeyClone',
															};

															dispatch(REMOVE_TREE(dataRemovePresentShow));
															dispatch(REMOVE_TREE(dataRemovePresentClone));
															dispatch(ASSIGNED_KEY(dataAssigned));
														}
													}}
													className="cursor"
												>
													<ISvg
														name={ISvg.NAME.CROSS}
														width={8}
														height={8}
														fill={colors.oranges}
													/>
												</div>
											</div>
										</Col>
									</Row>
								</li>
							</ul>
						);
					}
				})}
			</ul>
		);
	};

	const treeProctGroup = (dataProduct, idx) => {
		return (
			<ul style={{ padding: 0 }}>
				{dataProduct.map((item, index) => {
					if (item.children && item.children.length) {
						return (
							<ul
								style={{
									listStyleType: 'none',
									padding: '6px 0px',
								}}
							>
								<li
									style={{
										fontWeight: 'bold',
										borderBottom: '0.5px dashed #7A7B7B',
										padding: '6px 6px',
									}}
								>
									{item.name}
								</li>
								{treeProct(item.children)}
							</ul>
						);
					} else {
						return (
							<ul
								style={{
									listStyleType: 'none',
									borderBottom: '0.5px dashed #7A7B7B',
								}}
							>
								<li style={{ padding: '6px 0px' }}>
									<Row gutter={[12, 0]} type="flex">
										<Col span={21}>
											<div
												style={{
													wordWrap: 'break-word',
													wordBreak: 'break-word',
													overflow: 'hidden',
													height: '100%',
													display: 'flex',
													alignItems: 'center',
												}}
											>
												<span>{item.name}</span>
											</div>
										</Col>

										<Col span={3}>
											<div
												style={{
													height: 32,
													display: 'flex',
													alignItems: 'center',
												}}
											>
												<div
													style={{
														background: 'rgba(227, 95, 75, 0.1)',
														width: 20,
														height: 20,
														borderRadius: 10,
														display: 'flex',
														justifyContent: 'center',
														alignItems: 'center',
													}}
													onClick={() => {
														const dataRemovePresentShow = {
															key: 'listTreeShow',
															keyRoot: 'treeProductCombo',
															keyObject: 'children',
															keyLocationRoot: item.locationRoot,
															idx: index,
														};

														const dataRemovePresentClone = {
															key: 'listTreeClone',
															keyRoot: 'treeProductCombo',
															keyObject: 'children',
															keyLocationRoot: item.locationRoot,
															idx: index,
														};
														const dataAssigned = {
															keyRoot: 'treeProductCombo',
															keyTree: 'listTreeShow',
															keyListID: 'listKeyClone',
														};

														dispatch(REMOVE_TREE(dataRemovePresentShow));
														dispatch(REMOVE_TREE(dataRemovePresentClone));
														dispatch(ASSIGNED_KEY(dataAssigned));
													}}
													className="cursor"
												>
													<ISvg
														name={ISvg.NAME.CROSS}
														width={8}
														height={8}
														fill={colors.oranges}
													/>
												</div>
											</div>
										</Col>
									</Row>
								</li>
							</ul>
						);
					}
				})}
			</ul>
		);
	};

	return (
		<div style={{ width: '100%', height: '100%' }}>
			<Row gutter={[0, 16]}>
				<Col span={24}>
					<div style={{ width: '100%', height: '100%' }}>
						<Row gutter={[26, 0]}>
							<Col span={12}>
								<div style={{ width: '100%', height: '100%' }}>
									<Row gutter={[0, 12]}>
										<Col span={24}>
											<div
												style={{
													height: 455,
													overflow: 'auto',
													border: '1px solid rgba(122, 123, 123, 0.5)',
													paddingLeft: 18,
													paddingRight: 18,
												}}
											>
												<ITree
													checkable
													checkedKeys={treeProductRedux.listKeyClone}
													dataLoop={dataProduct}
													defaultExpandAll
													loadingList
													arrDisabled={treeProductRedux.listArrDisabled}
													onCheck={(checkedKeys, e) => {
														const dataTree = dataProduct.map((item) => {
															let keyRoot;
															let titleRoot;
															const children = e.checkedNodes.map((item1) => {
																let splitKey = item1.key.split('-');
																if (splitKey.length === 2) {
																	if (Number(splitKey[0]) === Number(item.id)) {
																		keyRoot = item.id;
																		titleRoot = item.name;
																		const id = item1.key;
																		const name = item1.props.title;
																		const locationChildrenReal =
																			item1.props.locationChildren;
																		const locationRootReal =
																			item1.props.locationRoot;
																		const attibute_max_value =
																			item1.props.dataProps.attibute_max_value;
																		const price = item1.props.dataProps.price;
																		const priority = item1.props.dataProps.priority;
																		const quantity_sales = 0;
																		const quantity_count = 0;
																		return {
																			id,
																			name,
																			priority,
																			locationChildrenReal,
																			locationRootReal,
																			attibute_max_value,
																			price,
																			quantity_sales,
																			quantity_count,
																		};
																	}
																}
																return;
															});
															if (
																typeof keyRoot === 'undefined' ||
																typeof titleRoot === 'undefined'
															) {
																return;
															}
															const id = keyRoot;
															const name = titleRoot;
															return { id, name, children };
														});
														let dataTreeSucces = dataTree.filter(function (element) {
															return element !== undefined;
														});
														dataTreeSucces.map((item) => {
															item.children = item.children.filter((item1) => {
																return item1 !== undefined;
															});
														});

														dataTreeSucces.map((item, index) => {
															item.children.map((item1, index1) => {
																item1.locationRoot = index;
																item1.locationChildren = index1;
															});
														});

														let dataKey = [];
														dataTreeSucces.map((item) => {
															item.children.map((item1) => {
																dataKey.push(item1.id);
															});
														});

														const dataActionKey = {
															keyRoot: `${acitonRedux}`,
															initial_listClone: 'listKeyClone',
															data: dataKey,
														};

														const dataAction = {
															keyRoot: `${acitonRedux}`,
															initial_listClone: 'listTreeClone',
															data: dataTreeSucces,
														};

														if (checkGroup) {
															if (dataTreeSucces.length > 1) {
																const content = (
																	<span>
																		Khai báo các sản phẩm thuộc
																		<span style={{ fontWeight: 600 }}>
																			{' '}
																			1 nhóm.
																		</span>
																	</span>
																);
																return IError(content);
															}
														}

														dispatch(ADD_TREE(dataAction));
														dispatch(ADD_TREE(dataActionKey));
													}}
												/>
											</div>
										</Col>
										<Col span={24}>
											<div
												style={{
													width: '100%',
													display: 'flex',
													justifyContent: 'flex-end',
												}}
											>
												<IButton
													title="Thêm"
													color={colors.main}
													icon={ISvg.NAME.BUTTONRIGHT}
													styleHeight={{
														width: 140,
													}}
													isRight={true}
													onClick={() => {
														const dataAction = {
															keyRoot: `${acitonRedux}`,
															initial_listClone: 'listTreeClone',
															initial_listShow: 'listTreeShow',
														};

														dispatch(SHOW_TREE(dataAction));
													}}
												/>
											</div>
										</Col>
									</Row>
								</div>
							</Col>
							<Col span={12}>
								<div
									style={{
										height: 510,
										border: '1px solid rgba(122, 123, 123, 0.5)',
										overflow: 'auto',
									}}
								>
									{checkGroup
										? treeProctGroup(treeProductRedux.listTreeShow)
										: treeProct(treeProductRedux.listTreeShow)}
								</div>
							</Col>
						</Row>
					</div>
				</Col>
				<Col span={24}>
					<div
						style={{
							display: 'flex',
							flexDirection: 'row',
							position: 'absolute',
							top: 60,
							right: 0,
						}}
					>
						<IButton
							title="Lưu"
							color={colors.main}
							icon={ISvg.NAME.SAVE}
							styleHeight={{
								width: 140,
								marginRight: 24,
							}}
							onClick={() => {
								const dataAction = {
									keyRoot: `${acitonRedux}`,
									initial_listShow: 'listTreeShow',
									initial_listConfirm: 'listConfirm',
								};

								let arrProductShow = [];
								treeProductRedux.listTreeShow.map((item, index) => {
									if (item.children && item.children.length) {
										item.children.map((item) => {
											const id = item.id;
											const name = item.name;
											arrProductShow.push({ id, name });
										});
									}
								});

								if (arrProductShow.length <= 0) {
									const content = <span>Vui lòng chọn từ 1 sản phẩm trở lên</span>;
									return IError(content);
								} else {
									if (checkGroup) {
										if (count_save === -1 || treeProductRedux.listArrListTreeConfirm.length === 0) {
											const dataGroup = {
												keyRoot: `${acitonRedux}`,
												initial_listShow: 'listTreeShow',
												initial_listConfirm: 'listConfirm',
												conditon_reward: true,
											};
											dispatch(SAVE_TREE_GROUP_COMBO_CONDITION(dataGroup));
										} else {
											const dataGroup1 = {
												keyRoot: `${acitonRedux}`,
												initial_listShow: 'listTreeShow',
												initial_listConfirm: 'listConfirm',
												conditon_reward: true,
											};
											dispatch(SAVE_TREE_GROUP_COMBO_OVER_CONDITION(dataGroup1));
										}
									} else {
										dispatch(SAVE_TREE(dataAction));
									}

									callbackClose();
								}
							}}
						/>
						<IButton
							title="Hủy bỏ"
							color={colors.oranges}
							icon={ISvg.NAME.CROSS}
							styleHeight={{
								width: 140,
							}}
							onClick={() => {
								if (count_save === -1) {
									callbackClose();
								} else {
									if (checkGroup) {
										dispatch(CLOSE_MODAL_COMBO_CONDITION());
										callbackClose();
									} else {
										callbackClose();
									}
								}
							}}
						/>
					</div>
				</Col>
			</Row>
		</div>
	);
}
