import { Checkbox, Col, Icon, List, message, Row, Tag } from "antd";
import React, { useEffect, useState } from "react";
import { StyledITileHeading } from "../Font/font";
import { StyledSearchCustom } from "..";
import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import ITableHtml from "../../ITableHtml";
import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import {
  ADD_MODAL_THUNK,
  REMOVE_MODAL_THUNK,
  SHOW_MODAL_THUNK,
  _fetchAPIListBuusinessRegion,
  COFIRM_MODAL_THUNK,
  ADD_ALL_MODAL_THUNK,
  REMOVE_ALL_MODAL_THUNK,
} from "../../../store/reducers";

export default function UseModalBusiness(props) {
  const { callback, arrayRegion } = props;
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { modalBusiness, eventImmediateNew } = dataRoot;
  const headerTable = [
    {
      name: "STT",
      align: "center",
    },

    {
      name: "Tên chi nhánh",
      align: "left",
    },

    {
      name: "Vùng kinh doanh",
      align: "left",
    },
    {
      name: "",
      align: "center",
    },
  ];

  const _fetchAPIListRegion = async (key_word) => {
    try {
      await dispatch(
        _fetchAPIListBuusinessRegion({
          key_word: key_word,
          list_region: eventImmediateNew.event_filter.arrayRegion,
        })
      );
    } catch (error) {
    } finally {
    }
  };

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="tr-table">
        {headerTable.map((item, index) => (
          <th className={"th-table"} style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{
            textAlign: "center",
            fontWeight: 400,
            fontSize: "clamp(8px, 4vw, 14px)",
          }}
        >
          {index + 1}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.agency_name}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.business_region_name}
        </td>

        <td className="td-table">
          <div
            style={{
              background: "rgba(227, 95, 75, 0.1)",
              width: 20,
              height: 20,
              borderRadius: 10,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            onClick={() => {
              const dataAction = {
                key_root: "modalBusiness",
                key: "listBusinessShow",
                value: index,
                isShow: true,
                key1: "listBusinessAdd",
              };
              dispatch(REMOVE_MODAL_THUNK(dataAction));
            }}
            className="cursor"
          >
            <ISvg
              name={ISvg.NAME.CROSS}
              width={8}
              height={8}
              fill={colors.oranges}
            />
          </div>
        </td>
      </tr>
    ));
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Chọn Chi Nhánh vùng kinh doanh áp dụng chính sách
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={7}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <Icon
                          type="search"
                          style={{
                            fontSize: 20,
                            color: colors.icon,
                          }}
                        />
                        <StyledSearchCustom
                          style={{ width: 400 }}
                          suffix={() => {
                            return null;
                          }}
                          onPressEnter={(e) => {
                            _fetchAPIListRegion(e.target.value);
                          }}
                          placeholder="nhập kí tự tìm kiếm"
                          prefix={() => {
                            return null;
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24} style={{ textAlign: "right" }}>
                      <Tag
                        color={colors.main}
                        onClick={() => {
                          const dataAction = {
                            key_root: "modalBusiness",
                            key_add: "listBusinessAdd",
                            key_show: "listBusinessShow",
                            list: "listBusiness",
                          };
                          dispatch(ADD_ALL_MODAL_THUNK(dataAction));
                        }}
                      >
                        Thêm tất cả
                      </Tag>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          height: 400,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        <List
                          dataSource={modalBusiness.listBusiness}
                          bordered={false}
                          loading={modalBusiness.loading}
                          renderItem={(item, index) => {
                            const idx = modalBusiness.listBusinessAdd
                              .map((el) => el.agency_id)
                              .indexOf(item.agency_id);

                            return (
                              <List.Item>
                                <div style={{ width: "100%", height: "100%" }}>
                                  <Row>
                                    <Col span={24}>
                                      <Checkbox
                                        checked={idx >= 0}
                                        onChange={(e) => {
                                          let checked = e.target.checked;
                                          if (checked) {
                                            const dataAction = {
                                              key_root: "modalBusiness",
                                              key: "listBusinessAdd",
                                              value: item,
                                            };
                                            dispatch(
                                              ADD_MODAL_THUNK(dataAction)
                                            );
                                          } else {
                                            const dataAction = {
                                              key_root: "modalBusiness",
                                              key: "listBusinessAdd",
                                              value: item,
                                              keyItem: "agency_id",
                                              isShow: false,
                                            };
                                            dispatch(
                                              REMOVE_MODAL_THUNK(dataAction)
                                            );
                                          }
                                        }}
                                      >
                                        <span>{item.agency_name}</span>
                                      </Checkbox>
                                    </Col>
                                  </Row>
                                </div>
                              </List.Item>
                            );
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            const dataAction = {
                              key_root: "modalBusiness",
                              key: "listBusinessAdd",
                              key_show: "listBusinessShow",
                            };
                            dispatch(SHOW_MODAL_THUNK(dataAction));
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={17}>
                <Tag
                  color={colors.main}
                  style={{ marginBottom: 12 }}
                  onClick={() => {
                    const dataAction = {
                      key_root: "modalBusiness",
                      key_add: "listBusinessAdd",
                      key_show: "listBusinessShow",
                    };
                    dispatch(REMOVE_ALL_MODAL_THUNK(dataAction));
                  }}
                >
                  Xóa tất cả
                </Tag>
                <div
                  style={{
                    height: 510,
                    borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                    borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <ITableHtml
                    childrenBody={bodyTableProduct(
                      modalBusiness.listBusinessShow
                    )}
                    childrenHeader={headerTableProduct(headerTable)}
                    isBorder={false}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 60,
              right: 0,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 24,
              }}
              onClick={() => {
                const dataAction = {
                  key_root: "modalBusiness",
                  key: "listBusinessShow",
                  key_show: "listBusinessConfirm",
                };
                dispatch(COFIRM_MODAL_THUNK(dataAction));
                callback();
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                callback();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
