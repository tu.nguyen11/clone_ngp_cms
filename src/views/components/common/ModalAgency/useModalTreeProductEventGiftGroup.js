import { Col, message, Row, Input, Spin } from "antd";
import React, { useEffect, useState } from "react";
import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
  SHOW_TREE,
  REMOVE_TREE,
  ASSIGNED_KEY,
  CREATE_DATA_PRODUCT,
  ADD_TREE,
  REMOVE_PRODUCT,
  ADD_TREE_KEY_CLONE,
  ADD_PRODUCT_GROUP_LEVEL,
  SAVE_TREE_GROUP,
  SAVE_TREE_GROUP_OVER,
  CLOSE_MODAL_TREE,
  ASSIGN_LIST_KEY_CLONE,
  ADD_TREE_CLONE_NOT_DISABLE,
} from "../../../store/reducers";
import ITreeNew from "../Tree/ITreeNew";
import { IError } from "../../../components/common";
import { StyledITileHeading } from "../Font/font";
import useWindowSize from "../../../../utils/useWindowSize";

const { Search } = Input;

const StyledSearchCustomNew = styled(Search)`
  height: 42px;

  width: 100%;
  border: 0px !important;

  .ant-input:focus {
    box-shadow: none !important;
  }

  .ant-input-affix-wrapper .ant-input {
    padding-left: 0px !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
  .ant-input-search: not(.ant-input-search-enter-button) {
    padding-right: 6px !important;
  }
  .ant-input-suffix {
    font-size: 20px;
  }

  .ant-input {
    padding-left: 0 !important;
  }
`;

export default function useModalTreeProductEventGiftGroup(
  callbackClose = () => {},
  product_type,
  checkGroup,
  count_save
) {
  let size = useWindowSize();
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { treeProductEventGiftGroup, eventGift, dataProduct } = dataRoot;
  const treeProductRedux = treeProductEventGiftGroup;
  const acitonRedux = "treeProductEventGiftGroup";
  const [loadingList, setLoadingList] = useState(true);
  const [search, setSearch] = useState("");

  const getListTreeProduct = async (search) => {
    try {
      setLoadingList(true);
      const data = await APIService._getListTreeProductEventGift(search, 1);
      let listKeyCloneNew = [];

      treeProductEventGiftGroup.listKeyClone.forEach((item) => {
        let ids = item.split("-");
        if (ids.length === 3) {
          listKeyCloneNew.push(item);
        }
      });

      const dataAction = {
        keyRoot: "treeProductEventGiftGroup",
        key: "listKeyClone",
        value: listKeyCloneNew,
      };

      dispatch(ASSIGN_LIST_KEY_CLONE(dataAction));

      let tree = data.product_type.map((itemLv1) => {
        let arrLv1Child = itemLv1.children.map((itemLv2) => {
          let arrLv2Child = itemLv2.children.map((itemLv3) => {
            if (!itemLv3.content) {
              itemLv3.description = "";
              return itemLv3;
            } else {
              return itemLv3;
            }
          });
          return {
            ...itemLv2,
            children: [...arrLv2Child],
          };
        });
        return {
          ...itemLv1,
          children: [...arrLv1Child],
        };
      });

      const keyDispatch = {
        key: "dataProduct",
        value: tree,
      };

      dispatch(CREATE_DATA_PRODUCT(keyDispatch));
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    getListTreeProduct(search);
  }, [search]);

  const treeProct = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding: "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[12, 0]} type="flex">
                    <Col span={21}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>

                    <Col span={3}>
                      <div
                        style={{
                          height: 32,
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                          onClick={() => {
                            if (checkGroup) {
                              const dataRemovePresentShow = {
                                key: "listTreeShow",
                                keyRoot: "treeProductEventGiftGroup",
                                keyObject: "children",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };

                              const dataRemovePresentClone = {
                                key: "listTreeClone",
                                keyRoot: "treeProductEventGiftGroup",
                                keyObject: "children",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };
                              const dataAssigned = {
                                keyRoot: "treeProductEventGiftGroup",
                                keyTree: "listTreeShow",
                                keyListID: "listKeyClone",
                              };

                              dispatch(REMOVE_TREE(dataRemovePresentShow));
                              dispatch(REMOVE_TREE(dataRemovePresentClone));
                              dispatch(ASSIGNED_KEY(dataAssigned));
                            } else {
                              const dataRemovePresentShow = {
                                key: "listTreeShow",
                                keyRoot: "treeProductEventGiftGroup",
                                keyObject: "children",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };

                              const dataRemovePresentClone = {
                                key: "listTreeClone",
                                keyRoot: "treeProductEventGiftGroup",
                                keyObject: "children",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };
                              const dataAssigned = {
                                keyRoot: "",
                                keyTree: "listTreeShow",
                                keyListID: "listKeyClone",
                              };

                              dispatch(REMOVE_TREE(dataRemovePresentShow));
                              dispatch(REMOVE_TREE(dataRemovePresentClone));
                              dispatch(ASSIGNED_KEY(dataAssigned));
                            }
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill={colors.oranges}
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  const treeProct1 = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct1(item.children)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={22}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 40,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    <Col span={2}>
                      <div
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "flex-end",
                          paddingRight: 10,
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            const dataAction = {
                              keyRoot: "treeProductEventGiftGroup",
                              key_listTreeClone: "listTreeClone",
                              key_listTreeShow: "listTreeShow",
                              key_listKeyClone: "listKeyClone",
                              key_objUpdateShow: "objUpdateShow",
                              indexProduct: index,
                              product: item,
                              listTreeClone:
                                treeProductEventGiftGroup.listTreeClone,
                              listTreeShow:
                                treeProductEventGiftGroup.listTreeShow,
                              listKeyClone:
                                treeProductEventGiftGroup.listKeyClone,
                            };

                            dispatch(REMOVE_PRODUCT(dataAction));
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill="#E35F4B"
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Chọn phiên bản
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={10}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <StyledSearchCustomNew
                          style={{ width: 465, padding: 0 }}
                          onPressEnter={(e) => {
                            setLoadingList(true);
                            setSearch(e.target.value);
                          }}
                          // value={search}
                          // placeholder="Tìm kiếm theo mã, tên phiên bản"
                          placeholder="Tìm kiếm theo mã, tên phiên bản"
                        />
                      </div>
                    </Col>

                    <Col span={24}>
                      <div
                        style={{
                          height: size.width > 1600 ? 532 : 332,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingRight: 10,
                        }}
                      >
                        {loadingList ? (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              width: "100%",
                              height: "100%",
                            }}
                          >
                            <Spin />
                          </div>
                        ) : (
                          <ITreeNew
                            checkable
                            checkedKeys={treeProductEventGiftGroup.listKeyClone}
                            dataLoop={dataProduct}
                            arrDisabled={treeProductRedux.listKeyCloneDisabled}
                            defaultExpandAll
                            loadingList
                            onCheck={(checkedKeys, e) => {
                              console.log("checkedKeys: ", checkedKeys);
                              const tree = [];
                              let arrUnCheck = [];
                              if (e.checked === false) {
                                const id = e.node.props.eventKey;
                                const arrId = id.split("-");
                                switch (arrId.length) {
                                  case 1:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          item.children.forEach((item2) => {
                                            arrUnCheck.push(item2.tree_id);
                                          });
                                        }
                                      );
                                    }
                                    break;
                                  case 2:
                                    {
                                      e.node.props.dataProps.children.forEach(
                                        (item) => {
                                          arrUnCheck.push(item.tree_id);
                                        }
                                      );
                                    }
                                    break;
                                  case 3:
                                    {
                                      arrUnCheck.push(
                                        e.node.props.dataProps.tree_id
                                      );
                                    }
                                    break;
                                  default:
                                    break;
                                }
                              }

                              checkedKeys.forEach((key, _, itself) => {
                                const keys = key.split("-");
                                const [id1, id2, id3] = keys;
                                //
                                const selectedItem1 = dataProduct.find(
                                  (item) => item.id.toString() === id1
                                );

                                const selectedItem2 =
                                  selectedItem1 &&
                                  selectedItem1.children.find(
                                    (item) => item.id.toString() === id2
                                  );

                                const selectedItem3 =
                                  selectedItem2 &&
                                  selectedItem2.children.find(
                                    (item) => item.id.toString() === id3
                                  );

                                switch (keys.length) {
                                  case 1: {
                                    if (selectedItem1) {
                                      tree.push(selectedItem1);
                                    }
                                    break;
                                  }

                                  case 2: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );
                                    // let childSelectedItem2Tmp = [
                                    //   ...selectedItem2.children,
                                    // ];
                                    //
                                    // treeProductRedux.listKeyCloneDisabled.forEach(
                                    //   (itemDisabled) => {
                                    //
                                    //     let idx = childSelectedItem2Tmp.findIndex(
                                    //       (item) =>
                                    //         item.tree_id === itemDisabled
                                    //     );
                                    //     if (idx !== -1) {
                                    //       childSelectedItem2Tmp.splice(idx, 1);
                                    //     }
                                    //   }
                                    // );
                                    if (!selectedItem1InShow) {
                                      tree.unshift({
                                        ...selectedItem1,
                                        children: [
                                          selectedItem2,
                                          // {
                                          //   ...selectedItem2,
                                          //   children: childSelectedItem2Tmp,
                                          // },
                                        ],
                                      });
                                    } else {
                                      selectedItem1InShow.children.unshift(
                                        selectedItem2
                                      );
                                    }
                                    break;
                                  }
                                  case 3: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    if (
                                      itself.find(
                                        (item) => item === [id1, id2].join("-")
                                      )
                                    )
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );

                                    if (!selectedItem1InShow) {
                                      selectedItem1InShow = {
                                        ...selectedItem1,
                                        children: [
                                          { ...selectedItem2, children: [] },
                                        ],
                                      };
                                      tree.push(selectedItem1InShow);
                                    }

                                    const selectedItem2InShow = selectedItem1InShow.children.find(
                                      (item) => item.id.toString() === id2
                                    );

                                    if (!selectedItem2InShow) {
                                      selectedItem1InShow.children.push({
                                        ...selectedItem2,
                                        children: [selectedItem3],
                                      });
                                    } else {
                                      selectedItem2InShow.children.push(
                                        selectedItem3
                                      );
                                    }
                                  }
                                  default:
                                    break;
                                }
                              });

                              const dataActionKey = {
                                keyRoot: "treeProductEventGiftGroup",
                                initial_listClone: "listKeyClone",
                                data:
                                  e.checked === true
                                    ? { checked: true, arr: [...checkedKeys] }
                                    : { checked: false, arr: [...arrUnCheck] },
                              };

                              const dataAction = {
                                keyRoot: "treeProductEventGiftGroup",
                                initial_listClone: "listTreeClone",
                                data: {
                                  checked: e.checked,
                                  tree: tree,
                                },
                              };
                              dispatch(ADD_TREE(dataAction));
                              dispatch(ADD_TREE_KEY_CLONE(dataActionKey));
                            }}
                          />
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            if (treeProductRedux.listTreeClone.length === 0) {
                              // message.error("Vui lòng chọn phiên bản");
                              message.error("Vui lòng chọn phiên bản");
                            } else {
                              if (treeProductRedux.listArrDisabled.length > 0) {
                                let listTreeCloneTmp = JSON.parse(
                                  JSON.stringify(treeProductRedux.listTreeClone)
                                );
                                treeProductRedux.listTreeClone.forEach(
                                  (itemLv1, indexLv1) => {
                                    if (
                                      itemLv1.children &&
                                      itemLv1.children.length
                                    ) {
                                      itemLv1.children.forEach(
                                        (itemLv2, indexLv2) => {
                                          if (
                                            itemLv2.children &&
                                            itemLv2.children.length
                                          ) {
                                            itemLv2.children.forEach(
                                              (itemLv3, indexLv3) => {
                                                const itemExists = treeProductRedux.listKeyCloneDisabled.find(
                                                  (item) =>
                                                    itemLv3.tree_id === item
                                                );
                                                if (itemExists) {
                                                  let indexRemove = listTreeCloneTmp[
                                                    indexLv1
                                                  ].children[
                                                    indexLv2
                                                  ].children.findIndex(
                                                    (item) =>
                                                      item.tree_id ===
                                                      itemExists
                                                  );
                                                  listTreeCloneTmp[
                                                    indexLv1
                                                  ].children[
                                                    indexLv2
                                                  ].children.splice(
                                                    indexRemove,
                                                    1
                                                  );
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );

                                let listTreeCloneNew = JSON.parse(
                                  JSON.stringify(listTreeCloneTmp)
                                );
                                listTreeCloneTmp.forEach(
                                  (itemLv1, indexLv1) => {
                                    itemLv1.children.forEach(
                                      (itemLv2, indexLv2) => {
                                        if (itemLv2.children.length === 0) {
                                          const idxLv2 = listTreeCloneNew[
                                            indexLv1
                                          ].children.findIndex((itemRemove) => {
                                            return (
                                              itemRemove.tree_id ===
                                              itemLv2.tree_id
                                            );
                                          });
                                          listTreeCloneNew[
                                            indexLv1
                                          ].children.splice(idxLv2, 1);
                                        }
                                      }
                                    );
                                  }
                                );

                                const dataAction1 = {
                                  keyRoot: `${acitonRedux}`,
                                  initial_listClone: "listTreeClone",
                                  listTreeCloneNew: listTreeCloneNew,
                                };

                                dispatch(
                                  ADD_TREE_CLONE_NOT_DISABLE(dataAction1)
                                );
                              }

                              const dataAction = {
                                keyRoot: `${acitonRedux}`,
                                initial_listClone: "listTreeClone",
                                initial_listShow: "listTreeShow",
                              };

                              dispatch(SHOW_TREE(dataAction));
                            }
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={14}>
                <Row>
                  <Col span={24}>
                    <div
                      style={{
                        borderTop: "1px solid rgba(122, 123, 123, 0.5)",
                        borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                        borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                        padding: "9px 6px",
                        color: "#000",
                        fontWeight: 600,
                      }}
                    >
                      {/* phiên bản */}
                      Phiên bản
                    </div>
                  </Col>
                  <Col span={24}>
                    <div
                      style={{
                        height: size.width > 1600 ? 600 : 400,
                        border: "1px solid rgba(122, 123, 123, 0.5)",
                        overflowY: "auto",
                        overflowX: "hidden",
                      }}
                    >
                      {treeProductEventGiftGroup.listTreeShow.length > 0
                        ? treeProct1(treeProductEventGiftGroup.listTreeShow)
                        : null}
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 40,
              right: -25,
            }}
          >
            <div style={{ display: "flex" }}>
              <IButton
                title="Lưu"
                color={colors.main}
                icon={ISvg.NAME.SAVE}
                styleHeight={{
                  width: 140,
                }}
                onClick={() => {
                  let arrProductShow = [];
                  treeProductRedux.listTreeShow.forEach((itemLv1) => {
                    if (itemLv1.children && itemLv1.children.length) {
                      itemLv1.children.forEach((itemLv2) => {
                        if (itemLv2.children && itemLv2.children.length) {
                          itemLv2.children.forEach((itemLv3) => {
                            const id = itemLv3.id;
                            const name = itemLv3.name;
                            const tree_id = itemLv3.tree_id;
                            arrProductShow.push({ id, name, tree_id });
                          });
                        }
                      });
                    }
                  });

                  if (arrProductShow.length <= 0) {
                    const content = (
                      <span>Vui lòng chọn tối thiểu 1 phiên bản trở lên</span>
                    );
                    return IError(content);
                  } else {
                    if (
                      treeProductEventGiftGroup.listArrListTreeConfirm
                        .length === 0
                    ) {
                      let dataAction = {
                        conditions_apply: eventGift.conditions_apply,
                        dataSaleNew: {
                          number_to: 0,
                          discount: 0,
                          valid_number_to: false,
                          valid_discount: false,
                        },
                        dataQuantityNew: {
                          number_to: 0,
                          discount: 0,
                          conversion_rate: 0,
                          valid_number_to: false,
                          valid_discount: false,
                          valid_conversion_rate: false,
                        },
                      };
                      dispatch(ADD_PRODUCT_GROUP_LEVEL(dataAction));
                    } else {
                      if (count_save === -1) {
                        let dataAction = {
                          conditions_apply: eventGift.conditions_apply,
                          dataSaleNew: {
                            number_to: 0,
                            discount: 0,
                            valid_number_to: false,
                            valid_discount: false,
                          },
                          dataQuantityNew: {
                            number_to: 0,
                            discount: 0,
                            conversion_rate: 0,
                            valid_number_to: false,
                            valid_discount: false,
                            valid_conversion_rate: false,
                          },
                        };
                        dispatch(ADD_PRODUCT_GROUP_LEVEL(dataAction));
                      }
                    }
                  }

                  if (
                    count_save === -1 ||
                    treeProductEventGiftGroup.listArrListTreeConfirm.length ===
                      0
                  ) {
                    const dataGroup = {
                      keyRoot: `${acitonRedux}`,
                      initial_listShow: "listTreeShow",
                      initial_listConfirm: "listConfirm",
                      initial_listKeyConfirm: "listKeyConfirm",
                    };
                    dispatch(SAVE_TREE_GROUP(dataGroup));
                  } else {
                    const dataGroup1 = {
                      keyRoot: `${acitonRedux}`,
                      initial_listShow: "listTreeShow",
                      initial_listConfirm: "listConfirm",
                      initial_listKeyConfirm: "listKeyConfirm",
                    };
                    dispatch(SAVE_TREE_GROUP_OVER(dataGroup1));
                  }

                  //   const dataAction = {
                  //     keyRoot: "treeProductEventGiftGroup",
                  //     initial_listShow: "listTreeShow",
                  //     initial_listConfirm: "listConfirm",
                  //     initial_listKeyConfirm: "listKeyConfirm",
                  //     dataListKeyConfirm: treeProductEventGiftGroup.listKeyClone,
                  //   };

                  //   dispatch(SAVE_TREE_NEW(dataAction));

                  callbackClose();
                }}
              />
              <IButton
                title="Hủy bỏ"
                color={colors.oranges}
                icon={ISvg.NAME.CROSS}
                styleHeight={{
                  width: 140,
                  marginLeft: 15,
                }}
                onClick={() => {
                  if (count_save === -1) {
                    callbackClose();
                  } else {
                    dispatch(CLOSE_MODAL_TREE());
                    callbackClose();
                  }
                  //   const dataAction = {
                  //     keyRoot: "treeProductEventGiftGroup",
                  //     initial_listClone: "listTreeClone",
                  //     initial_listShow: "listTreeShow",
                  //     dataListConfirm: treeProductEventGiftGroup.listConfirm,
                  //     initial_listKeyClone: "listKeyClone",
                  //     dataListKeyConfirm:
                  //       treeProductEventGiftGroup.listKeyConfirm,
                  //   };

                  //   dispatch(CANCEL_TREE_NEW(dataAction));
                }}
              />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}
