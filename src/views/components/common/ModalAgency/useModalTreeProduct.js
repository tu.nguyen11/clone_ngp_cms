import { Col, message, Row } from "antd";
import React, { useEffect, useState } from "react";

import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import {
  SAVE_TREE,
  ADD_TREE,
  SHOW_TREE,
  CANCEL_TREE,
  REMOVE_TREE,
  ASSIGNED_KEY,
  CREATE_DATA_PRODUCT,
} from "../../../store/reducers";
import ITree from "../Tree/ITree";

export default function useModalTreeProduct(callbackClose = () => {}) {
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { treeProduct, event, dataProduct } = dataRoot;
  const [loadingList, setLoadingList] = useState(true);

  const _getListTreeProduct = async () => {
    try {
      const data = await APIService._getListTreeProduct(0);
      data.cate.lstCate.forEach((item) => {
        item.children.map((item1) => {
          item1.priority = 1;
        });
      });
      const keyDispatch = {
        key: "dataProduct",
        value: data.cate.lstCate,
      };
      dispatch(CREATE_DATA_PRODUCT(keyDispatch));
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    _getListTreeProduct();
  }, []);

  const treeProct = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding: "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[12, 0]} type="flex">
                    <Col span={21}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>

                    <Col span={3}>
                      <div
                        style={{
                          height: 32,
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                          onClick={() => {
                            const dataRemovePresentShow = {
                              key: "listTreeShow",
                              keyRoot: "treeProduct",
                              keyObject: "children",
                              keyLocationRoot: item.locationRoot,
                              idx: index,
                            };

                            const dataRemovePresentClone = {
                              key: "listTreeClone",
                              keyRoot: "treeProduct",
                              keyObject: "children",
                              keyLocationRoot: item.locationRoot,
                              idx: index,
                            };
                            const dataAssigned = {
                              keyRoot: "treeProduct",
                              keyTree: "listTreeShow",
                              keyListID: "listKeyClone",
                            };

                            dispatch(REMOVE_TREE(dataRemovePresentShow));
                            dispatch(REMOVE_TREE(dataRemovePresentClone));
                            dispatch(ASSIGNED_KEY(dataAssigned));
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill={colors.oranges}
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={12}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <div
                        style={{
                          height: 455,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        <ITree
                          checkable
                          checkedKeys={treeProduct.listKeyClone}
                          dataLoop={dataProduct}
                          defaultExpandAll
                          loadingList
                          // blockNode={true}
                          // checkStrictly={true}
                          onCheck={(checkedKeys, e) => {
                            const dataTree = dataProduct.map((item) => {
                              let keyRoot;
                              let titleRoot;
                              const children = e.checkedNodes.map((item1) => {
                                let splitKey = item1.key.split("-");
                                if (splitKey.length === 2) {
                                  if (Number(splitKey[0]) === Number(item.id)) {
                                    keyRoot = item.id;
                                    titleRoot = item.name;
                                    const id = item1.key;
                                    const name = item1.props.title;
                                    const locationChildrenReal =
                                      item1.props.locationChildren;
                                    const locationRootReal =
                                      item1.props.locationRoot;

                                    const priority =
                                      item1.props.dataProps.priority;

                                    return {
                                      id,
                                      name,
                                      priority,
                                      locationChildrenReal,
                                      locationRootReal,
                                    };
                                  }
                                }
                                return;
                              });
                              if (
                                typeof keyRoot === "undefined" ||
                                typeof titleRoot === "undefined"
                              ) {
                                return;
                              }
                              const id = keyRoot;
                              const name = titleRoot;
                              return { id, name, children };
                            });
                            let dataTreeSucces = dataTree.filter(function (
                              element
                            ) {
                              return element !== undefined;
                            });
                            dataTreeSucces.map((item) => {
                              item.children = item.children.filter((item1) => {
                                return item1 !== undefined;
                              });
                            });

                            dataTreeSucces.map((item, index) => {
                              item.children.map((item1, index1) => {
                                item1.locationRoot = index;
                                item1.locationChildren = index1;
                              });
                            });

                            let dataKey = [];
                            dataTreeSucces.map((item) => {
                              item.children.map((item1) => {
                                dataKey.push(item1.id);
                              });
                            });

                            const dataActionKey = {
                              keyRoot: "treeProduct",
                              initial_listClone: "listKeyClone",
                              data: dataKey,
                            };

                            const dataAction = {
                              keyRoot: "treeProduct",
                              initial_listClone: "listTreeClone",
                              data: dataTreeSucces,
                            };
                            dispatch(ADD_TREE(dataAction));
                            dispatch(ADD_TREE(dataActionKey));
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            const dataAction = {
                              keyRoot: "treeProduct",
                              initial_listClone: "listTreeClone",
                              initial_listShow: "listTreeShow",
                            };

                            dispatch(SHOW_TREE(dataAction));
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={12}>
                <div
                  style={{
                    height: 510,
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                    overflow: "auto",
                  }}
                >
                  {treeProct(treeProduct.listTreeShow)}
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 60,
              right: 0,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 24,
              }}
              onClick={() => {
                const dataAction = {
                  keyRoot: "treeProduct",
                  initial_listShow: "listTreeShow",
                  initial_listConfirm: "listConfirm",
                  assigned_condition: true,
                };

                dispatch(SAVE_TREE(dataAction));

                callbackClose();
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                // dispatch(
                //   CANCEL_MODAL({
                //     keyInitial_root: "modalS2",
                //     keyInitial_listShow: "listS2Show",
                //     keyInitial_Add: "listS2Add",
                //     keyInitial_listAddClone: "listS2AddClone",
                //     keyInitial_listClone: "listS2Clone",
                //     keyInitial_list: "listS2",
                //   })
                // );
                // const dataAction = {
                //   keyRoot: "treeProduct",
                //   initial_listClone: "listTreeClone",
                // };
                // dispatch(CANCEL_TREE(dataAction));

                callbackClose();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
