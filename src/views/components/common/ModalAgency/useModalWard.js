import { Checkbox, Col, Icon, List, message, Row, Tag } from "antd";
import React, { useEffect, useState } from "react";
import { StyledITileHeading } from "../Font/font";
import { StyledSearchCustom } from "..";
import { colors } from "../../../../assets";
import IButton from "../../IButton";
import ISvg from "../../ISvg";
import ITableHtml from "../../ITableHtml";
import { useDispatch, useSelector } from "react-redux";
import {
  ADD_MODAL_THUNK,
  REMOVE_MODAL_THUNK,
  SHOW_MODAL_THUNK,
  _fetchAPIListWard,
  COFIRM_MODAL_THUNK,
  ADD_ALL_MODAL_THUNK,
  REMOVE_ALL_MODAL_THUNK,
} from "../../../store/reducers";

export default function useModalWard(props) {
  const { callback } = props;
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { modalWard, modalDistrict } = dataRoot;
  const headerTable = [
    {
      name: "STT",
      align: "center",
    },

    {
      name: "Tên Quận / Xã",
      align: "left",
    },

    {
      name: "",
      align: "center",
    },
  ];

  const _fetchAPIList = async (search) => {
    try {
      await dispatch(
        _fetchAPIListWard({
          search: search,
          district_id: modalDistrict.listDistrictConfirm.map((item) => item.id),
        })
      );
    } catch (error) {
    } finally {
    }
  };

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="tr-table">
        {headerTable.map((item, index) => (
          <th className={"th-table"} style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{
            textAlign: "center",
            fontWeight: 400,
            fontSize: "clamp(8px, 4vw, 14px)",
          }}
        >
          {index + 1}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.name}
        </td>

        <td className="td-table">
          <div
            style={{
              background: "rgba(227, 95, 75, 0.1)",
              width: 20,
              height: 20,
              borderRadius: 10,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            onClick={() => {
              const dataAction = {
                key_root: "modalWard",
                key: "listWardShow",
                value: index,
                isShow: true,
                key1: "listWardAdd",
              };
              dispatch(REMOVE_MODAL_THUNK(dataAction));
            }}
            className="cursor"
          >
            <ISvg
              name={ISvg.NAME.CROSS}
              width={8}
              height={8}
              fill={colors.oranges}
            />
          </div>
        </td>
      </tr>
    ));
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Chọn Phường Xã áp dụng chính sách
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={7}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <Icon
                          type="search"
                          style={{
                            fontSize: 20,
                            color: colors.icon,
                          }}
                        />
                        <StyledSearchCustom
                          style={{ width: 400 }}
                          suffix={() => {
                            return null;
                          }}
                          onPressEnter={(e) => {
                            _fetchAPIList(e.target.value);
                          }}
                          placeholder="nhập kí tự tìm kiếm"
                          prefix={() => {
                            return null;
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24} style={{ textAlign: "right" }}>
                      <Tag
                        color={colors.main}
                        onClick={() => {
                          const dataAction = {
                            key_root: "modalWard",
                            key_add: "listWardAdd",
                            key_show: "listWardShow",
                            list: "listWard",
                          };
                          dispatch(ADD_ALL_MODAL_THUNK(dataAction));
                        }}
                      >
                        Thêm tất cả
                      </Tag>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          height: 400,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        <List
                          dataSource={modalWard.listWard}
                          bordered={false}
                          loading={modalWard.loading}
                          renderItem={(item, index) => {
                            const idx = modalWard.listWardAdd
                              .map((el) => el.id)
                              .indexOf(item.id);

                            return (
                              <List.Item>
                                <div style={{ width: "100%", height: "100%" }}>
                                  <Row>
                                    <Col span={24}>
                                      <Checkbox
                                        checked={idx >= 0}
                                        onChange={(e) => {
                                          let checked = e.target.checked;
                                          if (checked) {
                                            const dataAction = {
                                              key_root: "modalWard",
                                              key: "listWardAdd",
                                              value: item,
                                            };
                                            dispatch(
                                              ADD_MODAL_THUNK(dataAction)
                                            );
                                          } else {
                                            const dataAction = {
                                              key_root: "modalWard",
                                              key: "listWardAdd",
                                              value: item,
                                              isShow: false,
                                              keyItem: "id",
                                            };
                                            dispatch(
                                              REMOVE_MODAL_THUNK(dataAction)
                                            );
                                          }
                                        }}
                                      >
                                        <span>{item.name}</span>
                                      </Checkbox>
                                    </Col>
                                  </Row>
                                </div>
                              </List.Item>
                            );
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            const dataAction = {
                              key_root: "modalWard",
                              key: "listWardAdd",
                              key_show: "listWardShow",
                            };
                            dispatch(SHOW_MODAL_THUNK(dataAction));
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={17}>
                <Tag
                  color={colors.main}
                  style={{ marginBottom: 12 }}
                  onClick={() => {
                    const dataAction = {
                      key_root: "modalWard",
                      key_add: "listWardAdd",
                      key_show: "listWardShow",
                    };
                    dispatch(REMOVE_ALL_MODAL_THUNK(dataAction));
                  }}
                >
                  Xóa tất cả
                </Tag>
                <div
                  style={{
                    height: 510,
                    borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                    borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <ITableHtml
                    childrenBody={bodyTableProduct(modalWard.listWardShow)}
                    childrenHeader={headerTableProduct(headerTable)}
                    isBorder={false}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 60,
              right: 0,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 24,
              }}
              onClick={() => {
                const dataAction = {
                  key_root: "modalWard",
                  key: "listWardShow",
                  key_show: "listWardConfirm",
                };
                dispatch(COFIRM_MODAL_THUNK(dataAction));
                callback();
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                callback();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
