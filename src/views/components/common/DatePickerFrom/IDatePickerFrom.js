import { DatePicker } from "antd";
import React from "react";
const { default: styled } = require("styled-components");

const StyledDataPicker = styled(DatePicker)`
  height: ${(props) => props.padding} !important;
  width: 100%;
  padding: ${(props) => props.padding} !important;
  border-bottom: ${(props) =>
    props.isBorderBottom ? "1px solid #cdcece !important" : "unset"};
  .ant-input {
    padding: ${(props) => props.paddingInput} !important;
  }

  /* border: unset !important; */
`;

export default function IDatePickerFrom(props) {
  return <StyledDataPicker allowClear={false} {...props} />;
}
