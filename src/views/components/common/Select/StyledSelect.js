import React from "react";
const { default: styled } = require("styled-components");
const { Select } = require("antd");

const StyledSelect = styled(Select)`
  min-height: ${(props) =>
    !props.minHeight ? "42px" : props.minHeight} !important ;
  min-width: ${(props) => props.minWidth} !important ;
  border-bottom: ${(props) => props.isBorderBottom}!important;

  @media screen and (max-width: 1300px) {
    min-width: 140px !important;
  }
  .ant-select-selection__rendered {
    margin-left: ${(props) => props.marginLeft} !important;
  }

  .ant-select-selector {
    min-height: 42px !important;
    display: "flex";
    align-items: center;
    padding: ${(props) => (props.ispadding ? "0px" : "0px 11px")}!important;
  }
  .ant-select {
    border: none !important;
  }
  .ant-select-selection-item {
    font-weight: ${(props) => props.fontWeight};
  }
  .ant-select-selection-search-input {
    min-height: 42px !important;
  }
  .ant-select-selection-placeholder {
    color: #22232b;
    font-style: normal;
    font-weight: normal;
    opacity: 1;
  }
  .ant-select-selector {
    padding: ${(props) => props.padding} !important;
  }
  .ant-select-selection-placeholder {
    color: ${(props) => props.colorPlaceholder} !important;
  }
`;
const { Option } = Select;

export default function StyledISelect(props) {
  const { data, isSearch = false } = props;
  let dataProps = data;
  return (
    <StyledSelect {...props} style={{ minHeight: 42, ...props.style }}>
      {dataProps.map((item) =>
        isSearch ? (
          <Option key={item.key} value={item.value}>
            {item.value}
          </Option>
        ) : (
          <Option key={item.key} value={item.key}>
            {item.value}
          </Option>
        )
      )}
    </StyledSelect>
  );
}
