import { Tree } from "antd";
import React from "react";
import styled from "styled-components";
const { TreeNode } = Tree;

const StyledTree = styled(Tree)`
  &.ant-tree li .ant-tree-node-content-wrapper {
    border-bottom: 0.5px dashed #7a7b7b;
    width: 100%;
  }
  &.ant-tree-title {
    font-weight: bold;
  }
  overflow: hidden;
`;

export default function ITree(props) {
  const { dataLoop = [], arrDisabled = [] } = props;

  const loopTree = (data) => {
    return data.map((item, index) => {
      if (item.children && item.children.length) {
        return (
          <TreeNode
            key={item.tree_id || item.id}
            title={<span style={{ fontWeight: 600 }}>{item.name}</span>}
            dataProps={item}
          >
            {loopTree(item.children)}
          </TreeNode>
        );
      }
      const is_disabled = arrDisabled.indexOf(item.tree_id);
      return (
        <TreeNode
          disabled={is_disabled > -1}
          key={item.tree_id || item.id}
          title={item.name}
          dataProps={item}
        />
      );
    });
  };

  return <StyledTree {...props}>{loopTree(dataLoop)}</StyledTree>;
}
