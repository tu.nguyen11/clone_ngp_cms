import { Tree } from 'antd'
import React from 'react'
import styled from 'styled-components'
const { TreeNode } = Tree

const StyledTree = styled(Tree)`
  &.ant-tree li .ant-tree-node-content-wrapper {
    border-bottom: 0.5px dashed #7a7b7b;
    width: 100%;
  }
  &.ant-tree-title {
    font-weight: bold;
  }
  overflow: hidden;
`

export default function ITree (props) {
  const {
    dataLoop = [],
    keyRoot = 'id',
    nameRoot = 'name',
    keyChildren = 'id',
    nameChildren = 'name'
  } = props

  const loopTree = (data, idx, indexRoot, indexChldren) =>
    data.map((item, index) => {
      let keyRootTemp = undefined
      let locationRootTemp = indexRoot
      let locationChildrenTemp = indexChldren

      if (item.children && item.children.length) {
        keyRootTemp = item.id
        locationRootTemp = index
        return (
          <TreeNode
            key={item[keyRoot]}
            title={<span style={{ fontWeight: 600 }}>{item[nameRoot]}</span>}
          >
            {loopTree(
              item.children,
              keyRootTemp,
              locationRootTemp,
              locationChildrenTemp
            )}
          </TreeNode>
        )
      }
      locationChildrenTemp = index
      return (
        <TreeNode
          key={
            typeof idx === 'undefined'
              ? item[keyChildren]
              : `${idx}-${item[keyChildren]}`
          }
          title={item[nameChildren]}
          dataProps={item}
          locationRoot={locationRootTemp}
          locationChildren={locationChildrenTemp}
        />
      )
    })
  return (
    <StyledTree {...props}>{loopTree(dataLoop, undefined, 0, 0)}</StyledTree>
  )
}
