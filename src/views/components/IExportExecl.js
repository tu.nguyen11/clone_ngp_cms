import React, { Component } from "react";
import { CSVLink } from "react-csv";

import IButton from "./IButton";
import ISvg from "./ISvg";
import { message } from "antd";
import { colors } from "../../assets";

export default class IExportExecl extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { layoutTable, fileName, title } = this.props; // layoutTable định dạng column execl , fileName tếp dowlonad có tên là gì , title là value hiển thị button

    return (
      <CSVLink data={layoutTable} filename={fileName}>
        <IButton
          icon={ISvg.NAME.DOWLOAND}
          title={title}
          color={colors.main}
          style={{ marginRight: 20 }}
        />
      </CSVLink>
    );
  }
}
