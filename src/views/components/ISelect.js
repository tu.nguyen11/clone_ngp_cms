import { Select, Tooltip } from "antd";
import React, { Component } from "react";

import "./style.css";
const { Option } = Select;

export default class ISelect extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { data, select = false } = this.props; // type : button là kích thước button , select kich thước flex:1
    const {
      isBackground = true,
      isBorderBottom = true,
      minWidthDropdown,
      isTooltip = false,
    } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: isBorderBottom ? "1px solid #cdcece" : "none",
          width: "100%",
        }}
      >
        <Select
          {...this.props}
          disabled={!select}
          size="large"
          dropdownStyle={{
            minWidth: data.length === 0 ? 0 : minWidthDropdown,
          }}
        >
          {data.map((item, index) => {
            return (
              <Option
                value={
                  item.key === undefined || item.key === null
                    ? item.id
                    : item.key
                }
                key={item.key || item.id}
                dataProps={item}
              >
                {isTooltip ? (
                  <Tooltip title={item.value || item.name}>
                    {item.value || item.name}
                  </Tooltip>
                ) : (
                  item.value || item.name
                )}
              </Option>
            );
          })}
        </Select>
      </div>
    );
  }
}
