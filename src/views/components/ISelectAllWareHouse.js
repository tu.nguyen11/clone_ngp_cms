import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectAllWareHouse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listWareHouse: []
    };
  }

  componentDidMount() {
    this._getAPIListWarehouse("");
  }

  _getAPIListWarehouse = async keySearch => {
    try {
      const data = await APIService._getSelectListWarehouse(keySearch);

      let dataNew = data.list_type.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value
        };
      });

      if (!this.props.all)
        dataNew.unshift({
          value: "Tất cả các kho",
          key: 0
        });
      this.setState({
        listWareHouse: dataNew
      });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { defaultValue, style, all = true } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%"
        }}
      >
        <Select
          {...this.props}
          // disabled={!select}
          size="large"
          defaultValue="Chọn kho"
        >
          {this.state.listWareHouse.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
