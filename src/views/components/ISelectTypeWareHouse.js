import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import "./style.css";
const { Option } = Select;
const data = [
  { key: 0, value: "Tất cả loại hình" },
  { key: 1, value: "Nhập kho" },
  { key: 2, value: "Xuât kho" },
  { key: 3, value: "Xuất chuyển kho" }
];
export default class ISelectTypeWareHouse extends Component {
  constructor(props) {
    super(props);
  }
  componentWillReceiveProps(nextprops) {}
  render() {
    const { style } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
      style={{
        background: isBackground ? "white" : "transparent",
        border: isBackground ? "1px solid #cdcece" : "none",
        borderBottom: "1px solid #cdcece",
        width: "100%"
      }}
    >
      <Select
        {...this.props}
        defaultValue
        style={{
        
        }}
        size="large"
      >
        {data.map((item, index) => (
          <Option value={item.key}>{item.value}</Option>
        ))}
      </Select>
    </div>
      
    );
  }
}
