import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import "./style.css";
const { Option } = Select;
let data = [
  { key: 0, value: "Ẩn" },
  { key: 1, value: "Hiện" }
];
let dataStore = [
  { key: 0, value: "Ngưng hoạt động" },
  { key: 1, value: "Đang hoạt động" }
];
let dataSaleMan = [
  { key: 0, value: "Ngưng hoạt động" },
  { key: 1, value: "Đang hoạt động" }
];

let dataEvent = [
  { key: -1, value: "Ngưng hoạt động" },
  { key: 1, value: "Đang hoạt động" }
];
export default class ISelectStatus extends Component {
  constructor(props) {
    super(props);
  }
  _get = type => {
    switch (type) {
      case "EVENT":
        return dataEvent.map((item, index) => (
          <Option value={item.key}>{item.value}</Option>
        ));
      case "STORE":
        return dataStore.map((item, index) => (
          <Option value={item.key}>{item.value}</Option>
        ));
      case "SALEMAN":
        return dataSaleMan.map((item, index) => (
          <Option value={item.key}>{item.value}</Option>
        ));

      default:
        return data.map((item, index) => (
          <Option value={item.key}>{item.value}</Option>
        ));
    }
  };
  render() {
    const { style, type, isBackground = true } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%"
        }}
      >
        <Select {...this.props} defaultValue style={{}} size="large">
          {this._get(type)}
        </Select>
      </div>
    );
  }
}
