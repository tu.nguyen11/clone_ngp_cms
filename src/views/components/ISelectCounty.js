import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectCounty extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCounty: [],
      search: "",
    };
  }

  componentDidMount() {
    const { city_id, callAPI } = this.props;

    if (callAPI) return this._getAPIListCounty(city_id, this.state.search);
    return;
  }

  _getAPIListCounty = async (city_id, search) => {
    const data = await APIService._getListCounty(city_id, search);

    const dataNew = data.district.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    this.setState({
      listCounty: dataNew,
    });
  };

  render() {
    const { style, callAPI, city_id, isBackground = true } = this.props; // CallAPI false không thực hiện , true thực hiện

    return (
      <div
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%",
        }}
      >
        <Select
          {...this.props}
          style={{
            ...style,
            width: "100%",
            fontSize: "1em",
            height: 40,
            color: "black",
            borderWidth: 0,
          }}
          size="large"
          optionFilterProp="children"
          showSearch
          onSearch={(keySearch) => {
            this.setState(
              {
                search: keySearch,
              },
              () => this._getAPIListCounty(city_id, this.state.search)
            );
          }}
          disabled={!callAPI}
        >
          {this.state.listCounty.map((item, index) => (
            <Option key={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
