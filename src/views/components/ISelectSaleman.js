import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";
import { Container, Col, Row } from "reactstrap";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectSaleman extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list_saleman: [],
      from_limit: 0,
      to_limit: 10,
      route_id: this.props.route_id,
    };
    this.handleScrollSelect = this.handleScrollSelect.bind(this);
  }

  componentDidMount() {
    this._APIgetSelectSaleman(
      this.state.route_id,
      this.state.from_limit,
      this.state.to_limit
    );
  }

  _APIgetSelectSaleman = async (route_id, from_limit, to_limit) => {
    const data = await APIService._getSelectSaleman(
      route_id,
      from_limit,
      to_limit
    );

    const dataNew = data.list_saleman.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    this.setState({
      list_saleman: dataNew,
    });
  };

  handleScrollSelect = async (e) => {
    const element = e.target;
    console.log(element.scrollHeight);
    if (element.scrollHeight == element.clientHeight + element.scrollTop) {
      let from_limit = this.state.from_limit + 10;
      let to_limit = this.state.to_limit + 10;
      if (to_limit <= this.state.to_limit) {
        this.setState({
          to_limit: this.state.to_limit,
          from_limit: this.state.from_limit,
        });

        const data = await APIService._APIgetSelectSaleman(
          this.state.route_id,
          this.state.from_limit,
          this.state.to_limit
        );
        data.list_saleman.map((item, index) => {
          const key = item.id;
          const value = item.name;
          return {
            key,
            value,
          };
        });

        const dataNew = [...this.state.list_saleman, ...data.list_saleman];
        this.setState({
          list_saleman: dataNew,
        });
      }
      return;
    }
  };

  render() {
    const { style } = this.props;
    const { isBackground = true } = this.props;

    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%",
        }}
      >
        <Select
          {...this.props}
          defaultValue
          onPopupScroll={this.handleScrollSelect}
          style={{
            ...style,
          }}
          size="large"
        >
          {this.state.list_saleman.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
