import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectAllTypeShipper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listTypeShipper: []
    };
  }

  componentDidMount() {
    this._getAPIListTypeShipper("");
  }

  _getAPIListTypeShipper = async () => {
    try {
      const data = await APIService._getListTypeShipper();

      let dataNew = data.list_type_shipper.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value
        };
      });
      dataNew.unshift({
        value: "Tất cả các loại",
        key: 0
      });

      this.setState({
        listTypeShipper: dataNew
      });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { defaultValue, style } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%"
        }}
      >
        <Select {...this.props} defaultValue style={{}} size="large">
          {this.state.listTypeShipper.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
