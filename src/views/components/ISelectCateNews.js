import { Select } from "antd";
import React, { Component } from "react";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectCateNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cate: [],
      type: "",
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.type !== prevState.type) {
      return {
        type: nextProps.type,
      };
    } else return null;
  }
  componentDidUpdate(nextProps, prevState) {
    if (prevState.type !== this.state.type) {
      this._APIgetDropListCateNews(this.state.type);
    }
  }

  _APIgetDropListCateNews = async (type) => {
    try {
      const { all = true } = this.props;
      const data = await APIService._getDropListCateNews(type);
      let dataNews = data.cate.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      if (all) {
        dataNews.unshift({ key: -1, value: "Tất cả" });
      }
      this.setState({
        cate: dataNews,
      });
    } catch (err) {
      console.log(err);
    }
  };

  componentDidMount() {
    this._APIgetDropListCateNews(this.state.type);
  }

  render() {
    const { isBackground = true, style } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%",
        }}
      >
        <Select
          {...this.props}
          style={{
            width: "100%",
            fontSize: "1em",
            height: 42,

            color: "black",
            ...style,
            borderBotom: "1px solid #ccc",
          }}
          size="large"
        >
          {this.state.cate.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
