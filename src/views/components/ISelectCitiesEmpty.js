import { Select } from "antd";
import React, { Component } from "react";
import { colors } from "../../assets";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectCitiesEmpty extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCity: []
    };
  }

  componentDidMount() {
    this._getListCitiesEmpty(this.props.supplier_id);
  }

  _getListCitiesEmpty = async supplier_id => {
    const data = await APIService._getListCitiesEmpty(supplier_id);

    const dataNew = data.list_city.map((item, index) => {
      const key = item.city_id;
      const value = item.city_name;
      return {
        key,
        value
      };
    });
    this.setState({
      listCity: dataNew
    });
  };

  render() {
    const { style } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%"
        }}
      >
        <Select
          {...this.props}
          style={{
            ...style,
            width: "100%",
            fontSize: "1em",
            height: 42,
            color: "black"
            // borderWidth: 0,
          }}
          size="large"
        >
          {this.state.listCity.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
