import { Button } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import "./ISvg.css";
import { colors } from "../../assets";
import { ITitle } from "./";

class IButton extends Component {
  render() {
    const {
      icon = true,
      title,
      color,
      type = "primary",
      hideColor = false,
      styleHeight,
      iconRight = false,
    } = this.props;
    var style = this.props.style;
    if (!style) {
      style = {};
    }
    return (
      // <div style={{ boxShadow: `2px 2px 5px ${color}`, ...style }}>
      <div
        style={{
          ...style,
        }}
      >
        <Button
          type={type}
          {...this.props}
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            width: "100%",
            minWidth: "8vw",
            height: 42,
            borderRadius: 0,
            borderColor: color,
            borderWidth: 1,
            background: colors.white,
            paddingLeft: 12,
            paddingRight: 12,
            ...styleHeight,
          }}
          // className="hover-button"
        >
          {iconRight ? null : (
            <div style={{ marginRight: icon === true ? 0 : 12 }}>
              <ISvg
                name={icon}
                width={20}
                height={20}
                fill={hideColor ? "none" : color}
              />
            </div>
          )}

          <ITitle
            title={title}
            style={{
              color: color,
              fontWeight: 500,
            }}
          ></ITitle>

          {iconRight ? (
            <div style={{ marginLeft: icon === true ? 0 : 12 }}>
              <ISvg
                name={icon}
                width={16}
                height={16}
                fill={hideColor ? "none" : color}
              />
            </div>
          ) : null}
        </Button>
      </div>
    );
  }
}
export default IButton;
