import ISvg from "./ISvg";
import ISearch from "./ISearch";
import IButton from "./IButton";
import ITitle from "./ITitle";
import ISelect from "./ISelect";
import ITable from "./ITable";
import IPagination from "./IPagination";
import IInput from "./IInput";
import IList from "./IList";
import IUpload from "./IUpload";
import ISelectMultiple from "./ISelectMultiple";
import ICascader from "./ICascader";
import IImage from "./IImage";
import ISelectCity from "./ISelectCity";
import ISelectCounty from "./ISelectCounty";
import IExportExecl from "./IExportExecl";
import ISelectStatus from "./ISelectStatus";
import IDatePicker from "./IDatePicker";
import ISelectAttrubie from "./ISelectAttrubie";
import IUploadMultiple from "./IUploadMultiple";
import ISelectWareHouse from "./ISelectWareHouse";
import ISelectTypeWareHouse from "./ISelectTypeWareHouse";
import ISelectAllStatus from "./ISelectAllStatus";
import ISelectAllWareHouse from "./ISelectAllWareHouse";
import ISelectAllTypeShipper from "./ISelectAllTypeShipper";
import ISelectShipperAssagin from "./ISelectShipperAssagin";
import ILoading from "./ILoading";
import IVinhHoang from "./IVinhHoang";
import ISelectGender from "./ISelectGender";
import ISelectRouterSalesman from "./ISelectRouterSalesman";
import IBannerIMG from "./IBannerIMG";
import ITableTransfer from "./ITableTransfer";
import ISelectSaleman from "./ISelectSaleman";
import IChart from "./IChart";
import IEditor from "./IEditor";
import ISelectTypeNews from "./ISelectTypeNews";
import ISelectStatusNews from "./ISelectStatusNews";
import ISelectCateNews from "./ISelectCateNews";
import ISelectBranch from "./ISelectBranch";
import ITableHtml from "./ITableHtml";
import IAutoComplete from "./IAutoComplete";
import ITableNotFooter from "./ITableNotFooter";
import IMonthPicker from "./IMonthPicker";
export {
  ITableNotFooter,
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IPagination,
  IInput,
  IList,
  IUpload,
  ISelectMultiple,
  ICascader,
  IImage,
  ISelectCity,
  ISelectCounty,
  IExportExecl,
  ISelectStatus,
  IDatePicker,
  ISelectAttrubie,
  IUploadMultiple,
  ISelectWareHouse,
  ISelectTypeWareHouse,
  ISelectAllStatus,
  ISelectAllWareHouse,
  ISelectAllTypeShipper,
  ISelectShipperAssagin,
  ILoading,
  IVinhHoang,
  ISelectGender,
  ISelectRouterSalesman,
  IBannerIMG,
  ITableTransfer,
  ISelectSaleman,
  IChart,
  IEditor,
  ISelectTypeNews,
  ISelectStatusNews,
  ISelectCateNews,
  ISelectBranch,
  ITableHtml,
  IAutoComplete,
  IMonthPicker
};
