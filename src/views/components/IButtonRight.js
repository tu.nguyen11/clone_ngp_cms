import {Button, Radio, Icon} from 'antd';
import React, {Component} from 'react';
import ISvg from './ISvg';
import {Container, Col, Row} from 'reactstrap';
import './ISvg.css';
import {colors} from '../../assets';
import {Typography} from 'antd';

const {Paragraph, Title} = Typography;
class IButtonRight extends Component {
  render() {
    const {
      icon,
      title,
      color,
      type = 'primary',
      //   shape = "round"
    } = this.props;
    var style = this.props.style;
    if (!style) {
      style = {};
    }

    return (
      <Button
        type={type}
        {...this.props}
        style={{
          ...style,
          flexDirection: 'row',
          display: 'flex',
          minWidth: 160,
          paddingLeft: 30,
          paddingRight: 30,
          height: 42,
          borderRadius: 0,
          borderColor: color,
          borderWidth: 1,
          background: colors.white,
        }}
      >
        <span
          style={{marginLeft: 15, color: color, fontSize: 14, fontWeight: 500}}
        >
          {title}
        </span>
        <div style={{marginLeft: icon == '' ? 0 : 30}}>
          <ISvg name={icon} width={20} height={20} fill={color} />
        </div>

        {/* <h6 style={{ color: color }}>{title}</h6> */}
      </Button>
    );
  }
}
export default IButtonRight;
