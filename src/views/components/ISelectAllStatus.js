import {Select} from 'antd';
import React, {Component} from 'react';
import ISvg from './ISvg';
import {colors} from '../../assets';

import {Container, Col, Row} from 'reactstrap';
import './style.css';
const {Option} = Select;
let data = [
  {key: 2, value: 'Tất cả'},
  {key: 0, value: 'Ẩn'},
  {key: 1, value: 'Hiện'},
];
let dataRouter = [
  {key: 2, value: 'Tất cả'},
  {key: 0, value: 'Tạm ngưng'},
  {key: 1, value: 'Đang hoạt động'},
];
export default class ISelectAllStatus extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {style, typeRouter} = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
      style={{
        background: isBackground ? "white" : "transparent",
        border: isBackground ? "1px solid #cdcece" : "none",
        borderBottom: "1px solid #cdcece",
        width: "100%"
      }}
    >
      <Select
        {...this.props}
        defaultValue
        style={{
        
        }}
        size="large"
      >
        {typeRouter
          ? dataRouter.map((item, index) => (
              <Option value={item.key}>{item.value}</Option>
            ))
          : data.map((item, index) => (
              <Option value={item.key}>{item.value}</Option>
            ))}
      </Select>
    </div>
     
    );
  }
}
