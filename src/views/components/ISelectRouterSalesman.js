import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectRouterSalesman extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listRouter: [],
      fromLimit: 0,
    };
    this.handleScrollSelect = this.handleScrollSelect.bind(this);
  }

  componentDidMount() {
    const { toLimit } = this.props;
    this._getAPIListRouterSalesman(this.state.fromLimit, toLimit);
  }

  handleScrollSelect = async (e) => {
    const { toLimit } = this.props;
    const element = e.target;

    console.log(element.scrollHeight);
    if (element.scrollHeight == element.clientHeight + element.scrollTop) {
      let ToLimit = toLimit + 5;

      if (ToLimit <= toLimit) {
        const data = await APIService._getListRouterSalesman(
          this.state.fromLimit,
          toLimit
        );

        const dataNew = [...this.state.listRouter, ...data.list_route];
        this.setState({
          listRouter: dataNew,
        });
      }
      return;
    }
  };

  _getAPIListRouterSalesman = async (fromLimit, toLimit) => {
    try {
      const data = await APIService._getListRouterSalesman(fromLimit, toLimit);

      let list_route = data.list_route.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });

      this.setState({
        listRouter: list_route,
      });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { defaultValue, style } = this.props;
    return (
      <Select
        {...this.props}
        defaultValue
        optionFilterProp="children"
        style={{
          ...style,
          width: "100%",
          fontSize: "1em",
          height: 42,
          maxWidth: 235,
          minWidth: 200,
          color: "black",
          borderWidth: 0,
        }}
        onPopupScroll={this.handleScrollSelect}
        size="large"
      >
        {this.state.listRouter.map((item, index) => (
          <Option value={item.key}>{item.value}</Option>
        ))}
      </Select>
    );
  }
}
