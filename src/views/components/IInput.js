import { Input, Tooltip, Icon } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { Container, Col, Row } from "reactstrap";
import "./ISvg.css";
import { colors } from "../../assets";

const { Search } = Input;
const { TextArea } = Input;

export default class IInput extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { style, loai = "input", password = false } = this.props;
    return password ? (
      <Input.Password
        {...this.props}
        style={{
          borderWidth: 0,
          borderBottomWidth: 1,
          padding: 0,
          borderRadius: 0,
          width: "100%",
          height: 40,
          background: "white",
          color: "black",
          // fontFamily: "Courier New",
          ...style,
        }}
      />
    ) : loai == "input" ? (
      <Input
        {...this.props}
        style={{
          borderWidth: 0,
          borderBottomWidth: 1,
          borderStyle: "solid",
          padding: 0,
          borderRadius: 0,
          width: "100%",
          height: 40,
          background: "white",
          color: "black",
          // fontFamily: "Courier New",
          ...style,
        }}
      />
    ) : (
      <TextArea
        {...this.props}
        style={{
          borderWidth: 1,
          padding: 10,
          borderRadius: 0,
          fontSize: "1em",
          width: "100%",
          background: "white",
          color: "black",
          // fontFamily: "Courier New",
          ...style,
        }}
      />
    );
  }
}
