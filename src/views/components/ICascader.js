import { Button, Radio, Icon, Cascader } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { Container, Col, Row } from "reactstrap";
import "./ISvg.css";
import { colors } from "../../assets";
import { Typography } from "antd";
import { APIService } from "../../services";

class ICascader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      county: [],
      cate: 0,
    };
  }
  componentDidMount() {
    const { type } = this.props; // city là thành phó , category là danh mục
    if (type === "city") this._getAPIListCities();
    if (type === "category") this._getAPIListBrandCategory();
  }

  _getAPIListBrandCategory = async () => {
    const { addRoot = false, slectAll = false, optionData, value } = this.props;
    const data = await APIService._getListBrandCategory();
    let category = data.categories.map((item, index) => {
      const value = item.id;
      const label = item.name;
      const isLeaf = false;
      return {
        value,
        label,
        isLeaf,
      };
    });
    if (addRoot) {
      category = [...category, { value: 0, label: "Root", isLeaf: false }];
    }
    if (slectAll) {
      category.unshift({
        value: 0,
        label: "Chọn tất cả",
      });
    }

    this.setState({
      data: category,
    });
  };

  _getAPIListCities = async () => {
    const data = await APIService._getListCities("");
    const cityList = data.cities.map((item, index) => {
      const value = item.id;
      const label = item.name;
      const isLeaf = false;

      return {
        value,
        label,
        isLeaf,
      };
    });
    this.setState({
      data: cityList,
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.value) {
      if (nextProps.value[0] != this.props.value[0]) {
        this.setState({ cate: nextProps.value[0] }, async () => {
          try {
            const data = await APIService._getListSubCategpry(this.state.cate);
            let indexArray = this.state.data.map((item, index) => {
              if (item.value == this.state.cate) return index;
            });
            let dataNew = data.categories.map((item, index) => {
              const value = item.id;
              const label = item.name;
              return {
                value,
                label,
              };
            });
            this.state.data[indexArray].children = dataNew;
            this.setState({
              data: this.state.data,
            });
          } catch (err) {
            console.log(err);
          }
        });
      }
    }
  }

  loadData = async (selectedOptions) => {
    const { type } = this.props;
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;

    // load options lazily

    if (type === "city") {
      const data = await APIService._getListCounty(
        selectedOptions[0].value,
        ""
      );
      targetOption.loading = false;
      targetOption.children = data.district.map((item, index) => {
        const value = item.id;
        const label = item.name;
        return {
          value,
          label,
        };
      });
      this.setState({
        data: [...this.state.data],
      });
    }

    if (type === "category") {
      const data = await APIService._getListSubCategpry(
        selectedOptions[0].value
      );
      targetOption.loading = false;
      targetOption.children = data.categories.map((item, index) => {
        const value = item.id;
        const label = item.name;
        return {
          value,
          label,
        };
      });
      this.setState({
        data: [...this.state.data],
      });
    }
    if (type === "") {
      const data = await APIService._getListSubCategpry(0);
      targetOption.loading = false;
      targetOption.children = data.categories.map((item, index) => {
        const value = item.id;
        const label = item.name;
        return {
          value,
          label,
        };
      });
      this.setState({
        data: [...this.state.data],
      });
    }
  };

  render() {
    const { categoryLevel = 2, level = 2, style, reponsive } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%",
        }}
      >
        <Cascader
          {...this.props}
          size="large"
          options={this.state.data}
          loadData={level === 2 ? this.loadData : ""}
          changeOnSelect
          // onChange={(value, selectedOptions) => {
          //
          //   if (value.length < 2) this._getAPIListCounty(value[0]);
          // }}
        />
      </div>
    );
  }
}
export default ICascader;
