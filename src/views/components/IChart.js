import React, { Component } from "react";
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util,
  Line,
} from "bizcharts";
import { priceFormat } from "../../utils";
import FormatterDay from "../../utils/FormatterDay";

export default class IChart extends Component {
  render() {
    const {
      arrayColor = ["#22232B", "#004C91"],
      height,
      tooltip,
      position1,
      data,
      year = false,
      AxisFormat,
    } = this.props;
    const cols = {
      dayOfMonth: {
        range: [0, 1],
        max: 31,
      },
      date: {
        // min: 0,
        max: 31,
        tickCount: 31,
      },
    };
    return (
      <div>
        <Chart height={height} data={data} scale={cols} forceFit>
          <Axis name={year ? "dayOfMonth" : "date"} />
          <Axis name="value" label={AxisFormat} />

          <Tooltip
            containerTpl='<div class="g2-tooltip"><p class="g2-tooltip-title"></p><table class="g2-tooltip-list"></table></div>'
            itemTpl='<tr class="g2-tooltip-list-item"><td>{name}: </td><td style="color:#22232B;font-weight:800">{value}</td></tr>'
            offset={50}
            g2-tooltip={{
              position: "absolute",
              visibility: "hidden",
              border: "1px solid #efefef",
              backgroundColor: "white",
              color: "#000",
              opacity: "0.8",
              padding: "5px 15px",
              transition: "top 200ms,left 200ms",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            g2-tooltip-list={{
              margin: "10px",
            }}
            showTitle={false}
          />
          {/* <Tooltip
            crosshairs={{
              type: "y",
            }}
          /> */}
          <Geom
            type="line"
            position={position1}
            size={2}
            color={[year ? "year" : "dayOfMonth", arrayColor]}
            tooltip={tooltip}
          />
        </Chart>
      </div>
    );
  }
}
