import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectShipperAssagin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listShipper: [],
      keySearchAssagin: "",
      warehouse_id: this.props.warehouse_id
    };
  }

  componentDidMount() {
    this._APIgetListShipperSelect(
      this.state.keySearchAssagin,
      this.state.warehouse_id
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.warehouse_id !== this.props.warehouse_id) {
      //Perform some operation
      this.setState(
        {
          warehouse_id: nextProps.warehouse_id
        },
        () =>
          this._APIgetListShipperSelect(
            this.state.keySearchAssagin,
            this.state.warehouse_id
          )
      );

      // this.classMethod();
    }
  }

  _APIgetListShipperSelect = async (keySearch, warehouse_id) => {
    try {
      const data = await APIService._getListShipperSelect(
        keySearch,
        warehouse_id
      );
      let dataNew = data.list_shippers.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value
        };
      });

      this.setState({
        listShipper: dataNew
      });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { defaultValue, style } = this.props;
    return (
      <Select
        {...this.props}
        defaultValue
        style={{
          ...style,
          width: "100%",
          fontSize: "1em",
          height: 42,

          color: "black",
          borderWidth: 0
        }}
        size="large"
        showSearch
        // onSearch={value => {
        //   if (this.timer) clearTimeout(this.timer);
        //   this.timer = setTimeout(
        //     this.setState(
        //       {
        //         keySearchAssagin: value
        //       },
        //       () => {
        //         this._APIgetListShipperSelect(this.state.keySearchAssagin);
        //       }
        //     ),
        //     500
        //   );
        // }}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
      >
        {this.state.listShipper.map((item, index) => (
          <Option value={item.key}>{item.value}</Option>
        ))}
      </Select>
    );
  }
}
