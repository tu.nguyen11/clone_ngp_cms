import React, { Component } from "react";
import { Upload, Icon, message, Modal } from "antd";
import { APIService } from "../../services";
import { ImageType } from "../../constant";
import ISvg from "./ISvg";
import "./upload.css";
import { IError } from "./common";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

class IUploadMultiple extends React.Component {
  constructor(props) {
    super(props);
    const { images, callback, remove, type = ImageType.PRODUCT, url } = props;
    var fileList = [];
    if (images && images.length > 0) {
      fileList = images.map((image, index) => {
        return {
          uid: index,
          name: image,
          url: url + image,
        };
      });
    }
    this.state = {
      previewVisible: false,
      previewImage: "",
      fileList: fileList,
    };
    this.beforeUpload = this.beforeUpload.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.images !== this.props.images ||
      nextProps.url !== this.props.url
    ) {
      //Perform some operation
      let fileList = [];
      fileList = nextProps.images.map((image, index) => {
        return {
          uid: index,
          name: image,
          url: nextProps.url + image,
        };
      });
      this.setState({ fileList: fileList });
      // this.classMethod();
    }
  }

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
    });
  };

  onRemove = (file) => {
    try {
      var index = this.state.fileList.indexOf(file);
      const { remove } = this.props;
      if (remove) {
        remove(index);
      }
      this.state.fileList.splice(index, 1);
      this.setState({});
    } catch (error) {}
  };

  beforeUpload(file) {
    // fileList không dùng bởi vì beforeUpload zô từng tấm hình
    // const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    // if (!isJpgOrPng) {
    //   message.error("You can only upload JPG/PNG file!");
    // }
    // const isLt2M = file.size / 1024 < 512;
    // if (!isLt2M) {
    //   message.error("Image must smaller than 512Kb!");
    // }
    // if (!isJpgOrPng || !isLt2M) {
    //   return null;
    // }
    if (file.size > 10000000) {
      return IError("Hình ảnh không upload trên 10mb");
    }

    const { callback } = this.props;
    let File = file;
    return new Promise(async (resolve, reject) => {
      try {
        const response = await APIService._uploadImage(ImageType.PRODUCT, File);
        var image = response.image_url + response.images[0];
        var file = {
          uid: this.state.fileList.length,
          name: response.name,
          url: image,
        };
        this.state.fileList.push(file);
        if (callback) {
          callback(response.images, response.image_url);
        }
        this.setState({});
        resolve();
      } catch (error) {
        this.setState({ loading: false });
        reject();
      }
    });
  }

  render() {
    const { previewVisible, previewImage, fileList } = this.state;
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const uploadButtonAgency = (
      <div
        style={
          {
            // display: 'flex',
            // justifyContent: 'center',
            // alignItems: 'center',
            // flexDirection: 'column',
            // width: this.props.width,
          }
        }
      >
        <ISvg
          name={ISvg.NAME.ADDUPLOAD}
          width={30}
          height={30}
          // fill={colors.icon.default}
        />
      </div>
    );
    return (
      <div className="clearfix" style={{}}>
        <Upload
          listType="picture-card"
          fileList={fileList}
          onPreview={this.handlePreview}
          onChange={this.handleChange}
          onRemove={this.onRemove}
          beforeUpload={this.beforeUpload}
          multiple
          accept=".jpg,.jpeg,.png"
        >
          {fileList.length >= 4 ? null : uploadButtonAgency}
        </Upload>
        <Modal
          visible={previewVisible}
          footer={null}
          onCancel={this.handleCancel}
        >
          <img alt="example" style={{ width: "100%" }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}

export default IUploadMultiple;
