import { Select } from "antd";
import React, { Component } from "react";
import ISvg from "./ISvg";
import { colors } from "../../assets";

import { Container, Col, Row } from "reactstrap";
import "./style.css";
import { APIService } from "../../services";
const { Option } = Select;

export default class ISelectCondiJoin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      condiJoin: [],
    };
  }

  componentDidMount() {
    this._getAPIListGift();
  }

  _getAPIListGift = async () => {
    try {
      const data = await APIService._getSelectGift();

      let dataNew = data.event_config.event_condition_joins.map(
        (item, index) => {
          const key = item.id;
          const value = item.name;
          return {
            key,
            value,
          };
        }
      );

      this.setState({
        condiJoin: dataNew,
      });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { defaultValue, style } = this.props;
    const { isBackground = true } = this.props;
    return (
      <div
        className={isBackground ? "" : "clear-pad"}
        style={{
          background: isBackground ? "white" : "transparent",
          border: isBackground ? "1px solid #cdcece" : "none",
          borderBottom: "1px solid #cdcece",
          width: "100%",
        }}
      >
        <Select
          {...this.props}
          defaultValue
          style={{
            width: "100%",
            fontSize: "1em",
            height: 42,
            // maxWidth: 235,
            // minWidth: 200,
            color: "black",
            borderWidth: 0,
            ...style,
          }}
          size="large"
        >
          {this.state.condiJoin.map((item, index) => (
            <Option value={item.key}>{item.value}</Option>
          ))}
        </Select>
      </div>
    );
  }
}
