import React, { Component } from 'react'
import { colors, images } from '../../assets'

class IImage extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    return <img {...this.props} alt='hình' />
  }
}

export default IImage
