import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import { colors, images } from "../../assets";
import { IInput, IButton, ITitle } from "../components";
// import ReCAPTCHA from "react-grecaptcha";
import { Checkbox, Form, message, Input } from "antd";
import { APIService, UserService } from "../../services";
// import * as Config from "../../config.json";
import "./input.css";
// const CAPCHA_KEY = "6LfIa7gZAAAAAG8xQWBhbhs23BMIUQQ3qDJfFo9D";
// const CAPCHA_KEY = "6LdIBsUUAAAAAPs-PuGUf00EUEorjFEN9v_oul6h";
import {ver} from '../../assets/version'
class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      errCapche: true,
      width: window.innerWidth,
      height: window.innerHeight,
      keyCapcha: "",
    };

    this.handleResize = this.handleResize.bind(this);

    this._submitLogin = this._submitLogin.bind(this);
    this.successCaptcha = this.successCaptcha.bind(this);
  }

  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  }

  successCaptcha() {
    this.setState({
      errCapche: false,
    });
  }

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  handlePassword(e) {
    this.setState({
      password: e.target.value,
    });
  }

  handleUserName(e) {
    this.setState({
      username: e.target.value,
    });
  }

  _submitLogin = async () => {
    // event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (err) {
        message.info("Thiếu thông tin cần thiết");
        return;
      }
      let obj = {
        username: values.username,
        password: values.password,
      };
      APIService._login(obj).then((data) => {
        localStorage.setItem("token", data.token);
        localStorage.setItem("user_id_login", data.id);
        localStorage.setItem("user_name_login", data.username);
        UserService._setToken(data.token);
        this.props.history.replace("/order/request/list");
      });
    });
  };

  componentDidMount() {
    let time = new Date();
    let domainCall = `http://nrms.ipicorp.co/ngp/cms/cms.json?${time.getTime()}`;
    let domain = window.location.hostname;
    if (
      domain.search("localhost") >= 0 ||
      domain.search("hub.ipicorp.co") >= 0
    ) {
      fetch(domainCall)
        .then((response) => response.json())
        .then((data) => {
          localStorage.setItem(
            "local_domain_server_NGP_ADMIN",
            data.admin.dev.restful
          );
        });
    }
    // else if (domain.search("cmctestadmin.ipicorp.co") >= 0) {
    //   fetch(domainCall)
    //     .then((response) => response.json())
    //     .then((data) => {
    //       localStorage.setItem(
    //         "local_domain_server_NGP_ADMIN",
    //         data.admin.test.restful
    //       );
    //     });
    // } else if (domain.search("admin.anhtin.vn") >= 0) {
    //   fetch(domainCall)
    //     .then((response) => response.json())
    //     .then((data) => {
    //       localStorage.setItem(
    //         "local_domain_server_NGP_ADMIN",
    //         data.admin.product.restful
    //       );
    //     });
    // }

    window.addEventListener("resize", this.handleResize);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const version = ver()
    return (
      <Container
        style={{
          background: colors.background,
          width: window.innerWidth,
          height: window.innerHeight,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        fluid
      >
        <div>
          <div
            style={{
              marginBottom: 20,
              display: "flex",
              justifyContent: "center",
            }}
          >
            <img src={images.logo} style={{ width: 240 }} />
          </div>

          <Form onSubmit={this.handleSubmit}>
            <Col
              style={{
                // width: 490,
                // height: 490,

                background: colors.white,
                paddingTop: 76,
                paddingBottom: 76,
                paddingLeft: 95,
                paddingRight: 95,
              }}
            >
              <Form.Item>
                {getFieldDecorator("username", {
                  rules: [{ required: true, message: "nhập tài khoản" }],
                })(
                  <Row className="p-0 mt-4 mb-0 ml-0 mr-0">
                    <IInput
                      placeholder="Tài khoản"
                      style={{ width: 300, padding: 10 }}
                    />
                  </Row>
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("password", {
                  rules: [{ required: true, message: "nhập mật khẩu" }],
                })(
                  <Row className="p-0 mt-0 mb-0 ml-0 mr-0">
                    <div className="example-input">
                      <Input.Password
                        placeholder="Mật khẩu"
                        style={{
                          padding: 0,
                          borderRadius: 0,
                          fontSize: "1em",
                          width: "100%",
                          height: 40,
                          background: "white",
                          color: "black",
                        }}
                      />
                    </div>
                  </Row>
                )}
              </Form.Item>
              {/* <Form.Item>
                {getFieldDecorator("capche", {
                  rules: [
                    {
                      required: this.state.errCapche,
                      message: 'chưa chọn captcha'
                    }
                  ]
                })(
                  <Row className='p-0 mt-0 mb-0 ml-0 mr-0'>
                    <ReCAPTCHA
                      sitekey={this.state.keyCapcha}
                      callback={this.successCaptcha}
                      // expiredCallback={this.expiredCallback}
                      locale='vi'
                    />
                  </Row>
                )}
              </Form.Item> */}
              <Row className="p-0 mt-0 mb-4 ml-0 mr-0">
                <IButton
                  style={{
                    width: 300,
                    color: colors.main,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    opacity: 1,
                  }}
                  onClick={() => {
                    // if (this.state.errCapche) {
                    //   return;
                    // }
                    this._submitLogin();
                  }}
                  color={colors.main}
                  htmlType="submit"
                  icon=""
                  title="Đăng nhập"
                />
              </Row>
              <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                <Checkbox style={{ marginRight: 17 }} />
                <ITitle level={4} title="Ghi nhớ thông tin đăng nhập" />
              </Row>
              <p>Version : {version} </p>
            </Col>

          </Form>
        </div>
      </Container>
    );
  }
}

const loginPage = Form.create({ name: "LoginPage" })(LoginPage);

export default loginPage;
