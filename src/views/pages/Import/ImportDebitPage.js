import { Col, message, Row, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { colors, images } from "../../../assets";
import { APIService } from "../../../services";
import {
  IButton,
  ISearch,
  ISelect,
  ISvg,
  ITable,
  ITitle,
} from "../../components";
import { StyledITitle } from "../../components/common/Font/font";
import { useHistory } from "react-router";
import { priceFormat } from "../../../utils";
import FormatterDay from "../../../utils/FormatterDay";

export default function ImportDebitPage() {
  const history = useHistory();
  const [loadingTable, setLoadingTable] = useState(false);
  const [dataTable, setDataTable] = useState({});
  const [filterTable, setFilterTable] = useState({
    page: 1,
    keySearch: "",
  });
  const [loadingImport, setLoadingImport] = useState(false);

  const _fetchAPIListPaymentTransaction = async (filter) => {
    try {
      const data = await APIService._getListImportDebit(
        filter.keySearch,
        filter.page,
        filter.file
      );
      data.response.map(
        (item, index) => (item.stt = (filterTable.page - 1) * 10 + index + 1)
      );
      setDataTable(data);
      setLoadingTable(false);
      console.log(data);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIListPaymentTransaction(filterTable);
  }, [filterTable]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Thời gian cập nhật",
      dataIndex: "time",
      key: "time",
      render: (time) => (
        <span>
          {FormatterDay.dateFormatWithString(
            time,
            "#DD#/#MM#/#YYYY# - #hhh#:#mm#"
          )}
        </span>
      ),
    },
    {
      title: "Tên đại lý",
      dataIndex: "shop_name",
      key: "shop_name",
      render: (shop_name) => <span>{shop_name}</span>,
    },
    {
      title: "Mã đại lý",
      dataIndex: "dms_code",
      key: "dms_code",
      render: (dms_code) => <span>{dms_code}</span>,
    },
    {
      title: "Hạn mức tín dụng ",
      dataIndex: "credit_limit",
      key: "credit_limit",
      render: (credit_limit) => <span>{priceFormat(credit_limit)}</span>,
    },
    {
      title: "Hạn mức gối đầu",
      dataIndex: "knee_limit",
      key: "knee_limit",
      render: (knee_limit) => <span>{priceFormat(knee_limit)}</span>,
    },
    {
      title: "Hạn nợ",
      dataIndex: "debt_term",
      key: "debt_term",
      render: (debt_term) => (
        <span>
          {debt_term === null || debt_term === undefined
            ? ""
            : `${debt_term} Ngày`}
        </span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Import công nợ ĐLC1
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm mã đại lý, tên đại lý">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm mã đại lý, tên đại lý"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    keySearch: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <div className="flex justify-end">
              <IButton
                title={"Xuất template"}
                color={colors.main}
                styleHeight={{
                  width: 130,
                }}
                onClick={() => {
                  window.open(
                    "http://cmcfile.ipicorp.co//file/templateimport/AnhTin_Template_cap_nhat_cong_no.xls"
                  );
                }}
              />
              <IButton
                title={"Import"}
                color={colors.main}
                styleHeight={{
                  width: 130,
                  marginLeft: 25,
                }}
                loading={loadingImport}
                onClick={() => {
                  var input = document.getElementById("my-file");
                  input.click();
                  input.onchange = async function (e) {
                    try {
                      var file = input.files[0];
                      if (!file) return;
                      setLoadingImport(true);
                      const data = await APIService._importFileDebit(file);
                      message.success("Import file thành công");
                      setLoadingImport(false);

                      await _fetchAPIListPaymentTransaction(filterTable);
                      e.target.value = null;
                    } catch (error) {
                      setLoadingImport(false);
                    }
                  };
                }}
              />
              <input
                type="file"
                id="my-file"
                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                style={{ display: "none" }}
              />
            </div>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={dataTable.response}
                style={{ width: "100%" }}
                defaultCurrent={1}
                sizeItem={dataTable.size}
                rowKey="id"
                indexPage={filterTable.page}
                maxpage={dataTable.total_record}
                loading={loadingTable}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilterTable({ ...filterTable, page: page });
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}
