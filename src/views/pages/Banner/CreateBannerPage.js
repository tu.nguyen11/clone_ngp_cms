import React, { useState, useEffect, useRef } from "react";
import "../../../App.css";
import moment from "moment";
import { Row, Col, Skeleton, message, Form, AutoComplete } from "antd";
import { ITitle, IButton, ISvg, ISelect } from "../../components";
import {
  IFormItem,
  IInputText,
  IDatePickerFrom,
} from "../../components/common";
import { colors, images } from "../../../assets";
import { ImageType } from "../../../constant";

import { APIService } from "../../../services";
import { useHistory } from "react-router-dom";

import IUploadHook from "./../../components/common/Upload/IUploadHook";

// const currentDate = new Date();

function CreateBannerPage(props) {
  const { Option } = AutoComplete;
  const format = "HH:mm:ss DD-MM-YYYY";
  const history = useHistory();
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;
  const typeTimeRef = useRef(null);

  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingButton, setIsLoadingButton] = useState(false);
  const [isOpenApply, setIsOpenApply] = useState(false);

  const [options, setOptions] = useState([]);
  const [optionsEvent, setOptionsEvent] = useState([]);
  const [optionsSubCate, setOptionsSubCate] = useState([]);
  const [dataRegion, setDataRegion] = useState([]);
  const [dataCities, setDataCities] = useState([]);
  // const [listEvent, setListEvent] = useState([]);

  const [regionId, setRegionId] = useState(-1);
  const [citiId, setCitiId] = useState(-1);
  // const [lvUser, setLvUser] = useState(-1);

  const [url, setUrl] = useState("");

  const [filterProduct, setFilterProduct] = useState({
    key: "",
  });

  const [search, setSearch] = useState("");
  const [searchSubCate, setSearchSubCate] = useState("");
  // const [endDateEvent, setEndDateEvent] = useState("");

  const typeBanner = [
    {
      key: 1,
      value: "Banner chính sách",
    },
    {
      key: 2,
      value: "Banner sản phẩm",
    },
    {
      key: 3,
      value: "Banner quảng bá",
    },
    {
      key: 4,
      value: "Banner ngành hàng con",
    },
  ];

  const userLevel = [
    // {
    //   key: 0,
    //   value: "Tất cả",
    // },
    {
      key: 1,
      value: "Đại lý Cấp 1",
    },
    // {
    //   key: 2,
    //   value: "Đại lý Cấp 2",
    // },
    // {
    //   key: 3,
    //   value: "End User",
    // },
  ];

  const _fetchAgencyGetRegion = async () => {
    try {
      const data = await APIService._getAllAgencyGetRegion();
      let dataNew = data.agency.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      dataNew.unshift({
        key: 0,
        value: "Tất cả",
      });
      setDataRegion(dataNew);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const _fetchListCityByRegion = async (arrayRegion) => {
    try {
      const data = await APIService._getListCityByRegion(arrayRegion);
      let dataNew = data.list_cities.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });

      dataNew.unshift({
        key: 0,
        value: "Tất cả",
      });

      setDataCities(dataNew);
      // setLoadingCity(false);
    } catch (error) {
      console.log(error);
      // setLoadingCity(false);
    }
  };

  const _fetchListProduct = async (filterProduct) => {
    try {
      const data = await APIService._getListProductBanner(filterProduct.key, 0);
      const dataNew = data.product.map((item) => {
        const id = item.id;
        const value = item.name.toLowerCase();
        const text = item.name;
        return { id, value, text };
      });
      setOptions(dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchListEvent = async (search) => {
    try {
      const data = await APIService._getListEventBanner(search);
      const dataNew = data.listEvent.map((item) => {
        const id = item.id;
        const value = item.name.toLowerCase();
        const text = item.name;
        return { id, value, text };
      });
      // setListEvent(data.listEvent);
      setOptionsEvent(dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchListSubCate = async (search) => {
    try {
      const data = await APIService._getListSubCategpry(0, search);
      let sub_category = data.categories.map((item, index) => {
        const id = item.id;
        const value = item.name.toLowerCase();
        const text = item.name;
        return { id, value, text };
      });

      setOptionsSubCate([...sub_category]);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    _fetchAgencyGetRegion();
  }, []);

  useEffect(() => {
    _fetchListProduct(filterProduct);
  }, [filterProduct]);

  useEffect(() => {
    _fetchListEvent(search);
  }, [search]);

  useEffect(() => {
    _fetchListSubCate(searchSubCate);
  }, [searchSubCate]);

  const handleStartOpenChange = (open) => {
    if (!open) {
      setIsOpenApply(true);
    }
  };

  const handleEndOpenChange = (open) => {
    setIsOpenApply(open);
  };

  const disabledStartDateApply = (startValue) => {
    let dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);
    if (startValue < dateNow.getTime()) {
      return true;
    }
    if (!startValue || !reduxForm.getFieldValue("end_date")) {
      return false;
    }

    return startValue.valueOf() > reduxForm.getFieldValue("end_date");
  };

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !reduxForm.getFieldValue("start_date")) {
      return false;
    }
    return endValue.valueOf() <= reduxForm.getFieldValue("start_date");
  };

  // const disabledEndDateApply = (endValue) => {
  //   const startDate = new Date(Number(reduxForm.getFieldValue("start_date")));
  //   startDate.setDate(startDate.getDate() + 1);
  //   if (endValue.valueOf() < currentDate.getTime()) {
  //     return true;
  //   }
  //   if (endDateEvent !== "") {
  //     const date = new Date(endDateEvent);
  //     if (endValue.valueOf() > date) {
  //       return true;
  //     }
  //   }
  //   if (!endValue || !reduxForm.getFieldValue("start_date")) {
  //     return false;
  //   }
  //   return endValue.valueOf() <= startDate.getTime();
  // };

  const postCreateBanner = async (obj) => {
    try {
      const data = await APIService._postCreateBanner(obj);
      setIsLoadingButton(false);
      message.success("Tạo thành công");
      history.push("/banner/list");
    } catch (err) {
      console.log(err);
      setIsLoadingButton(false);
    }
  };

  const handleSubmit = (e) => {
    setIsLoadingButton(true);
    e.preventDefault();
    reduxForm.validateFields((err, values) => {
      if (err) {
        setIsLoadingButton(false);
        return;
      }
      const conditionShow = [
        {
          cities: !values.cities ? [0] : [Number(values.cities)],
          region: !values.region ? [0] : [Number(values.region)],
        },
      ];
      const obj = {
        banner_type: !values.banner_type ? 1 : Number(values.banner_type),
        condition_show: conditionShow,
        end_date: values.end_date,
        start_date: values.start_date,
        id_event: !values.id_event ? 0 : Number(values.id_event),
        id_product: !values.id_product ? 0 : Number(values.id_product),
        id_product_type: !values.id_subcate ? 0 : Number(values.id_subcate),
        image: values.image,
        title: values.title,
        // user_type: !values.user_type ? 0 : Number(values.user_type),
        user_type: 1,
      };
      postCreateBanner(obj);
      console.log(Object.keys(obj));
    });
  };

  function removeVietnameseTones(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    // Remove extra spaces
    // Bỏ các khoảng trắng liền nhau
    str = str.replace(/ + /g, " ");
    str = str.trim();
    // Remove punctuations
    // Bỏ dấu câu, kí tự đặc biệt
    str = str.replace(
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
      " "
    );
    return str;
  }

  const renderOptionAutoComplete = (option, search) => {
    const _renderOption = option.map((item) => {
      let keySearch = removeVietnameseTones(search);
      let itemText = removeVietnameseTones(item.text);
      let index = itemText.toLowerCase().indexOf(keySearch.toLowerCase());

      let text1 = item.text.slice(0, index);
      let text2 = item.text.slice(index, index + keySearch.length);
      let text3 = item.text.slice(index + keySearch.length);

      return (
        <Option key={item.id} text={item.text}>
          <span>{text1}</span>
          <span style={{ fontWeight: 600, color: colors.main }}>{text2}</span>
          <span>{text3}</span>
        </Option>
      );
    });

    return _renderOption;
  };

  return (
    <div style={{ width: "100%" }}>
      <Form onSubmit={handleSubmit}>
        <Row gutter={[12, 16]}>
          <Col span={24}>
            <ITitle
              level={1}
              title="Tạo banner"
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </Col>
          <Col span={24}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              <IButton
                icon={ISvg.NAME.SAVE}
                loading={isLoadingButton}
                title="Lưu"
                htmlType="submit"
                color={colors.main}
              />
              <IButton
                icon={ISvg.NAME.CROSS}
                style={{ marginLeft: 15 }}
                title="Hủy"
                color={colors.red}
                onClick={() => {
                  history.push("/banner/list");
                }}
              />
            </div>
          </Col>
          <Col span={24} xl={16} xxl={14}>
            <div
              style={{
                height: "100%",
              }}
            >
              <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
                <Col span={16}>
                  <div
                    style={{
                      padding: "30px 40px",
                      background: colors.white,
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      height: 660,
                    }}
                  >
                    {isLoading ? (
                      <div>
                        <Skeleton
                          loading={true}
                          active
                          paragraph={{ rows: 16 }}
                        />
                      </div>
                    ) : (
                      <>
                        <Row gutter={[12, 20]}>
                          <Col>
                            <ITitle
                              level={2}
                              title="Thông tin banner"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col>
                            <IFormItem>
                              {getFieldDecorator("title", {
                                rules: [
                                  {
                                    required: true,
                                    message: "Vui lòng nhập tiêu đề",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <ITitle
                                      level={4}
                                      title={"Tiêu đề"}
                                      style={{
                                        fontWeight: 600,
                                      }}
                                    />
                                    <IInputText
                                      style={{ color: "#000" }}
                                      placeholder="Nhập tiêu đề"
                                      // onChange={(key) => {
                                      //   reduxForm.setFieldsValue({
                                      //     title: key,
                                      //   });
                                      // }}
                                      value={reduxForm.getFieldValue("title")}
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          <Col>
                            <Row gutter={[12, 12]}>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("banner_type", {
                                    rules: [
                                      {
                                        required: false,
                                        message: "Vui lòng nhập loại banner",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Loại banner"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <ISelect
                                          defaultValue={1}
                                          data={typeBanner}
                                          select={true}
                                          isBackground={false}
                                          style={{ marginTop: 1 }}
                                          placeholder="Chọn loại banner"
                                          onChange={(key) => {
                                            reduxForm.setFieldsValue({
                                              banner_type: key,
                                              id_event: null,
                                              id_product: null,
                                              start_date: undefined,
                                              end_date: undefined,
                                            });
                                            setSearch("");
                                          }}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              {reduxForm.getFieldValue("banner_type") ===
                              3 ? null : reduxForm.getFieldValue(
                                  "banner_type"
                                ) === 4 ? (
                                <Col span={12}>
                                  <IFormItem>
                                    {getFieldDecorator("id_subcate", {
                                      rules: [
                                        {
                                          required: true,
                                          message:
                                            "Vui lòng nhập tên ngành hàng con",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <ITitle
                                            level={4}
                                            title={"Tên ngành hàng con"}
                                            style={{
                                              fontWeight: 600,
                                            }}
                                          />
                                          <AutoComplete
                                            dataSource={renderOptionAutoComplete(
                                              optionsSubCate,
                                              searchSubCate
                                            )}
                                            style={{
                                              marginTop: 6,
                                            }}
                                            optionLabelProp="text"
                                            placeholder="Nhập tên ngành hàng con"
                                            onSelect={(id) => {
                                              reduxForm.setFieldsValue({
                                                id_subcate: id,
                                              });

                                              setSearchSubCate("");
                                            }}
                                            onSearch={(data) => {
                                              if (typeTimeRef.current) {
                                                clearTimeout(
                                                  typeTimeRef.current
                                                );
                                              }
                                              typeTimeRef.current = setTimeout(
                                                () => {
                                                  setSearchSubCate(data);
                                                },
                                                300
                                              );
                                            }}
                                            value={reduxForm.getFieldValue(
                                              "id_subcate"
                                            )}
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                              ) : reduxForm.getFieldValue("banner_type") ===
                                2 ? (
                                <Col span={12}>
                                  <IFormItem>
                                    {getFieldDecorator("id_product", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "Vui lòng nhập tên sản phẩm",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <ITitle
                                            level={4}
                                            title={"Tên sản phẩm"}
                                            style={{
                                              fontWeight: 600,
                                            }}
                                          />
                                          <AutoComplete
                                            dataSource={renderOptionAutoComplete(
                                              options,
                                              filterProduct.key
                                            )}
                                            style={{
                                              marginTop: 6,
                                            }}
                                            optionLabelProp="text"
                                            placeholder="Nhập tên sản phẩm"
                                            onSelect={(id) => {
                                              reduxForm.setFieldsValue({
                                                id_product: id,
                                              });

                                              setFilterProduct({
                                                ...filterProduct,
                                                key: "",
                                              });
                                            }}
                                            onSearch={(data) => {
                                              if (typeTimeRef.current) {
                                                clearTimeout(
                                                  typeTimeRef.current
                                                );
                                              }
                                              typeTimeRef.current = setTimeout(
                                                () => {
                                                  setFilterProduct({
                                                    ...filterProduct,
                                                    key: data,
                                                  });
                                                },
                                                300
                                              );
                                            }}
                                            value={reduxForm.getFieldValue(
                                              "id_product"
                                            )}
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                              ) : (
                                <Col span={12}>
                                  <IFormItem>
                                    {getFieldDecorator("id_event", {
                                      rules: [
                                        {
                                          required: true,
                                          message:
                                            "Vui lòng nhập tên chính sách",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <ITitle
                                            level={4}
                                            title={"Tên chính sách"}
                                            style={{
                                              fontWeight: 600,
                                            }}
                                          />
                                          <AutoComplete
                                            dataSource={renderOptionAutoComplete(
                                              optionsEvent,
                                              search
                                            )}
                                            style={{
                                              marginTop: 6,
                                            }}
                                            optionLabelProp="text"
                                            placeholder="Nhập tên chính sách"
                                            onSelect={(idEvent) => {
                                              reduxForm.setFieldsValue({
                                                id_event: idEvent,
                                              });
                                              // reduxForm.setFieldsValue({
                                              //   start_date: undefined,
                                              //   end_date: undefined,
                                              // });
                                              // const event = listEvent.find(
                                              //   (item) => {
                                              //     return (
                                              //       item.id === Number(idEvent)
                                              //     );
                                              //   }
                                              // );
                                              // setEndDateEvent(event.end_date);
                                              setSearch("");
                                            }}
                                            onSearch={(data) => {
                                              if (typeTimeRef.current) {
                                                clearTimeout(
                                                  typeTimeRef.current
                                                );
                                              }
                                              typeTimeRef.current = setTimeout(
                                                () => {
                                                  setSearch(data);
                                                },
                                                300
                                              );
                                            }}
                                            value={reduxForm.getFieldValue(
                                              "id_event"
                                            )}
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                              )}
                            </Row>
                          </Col>
                          <Col>
                            <Row gutter={[12, 12]}>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("start_date", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "Vui lòng nhập ngày bắt đầu",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Ngày bắt đầu"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <IDatePickerFrom
                                          showTime
                                          style={{ marginTop: 5 }}
                                          isBorderBottom={true}
                                          disabledDate={disabledStartDateApply}
                                          paddingInput="0px"
                                          placeholder="Nhập thời gian bắt đầu"
                                          onOpenChange={handleStartOpenChange}
                                          format={format}
                                          onChange={(date) => {
                                            reduxForm.setFieldsValue({
                                              start_date: date._d.getTime(),
                                            });
                                          }}
                                          value={
                                            reduxForm.getFieldValue(
                                              "start_date"
                                            ) === undefined
                                              ? undefined
                                              : moment(
                                                  new Date(
                                                    reduxForm.getFieldValue(
                                                      "start_date"
                                                    )
                                                  ),
                                                  format
                                                )
                                          }
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("end_date", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "Vui lòng nhập ngày kết thúc",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Ngày kết thúc"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <IDatePickerFrom
                                          showTime
                                          style={{ marginTop: 5 }}
                                          disabledDate={disabledEndDateApply}
                                          isBorderBottom={true}
                                          paddingInput="0px"
                                          open={isOpenApply}
                                          onOpenChange={handleEndOpenChange}
                                          format={format}
                                          placeholder="Nhập thời gian kết thúc"
                                          onChange={(date) => {
                                            reduxForm.setFieldsValue({
                                              end_date: date._d.getTime(),
                                            });
                                          }}
                                          value={
                                            reduxForm.getFieldValue(
                                              "end_date"
                                            ) === undefined
                                              ? undefined
                                              : moment(
                                                  new Date(
                                                    reduxForm.getFieldValue(
                                                      "end_date"
                                                    )
                                                  ),
                                                  format
                                                )
                                          }
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                        {reduxForm.getFieldValue("banner_type") === 1 ||
                        reduxForm.getFieldValue("banner_type") ===
                          undefined ? null : (
                          <div style={{ marginTop: 42 }}>
                            <Row gutter={[12, 20]}>
                              <Col style={{ marginTop: 6 }}>
                                <ITitle
                                  level={2}
                                  title="Điều kiện hiển thị"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col>
                                <Row gutter={[12, 12]}>
                                  <Col span={12}>
                                    <IFormItem>
                                      {getFieldDecorator("region", {
                                        rules: [
                                          {
                                            required: true,
                                            message: "Vui lòng chọn vùng",
                                          },
                                        ],
                                      })(
                                        <div>
                                          <Row>
                                            <ITitle
                                              level={4}
                                              title={"Khu vực"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                            <ISelect
                                              data={dataRegion}
                                              select={true}
                                              isBackground={false}
                                              style={{ marginTop: 1 }}
                                              placeholder="Chọn khu vực"
                                              onChange={(key) => {
                                                setRegionId(key);
                                                setCitiId(-1);
                                                // setLvUser(-1);
                                                reduxForm.setFieldsValue({
                                                  region: key,
                                                });
                                              }}
                                              onDropdownVisibleChange={(
                                                open
                                              ) => {
                                                if (!open) {
                                                  if (dataRegion.length === 0) {
                                                    return;
                                                  }
                                                  if (regionId === 0) {
                                                    return;
                                                  }
                                                  _fetchListCityByRegion([
                                                    regionId,
                                                  ]);
                                                  return;
                                                }
                                              }}
                                            />
                                          </Row>
                                        </div>
                                      )}
                                    </IFormItem>
                                  </Col>
                                  <Col span={12}>
                                    <IFormItem>
                                      {getFieldDecorator("cities", {
                                        rules: [
                                          {
                                            required: true,
                                            message:
                                              "Vui lòng chọn tỉnh/thành phố",
                                          },
                                        ],
                                      })(
                                        <div>
                                          <Row>
                                            <ITitle
                                              level={4}
                                              title={"Tỉnh/Thành phố"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                            <ISelect
                                              data={dataCities}
                                              select={
                                                regionId === -1 ? false : true
                                              }
                                              isBackground={false}
                                              style={{ marginTop: 1 }}
                                              placeholder="Chọn tỉnh/thành phố"
                                              onChange={(key) => {
                                                setCitiId(key);
                                                reduxForm.setFieldsValue({
                                                  cities: key,
                                                });
                                              }}
                                              onDropdownVisibleChange={(
                                                open
                                              ) => {
                                                if (open) {
                                                  if (setRegionId === 0) return;
                                                  _fetchListCityByRegion([
                                                    regionId,
                                                  ]);
                                                  return;
                                                }
                                              }}
                                              value={
                                                citiId === -1
                                                  ? undefined
                                                  : citiId
                                              }
                                            />
                                          </Row>
                                        </div>
                                      )}
                                    </IFormItem>
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("user_type", {
                                    rules: [
                                      {
                                        required: false,
                                        message: "Vui lòng chọn cấp người dùng",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Cấp người dùng"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <ISelect
                                          data={userLevel}
                                          select={false}
                                          // select={citiId !== -1 ? true : false}
                                          isBackground={false}
                                          style={{ marginTop: 1 }}
                                          placeholder="Chọn cấp người dùng"
                                          onChange={(key) => {
                                            // setLvUser(key);
                                            reduxForm.setFieldsValue({
                                              user_type: key,
                                            });
                                          }}
                                          value={
                                            // lvUser === -1 ? undefined : lvUser
                                            1
                                          }
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                            </Row>
                          </div>
                        )}
                      </>
                    )}
                  </div>
                </Col>
                <Col span={8}>
                  <div
                    style={{
                      padding: "30px 40px",
                      background: colors.white,
                      height: "100%",
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      width: 330,
                      height: 660,
                    }}
                  >
                    {isLoading ? (
                      <div>
                        <Skeleton
                          loading={true}
                          active
                          paragraph={{ rows: 16 }}
                        />
                      </div>
                    ) : (
                      <div>
                        <ITitle
                          level={2}
                          title="Hình ảnh"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                        <IFormItem>
                          {getFieldDecorator("image", {
                            rules: [
                              {
                                required: true,
                                message: "Vui lòng chọn ảnh",
                              },
                            ],
                          })(
                            <div style={{ marginTop: 20 }}>
                              <IUploadHook
                                type={ImageType.BANNER}
                                callback={(nameFile, url) => {
                                  reduxForm.setFieldsValue({
                                    image: nameFile,
                                  });
                                  setUrl(url);
                                }}
                                dataProps={
                                  reduxForm.getFieldValue("image") === "" ||
                                  !reduxForm.getFieldValue("image")
                                    ? []
                                    : [
                                        {
                                          uid: -1,
                                          name: reduxForm.getFieldValue(
                                            "image"
                                          ),
                                          status: "done",
                                          url:
                                            url +
                                            reduxForm.getFieldValue("image"),
                                        },
                                      ]
                                }
                                width="100%"
                                height="100px"
                              />
                            </div>
                          )}
                        </IFormItem>
                      </div>
                    )}
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const createBannerPage = Form.create({ name: "CreateBannerPage" })(
  CreateBannerPage
);

export default createBannerPage;
