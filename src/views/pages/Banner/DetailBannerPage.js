import React, { useState, useEffect } from "react";
import { Row, Col, Modal, Skeleton, message } from "antd";
import { ITitle, IImage, ITableHtml } from "../../components";
import { colors, images } from "../../../assets";

import { APIService } from "../../../services";
import { useParams } from "react-router-dom";

import FormatterDay from "../../../utils/FormatterDay";

function DetailBannerPage(props) {
  const [isLoading, setIsLoading] = useState(true);
  const paramsBanner = useParams();

  const [dataDetail, setDataDetail] = useState({
    banner: {},
    loading: false,
    image_url: "",
    condition_show_for_event: {},
  });

  useEffect(() => {
    _getAPIDetailBanner(paramsBanner.id);
  }, []);

  const _getAPIDetailBanner = async (id) => {
    try {
      const data = await APIService._getDetailBanner(id);
      setDataDetail(data);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
      paddingLeft: 10,
    },
    {
      name: "Cấp đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
  ];

  const headerTable = (dataHeader) => {
    return (
      <tr className="tr-table scroll">
        {dataHeader.map((item, index) => (
          <th
            className="th-table"
            style={{ textAlign: item.align, paddingLeft: item.paddingLeft }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (databody) => {
    return databody.map((item) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal", paddingLeft: 10 }}
        >
          {item.user_agency_name}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {item.level}
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "left",
            fontWeight: "normal",
            width: 120,
          }}
        >
          {item.user_agency_code}
        </td>
      </tr>
    ));
  };

  const { banner, image_url } = dataDetail;

  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 50]}>
        <Col span={24}>
          <ITitle
            level={1}
            title="Chi tiết banner"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        <Col span={24} xl={16} xxl={14}>
          <div
            style={{
              height: "100%",
            }}
          >
            <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
              <Col span={16}>
                <div
                  style={{
                    padding: "30px 40px",
                    background: colors.white,
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                    height:
                      banner.banner_type === 1 && banner.event_type === 2
                        ? 880
                        : 700,
                  }}
                >
                  {isLoading ? (
                    <div>
                      <Skeleton
                        loading={true}
                        active
                        paragraph={{ rows: 16 }}
                      />
                    </div>
                  ) : (
                    <>
                      <Row gutter={[12, 20]}>
                        <Col>
                          <ITitle
                            level={2}
                            title="Thông tin banner"
                            style={{
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col>
                          <div>
                            <p style={{ fontWeight: 600 }}>Tiêu đề</p>
                            <span>{!banner.title ? "-" : banner.title}</span>
                          </div>
                        </Col>
                        <Col>
                          <Row gutter={[12, 12]}>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 600 }}>Loại banner</p>
                                <span>
                                  {!banner.banner_type
                                    ? "-"
                                    : banner.banner_type === 1
                                    ? "Banner chính sách"
                                    : banner.banner_type === 2
                                    ? "Banner sản phẩm"
                                    : banner.banner_type === 3
                                    ? "Banner quảng bá"
                                    : "Banner ngành hàng con"}
                                </span>
                              </div>
                            </Col>
                            {banner.banner_type === 3 ? null : (
                              <Col span={12}>
                                <div>
                                  <p style={{ fontWeight: 600 }}>
                                    {banner.banner_type === 1
                                      ? "Tên chính sách"
                                      : banner.banner_type === 2
                                      ? "Tên sản phẩm"
                                      : "Tên ngành hàng con"}
                                  </p>
                                  <span>
                                    {!banner.banner_type_name
                                      ? "-"
                                      : banner.banner_type_name}
                                  </span>
                                </div>
                              </Col>
                            )}
                          </Row>
                        </Col>
                        <Col>
                          <Row gutter={[12, 12]}>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 600 }}>Ngày bắt đầu</p>
                                <span>
                                  {!banner.start_date
                                    ? "-"
                                    : FormatterDay.dateFormatWithString(
                                        banner.start_date,
                                        "#DD#/#MM#/#YYYY#"
                                      )}
                                </span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 600 }}>Ngày kết thúc</p>
                                <span>
                                  {!banner.end_date
                                    ? "-"
                                    : FormatterDay.dateFormatWithString(
                                        banner.end_date,
                                        "#DD#/#MM#/#YYYY#"
                                      )}
                                </span>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                      <div style={{ marginTop: 42 }}>
                        <Row gutter={[12, 20]}>
                          <Col style={{ marginTop: 6 }}>
                            <ITitle
                              level={2}
                              title="Điều kiện hiển thị"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col>
                            <Row gutter={[12, 12]}>
                              <Col span={12}>
                                <div>
                                  <div
                                    style={{
                                      marginBottom:
                                        banner.event_type === 1 ? 10 : 0,
                                    }}
                                  >
                                    <p style={{ fontWeight: 600 }}>Khu vực</p>
                                  </div>
                                  {banner.banner_type === 2 ||
                                  banner.banner_type === 3 ||
                                  banner.banner_type === 4 ? (
                                    <p style={{ padding: "6px 0px" }}>
                                      <span>{banner.region_name}</span>
                                    </p>
                                  ) : banner.event_type === 1 ? (
                                    <p
                                      style={{
                                        padding: "6px 6px",
                                        minHeight: 150,
                                        overflow: "auto",
                                        border:
                                          "1px solid rgba(122, 123, 123, 0.5)",
                                      }}
                                    >
                                      {banner.condition_show_for_event.region.map(
                                        (item) => {
                                          return (
                                            <span>{item.name + ", "}</span>
                                          );
                                        }
                                      )}
                                    </p>
                                  ) : (
                                    <p>-</p>
                                  )}
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <div
                                    style={{
                                      marginBottom:
                                        banner.event_type === 1 ? 10 : 0,
                                    }}
                                  >
                                    <p style={{ fontWeight: 600 }}>
                                      Tỉnh/Thành phố
                                    </p>
                                  </div>
                                  {banner.banner_type === 2 ||
                                  banner.banner_type === 3 ||
                                  banner.banner_type === 4 ? (
                                    <p style={{ padding: "6px 0px" }}>
                                      <span>{banner.city_name}</span>
                                    </p>
                                  ) : banner.event_type === 1 ? (
                                    <p
                                      style={{
                                        padding: "6px 6px",
                                        minHeight: 150,
                                        maxHeight: 150,
                                        overflow: "auto",
                                        border:
                                          "1px solid rgba(122, 123, 123, 0.5)",
                                      }}
                                    >
                                      {banner.condition_show_for_event.city.map(
                                        (item) => {
                                          return (
                                            <span>{item.name + ", "}</span>
                                          );
                                        }
                                      )}
                                    </p>
                                  ) : (
                                    <p>-</p>
                                  )}
                                </div>
                              </Col>
                            </Row>
                          </Col>

                          <Col>
                            <div>
                              <p style={{ fontWeight: 600 }}>Cấp người dùng</p>
                              <span>
                                {!banner.user_type_name
                                  ? "-"
                                  : banner.user_type_name}
                              </span>
                            </div>
                          </Col>
                          {banner.banner_type === 2 ||
                          banner.banner_type ===
                            3 ? null : banner.event_type !== 2 ? null : (
                            <>
                              <Col style={{ marginTop: 15 }}>
                                <ITitle
                                  level={2}
                                  title="Đại lý hiển thị"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col>
                                <ITableHtml
                                  height="300px"
                                  childrenBody={bodyTable(
                                    banner.condition_show_for_event.user
                                  )}
                                  childrenHeader={headerTable(dataHeader)}
                                  style={{
                                    border:
                                      "1px solid rgba(122, 123, 123, 0.5)",
                                    // padding: "0px 8px ",
                                    overflow: "auto",
                                  }}
                                  isBorder={false}
                                />
                              </Col>
                            </>
                          )}
                        </Row>
                      </div>
                    </>
                  )}
                </div>
              </Col>
              <Col span={8}>
                <div
                  style={{
                    padding: "30px 40px",
                    background: colors.white,
                    height: "100%",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                    width: 330,
                  }}
                >
                  {isLoading ? (
                    <div>
                      <Skeleton
                        loading={true}
                        active
                        paragraph={{ rows: 16 }}
                      />
                    </div>
                  ) : (
                    <div>
                      <ITitle
                        level={2}
                        title="Hình ảnh"
                        style={{
                          fontWeight: "bold",
                        }}
                      />
                      <IImage
                        src={image_url + banner.image}
                        style={{
                          width: 250,
                          height: 100,
                          borderRadius: 10,
                          marginTop: 20,
                        }}
                      />
                    </div>
                  )}
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default DetailBannerPage;
