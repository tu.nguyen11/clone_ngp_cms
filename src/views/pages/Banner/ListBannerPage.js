import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message, Modal } from "antd";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IButton,
  IImage,
} from "../../components";
import { colors, images } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import ModalBanner from "./ModalBanner";
import { useHistory } from "react-router-dom";

const formatStringDate = "#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#";
function ListBannerPage(props) {
  const history = useHistory();
  const [filter, setFilter] = useState({
    key: "",
    page: 1,
    type: -1,
  });

  const [dataTable, setDataTable] = useState({
    listBanner: [],
    total: 1,
    size: 10,
    image_url: "",
  });

  const [loadingTable, setLoadingTable] = useState(true);
  const [visibleModalBanner, setVisibleModalBanner] = useState(false);

  const _fetchAPIListBanner = async (filter) => {
    try {
      const data = await APIService._getListBanner(filter);
      data.listBanner.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        return item;
      });

      let dataID = data.listBanner.map((item) => {
        return item.id;
      });

      setDataTable({
        listBanner: data.listBanner,
        size: data.size,
        total: data.total,
        image_url: data.image_url,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIListBanner(filter);
  }, [filter]);

  const typeBanner = [
    {
      key: -1,
      value: "Tất cả",
    },
    {
      key: 1,
      value: "Banner chính sách",
    },
    {
      key: 2,
      value: "Banner sản phẩm",
    },
    {
      key: 3,
      value: "Banner quảng bá",
    },
    {
      key: 4,
      value: "Banner ngành hàng con",
    },
  ];

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tiêu đề",
      dataIndex: "title",
      key: "title",
      render: (title) => (
        <Tooltip title={title}>
          <span>{title}</span>
        </Tooltip>
      ),
    },
    {
      title: "Loại banner",
      dataIndex: "type_name",
      key: "type_name",
      align: "center",
      render: (type_name) => <span>{type_name}</span>,
    },
    {
      title: "Hình ảnh",
      dataIndex: "image",
      align: "center",
      key: "image",
      render: (image) => {
        return (
          <IImage
            src={dataTable.image_url + image}
            style={{ width: 60, height: 35 }}
          />
        );
      },
    },
    {
      title: "Ngày bắt đầu",
      dataIndex: "start_date",
      key: "start_date",
      render: (start_date) => (
        <span>
          {FormatterDay.dateFormatWithString(start_date, formatStringDate)}
        </span>
      ),
    },
    {
      title: "Ngày kết thúc",
      dataIndex: "end_date",
      key: "end_date",
      render: (end_date) => (
        <span>
          {FormatterDay.dateFormatWithString(end_date, formatStringDate)}
        </span>
      ),
    },
    {
      title: "Thứ tự hiển thị",
      dataIndex: "sort_order",
      key: "sort_order",
      align: "center",
      render: (sort_order) =>
        sort_order === 0 ? <span>-</span> : <span>{sort_order}</span>,
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Danh sách banner
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tiêu đề banner">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tiêu đề banner"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={12}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <ITitle
                        title="Banner"
                        level={4}
                        style={{
                          marginLeft: 20,
                        }}
                      />
                      <div style={{ margin: "0px 15px 0px 15px" }}>
                        <div style={{ width: 200 }}>
                          <ISelect
                            defaultValue="Tất cả"
                            data={typeBanner}
                            select={true}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setFilter({
                                ...filter,
                                type: value,
                                page: 1,
                              });
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col span={12}>
                    <div className="flex justify-end">
                      <IButton
                        icon={ISvg.NAME.ARROWUP}
                        title={"Tạo mới"}
                        color={colors.main}
                        styleHeight={{
                          width: 120,
                          marginLeft: 25,
                        }}
                        onClick={() => {
                          history.push("/banner/create/0");
                        }}
                      />
                      <IButton
                        title={"Ưu tiên hiển thị"}
                        color={colors.main}
                        styleHeight={{
                          width: 180,
                          marginLeft: 25,
                        }}
                        onClick={() => {
                          setVisibleModalBanner(true);
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listBanner}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.listBanner[indexRow].id;
                          props.history.push("/banner/detail/" + id);
                        }, // click row
                        onMouseDown: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.listBanner[indexRow].id;
                          if (event.button == 2 || event == 4) {
                            window.open("/banner/detail/" + id, "_blank");
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={visibleModalBanner}
        footer={null}
        width={1450}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        <ModalBanner
          openModalProps={visibleModalBanner}
          handleCancel={() => {
            setVisibleModalBanner(false);
          }}
          callback1={() => {
            _fetchAPIListBanner(filter);
          }}
        />
      </Modal>
    </div>
  );
}

export default ListBannerPage;
