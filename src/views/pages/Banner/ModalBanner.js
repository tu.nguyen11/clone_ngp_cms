import React, { useState, useEffect } from "react";
import { Row, Col, List, Checkbox, Input, message, notification } from "antd";
import { colors } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { ITitle, ISvg, IButton, ISelect } from "../../components";
import { APIService } from "../../../services";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import FormatterDay from "../../../utils/FormatterDay";

const { Search } = Input;

const formatStringDate = "#DD#/#MM#/#YYYY#";
const reorder = (list, startIndex, endIndex) => {
  let result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const openNotification = () => {
  notification.error({
    message: "Thông báo",
    description: "Vượt quá số lượng quy định (20). Vui lòng xóa bớt banner",
    onClick: () => {
      console.log("Notification Clicked!");
    },
  });
};

const ModalBanner = (props) => {
  const [dataTable, setDataTable] = useState({
    rows: [],
    loadingList: true,
  });
  const [loading, setLoading] = useState(true);

  const [isLoadingSave, setIsLoadingSave] = useState(false);
  const [dataList, setDataList] = useState([]);
  const [dataIDCheck, setDataIDCheck] = useState([]);
  const [filter, setFilter] = useState({
    search: "",
  });

  const [filterLoadMore, setFilterLoadMore] = useState({
    search: "",
  });

  const columns = [
    { title: "STT", key: "stt", width: 60, textAlign: "center" },
    {
      title: "Tên banner",
      key: "title",
      width: 250,
      textAlign: "left",
    },
    {
      title: "Loại banner",
      key: "banner_type",
      width: 180,
      textAlign: "left",
    },
    {
      title: "Ngày bắt đầu",
      key: "start_date",
      width: 120,
      textAlign: "left",
    },
    {
      title: "Ngày kết thúc",
      key: "end_date",
      width: 120,
      textAlign: "left",
    },
    { title: "", key: "id", width: 80, textAlign: "center" },
  ];

  const onDragEnd = (result) => {
    console.log(result);
    if (!result.destination) {
      return;
    }

    let items = reorder(
      dataTable.rows,
      result.source.index,
      result.destination.index
    );
    items.map((item, index) => {
      item.stt = index + 1;
    });
    let dataIDDrag = items.map((item) => item.id);
    dataList.map((item, index) => {
      if (dataIDDrag.indexOf(Number(item.id)) != -1) {
        item.number = dataIDDrag.indexOf(Number(item.id)) + 1;
        return;
      }
    });
    dataTable.rows = items;
    setDataTable({ ...dataTable, rows: dataTable.rows });
    setDataList(dataList);
    setDataIDCheck(dataIDDrag);
  };

  const _fetchAPIListHotBanner = async (filter) => {
    try {
      const data = await APIService._getAPIAllListHotBanner();
      data.banner.map((item, index) => {
        item.stt = index + 1;
        item.code = "code" + item.id;
        return item;
      });

      let dataID = data.banner.map((item) => {
        return item.id;
      });

      setDataTable({
        ...dataTable,
        rows: data.banner,
      });

      setDataIDCheck(dataID);
      setLoading(false);
      await _fetchAPIListSelectHotBanner(filter.search, dataID);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchAPIListSelectHotBanner = async (filter, dataID) => {
    try {
      const data = await APIService._getAPIListSelectHotBanner(filter);
      if (dataID.length !== 0) {
        let arrDataID = [...dataID];
        setDataIDCheck(arrDataID);
        data.lst_banner.map((item, index) => {
          if (arrDataID.indexOf(Number(item.id)) != -1) {
            item.is_check = 1;
            // item.number = arrDataID.indexOf(Number(item.id)) + 1;
            return;
          }
          return (item.is_check = 0);
        });
      } else {
        data.lst_banner.map((item, index) => {
          if (dataIDCheck.indexOf(Number(item.id)) != -1) {
            item.is_check = 1;
            // item.number = dataIDCheck.indexOf(Number(item.id)) + 1;
            return;
          }
          return (item.is_check = 0);
        });
      }
      setDataList([...data.lst_banner]);
    } catch (err) {
      console.log(err);
    }
  };

  const _postUpdateListHotBanner = async (arr) => {
    try {
      const data = await APIService._postUpdateHotBanner(arr);
      message.success("Cập nhật thành công");
      setIsLoadingSave(false);
      props.handleCancel();
      props.callback1();
    } catch (err) {
      setIsLoadingSave(false);
    }
  };

  const handleRemove = (id) => {
    let dataIDCheckClone = [...dataIDCheck];
    let index = dataTable.rows
      .map((item, index) => {
        return item.id;
      })
      .indexOf(id);
    dataTable.rows.splice(index, 1);
    dataList.map((item, index) => {
      if (item.id == id) {
        item.is_check = 0;
        item.number = 0;
      }
    });

    let indexIdCheck = dataIDCheckClone.indexOf(id);
    dataIDCheckClone.splice(indexIdCheck, 1);

    dataList.map((item, index) => {
      if (dataIDCheckClone.indexOf(Number(item.id)) != -1) {
        item.number = dataIDCheckClone.indexOf(Number(item.id)) + 1;
        return;
      }
    });
    dataTable.rows.map((item, index) => {
      item.stt = index + 1;
    });
    setDataTable({ ...dataTable, rows: dataTable.rows });
    setDataList(dataList);
    setDataIDCheck(dataIDCheckClone);
  };

  useEffect(() => {
    _fetchAPIListHotBanner(filter);
  }, [filter, props.openModalProps]);

  useEffect(() => {
    _fetchAPIListSelectHotBanner(filterLoadMore.search, []);
  }, [filterLoadMore]);

  const tableHeaders = (
    <thead>
      <div>
        <tr>
          {columns.map(function (column) {
            return (
              <th width={column.width} style={{ textAlign: column.textAlign }}>
                {column.title}
              </th>
            );
          })}
        </tr>
      </div>
    </thead>
  );

  return (
    <Row gutter={[0, 24]}>
      <Col span={24}>
        <div>
          <StyledITitle>Sắp xếp độ ưu tiên hiển thị banner</StyledITitle>
        </div>
      </Col>
      <Col span={24}>
        <Row gutter={[24, 0]}>
          <Col span={10}>
            <Row gutter={[0, 15]}>
              <Col span={24}>
                <Search
                  placeholder="tìm banner"
                  onPressEnter={(e) => {
                    setFilterLoadMore({
                      ...filterLoadMore,
                      search: e.target.value,
                    });
                  }}
                />
              </Col>
              <Col span={24}>
                <Row>
                  <div
                    style={{
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                      backgroundColor: "#E4EFFF",
                      height: 50,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Col span={2}></Col>
                    <Col span={7}>
                      <span style={{ fontWeight: "bold", padding: "12px" }}>
                        Tên banner
                      </span>
                    </Col>
                    <Col span={5}>
                      <span style={{ fontWeight: "bold", padding: "12px" }}>
                        Loại banner
                      </span>
                    </Col>
                    <Col
                      span={5}
                      style={{ display: "flex", justifyContent: "center" }}
                    >
                      <span style={{ fontWeight: "bold", padding: "12px" }}>
                        Ngày bắt đầu
                      </span>
                    </Col>
                    <Col
                      span={5}
                      style={{ display: "flex", justifyContent: "center" }}
                    >
                      <span style={{ fontWeight: "bold", padding: "12px" }}>
                        Ngày kết thúc
                      </span>
                    </Col>
                  </div>
                </Row>

                <div
                  style={{
                    height: 350,
                    overflowY: "auto",
                    overflowX: "hidden",
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}
                >
                  <List
                    dataSource={dataList}
                    bordered={false}
                    renderItem={(item, index) => (
                      <Row>
                        <List.Item>
                          <Col span={2}>
                            <Checkbox
                              value={{ ...item, ...{ stt: index } }}
                              checked={item.is_check === 1 ? true : false}
                              style={{ marginRight: 35 }}
                              defaultChecked={
                                item.is_check === 1 ? true : false
                              }
                              onChange={(e) => {
                                if (e.target.checked === true) {
                                  dataList[index].is_check = 1;
                                  setDataList(dataList.slice(0));
                                } else {
                                  dataList[index].is_check = 0;
                                  setDataList(dataList.slice(0));
                                }
                              }}
                            />
                          </Col>
                          <Col span={7}>
                            <ITitle level={4} title={item.title} />
                          </Col>
                          <Col span={5} style={{ marginLeft: 15 }}>
                            <ITitle
                              level={4}
                              title={
                                item.banner_type === 1
                                  ? "Banner chính sách"
                                  : item.banner_type === 2
                                  ? "Banner sản phẩm"
                                  : item.banner_type === 3
                                  ? "Banner quảng bá"
                                  : "Banner ngành hàng con"
                              }
                            />
                          </Col>
                          <Col span={5} style={{ marginLeft: 15 }}>
                            <ITitle
                              level={4}
                              title={FormatterDay.dateFormatWithString(
                                item.start_date,
                                formatStringDate
                              )}
                            />
                          </Col>
                          <Col span={5} style={{ marginLeft: 15 }}>
                            <ITitle
                              level={4}
                              title={FormatterDay.dateFormatWithString(
                                item.end_date,
                                formatStringDate
                              )}
                            />
                          </Col>
                        </List.Item>
                      </Row>
                    )}
                  />
                </div>
              </Col>
              <Col span={24}>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Thêm"
                    color={colors.main}
                    icon={ISvg.NAME.BUTTONRIGHT}
                    styleHeight={{
                      width: 140,
                    }}
                    isRight={true}
                    onClick={() => {
                      let dataCheck = dataList;
                      let dataRows = dataTable.rows;
                      let dataIDCheckClone = dataIDCheck;
                      let dataNew = [];
                      let sumCheck = 0;
                      dataRows.map((item, index) => {
                        sumCheck = sumCheck + 1;
                      });

                      dataCheck.map((item, index) => {
                        if (item.is_check === 1) {
                          let dataFilter = dataTable.rows.find((item1) => {
                            return item1.id === item.id;
                          });
                          if (!dataFilter) {
                            dataNew.push(item);
                          }
                        }
                      });

                      dataNew.map((item) => {
                        sumCheck = sumCheck + 1;
                      });

                      if (dataNew.length === 0) {
                        message.warning("Bạn chưa chọn banner để thêm");
                        return;
                      }

                      if (sumCheck > 20 || sumCheck == 0) {
                        openNotification();
                        return;
                      } else {
                        dataNew.map((item, index) => {
                          item.code = "code" + item.id;
                          return item;
                        });
                        dataNew.map((item) => {
                          dataTable.rows.unshift(item);
                          dataIDCheckClone.unshift(item.id);
                        });
                        dataTable.rows.map((item, index) => {
                          item.stt = index + 1;
                        });
                        setDataTable({ ...dataTable, rows: dataTable.rows });
                        setDataIDCheck(dataIDCheckClone);
                      }
                      return;
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={14}>
            <div
              style={{
                height: 505,
                overflow: "auto",
                border: "1px solid rgba(122, 123, 123, 0.5)",
              }}
            >
              <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="droppable">
                  {(droppableProvided, droppableSnapshot) => (
                    <div ref={droppableProvided.innerRef}>
                      <table
                        className="table table-bordered table-hover"
                        width="100%"
                      >
                        {tableHeaders}

                        {dataTable.rows.map((row, index) => {
                          return (
                            <Draggable
                              key={row.id}
                              draggableId={row.code}
                              index={index}
                            >
                              {(draggableProvided, draggableSnapshot) => (
                                <div
                                  className="table-edit"
                                  ref={draggableProvided.innerRef}
                                  {...draggableProvided.draggableProps}
                                  {...draggableProvided.dragHandleProps}
                                >
                                  <tr /*ref={lastProductElementRef}*/>
                                    {columns.map(function (column) {
                                      return column.key == "id" ? (
                                        <td
                                          width={column.width}
                                          style={{
                                            textAlign: column.textAlign,
                                          }}
                                        >
                                          <div
                                            style={{
                                              width: 20,
                                              height: 20,
                                              borderRadius: 10,
                                              display: "flex",
                                              justifyContent: "center",
                                              alignItems: "center",
                                              background: colors.gray._300,
                                              cursor: "pointer",
                                            }}
                                            onClick={() => {
                                              handleRemove(row[column.key]);
                                            }}
                                          >
                                            <ISvg
                                              name={ISvg.NAME.CROSS}
                                              width={7}
                                              height={7}
                                              fill={colors.oranges}
                                            />
                                          </div>
                                        </td>
                                      ) : (
                                        <td
                                          width={column.width}
                                          style={{
                                            textAlign: column.textAlign,
                                          }}
                                        >
                                          {column.key === "start_date"
                                            ? FormatterDay.dateFormatWithString(
                                                row[column.key],
                                                formatStringDate
                                              )
                                            : column.key === "end_date"
                                            ? FormatterDay.dateFormatWithString(
                                                row[column.key],
                                                formatStringDate
                                              )
                                            : column.key !== "banner_type"
                                            ? row[column.key]
                                            : row[column.key] === 1
                                            ? "Banner chính sách"
                                            : row[column.key] === 2
                                            ? "Banner sản phẩm"
                                            : row[column.key] === 3
                                            ? "Banner quảng bá"
                                            : "Banner ngành hàng con"}
                                        </td>
                                      );
                                    })}
                                  </tr>
                                </div>
                              )}
                            </Draggable>
                          );
                        })}
                        {droppableProvided.placeholder}
                      </table>
                    </div>
                  )}
                </Droppable>
              </DragDropContext>
            </div>
          </Col>
        </Row>
      </Col>
      <Col span={24}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "absolute",
            top: 60,
            right: -25,
          }}
        >
          <IButton
            title="Lưu"
            color={colors.main}
            icon={ISvg.NAME.SAVE}
            loading={isLoadingSave}
            styleHeight={{
              width: 140,
              marginRight: 24,
            }}
            onClick={() => {
              setIsLoadingSave(true);
              let arrId = dataTable.rows.map((item, index) => {
                return {
                  id: item.id,
                };
              });
              _postUpdateListHotBanner(arrId);
            }}
          />
          <IButton
            title="Hủy bỏ"
            color={colors.oranges}
            icon={ISvg.NAME.CROSS}
            styleHeight={{
              width: 140,
            }}
            onClick={() => {
              props.handleCancel();
            }}
          />
        </div>
      </Col>
    </Row>
  );
};

export default ModalBanner;
