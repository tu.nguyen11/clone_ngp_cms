import React, { Component } from "react";
import { Row, Col, Button, List, message, Skeleton, Spin } from "antd";
import { ITitle, IButton, IImage, IChart } from "../../components";
import { colors } from "../../../assets";

import { priceFormat, dayInMonth } from "../../../utils";
import TweenOne from "rc-tween-one";
import Children from "rc-tween-one/lib/plugin/ChildrenPlugin";
import FormatterDay from "../../../utils/FormatterDay";
import { APIService } from "../../../services";

TweenOne.plugins.push(Children);

const DAILYNEW = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Số đại lý tạo mới",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu"
      };
    return {
      name: "Số đại lý tạo mới",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value
    };
  }
];

const DAILYHĐ = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Số đại lý hoạt động",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu"
      };
    return {
      name: "Số đại lý hoạt động",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value
    };
  }
];

const DAILYNOHĐ = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Số đại lý không hoạt động",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu"
      };
    return {
      name: "Số đại lý không hoạt động",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value
    };
  }
];

export default class ApplicationDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 10000,
      year: 0,
      city_id: 0,
      month: 0,
      dataDaiLyNew: [],
      dataAgencyWork: [],
      dataAgencyNotWork: [],
      animation: {
        Children: {
          value: 10000
        },
        duration: 1000
      },
      formatMoney: false,
      isLoading: true,
      isSpin: false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.warn("hook", nextProps, prevState);
    if (
      nextProps.year !== prevState.year ||
      nextProps.month !== prevState.month ||
      nextProps.city_id !== prevState.city_id
    ) {
      return {
        year: nextProps.year,
        month: nextProps.month,
        city_id: nextProps.city_id
      };
    } else return null;
  }

  async componentDidUpdate(prevProps, prevState) {
    if (
      prevState.year !== this.state.year ||
      prevState.month !== this.state.month ||
      prevState.city_id !== this.state.city_id
    ) {
      await this.setState({
        isSpin: true
      });

      // await this._APIgetChartCountNewAgencyDashboard(
      //   this.state.month,
      //   this.state.year,
      //   this.state.city_id
      // );

      // await this._APIgetChartCountNewAgencyWorkDashboard(
      //   this.state.month,
      //   this.state.year,
      //   this.state.city_id
      // );
      // await this._APIgetChartCountNewAgencyNotWorkDashboard(
      //   this.state.month,
      //   this.state.year,
      //   this.state.city_id
      // );

      const data = await Promise.all([
        this._APIgetChartCountNewAgencyDashboard(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetChartCountNewAgencyWorkDashboard(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetChartCountNewAgencyNotWorkDashboard(
          this.state.month,
          this.state.year,
          this.state.city_id
        )
      ]);

      await this.setState({
        isSpin: false
      });
    }
  }

  _APIgetChartCountNewAgencyDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getChartCountNewAgencyDashboard(
        month,
        year,
        city_id
      );
      let lengthDayData = data.new_agency.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.new_agency.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null
          });
        }
      }

      this.setState({
        // dataDaiLyNew: data.new_agency.chart
        dataDaiLyNew: dataNew
      });
    } catch (err) {
      message.err(err);
    }
  };

  _APIgetChartCountNewAgencyWorkDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getChartCountNewAgencyWorkDashboard(
        month,
        year,
        city_id
      );
      let lengthDayData = data.agency_work.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.agency_work.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null
          });
        }
      }

      this.setState({
        dataAgencyWork: dataNew
      });
    } catch (err) {
      message.err(err);
    }
  };

  _APIgetChartCountNewAgencyNotWorkDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getChartCountNewAgencyNotWorkDashboard(
        month,
        year,
        city_id
      );
      let lengthDayData = data.agency_not_work.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.agency_not_work.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null
          });
        }
      }

      this.setState({
        dataAgencyNotWork: dataNew
      });
    } catch (err) {
      message.err(err);
    }
  };

  async componentDidMount() {
    // await this._APIgetChartCountNewAgencyDashboard(
    //   this.state.month,
    //   this.state.year,
    //   this.state.city_id
    // );

    // await this._APIgetChartCountNewAgencyWorkDashboard(
    //   this.state.month,
    //   this.state.year,
    //   this.state.city_id
    // );
    // await this._APIgetChartCountNewAgencyNotWorkDashboard(
    //   this.state.month,
    //   this.state.year,
    //   this.state.city_id
    // );

    const data = await Promise.all([
      this._APIgetChartCountNewAgencyDashboard(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetChartCountNewAgencyWorkDashboard(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetChartCountNewAgencyNotWorkDashboard(
        this.state.month,
        this.state.year,
        this.state.city_id
      )
    ]);

    await this.setState({
      isLoading: false
    });
  }

  render() {
    const { dataDaiLyNew, dataAgencyWork, dataAgencyNotWork } = this.state;
    let scalee = {
      date: {
        type: "time",
        mask: "D",
        tickCount: dayInMonth(this.state.month, this.state.year),
        sync: true
      }
    };
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          border: "1px solid  #DEDFE4",
          height: "100%"
        }}
      >
        <Spin spinning={this.state.isSpin}>
          <Row style={{ height: "100%" }}>
            <Col
              lg={12}
              style={{ height: "100%", borderRight: "1px solid  #DEDFE4" }}
            >
              <Row
                lg={12}
                style={{
                  borderBottom: "1px solid #DEDFE4",
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30
                }}
              >
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 6 }}
                >
                  <div style={{}}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16
                      }}
                    >
                      <ITitle
                        level={3}
                        title="Đại lý tạo mới"
                        style={{
                          color: colors.black,
                          fontWeight: "bold",
                          flex: 1,
                          display: "flex",
                          alignItems: "flex-end"
                        }}
                      />
                    </div>
                    <IChart
                      colorChart={colors.main}
                      height={270}
                      data={dataDaiLyNew}
                      tooltip1={DAILYNEW}
                      scale={scalee}
                      position1="date*value"
                    />
                  </div>
                </Skeleton>
              </Row>
              <Row
                lg={12}
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30
                }}
              >
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 6 }}
                >
                  <div style={{}}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Đại lý hoạt động"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end"
                          }}
                        />
                      </div>
                      <IChart
                        colorChart={colors.blackChart}
                        height={270}
                        data={dataAgencyWork}
                        tooltip1={DAILYHĐ}
                        scale={scalee}
                        position1="date*value"
                      />
                    </Row>
                  </div>
                </Skeleton>
              </Row>
            </Col>
            <Col
              lg={8}
              style={{
                height: "100%"
              }}
            >
              <div
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30
                }}
              >
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 12 }}
                >
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Đại lý không hoạt động"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end"
                          }}
                        />
                      </div>
                      <IChart
                        colorChart={colors.red}
                        height={180}
                        data={dataAgencyNotWork}
                        tooltip1={DAILYNOHĐ}
                        scale={scalee}
                        position1="date*value"
                      />
                    </Row>
                  </Row>
                </Skeleton>
              </div>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }
}
