import React, { Component } from "react";
import { Row, Col, Button, List } from "antd";
import { ITitle, IButton, IImage } from "../../components";
import { colors } from "../../../assets";
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";
import { priceFormat } from "../../../utils";
import TweenOne from "rc-tween-one";
import Children from "rc-tween-one/lib/plugin/ChildrenPlugin";
const ButtonGroup = Button.Group;

var second = 1000;
var minute = 1000 * 60;
var hour = 60 * minute;
var day = 24 * hour;

const dataList = [
  {
    src:
      "http://dev-res.vinhhoang.com.vn/kido_image/product/1577257388748575.png",
    name: "Gạo Hạt Ngọc Trời Tiên Nữ",
    sluong: 10,
    doanhso: 120000
  },
  {
    src:
      "http://dev-res.vinhhoang.com.vn/kido_image/product/1577257388748575.png",
    name: "Gạo Hạt Ngọc Trời Tiên Nữ",
    sluong: 10,
    doanhso: 120000
  },
  {
    src:
      "http://dev-res.vinhhoang.com.vn/kido_image/product/1577257388748575.png",
    name: "Gạo Hạt Ngọc Trời Tiên Nữ",
    sluong: 10,
    doanhso: 120000
  },
  {
    src:
      "http://dev-res.vinhhoang.com.vn/kido_image/product/1577257388748575.png",
    name: "Gạo Hạt Ngọc Trời Tiên Nữ",
    sluong: 10,
    doanhso: 120000
  }
];
TweenOne.plugins.push(Children);
function toInterge(number, fix = 1) {
  if (Math.round(number) === number) {
    return "" + number;
  }

  return "" + Number(number).toFixed(fix);
}

function humanizeDuration(duration, fix = 1) {
  if (duration === 0) {
    return 0;
  }

  if (duration < minute) {
    return toInterge(duration / second, fix) + " 秒";
  }

  if (duration < hour) {
    return toInterge(duration / minute, fix) + " 分";
  }

  if (duration < day) {
    return toInterge(duration / hour, fix) + "小时";
  }

  return toInterge(duration / hour / 24, fix) + " 天";
}

var data = [
  {
    date: 1580490000000,
    pv: 100000000,
    time: 12351000,
    count: 1
  },
  {
    date: 1580576400000,
    pv: 150000000,
    time: 15000,
    count: 2
  },
  {
    date: 1580662800000,
    pv: 100000000,
    time: 25000,
    count: 3
  },
  {
    date: 1580749200000,
    pv: 300000000,
    time: 30000,
    count: 4
  },
  {
    date: 1580835600000,
    pv: 250000000,
    time: 35000,
    count: 5
  },
  {
    date: 1580922000000,
    pv: 230000000,
    time: 12351000,
    count: 1
  },
  {
    date: 1581008400000,
    pv: 251242356,
    time: 15000,
    count: 2
  },
  {
    date: 1581094800000,
    pv: 451242356,
    time: 25000,
    count: 3
  },
  {
    date: 1581181200000,
    pv: 151242356,

    time: 30000,
    count: 4
  },
  {
    date: 1581267600000,
    pv: 251242356,
    time: 35000,
    count: 5
  }
];

var dataOrder = [
  {
    date: 1580490000000,
    pv: 100,
    time: 12351000,
    count: 1
  },
  {
    date: 1580576400000,
    pv: 51,
    time: 15000,
    count: 2
  },
  {
    date: 1580662800000,
    pv: 30,
    time: 25000,
    count: 3
  },
  {
    date: 1580749200000,
    pv: 31,
    time: 30000,
    count: 4
  },
  {
    date: 1580835600000,
    pv: 22,
    time: 35000,
    count: 5
  },
  {
    date: 1580922000000,
    pv: 12,
    time: 12351000,
    count: 1
  },
  {
    date: 1581008400000,
    pv: 8,
    time: 15000,
    count: 2
  },
  {
    date: 1581094800000,
    pv: 6,
    time: 25000,
    count: 3
  },
  {
    date: 1581181200000,
    pv: 5,
    time: 30000,
    count: 4
  },
  {
    date: 1581267600000,
    pv: 68,
    time: 35000,
    count: 5
  }
];

function pick(data, field) {
  return data.map(function(item) {
    var result = {};

    for (var key in item) {
      if (item.hasOwnProperty(key) && field.indexOf(key) !== -1) {
        result[key] = item[key];
      }
    }

    return result;
  });
}

var scale = {
  date: {
    alias: "Doanh số",
    type: "time",
    mask: "DD/MM/YYYY"
  },
  pv: {
    alias: "Doanh số",

    min: 0,
    tickCount: 6
  },
  time: {
    alias: "Doanh số",

    formatter: function(value) {
      return humanizeDuration(value, 0);
    },
    tickCount: 6
  },
  count: {
    alias: "Doanh số"
  }
};

export default class OperateDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 10000,
      animation: {
        Children: {
          value: 10000
        },
        duration: 1000
      },
      formatMoney: false
    };
  }
  _renderChart = (colorChart, height, data = []) => {
    return (
      <div>
        <Chart
          height={height}
          padding={[20, 80, 80, 80]}
          forceFit
          scale={{
            time: {
              sync: true
            }
          }}
        >
          <Tooltip />
          <View data={pick(data, ["pv", "time", "date"])} scale={scale}>
            <Axis name="time" grid={null} />
            <Geom
              type="line"
              position="date*pv*count"
              color={colorChart}
              size={2}
            />
          </View>
          {/* <View
    data={pick(dash, ["pv", "time", "date"])}
    scale={scale}
  >
    <Axis name="time" visible={false} />
    <Geom
      type="line"
      position="date*time"
      color="white"
      size={3}
      style={{
        lineDash: [4, 4]
      }}
    />
  </View> */}
        </Chart>
      </div>
    );
  };

  render() {
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          border: "1px solid  #DEDFE4",
          height: "100%"
        }}
      >
        <Row style={{ height: "100%" }}>
          <Col lg={12} style={{ height: "100%" }}>
            <Row lg={12} style={{ borderBottom: "1px solid #DEDFE4" }}>
              <div
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginBottom: 16
                  }}
                >
                  <ITitle
                    level={3}
                    title="Tỷ lệ giao hàng hàng thực tế trên đơn hàng"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      flex: 1,
                      display: "flex",
                      alignItems: "flex-end"
                    }}
                  />
                </div>
                {this._renderChart(colors.main, 270, data)}
              </div>
            </Row>
            <Row lg={12}>
              <div
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30
                }}
              >
                <Row>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      marginBottom: 16
                    }}
                  >
                    <ITitle
                      level={3}
                      title="Tỷ lệ đơn trả về Admin Panel"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        flex: 1,
                        display: "flex",
                        alignItems: "flex-end"
                      }}
                    />
                  </div>
                  {this._renderChart(colors.blackChart, 270, dataOrder)}
                </Row>
              </div>
            </Row>
          </Col>
          <Col
            lg={12}
            style={{
              borderLeft: "1px solid  #DEDFE4",
              height: "100%"
            }}
          >
            <div
              style={{
                paddingLeft: 25,
                paddingRight: 25,
                paddingTop: 30,
                paddingBottom: 30
              }}
            >
              <Row span={8}>
                <Row>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      marginBottom: 16
                    }}
                  >
                    <ITitle
                      level={3}
                      title="Tỷ lệ giao hàng trễ quá 48h"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        flex: 1,
                        display: "flex",
                        alignItems: "flex-end"
                      }}
                    />
                  </div>
                  {this._renderChart(colors.red, 180, data)}
                </Row>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
