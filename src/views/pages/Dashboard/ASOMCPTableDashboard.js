import React, { Component } from "react";
import { Spin, Divider } from "antd";
import { Row, Col } from "reactstrap";
import { colors } from "../../../assets";

export default class ASOMCPTableDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      abc: [
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
        { value: 122 },
      ],
      color: "#FFF8E7",
      isSpin: false,

      width: window.innerWidth,
      height: window.innerHeight,
      yearNow: new Date(),
      YearAgo: new Date().getFullYear - 1,
    };

    this.handleResize = this.handleResize.bind(this);
  }

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }
  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }
  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
  }
  _renderRowMonth = (data, color, bold) => {
    return data.map(() => {
      return (
        <td
          style={{
            background: color,
            textAlign: "right",
            paddingRight: 16,
            color: colors.text.black,
            fontWeight: bold ? "bold" : "normal",
          }}
        >
          1212
        </td>
      );
    });
  };
  _renderRowDay = () => {
    var arr = [...new Array(31)]; // Tạo mảng có 30 phần tử
    return arr.map((item, index) => (
      <th style={{ width: 100, textAlign: "center" }}>Ngày {index + 1}</th>
    ));
  };
  _renderYear = () => {
    return (
      <table border="1" style={{ width: 1758, borderColor: colors.line }}>
        <tr>
          <th colSpan={2} style={{ height: 60, textAlign: "center" }}>
            Tiêu chí
          </th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 1</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 2</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 3</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 4</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 5</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 6</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 7</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 8</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 9</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 10</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 11</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 12</th>
          <th style={{ width: 140, textAlign: "center" }}> YTD/ Average</th>
        </tr>
        {/* Danh thu */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              height: "100%",
              verticalAlign: "top",
              paddingRight: 20,
              paddingLeft: 20,
              paddingTop: 30,
            }}
            rowSpan={20}
          >
            <div style={{ display: "flex", flexDirection: "column" }}>
              <span style={{ fontWeight: 500 }}> Vùng </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Khu Vực </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Tỉnh </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 60,
              }}
            >
              <span style={{ fontWeight: 500 }}> Kênh </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 50,
              }}
            >
              <span style={{ fontWeight: 500 }}> Ngành </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Nhóm </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Phân khúc </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Nhóm sản phẩm </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Packsize </span>
              <span>ĐB Sông cửu long</span>
            </div>
          </td>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Doanh thu 2019
          </td>
          {this._renderRowMonth(this.state.abc, this.state.color, false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Doanh thu 2020
          </td>
          {this._renderRowMonth(this.state.abc, this.state.color, false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, this.state.color, false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, this.state.color, true)}
        </tr>

        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            #Đơn hàng 2019
          </td>
          {this._renderRowMonth(this.state.abc, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            #Đơn hàng 2020
          </td>
          {this._renderRowMonth(this.state.abc, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, "#F6F8FE", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Dropsize 2019
          </td>
          {this._renderRowMonth(this.state.abc, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Dropsize 2020
          </td>
          {this._renderRowMonth(this.state.abc, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, "#E0FFED", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            ASO 2019
          </td>
          {this._renderRowMonth(this.state.abc, "#FFE5E1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            ASO 2020
          </td>
          {this._renderRowMonth(this.state.abc, "#FFE5E1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, "#FFE5E1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, "#FFE5E1", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            VPO 2019
          </td>
          {this._renderRowMonth(this.state.abc, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            VPO 2020
          </td>
          {this._renderRowMonth(this.state.abc, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, "#F1F1F1", true)}
        </tr>
      </table>
    );
  };
  _renderTitleDay = () => {
    var arr = [...new Array(31)]; // Tạo mảng có 30 phần tử
    return arr.map((item, index) => (
      <th style={{ width: 100, textAlign: "center" }}>Ngày {index + 1}</th>
    ));
  };
  _renderMonth = () => {
    return (
      <table border="1" style={{ width: 3738, borderColor: colors.line }}>
        <tr>
          <th colSpan={2} style={{ height: 60, textAlign: "center" }}>
            Tiêu chí
          </th>
          {this._renderTitleDay()}
          <th style={{ width: 140, textAlign: "center" }}> YTD/ Average</th>
        </tr>
        {/* Danh thu */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              height: "100%",
              verticalAlign: "top",
              paddingRight: 20,
              paddingLeft: 20,
              paddingTop: 30,
            }}
            rowSpan={20}
          >
            <div style={{ display: "flex", flexDirection: "column" }}>
              <span style={{ fontWeight: 500 }}> Vùng </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Khu Vực </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Tỉnh </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 60,
              }}
            >
              <span style={{ fontWeight: 500 }}> Kênh </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 50,
              }}
            >
              <span style={{ fontWeight: 500 }}> Ngành </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Nhóm </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Phân khúc </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Nhóm sản phẩm </span>
              <span>ĐB Sông cửu long</span>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
              }}
            >
              <span style={{ fontWeight: 500 }}> Packsize </span>
              <span>ĐB Sông cửu long</span>
            </div>
          </td>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Doanh thu 2019
          </td>
          {this._renderRowMonth(this.state.abc, this.state.color, false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Doanh thu 2020
          </td>
          {this._renderRowMonth(this.state.abc, this.state.color, false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, this.state.color, false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, this.state.color, true)}
        </tr>

        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            #Đơn hàng 2019
          </td>
          {this._renderRowMonth(this.state.abc, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            #Đơn hàng 2020
          </td>
          {this._renderRowMonth(this.state.abc, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, "#F6F8FE", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Dropsize 2019
          </td>
          {this._renderRowMonth(this.state.abc, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Dropsize 2020
          </td>
          {this._renderRowMonth(this.state.abc, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, "#E0FFED", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            ASO 2019
          </td>
          {this._renderRowMonth(this.state.abc, "#FFE5E1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            ASO 2020
          </td>
          {this._renderRowMonth(this.state.abc, "#FFE5E1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, "#FFE5E1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, "#FFE5E1", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            VPO 2019
          </td>
          {this._renderRowMonth(this.state.abc, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            VPO 2020
          </td>
          {this._renderRowMonth(this.state.abc, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._renderRowMonth(this.state.abc, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            2020 vs 2019 (%)
          </td>
          {this._renderRowMonth(this.state.abc, "#F1F1F1", true)}
        </tr>
      </table>
    );
  };
  render() {
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          overflowX: "auto",
        }}
      >
        <Spin spinning={this.state.isSpin}>
          <div style={{ width: "100%" }}>
            {this.props.viewYear ? this._renderYear() : this._renderMonth()}
          </div>
        </Spin>
      </div>
    );
  }
}
