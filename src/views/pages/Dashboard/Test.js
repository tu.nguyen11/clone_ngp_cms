import React, { Component } from "react";
import { G2, Chart, Geom, Axis, Tooltip, View } from "bizcharts";
export default class IChart extends Component {
  render() {
    const {
      colorChart,
      height,
      data = [],
      tooltip1,
      chart2 = false,
      colorChart2,
      data2 = [],
      tooltip2,
      scale,
      nameTooltip = "",
      position1 = "",
      position2 = "",
    } = this.props;
    return (
      <div>
        <Chart
          height={height}
          padding="auto"
          forceFit
          scale={scale}
          data={[
            {
              id: 93,
              value: 35618,
              date: 1590944400000,
              dayOfMonth: 1,
            },
            {
              id: 0,
              value: 12,
              date: 1591030800000,
              dayOfMonth: 2,
            },
            {
              id: 323,
              value: 0,
              date: 1591117200000,
              dayOfMonth: 3,
            },
            {
              id: 40,
              value: 5324,
              date: 1591203600000,
              dayOfMonth: 4,
            },
            {
              id: 0,
              value: 4234,
              date: 1591290000000,
              dayOfMonth: 5,
            },
            {
              id: 0,
              value: 13423,
              date: 1591376400000,
              dayOfMonth: 6,
            },
            {
              id: 0,
              value: 234230,
              date: 1591462800000,
              dayOfMonth: 7,
            },
            {
              id: 0,
              value: 234,
              date: 1591549200000,
              dayOfMonth: 8,
            },
            {
              id: 0,
              value: 234230,
              date: 1591635600000,
              dayOfMonth: 9,
            },
            {
              id: 0,
              value: 2340,
              date: 1591722000000,
              dayOfMonth: 10,
            },
            {
              id: 0,
              value: 340,
              date: 1591808400000,
              dayOfMonth: 11,
            },
            {
              id: 0,
              value: 3240,
              date: 1591894800000,
              dayOfMonth: 12,
            },
          ]}
        >
          <Tooltip
            containerTpl='<div class="g2-tooltip"><p class="g2-tooltip-title"></p><table class="g2-tooltip-list"></table></div>'
            itemTpl='<tr class="g2-tooltip-list-item"><td>{name}: </td><td style="color:#22232B;font-weight:800">{value}</td></tr>'
            offset={50}
            g2-tooltip={{
              position: "absolute",
              visibility: "hidden",
              border: "1px solid #efefef",
              backgroundColor: "white",
              color: "#000",
              opacity: "0.8",
              padding: "5px 15px",
              transition: "top 200ms,left 200ms",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            g2-tooltip-list={{
              margin: "10px",
            }}
          />
          <Axis name="value" />
          <Axis name="date" />
          <Geom
            type="line"
            position={position1}
            color={chart2 ? colorChart2 : colorChart}
            size={2}
            tooltip={chart2 ? null : tooltip1}
          />
          {chart2 ? (
            <Geom
              type="line"
              position={position2}
              color={colorChart2}
              tooltip={tooltip2}
              size={2}
            />
          ) : null}
        </Chart>
      </div>
    );
  }
}
