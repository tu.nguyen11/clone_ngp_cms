import React, { Component } from "react"; 
import { Row, Col, Button, List, message, Skeleton, Spin} from "antd";
import { ITitle, IChart } from "../../components";
import { colors } from "../../../assets";

import {
  dayInMonth,
  priceFormat,
  priceShortFormat,
} from "../../../utils";
import TweenOne from "rc-tween-one";
import Children from "rc-tween-one/lib/plugin/ChildrenPlugin";
import { APIService } from "../../../services";
import InfiniteScroll from "react-infinite-scroller";
// const ButtonGroup = Button.Group;

TweenOne.plugins.push(Children);

export default class BusinessDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSales: [],
      dataASO: [],
      dataOrder: [],
      dataDropsize: [],
      dataVPO: [],
      dataList: [],
      value: 1,
      url: "",
      year: 0,
      city_id: 0,
      month: 0,
      id_region: 0,
      id_area: 0,
      id_chanel: 0,
      id_product_type: 0,
      id_product_group: 0,
      viewYear: false,
      reloadData: false,
      ////////////
      sort: 1,
      valueMoney: 0,
      isLoading: false,
      isLoadingList: true,
      isSpin: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.warn("hook", nextProps, prevState);
    if (
      nextProps.year !== prevState.year ||
      nextProps.month !== prevState.month ||
      nextProps.city_id !== prevState.city_id ||
      nextProps.dataSales !== prevState.dataSales ||
      nextProps.dataVPO !== prevState.dataVPO ||
      nextProps.dataOrder !== prevState.dataOrder ||
      nextProps.dataASO !== prevState.dataASO ||
      nextProps.dataDropsize !== prevState.dataDropsize ||
      nextProps.reloadData !== prevState.reloadData ||
      nextProps.showYear !== prevState.viewYear ||
      nextProps.id_region !== prevState.id_region ||
      nextProps.id_area !== prevState.id_area ||
      nextProps.id_chanel !== prevState.id_chanel ||
      nextProps.id_product_type !== prevState.id_product_type ||
      nextProps.id_product_group !== prevState.id_product_group
    ) {
      return {
        year: nextProps.year,
        month: nextProps.month,
        city_id: nextProps.city_id,
        dataSales: nextProps.dataSales,
        dataVPO: nextProps.dataVPO,
        dataOrder: nextProps.dataOrder,
        dataASO: nextProps.dataASO,
        dataDropsize: nextProps.dataDropsize,
        reloadData: nextProps.reloadData,
        viewYear: nextProps.showYear,
        id_region: nextProps.id_region,
        id_area: nextProps.id_area,
        id_chanel: nextProps.id_chanel,
        id_product_type: nextProps.id_product_type,
        id_product_group: nextProps.id_product_group,
      };
    } else return null;
  }
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.reloadData !== this.state.reloadData) {
      const data = await Promise.all([
        this._APIgetTopProduct(
          this.state.month,
          this.state.year,
          this.state.city_id,
          this.state.sort,
          this.state.id_region,
          this.state.id_area,
          this.state.id_chanel,
          this.state.id_product_type,
          this.state.id_product_group
        ),
      ]);

      await this.setState({
        isLoading: false,
      });
    }
  }

  _APIgetTopProduct = async (
    month,
    year,
    city_id,
    type,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      const data = await APIService._getTopProductSell(
        month,
        year,
        city_id,
        type,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      this.props.callback(false);
      this.setState({
        dataList: data.topSell,
        isLoadingList: false,
      });
    } catch (err) {
      message.error(err);
    }
  };

  async componentDidMount() {
    const data = await Promise.all([
      this._APIgetTopProduct(
        this.state.month,
        this.state.year,
        this.state.city_id,
        this.state.sort,
        this.state.id_region,
        this.state.id_area,
        this.state.id_chanel,
        this.state.id_product_type,
        this.state.id_product_group
      ),
    ]);

    await this.setState({
      isLoading: false,
    });
  }

  render() {
    const {
      dataSales,
      dataASO,
      dataDropsize,
      dataVPO,
      dataOrder,
      dataList,
      isLoadingList,
    } = this.state;
    let scaleDoanhSo = {
      date: {
        type: "time",
        mask: "D",
        tickCount: dayInMonth(this.state.month, this.state.year),
        sync: true,
      },

      value: {
        alias: "Doanh số",
        formatter: function (val) {
          return val;
        },
      },
    };
    const TooltipRevenue = [
      "date*value*dayOfMonth*year",
      (date, value, month, year) => {
        if (this.state.viewYear) {
          if (value === null) {
            return {
              name: "Doanh số" + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Doanh số " + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)) + "M",
          };
        } else {
          if (value === null) {
            return {
              name: "Doanh số " + date + "/" + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Doanh số " + date + "/" + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)) + "M",
          };
        }
      },
    ];
    const TooltipASO = [
      "date*value*dayOfMonth*year",
      (date, value, month, year) => {
        if (this.state.viewYear) {
          if (value === null) {
            return {
              name: "Số lượng đơn hàng " + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Số lượng đơn hàng " + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)) + "M",
          };
        } else {
          if (value === null) {
            return {
              name: "Số lượng đơn hàng " + date + "/" + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Số lượng đơn hàng " + date + "/" + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)) + "M",
          };
        }
      },
    ];
    const TooltipOrder = [
      "date*value*dayOfMonth*year",
      (date, value, month, year) => {
        if (this.state.viewYear) {
          if (value === null) {
            return {
              name: "Số đại lý đặt hàng " + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Số đại lý đặt hàng " + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)),
          };
        } else {
          if (value === null) {
            return {
              name: "Số đại lý đặt hàng " + date + "/" + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Số đại lý đặt hàng " + date + "/" + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)),
          };
        }
      },
    ];
    const TooltipVPO = [
      "date*value*dayOfMonth*year",
      (date, value, month, year) => {
        if (this.state.viewYear) {
          if (value === null) {
            return {
              name: "Số đơn hàng/cửa hàng " + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Số đơn hàng/cửa hàng " + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)),
          };
        } else {
          if (value === null) {
            return {
              name: "Số đơn hàng/cửa hàng " + date + "/" + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Số đơn hàng/cửa hàng " + date + "/" + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)),
          };
        }
      },
    ];
    const TooltipDropsize = [
      "date*value*dayOfMonth*year",
      (date, value, month, year) => {
        if (this.state.viewYear) {
          if (value === null) {
            return {
              name: "Doanh số TB " + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Doanh số TB " + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)) + "M",
          };
        } else {
          if (value === null) {
            return {
              name: "Doanh số TB " + date + "/" + month + "/" + year,
              value: "Chưa có dữ liệu",
            };
          }
          return {
            name: "Doanh số TB " + date + "/" + month + "/" + year,
            value: priceFormat(Number.parseFloat(value).toFixed(0)) + "M",
          };
        }
      },
    ];

    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          border: "1px solid  #DEDFE4",
          height: "100%",
        }}
      >
        <Spin spinning={this.state.isSpin}>
          <Row>
            <Col
              lg={12}
              style={{
                height: "100%",
              }}
            >
              <Row
                lg={12}
                style={{
                  borderBottom: "1px solid #DEDFE4",
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                }}
              >
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 3 }}
                >
                  <div style={{}}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16,
                      }}
                    >
                      <ITitle
                        level={3}
                        title="Doanh thu"
                        style={{
                          color: colors.black,
                          fontWeight: "bold",
                          flex: 1,
                          display: "flex",
                          alignItems: "flex-end",
                        }}
                      />
                      <div
                        // className="cursor"
                        // onClick={() => this.props.callback(2)}
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            marginRight: 50,
                          }}
                        >
                          <div
                            style={{
                              width: 10,
                              height: 10,
                              background: "black",
                              marginRight: 10,
                            }}
                          ></div>
                          <span style={{ fontSize: 12 }}>
                            {this.state.viewYear
                              ? this.state.year - 1
                              : this.state.month - 1}
                          </span>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              width: 10,
                              height: 10,
                              background: colors.main,
                              marginRight: 10,
                            }}
                          ></div>
                          <span style={{ fontSize: 12 }}>
                            {this.state.viewYear
                              ? this.state.year
                              : this.state.month}
                          </span>
                        </div>
                      </div>
                    </div>

                    <IChart
                      height={270}
                      data={dataSales}
                      // scale={scaleDoanhSo}
                      position1={
                        this.state.viewYear ? "dayOfMonth*value" : "date*value"
                      }
                      tooltip={TooltipRevenue}
                      year={this.state.viewYear}
                      AxisFormat={{
                        formatter: (value) => `${priceFormat(value)} M`,
                      }}
                    />
                  </div>
                </Skeleton>
              </Row>
              <Row lg={12}>
                <div
                  style={{
                    paddingLeft: 25,
                    paddingRight: 25,
                    paddingTop: 30,
                    paddingBottom: 30,
                  }}
                >
                  <Skeleton
                    loading={this.state.isLoading}
                    active
                    paragraph={{ rows: 3 }}
                  >
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="ASO"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          // className="cursor"
                          // onClick={() => this.props.callback(2)}
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 50,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "black",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>
                              {this.state.viewYear
                                ? this.state.year - 1
                                : this.state.month - 1}
                            </span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.main,
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>
                              {this.state.viewYear
                                ? this.state.year
                                : this.state.month}
                            </span>
                          </div>
                        </div>
                      </div>

                      <IChart
                        height={270}
                        data={dataASO}
                        // scale={scaleDoanhSo}
                        position1={
                          this.state.viewYear
                            ? "dayOfMonth*value"
                            : "date*value"
                        }
                        tooltip={TooltipASO}
                        year={this.state.viewYear}
                        AxisFormat={{
                          formatter: (value) => `${priceFormat(value) + "M"}`,
                        }}
                      />
                    </Row>
                  </Skeleton>
                </div>
              </Row>
            </Col>
            <Col
              lg={7}
              style={{
                borderLeft: "1px solid  #DEDFE4",
                borderRight: "1px solid  #DEDFE4",
                height: "100%",
              }}
            >
              <div
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30,
                }}
              >
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 12 }}
                >
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Đơn hàng"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          // className="cursor"
                          // onClick={() => this.props.callback(2)}
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 50,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "black",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>
                              {this.state.viewYear
                                ? this.state.year - 1
                                : this.state.month - 1}
                            </span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.main,
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>
                              {this.state.viewYear
                                ? this.state.year
                                : this.state.month}
                            </span>
                          </div>
                        </div>
                      </div>
                      <IChart
                        height={180}
                        data={dataOrder}
                        scale={scaleDoanhSo}
                        position1={
                          this.state.viewYear
                            ? "dayOfMonth*value"
                            : "date*value"
                        }
                        tooltip={TooltipOrder}
                        year={this.state.viewYear}
                        AxisFormat={{
                          formatter: (value) => `${priceFormat(value)} `,
                        }}
                      />
                    </Row>
                  </Row>
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Dropsize"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          // className="cursor"
                          // onClick={() => this.props.callback(2)}
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 50,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "black",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>
                              {this.state.viewYear
                                ? this.state.year - 1
                                : this.state.month - 1}
                            </span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.main,
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>
                              {this.state.viewYear
                                ? this.state.year
                                : this.state.month}
                            </span>
                          </div>
                        </div>
                      </div>
                      <IChart
                        height={180}
                        data={dataDropsize}
                        scale={scaleDoanhSo}
                        position1={
                          this.state.viewYear
                            ? "dayOfMonth*value"
                            : "date*value"
                        }
                        tooltip={TooltipDropsize}
                        year={this.state.viewYear}
                        AxisFormat={{
                          formatter: (value) => `${priceShortFormat(value)} M`,
                        }}
                      />
                    </Row>
                  </Row>
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="VPO"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          // className="cursor"
                          // onClick={() => this.props.callback(2)}
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 50,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "black",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>
                              {this.state.viewYear
                                ? this.state.year - 1
                                : this.state.month - 1}
                            </span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.main,
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>
                              {this.state.viewYear
                                ? this.state.year
                                : this.state.month}
                            </span>
                          </div>
                        </div>
                      </div>

                      <IChart
                        colorChart={colors.blackChart}
                        height={180}
                        data={dataVPO}
                        scale={scaleDoanhSo}
                        position1={
                          this.state.viewYear
                            ? "dayOfMonth*value"
                            : "date*value"
                        }
                        tooltip={TooltipVPO}
                        year={this.state.viewYear}
                        AxisFormat={{
                          formatter: (value) => `${priceFormat(value)} `,
                        }}
                      />
                    </Row>
                  </Row>
                </Skeleton>
              </div>
            </Col>
            <Col
              lg={5}
              style={{ paddingLeft: 25, paddingRight: 25, paddingTop: 30 }}
            >
              <Skeleton
                loading={this.state.isLoading}
                active
                paragraph={{ rows: 12 }}
              >
                <Row style={{ height: "100%" }}>
                  <ITitle
                    level={3}
                    title="Sản phẩm bán chạy"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      display: "flex",
                      alignItems: "flex-end",
                    }}
                  />
                </Row>
                <Row style={{ paddingTop: 14 }}>
                  <Col span={8}>
                    {" "}
                    <Button
                      style={{
                        minWidth: 0,
                        width: "100%",
                        height: 42,
                        border: "1px solid #DEDFE4",
                        background:
                          this.state.sort == 1 ? colors.gray._400 : "white",
                        borderRadius: 0,
                      }}
                      onClick={() => {
                        this.setState(
                          {
                            sort: 1,
                            isLoadingList: true,
                          },
                          () =>
                            this._APIgetTopProduct(
                              this.state.month,
                              this.state.year,
                              this.state.city_id,
                              this.state.sort,
                              this.state.id_region,
                              this.state.id_area,
                              this.state.id_chanel,
                              this.state.id_product_type,
                              this.state.id_product_group
                            )
                        );
                      }}
                    >
                      SKU
                    </Button>
                  </Col>
                  <Col span={8}>
                    {" "}
                    <Button
                      style={{
                        width: "100%",
                        border: "1px solid #DEDFE4",
                        height: 42,
                        background:
                          this.state.sort == 2 ? colors.gray._400 : "white",
                        borderRadius: 0,
                      }}
                      onClick={() => {
                        this.setState(
                          {
                            sort: 2,
                            isLoadingList: true,
                          },
                          () =>
                            this._APIgetTopProduct(
                              this.state.month,
                              this.state.year,
                              this.state.city_id,
                              this.state.sort,
                              this.state.id_region,
                              this.state.id_area,
                              this.state.id_chanel,
                              this.state.id_product_type,
                              this.state.id_product_group
                            )
                        );
                      }}
                    >
                      Nhóm
                    </Button>
                  </Col>
                  <Col span={8}>
                    <Button
                      style={{
                        width: "100%",
                        height: 42,
                        border: "1px solid #DEDFE4",
                        background:
                          this.state.sort == 3 ? colors.gray._400 : "white",
                        borderRadius: 0,
                      }}
                      onClick={() => {
                        this.setState(
                          {
                            sort: 3,
                            isLoadingList: true,
                          },
                          () =>
                            this._APIgetTopProduct(
                              this.state.month,
                              this.state.year,
                              this.state.city_id,
                              this.state.sort,
                              this.state.id_region,
                              this.state.id_area,
                              this.state.id_chanel,
                              this.state.id_product_type,
                              this.state.id_product_group
                            )
                        );
                      }}
                    >
                      Nhãn
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <div
                    style={{
                      marginTop: 14,
                      height: "100%",
                    }}
                  >
                    <div
                      style={{
                        height: 600,
                        overflowY: "auto",
                        width: "100%",
                      }}
                    >
                      <InfiniteScroll
                        initialLoad={false}
                        // pageStart={0}
                        // loadMore={handleInfiniteOnLoad}
                        // hasMore={!isLoading && hasMoreList}
                        useWindow={false}
                      >
                        <List
                          loading={isLoadingList}
                          style={{
                            paddingTop: 14,
                            height: "60vh",
                            overflow: "auto",
                            marginBottom: 14,
                          }}
                          dataSource={dataList}
                          bordered={false}
                          // loading={this.state.loadingList}
                          renderItem={(item) => (
                            <Row
                              className="pl-0 ml-0"
                              style={{
                                marginBottom: 20,
                                display: "flex",
                                flexDirection: "row",
                              }}
                            >
                              <img
                                src={item.image}
                                style={{
                                  width: 78,
                                  height: 78,
                                  marginRight: 12,
                                  border: "1px solid #D8DBDC",
                                }}
                              />
                              <Row className="ml-0 pl-0">
                                <ITitle
                                  title={item.name}
                                  level="4"
                                  style={{ fontWeight: 700 }}
                                />
                                <ITitle
                                  title={"Số lượng : " + item.quantity}
                                  level="4"
                                />
                                <ITitle
                                  title={"Doanh số : " + item.total_price + "M"}
                                  level="4"
                                />
                              </Row>
                            </Row>
                          )}
                        />
                      </InfiniteScroll>
                    </div>
                  </div>
                </Row>

                {/* <Row style={{ paddingTop: 58 }}>
                  <ITitle
                    level={3}
                    title="Giá trị tồn kho hôm nay"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      display: "flex",
                      alignItems: "flex-end",
                    }}
                  />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <TweenOne
                      animation={{
                        Children: {
                          // value: this.state.valueMoney.value,
                          value: 122321321,
                          formatMoney: { thousand: ".", decimal: "," },
                        },
                        duration: 500,
                      }}
                      style={{ fontSize: 30 }}
                    >
                      0
                    </TweenOne>
                    <span style={{ fontSize: 30 }}>đ</span>
                  </div>
                </Row>
             */}
              </Skeleton>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }
}
