import React, { Component } from "react";
import { Container } from "reactstrap";
import { ITitle, IButton, ISvg, IDatePicker, ISelect } from "../../components";
import { colors } from "../../../assets";
import {
  Tabs,
  Row,
  DatePicker,
  message,
  Modal,
  Col,
  Checkbox,
  Skeleton,
} from "antd";
import { StickyContainer, Sticky } from "react-sticky";
import BusinessDashboard from "./BusinessDashboard";
import ApplicationDashboard from "./ApplicationDashboard";
import OperateDashboard from "./OperateDashboard";
import SalesmanDashboard from "./SalesmanDashboard";
import DetailSalesDashboard from "./DetailSalesDashboard";
import DetailOrderDashboard from "./DetailOrderDashboard";
import moment from "moment";
import "moment/locale/vi";
import { APIService } from "../../../services";
import ASOMCPDashboard from "./ASOMCPDashboard";
import BusinessTableDashboard from "./BusinessTableDashboard";
import ASOMCPTableDashboard from "./ASOMCPTableDashboard";
import { priceChartFormat } from "../../../utils";
const { MonthPicker, RangePicker } = DatePicker;

const { TabPane } = Tabs;
const renderTabBar = (props, DefaultTabBar) => (
  <Sticky bottomOffset={80}>
    {({ style }) => (
      <DefaultTabBar
        {...props}
        style={{ ...style, zIndex: 1, background: "#fff" }}
      />
    )}
  </Sticky>
);

const TimePresent = new Date();
const monthPresent = TimePresent.getMonth() + 1;
const yearPresent = TimePresent.getFullYear();
export default class HomeDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyTab: 1,
      id_cityDashboard: 0,
      monthDashboard: monthPresent,
      yearDashboard: yearPresent,
      dataRegion: [],
      dataArea: [],
      dataCity: [],
      dataChanel: [],
      dataProductType: [],
      dataProductGroup: [],
      id_region: 0,
      id_area: 0,
      id_city: 0,
      id_chanel: 0,
      id_product_type: 0,
      id_product_group: 0,
      infoFilter: {
        name_region: "Tất cả",
        name_area: "Tất cả",
        name_city: "Tất cả",
        name_chanel: "Tất cả",
        name_product_type: "Tất cả",
        name_product_group: "Tất cả",
      },
      infoFilterTable: {
        name_region: "Tất cả",
        name_area: "Tất cả",
        name_city: "Tất cả",
        name_chanel: "Tất cả",
        name_product_type: "Tất cả",
        name_product_group: "Tất cả",
      },
      isLoading: true,
      viewTable: false,
      viewYear: true,
      showYear: true,
      visible: false,
      reloadData: false,
      dataSales: [],
      dataASO: [],
      dataOrder: [],
      dataDropsize: [],
      dataVPO: [],
      dataList: [],
      dataTableSales: {
        dataAgo: [],
        dataNew: [],
      },
      dataTableASO: {
        dataAgo: [],
        dataNew: [],
      },
      dataTableOrder: {
        dataAgo: [],
        dataNew: [],
      },
      dataTableDropsize: {
        dataAgo: [],
        dataNew: [],
      },
      dataTableVPO: {
        dataAgo: [],
        dataNew: [],
      },
    };
  }

  async componentDidMount() {
    this._getAPI();

    setInterval(() => {
      this._getAPI();
    }, 300000);
  }
  _getAPIDropList = async () => {
    try {
      const { id_area, id_region, id_product_type } = this.state;

      await this._APIDropListRegion();
      await this._APIDropListArea(id_region);
      await this._APIDropListCity(id_area);
      await this._APIDropListChanel();
      await this._APIDropListProductType();
      await this._APIDropListProductGroup(id_product_type);
    } catch (error) {}
  };
  _getAPIDataChart = async () => {
    try {
      await this._APIgetRevenueDashboard(
        this.state.monthDashboard,
        this.state.yearDashboard,
        this.state.id_city,
        this.state.id_region,
        this.state.id_area,
        this.state.id_chanel,
        this.state.id_product_type,
        this.state.id_product_group
      );
      await this._APIgetASODashboard(
        this.state.monthDashboard,
        this.state.yearDashboard,
        this.state.id_city,
        this.state.id_region,
        this.state.id_area,
        this.state.id_chanel,
        this.state.id_product_type,
        this.state.id_product_group
      );
      await this._APIgetOrderDashboard(
        this.state.monthDashboard,
        this.state.yearDashboard,
        this.state.id_city,
        this.state.id_region,
        this.state.id_area,
        this.state.id_chanel,
        this.state.id_product_type,
        this.state.id_product_group
      );
      await this._APIgetVPODashboard(
        this.state.monthDashboard,
        this.state.yearDashboard,
        this.state.id_city,
        this.state.id_region,
        this.state.id_area,
        this.state.id_chanel,
        this.state.id_product_type,
        this.state.id_product_group
      );
      await this._APIgetDropsizeDashboard(
        this.state.monthDashboard,
        this.state.yearDashboard,
        this.state.id_city,
        this.state.id_region,
        this.state.id_area,
        this.state.id_chanel,
        this.state.id_product_type,
        this.state.id_product_group
      );
    } catch (error) {}
  };
  _getAPI = async () => {
    await this._getAPIDataChart();
  };
  _APIgetRevenueDashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      const data = await APIService._getRevenueDashboard(
        this.state.viewYear ? 0 : month - 1,
        this.state.viewYear ? year - 1 : year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      const data2 = await APIService._getRevenueDashboard(
        this.state.viewYear ? 0 : month,
        year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      data.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.valueTable = item.value;
        item.value = item.value / 1000000;

        console.log("value table" + item.valueTable);
        console.log("value" + item.value);
      });

      data2.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.valueTable = item.value;
        item.value = item.value / 1000000;
        console.log("value table" + item.valueTable);
        console.log("value" + item.value);
      });
      // this.props.callback(false);
      let dataNew = data.chart.concat(data2.chart);

      const obj = {
        valueTable: data.average_value,
      };
      const obj2 = {
        valueTable: data2.average_value,
      };

      this.state.dataTableSales.dataAgo = [...data.chart];
      this.state.dataTableSales.dataNew = data2.chart;
      this.state.dataTableSales.dataAgo.push(obj);
      this.state.dataTableSales.dataNew.push(obj2);
      this.setState({
        dataSales: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIgetASODashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      const data = await APIService._getASODashboard(
        this.state.viewYear ? 0 : month - 1,
        this.state.viewYear ? year - 1 : year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      const data2 = await APIService._getASODashboard(
        this.state.viewYear ? 0 : month,
        year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      data.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.valueTable = item.value;
      });
      data2.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.value = item.value / 1000000;
        item.valueTable = item.value;
      });

      let dataNew = data.chart.concat(data2.chart);
      const obj = {
        valueTable: data.average_value,
      };
      const obj2 = {
        valueTable: data2.average_value,
      };

      this.state.dataTableASO.dataAgo = [...data.chart];
      this.state.dataTableASO.dataNew = data2.chart;
      this.state.dataTableASO.dataAgo.push(obj);
      this.state.dataTableASO.dataNew.push(obj2);
      this.setState({
        dataASO: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIgetOrderDashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      const data = await APIService._getOrderDashboard(
        this.state.viewYear ? 0 : month - 1,
        this.state.viewYear ? year - 1 : year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      const data2 = await APIService._getOrderDashboard(
        this.state.viewYear ? 0 : month,
        year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      data.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.valueTable = item.value;
      });
      data2.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.valueTable = item.value;
      });

      let dataNew = data.chart.concat(data2.chart);

      const obj = {
        valueTable: data.average_value,
      };
      const obj2 = {
        valueTable: data2.average_value,
      };

      this.state.dataTableOrder.dataAgo = [...data.chart];
      this.state.dataTableOrder.dataNew = data2.chart;
      this.state.dataTableOrder.dataAgo.push(obj);
      this.state.dataTableOrder.dataNew.push(obj2);
      this.setState({
        dataOrder: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIgetDropsizeDashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      const data = await APIService._getDropsizeDashboard(
        this.state.viewYear ? 0 : month - 1,
        this.state.viewYear ? year - 1 : year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      const data2 = await APIService._getDropsizeDashboard(
        this.state.viewYear ? 0 : month,
        year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      data.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.valueTable = item.value;

        item.value = item.value / 1000000;
      });
      data2.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.value = item.value / 1000000;
        item.valueTable = item.value;
      });

      let dataNew = data.chart.concat(data2.chart);
      const obj = {
        valueTable: data.average_value,
      };
      const obj2 = {
        valueTable: data2.average_value,
      };

      this.state.dataTableDropsize.dataAgo = [...data.chart];
      this.state.dataTableDropsize.dataNew = data2.chart;
      this.state.dataTableDropsize.dataAgo.push(obj);
      this.state.dataTableDropsize.dataNew.push(obj2);
      this.setState({
        dataDropsize: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIgetVPODashboard = async (
    month,
    year,
    city_id,
    id_region,
    id_area,
    id_chanel,
    id_product_type,
    id_product_group
  ) => {
    try {
      const data = await APIService._getVPODashboard(
        this.state.viewYear ? 0 : month - 1,
        this.state.viewYear ? year - 1 : year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      const data2 = await APIService._getVPODashboard(
        this.state.viewYear ? 0 : month,
        year,
        city_id,
        id_region,
        id_area,
        id_chanel,
        id_product_type,
        id_product_group
      );
      data.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.valueTable = item.value;
      });
      data2.chart.map((item) => {
        item.year = item.year.toString();
        item.dayOfMonth = item.dayOfMonth.toString();
        item.valueTable = item.value;
      });

      let dataNew = data.chart.concat(data2.chart);
      const obj = {
        valueTable: data.average_value,
      };
      const obj2 = {
        valueTable: data2.average_value,
      };

      this.state.dataTableVPO.dataAgo = [...data.chart];
      this.state.dataTableVPO.dataNew = data2.chart;
      this.state.dataTableVPO.dataAgo.push(obj);
      this.state.dataTableVPO.dataNew.push(obj2);
      this.setState({
        dataVPO: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };

  _APIDropListRegion = async () => {
    try {
      const data = await APIService._getDropListRegion();
      let dataNews = data.droplist.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNews.unshift({
        key: 0,
        value: "Tất cả",
      });
      this.setState({
        dataRegion: dataNews,
        isLoading: false,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIDropListArea = async (id_region) => {
    try {
      const data = await APIService._getDropListArea(id_region);
      let dataNews = data.droplist.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNews.unshift({
        key: 0,
        value: "Tất cả",
      });
      this.setState({
        dataArea: dataNews,
        isLoading: false,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIDropListCity = async (id_area) => {
    try {
      const data = await APIService._getDropListCity(id_area);
      let dataNews = data.droplist.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNews.unshift({
        key: 0,
        value: "Tất cả",
      });
      this.setState({
        dataCity: dataNews,
        isLoading: false,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIDropListChanel = async () => {
    try {
      const data = await APIService._getDropListChanel();
      let dataNews = data.droplist.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNews.unshift({
        key: 0,
        value: "Tất cả",
      });
      this.setState({
        dataChanel: dataNews,
        isLoading: false,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIDropListProductType = async () => {
    try {
      const data = await APIService._getDropListProductType("");
      let dataNews = data.droplist.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNews.unshift({
        key: 0,
        value: "Tất cả",
      });
      this.setState({
        dataProductType: dataNews,
        isLoading: false,
      });
    } catch (err) {
      message.error(err);
    }
  };
  _APIDropListProductGroup = async (id_product_type) => {
    try {
      const data = await APIService._getDropListProductGroup(id_product_type);
      let dataNews = data.droplist.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNews.unshift({
        key: 0,
        value: "Tất cả",
      });
      this.setState({
        dataProductGroup: dataNews,
        isLoading: false,
      });
    } catch (err) {
      message.error(err);
    }
  };

  render() {
    const {
      dataChanel,
      dataRegion,
      dataCity,
      dataProductGroup,
      dataProductType,
      dataArea,
      id_area,
      id_chanel,
      id_city,
      id_product_group,
      id_product_type,
      id_region,
      infoFilter,
    } = this.state;
    return (
      <Container fluid>
        <Row
          className="p-0"
          style={{
            marginBottom: 24,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <ITitle
            level={1}
            title="Dashboard"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <div style={{ width: 170 }}>
            <IButton
              icon={ISvg.NAME.DOWLOAND}
              title="Xuất file"
              color={colors.main}
              onClick={() => {
                message.warning("Chức năng đang cập nhật");
              }}
            />
          </div>
        </Row>
        <Row style={{}}>
          <StickyContainer
            style={{ width: "100%", background: colors.background }}
          >
            <Tabs
              defaultActiveKey={this.state.keyTab}
              renderTabBar={renderTabBar}
              style={{ width: "100%", background: colors.background }}
              onTabClick={(value) => {
                this.setState({
                  keyTab: value,
                });
              }}
            >
              <TabPane
                tab={
                  <span
                    style={{
                      color: "rgba(34, 35, 43, 0.5)",
                      fontWeight: "600",
                      fontSize: 14,
                    }}
                  >
                    Kinh doanh
                  </span>
                }
                key="1"
              >
                <div>
                  {!this.state.viewTable ? (
                    <BusinessDashboard
                      year={this.state.yearDashboard}
                      month={this.state.monthDashboard}
                      city_id={this.state.id_city}
                      id_region={this.state.id_region}
                      id_area={this.state.id_area}
                      id_chanel={this.state.id_chanel}
                      id_product_type={this.state.id_product_type}
                      id_product_group={this.state.id_product_group}
                      dataSales={this.state.dataSales}
                      dataVPO={this.state.dataVPO}
                      dataOrder={this.state.dataOrder}
                      dataASO={this.state.dataASO}
                      dataDropsize={this.state.dataDropsize}
                      showYear={this.state.showYear}
                      reloadData={this.state.reloadData}
                      callback={(key) => {
                        this.setState({
                          reloadData: key,
                        });
                      }}
                    />
                  ) : (
                    <BusinessTableDashboard
                      year={this.state.yearDashboard}
                      month={this.state.monthDashboard}
                      city_id={this.state.id_city}
                      dataTableSales={this.state.dataTableSales}
                      dataTableVPO={this.state.dataTableVPO}
                      dataTableOrder={this.state.dataTableOrder}
                      dataTableASO={this.state.dataTableASO}
                      dataTableDropsize={this.state.dataTableDropsize}
                      showYear={this.state.showYear}
                      reloadData={this.state.reloadData}
                      infoFilter={
                        !this.state.reloadData
                          ? this.state.infoFilterTable
                          : this.state.infoFilter
                      }
                      callback={(key) => {
                        this.setState({
                          reloadData: key,
                        });
                      }}
                    />
                  )}
                </div>
              </TabPane>
              {/* <TabPane
                tab={
                  <span
                    style={{
                      color: "rgba(34, 35, 43, 0.5)",
                      fontWeight: "600",
                      fontSize: 14,
                    }}
                  >
                    ASO-MCP
                  </span>
                }
                key="2"
              >
                {this.state.viewTable ? (
                  <ASOMCPDashboard
                    year={this.state.yearDashboard}
                    month={this.state.monthDashboard}
                    city_id={this.state.id_cityDashboard}
                  />
                ) : (
                  <ASOMCPTableDashboard viewYear={this.state.viewYear} />
                )}
              </TabPane>
          */}
            </Tabs>
          </StickyContainer>
          <div
            style={{
              position: "absolute",
              top: 0,
              right: 0,
              display: "flex",
              flexDirection: "row",
            }}
          >
            <div style={{ width: 170, marginRight: 24 }}>
              {/* <ISelect
                select={true}
                style={{ height: 40 }}
                placeholder="Khu vực"
                data={this.state.datacityList}
                value={this.state.id_cityDashboard}
                onChange={key => {
                  this.setState({
                    id_cityDashboard: key
                  });
                }}
              /> */}
              <IButton
                icon={ISvg.NAME.EXPERIMENT}
                title="Lọc hiển thị"
                color={colors.main}
                onClick={() => {
                  this.setState(
                    {
                      visible: true,
                    },
                    () => this._getAPIDropList()
                  );
                }}
              />
            </div>
            <div style={{ width: 170 }}>
              <IButton
                icon={
                  !this.state.viewTable ? ISvg.NAME.Article : ISvg.NAME.Chart
                }
                title={
                  !this.state.viewTable ? "Xem dạng bảng" : "Xem dạng chart"
                }
                color={colors.main}
                onClick={() => {
                  this.setState({
                    viewTable: !this.state.viewTable,
                  });
                }}
              />
            </div>
          </div>
          {this.state.keyTab == 1 ? (
            <Modal
              visible={this.state.visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
              footer={null}
              closable={false}
              maskClosable={false}
              closeIcon={<div></div>}
              width={800}
            >
              <Row>
                <Col span={11}>
                  <Row>
                    <p style={{ color: colors.text.black, fontWeight: "bold" }}>
                      Lọc hiển thị
                    </p>
                  </Row>
                  <Row
                    style={{
                      paddingTop: 28,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Col span={8}>
                      <span>Vùng</span>
                    </Col>
                    <Col span={16}>
                      <ISelect
                        bordered={false}
                        value={id_region}
                        select={true}
                        colorPlaceholder="#bfbfbf"
                        isBorderBottom="1px solid #D8DBDC"
                        placeholder="chọn trạng thái"
                        onChange={(key, item) => {
                          infoFilter.name_region = item.props.children;
                          this.setState(
                            {
                              id_region: key,
                              id_area: 0,
                              id_city: 0,
                              isLoading: true,
                              dataArea: [],
                              dataCity: [],
                            },
                            () => this._APIDropListArea(key)
                          );
                        }}
                        data={dataRegion}
                      />
                    </Col>
                  </Row>
                  <Row
                    style={{
                      paddingTop: 28,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Col span={8}>
                      <span>Khu vực</span>
                    </Col>
                    <Col span={16}>
                      <ISelect
                        bordered={false}
                        // disable={id_area === 0 ? true : false}
                        value={id_area}
                        select={true}
                        colorPlaceholder="#bfbfbf"
                        isBorderBottom="1px solid #D8DBDC"
                        placeholder="chọn khu vực"
                        onChange={(key, item) => {
                          infoFilter.name_area = item.props.children;
                          this.setState(
                            {
                              id_area: key,
                              id_city: 0,
                              isLoading: true,
                              dataCity: [],
                            },
                            () => {
                              this._APIDropListCity(key);
                            }
                          );
                        }}
                        data={dataArea}
                      />
                    </Col>
                  </Row>
                  <Row
                    style={{
                      paddingTop: 28,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Col span={8}>
                      <span>Tỉnh</span>
                    </Col>
                    <Col span={16}>
                      <ISelect
                        bordered={false}
                        value={id_city}
                        select={true}
                        colorPlaceholder="#bfbfbf"
                        isBorderBottom="1px solid #D8DBDC"
                        placeholder="chọn tỉnh"
                        onChange={(key, item) => {
                          infoFilter.name_city = item.props.children;
                          this.setState({
                            id_city: key,
                          });
                        }}
                        data={dataCity}
                      />
                    </Col>
                  </Row>
                  <Row
                    style={{
                      paddingTop: 28,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Col span={8}>
                      <span>Kênh</span>
                    </Col>
                    <Col span={16}>
                      <ISelect
                        bordered={false}
                        value={id_chanel}
                        select={true}
                        colorPlaceholder="#bfbfbf"
                        isBorderBottom="1px solid #D8DBDC"
                        placeholder="chọn kênh"
                        onChange={(key, item) => {
                          infoFilter.name_chanel = item.props.children;
                          this.setState({
                            id_chanel: key,
                          });
                        }}
                        data={dataChanel}
                      />
                    </Col>
                  </Row>
                </Col>
                <Col span={2}></Col>
                <Col span={11}>
                  <Row>
                    <Col span={8}>
                      <span>Xem theo</span>
                    </Col>
                    <Col
                      span={16}
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <Checkbox
                        checked={this.state.viewYear ? false : true}
                        onChange={(value) => {
                          this.state.viewYear = false;
                          this.setState({});
                        }}
                      >
                        Tháng
                      </Checkbox>
                      <Checkbox
                        checked={this.state.viewYear ? true : false}
                        onChange={(value) => {
                          this.state.viewYear = true;
                          this.setState({});
                        }}
                      >
                        Năm
                      </Checkbox>
                    </Col>
                  </Row>
                  <Row
                    style={{
                      paddingTop: 28,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Col span={8}>
                      <span>Ngành</span>
                    </Col>
                    <Col span={16}>
                      <ISelect
                        bordered={false}
                        value={id_product_type}
                        select={true}
                        colorPlaceholder="#bfbfbf"
                        isBorderBottom="1px solid #D8DBDC"
                        placeholder="chọn ngành"
                        onChange={(key, item) => {
                          infoFilter.name_product_type = item.props.children;

                          this.setState(
                            {
                              id_product_type: key,
                              id_product_group: 0,
                              isLoading: true,
                            },
                            () => this._APIDropListProductGroup(key)
                          );
                        }}
                        data={dataProductType}
                      />
                    </Col>
                  </Row>
                  <Row
                    style={{
                      paddingTop: 28,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Col span={8}>
                      <span>Nhóm PK</span>
                    </Col>
                    <Col span={16}>
                      <ISelect
                        bordered={false}
                        value={id_product_group}
                        select={true}
                        colorPlaceholder="#bfbfbf"
                        isBorderBottom="1px solid #D8DBDC"
                        placeholder="chọn nhóm PK"
                        onChange={(key, item) => {
                          infoFilter.name_product_group = item.props.children;

                          this.setState({
                            id_product_group: key,
                          });
                        }}
                        data={dataProductGroup}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row
                style={{
                  position: "absolute",
                  right: 0,
                  bottom: -60,
                }}
              >
                <div style={{ display: "flex", flexDirection: "row" }}>
                  <IButton
                    icon={ISvg.NAME.EXPERIMENT}
                    title="Lọc"
                    color={colors.main}
                    onClick={() => {
                      this.setState(
                        {
                          reloadData: true,
                          visible: false,
                          showYear: this.state.viewYear,
                          // infoFilterTable: this.state.infoFilter,
                        },
                        () => this._getAPIDataChart()
                      );
                    }}
                    style={{ marginRight: 20 }}
                  />
                  <IButton
                    icon={ISvg.NAME.CROSS}
                    title="Hủy bỏ"
                    onClick={() => {
                      // viewYear = true;

                      this.setState(
                        {
                          id_area: 0,
                          id_chanel: 0,
                          id_city: 0,
                          id_region: 0,
                          id_product_group: 0,
                          id_product_type: 0,
                          visible: false,
                          isLoading: true,
                        },
                        () => this.setState({ isLoading: false })
                      );
                    }}
                    color={colors.oranges}
                  />
                </div>
              </Row>
            </Modal>
          ) : (
            <div></div>
            // <Modal
            //   visible={this.state.visible}
            //   onOk={this.handleOk}
            //   onCancel={this.handleCancel}
            //   footer={null}
            //   closable={false}
            //   maskClosable={false}
            //   closeIcon={<div></div>}
            //   // width={}
            // >
            //   <Row>
            //     <Col span={24}>
            //       <Row>
            //         <p style={{ color: colors.text.black, fontWeight: "bold" }}>
            //           Lọc hiển thị
            //         </p>
            //       </Row>
            //       <Row style={{ paddingTop: 28 }}>
            //         <Col span={8}>
            //           <span>Xem theo</span>
            //         </Col>
            //         <Col
            //           span={16}
            //           style={{
            //             display: "flex",
            //             justifyContent: "space-between",
            //           }}
            //         >
            //           <Checkbox checked={viewYear ? false : true}>
            //             Tháng
            //           </Checkbox>
            //           <Checkbox checked={viewYear ? true : false}>
            //             Năm
            //           </Checkbox>
            //         </Col>
            //       </Row>
            //       <Row
            //         style={{
            //           paddingTop: 28,
            //           display: "flex",
            //           alignItems: "center",
            //         }}
            //       >
            //         <Col span={8}>
            //           <span>Vùng</span>
            //         </Col>
            //         <Col span={16}>
            //           <ISelect
            //             bordered={false}
            //             // defaultValue={form.getFieldValue("status")}
            //             select={true}
            //             colorPlaceholder="#bfbfbf"
            //             isBorderBottom="1px solid #D8DBDC"
            //             placeholder="chọn trạng thái"
            //             onChange={() => {}}
            //             data={[
            //               { key: 1, value: "Đang hoạt động" },
            //               { key: 0, value: "Tạm ngưng" },
            //             ]}
            //           />
            //         </Col>
            //       </Row>
            //       <Row
            //         style={{
            //           paddingTop: 28,
            //           display: "flex",
            //           alignItems: "center",
            //         }}
            //       >
            //         <Col span={8}>
            //           <span>Khu vực</span>
            //         </Col>
            //         <Col span={16}>
            //           <ISelect
            //             bordered={false}
            //             // defaultValue={form.getFieldValue("status")}
            //             select={true}
            //             colorPlaceholder="#bfbfbf"
            //             isBorderBottom="1px solid #D8DBDC"
            //             placeholder="chọn trạng thái"
            //             onChange={() => {}}
            //             data={[
            //               { key: 1, value: "Đang hoạt động" },
            //               { key: 0, value: "Tạm ngưng" },
            //             ]}
            //           />
            //         </Col>
            //       </Row>
            //       <Row
            //         style={{
            //           paddingTop: 28,
            //           display: "flex",
            //           alignItems: "center",
            //         }}
            //       >
            //         <Col span={8}>
            //           <span>Tỉnh</span>
            //         </Col>
            //         <Col span={16}>
            //           <ISelect
            //             bordered={false}
            //             // defaultValue={form.getFieldValue("status")}
            //             select={true}
            //             colorPlaceholder="#bfbfbf"
            //             isBorderBottom="1px solid #D8DBDC"
            //             placeholder="chọn trạng thái"
            //             onChange={() => {}}
            //             data={[
            //               { key: 1, value: "Đang hoạt động" },
            //               { key: 0, value: "Tạm ngưng" },
            //             ]}
            //           />
            //         </Col>
            //       </Row>
            //       <Row
            //         style={{
            //           paddingTop: 28,
            //           display: "flex",
            //           alignItems: "center",
            //         }}
            //       >
            //         <Col span={8}>
            //           <span>Kênh</span>
            //         </Col>
            //         <Col span={16}>
            //           <ISelect
            //             bordered={false}
            //             // defaultValue={form.getFieldValue("status")}
            //             select={true}
            //             colorPlaceholder="#bfbfbf"
            //             isBorderBottom="1px solid #D8DBDC"
            //             placeholder="chọn trạng thái"
            //             onChange={() => {}}
            //             data={[
            //               { key: 1, value: "Đang hoạt động" },
            //               { key: 0, value: "Tạm ngưng" },
            //             ]}
            //           />
            //         </Col>
            //       </Row>
            //     </Col>
            //   </Row>
            //   <Row
            //     style={{
            //       position: "absolute",
            //       right: 0,
            //       bottom: -60,
            //     }}
            //   >
            //     <div style={{ display: "flex", flexDirection: "row" }}>
            //       <IButton
            //         icon={ISvg.NAME.EXPERIMENT}
            //         title="Lọc"
            //         color={colors.main}
            //         onClick={() => {
            //           message.warning("Chức năng đang cập nhật");
            //         }}
            //         style={{ marginRight: 20 }}
            //       />
            //       <IButton
            //         icon={ISvg.NAME.CROSS}
            //         title="Hủy bỏ"
            //         onClick={() => {
            //           this.setState({
            //             visible: false,
            //           });
            //         }}
            //         color={colors.oranges}
            //       />
            //     </div>
            //   </Row>
            // </Modal>
          )}
        </Row>
      </Container>
    );
  }
}
