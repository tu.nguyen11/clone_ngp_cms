import React, { Component } from "react";
import { Row, Col, Button, List } from "antd";
import { ITitle, IButton, IImage, ISvg } from "../../components";
import { colors } from "../../../assets";
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";
import { priceFormat } from "../../../utils";
import TweenOne from "rc-tween-one";
import Children from "rc-tween-one/lib/plugin/ChildrenPlugin";
import DataSet from "@antv/data-set";

const ButtonGroup = Button.Group;

var second = 1000;
var minute = 1000 * 60;
var hour = 60 * minute;
var day = 24 * hour;

TweenOne.plugins.push(Children);
function toInterge(number, fix = 1) {
  if (Math.round(number) === number) {
    return "" + number;
  }

  return "" + Number(number).toFixed(fix);
}

function humanizeDuration(duration, fix = 1) {
  if (duration === 0) {
    return 0;
  }

  if (duration < minute) {
    return toInterge(duration / second, fix) + " 秒";
  }

  if (duration < hour) {
    return toInterge(duration / minute, fix) + " 分";
  }

  if (duration < day) {
    return toInterge(duration / hour, fix) + "小时";
  }

  return toInterge(duration / hour / 24, fix) + " 天";
}

var data = [
  {
    date: 1580490000000,
    pv: 100000000,
    time: 12351000,
    count: 1
  },
  {
    date: 1580576400000,
    pv: 150000000,
    time: 15000,
    count: 2
  },
  {
    date: 1580662800000,
    pv: 100000000,
    time: 25000,
    count: 3
  },
  {
    date: 1580749200000,
    pv: 300000000,
    time: 30000,
    count: 4
  },
  {
    date: 1580835600000,
    pv: 250000000,
    time: 35000,
    count: 5
  },
  {
    date: 1580922000000,
    pv: 230000000,
    time: 12351000,
    count: 1
  },
  {
    date: 1581008400000,
    pv: 251242356,
    time: 15000,
    count: 2
  },
  {
    date: 1581094800000,
    pv: 451242356,
    time: 25000,
    count: 3
  },
  {
    date: 1581181200000,
    pv: 151242356,

    time: 30000,
    count: 4
  },
  {
    date: 1581267600000,
    pv: 251242356,
    time: 35000,
    count: 5
  }
];

var dataOrder = [
  {
    date: 1580490000000,
    pv: 100,
    time: 12351000,
    count: 1
  },
  {
    date: 1580576400000,
    pv: 51,
    time: 15000,
    count: 2
  },
  {
    date: 1580662800000,
    pv: 30,
    time: 25000,
    count: 3
  },
  {
    date: 1580749200000,
    pv: 31,
    time: 30000,
    count: 4
  },
  {
    date: 1580835600000,
    pv: 22,
    time: 35000,
    count: 5
  },
  {
    date: 1580922000000,
    pv: 12,
    time: 12351000,
    count: 1
  },
  {
    date: 1581008400000,
    pv: 8,
    time: 15000,
    count: 2
  },
  {
    date: 1581094800000,
    pv: 6,
    time: 25000,
    count: 3
  },
  {
    date: 1581181200000,
    pv: 5,
    time: 30000,
    count: 4
  },
  {
    date: 1581267600000,
    pv: 68,
    time: 35000,
    count: 5
  }
];

function pick(data, field) {
  return data.map(function(item) {
    var result = {};

    for (var key in item) {
      if (item.hasOwnProperty(key) && field.indexOf(key) !== -1) {
        result[key] = item[key];
      }
    }

    return result;
  });
}

var scale = {
  date: {
    alias: "Doanh số",
    type: "time",
    mask: "DD/MM/YYYY"
  },
  pv: {
    alias: "Doanh số",

    min: 0,
    tickCount: 6
  },
  time: {
    alias: "Doanh số",

    formatter: function(value) {
      return humanizeDuration(value, 0);
    },
    tickCount: 6
  },
  count: {
    alias: "Doanh số"
  }
};

const data1 = [
  {
    country: "Hư hại,bể,vỡ",
    year: "1750",
    value: 163
  },
  {
    country: "Giao sai hàng",
    year: "1800",
    value: 203
  },
  {
    country: "Hàng quá hạn",
    year: "1850",
    value: 276
  },
  {
    country: "Hàng quá hạn",
    year: "1900",
    value: 408
  },
  {
    country: "Hàng quá hạn",
    year: "1950",
    value: 547
  },
  {
    country: "Hàng quá hạn",
    year: "1999",
    value: 729
  },
  {
    country: "Hàng quá hạn",
    year: "2050",
    value: 628
  },
  {
    country: "Hàng quá hạn",
    year: "2100",
    value: 828
  },
  {
    country: "Giao sai hàng",
    year: "1750",
    value: 502
  },
  {
    country: "Giao sai hàng",
    year: "1800",
    value: 635
  },
  {
    country: "Giao sai hàng",
    year: "1850",
    value: 809
  },
  {
    country: "Giao sai hàng",
    year: "1900",
    value: 947
  },
  {
    country: "Giao sai hàng",
    year: "1950",
    value: 1402
  },
  {
    country: "Giao sai hàng",
    year: "1999",
    value: 3634
  },
  {
    country: "Giao sai hàng",
    year: "2050",
    value: 5268
  },
  {
    country: "Giao sai hàng",
    year: "2100",
    value: 7268
  },
  {
    country: "Giao thiếu hàng",
    year: "1750",
    value: 123
  },
  {
    country: "Giao thiếu hàng",
    year: "1750",
    value: 134
  },
  {
    country: "Giao thiếu hàng",
    year: "1800",
    value: 532
  },
  {
    country: "Giao thiếu hàng",
    year: "1850",
    value: 213
  },
  {
    country: "Giao thiếu hàng",
    year: "1900",
    value: 222
  },
  {
    country: "Giao thiếu hàng",
    year: "1950",
    value: 124
  },
  {
    country: "Giao thiếu hàng",
    year: "1999",
    value: 124
  },
  {
    country: "Giao thiếu hàng",
    year: "2050",
    value: 124
  },
  {
    country: "Giao thiếu hàng",
    year: "2100",
    value: 124
  }
];
const ds1 = new DataSet();
const dv1 = ds1
  .createView()
  .source(data1)
  .transform({
    type: "percent",
    field: "value",
    // 统计销量
    dimension: "country",
    // 每年的占比
    groupBy: ["year"],
    // 以不同产品类别为分组
    as: "percent"
  });
const cols1 = {
  percent: {
    min: 0,

    formatter(val) {
      return (val * 100).toFixed(2) + "%";
    }
  }
};

export default class DetailOrderDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 10000,
      animation: {
        Children: {
          value: 27
        },
        duration: 1000
      },
      formatMoney: false
    };
  }
  _renderChart = (colorChart, height, data = []) => {
    return (
      <div>
        <Chart
          height={height}
          padding={[20, 80, 80, 80]}
          forceFit
          scale={{
            time: {
              sync: true
            }
          }}
        >
          <Tooltip />
          <View data={pick(data, ["pv", "time", "date"])} scale={scale}>
            <Axis name="time" grid={null} />
            <Geom
              type="line"
              position="date*pv*count"
              color={colorChart}
              size={2}
            />
          </View>
        </Chart>
      </div>
    );
  };

  render() {
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          border: "1px solid  #DEDFE4",
          height: "100%"
        }}
      >
        <Row style={{ height: "100%" }}>
          <Col
            lg={12}
            style={{ height: "100%", borderRight: "1px solid  #DEDFE4" }}
          >
            <Row lg={12} style={{ borderBottom: "1px solid #DEDFE4" }}>
              <div
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginBottom: 16
                  }}
                >
                  <ITitle
                    level={3}
                    title="Số lượng đơn hàng"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      flex: 1,
                      display: "flex",
                      alignItems: "flex-end"
                    }}
                  />
                </div>
                {this._renderChart(colors.main, 270, data)}
              </div>
            </Row>
            <Row lg={12}>
              <div
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30
                }}
              >
                <Row>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      marginBottom: 16
                    }}
                  >
                    <ITitle
                      level={3}
                      title="Nguyên nhân trả/hủy đơn hàng"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        flex: 1,
                        display: "flex",
                        alignItems: "flex-end"
                      }}
                    />
                  </div>
                  <div>
                    <Chart height={400} data={dv1} scale={cols1} forceFit>
                      <Legend layout="horizontal" />

                      <Axis name="year" />
                      <Axis name="percent" />
                      <Axis name="Giao thiếu hàng" />

                      <Tooltip />
                      <Geom
                        type="intervalStack"
                        position="year*percent*Giao thiếu hàng"
                        color={[
                          "country",
                          ["#CD253B", "#FF9384", "#FFC5C5", "#FFE6A6"]
                        ]}
                      />
                    </Chart>
                  </div>
                </Row>
              </div>
            </Row>
          </Col>
          <Col lg={10} style={{}}>
            <div
              style={{
                paddingLeft: 25,
                paddingRight: 25,
                paddingTop: 30,
                paddingBottom: 30
              }}
            >
              <Row span={8}>
                <Row>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      marginBottom: 16
                    }}
                  >
                    <ITitle
                      level={3}
                      title="Số lượng đơn hàng trả/hủy"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        flex: 1,
                        display: "flex",
                        alignItems: "flex-end"
                      }}
                    />
                  </div>
                  {this._renderChart(colors.blackChart, 180, data)}
                </Row>
              </Row>
              <Row span={8}>
                <Row>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      marginBottom: 16
                    }}
                  >
                    <ITitle
                      level={3}
                      title="Tỷ lệ trả/hủy đơn hàng"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        flex: 1,
                        display: "flex",
                        alignItems: "flex-end"
                      }}
                    />
                  </div>
                  {this._renderChart(colors.main, 180, dataOrder)}
                </Row>
              </Row>
              <Row span={8}>
                <Row>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      marginBottom: 16
                    }}
                  >
                    <ITitle
                      level={3}
                      title="Tăng trưởng đơn hàng"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        flex: 1,
                        display: "flex",
                        alignItems: "flex-end"
                      }}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <div style={{ width: 200 }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center"
                        }}
                      >
                        <TweenOne
                          animation={this.state.animation}
                          style={{ fontSize: 40, color: colors.main }}
                        >
                          0
                        </TweenOne>

                        <span
                          style={{
                            fontWeight: 500
                          }}
                        >
                          đơn hàng
                        </span>
                      </div>

                      <span
                        style={{
                          fontWeight: 500,
                          color: "rgba(34, 35, 43, 0.5)"
                        }}
                      >
                        so với hôm trước
                      </span>
                    </div>

                    <div style={{ marginLeft: 42 }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center"
                        }}
                      >
                        <TweenOne
                          animation={this.state.animation}
                          style={{ fontSize: 40, color: colors.main }}
                        >
                          0
                        </TweenOne>
                        <span
                          style={{
                            fontSize: 40,
                            color: colors.main,
                            marginRight: 32
                          }}
                        >
                          %
                        </span>
                        <ISvg
                          name={ISvg.NAME.ArrowThinup}
                          width={20}
                          height={20}
                          fill={colors.main}
                        />
                      </div>
                      <span
                        style={{
                          opacity: 0,
                          color: colors.main,
                          marginRight: 32
                        }}
                      >
                        %
                      </span>
                    </div>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <div style={{ width: 200 }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center"
                        }}
                      >
                        <TweenOne
                          animation={this.state.animation}
                          style={{ fontSize: 40, color: "#E56956" }}
                        >
                          0
                        </TweenOne>

                        <span
                          style={{
                            fontWeight: 500
                          }}
                        >
                          đơn hàng
                        </span>
                      </div>

                      <span
                        style={{
                          fontWeight: 500,
                          color: "rgba(34, 35, 43, 0.5)"
                        }}
                      >
                        so với cùng kỳ tháng trước
                      </span>
                    </div>

                    <div style={{ marginLeft: 42 }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center"
                        }}
                      >
                        <TweenOne
                          animation={this.state.animation}
                          style={{ fontSize: 40, color: "#E56956" }}
                        >
                          0
                        </TweenOne>
                        <span
                          style={{
                            fontSize: 40,
                            color: "#E56956",
                            marginRight: 32
                          }}
                        >
                          %
                        </span>
                        <ISvg
                          name={ISvg.NAME.ArrowThindown}
                          width={20}
                          height={20}
                          fill={"#E56956"}
                        />
                      </div>
                      <span
                        style={{
                          opacity: 0,
                          color: colors.main,
                          marginRight: 32
                        }}
                      >
                        %
                      </span>
                    </div>
                  </div>
                </Row>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
