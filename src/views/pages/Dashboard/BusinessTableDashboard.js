import React, { Component } from "react";
import { Spin, Divider } from "antd";
import { Row, Col } from "reactstrap";
import { colors } from "../../../assets";
import { priceFormat, priceShortFormat } from "../../../utils";

export default class BusinessTableDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "#FFF8E7",
      isSpin: false,

      width: window.innerWidth,
      height: window.innerHeight,
      year: 0,
      month: 0,
      dataTableSales: {
        dataAgo: [],
        dataNew: [],
      },
      dataTableASO: {
        dataAgo: [],
        dataNew: [],
      },
      dataTableOrder: {
        dataAgo: [],
        dataNew: [],
      },
      dataTableDropsize: {
        dataAgo: [],
        dataNew: [],
      },
      dataTableVPO: {
        dataAgo: [],
        dataNew: [],
      },
      viewYear: true,
      infoFilter: {},
      reloadData: false,
    };

    this.handleResize = this.handleResize.bind(this);
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    console.warn("hook", nextProps, prevState);
    if (
      nextProps.year !== prevState.year ||
      nextProps.month !== prevState.month ||
      nextProps.city_id !== prevState.city_id ||
      nextProps.dataTableSales !== prevState.dataTableSales ||
      nextProps.dataTableVPO !== prevState.dataTableVPO ||
      nextProps.dataTableOrder !== prevState.dataTableOrder ||
      nextProps.dataTableASO !== prevState.dataTableASO ||
      nextProps.dataTableDropsize !== prevState.dataTableDropsize ||
      nextProps.reloadData !== prevState.reloadData ||
      nextProps.showYear !== prevState.viewYear ||
      nextProps.infoFilter !== prevState.infoFilter
    ) {
      return {
        year: nextProps.year,
        month: nextProps.month,
        city_id: nextProps.city_id,
        dataTableSales: nextProps.dataTableSales,
        dataTableVPO: nextProps.dataTableVPO,
        dataTableOrder: nextProps.dataTableOrder,
        dataTableASO: nextProps.dataTableASO,
        dataTableDropsize: nextProps.dataTableDropsize,
        reloadData: nextProps.reloadData,
        viewYear: nextProps.showYear,
        infoFilter: nextProps.infoFilter,
      };
    } else return null;
  }
  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }
  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
  }
  // componentDidUpdate(prevProps, prevState) {
  //   if (prevProps.reloadData !== this.state.reloadData) {
  //     this.props.callback(false);
  //   }
  // }
  _renderRowMonth = (data, color, bold, price) => {
    return data.map((item) => {
      if (!item.valueTable) {
        return (
          <td
            style={{
              background: color,
              textAlign: "right",
              paddingRight: 16,
              color: colors.text.black,
              fontWeight: bold ? "bold" : "normal",
            }}
          >
            -
          </td>
        );
      }
      return (
        <td
          style={{
            background: color,
            textAlign: "right",
            paddingRight: 16,
            color: colors.text.black,
            fontWeight: bold ? "bold" : "normal",
          }}
        >
          {price
            ? priceShortFormat(item.valueTable)
            : priceFormat(Number.parseFloat(item.valueTable).toFixed(0))}
        </td>
      );
    });
  };
  _renderRowDay = () => {
    var arr = [...new Array(31)]; // Tạo mảng có 30 phần tử
    return arr.map((item, index) => (
      <th style={{ width: 130, textAlign: "center" }}>Ngày {index + 1}</th>
    ));
  };
  _infoFilter = (item) => {
    return (
      <td
        style={{
          width: 178,
          height: "100%",
          verticalAlign: "top",
          paddingRight: 20,
          paddingLeft: 20,
          paddingTop: 30,
        }}
        rowSpan={20}
      >
        <div style={{ display: "flex", flexDirection: "column" }}>
          <span style={{ fontWeight: 500 }}> Vùng </span>
          <span>{item.name_region}</span>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            marginTop: 20,
          }}
        >
          <span style={{ fontWeight: 500 }}> Khu Vực </span>
          <span>{item.name_area}</span>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            marginTop: 20,
          }}
        >
          <span style={{ fontWeight: 500 }}> Tỉnh </span>
          <span>{item.name_city}</span>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            marginTop: 60,
          }}
        >
          <span style={{ fontWeight: 500 }}> Kênh </span>
          <span>{item.name_chanel}</span>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            marginTop: 50,
          }}
        >
          <span style={{ fontWeight: 500 }}> Ngành </span>
          <span>{item.name_product_type}</span>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            marginTop: 20,
          }}
        >
          <span style={{ fontWeight: 500 }}> Nhóm </span>
          <span>{item.name_product_group}</span>
        </div>
      </td>
    );
  };
  _getTotal = (data, color, bold, price) => {
    let result = 0;
    return data.dataAgo.map((item, index) => {
      return data.dataNew.map((item1, index1) => {
        if (index == index1) {
          result = item1.valueTable - item.valueTable;
          let valueTable = Math.round(result);
          if (valueTable < 0) {
            valueTable = `(${
              price ? priceShortFormat(valueTable * -1) : valueTable * -1
            })`;
            return (
              <td
                style={{
                  background: color,
                  textAlign: "right",
                  paddingRight: 16,
                  color: colors.text.black,
                  fontWeight: bold ? "bold" : "normal",
                }}
              >
                {priceFormat(valueTable)}
              </td>
            );
          }
          if (!result) {
            return (
              <td
                style={{
                  background: color,
                  textAlign: "right",
                  paddingRight: 16,
                  color: colors.text.black,
                  fontWeight: bold ? "bold" : "normal",
                }}
              >
                -
              </td>
            );
          }
          return (
            <td
              style={{
                background: color,
                textAlign: "right",
                paddingRight: 16,
                color: colors.text.black,
                fontWeight: bold ? "bold" : "normal",
              }}
            >
              {price
                ? priceShortFormat(result)
                : priceFormat(Number.parseFloat(result).toFixed(0))}
            </td>
          );
        }
      });
    });
  };
  _getPercent = (data, color, bold) => {
    let result = 0;
    return data.dataAgo.map((item, index) => {
      return data.dataNew.map((item1, index1) => {
        if (index == index1) {
          if (!item.valueTable) {
            return (
              <td
                style={{
                  background: color,
                  textAlign: "right",
                  paddingRight: 16,
                  color: colors.text.black,
                  fontWeight: bold ? "bold" : "normal",
                }}
              >
                -
              </td>
            );
          }
          result =
            ((item1.valueTable - item.valueTable) / item.valueTable) * 100;
          let valueTable = Math.round(result);
          if (valueTable < 0) {
            valueTable = `(${valueTable * -1})`;
          }
          return (
            <td
              style={{
                background: color,
                textAlign: "right",
                paddingRight: 16,
                color: colors.text.black,
                fontWeight: bold ? "bold" : "normal",
              }}
            >
              {valueTable + "%"}
            </td>
          );
        }
      });
    });
  };
  _renderYear = () => {
    const {
      dataTableASO,
      dataTableOrder,
      dataTableDropsize,
      dataTableVPO,
      dataTableSales,
      infoFilter,
    } = this.state;
    console.log(dataTableOrder);
    return (
      <table border="1" style={{ width: 1758, borderColor: colors.line }}>
        <tr>
          <th colSpan={2} style={{ height: 60, textAlign: "center" }}>
            Tiêu chí
          </th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 1</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 2</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 3</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 4</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 5</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 6</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 7</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 8</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 9</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 10</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 11</th>
          <th style={{ width: 100, textAlign: "center" }}>Tháng 12</th>
          <th style={{ width: 140, textAlign: "center" }}> YTD/ Average</th>
        </tr>
        {/* Danh thu */}
        <tr style={{ height: 40 }}>
          {this._infoFilter(infoFilter)}
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Doanh thu {this.state.year - 1}(M)
          </td>

          {this._renderRowMonth(
            dataTableSales.dataAgo,
            this.state.color,
            false,
            true
          )}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Doanh thu {this.state.year} (M)
          </td>
          {this._renderRowMonth(
            dataTableSales.dataNew,
            this.state.color,
            false,
            true
          )}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableSales, this.state.color, false, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            {this.state.year} vs {this.state.year - 1} (%)
          </td>
          {this._getPercent(dataTableSales, this.state.color, true)}
        </tr>

        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            #Đơn hàng {this.state.year - 1}
          </td>
          {this._renderRowMonth(dataTableOrder.dataAgo, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            #Đơn hàng {this.state.year}
          </td>
          {this._renderRowMonth(dataTableOrder.dataNew, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableOrder, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            {this.state.year} vs {this.state.year - 1} (%)
          </td>
          {this._getPercent(dataTableOrder, "#F6F8FE", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Dropsize {this.state.year - 1}(M)
          </td>
          {this._renderRowMonth(dataTableDropsize.dataAgo, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Dropsize {this.state.year}(M)
          </td>
          {this._renderRowMonth(dataTableDropsize.dataNew, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableDropsize, "#E0FFED", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            {this.state.year} vs {this.state.year - 1} (%)
          </td>
          {this._getPercent(dataTableDropsize, "#E0FFED", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            ASO {this.state.year - 1} (M)
          </td>
          {this._renderRowMonth(dataTableASO.dataAgo, "#FFE5E1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            ASO {this.state.year}(M)
          </td>
          {this._renderRowMonth(dataTableASO.dataNew, "#FFE5E1", false, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableASO, "#FFE5E1", false, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            {this.state.year} vs {this.state.year - 1} (%)
          </td>
          {this._getPercent(dataTableASO, "#FFE5E1", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            VPO {this.state.year - 1}
          </td>
          {this._renderRowMonth(dataTableVPO.dataAgo, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            VPO {this.state.year}
          </td>
          {this._renderRowMonth(dataTableVPO.dataNew, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableVPO, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            {this.state.year} vs {this.state.year - 1} (%)
          </td>
          {this._getPercent(dataTableVPO, "#F1F1F1", true)}
        </tr>
      </table>
    );
  };
  _renderTitleDay = () => {
    var arr = [...new Array(31)]; // Tạo mảng có 30 phần tử
    return arr.map((item, index) => (
      <th style={{ width: 100, textAlign: "center" }}>Ngày {index + 1}</th>
    ));
  };
  _renderMonth = () => {
    const {
      dataTableASO,
      dataTableOrder,
      dataTableDropsize,
      dataTableVPO,
      dataTableSales,
      infoFilter,
    } = this.state;
    return (
      <table
        border="1"
        style={{ width: 3738, borderColor: colors.line, tableLayout: "fixed" }}
      >
        <tr>
          <th colSpan={2} style={{ height: 60, textAlign: "center" }}>
            Tiêu chí
          </th>
          {this._renderTitleDay()}
          <th style={{ width: 140, textAlign: "center" }}> YTD/ Average</th>
        </tr>
        {/* Danh thu */}
        <tr style={{ height: 40 }}>
          {this._infoFilter(infoFilter)}
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Doanh thu tháng {this.state.month - 1} (M)
          </td>
          {this._renderRowMonth(
            dataTableSales.dataAgo,
            this.state.color,
            false,
            true
          )}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Doanh thu tháng {this.state.month} (M)
          </td>
          {this._renderRowMonth(
            dataTableSales.dataNew,
            this.state.color,
            false,
            true
          )}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableSales, this.state.color, false, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Tháng {this.state.month - 1} vs tháng {this.state.month} (%)
          </td>
          {this._getPercent(dataTableSales, this.state.color, true)}
        </tr>

        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            #Đơn hàng tháng {this.state.month - 1}
          </td>
          {this._renderRowMonth(dataTableOrder.dataAgo, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            #Đơn hàng tháng {this.state.month}
          </td>
          {this._renderRowMonth(dataTableOrder.dataNew, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableOrder, "#F6F8FE", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Tháng {this.state.month - 1} vs tháng {this.state.month} (%)
          </td>
          {this._getPercent(dataTableOrder, "#F6F8FE", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Dropsize tháng {this.state.month - 1}(M)
          </td>
          {this._renderRowMonth(
            dataTableDropsize.dataAgo,
            "#E0FFED",
            false,
            true
          )}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Dropsize tháng {this.state.month}(M)
          </td>
          {this._renderRowMonth(
            dataTableDropsize.dataNew,
            "#E0FFED",
            false,
            true
          )}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableDropsize, "#E0FFED", false, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Tháng {this.state.month - 1} vs tháng {this.state.month} (%)
          </td>
          {this._getPercent(dataTableDropsize, "#E0FFED", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            ASO tháng {this.state.month - 1}(M)
          </td>
          {this._renderRowMonth(dataTableASO.dataAgo, "#FFE5E1", true, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            ASO tháng {this.state.month}(M)
          </td>
          {this._renderRowMonth(dataTableASO.dataNew, "#FFE5E1", true, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableASO, "#FFE5E1", false, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Tháng {this.state.month - 1} vs tháng {this.state.month} (%)
          </td>
          {this._getPercent(dataTableASO, "#FFE5E1", true)}
        </tr>
        {/* Đơn hàng */}
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            VPO tháng {this.state.month - 1}
          </td>
          {this._renderRowMonth(dataTableVPO.dataAgo, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            VPO tháng {this.state.month}
          </td>
          {this._renderRowMonth(dataTableVPO.dataNew, "#F1F1F1", false)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            (+/-)
          </td>
          {this._getTotal(dataTableVPO, "#F1F1F1", false, true)}
        </tr>
        <tr style={{ height: 40 }}>
          <td
            style={{
              width: 178,
              paddingLeft: 27,
              color: colors.text.black,
            }}
          >
            Tháng {this.state.month - 1} vs tháng {this.state.month} (%)
          </td>
          {this._getPercent(dataTableVPO, "#F1F1F1", true)}
        </tr>
      </table>
    );
  };

  render() {
    console.log(this.state.dataTableASO.dataAgo);
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          overflowX: "auto",
        }}
      >
        <Spin spinning={this.state.isSpin}>
          <div style={{ width: "100%" }}>
            {this.state.viewYear ? this._renderYear() : this._renderMonth()}
          </div>
        </Spin>
      </div>
    );
  }
}
