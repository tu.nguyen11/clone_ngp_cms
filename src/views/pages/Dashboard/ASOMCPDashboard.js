import React, { Component } from "react";
import { Row, Col, Button, List, message, Skeleton, Spin } from "antd";
import { ITitle, IButton, IImage, IChart } from "../../components";
import { colors } from "../../../assets";

import { priceFormat, dayInMonth } from "../../../utils";
import TweenOne from "rc-tween-one";
import Children from "rc-tween-one/lib/plugin/ChildrenPlugin";
import FormatterDay from "../../../utils/FormatterDay";
import { APIService } from "../../../services";
import InfiniteScroll from "react-infinite-scroller";
const ButtonGroup = Button.Group;

TweenOne.plugins.push(Children);

const DONHANG = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Số lượng đơn hàng",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Số lượng đơn hàng",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value,
    };
  },
];

const DAILY = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Số đại lý đặt hàng",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Số đại lý đặt hàng",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value,
    };
  },
];

const dsTB = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Doanh số TB",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };

    return {
      name: "Doanh số TB",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value,
    };
  },
];

let DOANHSO = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Doanh số",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Doanh số",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: priceFormat(value + "đ"),
    };
  },
];

const sodonDL = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Số đơn hàng/cửa hàng",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Số đơn hàng/cửa hàng",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value,
    };
  },
];

export default class ASOMCPDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSales: [
        {
          month: "1",
          city: "2016",
          revenue: 0,
        },
        {
          month: "1",
          city: "2015",
          revenue: 0,
        },

        {
          month: "1",
          city: "2017",
          revenue: 0,
        },

        {
          month: "2",
          city: "2017",
          revenue: 5,
        },
        {
          month: "2",
          city: "2015",
          revenue: 2,
        },
        {
          month: "2",
          city: "2016",
          revenue: 3,
        },
        {
          month: "3",
          city: "2016",
          revenue: 1,
        },

        {
          month: "3",
          city: "2017",
          revenue: 5.7,
        },
        {
          month: "3",
          city: "2015",
          revenue: 5,
        },
        {
          month: "4",
          city: "2015",
          revenue: 5,
        },
        {
          month: "4",
          city: "2016",
          revenue: 14.5,
        },
        {
          month: "4",
          city: "2017",
          revenue: 8.5,
        },
        {
          month: "5",
          city: "2015",
          revenue: 5,
        },
        {
          month: "5",
          city: "2016",
          revenue: 18.4,
        },
        {
          month: "5",
          city: "2017",
          revenue: 11.9,
        },
        {
          month: "6",
          city: "2015",
          revenue: 5,
        },
        {
          month: "6",
          city: "2016",
          revenue: 21.5,
        },
        {
          month: "6",
          city: "2017",
          revenue: 15.2,
        },
        {
          month: "7",
          city: "2015",
          revenue: 5,
        },
        {
          month: "7",
          city: "2016",
          revenue: 25.2,
        },
        {
          month: "7",
          city: "2017",
          revenue: 17,
        },
        {
          month: "8",
          city: "2016",
          revenue: 26.5,
        },
        {
          month: "8",
          city: "2017",
          revenue: 16.6,
        },
        {
          month: "9",
          city: "2016",
          revenue: 23.3,
        },
        {
          month: "9",
          city: "2017",
          revenue: 14.2,
        },
        {
          month: "10",
          city: "2016",
          revenue: 18.3,
        },
        {
          month: "10",
          city: "2017",
          revenue: 10.3,
        },
        {
          month: "11",
          city: "2016",
          revenue: 13.9,
        },
        {
          month: "11",
          city: "2017",
          revenue: 6.6,
        },
        {
          month: "12",
          city: "2016",
          revenue: 9.6,
        },
        {
          month: "12",
          city: "2017",
          revenue: 4.8,
        },
      ],
      arrayColor: ["#22232B", "#004C91", "#E35F4B"],

      dataOrder: [],
      dataAgency: [],
      dataAvergeSaleOrder: [],
      dataOrderByAgency: [],
      dataList: [],
      value: 1,
      url: "",
      year: 0,
      city_id: 0,
      month: 0,
      sort: 1,
      valueMoney: 0,
      isLoading: false,
      isLoadingList: true,
      isSpin: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.warn("hook", nextProps, prevState);
    if (
      nextProps.year !== prevState.year ||
      nextProps.month !== prevState.month ||
      nextProps.city_id !== prevState.city_id
    ) {
      return {
        year: nextProps.year,
        month: nextProps.month,
        city_id: nextProps.city_id,
      };
    } else return null;
  }

  // async componentDidUpdate(prevProps, prevState) {
  //   if (
  //     prevState.year !== this.state.year ||
  //     prevState.month !== this.state.month ||
  //     prevState.city_id !== this.state.city_id
  //   ) {
  //     await this.setState({
  //       isSpin: true,
  //     });

  //     const data = await Promise.all([
  //       this._APIgetSalesDashboard(
  //         this.state.month,
  //         this.state.year,
  //         this.state.city_id
  //       ),
  //       this._APIgetOrderDashboard(
  //         this.state.month,
  //         this.state.year,
  //         this.state.city_id
  //       ),
  //       this._APIgetAgencyDashboard(
  //         this.state.month,
  //         this.state.year,
  //         this.state.city_id
  //       ),
  //       this._APIgetAverageSaleOrderDashboard(
  //         this.state.month,
  //         this.state.year,
  //         this.state.city_id
  //       ),
  //       this._APIgetOrderByAgencyDashboard(
  //         this.state.month,
  //         this.state.year,
  //         this.state.city_id
  //       ),
  //       this._APIgetTopProductSodl(
  //         this.state.month,
  //         this.state.year,
  //         this.state.city_id,
  //         this.state.sort
  //       ),
  //       this._APIgetInventoryValueToday(
  //         this.state.month,
  //         this.state.year,
  //         this.state.city_id
  //       ),
  //     ]);

  //     await this.setState({
  //       isSpin: false,
  //     });
  //   }
  // }

  _APIgetOrderDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getOrderDashboard(month, year, city_id);
      let lengthDayData = data.order.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.order.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataOrder: dataNew,
      });
    } catch (err) {
      message.err(err);
    }
  };

  _APIgetTopProductSodl = async (month, year, city_id, sort) => {
    try {
      const data = await APIService._getTopProductSodl(
        month,
        year,
        city_id,
        sort
      );

      this.setState({
        dataList: data.top_sold,
        url: data.image_url,
        isLoadingList: false,
      });
    } catch (err) {
      message.err(err);
      this.setState({
        isLoadingList: false,
      });
    }
  };

  _APIgetAgencyDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getAgencyDashboard(month, year, city_id);
      let lengthDayData = data.agency_ordered.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.agency_ordered.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataAgency: dataNew,
      });
    } catch (err) {
      message.err(err);
    }
  };

  _APIgetAverageSaleOrderDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getAverageSaleOrderDashboard(
        month,
        year,
        city_id
      );
      let lengthDayData = data.average_sale.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.average_sale.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataAvergeSaleOrder: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };

  _APIgetSalesDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getSalesDashboard(month, year, city_id);
      let lengthDayData = data.sale.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.sale.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataSales: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };

  _APIgetOrderByAgencyDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getOrderByAgencyDashboard(
        month,
        year,
        city_id
      );
      let lengthDayData = data.order_by_agency.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.order_by_agency.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataOrderByAgency: dataNew,
      });
    } catch (err) {
      message.err(err);
    }
  };

  _APIgetInventoryValueToday = async (month, year, city_id) => {
    try {
      const data = await APIService._getInventoryValueToday(
        month,
        year,
        city_id
      );

      this.setState({
        valueMoney: data.inventory_value_today,
      });
    } catch (err) {
      message.err(err);
    }
  };

  // async componentDidMount() {

  //   const data = await Promise.all([
  //     this._APIgetSalesDashboard(
  //       this.state.month,
  //       this.state.year,
  //       this.state.city_id
  //     ),
  //     this._APIgetOrderDashboard(
  //       this.state.month,
  //       this.state.year,
  //       this.state.city_id
  //     ),
  //     this._APIgetAgencyDashboard(
  //       this.state.month,
  //       this.state.year,
  //       this.state.city_id
  //     ),
  //     this._APIgetAverageSaleOrderDashboard(
  //       this.state.month,
  //       this.state.year,
  //       this.state.city_id
  //     ),
  //     this._APIgetOrderByAgencyDashboard(
  //       this.state.month,
  //       this.state.year,
  //       this.state.city_id
  //     ),
  //     this._APIgetTopProductSodl(
  //       this.state.month,
  //       this.state.year,
  //       this.state.city_id,
  //       this.state.sort
  //     ),
  //     this._APIgetInventoryValueToday(
  //       this.state.month,
  //       this.state.year,
  //       this.state.city_id
  //     ),
  //   ]);

  //   await this.setState({
  //     isLoading: false,
  //   });
  // }

  render() {
    const {
      dataSales,
      dataAgency,
      dataOrder,
      dataAvergeSaleOrder,
      dataOrderByAgency,
      dataList,
      isLoadingList,
    } = this.state;
    let scaleDoanhSo = {
      date: {
        type: "time",
        mask: "D",
        tickCount: dayInMonth(this.state.month, this.state.year),
        sync: true,
      },

      value: {
        alias: "Doanh số",
        formatter: function (val) {
          return priceFormat(val);
        },
      },
    };
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          border: "1px solid  #DEDFE4",
          height: "100%",
        }}
      >
        <Spin spinning={this.state.isSpin}>
          <Row>
            <Col
              lg={14}
              style={{
                height: "100%",
              }}
            >
              <Row
                lg={12}
                style={{
                  borderBottom: "1px solid #DEDFE4",
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                }}
              >
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 3 }}
                >
                  <div style={{}}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16,
                      }}
                    >
                      <ITitle
                        level={3}
                        title="Total"
                        style={{
                          color: colors.black,
                          fontWeight: "bold",
                          flex: 1,
                          display: "flex",
                          alignItems: "flex-end",
                        }}
                      />
                      <div
                        // className="cursor"
                        // onClick={() => this.props.callback(2)}
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            marginRight: 20,
                          }}
                        >
                          <div
                            style={{
                              width: 10,
                              height: 10,
                              background: colors.main,
                              marginRight: 10,
                            }}
                          ></div>
                          <span style={{ fontSize: 12 }}>Doanh thu</span>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            marginRight: 20,
                          }}
                        >
                          <div
                            style={{
                              width: 10,
                              height: 10,
                              background: "#E35F4B",
                              marginRight: 10,
                            }}
                          ></div>
                          <span style={{ fontSize: 12 }}>MCP</span>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              width: 10,
                              height: 10,
                              background: "black",
                              marginRight: 10,
                            }}
                          ></div>
                          <span style={{ fontSize: 12 }}>ASO</span>
                        </div>
                      </div>
                    </div>

                    <IChart
                      // colorChart={colors.main}
                      arrayColor={this.state.arrayColor}
                      height={270}
                      data={dataSales}
                      tooltip1={DOANHSO}
                      scale={scaleDoanhSo}
                      position1="date*value"
                      average_sale
                    />
                  </div>
                </Skeleton>
              </Row>
              <Row lg={12}>
                <div
                  style={{
                    paddingLeft: 25,
                    paddingRight: 25,
                    paddingTop: 30,
                    paddingBottom: 30,
                  }}
                >
                  <Skeleton
                    loading={this.state.isLoading}
                    active
                    paragraph={{ rows: 3 }}
                  >
                    <Row>
                      <Col span={11}>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            marginBottom: 16,
                          }}
                        >
                          <ITitle
                            level={3}
                            title="Horeca"
                            style={{
                              color: colors.black,
                              fontWeight: "bold",
                              flex: 1,
                              display: "flex",
                              alignItems: "flex-end",
                            }}
                          />
                          <div
                            // className="cursor"
                            // onClick={() => this.props.callback(2)}
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                marginRight: 20,
                              }}
                            >
                              <div
                                style={{
                                  width: 10,
                                  height: 10,
                                  background: colors.main,
                                  marginRight: 10,
                                }}
                              ></div>
                              <span style={{ fontSize: 12 }}>Doanh thu</span>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                marginRight: 20,
                              }}
                            >
                              <div
                                style={{
                                  width: 10,
                                  height: 10,
                                  background: "#E35F4B",
                                  marginRight: 10,
                                }}
                              ></div>
                              <span style={{ fontSize: 12 }}>MCP</span>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                              }}
                            >
                              <div
                                style={{
                                  width: 10,
                                  height: 10,
                                  background: "black",
                                  marginRight: 10,
                                }}
                              ></div>
                              <span style={{ fontSize: 12 }}>ASO</span>
                            </div>
                          </div>
                        </div>
                        <IChart
                          colorChart={colors.blackChart}
                          height={270}
                          data={dataSales}
                          tooltip1={DONHANG}
                          scale={scaleDoanhSo}
                          position1="date*value"
                          arrayColor={this.state.arrayColor}
                        />
                      </Col>
                      <Col span={2}></Col>
                      <Col span={11}>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            marginBottom: 16,
                          }}
                        >
                          <ITitle
                            level={3}
                            title="CQXN"
                            style={{
                              color: colors.black,
                              fontWeight: "bold",
                              flex: 1,
                              display: "flex",
                              alignItems: "flex-end",
                            }}
                          />
                          <div
                            // className="cursor"
                            // onClick={() => this.props.callback(2)}
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                marginRight: 20,
                              }}
                            >
                              <div
                                style={{
                                  width: 10,
                                  height: 10,
                                  background: colors.main,
                                  marginRight: 10,
                                }}
                              ></div>
                              <span style={{ fontSize: 12 }}>Doanh thu</span>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                marginRight: 20,
                              }}
                            >
                              <div
                                style={{
                                  width: 10,
                                  height: 10,
                                  background: "#E35F4B",
                                  marginRight: 10,
                                }}
                              ></div>
                              <span style={{ fontSize: 12 }}>MCP</span>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                              }}
                            >
                              <div
                                style={{
                                  width: 10,
                                  height: 10,
                                  background: "black",
                                  marginRight: 10,
                                }}
                              ></div>
                              <span style={{ fontSize: 12 }}>ASO</span>
                            </div>
                          </div>
                        </div>
                        <IChart
                          colorChart={colors.blackChart}
                          height={270}
                          data={dataSales}
                          tooltip1={DONHANG}
                          scale={scaleDoanhSo}
                          position1="date*value"
                          arrayColor={this.state.arrayColor}
                        />
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Row>
            </Col>
            <Col
              lg={10}
              style={{
                borderLeft: "1px solid  #DEDFE4",
                borderRight: "1px solid  #DEDFE4",
                height: "100%",
              }}
            >
              <div
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30,
                }}
              >
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 12 }}
                >
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Sỉ"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          // className="cursor"
                          // onClick={() => this.props.callback(2)}
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 20,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.main,
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>Doanh thu</span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 20,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "#E35F4B",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>MCP</span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "black",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>ASO</span>
                          </div>
                        </div>
                      </div>
                      <IChart
                        colorChart={colors.blackChart}
                        height={180}
                        data={dataSales}
                        tooltip1={DAILY}
                        scale={scaleDoanhSo}
                        position1="date*value"
                        arrayColor={this.state.arrayColor}
                      />
                    </Row>
                  </Row>
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="TH Đ. Phố"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          // className="cursor"
                          // onClick={() => this.props.callback(2)}
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 20,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.main,
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>Doanh thu</span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 20,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "#E35F4B",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>MCP</span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "black",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>ASO</span>
                          </div>
                        </div>
                      </div>
                      <IChart
                        colorChart={colors.main}
                        height={180}
                        data={dataSales}
                        tooltip1={dsTB}
                        scale={scaleDoanhSo}
                        position1="date*value"
                        arrayColor={this.state.arrayColor}
                      />
                    </Row>
                  </Row>
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="B2B"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          // className="cursor"
                          // onClick={() => this.props.callback(2)}
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 20,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.main,
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>Doanh thu</span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 20,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "#E35F4B",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>MCP</span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "black",
                                marginRight: 10,
                              }}
                            ></div>
                            <span style={{ fontSize: 12 }}>ASO</span>
                          </div>
                        </div>
                      </div>

                      <IChart
                        colorChart={colors.blackChart}
                        height={180}
                        data={dataSales}
                        tooltip1={sodonDL}
                        scale={scaleDoanhSo}
                        position1="date*value"
                        arrayColor={this.state.arrayColor}
                      />
                    </Row>
                  </Row>
                </Skeleton>
              </div>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }
}
