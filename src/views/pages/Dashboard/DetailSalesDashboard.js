import React, { Component } from "react";
import { Row, Col, Button, List, message, Skeleton, Spin } from "antd";
import { ITitle, IButton, IImage, ISvg, IChart } from "../../components";
import { colors } from "../../../assets";

import { priceFormat, dayInMonth } from "../../../utils";
import TweenOne from "rc-tween-one";
import FormatterDay from "../../../utils/FormatterDay";
import { APIService } from "../../../services";

const DOANHSO = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Doanh số",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Doanh số",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: priceFormat(value + "đ"),
    };
  },
];

const DOANHSO1 = [
  "value*date*month",
  (value, date, month) => {
    if (value == null)
      return {
        name: `Doanh số/đơn hàng tháng ${month}`,
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: `Doanh số/đơn hàng tháng ${month}`,
      title: "Ngày " + FormatterDay.dateFormatWithString(date, "#DD#"),
      value: priceFormat(value + "đ"),
    };
  },
];

export default class DetailSalesDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 10000,
      year: 0,
      city_id: 0,
      month: 0,
      dataSales: [],
      dataOrder: [],
      datalastMonth: {},
      datalastDay: {},
      formatMoney: false,
      isLoading: true,
      isSpin: false,
    };
  }

  _APIgetAverageSaleGrowthOrder = async (month, year, city_id) => {
    try {
      const data = await APIService._getAverageSaleGrowthOrder(
        month,
        year,
        city_id
      );
      let lengthDayData = data.average_sale.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.average_sale.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }

      dataNew.map((item, index) => {
        dataNew[index].month = String(month);
      });

      const data1 = await APIService._getAverageSaleGrowthOrder(
        month == 1 ? 12 : month - 1,
        month == 1 ? year - 1 : year,
        city_id
      );

      let lengthDayData1 = data1.average_sale.chart.length;
      let lengthDay1 = dayInMonth(
        month == 1 ? 12 : month - 1,
        month == 1 ? year - 1 : year
      );
      let dataNew1 = data1.average_sale.chart;
      if (dataNew1.length == 0) {
        let time1 = new Date(
          month == 1 ? year - 1 : year,
          month == 1 ? 11 : month - 2,
          1
        );

        for (let i1 = 1; i1 <= lengthDay1 - lengthDayData1; i1++) {
          dataNew1.push({
            date: time1.getTime() - 86400000 + 86400000 * i1,
            value: null,
          });
        }
      } else {
        for (let i1 = 1; i1 <= lengthDay1 - lengthDayData1; i1++) {
          dataNew1.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      dataNew1.map((item, index) => {
        dataNew1[index].month = month == 1 ? "12" : String(month - 1);
      });

      // dataNew.map((item, index) => {
      //   dataNew1[index].date = item.date;
      // });
      if (dataNew.length >= dataNew1.length) {
        let lengtArray = dataNew1.length;
        for (let x = 0; x < lengtArray; x++) {
          dataNew1[x].date = dataNew[x].date;
        }
      } else {
        let lengtArray = dataNew.length;
        for (let x = 0; x < lengtArray; x++) {
          dataNew[x].date = dataNew1[x].date;
        }
      }

      let dataSuscess = [];
      dataSuscess = dataNew.concat(dataNew1);
      this.setState({
        dataOrder: dataSuscess,
      });
    } catch (err) {
      message.err(err);
    }
  };

  _APIgetSaleGrowthOrder = async (month, year, city_id) => {
    try {
      const data = await APIService._getSaleGrowthOrder(month, year, city_id);
      this.setState({
        datalastMonth: data.sale_growth_order_compare_last_month,
        datalastDay: data.sale_growth_order_compare_last_day,
      });
    } catch (err) {
      message.err(err);
    }
  };

  _APIgetSalesDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getSalesDashboard(month, year, city_id);
      let lengthDayData = data.sale.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.sale.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);

        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataSales: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    console.warn("hook", nextProps, prevState);
    if (
      nextProps.year !== prevState.year ||
      nextProps.month !== prevState.month ||
      nextProps.city_id !== prevState.city_id
    ) {
      return {
        year: nextProps.year,
        month: nextProps.month,
        city_id: nextProps.city_id,
      };
    } else return null;
  }

  async componentDidUpdate(prevProps, prevState) {
    if (
      prevState.year !== this.state.year ||
      prevState.month !== this.state.month ||
      prevState.city_id !== this.state.city_id
    ) {
      await this.setState({
        isSpin: true,
      });

      const data = await Promise.all([
        this._APIgetAverageSaleGrowthOrder(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetSalesDashboard(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetSaleGrowthOrder(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
      ]);

      await this.setState({
        isSpin: false,
      });
    }
  }

  async componentDidMount() {
    const data = await Promise.all([
      this._APIgetAverageSaleGrowthOrder(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetSalesDashboard(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetSaleGrowthOrder(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
    ]);

    await this.setState({
      isLoading: false,
    });
  }

  render() {
    const { dataSales, dataOrder } = this.state;
    let scalee = {
      date: {
        type: "time",
        mask: "D",
        tickCount: dayInMonth(this.state.month, this.state.year),
      },
      value: {
        alias: "Doanh số",
        formatter: function (val) {
          return priceFormat(val);
        },
      },
    };

    let string = "tháng 12/" + (this.state.year - 1);
    let string2 = "tháng " + (this.state.month - 1) + "/" + this.state.year;
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          border: "1px solid  #DEDFE4",
          height: "100%",
        }}
      >
        <Spin spinning={this.state.isSpin}>
          <Row style={{ height: "100%", width: "100%" }}>
            <Col
              lg={12}
              style={{
                height: "100%",
                borderRight: "1px solid #DEDFE4",
              }}
            >
              <Row lg={12} style={{ borderBottom: "1px solid #DEDFE4" }}>
                <div
                  style={{
                    paddingLeft: 25,
                    paddingRight: 25,
                    paddingTop: 30,
                    paddingBottom: 30,
                  }}
                >
                  <Skeleton
                    loading={this.state.isLoading}
                    active
                    paragraph={{ rows: 6 }}
                  >
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16,
                      }}
                    >
                      <ITitle
                        level={3}
                        title="Doanh số"
                        style={{
                          color: colors.black,
                          fontWeight: "bold",
                          flex: 1,
                          display: "flex",
                          alignItems: "flex-end",
                        }}
                      />
                    </div>
                    {/* {this._renderChart(colors.main, 270, data)} */}
                    <IChart
                      colorChart={colors.main}
                      height={270}
                      data={dataSales}
                      tooltip1={DOANHSO}
                      scale={scalee}
                      position1="date*value"
                    />
                  </Skeleton>
                </div>
              </Row>
              <Row lg={12}>
                <div
                  style={{
                    paddingLeft: 25,
                    paddingRight: 25,
                    paddingTop: 30,
                    paddingBottom: 30,
                  }}
                >
                  <Skeleton
                    loading={this.state.isLoading}
                    active
                    paragraph={{ rows: 6 }}
                  >
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Tăng trưởng doanh số trung bình đơn hàng"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: 12,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.main,
                                marginRight: 8,
                              }}
                            />
                            <span>
                              {this.state.month == 1 ? string : string2}
                            </span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: colors.blackChart,
                                marginRight: 8,
                              }}
                            />
                            <span>
                              {"tháng " +
                                this.state.month +
                                "/" +
                                this.state.year}
                            </span>
                          </div>
                        </div>
                      </div>

                      <IChart
                        height={270}
                        data={dataOrder}
                        chart2={true}
                        scale={scalee}
                        tooltip2={DOANHSO1}
                        position1="date*value"
                        position2="date*value"
                        colorChart2={[
                          "month",
                          [colors.blackChart, colors.main],
                        ]}
                      />
                    </Row>
                  </Skeleton>
                </div>
              </Row>
            </Col>

            <Col
              lg={12}
              style={{ paddingLeft: 25, paddingRight: 25, paddingTop: 30 }}
            >
              <Skeleton
                loading={this.state.isLoading}
                active
                paragraph={{ rows: 12 }}
              >
                <div style={{}}>
                  <ITitle
                    level={3}
                    title="Doanh số/đơn hàng"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      display: "flex",
                      alignItems: "flex-end",
                    }}
                  />
                </div>
                <div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <TweenOne
                        animation={{
                          Children: {
                            value: this.state.datalastDay.value,
                            formatMoney: { thousand: ".", decimal: "," },
                          },
                          duration: 500,
                        }}
                        style={{
                          fontSize: 30,
                          color:
                            this.state.datalastDay.is_increse == 1
                              ? colors.main
                              : "#CD253B",
                        }}
                      >
                        0
                      </TweenOne>
                      <span
                        style={{
                          fontSize: 30,
                          color:
                            this.state.datalastDay.is_increse == 1
                              ? colors.main
                              : "#CD253B",
                          marginRight: 32,
                        }}
                      >
                        đ
                      </span>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <TweenOne
                        animation={{
                          Children: {
                            value: this.state.datalastDay.percent,
                          },
                          duration: 500,
                        }}
                        style={{
                          fontSize: 30,
                          color:
                            this.state.datalastDay.is_increse == 1
                              ? colors.main
                              : "#CD253B",
                        }}
                      >
                        0
                      </TweenOne>
                      <span
                        style={{
                          fontSize: 30,
                          color:
                            this.state.datalastDay.is_increse == 1
                              ? colors.main
                              : "#CD253B",
                          marginRight: 32,
                        }}
                      >
                        %
                      </span>
                      {this.state.datalastDay.is_increse == 1 ? (
                        <ISvg
                          name={ISvg.NAME.ArrowThinup}
                          width={20}
                          height={20}
                          fill={colors.main}
                        />
                      ) : (
                        <ISvg
                          name={ISvg.NAME.ArrowThindown}
                          width={20}
                          height={20}
                          fill={"#CD253B"}
                        />
                      )}
                    </div>
                  </div>
                  <span
                    style={{ fontWeight: 500, color: "rgba(34, 35, 43, 0.5)" }}
                  >
                    so với hôm trước
                  </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                        }}
                      >
                        <TweenOne
                          animation={{
                            Children: {
                              value: this.state.datalastMonth.value,
                              formatMoney: { thousand: ".", decimal: "," },
                            },
                            duration: 500,
                          }}
                          style={{
                            fontSize: 30,
                            color:
                              this.state.datalastMonth.is_increse == 1
                                ? colors.main
                                : "#CD253B",
                          }}
                        >
                          0
                        </TweenOne>
                        <span
                          style={{
                            fontSize: 30,
                            color:
                              this.state.datalastMonth.is_increse == 1
                                ? colors.main
                                : "#CD253B",
                            marginRight: 32,
                          }}
                        >
                          đ
                        </span>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                        }}
                      >
                        <TweenOne
                          animation={{
                            Children: {
                              value: this.state.datalastMonth.percent,
                            },
                            duration: 500,
                          }}
                          style={{
                            fontSize: 30,
                            color:
                              this.state.datalastMonth.is_increse == 1
                                ? colors.main
                                : "#CD253B",
                          }}
                        >
                          0
                        </TweenOne>
                        <span
                          style={{
                            fontSize: 30,
                            color:
                              this.state.datalastMonth.is_increse == 1
                                ? colors.main
                                : "#CD253B",
                            marginRight: 32,
                          }}
                        >
                          %
                        </span>
                        {this.state.datalastMonth.is_increse == 1 ? (
                          <ISvg
                            name={ISvg.NAME.ArrowThinup}
                            width={20}
                            height={20}
                            fill={colors.main}
                          />
                        ) : (
                          <ISvg
                            name={ISvg.NAME.ArrowThindown}
                            width={20}
                            height={20}
                            fill={"#CD253B"}
                          />
                        )}
                      </div>
                    </div>
                  </div>
                  <span
                    style={{ fontWeight: 500, color: "rgba(34, 35, 43, 0.5)" }}
                  >
                    so với cùng kỳ tháng trước
                  </span>
                </div>
              </Skeleton>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }
}
