import React, { Component } from "react";
import { Row, Col, Button, List, message, Skeleton, Spin } from "antd";
import { ITitle, IButton, IImage, IChart } from "../../components";
import { colors } from "../../../assets";
import { priceFormat, dayInMonth } from "../../../utils";
import TweenOne from "rc-tween-one";
import FormatterDay from "../../../utils/FormatterDay";
import { APIService } from "../../../services";

const DOANHSO = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Doanh số tuyến",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Doanh số tuyến",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: priceFormat(value + "đ"),
    };
  },
];

const DOANHSOYB = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Doanh số TB",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Doanh số TB",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: priceFormat(value + "đ"),
    };
  },
];

const VTDL2 = [
  "value*date*type",
  (value, date, type) => {
    if (value == null)
      return {
        name: type,
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: type,
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value,
    };
  },
];

const DHTC = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Số đơn hàng thành công",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Số đơn hàng thành công",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value,
    };
  },
];

const TLDH = [
  "value*date",
  (value, date) => {
    if (value == null)
      return {
        name: "Số đơn hàng/lần viếng thăm",
        title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
        value: "Chưa có dữ liệu",
      };
    return {
      name: "Số đơn hàng/lần viếng thăm",
      title: FormatterDay.dateFormatWithString(date, "#DD#/#MM#/#YYYY#"),
      value: value,
    };
  },
];

export default class SalesmanDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 10000,
      year: 0,
      city_id: 0,
      month: 0,
      dataSales: [],
      dataAverageSaleStaff: [],
      dataCountOrderByStaff: [],
      dataRatioOrderForVisit: [],
      dataCountStaffFinishVisit: [],
      dataAgencyVisitted: [],
      dataCountStaffWork: {},
      dataAverageTimeVisit: { value: 0 },
      dataSuscces: [],
      animation: {
        Children: {
          value: 27,
        },
        duration: 1000,
      },
      formatMoney: false,
      isLoading: true,
      isSpin: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.warn("hook", nextProps, prevState);
    if (
      nextProps.year !== prevState.year ||
      nextProps.month !== prevState.month ||
      nextProps.city_id !== prevState.city_id
    ) {
      return {
        year: nextProps.year,
        month: nextProps.month,
        city_id: nextProps.city_id,
      };
    } else return null;
  }

  async componentDidUpdate(prevProps, prevState) {
    if (
      prevState.year !== this.state.year ||
      prevState.month !== this.state.month ||
      prevState.city_id !== this.state.city_id
    ) {
      await this.setState({
        isSpin: true,
      });

      const data = await Promise.all([
        this._APIgetSalesDashboard(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetChartAverageSaleStaffDashboard(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetChartCountOrderByStaffDashboard(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetChartRatioOrderForVisitDashboard(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetCountStaffWord(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetAverageTimeVisit(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        this._APIgetTwoCountAgenCyandStaff(
          this.state.month,
          this.state.year,
          this.state.city_id
        ),
        // this._APIgetChartCountStaffFinishVisit(
        //   this.state.month,
        //   this.state.year,
        //   this.state.city_id
        // )
      ]);

      await this.setState({
        isSpin: false,
      });
    }
  }

  _APIgetSalesDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getChartSaleStaff(month, year, city_id);
      let lengthDayData = data.average_sale_staff.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.average_sale_staff.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataSales: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };

  // _APIgetChartCountStaffFinishVisit = async (month, year, city_id) => {
  //   try {
  //     const data = await APIService._getChartCountStaffFinishVisit(
  //       month,
  //       year,
  //       city_id
  //     );
  //     let lengthDayData = data.staff_finish_visit.chart.length;
  //     let lengthDay = dayInMonth(month, year);
  //     let dataNew = data.staff_finish_visit.chart;
  //     if (dataNew.length == 0) {
  //       let time = new Date(year, month - 1, 1);
  //       for (let i = 1; i <= lengthDay - lengthDayData; i++) {
  //         dataNew.push({
  //           date: time.getTime() - 86400000 + 86400000 * i,
  //           value: null
  //         });
  //       }
  //     } else {
  //       for (let i = 1; i <= lengthDay - lengthDayData; i++) {
  //         dataNew.push({
  //           date: dataNew[dataNew.length - 1].date + 86400000,
  //           value: null
  //         });
  //       }
  //     }
  //     this.setState({
  //       dataCountStaffFinishVisit: dataNew
  //     });
  //   } catch (err) {
  //     message.error(err);
  //   }
  // };

  _APIgetChartAverageSaleStaffDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getChartAverageSaleStaffDashboard(
        month,
        year,
        city_id
      );
      let lengthDayData = data.average_sale_staff.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.average_sale_staff.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataAverageSaleStaff: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };

  _APIgetChartCountOrderByStaffDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getChartCountOrderByStaffDashboard(
        month,
        year,
        city_id
      );
      let lengthDayData = data.order_by_staff.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.order_by_staff.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }

      this.setState({
        dataCountOrderByStaff: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };

  _APIgetChartRatioOrderForVisitDashboard = async (month, year, city_id) => {
    try {
      const data = await APIService._getChartRatioOrderForVisitDashboard(
        month,
        year,
        city_id
      );
      let lengthDayData = data.ratio_order_for_visit.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.ratio_order_for_visit.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      this.setState({
        dataRatioOrderForVisit: dataNew,
      });
    } catch (err) {
      message.error(err);
    }
  };

  _APIgetCountStaffWord = async (month, year, city_id) => {
    try {
      const data = await APIService._getCountStaffWord(month, year, city_id);
      this.setState({
        dataCountStaffWork: data.count_staff_work,
      });
    } catch (err) {
      message.error(err);
    }
  };

  _APIgetAverageTimeVisit = async (month, year, city_id) => {
    try {
      const data = await APIService._getAverageTimeVisit(month, year, city_id);

      this.setState({
        dataAverageTimeVisit: data.ratio_order_for_visit,
      });
    } catch (err) {
      message.error(err);
    }
  };

  _APIgetTwoCountAgenCyandStaff = async (month, year, city_id) => {
    try {
      const data = await APIService._getCountAgencyVisited(
        month,
        year,
        city_id
      );
      let lengthDayData = data.agency_visitted.chart.length;
      let lengthDay = dayInMonth(month, year);
      let dataNew = data.agency_visitted.chart;
      if (dataNew.length == 0) {
        let time = new Date(year, month - 1, 1);
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: time.getTime() - 86400000 + 86400000 * i,
            value: null,
          });
        }
      } else {
        for (let i = 1; i <= lengthDay - lengthDayData; i++) {
          dataNew.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }

      dataNew.map((item, index) => {
        dataNew[index].type = "Số đại lý viếng thăm";
      });

      const data1 = await APIService._getChartCountStaffFinishVisit(
        month,
        year,
        city_id
      );
      let lengthDayData1 = data1.staff_finish_visit.chart.length;
      let lengthDay1 = dayInMonth(month, year);
      let dataNew1 = data1.staff_finish_visit.chart;
      if (dataNew1.length == 0) {
        let time1 = new Date(year, month - 1, 1);
        for (let i1 = 1; i1 <= lengthDay1 - lengthDayData1; i1++) {
          dataNew1.push({
            date: time1.getTime() - 86400000 + 86400000 * i1,
            value: null,
          });
        }
      } else {
        for (let i1 = 1; i1 <= lengthDay1 - lengthDayData1; i1++) {
          dataNew1.push({
            date: dataNew[dataNew.length - 1].date + 86400000,
            value: null,
          });
        }
      }
      dataNew1.map((item, index) => {
        dataNew1[index].type = "Số lần viếng thăm";
      });
      let dataSuscces = [];
      dataSuscces = dataNew.concat(dataNew1);

      this.setState({
        dataSuscces: dataSuscces,
      });
    } catch (err) {
      message.error(err);
    }
  };

  async componentDidMount() {
    const data = await Promise.all([
      this._APIgetSalesDashboard(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetChartAverageSaleStaffDashboard(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetChartCountOrderByStaffDashboard(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetChartRatioOrderForVisitDashboard(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetCountStaffWord(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetAverageTimeVisit(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      this._APIgetTwoCountAgenCyandStaff(
        this.state.month,
        this.state.year,
        this.state.city_id
      ),
      // this._APIgetChartCountStaffFinishVisit(
      //   this.state.month,
      //   this.state.year,
      //   this.state.city_id
      // )
    ]);
    this.setState({
      isLoading: false,
    });
  }

  render() {
    const {
      dataSales,
      dataAverageSaleStaff,
      dataCountOrderByStaff,
      dataRatioOrderForVisit,
      dataAgencyVisitted,
      dataCountStaffFinishVisit,
      dataSuscces,
    } = this.state;
    let scalee = {
      date: {
        type: "time",
        mask: "D",
        tickCount: dayInMonth(this.state.month, this.state.year),
        sync: true,
      },

      value: {
        alias: "Doanh số",
        formatter: function (val) {
          return priceFormat(val);
        },
      },
    };
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          background: "white",
          border: "1px solid  #DEDFE4",
          height: "100%",
        }}
      >
        <Spin spinning={this.state.isSpin}>
          <Row>
            <Col lg={12} style={{ height: "100%" }}>
              <Row lg={12} style={{ borderBottom: "1px solid #DEDFE4" }}>
                <div
                  style={{
                    paddingLeft: 25,
                    paddingRight: 25,
                    paddingTop: 30,
                  }}
                >
                  <Skeleton
                    loading={this.state.isLoading}
                    active
                    paragraph={{ rows: 6 }}
                  >
                    <div>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Doanh số"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                      </div>
                      <IChart
                        colorChart={colors.main}
                        height={270}
                        data={dataSales}
                        tooltip1={DOANHSO}
                        scale={scalee}
                        position1="date*value"
                      />
                    </div>
                  </Skeleton>
                </div>
              </Row>
              <Row lg={12}>
                <div
                  style={{
                    paddingLeft: 25,
                    paddingRight: 25,
                    paddingTop: 30,
                    paddingBottom: 30,
                  }}
                >
                  <Skeleton
                    loading={this.state.isLoading}
                    active
                    paragraph={{ rows: 6 }}
                  >
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Doanh số trung bình nhân viên"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                      </div>
                      <IChart
                        colorChart={colors.blackChart}
                        height={270}
                        data={dataAverageSaleStaff}
                        tooltip1={DOANHSOYB}
                        scale={scalee}
                        position1="date*value"
                      />
                    </Row>
                  </Skeleton>
                </div>
              </Row>
            </Col>
            <Col
              lg={8}
              style={{
                borderLeft: "1px solid  #DEDFE4",
                borderRight: "1px solid  #DEDFE4",
              }}
            >
              <div
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  paddingTop: 30,
                  paddingBottom: 30,
                }}
              >
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 6 }}
                >
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Viếng thăm đại lý"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                        <div
                          style={{
                            display: "flex",
                            flexDirection:
                              window.innerWidth <= 1500 ? "column" : "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              marginRight: window.innerWidth <= 1500 ? 0 : 12,
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "#CD253B",
                                marginRight: 8,
                              }}
                            />
                            <span>Số đại lý viếng thăm</span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 10,
                                height: 10,
                                background: "#FF9384",
                                marginRight: 8,
                              }}
                            />
                            <span>Số lần viếng thăm</span>
                          </div>
                        </div>
                      </div>

                      <IChart
                        colorChart="#CD253B"
                        height={180}
                        data={dataSuscces}
                        chart2={true}
                        scale={scalee}
                        tooltip2={VTDL2}
                        position1="date*value"
                        position2="date*value"
                        colorChart2={["type", ["#CD253B", "#FF9384"]]}
                      />
                    </Row>
                  </Row>
                </Skeleton>
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 6 }}
                >
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Đơn hàng thành công"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                      </div>

                      <IChart
                        colorChart={colors.main}
                        height={180}
                        data={dataCountOrderByStaff}
                        tooltip1={DHTC}
                        scale={scalee}
                        position1="date*value"
                      />
                    </Row>
                  </Row>
                </Skeleton>
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 6 }}
                >
                  <Row span={8}>
                    <Row>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: 16,
                        }}
                      >
                        <ITitle
                          level={3}
                          title="Tỷ lệ lấy đơn hàng"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            flex: 1,
                            display: "flex",
                            alignItems: "flex-end",
                          }}
                        />
                      </div>
                      <IChart
                        colorChart={colors.blackChart}
                        height={180}
                        data={dataRatioOrderForVisit}
                        tooltip1={TLDH}
                        scale={scalee}
                        position1="date*value"
                      />
                    </Row>
                  </Row>
                </Skeleton>
              </div>
            </Col>
            <Col lg={4} style={{ paddingLeft: 25, paddingRight: 25 }}>
              <Row span={24} style={{ paddingTop: 30 }}>
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 6 }}
                >
                  <div style={{}}>
                    <ITitle
                      level={3}
                      title="NVBH hoạt động"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        display: "flex",
                        alignItems: "flex-end",
                      }}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <TweenOne
                      animation={{
                        Children: {
                          value: this.state.dataCountStaffWork.value,
                        },
                        duration: 1000,
                      }}
                      style={{ fontSize: 30, marginBottom: 12 }}
                    >
                      0
                    </TweenOne>
                    <span style={{ fontSize: 10, marginLeft: 12 }}>
                      {this.state.dataCountStaffWork.attribute_name}
                    </span>
                  </div>
                </Skeleton>
              </Row>
              <Row span={24} style={{ paddingTop: 30 }}>
                <Skeleton
                  loading={this.state.isLoading}
                  active
                  paragraph={{ rows: 6 }}
                >
                  <div style={{}}>
                    <ITitle
                      level={3}
                      title="Thời lượng viếng thăm TB"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        display: "flex",
                        alignItems: "flex-end",
                      }}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <TweenOne
                      animation={{
                        Children: {
                          value: this.state.dataAverageTimeVisit.value,
                          floatLength: 2,
                        },
                        duration: 1000,
                      }}
                      style={{ fontSize: 30, marginBottom: 12 }}
                    >
                      0
                    </TweenOne>
                    <span style={{ fontSize: 10, marginLeft: 12 }}>
                      {this.state.dataAverageTimeVisit.attribute_name}
                    </span>
                  </div>
                </Skeleton>
              </Row>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }
}
