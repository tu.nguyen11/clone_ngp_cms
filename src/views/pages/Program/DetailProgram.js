import { Table, Typography, Avatar, Button, List, Modal, Card } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList
} from "../../components";
import { priceFormat } from "../../../utils";
import { Container, Col, Row } from "reactstrap";
import InfiniteScroll from "react-infinite-scroller";
import { colors } from "../../../assets";
const { Paragraph, Title } = Typography;

export default class DetailProgram extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Container fluid style={{ width: 815, height: 400 }}>
        <Card style={{ paddingTop: 28, paddingLeft: 40 }}>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ flex: 1 }}>
              <div style={{ marginBottom: 28 }}>
                <ITitle
                  level={2}
                  title="Thông tin sản phẩm"
                  style={{ fontWeight: "bold" }}
                />
              </div>
              <div style={{ marginBottom: 20 }}>
                <ITitle
                  level={4}
                  title="Nhãn hàng"
                  style={{ fontWeight: 700 }}
                />
                <ITitle level={4} title="Bà Lá Xanh" />
              </div>
              <div style={{ marginBottom: 20 }}>
                <ITitle
                  level={4}
                  title="Tên nhóm sản phẩm"
                  style={{ fontWeight: 700 }}
                />
                <ITitle level={4} title="Phân bón sinh học" />
              </div>
              <div
                style={{
                  marginBottom: 20,
                  display: "flex",
                  flexDirection: "row"
                }}
              >
                <div style={{ flex: 1 }}>
                  <ITitle
                    level={4}
                    title="Nhóm phụ thuộc"
                    style={{ fontWeight: 700 }}
                  />
                  <ITitle level={4} title="Phân bón sinh học A" />
                  <ITitle level={4} title="Phân bón sinh học B" />
                  <ITitle level={4} title="Phân bón sinh học C" />
                </div>
                <div style={{ flex: 1 }}>
                  <ITitle
                    level={4}
                    title="Trạng thái"
                    style={{ fontWeight: 700 }}
                  />
                  <ITitle level={4} title="Hiện" />
                </div>
              </div>
            </div>
            <div
              style={{
                width: 260,
                alignItems: "center",
                flexDirection: "column",
                display: "flex",
                borderWidth: 2,
                borderColor: "red"  
              }}
            >
              <ITitle
                level={2}
                title="Hình ảnh phân loại"
                style={{ fontWeight: "bold" }}
              />
              <img
                src="https://i.imgur.com/Jtq8rOB.png"
                style={{ width: 175, height: 175, marginTop: 28 }}
              />
            </div>
          </div>
        </Card>
      </Container>
    );
  }
}
