import React, {useState, useEffect, useRef} from 'react';
import {Col, Form, Row, Switch, message, Skeleton} from "antd";
import {IButton, IInput, ISelect, ISvg, ITitle} from "../../components";
import {colors} from "../../../assets";
import {StyledITitle} from "../../components/common/Font/font";
import {IFormItem, IInputText, IInputTextArea} from "../../components/common";
import {useHistory, useParams} from "react-router";
import {Box} from './BoxEditing'
import {DragDropContext, Droppable, Draggable} from "react-beautiful-dnd";
import {APIService} from "../../../services";

function CreateSurvey(props) {
    const param = useParams();
    const {id, type} = param;
    const [data, setData] = useState({})
    const [listQuestion, setListQuestion] = useState([])
    const history = useHistory();
    const reduxForm = props.form;
    const {getFieldDecorator} = reduxForm;
    const [isEditing, setIsEditing] = useState(null)
    const [listQuesDeleted, setListQuesDeleted] = useState([])
    const arrUnderfine = ['', null, undefined]
    const [isLoading, setIsLoading] = useState(true)
    const [loadingButton, setLoadingButton] = useState(false)
    const prevIndexEdit = useRef()
    useEffect(() => {
        prevIndexEdit.current = isEditing;
    })
    const getDetailCustomerSurvey = async (id) => {
        try {
            const dataResponse = await APIService._getDetailCustomerSurvey(id)
            setIsLoading(false)
            setData(dataResponse.detail)
            let dataQ = dataResponse.detail.lst_question.map(item => {
                item.lst_answer_id_deleted = []
                return item
            })
            setListQuestion(dataQ)
            reduxForm.setFieldsValue({
                _des: dataResponse.detail.description,
                _title: dataResponse.detail.title,
                _listQues: dataResponse.detail.lst_question
            })


        } catch (error) {
            setIsLoading(false)
            message.error(error)
        }
    }

    async function updateData(obj) {
        try {
            const data = await APIService._updateCustomerSurvey(obj)
            setLoadingButton(false)
            message.success("Cập nhập thông tin khảo sát thành công");
            history.goBack();
        } catch (err) {
            setLoadingButton(false)
            console.log(err)
        }
    }

    const arrQuestionType = [
        {
            key: 1,
            value: "Trắc nghiệm",
        },
        {
            key: 2,
            value: "Nhập trả lời",
        },
    ];
    useEffect(() => {
        getDetailCustomerSurvey(id)
    }, [])
    const handleEditing = (id) => {
        setIsEditing(id)
    }
    const handleCancelEditing = () => {
        setIsEditing(null)
    }
    const handleCheck = (question) => {
        let ischecked = true
        if (arrUnderfine.includes(question.quesition_name)) {
            ischecked = false
        }
        if (question.radio_or_text_type === 1 && question.lst_answer.length === 0) {
            ischecked = false
        }
        if (question.radio_or_text_type === 1 && question.lst_answer.length !== 0) {
            question.lst_answer.forEach(item => {
                if (arrUnderfine.includes(item.answer_name)) {
                    ischecked = false
                }
            })
        }
        return ischecked
    }
    const reorder = (list, startIndex, endIndex) => {
        let result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);
        return result;
    };
    console.log(listQuestion)
    const onSubmit = (e) => {
        e.preventDefault();
        if (isEditing !== null) {
            message.error('Hoàn thành chỉnh sửa câu hỏi')
            return
        }
        props.form.validateFields((err, obj) => {
            if (!err) {
                let filterListDelQues = listQuesDeleted.filter(item => item !== 0)
                let newlistQuestion = listQuestion.map((item,index)=>{
                    item.index = index + 1
                    return item
                })
                const objValue = {
                    description: obj._des,
                    id,
                    lst_id_deleted: filterListDelQues,
                    lst_question_answer: newlistQuestion,
                    title: obj._title
                };
                setLoadingButton(true)
                updateData(objValue)
            }
        })
    }
    const renderBodyCard = (listChildQues, indexQues) => {
        return (
            <DragDropContext onDragEnd={(result) => {
                if (!result.destination) {
                    return;
                }
                let sorted = reorder(
                    listChildQues,
                    result.source.index,
                    result.destination.index
                )
                sorted.map((item, index) => {
                    item.index = index + 1
                    return item
                })
                let newListQues = [...listQuestion]
                newListQues[indexQues].lst_answer = sorted
                setListQuestion(newListQues)
            }}>
                <Droppable droppableId="droppable">
                    {(droppableProvided, droppableSnapshot) => (
                        <div    {...droppableProvided.droppableProps} ref={droppableProvided.innerRef}>
                            {listChildQues.map((item, index) => {
                                return (
                                    <Draggable
                                        key={item.index}
                                        draggableId={item.answer_name.replace(' ', '') + index}
                                        index={index}
                                    >
                                        {(draggableProvided, draggableSnapshot) => (
                                            <div ref={draggableProvided.innerRef}
                                                 {...draggableProvided.draggableProps}
                                                 {...draggableProvided.dragHandleProps}>
                                                <Row
                                                >
                                                    <Col style={{
                                                        display: 'flex',
                                                        alignItems: 'flex-end',
                                                        paddingTop: 10
                                                    }} span={2}>
                                                        <div style={{cursor: 'all-scroll'}}>
                                                            <ISvg
                                                                name={ISvg.NAME.SVGMove}
                                                                width={20}
                                                                height={20}
                                                                fill={colors.main}
                                                            />
                                                        </div>
                                                        <div style={{
                                                            background: "white",
                                                            border: "1px solid black",
                                                            width: 20,
                                                            height: 20,
                                                            borderRadius: 10,
                                                            display: "flex",
                                                            justifyContent: "center",
                                                            alignItems: "center",
                                                            fontWeight: 500,
                                                        }}></div>
                                                    </Col>
                                                    <Col span={21}>
                                                        <IInput
                                                            value={item.answer_name}
                                                            placeholder='Thêm câu trả lời'
                                                            onChange={(e) => {
                                                                listChildQues[index].answer_name = e.target.value;
                                                                let newAr = [...listQuestion]
                                                                newAr[indexQues].lst_answer = listChildQues
                                                                setListQuestion(newAr)
                                                            }}
                                                        />
                                                    </Col>
                                                    <Col span={1}
                                                         style={{
                                                             display: "flex",
                                                             justifyContent: "flex-end",
                                                             paddingTop: 10
                                                         }}
                                                    >
                                                        <div
                                                            style={{
                                                                width: 20,
                                                                height: 20,
                                                                borderRadius: 10,
                                                                display: "flex",
                                                                justifyContent: "center",
                                                                alignItems: "center",
                                                                background:
                                                                    "rgba(227, 95, 75, 0.1)",
                                                                cursor: "pointer",
                                                            }}
                                                            onClick={() => {
                                                                let listChildQuesDel = listChildQues.filter(item1 => item1.index !== item.index)
                                                                /**
                                                                 * sort index after del item
                                                                 */
                                                                listChildQuesDel.map((item, index) => {
                                                                    item.index = index + 1
                                                                    return item
                                                                })
                                                                let newAr = [...listQuestion]
                                                                newAr[indexQues].lst_answer = listChildQuesDel
                                                                if (item.answer_id !== 0) {
                                                                    newAr[indexQues].lst_answer_id_deleted = [...newAr[indexQues].lst_answer_id_deleted, item.answer_id]
                                                                }
                                                                setListQuestion(newAr)
                                                            }}
                                                        >
                                                            <ISvg
                                                                name={ISvg.NAME.CROSS}
                                                                width={7}
                                                                height={7}
                                                                fill={colors.oranges}
                                                            />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        )}
                                    </Draggable>
                                )
                            })}
                            {droppableProvided.placeholder}
                        </div>)}
                </Droppable>
                <div
                    onClick={() => {
                        listChildQues.push({
                            answer_id: 0,
                            answer_name: '',
                            index: listChildQues.length + 1
                        })
                        let newListQues = [...listQuestion]
                        newListQues[indexQues].lst_answer = listChildQues
                        setListQuestion(newListQues)
                    }}
                    style={{marginLeft: 50, marginTop: 10, cursor: "pointer"}}>
                    <u>Thêm câu trả lời</u></div>
            </DragDropContext>
        )
    }

    return (
        <React.Fragment>
            <React.Fragment>
                <div style={{width: "100%"}}>
                    <Form onSubmit={onSubmit}>
                        <Row>
                            <Col spn={24}>
                                <div>
                                    <ITitle
                                        level={1}
                                        title="Chỉnh sửa thông tin khảo sát"
                                        style={{color: colors.main, fontWeight: "bold"}}
                                    />
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col spn={24}
                                 style={{marginBottom: 50}}
                            >
                                <div className='flex justify-end'>
                                    <IButton
                                        title={"Lưu"}
                                        loading={loadingButton}
                                        disabled={isLoading}
                                        icon={ISvg.NAME.SAVE}
                                        styleHeight={{
                                            width: 160,
                                            marginLeft: 15,
                                        }}
                                        color={colors.main}
                                        htmlType="submit"
                                    />
                                    <IButton
                                        title={"Hủy"}
                                        icon={ISvg.NAME.CROSS}
                                        styleHeight={{
                                            width: 160,
                                            marginLeft: 15,
                                        }}
                                        color={colors.oranges}
                                        onClick={() => {
                                            if (type === 'edit') {
                                                history.push('/survey/detail/' + id)
                                            } else history.push('/survey/list/')

                                        }}
                                    />
                                </div>
                            </Col>
                        </Row>
                        <Row gutter={[32, 16]} style={{display: 'flex'}}>
                            <Col span={8}>
                                <div style={{
                                    padding: "22px 30px",
                                    background: colors.white,
                                    height: "100%",
                                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                                }}>
                                    <Skeleton active paragraph={{rows: 20}} loading={isLoading}>
                                        <Row gutter={[0, 20]}>
                                            <Col span={24}>
                                                <StyledITitle style={{
                                                    color: "black",
                                                    fontSize: 16,
                                                    marginBottom: 30
                                                }}>
                                                    Thông tin thu thập
                                                </StyledITitle>
                                            </Col>
                                            <Col span={24} style={{marginBottom: 10}}>
                                                <Row gutter={[12, 0]}>
                                                    <Col span={24}>
                                                        <div>
                                                            <ITitle
                                                                level={4}
                                                                title="Tiêu đề khảo sát"
                                                                style={{
                                                                    color: colors.black,
                                                                    fontWeight: "bold"
                                                                }}
                                                            />
                                                            <IFormItem>
                                                                {getFieldDecorator("_title", {
                                                                    rules: [
                                                                        {
                                                                            required: true,
                                                                            message: "vui lòng nhập tiêu đề khảo sát",
                                                                        },
                                                                    ],
                                                                })(
                                                                    <IInputText
                                                                        value={reduxForm.getFieldValue("_title")}
                                                                    />
                                                                )}
                                                            </IFormItem>
                                                        </div>
                                                    </Col>

                                                </Row>
                                            </Col>
                                            <Col span={24} style={{marginBottom: 10}}>
                                                <Row>
                                                    <Col span={12}>
                                                        <div>
                                                            <ITitle
                                                                level={4}
                                                                title="Trạng thái"
                                                                style={{
                                                                    color: colors.black,
                                                                    fontWeight: "bold"
                                                                }}
                                                            />
                                                            <span
                                                                style={{
                                                                    wordWrap: "break-word",
                                                                    wordBreak: "break-all",
                                                                }}
                                                            >
                                                            {data.status_name}
                                                        </span>
                                                        </div>
                                                    </Col>
                                                    <Col span={12}>
                                                        <div>
                                                            <ITitle
                                                                level={4}
                                                                title="Showroom"
                                                                style={{
                                                                    color: colors.black,
                                                                    fontWeight: "bold"
                                                                }}
                                                            />
                                                            <span
                                                                style={{
                                                                    wordWrap: "break-word",
                                                                    wordBreak: "break-all",
                                                                }}
                                                            >
                                                            {data.show_room_name}
                                                        </span>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col span={24}>
                                                <div>
                                                    <ITitle
                                                        level={4}
                                                        title="Mô tả"
                                                        style={{
                                                            color: colors.black,
                                                            fontWeight: "bold"
                                                        }}
                                                    />
                                                    <IFormItem>
                                                        {getFieldDecorator("_des", {
                                                            rules: [
                                                                {
                                                                    required: true,
                                                                    message: "vui lòng nhập tiêu đề khảo sát",
                                                                },
                                                            ],
                                                        })(
                                                            <IInputText
                                                                value={reduxForm.getFieldValue("_des")}
                                                            />
                                                        )}
                                                    </IFormItem>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Skeleton>
                                </div>
                            </Col>
                            <Col span={11}>

                                <div style={{
                                    padding: "22px 30px",
                                    background: colors.white,
                                    height: "100%",
                                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                                }}>
                                    <Skeleton active paragraph={{rows: 20}} loading={isLoading}>
                                        <Row gutter={[0, 20]}>
                                            <Col span={24}>
                                                <StyledITitle style={{
                                                    color: "black",
                                                    fontSize: 16,
                                                    paddingBottom: 10
                                                }}>
                                                    Thông tin Câu hỏi
                                                </StyledITitle>
                                            </Col>
                                        </Row>
                                        {Array.isArray(listQuestion) ? listQuestion.map((question, indexQuestion) => {
                                            return (
                                                <Box isEditing={isEditing == indexQuestion}>
                                                    <Row>
                                                        <Col span={1} className={'text-center'}>
                                                                            <span
                                                                                className='indexList'>{indexQuestion + 1}</span>
                                                        </Col>
                                                        <Col span={22}>
                                                            <div className={'cardContent'}>
                                                                {isEditing == indexQuestion ? (
                                                                    <>
                                                                        <Row gutter={[16, 16]}
                                                                             className={'heading'}>
                                                                            <Col span={14}>
                                                                                <Form.Item>
                                                                                    <IInputText
                                                                                        placeholder={"Nhập câu hỏi"}
                                                                                        value={question.quesition_name}
                                                                                        onChange={(e) => {
                                                                                            let newArr = [...listQuestion];
                                                                                            newArr[indexQuestion].quesition_name = e.target.value
                                                                                            setListQuestion(newArr)
                                                                                        }}
                                                                                    />
                                                                                </Form.Item>
                                                                            </Col>
                                                                            <Col span={10}>
                                                                                <Form.Item>
                                                                                    <ISelect
                                                                                        select={question.default_question !== 1}
                                                                                        data={arrQuestionType}
                                                                                        value={question.radio_or_text_type}
                                                                                        onChange={(key) => {
                                                                                            const newArray = [...listQuestion];
                                                                                            newArray[indexQuestion] = {
                                                                                                ...newArray[indexQuestion],
                                                                                                radio_or_text_type: key
                                                                                            };
                                                                                            setListQuestion(newArray);
                                                                                        }}
                                                                                    />
                                                                                </Form.Item>
                                                                            </Col>
                                                                        </Row>
                                                                        <Row gutter={[16, 16]}
                                                                             className={'body'}>
                                                                            {question.radio_or_text_type == 2 ? (
                                                                                <Col span={24}>
                                                                                    <Form.Item>
                                                                                        <IInput
                                                                                            placeholder={'Viết câu trả lời của bạn'}
                                                                                        />
                                                                                    </Form.Item>
                                                                                </Col>
                                                                            ) : renderBodyCard(question.lst_answer, indexQuestion)}

                                                                        </Row>

                                                                        <Row gutter={[16, 16]}
                                                                             className={'footer'}>
                                                                            <Col span={6}
                                                                                 className={'borderRight'}>
                                                                                <Form.Item style={{
                                                                                    display: 'flex',
                                                                                    alignItems: "center",
                                                                                    marginBottom: 0
                                                                                }} name="switch"
                                                                                           label="Bắt buộc"
                                                                                           valuePropName="checked">
                                                                                    <Switch
                                                                                        defaultChecked={question.is_required === 1}
                                                                                        onChange={e => {
                                                                                            const newArray = [...listQuestion];
                                                                                            newArray[indexQuestion] = {
                                                                                                ...newArray[indexQuestion],
                                                                                                is_required: e ? 1 : 2
                                                                                            };
                                                                                            setListQuestion(newArray);

                                                                                        }}
                                                                                        style={{marginBottom: 8}}
                                                                                    />
                                                                                </Form.Item>
                                                                            </Col>
                                                                            <Col
                                                                                span={4}>
                                                                                {question.default_question !== 1 && (
                                                                                    <div style={{
                                                                                        cursor: 'Pointer',
                                                                                        marginTop: 7
                                                                                    }}
                                                                                         onClick={() => {
                                                                                             setListQuesDeleted([...listQuesDeleted, question.question_id])
                                                                                             let newArr = listQuestion.filter((item, index) => index !== indexQuestion)
                                                                                             setListQuestion(newArr)
                                                                                             handleCancelEditing()
                                                                                         }}>
                                                                                        <ISvg
                                                                                            name={ISvg.NAME.DELETE}
                                                                                            width={20}
                                                                                            height={20}
                                                                                            fill={colors.gray._400}
                                                                                        />
                                                                                    </div>
                                                                                )}
                                                                            </Col>
                                                                            <Col style={{
                                                                                display: 'flex',
                                                                                justifyContent: 'flex-end'
                                                                            }}
                                                                                 span={14}>
                                                                                <IButton
                                                                                    title={'Xong'}
                                                                                    color={!handleCheck(question) ? colors.gray._400 : colors.main}
                                                                                    disabled={!handleCheck(question)}
                                                                                    onClick={() => {
                                                                                        if (!handleCheck(question)) {
                                                                                            message.error('Vui lòng nhập đẩy đủ fields')
                                                                                            return
                                                                                        }

                                                                                        handleCancelEditing()
                                                                                    }}
                                                                                />
                                                                            </Col>
                                                                        </Row>
                                                                    </>
                                                                ) : (
                                                                    <div>
                                                                        <p>{question.quesition_name}</p>
                                                                        {question.radio_or_text_type == 1 ? question.lst_answer.map((childQues, indexChildQues) => {
                                                                            return (<div
                                                                                key={indexChildQues}
                                                                                style={{
                                                                                    display: "flex",
                                                                                    flexDirection: "row",
                                                                                    paddingTop: "15px",
                                                                                }}
                                                                            >
                                                                                <div
                                                                                    style={{
                                                                                        display: "flex",
                                                                                        cursor: "pointer",
                                                                                    }}
                                                                                >
                                                                                    <div
                                                                                        style={{
                                                                                            width: 20,
                                                                                            height: 20,
                                                                                            border: "1px solid " +
                                                                                                `${colors.line_2}`,
                                                                                            borderRadius: 10,
                                                                                        }}
                                                                                    />
                                                                                    <div
                                                                                        style={{
                                                                                            flex: 1,
                                                                                            marginLeft: 10,
                                                                                        }}
                                                                                    >
                                                                                        <span
                                                                                            style={{

                                                                                                fontSize: 14,
                                                                                                color: childQues.answer_name !== 'Khác' ?
                                                                                                    colors.text.black : colors.text.gray
                                                                                            }}
                                                                                        >
                                                                                    {childQues.answer_name}
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>)
                                                                        }) : (
                                                                            <div
                                                                                style={{marginBottom: 5}}>
                                                                                <IInput
                                                                                    style={{
                                                                                        color: "#000",
                                                                                        marginTop: 12,
                                                                                        padding: 10,
                                                                                    }}

                                                                                    className="input-border"
                                                                                    // loai="textArea"
                                                                                    placeholder="Câu trả lời của bạn..."

                                                                                />
                                                                            </div>
                                                                        )}
                                                                    </div>
                                                                )}
                                                            </div>
                                                        </Col>
                                                        <Col span={1}>
                                                            {isEditing !== indexQuestion && (
                                                                <div style={{cursor: 'Pointer'}}
                                                                     onClick={() => {
                                                                         if (isEditing !== null) {
                                                                             if (!handleCheck(listQuestion[prevIndexEdit.current])) {
                                                                                 message.error('Vui lòng nhập đẩy đủ fields')
                                                                                 return
                                                                             }
                                                                         }
                                                                         handleEditing(indexQuestion)
                                                                     }}>
                                                                    <ISvg
                                                                        name={ISvg.NAME.WRITE}
                                                                        width={20}
                                                                        height={20}
                                                                        fill={colors.main}
                                                                    />
                                                                </div>
                                                            )}
                                                        </Col>
                                                    </Row>

                                                </Box>
                                            )
                                        }) : null}
                                        {isEditing == null && (
                                            <Row>
                                                <Col span={24}
                                                     style={{
                                                         display: 'flex',
                                                         justifyContent: 'flex-end',
                                                         marginTop: 20
                                                     }}>
                                                    <IButton
                                                        title={'Tạo'}
                                                        color={colors.main}
                                                        onClick={() => {
                                                            setListQuestion([...listQuestion, {
                                                                quesition_name: "Nhập câu hỏi",
                                                                lst_answer: [],
                                                                is_required: 0,
                                                                question_id: 0,
                                                                default_question: 0,
                                                                radio_or_text_type: 2
                                                            }])
                                                            handleEditing(listQuestion.length)
                                                        }}
                                                    />

                                                </Col>
                                            </Row>
                                        )}
                                    </Skeleton>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </div>
            </React.Fragment>
        </React.Fragment>
    )
}

const createSurvey = Form.create({name: "CreateSurvey"})(CreateSurvey);
export default createSurvey;
