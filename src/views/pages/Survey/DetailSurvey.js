import React, {useEffect, useState} from 'react'
import {Col, Row, Skeleton, message} from "antd";
import {IButton, IInput, ISvg, ITitle} from "../../components";
import {colors} from "../../../assets";
import {StyledITitle} from "../../components/common/Font/font";
import {APIService} from "../../../services";
import {useHistory, useParams} from 'react-router-dom';

export default function DetailSurvey() {
    const history = useHistory()
    const param = useParams();
    const {id} = param;
    const [data, setData] = useState({})
    const [isLoading, setLoading] = useState(true);
    const getDetailCustomerSurvey = async (id) => {
        try {
            const dataResponse = await APIService._getDetailCustomerSurvey(id)
            setData(dataResponse.detail)
            setLoading(false)
        } catch (error) {
            setLoading(false)
            message.error(error)
        }
    }

    useEffect(() => {
        if (id) {
            getDetailCustomerSurvey(id)
        }
    }, [])
    const renderChildAnswer = (child, typeQues) => {
        if (typeQues === 1) {
            if (Array.isArray(child)) {
                return (
                    child.map(
                        (answer, idxAnswer) => {
                            return (
                                <div
                                    key={idxAnswer}
                                    style={{
                                        display: "flex",
                                        flexDirection: "row",
                                        paddingTop: "15px",
                                    }}
                                >
                                    <div
                                        style={{
                                            display: "flex",
                                            cursor: "pointer",
                                        }}
                                    >
                                        <div
                                            style={{
                                                width: 20,
                                                height: 20,
                                                border: "1px solid " +
                                                    `${colors.line_2}`,
                                                borderRadius: 10,
                                            }}
                                        />
                                        <div
                                            style={{
                                                flex: 1,
                                                marginLeft: 10,
                                            }}
                                        >
                                            <span
                                                style={{
                                                    fontSize: 14,
                                                    color:
                                                    colors.text.black,
                                                }}
                                            >
                                                {answer.answer_name}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            );
                        }
                    )

                )
            } else {
                return null;
            }
        } else return (
            <div style={{marginBottom: 5}}>
                <IInput
                    style={{
                        color: "#000",
                        marginTop: 12,
                        minHeight: 50,
                    }}

                    className="input-border"
                    loai="textArea"
                    placeholder="Câu trả lời của bạn..."

                />
            </div>
        )

    }
    return (
        <React.Fragment>
            <div style={{width: "100%"}}>
                <Row>
                    <Col spn={24}>
                        <div>
                            <ITitle
                                level={1}
                                title="Chi tiết thông tin khảo sát"
                                style={{color: colors.main, fontWeight: "bold"}}
                            />
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col spn={24}
                         style={{marginBottom: 50}}
                    >
                        <div className='flex justify-end'>
                            <IButton
                                title={"Chỉnh sửa"}
                                icon={ISvg.NAME.WRITE}
                                styleHeight={{
                                    width: 160,
                                    marginLeft: 15,
                                }}
                                color={colors.main}
                                onClick={() => {
                                    history.push('/survey/edit/' + id)
                                }}
                            />
                        </div>
                    </Col>
                </Row>
                <Row gutter={[32, 16]} style={{display: 'flex'}}>
                    <Col span={8}>
                        <div style={{
                            padding: "22px 30px",
                            background: colors.white,
                            height: "100%",
                            boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        }}>
                            <Skeleton active paragraph={{rows: 20}} loading={isLoading}>
                                <Row gutter={[0, 20]}>
                                    <Col span={24}>
                                        <StyledITitle style={{color: "black", fontSize: 16}}>
                                            Thông tin thu thập
                                        </StyledITitle>
                                    </Col>

                                    <Col span={24}>
                                        <Row gutter={[12, 0]}>
                                            <Col span={12}>
                                                <div>
                                                    <ITitle
                                                        level={4}
                                                        title="Tiêu đề khảo sát"
                                                        style={{color: colors.black, fontWeight: "bold"}}
                                                    />
                                                    <span
                                                        style={{
                                                            wordWrap: "break-word",
                                                            wordBreak: "break-all",
                                                        }}
                                                    >
                                                    {data.title}
                                                </span>
                                                </div>
                                            </Col>
                                            <Col span={12}>
                                                <div>
                                                    <ITitle
                                                        level={4}
                                                        title="Showroom"
                                                        style={{color: colors.black, fontWeight: "bold"}}
                                                    />
                                                    <span
                                                        style={{
                                                            wordWrap: "break-word",
                                                            wordBreak: "break-all",
                                                        }}
                                                    >
                                                    {data.show_room_name}
                                                </span>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col span={24}>
                                        <div>
                                            <ITitle
                                                level={4}
                                                title="Trạng thái"
                                                style={{color: colors.black, fontWeight: "bold"}}
                                            />
                                            <span
                                                style={{
                                                    wordWrap: "break-word",
                                                    wordBreak: "break-all",
                                                }}
                                            >
                                            {data.status_name}
                                        </span>
                                        </div>
                                    </Col>
                                    <Col span={24}>
                                        <div>
                                            <ITitle
                                                level={4}
                                                title="Mô tả"
                                                style={{color: colors.black, fontWeight: "bold"}}
                                            />
                                            <span
                                                style={{
                                                    wordWrap: "break-word",
                                                    wordBreak: "break-all",
                                                }}
                                            >
                                            {data.description}
                                        </span>
                                        </div>
                                    </Col>

                                </Row>
                            </Skeleton>
                        </div>

                    </Col>
                    <Col span={11}>
                        <div style={{
                            padding: "22px 30px",
                            background: colors.white,
                            height: "100%",
                            boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        }}>
                            <Skeleton active paragraph={{rows: 20}} loading={isLoading}>
                                <Row gutter={[0, 20]}>
                                    <Col span={24}>
                                        <StyledITitle style={{color: "black", fontSize: 16, paddingBottom: 10}}>
                                            Thông tin Câu hỏi
                                        </StyledITitle>
                                        {Array.isArray(data.lst_question) ? data.lst_question.map((question, idxQuestion) => {
                                            return (
                                                <Col span={24}>
                                                    <div
                                                        style={{
                                                            display: "flex",
                                                        }}
                                                    >
                                                        <div
                                                            style={{
                                                                width: 20,
                                                                display: "flex",
                                                                flexDirection: "column",
                                                                alignItems: "center",
                                                                marginRight: 8,
                                                            }}
                                                        >
                                                            <div
                                                                style={{
                                                                    display: "flex",
                                                                    justifyContent: "center",
                                                                }}
                                                            >
                                                                <div
                                                                    style={{
                                                                        background: "white",
                                                                        border: "2px solid black",
                                                                        fontSize:'11px',
                                                                        width: 20,
                                                                        height: 20,
                                                                        borderRadius: 10,
                                                                        display: "flex",
                                                                        justifyContent: "center",
                                                                        alignItems: "center",
                                                                        fontWeight: 700,
                                                                    }}
                                                                >
                                                                    {idxQuestion + 1}
                                                                </div>
                                                            </div>
                                                            <div
                                                                style={{
                                                                    width: 1,
                                                                    height: "100%",
                                                                }}
                                                            ></div>
                                                        </div>
                                                        <div
                                                            style={{
                                                                width: "100%",
                                                                paddingRight: 10,
                                                            }}
                                                        >
                                                            <div>
                                                                <p style={{fontWeight: 500}}>
                                                                    {question.is_required == 1 ? question.quesition_name + ' (*)' : question.quesition_name}
                                                                </p>
                                                            </div>
                                                            <div
                                                            >
                                                                {renderChildAnswer(question.lst_answer, question.radio_or_text_type)}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                            );
                                        }) : null}
                                    </Col>
                                </Row>
                            </Skeleton>
                        </div>
                    </Col>
                </Row>
            </div>


        </React.Fragment>

    )
}
