import styled from 'styled-components'


const Box = styled.div`
  background: #FFFFFF;
  margin-bottom:24px;
  .indexList{
      border: 2px solid #666;
      border-radius: 50%;
      font-size: 1rem;
      display: flex;
      font-size:11px;
      justify-content: center;
      align-items: center;
      width: 20px; 
      height: 20px;  
      font-weight:700;
  }
  .ant-form-item{
  margin-bottom:4px !important;
  }
    .cardContent {
          border: 1px solid ${props => props.isEditing ? '#0071C5;' : '#C4C4C4'};
          margin: 0 15px;
          border-radius: 2px;
          padding:16px;
    }
      .body{
      padding-bottom: 27px;
      }
    .footer{
          &:before {
            content: "";
            position: absolute;
            top: 0;
            left: 50%;
            right: 50%;
            transform: translateX(-50%);
            width: 96%;
            height: 1px;
            background: #7A7B7B;
            opacity: 0.5;
           }
    }
   .borderRight{
    height: 54px;
    &:after {
            content: "";
            position: absolute;
            right: 0;
            top: 50%;
            bottom: 50%;
            transform: translateY(-50%);
            width: 1px;
            height: 30px;
            background: #7A7B7B;
            opacity: 0.5;
    }
  }
`;


export {
    Box
}
