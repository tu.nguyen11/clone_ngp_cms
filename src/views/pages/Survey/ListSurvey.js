import React, { useState, useEffect } from 'react'
import FormatterDay from "../../../utils/FormatterDay";
import { Col, message, Row, Tooltip } from "antd";
import { ITable } from "../../components";
import { StyledITitle } from "../../components/common/Font/font";
import { colors } from "../../../assets";
import ApiService from '../../../services/APIService'
import { useHistory } from "react-router";


function ListSurvey(props) {
    const [data, setData] = useState({ })
    const { history } = useHistory()
    const [loading, setIsLoading] = useState(true)
    const getListSurvey = async () => {
        try {
            const dataResponse = await ApiService._getListCustomerSurvey();
            dataResponse.customer_survey.map((item, index) => {
                item.stt = index + 1
                return item
            })
            setIsLoading(false)
            setData(dataResponse)
        } catch (err) {
            message.error(err)
        }

    }
    useEffect(() => {
        getListSurvey()

    }, [])
    const columns = [
        {
            title: "STT",
            dataIndex: "stt",
            width: 80,
            key: "stt",
            align: "center",
        },
        {
            title: "Tiêu đề khảo sát",
            dataIndex: "title",
            key: "title",
            render: (title) => <span>{!title ? "-" : title}</span>,
        },
        {
            title: "Thời gian cập nhật",
            dataIndex: "update_time",
            key: "update_time",
            render: (update_time) => (
                <span>
                    {!update_time || update_time <= 0
                        ? "-"
                        : FormatterDay.dateFormatWithString(
                            update_time,
                            "#DD#/#MM#/#YYYY# #hhh#:#mm#"
                        )}
                </span>
            ),
        },

        {
            title: "Số lượng câu hỏi",
            align: "center",
            dataIndex: "countQuestion",
            key: "countQuestion",
            render: (countQuestion) => (
                <Tooltip title={countQuestion}>
                    <span>{!countQuestion ? "-" : countQuestion}</span>
                </Tooltip>
            ),
        },
        {
            title: "Showroom",
            dataIndex: "show_room_name",
            key: "show_room_name",
            render: (show_room_name) => <span>{!show_room_name ? "-" : show_room_name}</span>,
        },
        {
            title: "Trạng thái",
            dataIndex: "status_name",
            key: "status_name",
            render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
        },

        {
            title: "Mô tả",
            dataIndex: "description",
            key: "description",
            render: (description) => <span>{!description ? "-" : description}</span>,
        },
    ];

    return (
        <React.Fragment>
            <div>
                <Row>
                    <Col span={24} style={{ paddingBottom: 147 }}>
                        <Row>
                            <Col span={12}>
                                <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                                    Quản lý thu thập thông tin khảo sát khách hàng
                                </StyledITitle>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={24}>
                        <Row>
                            <ITable
                                columns={columns}
                                data={data.customer_survey}
                                style={{ width: "100%" }}
                                defaultCurrent={1}
                                size="small"
                                maxpage={data.total}
                                loading={loading}
                                hidePage={true}
                                onRow={(record, rowIndex) => {
                                    return {
                                        onClick: (event) => {
                                            const indexRow = Number(
                                                event.currentTarget.attributes[1].ownerElement
                                                    .dataset.rowKey
                                            );
                                            const id = data.customer_survey[indexRow].id;
                                            const pathname = "/survey/detail/" + id;
                                            props.history.push(pathname);
                                        }, // click row
                                        onMouseDown: (event) => {
                                            const indexRow = Number(
                                                event.currentTarget.attributes[1].ownerElement
                                                    .dataset.rowKey
                                            );
                                            const id = data.customer_survey[indexRow].id;
                                            const pathname = "/survey/detail/" + id;
                                            if (event.button == 2 || event == 4) {
                                                window.open(pathname, "_blank");
                                            }
                                        },
                                    };
                                }}
                            />
                        </Row>
                    </Col>
                </Row>


            </div>
        </React.Fragment>
    )
}
export default ListSurvey
