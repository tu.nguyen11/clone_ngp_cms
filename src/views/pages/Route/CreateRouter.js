import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  IInput,
  IUpload,
  ISelectStatus,
  ISelectAllStatus,
  ISelectWareHouse,
  ISelectGender,
  ISelectRouterSalesman,
  ILoading,
  ITableTransfer,
  ISelectSaleman,
} from "../../components";
import { colors, strings } from "../../../assets";
import { message, Button, Form, DatePicker, Upload, Modal, Empty } from "antd";
import { ImageType } from "../../../constant";
import { APIService } from "../../../services";
import moment from "moment";
import ISelectFrequency from "../../components/ISelectFrequency";
import ModalRouterAssignStock from "./ModalRouterAssignStock";
let dateFormat = "dd/MM/yyyy";

class CreateRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errEnd: true,
      errStart: true,
      dateEnd: 0,
      dateStart: 0,
      errStatus: true,
      status: 0,
      errStock: true,
      stock: 0,
      data: {},
      visible: false,
      errSaleman: true,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onOkDatePickerStart = this.onOkDatePickerStart.bind(this);
    this.onOkDatePickerEnd = this.onOkDatePickerEnd.bind(this);
    this.onChangeStatus = this.onChangeStatus.bind(this);
    this.type = this.props.match.params.type;
    this.id = Number(this.props.match.params.id);
    this.bool = this.type == "create";
    this.loading = this.bool;
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentDidMount() {
    if (this.id != 0) {
      this._APIgetDetailRouter(this.id);
      // this.loading = true;
    }
  }

  _APIgetDetailRouter = async (idRouter) => {
    try {
      const data = await APIService._getDetailRouter(idRouter);
      this.loading = true;
      let listShop = data.route_detail.listShop.map((item, index) => {
        const id = item.id;
        const shop_name = item.shop_name;
        const frequency_id = item.frequency_id;
        const frequency_code = item.frequency_code;
        return { id, shop_name, frequency_id, frequency_code };
      });
      data.route_detail.listShop = listShop;
      this.setState({
        data: data.route_detail,
        dateStart: data.route_detail.start_date,
        dateEnd: data.route_detail.end_date,
      });
    } catch (error) {}
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      if (this.bool) {
        const objAdd = {
          route_name: fieldsValue.nameTuyen,
          status: this.state.status,
          warehouse_id: this.state.stock,
          route_code: fieldsValue.maTuyen,
          start_date: this.state.dateStart,
          end_date: this.state.dateEnd,
        };
        this._APIpostAddRouter(objAdd);
      } else {
        let listShop = this.state.data.listShop.map((item, index) => {
          const id = item.id;
          const shop_name = item.shop_name;
          const frequency_id = item.frequency_id;
          return { id, shop_name, frequency_id };
        });

        const objUpdate = {
          route_id: this.id,
          route_name: fieldsValue.nameTuyen,
          route_code: fieldsValue.maTuyen,
          status: fieldsValue.status,
          warehouse_id: fieldsValue.kho,
          start_date: this.state.dateStart,
          end_date: this.state.dateEnd,
          saleman_id: fieldsValue.admimCharge,
          listShop: listShop,
        };

        this._APIpostEditRouter(objUpdate);
      }
    });
  };

  _APIpostEditRouter = async (obj) => {
    try {
      const data = await APIService._postUpdateRouter(obj);
      message.success("Sửa tuyến thành công");
      this.props.history.push("/detailRoute/" + this.id);
    } catch (err) {
      console.log(err);
    }
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = (e) => {};

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  _APIpostAddRouter = async (obj) => {
    try {
      const data = await APIService._postAddRouter(obj);
      message.success("Thêm tuyến thành công");
      this.props.history.push("/routePage");
    } catch (err) {
      console.log(err);
    }
  };
  onOkDatePickerStart = (date, dateString) => {
    this.state.dateStart = Date.parse(date._d);
    this.setState(
      {
        dateStart: this.state.dateStart,
        errStart: false,
      },
      () => {
        this.props.form.validateFields(["dateBegin"], { force: true });
      }
    );
  };

  onOkDatePickerEnd = (date, dateString) => {
    this.state.dateEnd = Date.parse(date._d);
    this.setState(
      {
        dateEnd: this.state.dateEnd,
        errEnd: false,
      },
      () => {
        this.props.form.validateFields(["dateEnd"], { force: true });
      }
    );
  };

  onChangeStatus = (value) => {
    // this.state.objAdd.status = Number(value);
    this.setState({ status: value, errStatus: false }, () =>
      this.props.form.validateFields(["statusContract"], { force: true })
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { data } = this.state;
    return (
      <Container fluid style={{ flex: 1 }}>
        {/* <Col className="d-flex flex-column" style={{ height: "100%" }}> */}
        <Row className="mt-3">
          <ITitle
            level={1}
            title={this.bool ? "Tạo tuyến mới" : "Chỉnh sửa tuyến"}
            style={{
              // marginLeft: 5,
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
            }}
          />
        </Row>
        {this.loading ? (
          <Form onSubmit={this.handleSubmit}>
            <Row className="m-0 p-0 mt-4">
              <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}></Row>
              <Row>
                <IButton
                  icon={ISvg.NAME.SAVE}
                  title={"Lưu"}
                  htmlType="submit"
                  color={colors.main}
                />
                <IButton
                  icon={ISvg.NAME.CROSS}
                  title={"Hủy"}
                  color={colors.oranges}
                  style={{ marginLeft: 20 }}
                  onClick={() => {
                    this.props.history.push("/routePage");
                  }}
                />
              </Row>
            </Row>
            <Row
              style={{ height: "100%" }}
              className="p-0"
              sm={12}
              md={12}
              style={{}}
            >
              <Col xs={5} style={{ background: colors.white }}>
                <Row className="m-0 p-0" style={{}}>
                  <ITitle
                    level={4}
                    title="Thông tin tuyến"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      marginTop: 30,
                      marginLeft: 40,
                      marginBottom: 26,
                    }}
                  />
                </Row>
                <div style={{ marginLeft: 40, marginRight: 40 }}>
                  <Row className="p-0 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("nameTuyen", {
                          rules: [
                            {
                              type: "string",
                              required: true,
                              pattern: new RegExp(
                                "^[a-zA-Z\\sàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]*$"
                              ),
                              message:
                                "Không được để trống , chỉ nhập chữ và khoảng cách , không nhập số và ký tự đặc biệt . Tối đa 50 ký tự",
                              max: 50,
                            },
                          ],
                          initialValue: this.bool ? "" : data.route_name,
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Tên tuyến"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              {this.bool ? (
                                <IInput placeholder="Nhập tên tuyến" />
                              ) : (
                                <IInput
                                  placeholder="Nhập tên tuyến"
                                  value={data.route_name}
                                  onChange={(e) => {
                                    data.route_name = e.target.value;
                                    this.setState({});
                                  }}
                                />
                              )}
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>

                    <Col
                      className="p-0"
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator("status", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errStatus,
                              message: "chọn trạng thái",
                            },
                          ],
                          initialValue: this.bool ? "" : data.status_id,
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Trạng thái"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              {this.bool ? (
                                <ISelectStatus
                                  style={{
                                    width: "100%",
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  onChange={(key) => {
                                    this.state.status = key;
                                    this.setState({
                                      status: this.state.status,
                                      errStatus: false,
                                    });
                                  }}
                                  type="SALEMAN"
                                  isBackground={false}
                                />
                              ) : (
                                <ISelectStatus
                                  style={{
                                    width: "100%",
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  value={data.status_id}
                                  onChange={(key) => {
                                    data.status_id = key;
                                    this.setState(
                                      {
                                        data: this.state.data,
                                        errStart: false,
                                      },
                                      () =>
                                        this.props.form.validateFields(
                                          ["status"],
                                          { force: true }
                                        )
                                    );
                                  }}
                                  type="SALEMAN"
                                  isBackground={false}
                                />
                              )}
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("maTuyen", {
                          rules: [
                            {
                              type: "string",
                              whitespace: true,
                              required: true,
                              pattern: new RegExp(
                                "^[a-zA-Z0-9_-\\sàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]*$"
                              ),
                              message:
                                "Chỉ nhập chữ, số. Ký tự đặc biệt cho phép dấu . _ - .Không được nhập khoảng cách. Tối đa 20 ký tự",
                              max: 20,
                            },
                          ],
                          initialValue: this.bool ? "" : data.route_code,
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Mã tuyến"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              {this.bool ? (
                                <IInput placeholder="Nhập mã tuyến" />
                              ) : (
                                <IInput
                                  placeholder="Nhập mã tuyến"
                                  value={data.route_code}
                                  onChange={(e) => {
                                    data.route_code = e.target.value;
                                    this.setState({});
                                  }}
                                />
                              )}
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>

                    <Col
                      className="p-0"
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator("kho", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errStock,
                              message: "chọn loại kho",
                            },
                          ],
                          initialValue: this.bool ? "" : data.warehouse_id,
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Kho trực thuộc"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              {this.bool ? (
                                <ISelectWareHouse
                                  style={{
                                    width: "100%",
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  onChange={(key) => {
                                    this.state.stock = key;
                                    this.setState(
                                      {
                                        stock: this.state.stock,
                                        errStock: false,
                                      },
                                      () =>
                                        this.props.form.validateFields(
                                          ["kho"],
                                          { force: true }
                                        )
                                    );
                                  }}
                                  isBackground={false}
                                />
                              ) : (
                                <ISelectWareHouse
                                  style={{
                                    width: "100%",
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  value={data.warehouse_id}
                                  onChange={(key) => {
                                    data.warehouse_id = key;
                                    this.setState({
                                      data: this.state.data,
                                      errStock: false,
                                    });
                                  }}
                                  isBackground={false}
                                />
                              )}
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>

                  {this.bool ? null : (
                    <Row className="p-0 mb-4 ml-0 mr-0 mt-0">
                      <Col className="p-0 m-0">
                        <Row className="p-0 m-0">
                          <ITitle
                            level={4}
                            title={"Danh sách đại lý trực thuộc"}
                            style={{ fontWeight: 500 }}
                          />
                          <div
                            className="cursor"
                            style={{
                              flex: 1,
                              display: "flex",
                              justifyContent: "flex-end",
                            }}
                            onClick={() => {
                              this.showModal();
                            }}
                          >
                            <ISvg
                              name={ISvg.NAME.ARROWUP}
                              width={20}
                              height={20}
                              fill={colors.main}
                            />
                          </div>
                        </Row>
                        {data.listShop.length === 0 ? (
                          <div style={{ marginTop: 12 }}>
                            <span>Không có danh sách đại lý trực thuộc</span>
                          </div>
                        ) : (
                          data.listShop.map((item, index) => {
                            return (
                              <Row className="m-1">
                                <Col className="p-0 m-0">
                                  <ITitle
                                    style={{ marginTop: 10 }}
                                    level={4}
                                    title={item.shop_name}
                                  />
                                </Col>

                                <Col className="p-0 m-0" xs="auto">
                                  <ITitle
                                    style={{ marginTop: 10 }}
                                    level={4}
                                    title={item.frequency_code}
                                  />
                                </Col>
                              </Row>
                            );
                          })
                        )}
                      </Col>
                    </Row>
                  )}
                </div>
              </Col>
              <Col className="m-0 p-0 " xs="auto">
                <div
                  style={{
                    width: 1,
                    height: "100%",
                    background: colors.gray._300,
                  }}
                ></div>
              </Col>
              <Col xs="auto" style={{ background: colors.white }}>
                <Row className="m-0 p-0" style={{}}>
                  <ITitle
                    level={4}
                    title="Thời gian hiệu lực"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      marginTop: 30,
                      marginBottom: 26,
                      marginLeft: 40,
                    }}
                  />
                </Row>
                <div style={{ marginLeft: 40, marginRight: 40 }}>
                  <Row className="m-0 p-0" style={{}}>
                    <Form.Item>
                      {getFieldDecorator("dateBegin", {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: this.state.errStart,
                            message: "chọn thời gian bắt đầu",
                          },
                        ],
                        initialValue: this.bool ? "" : data.start_date,
                      })(
                        <div>
                          <Row className="m-0 p-0 mb-2">
                            <ITitle
                              level={4}
                              title={"Bắt đầu"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                              }}
                            />
                          </Row>
                          <Row className="m-0 p-0 ">
                            {this.bool ? (
                              <DatePicker
                                style={{
                                  width: "100%",
                                  background: colors.white,
                                  borderStyle: "solid",
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1,
                                }}
                                format="DD/MM/YYYY hh:mm"
                                showTime
                                placeholder="nhập thời gian bắt đầu"
                                // suffixIcon={() => null}
                                size="large"
                                onChange={this.onOkDatePickerStart}
                              />
                            ) : (
                              <DatePicker
                                style={{
                                  width: "100%",
                                  background: colors.white,
                                  borderStyle: "solid",
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1,
                                }}
                                format="DD/MM/YYYY hh:mm"
                                showTime
                                size="large"
                                value={moment(
                                  new Date(this.state.dateStart),
                                  dateFormat
                                )}
                                onChange={this.onOkDatePickerStart}
                              />
                            )}
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Row>

                  <Row className="m-0 p-0" style={{}}>
                    <Form.Item>
                      {getFieldDecorator("dateBegin", {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: this.state.errStart,
                            message: "chọn thời gian bắt đầu",
                          },
                        ],
                        initialValue: this.bool ? "" : data.start_date,
                      })(
                        <div>
                          <Row className="m-0 p-0 mb-2">
                            <ITitle
                              level={4}
                              title={"Kết thúc"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                              }}
                            />
                          </Row>
                          <Row className="m-0 p-0 ">
                            {this.bool ? (
                              <DatePicker
                                style={{
                                  width: "100%",
                                  background: colors.white,
                                  borderStyle: "solid",
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1,
                                }}
                                showTime
                                format="DD/MM/YYYY hh:mm"
                                placeholder="nhập thời gian kết thúc"
                                // suffixIcon={() => null}
                                size="large"
                                onChange={this.onOkDatePickerEnd}
                              />
                            ) : (
                              <DatePicker
                                style={{
                                  width: "100%",
                                  background: colors.white,
                                  borderStyle: "solid",
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1,
                                }}
                                format="DD/MM/YYYY hh:mm"
                                showTime
                                placeholder="Nhập thời gian kết thúc"
                                value={moment(
                                  new Date(this.state.dateEnd),
                                  dateFormat
                                )}
                                // suffixIcon={() => null}
                                size="large"
                                onChange={this.onOkDatePickerEnd}
                              />
                            )}
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Row>

                  {this.bool ? null : (
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("admimCharge", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errSaleman,
                              message: "chọn ngày kết thúc",
                            },
                          ],
                          initialValue: this.bool ? "" : data.salman_id,
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Nhân viên phụ trách"}
                              style={{
                                color: colors.black,
                                fontWeight: 500,
                              }}
                            />
                            <ISelectSaleman
                              value={data.salman_id}
                              route_id={this.id}
                              isBackground={false}
                              style={{ paddingLeft: 12 }}
                              onChange={(key) => {
                                data.salman_id = key;
                                this.setState(
                                  {
                                    errSaleman: false,
                                    data: this.state.data,
                                  },
                                  () =>
                                    this.props.form.validateFields(
                                      ["admimCharge"],
                                      { force: true }
                                    )
                                );
                              }}
                            />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>
                  )}
                </div>
              </Col>
            </Row>
            <Modal
              visible={this.state.visible}
              width={window.innerWidth - 200}
              footer={null}
              onCancel={this.handleCancel}
            >
              <ModalRouterAssignStock
                dataRight={data.listShop}
                onSave={(arr) => {
                  this.handleCancel();
                  data.listShop = arr;
                  this.setState({
                    data: this.state.data,
                  });
                }}
                onCancel={(arr) => {
                  this.handleCancel();
                }}
              />
            </Modal>
          </Form>
        ) : (
          <ILoading />
        )}
        {/* </Col> */}
      </Container>
    );
  }
}
const createRouter = Form.create({ name: "CreateRouter" })(CreateRouter);

export default createRouter;
