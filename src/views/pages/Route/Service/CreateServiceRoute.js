import {
  Row,
  Col,
  Modal,
  Form,
  message,
  Skeleton,
  BackTop,
  Tooltip,
} from "antd";
import React, { useEffect, useState } from "react";
import { ISvg, IButton, ITitle, ISelect } from "../../../components";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import useTranferLocality from "./useTranferLocality";
import { useHistory, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  IDatePickerFrom,
  IInputText,
  IFormItem,
} from "../../../components/common";
import moment from "moment";
import { StyledITileHeading } from "../../../components/common/Font/font";
import { CLEAR_REDUX_LOCALITY, ARRAY_LOCALITY } from "../../../store/reducers";

const NewDate = new Date();

function CreateServiceRoute(props) {
  const history = useHistory();
  const params = useParams();
  const { id, type } = params;
  const isCheck = type === "edit";
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;
  const [isModal, setModal] = useState(false);
  const [isLoading, setLoading] = useState(isCheck);
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [dataDetail, setDataDetail] = useState({ routeDvnnRegion: [] });
  const dispatch = useDispatch();
  const dataProduct = useSelector((state) => state);
  let { listLocalityAdd, name_agency } = dataProduct;

  const disabledStartDateApply = (startValue) => {
    if (!startValue || !NewDate) {
      return false;
    }
    if (
      startValue.valueOf() < NewDate.valueOf() ||
      startValue.valueOf() > reduxForm.getFieldValue("time_end")
    ) {
      return startValue.valueOf();
    }
  };

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !reduxForm.getFieldValue("time_start")) {
      return endValue.valueOf() < NewDate.valueOf();
    }
    return endValue.valueOf() <= reduxForm.getFieldValue("time_start");
  };

  const [dataDropSalesman, setDataDropSalesman] = useState([]);

  const [loadingAgency, setLoadingAgency] = useState(false);
  const [dataAgency, setDataAgency] = useState([]);

  // function capitalizeFirstLetter(string) {
  //   return string.charAt(0).toUpperCase() + string.slice(1);
  // }

  const _getAPIListAgency = async () => {
    try {
      const data = await APIService._getBranchDropList();
      const dataNew = data.agency.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });

      setDataAgency(dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  const fetchAPIDropSalesman = async (key, id, agencyId) => {
    try {
      setLoadingAgency(true);
      const data = await APIService._getDroplistSalesman(key, id, agencyId);

      let dataNew = data.saleman_dvnn.map((item) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setDataDropSalesman(dataNew);
      setLoadingAgency(false);
    } catch (error) {
      console.log(error);
      setLoadingAgency(false);
    }
  };

  useEffect(() => {
    _getAPIListAgency();
  }, []);

  const fetchAPIDetail = async (router_id) => {
    try {
      const data = await APIService._getDetailServiceRoute(router_id);

      const objData = data.route_dvnn;
      setDataDetail(data.route_dvnn);
      reduxForm.setFieldsValue({
        name_router: objData.route_name,
        status_router: objData.status,
        code_router: objData.route_code,
        time_start: objData.start_date,
        time_end: objData.end_date,
        branch_under: 1,
        saleman_under: objData.sale_id,
      });
      dispatch(ARRAY_LOCALITY(objData.routeDvnnRegion));
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  const fetchUpdateRouter = async (obj) => {
    try {
      setLoadingCreate(true);
      const data = await APIService._updateAddRouter(obj);

      message.success("Cập nhập thành công");
      setLoadingCreate(false);
      history.goBack();
    } catch (error) {
      console.log(error);
      setLoadingCreate(false);
    }
  };

  const fetchAddRouter = async (obj) => {
    try {
      const data = await APIService._postAddRouter(obj);

      message.success("Thêm tuyến thành công");
      setLoadingCreate(false);
      history.push("/route/serviceSalesman");
    } catch (error) {
      console.log(error);
      setLoadingCreate(false);
    }
  };

  useEffect(() => {
    if (isCheck) {
      fetchAPIDetail(id);
      return;
    }
    // dispatch(RESET_LOCALITY());
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        if (isCheck) {
          let list_region = listLocalityAdd.map((item) => {
            const ward_id = item.ward_id;
            const frequency_id = item.frequency_id;
            const id = 0;
            return { ward_id, frequency_id, id };
          });
          const objEdit = {
            name: values.name_router,
            code: values.code_router,
            status: values.status_router,
            id: id,
            saleman_id: values.saleman_under,
            start_date: values.time_start,
            end_date: values.time_end,
            list_region: list_region,
            route_dvnn_id: id,
          };
          fetchUpdateRouter(objEdit);
        } else {
          let list_region = listLocalityAdd.map((item) => {
            const ward_id = item.ward_id;
            const frequency_id = item.frequency_id;
            const id = 0;
            return { ward_id, frequency_id, id };
          });
          const objAdd = {
            name: values.name_router,
            code: values.code_router,
            status: values.status_router,
            saleman_id: values.saleman_under,
            start_date: values.time_start,
            end_date: values.time_end,
            list_region: list_region,
            route_dvnn_id: 0,
          };
          fetchAddRouter(objAdd);
        }
      }
    });
  };

  return (
    <div style={{ width: "100%" }}>
      <Row>
        <Col flex="auto">
          <ITitle
            level={1}
            title={
              isCheck ? "Chỉnh sửa tuyến dịch vụ" : "Tạo tuyến dịch vụ mới"
            }
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
      </Row>

      <BackTop description={1000} />

      <div style={{ margin: "32px 0px" }}>
        <Form onSubmit={(event) => handleSubmit(event)}>
          <Row>
            <Col flex="auto">
              <div className="flex justify-end">
                <div style={{ marginRight: 15 }}>
                  <IButton
                    color={colors.main}
                    title="Lưu"
                    loading={loadingCreate}
                    icon={ISvg.NAME.SAVE}
                    styleHeight={{ width: 140 }}
                    htmlType="submit"
                  />
                </div>

                <IButton
                  onClick={() => {
                    dispatch(CLEAR_REDUX_LOCALITY());
                    history.goBack();
                  }}
                  styleHeight={{ width: 140 }}
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  title="Hủy"
                />
              </div>
            </Col>
          </Row>
          <div
            style={{
              margin: "32px 0px 12px 0px",
              width: 815,
              background: "white",
              boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
            }}
          >
            <div>
              <Row gutter={[24, 0]}>
                <Col span={16}>
                  <div
                    style={{
                      padding: "30px 40px 120px 40px",
                      borderRight: "1px solid #D8DBDC",
                    }}
                  >
                    <Skeleton
                      loading={isLoading}
                      active
                      paragraph={{ rows: 12 }}
                    >
                      <Row gutter={[0, 24]}>
                        <Col span={24}>
                          <StyledITileHeading
                            style={{ marginTop: 8 }}
                            minFont="12px"
                            maxFont="16px"
                          >
                            Thông tin tuyến
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <div style={{ marginTop: 12 }}>
                            <p style={{ fontWeight: 500 }}>
                              Tên tuyến
                              <span style={{ marginLeft: 5, color: "red" }}>
                                *
                              </span>
                            </p>
                            <IFormItem>
                              {getFieldDecorator("name_router", {
                                rules: [
                                  {
                                    required: true,
                                    message: "nhập tên tuyển bán hàng",
                                  },
                                ],
                              })(
                                <IInputText
                                  placeholder="nhập tên tuyển bán hàng"
                                  value={reduxForm.getFieldValue("name_router")}
                                />
                              )}
                            </IFormItem>
                          </div>
                        </Col>
                        <Col span={24}>
                          <div>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <p style={{ fontWeight: 500 }}>
                                  Trạng thái
                                  <span style={{ marginLeft: 5, color: "red" }}>
                                    *
                                  </span>
                                </p>

                                <IFormItem>
                                  {getFieldDecorator("status_router", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "chọn trạng thái",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <ISelect
                                        className="clear-pad"
                                        placeholder="chọn trạng thái"
                                        data={[
                                          { key: 1, value: "Đang hoạt động" },
                                          { key: 0, value: "Tạm ngưng" },
                                        ]}
                                        style={{
                                          background: colors.white,
                                          borderStyle: "solid",
                                          borderWidth: 0,
                                          borderColor: colors.line,
                                          borderBottomWidth: 1,
                                        }}
                                        type="write"
                                        select={true}
                                        isBackground={false}
                                        onChange={(key, option) => {
                                          debugger;
                                          reduxForm.setFieldsValue({
                                            status_router:
                                              option.props.dataProps.key,
                                          });
                                        }}
                                        value={reduxForm.getFieldValue(
                                          "status_router"
                                        )}
                                      />
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p style={{ fontWeight: 500 }}>
                                    Mã tuyến
                                    <span
                                      style={{ marginLeft: 5, color: "red" }}
                                    >
                                      *
                                    </span>
                                  </p>
                                  {
                                    <IFormItem>
                                      {getFieldDecorator("code_router", {
                                        rules: [
                                          {
                                            required: true,
                                            message: "nhập mã tuyến",
                                          },
                                        ],
                                      })(
                                        <IInputText
                                          disabled={isCheck ? true : false}
                                          placeholder="nhập mã tuyến"
                                          value={reduxForm.getFieldValue(
                                            "code_router"
                                          )}
                                        />
                                      )}
                                    </IFormItem>
                                  }
                                </div>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                        <Col span={24}>
                          <div>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <div>
                                  <p style={{ fontWeight: 500 }}>
                                    PKD trực thuộc
                                    <span
                                      style={{ marginLeft: 5, color: "red" }}
                                    >
                                      *
                                    </span>
                                  </p>
                                  <IFormItem>
                                    {getFieldDecorator("branch_under", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn PKD trực thuộc",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <ISelect
                                          className="clear-pad"
                                          placeholder="chọn PKD trực thuộc"
                                          type="write"
                                          select={
                                            type === "edit" ? false : true
                                          }
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                          }}
                                          isBackground={false}
                                          onChange={async (key, option) => {
                                            reduxForm.setFieldsValue({
                                              branch_under: Number(option.key),
                                            });
                                            debugger;

                                            await fetchAPIDropSalesman(
                                              "",
                                              id,
                                              key
                                            );
                                          }}
                                          value={
                                            !reduxForm.getFieldValue(
                                              "branch_under"
                                            )
                                              ? undefined
                                              : reduxForm.getFieldValue(
                                                  "branch_under"
                                                )
                                          }
                                          data={dataAgency}
                                        />
                                      </div>
                                    )}
                                  </IFormItem>
                                </div>
                              </Col>

                              <Col span={12}>
                                <p style={{ fontWeight: 500 }}>
                                  Nhân viên phụ trách
                                  <span style={{ marginLeft: 5, color: "red" }}>
                                    *
                                  </span>
                                </p>
                                <div>
                                  <IFormItem>
                                    {getFieldDecorator("saleman_under", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn nhân viên",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <ISelect
                                          select={
                                            !reduxForm.getFieldValue(
                                              "branch_under"
                                            ) || loadingAgency
                                              ? false
                                              : true
                                          }
                                          loading={loadingAgency}
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                          }}
                                          type="write"
                                          isBackground={false}
                                          onChange={(key, option) => {
                                            reduxForm.setFieldsValue({
                                              saleman_under: Number(option.key),
                                            });
                                          }}
                                          defaultValue={reduxForm.getFieldValue(
                                            "saleman_under"
                                          )}
                                          placeholder="chọn nhân viên"
                                          data={dataDropSalesman}
                                        />
                                      </div>
                                    )}
                                  </IFormItem>
                                </div>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                        <Col span={24}>
                          <Row
                            type="flex"
                            justify="space-between"
                            style={{ alignItems: "center" }}
                          >
                            <Col flex="auto">
                              <StyledITileHeading
                                style={{ marginTop: 12 }}
                                minFont="12px"
                                maxFont="16px"
                              >
                                Danh sách Địa bàn
                                <span style={{ marginLeft: 5, color: "red" }}>
                                  *
                                </span>
                              </StyledITileHeading>
                            </Col>
                            <Col flex="auto">
                              <Tooltip title="Gán địa bàn" placement="right">
                                <div
                                  style={{
                                    width: 50,
                                    display: "flex",
                                    justifyContent: "flex-end",
                                  }}
                                  className="cursor"
                                  onClick={() => {
                                    if (
                                      !reduxForm.getFieldValue("branch_under")
                                    ) {
                                      message.warning(
                                        "Vui lòng chọn Pkd trực thuộc trước"
                                      );
                                      return;
                                    }
                                    setModal(true);
                                  }}
                                >
                                  <ISvg
                                    width={20}
                                    height={20}
                                    fill={colors.main}
                                    name={ISvg.NAME.ARROWUP}
                                  />
                                </div>
                              </Tooltip>
                            </Col>
                          </Row>
                        </Col>
                        {listLocalityAdd.length === 0 ? null : (
                          <Col span={24}>
                            <div>
                              <Row gutter={[12, 0]}>
                                <Col span={6}>
                                  <span style={{ fontWeight: 500 }}>
                                    Phường / Xã
                                  </span>
                                </Col>
                                <Col span={6}>
                                  <span style={{ fontWeight: 500 }}>
                                    Quận / Huyện
                                  </span>
                                </Col>
                                <Col span={6}>
                                  <span style={{ fontWeight: 500 }}>
                                    Tỉnh / Thành
                                  </span>
                                </Col>
                                <Col span={6}>
                                  <span style={{ fontWeight: 500 }}>
                                    Tuần suất
                                  </span>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        )}
                        <Col
                          span={24}
                          style={{
                            height: 250,
                            overflowY: "auto",
                            overflowX: "hidden",
                          }}
                        >
                          {listLocalityAdd.map((item) => {
                            return (
                              <Col
                                span={24}
                                style={{ paddingTop: 4, paddingBottom: 4 }}
                              >
                                <div>
                                  <Row gutter={[12, 0]}>
                                    <Col span={6}>
                                      <span>{item.ward_name}</span>
                                    </Col>
                                    <Col span={6}>
                                      <span>{item.district_name}</span>
                                    </Col>
                                    <Col span={6}>
                                      <span>{item.city_name}</span>
                                    </Col>
                                    <Col span={6}>
                                      <span style={{ fontWeight: 500 }}>
                                        {item.frequency_code}
                                      </span>
                                    </Col>
                                  </Row>
                                </div>
                              </Col>
                            );
                          })}
                        </Col>
                      </Row>
                    </Skeleton>
                  </div>
                </Col>
                <Col span={8}>
                  <div
                    style={{
                      padding: "30px 24px 30px 12px",
                    }}
                  >
                    <Skeleton
                      loading={isLoading}
                      active
                      paragraph={{ rows: 12 }}
                    >
                      <Row gutter={[0, 24]}>
                        <Col span={24}>
                          <StyledITileHeading
                            style={{ marginTop: 8 }}
                            minFont="12px"
                            maxFont="16px"
                          >
                            Thời gian hiệu lực
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <div
                            style={{
                              marginTop: 12,
                            }}
                          >
                            <p style={{ fontWeight: 500 }}>
                              Bắt đầu
                              <span style={{ marginLeft: 5, color: "red" }}>
                                *
                              </span>
                            </p>
                            <IFormItem>
                              {getFieldDecorator("time_start", {
                                rules: [
                                  {
                                    required: true,
                                    message: "nhập thời gian bắt đầu",
                                  },
                                ],
                              })(
                                <div
                                  style={{ borderBottom: "1px solid #d8dbdc" }}
                                >
                                  <IDatePickerFrom
                                    bordered={false}
                                    paddingInput="0px"
                                    disabledDate={disabledStartDateApply}
                                    onChange={(date, dateString) => {
                                      let timeStart = date._d.getTime();
                                      reduxForm.setFieldsValue({
                                        time_start: timeStart,
                                      });
                                    }}
                                    showTime
                                    showToday={false}
                                    defaultValue={
                                      isCheck
                                        ? moment(
                                            new Date(
                                              reduxForm.getFieldValue(
                                                "time_start"
                                              )
                                            ),
                                            "hh:mm:ss DD-MM-YYYY"
                                          )
                                        : undefined
                                    }
                                    disabled={isCheck}
                                    placeholder="nhập thời gian bắt đầu"
                                  />
                                </div>
                              )}
                            </IFormItem>
                          </div>
                        </Col>

                        <Col span={24}>
                          <p style={{ fontWeight: 500 }}>
                            Kết thúc
                            <span style={{ marginLeft: 5, color: "red" }}>
                              *
                            </span>
                          </p>
                          <IFormItem>
                            {getFieldDecorator("time_end", {
                              rules: [
                                {
                                  required: true,
                                  message: "nhập thời gian kết thúc",
                                },
                              ],
                            })(
                              <div
                                style={{
                                  borderBottom: "1px solid #d8dbdc",
                                }}
                              >
                                <IDatePickerFrom
                                  bordered={false}
                                  paddingInput="0px"
                                  disabledDate={disabledEndDateApply}
                                  onChange={(date, dateString) => {
                                    let timeEnd = date._d.getTime();

                                    reduxForm.setFieldsValue({
                                      time_end: timeEnd,
                                    });
                                  }}
                                  showTime
                                  showToday={false}
                                  defaultValue={
                                    isCheck
                                      ? moment(
                                          new Date(
                                            reduxForm.getFieldValue("time_end")
                                          ),
                                          "hh:mm:ss DD-MM-YYYY"
                                        )
                                      : undefined
                                  }
                                  placeholder="nhập thời gian kết thúc"
                                />
                              </div>
                            )}
                          </IFormItem>
                        </Col>
                      </Row>
                    </Skeleton>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </Form>
      </div>
      <Modal
        visible={isModal}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useTranferLocality(
          () => setModal(false),
          id,
          reduxForm.getFieldValue("branch_under")
        )}
      </Modal>
    </div>
  );
}
const createServiceRoute = Form.create({ name: "CreateServiceRoute" })(
  CreateServiceRoute
);

export default createServiceRoute;
