import { Col, Row } from "antd";
import { set } from "lodash";
import React, { useEffect, useState } from "react";
import { ISelect } from "../../../components";
import { APIService } from "../../../../services";

function useAddress(callback = () => {}) {
  const [isPopupScroll, setPopupScroll] = useState(false);
  const [isDisabled, setDisabled] = useState(false);
  const [listCities, setListCities] = useState([]);
  const [listDistricts, setListDistricts] = useState([]);
  const [filterAddress, setfilerAddress] = useState({
    city: 0,
    districts: 0,
  });
  const [loadingDistricts, setLoadingDistricts] = useState(false);

  const [loadingCities, setLoadingCities] = useState(true);
  const [fileterCities, setFilterCities] = useState("");
  let timer = null;
  const fetchListCities = async (keySearch) => {
    try {
      const data = await APIService._getListCities(keySearch);
      let dataNew = data.cities.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      setListCities(dataNew);
      setLoadingCities(false);
    } catch (error) {
      console.log(error);
      setLoadingCities(false);
    }
  };

  const fetchDistricts = async (city_id, key) => {
    try {
      const data = await APIService._getListCounty(city_id, key);
      let dataNew = data.district.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });

      setListDistricts(dataNew);
      setDisabled(true);
      setLoadingDistricts(false);
      setPopupScroll(true);
    } catch (error) {
      console.log(error);
      setLoadingDistricts(false);
    }
  };

  useEffect(() => {
    fetchListCities(fileterCities);
  }, [fileterCities]);

  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[24, 0]}>
        <Col span={12}>
          <ISelect
            data={listCities}
            style={{ width: "100%" }}
            minWidth="100%"
            defaultValue="Tỉnh / Thành"
            loading={loadingCities}
            showSearch={true}
            isSearch={true}
            select={true}
            onChange={(key, option) => {
              const city_id = Number(option.key);
              setfilerAddress({ ...filterAddress, city: Number(option.key) });
              setLoadingDistricts(true);
              fetchDistricts(city_id, "");
            }}
            onSearch={(value) => {
              if (timer) {
                clearTimeout(timer);
              }
              timer = setTimeout(() => {
                setLoadingCities(true);
                setFilterCities(value);
              }, 500);
            }}
          />
        </Col>
        <Col span={12}>
          <ISelect
            data={listDistricts}
            minWidth="100%"
            defaultValue="Quận / Huyện"
            select={isDisabled}
            isSearch={true}
            showSearch={true}
            onChange={(key, option) => {
              callback(filterAddress.city, Number(option.key));
              // setPopupScroll(false);
            }}
            onDropdownVisibleChange={(open) => setPopupScroll(open)}
            loading={loadingDistricts}
            open={isPopupScroll}
          />
        </Col>
      </Row>
    </div>
  );
}

export default useAddress;
