const Add_locality = (state, action) => {
  let arrActive = state.listLocalityAdd.map((item) => item.ward_id);
  let arrActiveAddClone = state.listLocalityAddClone.map((item) => item.id);
  if (action.payload.status) {
    if (arrActive.indexOf(action.payload.data.ward_id) === -1) {
      let obj = {
        ...action.payload.data,
        frequency_id: 0,
        frequency_code: "",
      };
      state.listLocalityAddClone.push(obj);
    } else {
      if (arrActiveAddClone.indexOf(action.payload.data.ward_id) === -1) {
        let obj = {
          ...action.payload.data,
          frequency_id: 0,
          frequency_code: "",
        };
        state.listLocalityAddClone.push(obj);
      }
    }
  } else {
    let arrActiveClone = state.listLocalityAddClone.map((item) => item.ward_id);
    let idx = arrActiveClone.indexOf(action.payload.data.ward_id);
    state.listLocalityAddClone.splice(idx, 1);
  }
  return state;
};

const Remove_locality = (state, action) => {
  state.listLocalityShow.splice(action.payload, 1);
  state.listLocalityAddClone.splice(action.payload, 1);
  return state;
};

const Update_ActiveList = (state, action) => {
  debugger;
  let arrActive = state.listLocality.map((item) => item.ward_id);
  let idx = arrActive.indexOf(action.payload.id);
  state.listLocality[idx].status = action.payload.status;
  return state;
};

const Select_frequency_id = (state, action) => {
  debugger;
  let arrActive = state.listLocalityShow.map((item) => item.ward_id);
  let idx = arrActive.indexOf(action.payload.data.ward_id);

  state.listLocalityShow[idx].frequency_id = action.payload.key;
  state.listLocalityShow[idx].frequency_code = action.payload.name;

  let arrActiveAddClone = state.listLocalityAddClone.map(
    (item) => item.ward_id
  );
  let idxAddClone = arrActiveAddClone.indexOf(action.payload.data.ward_id);

  state.listLocalityAddClone[idxAddClone].frequency_id = action.payload.key;
  state.listLocalityAddClone[idxAddClone].frequency_code = action.payload.name;
  return state;
};

const Save_locality = (state, action) => {
  state.listLocalityShow = state.listLocalityAddClone;
  let arrTmp = state.listLocalityShow;
  for (var i = 0; i < arrTmp.length; ++i) {
    for (var j = i + 1; j < arrTmp.length; ++j) {
      if (arrTmp[i].ward_id == arrTmp[j].ward_id) {
        arrTmp.splice(j--, 1);
      }
    }
  }
  state.listLocalityShow = arrTmp;
  return state;
};

const Confirm_locality = (state, action) => {
  state.listLocalityAdd = state.listLocalityShow;
  return state;
};

const Array_locality = (state, action) => {
  state.listLocalityAdd = action.payload;
  state.listLocalityShow = action.payload;
  return state;
};

const Reset_locality = (state, action) => {
  state.listLocalityAddClone = [];
  state.listLocalityAdd = [];
  state.listLocalityShow = [];
  return state;
};

const Not_checkAll = (state, action) => {
  state.listLocalityAddClone = [];
  return state;
};

const Deafuat_listLocality = (state, action) => {
  let arrActive = state.listLocalityShow.map((item) => item.ward_id);
  action.payload.map((item) => {
    if (arrActive.indexOf(item.ward_id) !== -1) {
      item.status = 1;
    }
  });
  state.listLocalityAddClone = state.listLocalityShow;
  state.listLocality = action.payload;
  state.listLocalityClone = action.payload;
  return state;
};

const Cancel_locality = (state, action) => {
  state.listLocalityShow = state.listLocalityAdd;
  state.listLocalityClone.map((item) => {
    if (item.status === 1) {
      return item.status === 0;
    }
  });
  state.listLocality = state.listLocalityClone;
  state.listLocalityAddClone = [];
  return state;
};

export {
  Add_locality,
  Array_locality,
  Select_frequency_id,
  Save_locality,
  Confirm_locality,
  Reset_locality,
  Deafuat_listLocality,
  Remove_locality,
  Update_ActiveList,
  Cancel_locality,
  Not_checkAll,
};
