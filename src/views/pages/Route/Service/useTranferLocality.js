import { SearchOutlined } from "@ant-design/icons";
import { Checkbox, Col, Row, List, Affix, Spin, message } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import useAddress from "./useAddress";

import { StyledSearchCustom } from "../../../components/common";
import { IButton, ISelect, ISvg, ITableHtml } from "../../../components";
import { StyledITileHeading } from "../../../components/common/Font/font";
import {
  ADD_LOCALITY,
  CONFIRM_LOCALITY,
  SAVE_LOCALITY,
  SELECT_FREQUENCY_ID,
  DEAFUAR_LISTLOCALITY,
  REMOVE_LOCALITY,
  UPDATE_ACTIVELIST,
  CANCEL_LOCALITY,
  RESET_LOCALITY,
  NOT_CHECK_ALL,
} from "../../../store/reducers";

let headerTable = [
  {
    name: "STT",
    align: "center",
  },
  {
    name: "Phường / Xã",
    align: "left",
  },
  {
    name: "Quận / Huyện",
    align: "left",
  },
  {
    name: "Tỉnh / Thành",
    align: "left",
  },
  {
    name: "Tần suất",
    align: "left",
  },
  {
    name: "",
    align: "center",
  },
];

export default function useTranferLocality(
  callbackClose = () => {},
  route_dvnn_id,
  agencyId
) {
  const [loadingList, setLoadingList] = useState(true);

  const [filterList, setFilterList] = useState({
    key: "",
    page: 1,
    route_dvnn_id: route_dvnn_id,
    district_id: 0,
    city_id: 0,
    agencyId: agencyId,
  });

  const [selectFrequency, setSelectFrenquency] = useState([]);
  const [filterFrequency, setFilterFrequency] = useState({
    from_limit: 0,
    to_limit: 10,
  });

  const dispatch = useDispatch();
  const dataProduct = useSelector((state) => state);
  const {
    listLocalityShow,
    listLocality,
    listLocalityAddClone,
    listLocalityClone,
  } = dataProduct;

  const [checkAll, setCheckAll] = useState(false);

  const fetchAPIListLocality = async (obj) => {
    try {
      const data = await APIService._getListLocality(obj);
      dispatch(DEAFUAR_LISTLOCALITY(data.ward));

      if (data.ward.length === 0) {
        setCheckAll(false);
      }
      if (listLocalityShow.length > 0) {
        if (listLocalityShow[0].district_name === data.ward[0].district_name) {
          if (listLocalityShow.length === data.ward.length) {
            setCheckAll(true);
          } else {
            setCheckAll(false);
          }
        } else {
          setCheckAll(false);
        }
      }

      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  const fetchAPISelectFrequency = async (obj) => {
    try {
      const { from_limit, to_limit } = obj;
      const data = await APIService._getSelectFrequency(from_limit, to_limit);

      let dataNew = data.frequency_plan.map((item) => {
        const key = item.id;
        const value = item.code;
        return { key, value };
      });
      setSelectFrenquency(dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchAPISelectFrequency(filterFrequency);
  }, [filterFrequency]);

  useEffect(() => {
    if (!agencyId) {
      return;
    }
    fetchAPIListLocality({ ...filterList, agencyId: agencyId });
  }, [filterList, agencyId]);

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="tr-table">
        {headerTable.map((item, index) => (
          <th className={"th-table"} style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr>
        <td className="td-table" style={{ textAlign: "center" }}>
          {index + 1}
        </td>
        <td className="td-table">{item.ward_name}</td>

        <td className="td-table">{item.district_name}</td>
        <td className="td-table">{item.city_name}</td>
        <td className="td-table">
          <ISelect
            bordered={false}
            isBorderBottom="1px solid #D8DBDC"
            minWidth="140px"
            fontWeight="500"
            showSearch={true}
            isSearch={true}
            select={true}
            value={!item.frequency_id ? "Chọn tần suất" : item.frequency_id}
            onChange={(key, option) => {
              debugger;
              dispatch(
                SELECT_FREQUENCY_ID({
                  data: item,
                  key: Number(option.key),
                  name: option.props.children,
                })
              );
            }}
            data={selectFrequency}
          />
        </td>
        <td className="td-table">
          <div
            style={{
              background: "rgba(227, 95, 75, 0.1)",
              width: 20,
              height: 20,
              borderRadius: 10,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              cursor: "pointer",
            }}
            onClick={() => {
              dispatch(REMOVE_LOCALITY(index));
              setCheckAll(false);
              dispatch(
                UPDATE_ACTIVELIST({
                  id: item.ward_id,
                  status: 0,
                })
              );
            }}
            className="cursor-push remove"
          >
            <ISvg width={8} height={8} name={ISvg.NAME.CROSS} fill="#E35F4B" />
          </div>
        </td>
      </tr>
    ));
  };

  const handleInfiniteOnLoad = () => {
    setLoadingList(true);

    setFilterList({ ...filterList, page: filterList.page + 1 });
  };

  let timer = null;
  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading
            style={{ marginTop: 8 }}
            minFont="12px"
            maxFont="16px"
          >
            Gán địa bàn vào tuyến dịch vụ
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={9}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <div style={{ display: "flex" }}>
                          <SearchOutlined
                            style={{
                              fontSize: 20,
                              color: colors.svg_default,
                              paddingTop: 10,
                            }}
                          />
                          <StyledSearchCustom
                            style={{ width: 400 }}
                            suffix={() => {
                              return null;
                            }}
                            placeholder="nhập kí tự tìm kiếm"
                            onPressEnter={(e) => {
                              let keySearch = e.target.value;
                              setFilterList({
                                ...filterList,
                                key: keySearch,
                              });
                            }}
                            // prefix={() => {
                            //   return null;
                            // }}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col span={24}>
                      {useAddress((city, cities) => {
                        setLoadingList(true);
                        setFilterList({
                          ...filterList,
                          district_id: cities,
                          city_id: city,
                          page: 1,
                        });
                      })}
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          height: 350,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        {/* <InfiniteScroll
                          initialLoad={false}
                          pageStart={0}
                          loadMore={handleInfiniteOnLoad}
                          hasMore={!loadingList && hasMoreList}
                          useWindow={false}
                        > */}
                        <List
                          dataSource={[...[{}], ...listLocality]}
                          bordered={false}
                          loading={loadingList}
                          renderItem={(item, index) => {
                            return index === 0 ? (
                              <List.Item>
                                <div style={{ width: "100%", height: "100%" }}>
                                  <Row>
                                    <Col span={3}>
                                      <Checkbox
                                        checked={checkAll}
                                        onChange={(e) => {
                                          let checked = e.target.checked;
                                          setCheckAll(checked);
                                          listLocality.forEach((item) => {
                                            debugger;
                                            dispatch(
                                              ADD_LOCALITY({
                                                data: item,
                                                status: checked,
                                              })
                                            );
                                            dispatch(
                                              UPDATE_ACTIVELIST({
                                                status: checked ? 1 : 0,
                                                id: item.ward_id,
                                              })
                                            );
                                          });
                                          if (checked === false) {
                                            dispatch(NOT_CHECK_ALL());
                                          }
                                        }}
                                      />
                                    </Col>
                                    <Col span={7}>
                                      <span style={{ fontWeight: 500 }}>
                                        Phường / Xã
                                      </span>
                                    </Col>
                                    <Col span={7}>
                                      <span style={{ fontWeight: 500 }}>
                                        Quận / Huyện
                                      </span>
                                    </Col>
                                    <Col span={7}>
                                      <span style={{ fontWeight: 500 }}>
                                        Tỉnh / Thành
                                      </span>
                                    </Col>
                                  </Row>
                                </div>
                              </List.Item>
                            ) : (
                              <List.Item>
                                <div style={{ width: "100%", height: "100%" }}>
                                  <Row>
                                    <Col span={3}>
                                      <Checkbox
                                        defaultChecked={item.status === 1}
                                        checked={item.status === 1}
                                        onChange={(e) => {
                                          let checked = e.target.checked;
                                          if (!checked) {
                                            setCheckAll(false);
                                          }
                                          dispatch(
                                            ADD_LOCALITY({
                                              data: item,
                                              status: checked,
                                            })
                                          );
                                          dispatch(
                                            UPDATE_ACTIVELIST({
                                              status: checked ? 1 : 0,
                                              id: item.ward_id,
                                            })
                                          );
                                        }}
                                      />
                                    </Col>
                                    <Col span={7}>
                                      <span>{item.ward_name}</span>
                                    </Col>
                                    <Col span={7}>
                                      <span>{item.district_name}</span>
                                    </Col>
                                    <Col span={7}>
                                      <span>{item.city_name}</span>
                                    </Col>
                                  </Row>
                                </div>
                              </List.Item>
                            );
                          }}
                        />
                        {/* {loadingList && hasMoreList && (
                            <div
                              style={{
                                position: "absolute",
                                bottom: 40,
                                width: "100%",
                                textAlign: "center",
                              }}
                            >
                              <Spin />
                            </div>
                          )} */}
                        {/* </InfiniteScroll> */}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          color={colors.main}
                          title="Thêm"
                          icon={ISvg.NAME.BUTTONRIGHT}
                          minWidth="150px"
                          isRight={true}
                          onClick={() => {
                            if (listLocalityAddClone.length === 0) {
                              message.error("Chưa chọn địa bàn");
                              return;
                            }
                            if (listLocalityShow.length > 0) {
                              let district = listLocalityShow[0].district_name;
                              if (
                                district !==
                                listLocalityAddClone[
                                  listLocalityAddClone.length - 1
                                ].district_name
                              ) {
                                message.error(
                                  "Vui lòng chọn địa bàn cùng Quận/Huyện, Tỉnh/Thành phố"
                                );
                                return;
                              }
                            }

                            dispatch(SAVE_LOCALITY());
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={15}>
                <div
                  style={{
                    height: 512,
                    borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                    borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <ITableHtml
                    childrenBody={bodyTableProduct(listLocalityShow)}
                    childrenHeader={headerTableProduct(headerTable)}
                    isBorder={false}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <div
        style={{ position: "absolute", bottom: -55, right: 0, display: "flex" }}
      >
        <IButton
          color={colors.main}
          title="Lưu"
          width={20}
          height={20}
          icon={ISvg.NAME.SAVE}
          minWidth="150px"
          onClick={() => {
            let error = 0;
            listLocalityShow.map((item) => {
              if (!item.frequency_id) {
                error = error + 1;
                return;
              }
            });
            if (error !== 0) {
              message.error("Chưa chọn tuần suất");
              return;
            }
            dispatch(CONFIRM_LOCALITY());
            callbackClose();
          }}
        />
        <IButton
          color={colors.oranges}
          title="Hủy bỏ"
          width={16}
          height={16}
          style={{ marginLeft: 15 }}
          icon={ISvg.NAME.CROSS}
          minWidth="150px"
          onClick={() => {
            dispatch(CANCEL_LOCALITY());
            setCheckAll(false);
            callbackClose();
          }}
        />
      </div>
    </div>
  );
}
