import { Col, Row, Tooltip, message, Popconfirm } from "antd";
import React, { useEffect, useState } from "react";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import {
  ISearch,
  ISelect,
  ISvg,
  ITable,
  ITitle,
  IButton,
} from "../../../components";
import { StyledITitle } from "../../../components/common/Font/font";
import { useHistory } from "react-router";
import { CLEAR_REDUX_LOCALITY } from "../../../store/reducers";
import { useDispatch } from "react-redux";

export default function ListServiceRoute() {
  const history = useHistory();
  const dispatch = useDispatch();
  const [loadingTable, setLoadingTable] = useState(true);
  const [dataTable, setDataTable] = useState({});
  const [filterTable, setFilterTable] = useState({
    agency_id: 0,
    key: "",
    page: 1,
  });
  const [dataBranch, setDataBranch] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loadingRemove, setLoadingRemove] = useState(false);

  const _fetchAPIListServiceSales = async (filter) => {
    try {
      const data = await APIService._getListServiceRoute(
        filter.key,
        filter.agency_id,
        filter.page
      );
      data.route_dvnn.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
          agencyId: item.agencyId,
        });
      });
      setDataTable(data);
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  const _getAPIListBranch = async () => {
    try {
      const data = await APIService._getBranchDropList();
      const dataNew = data.agency.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDataBranch(dataNew);
    } catch (error) {}
  };

  const fetchRemoveRouter = async (obj) => {
    try {
      const data = await APIService._postRemoveRouterService(obj);
      message.success("Xóa thành công");
      setSelectedRowKeys([]);
      setLoadingRemove(false);
      setLoadingTable(true);
      _fetchAPIListServiceSales(filterTable);
    } catch (error) {
      console.log(error);
      setLoadingRemove(false);
    }
  };

  useEffect(() => {
    _getAPIListBranch();
  }, []);

  useEffect(() => {
    _fetchAPIListServiceSales(filterTable);
  }, [filterTable]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Mã tuyến",
      dataIndex: "route_code",
      key: "route_code",
    },
    {
      title: "Tên tuyến",
      dataIndex: "route_name",
      key: "route_name",
    },
    // {
    //   title: 'Phòng kinh doanh',
    //   dataIndex: 'agency_name',
    //   key: 'agency_name'
    // },
    {
      title: "Số S3 trong tuyến",
      dataIndex: "number_route_region",
      key: "number_route_region",
    },
    {
      title: "Mã nhân viên",
      dataIndex: "sale_code",
      key: "sale_code",
    },

    {
      title: "Nhân viên DVNN",
      dataIndex: "sale_name",
      key: "sale_name",
    },
    {
      title: "Số điện thoại",
      dataIndex: "sale_phone",
      key: "sale_phone",
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
    },
    // {
    //   align: "right",
    //   width: 80,
    //   render: (obj) => {
    //     return (
    //       <div style={{ width: 40 }}>
    //         <Tooltip title="Chi tiết tuyến">
    //           <div
    //             id="nextDetail"
    //             className="cursor-push"
    //             onClick={(e) => {
    //               e.stopPropagation();
    //               history.push(`/serviceSalesman/detail/${obj.id}`);
    //             }}
    //           >
    //             <ISvg
    //               name={ISvg.NAME.CHEVRONRIGHT}
    //               width={11}
    //               height={20}
    //               fill={colors.icon.default}
    //             />
    //           </div>
    //         </Tooltip>
    //       </div>
    //     );
    //   },
    // },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Quản lý tuyến dịch vụ
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã tuyến, tên tuyến, mã nhân viên, nhân viên bán hàng">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo mã tuyến, tên tuyến, mã nhân viên, nhân viên bán hàng"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    key: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={16}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginLeft: 25,
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.LOCATION}
                    with={16}
                    height={20}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Phòng kinh doanh"
                    level={4}
                    style={{
                      marginLeft: 25,
                      marginRight: 25,
                    }}
                  />

                  <div style={{ width: 200 }}>
                    <ISelect
                      defaultValue="Phòng kinh doanh"
                      data={dataBranch}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          agency_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>
              <Col span={8}>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Tạo mới"
                    color={colors.main}
                    icon={ISvg.NAME.ARROWUP}
                    styleHeight={{
                      width: 140,
                      marginRight: 15,
                    }}
                    isRight={true}
                    onClick={() => {
                      dispatch(CLEAR_REDUX_LOCALITY());
                      history.push(`/route/service/create/0`);
                    }}
                  />

                  <Popconfirm
                    placement="bottom"
                    title="Nhấn đồng ý để xóa"
                    okText="Đồng ý"
                    cancelText="Hủy"
                    onConfirm={() => {
                      if (selectedRowKeys.length === 0) {
                        message.error("Chưa chọn tuyến để xóa");
                        return;
                      }
                      setLoadingRemove(true);
                      const arrId = selectedRowKeys.map((item) => {
                        const itemParse = JSON.parse(item);
                        return itemParse.id;
                      });

                      const objRemove = {
                        id: arrId,
                      };

                      fetchRemoveRouter(objRemove);
                    }}
                  >
                    <IButton
                      color={colors.oranges}
                      title="Xóa"
                      icon={ISvg.NAME.DELETE}
                      styleHeight={{ width: 140 }}
                      loading={loadingRemove}
                    />
                  </Popconfirm>
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={dataTable.route_dvnn}
                style={{ width: "100%" }}
                defaultCurrent={1}
                sizeItem={dataTable.size}
                rowKey="rowTable"
                indexPage={filterTable.page}
                maxpage={dataTable.total}
                loading={loadingTable}
                rowSelection={rowSelection}
                onRow={(record, rowIndex) => {
                  return {
                    onClick: (event) => {
                      const rowTable = Number(
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey
                      );

                      const item = JSON.parse(rowTable);

                      history.push(
                        `/serviceSalesman/detail/${item.agencyId}/${item.id}`
                      );
                    },
                    onMouseDown: (event) => {
                      const rowTable = Number(
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey
                      );

                      const item = JSON.parse(rowTable);

                      if (event.button == 2 || event == 4) {
                        window.open(
                          `/serviceSalesman/detail/${item.agencyId}/${item.id}`,
                          "_blank"
                        );
                      }
                    },
                  };
                }}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilterTable({ ...filterTable, page: page });
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}
