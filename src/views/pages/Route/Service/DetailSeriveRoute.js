import { BackTop, Col, Empty, message, Row, Skeleton, Popconfirm } from "antd";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";
import { ISvg, IButton } from "../../../components";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";

export default function DetailSeriveRoute() {
  const history = useHistory();
  const params = useParams();

  const [isLoading, setLoading] = useState(true);
  const [dataDetail, setDataDetail] = useState({ routeDvnnRegion: [] });
  const { id } = params;

  const fetchAPIDetail = async (router_id) => {
    try {
      const data = await APIService._getDetailServiceRoute(router_id);
      setDataDetail(data.route_dvnn);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  const [loadingRemove, setLoadingRemove] = useState(false);
  const fetchRemoveRouter = async (obj) => {
    try {
      const data = await APIService._postRemoveRouterService(obj);
      message.success("Xóa thành công");
      setLoadingRemove(false);
      await fetchAPIDetail(id);
    } catch (error) {
      console.log(error);
      setLoadingRemove(false);
    }
  };

  useEffect(() => {
    fetchAPIDetail(id);
  }, []);

  return (
    <div style={{ padding: "18px 0px 0px 0px", width: "100%" }}>
      <Row gutter={[0, 20]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main, marginTop: 8 }}>
              Chi tiết tuyến dịch vụ
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div className="flex justify-end">
            <IButton
              color={colors.main}
              title="Chỉnh sửa"
              icon={ISvg.NAME.WRITE}
              style={{ width: 140, marginRight: 15 }}
              onClick={() => {
                history.push(`/route/sales/edit/${id}`);
              }}
            />
            <Popconfirm
              placement="bottom"
              title="Nhấn đồng ý để xóa"
              okText="Đồng ý"
              cancelText="Hủy"
              onConfirm={() => {
                const objRemove = {
                  id: [Number(id)],
                };
                setLoadingRemove(true);
                fetchRemoveRouter(objRemove);
              }}
            >
              <IButton
                color={colors.oranges}
                title="Xóa"
                icon={ISvg.NAME.DELETE}
                styleHeight={{ width: 140 }}
                loading={loadingRemove}
              />
            </Popconfirm>
          </div>
        </Col>
      </Row>

      <BackTop description={1000} />

      <div style={{ margin: "32px 0px" }}>
        <div
          style={{
            margin: "32px 0px 12px 0px",
            width: 815,
            background: "white",
            boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
          }}
        >
          <div>
            <Row gutter={[24, 0]}>
              <Col span={16}>
                <div
                  style={{
                    padding: "30px 40px 120px 40px",
                    borderRight: "1px solid #D8DBDC",
                  }}
                >
                  <Skeleton loading={isLoading} active paragraph={{ rows: 12 }}>
                    <Row gutter={[0, 24]}>
                      <Col span={24}>
                        <StyledITileHeading
                          style={{ marginTop: 8 }}
                          minFont="12px"
                          maxFont="16px"
                        >
                          Thông tin tuyến
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <div style={{ marginTop: 12 }}>
                          <p style={{ fontWeight: 500 }}>Tên tuyến</p>
                          <span>{dataDetail.route_name}</span>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 500 }}>Trạng thái</p>
                                <span>{dataDetail.route_status}</span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 500 }}>Mã tuyến</p>
                                <span>{dataDetail.route_code}</span>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 500 }}>
                                  Phòng kinh doanh
                                </p>
                                <span
                                  style={{
                                    textTransform: "capitalize",
                                  }}
                                >
                                  {dataDetail.agency_under}
                                </span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 500 }}>
                                  Nhân viên phụ trách
                                </p>
                                <span>{dataDetail.sale_name}</span>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <Row>
                          <Col span={20}>
                            <StyledITileHeading
                              style={{ marginTop: 12 }}
                              minFont="12px"
                              maxFont="16px"
                            >
                              Danh sách Địa bàn
                            </StyledITileHeading>
                          </Col>
                          <Col span={4}>
                            <div
                              style={{
                                height: 45,
                                display: "flex",
                                justifyContent: "flex-end",
                              }}
                            >
                              <ISvg
                                name={ISvg.NAME.VIEWLIST}
                                width={20}
                                height={18}
                                fill={colors.icon.default}
                              />
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      {dataDetail.routeDvnnRegion.length === 0 ? (
                        <div
                          style={{
                            width: "100%",
                            textAlign: "center",
                            marginTop: 12,
                          }}
                        >
                          <Empty description="Chưa có danh sách địa bàn" />
                        </div>
                      ) : (
                        <Col span={24}>
                          <div>
                            <Row gutter={[12, 0]}>
                              <Col span={6}>
                                <span style={{ fontWeight: 500 }}>
                                  Phường / Xã
                                </span>
                              </Col>
                              <Col span={6}>
                                <span style={{ fontWeight: 500 }}>
                                  Quận / Huyện
                                </span>
                              </Col>
                              <Col span={6}>
                                <span style={{ fontWeight: 500 }}>
                                  Tỉnh / Thành
                                </span>
                              </Col>
                              <Col span={6}>
                                <span style={{ fontWeight: 500 }}>
                                  Tuần suất
                                </span>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      )}
                      {dataDetail.routeDvnnRegion.map((item) => {
                        return (
                          <Col
                            span={24}
                            style={{ paddingTop: 4, paddingBottom: 4 }}
                          >
                            <div>
                              <Row gutter={[12, 0]}>
                                <Col span={6}>
                                  <span>{item.ward_name}</span>
                                </Col>
                                <Col span={6}>
                                  <span>{item.district_name}</span>
                                </Col>
                                <Col span={6}>
                                  <span>{item.city_name}</span>
                                </Col>
                                <Col span={6}>
                                  <span style={{ fontWeight: 500 }}>
                                    {item.frequency_code}
                                  </span>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        );
                      })}
                    </Row>
                  </Skeleton>
                </div>
              </Col>
              <Col span={8}>
                <div
                  style={{
                    padding: "30px 12px",
                  }}
                >
                  <Skeleton loading={isLoading} active paragraph={{ rows: 12 }}>
                    <Row gutter={[0, 24]}>
                      <Col span={24}>
                        <StyledITileHeading
                          style={{ marginTop: 8 }}
                          minFont="12px"
                          maxFont="16px"
                        >
                          Thời gian hiệu lực
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <div style={{ marginTop: 12 }}>
                          <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                          <span>
                            {FormatterDay.dateFormatWithString(
                              dataDetail.start_date,
                              "#hhh#:#mm#:#ss# #DD#-#MM#-#YYYY#"
                            )}
                          </span>
                        </div>
                      </Col>

                      <Col span={24}>
                        <div>
                          <p style={{ fontWeight: 500 }}>Kết thúc</p>
                          <span>
                            {FormatterDay.dateFormatWithString(
                              dataDetail.end_date,
                              "#hhh#:#mm#:#ss# #DD#-#MM#-#YYYY#"
                            )}
                          </span>
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </div>
  );
}
