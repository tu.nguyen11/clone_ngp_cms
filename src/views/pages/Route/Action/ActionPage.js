import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton, Tooltip, message } from "antd";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IDatePicker,
  IButton,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import { useHistory } from "react-router-dom";
import FormatterDay from "../../../../utils/FormatterDay";

const widthScreen = window.innerWidth;
const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

function ActionPage(props) {
  const history = useHistory();
  const [dataTable, setDataTable] = useState([]);
  const [isLoadingTable, setLoadingTable] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [pagination, setPagination] = useState({
    total: 0,
    sizeItem: 0,
  });

  const [dataListRoute, setDataListRoute] = useState([]);

  const [filter, setFilter] = useState({
    id_route_dvnn: 0,
    page: 1,
    from_date: startValue,
    to_date: NewDate.setHours(23, 59, 59, 999),
    key: "",
    agency_id: 0,
  });

  const _getAPIListAction = async (filter) => {
    try {
      const data = await APIService._getListAction(filter);
      const newData = data.data.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        return item;
      });
      setDataTable(newData);
      setIsLoading(false);
      setPagination({
        ...pagination,
        total: data.total,
        sizeItem: data.size,
      });

      setLoadingTable(false);
    } catch (error) {
      message.error(error);
      setIsLoading(false);
      setLoadingTable(false);
    }
  };

  const getBranchDropList = async () => {
    try {
      const data = await APIService._getBranchDropList();
      const dataNew = data.agency.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      dataNew.unshift({ key: 0, value: "Tất cả" });
      setDataListRoute(dataNew);
    } catch (error) {}
  };

  useEffect(() => {
    _getAPIListAction(filter);
  }, [filter]);

  useEffect(() => {
    getBranchDropList();
  }, []);

  const onChangePage = (page) => {
    setFilter({ ...filter, page: page }, [filter]);
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      width: 80,
      align: "center",
    },
    {
      title: "Tên tác vụ",
      dataIndex: "name_service",
      key: "name_service",
    },
    {
      title: "Loại dữ liệu",
      dataIndex: "type_service",
      key: "type_service",
    },
    {
      title: "Ngày bắt đầu",
      dataIndex: "start_date",
      key: "start_date",
      render: (start_date) => {
        return (
          <span>
            {FormatterDay.dateFormatWithString(start_date, "#DD#/#MM#/#YYYY#")}
          </span>
        );
      },
    },
    {
      title: "Ngày kết thúc",
      dataIndex: "end_date",
      key: "end_date",
      render: (end_date) => {
        return (
          <span>
            {FormatterDay.dateFormatWithString(end_date, "#DD#/#MM#/#YYYY#")}
          </span>
        );
      },
    },
    {
      title: "Phòng kinh doanh",
      dataIndex: "route_name",
      key: "route_name",
    },
    {
      title: "Vùng áp dụng",
      dataIndex: "region_name",
      key: "region_name",
      align: "center",
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      align: "center",
    },
    {
      align: "right",
      width: "80px",
      fixed: widthScreen <= 1440 ? "right" : "",

      render: (item) => {
        return (
          <div style={{ width: 40 }}>
            <Tooltip title="Chi tiết tác vụ">
              <div
                id="nextDetail"
                className="cursor-push"
                onClick={(e) => {
                  e.stopPropagation();
                  history.push(`/route/action/detail/` + item.id_service);
                }}
              >
                <ISvg
                  width={10}
                  height={20}
                  name={ISvg.NAME.CHEVRONRIGHT}
                  fill={colors.icon.default}
                />
              </div>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <div>
        <Row gutter={[0, 24]}>
          <Col span={24}>
            <Row>
              <Col span={12}>
                <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                  Quản lý Tác vụ
                </StyledITitle>
              </Col>
              <Col span={12}>
                <div className="flex justify-end">
                  <Tooltip title="Tìm kiếm theo tên tác vụ">
                    <ISearch
                      className="cursor"
                      placeholder="Tìm kiếm theo tên tác vụ"
                      onPressEnter={(e) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          key: e.target.value,
                          page: 1,
                        });
                      }}
                      icon={
                        <div
                          style={{
                            display: "flex",
                            width: 42,
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <img
                            src={images.icSearch}
                            style={{ width: 16, height: 16 }}
                            alt=""
                          />
                        </div>
                      }
                    />
                  </Tooltip>
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Col span={21}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <ISvg
                  name={ISvg.NAME.EXPERIMENT}
                  width={16}
                  height={16}
                  fill={colors.icon.default}
                />
                <ITitle
                  title="Từ ngày"
                  level={4}
                  style={{
                    marginLeft: 25,
                    marginRight: 25,
                  }}
                />
                <div style={{ marginRight: 25 }}>
                  <IDatePicker
                    isBackground
                    from={filter.from_date}
                    to={filter.to_date}
                    onChange={(date, dateString) => {
                      if (date.length <= 1) {
                        return;
                      }

                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        from_date: date[0]._d.setHours(0, 0, 0),
                        to_date: date[1]._d.setHours(23, 59, 59),
                        page: 1,
                      });
                    }}
                    style={{
                      background: colors.white,
                      borderWidth: 1,
                      borderColor: colors.line,
                      borderStyle: "solid",
                    }}
                  />
                </div>
                <ISvg
                  name={ISvg.NAME.LOCATION}
                  width={20}
                  height={20}
                  fill={colors.icon.default}
                />
                <ITitle
                  title="Khu vực"
                  level={4}
                  style={{
                    marginLeft: 20,
                    marginRight: 20,
                  }}
                />
                <div style={{ width: 150, margin: "0px 10px 0px 15px" }}>
                  <ISelect
                    defaultValue="Tất cả"
                    data={dataListRoute}
                    select={true}
                    onChange={(value) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        agency_id: value,
                        page: 1,
                      });
                    }}
                  />
                </div>
              </div>
            </Col>
            <Col span={3}>
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Tạo mới"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWUP}
                  styleHeight={{
                    width: 140,
                  }}
                  isRight={true}
                  onClick={() => {
                    history.push(`/route/action/create/0`);
                  }}
                />
              </div>
            </Col>
          </Col>
        </Row>

        <Row style={{ marginTop: 36 }}>
          <ITable
            data={dataTable}
            columns={columns}
            style={{ width: "100%" }}
            rowKey="id_service"
            defaultCurrent={1}
            loading={isLoadingTable}
            maxpage={pagination.total}
            sizeItem={pagination.sizeItem}
            indexPage={filter.page}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );
                  history.push(`/route/action/detail/` + indexRow);
                }, // click row
                onMouseDown: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );

                  if (event.button == 2 || event == 4) {
                    window.open(`/route/action/detail/` + indexRow, "_blank");
                  }
                },
              };
            }}
            callbackPagination={(page) => {
              setLoadingTable(true);
              onChangePage(page);
            }}
          />
        </Row>
      </div>
    </div>
  );
}

export default ActionPage;
