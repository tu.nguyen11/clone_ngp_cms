import { Col, message, Row, Form, Skeleton } from "antd";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import moment from "moment";

import {
  IInputText,
  IDatePickerFrom,
  IFormItem,
} from "../../../components/common";
import { IButton, ISelect, ISvg, ITitle } from "../../../components";
import { StyledITileHeading } from "../../../components/common/Font/font";

let timer = null;
const NewDate = new Date();

function CreateActionPage(props) {
  const history = useHistory();
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;
  const params = useParams();
  const { id, type } = params;
  const [isEdit, setEdit] = useState(type == "create" ? false : true);
  const [key, setKeySearch] = useState("");
  const [dataDetail, setDataDetail] = useState({});
  const [dataInfoAction, setDataInfoAction] = useState([]);
  const [dataListRoute, setDataListRoute] = useState([]);

  const [render, setRender] = useState(true);
  const [isLoading, setLoading] = useState(type == "create" ? false : true);

  const [list, setList] = useState(true);

  const _postAPIAddAction = async (obj) => {
    try {
      const data = await APIService._postAddAction(obj);
      message.success("Tạo tác vụ thành công");
      history.goBack();
    } catch (error) {}
  };

  const _postAPIUpdateAction = async (obj) => {
    try {
      console.log(JSON.stringify(obj));
      const data = await APIService._postUpdateAction(obj);
      message.success("Chỉnh sửa tác vụ thành công");
      history.goBack();
    } catch (error) {}
  };

  const _getAPIListRoute = async () => {
    try {
      const data = await APIService._getListRoute();
      const dataNew = data.data.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      setDataListRoute(dataNew);
    } catch (error) {}
  };

  const disabledEndDateApply = (endValue) => {
    let a = reduxForm.getFieldValue("start_date");

    if (!endValue || !reduxForm.getFieldValue("start_date")) {
      return false;
    }
    return endValue.valueOf() <= reduxForm.getFieldValue("start_date");
  };

  const disabledStartDateApply = (startValue) => {
    if (!startValue || !NewDate) {
      return false;
    }
    return startValue.valueOf() < NewDate.valueOf();
  };

  const _getAPIDetailAction = async () => {
    try {
      const data = await APIService._getDetailTacVu(id);
      let dataNew = [];
      data.data.list_action_detail.map((item) => {
        const obj = {
          valid: true,
        };
        obj.min_length = item.min_lenght_value;
        obj.max_length = item.max_lenght_value;
        obj.name = item.name;
        obj.type = item.type;
        dataNew.push(obj);
      });
      reduxForm.setFieldsValue({
        title: data.data.action_name,
        status: data.data.status,
        route_dvnn_id: data.data.route_dvnn_id,
        start_date: data.data.start_date,
        end_date: data.data.end_date,
      });
      setDataDetail(data.data);
      setDataInfoAction(dataNew);

      setLoading(false);
    } catch (error) {
      setLoading(false);
      message.err(error);
    }
  };

  async function _getAPI() {
    await _getAPIListRoute();
    if (isEdit) {
      await _getAPIDetailAction();
    }
  }

  useEffect(() => {
    _getAPI();
  }, []);

  useEffect(() => {
    setList(true);
  }, [dataInfoAction]);

  useEffect(() => {
    _getAPIListRoute(key);
  }, [key]);

  const onFinish = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        const addActionRequest = {
          end_date: values.end_date,
          route_dvnn_id: values.route_dvnn_id,
          start_date: values.start_date,
          status: values.status,
          title: values.title,
          valid: true,
        };

        let lstInfo = dataInfoAction;

        const obj = {
          addActionRequest,
          lstInfo,
          action_id: isEdit ? id : 0,
        };

        console.log(obj);

        isEdit ? _postAPIUpdateAction(obj) : _postAPIAddAction(obj);
      }
    });
  };

  return (
    <div
      style={{
        width: "100%",
      }}
    >
      <Form onSubmit={(event) => onFinish(event)}>
        <Row>
          <Col flex="auto">
            <div>
              <ITitle
                level={1}
                title={isEdit ? "Chỉnh sửa tác vụ" : "Tạo tác vụ mới"}
                style={{
                  color: colors.mainDark,
                  fontWeight: "bold",
                  marginTop: 14,
                }}
              />
            </div>
          </Col>
        </Row>

        <Row style={{ margin: "16px 0px" }}>
          <Col span={24}>
            <div className="flex justify-end">
              <IButton
                color={colors.main}
                title="Lưu"
                icon={ISvg.NAME.SAVE}
                styleHeight={{ width: 140 }}
                htmlType="submit"
              />

              <IButton
                title="Hủy"
                styleHeight={{ width: 140, marginLeft: 15 }}
                color={colors.oranges}
                icon={ISvg.NAME.CROSS}
                onClick={() => history.goBack()}
              />
            </div>
          </Col>
        </Row>

        <Row>
          <Col flex="auto">
            <Row>
              <Col
                style={{
                  backgroundColor: "white",
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  width: 550,
                }}
              >
                <div style={{ padding: "31px 40px" }}>
                  <Skeleton paragraph={{ rows: 16 }} active loading={isLoading}>
                    <Row gutters={[0, 24]}>
                      <Col span={24}>
                        <StyledITileHeading
                          style={{ marginTop: 8 }}
                          minFont="12px"
                          maxFont="16px"
                        >
                          Thông tin công việc
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <div style={{ marginTop: 25 }}>
                          <p style={{ fontWeight: 500 }}>
                            Tên tác vụ
                            <span style={{ marginLeft: 5, color: "red" }}>
                              *
                            </span>
                          </p>
                          <IFormItem>
                            {getFieldDecorator("title", {
                              rules: [
                                {
                                  required: true,
                                  message: "nhập tên tác vụ",
                                },
                              ],
                            })(
                              <IInputText
                                placeholder="nhập tên tác vụ"
                                value={reduxForm.getFieldValue("title")}
                              />
                            )}
                          </IFormItem>
                        </div>
                      </Col>
                      <Col span={24} style={{ marginTop: 25 }}>
                        <Row>
                          <Col span={11}>
                            <div>
                              <p style={{ fontWeight: 500 }}>
                                Trạng thái
                                <span style={{ marginLeft: 5, color: "red" }}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("status", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "chọn trạng thái",
                                    },
                                  ],
                                })(
                                  <div>
                                    <ISelect
                                      className="clear-pad"
                                      placeholder="chọn trạng thái"
                                      data={[
                                        { key: 1, value: "Đang hoạt động" },
                                        { key: 0, value: "Tạm ngưng" },
                                      ]}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                      }}
                                      type="write"
                                      select={true}
                                      isBackground={false}
                                      onChange={(key, option) => {
                                        reduxForm.setFieldsValue({
                                          status: Number(option.key),
                                        });
                                      }}
                                      value={reduxForm.getFieldValue("status")}
                                    />
                                  </div>
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                          <Col span={2}></Col>

                          <Col span={11}>
                            <div>
                              <p style={{ fontWeight: 500 }}>
                                Tuyến áp dụng
                                <span style={{ marginLeft: 5, color: "red" }}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("route_dvnn_id", {
                                  rules: [
                                    {
                                      required: false,
                                      message: "chọn tuyến áp dụng",
                                    },
                                  ],
                                })(
                                  <div>
                                    <ISelect
                                      className="clear-pad"
                                      placeholder="chọn tuyến áp dụng"
                                      data={dataListRoute}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                      }}
                                      type="write"
                                      select={true}
                                      isBackground={false}
                                      onChange={(key, option) => {
                                        reduxForm.setFieldsValue({
                                          route_dvnn_id: Number(option.key),
                                        });
                                      }}
                                      value={reduxForm.getFieldValue(
                                        "route_dvnn_id"
                                      )}
                                    />
                                  </div>
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24} style={{ marginTop: 25 }}>
                        <Row>
                          <Col span={11}>
                            <div>
                              <p style={{ fontWeight: 500 }}>
                                Từ ngày
                                <span style={{ marginLeft: 5, color: "red" }}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("start_date", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "nhập thời gian bắt đầu",
                                    },
                                  ],
                                })(
                                  <div
                                    style={{
                                      borderBottom: "1px solid #d8dbdc",
                                    }}
                                  >
                                    <IDatePickerFrom
                                      bordered={false}
                                      paddingInput="0px"
                                      style={{ marginTop: 3 }}
                                      padding="0px 0px 4px 0px"
                                      disabledDate={disabledStartDateApply}
                                      onChange={(date, dateString) => {
                                        let timeStart = date._d.getTime();
                                        reduxForm.setFieldsValue({
                                          start_date: timeStart,
                                        });
                                      }}
                                      showTime
                                      showToday={false}
                                      defaultValue={
                                        isEdit
                                          ? moment(
                                              new Date(
                                                reduxForm.getFieldValue(
                                                  "start_date"
                                                )
                                              ),
                                              "hh:mm:ss DD-MM-YYYY"
                                            )
                                          : undefined
                                      }
                                      disabled={isEdit}
                                      placeholder="nhập thời gian bắt đầu"
                                    />
                                  </div>
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                          <Col span={2}></Col>
                          <Col span={11}>
                            <div>
                              <p style={{ fontWeight: 500 }}>
                                Đến ngày
                                <span style={{ marginLeft: 5, color: "red" }}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("end_date", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "nhập thời gian kết thúc",
                                    },
                                  ],
                                })(
                                  <div
                                    style={{
                                      borderBottom: "1px solid #d8dbdc",
                                    }}
                                  >
                                    <IDatePickerFrom
                                      bordered={false}
                                      paddingInput="0px"
                                      style={{ marginTop: 3 }}
                                      disabledDate={disabledEndDateApply}
                                      onChange={(date, dateString) => {
                                        let timeStart = date._d.getTime();
                                        reduxForm.setFieldsValue({
                                          end_date: timeStart,
                                        });
                                      }}
                                      showTime
                                      showToday={false}
                                      defaultValue={
                                        isEdit
                                          ? moment(
                                              new Date(
                                                reduxForm.getFieldValue(
                                                  "end_date"
                                                )
                                              ),
                                              "hh:mm:ss DD-MM-YYYY"
                                            )
                                          : undefined
                                      }
                                      padding="0px 0px 4px 0px"
                                      disabled={isEdit}
                                      placeholder="nhập thời gian kết thúc"
                                    />
                                  </div>
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <div style={{ paddingTop: 25 }}>
                      <StyledITileHeading
                        style={{ marginTop: 8 }}
                        minFont="12px"
                        maxFont="16px"
                      >
                        Thông tin Tác vụ
                        {/* <span style={{ marginLeft: 5, color: "red" }}>*</span> */}
                      </StyledITileHeading>
                    </div>
                    {list
                      ? dataInfoAction.map((item, index) => {
                          return (
                            <div>
                              <div
                                style={{
                                  borderBottom: "1px solid #DADADA",
                                  paddingBottom: 20,
                                }}
                              >
                                <Row style={{ paddingTop: 25 }}>
                                  <Col span={11}>
                                    <div>
                                      <p style={{ fontWeight: 500 }}>
                                        Thông tin
                                        {/* <span
                                          style={{
                                            marginLeft: 5,
                                            color: "red",
                                          }}
                                        >
                                          *
                                        </span> */}
                                      </p>
                                      <IFormItem>
                                        {/* {getFieldDecorator('name', {
                                          rules: [
                                            {
                                              message:
                                                'Vui lòng không để trống!'
                                            }
                                          ]
                                        })( */}
                                        <IInputText
                                          placeholder="nhập thông tin"
                                          onChange={(e) => {
                                            item.name = e.target.value;
                                          }}
                                          defaultValue={item.name}
                                        />
                                        {/* )} */}
                                      </IFormItem>
                                    </div>
                                  </Col>

                                  <Col span={2}></Col>

                                  <Col span={11}>
                                    <div>
                                      <p style={{ fontWeight: 500 }}>
                                        Loại dữ liệu
                                        {/* <span
                                          style={{
                                            marginLeft: 5,
                                            color: "red",
                                          }}
                                        >
                                          *
                                        </span> */}
                                      </p>
                                      <IFormItem>
                                        {getFieldDecorator("type", {
                                          rules: [
                                            {
                                              required: false,
                                              message: "chọn loại dữ liệu",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <ISelect
                                              className="clear-pad"
                                              placeholder="chọn loại dữ liệu"
                                              data={[
                                                { key: 2, value: "Số" },
                                                { key: 1, value: "Chữ" },
                                                { key: 3, value: "Đoạn văn" },
                                              ]}
                                              style={{
                                                background: colors.white,
                                                borderStyle: "solid",
                                                borderWidth: 0,
                                                borderColor: colors.line,
                                                borderBottomWidth: 1,
                                              }}
                                              type="write"
                                              select={true}
                                              isBackground={false}
                                              onChange={(key, option) => {
                                                item.type = key;

                                                reduxForm.setFieldsValue({
                                                  type: key,
                                                });
                                                setRender(!render);
                                              }}
                                              value={
                                                !item.type
                                                  ? undefined
                                                  : item.type
                                              }
                                            />
                                          </div>
                                        )}
                                      </IFormItem>
                                    </div>
                                  </Col>
                                </Row>
                                <Row style={{ paddingTop: 25 }}>
                                  <Col span={11}>
                                    <div>
                                      <p style={{ fontWeight: 500 }}>
                                        Tối thiểu
                                        {/* <span
                                          style={{
                                            marginLeft: 5,
                                            color: "red",
                                          }}
                                        >
                                          *
                                        </span> */}
                                      </p>
                                      <IFormItem>
                                        {getFieldDecorator("min_length", {
                                          rules: [
                                            {
                                              required: false,
                                              message:
                                                "Vui lòng không để trống!",
                                            },
                                          ],
                                        })(
                                          <div
                                            style={{
                                              display: "flex",
                                              flexDirection: "row",
                                            }}
                                          >
                                            <IInputText
                                              onChange={(e) => {
                                                item.min_length =
                                                  e.target.value;
                                              }}
                                              placeholder="nhập tối thiểu"
                                              type="number"
                                              max={item.max_length}
                                              min={0}
                                              defaultValue={item.min_length}
                                            />
                                            <div
                                              style={{
                                                width: 45,
                                                alignItems: "center",
                                                display: "flex",
                                                paddingLeft: 5,
                                              }}
                                            >
                                              <span style={{ color: "black" }}>
                                                Ký tự
                                              </span>
                                            </div>
                                          </div>
                                        )}
                                      </IFormItem>
                                    </div>
                                  </Col>
                                  <Col span={2}></Col>

                                  <Col span={11}>
                                    <div>
                                      <p style={{ fontWeight: 500 }}>
                                        Tối đa
                                        {/* <span
                                          style={{
                                            marginLeft: 5,
                                            color: "red",
                                          }}
                                        >
                                          *
                                        </span> */}
                                      </p>
                                      <IFormItem>
                                        {getFieldDecorator("max_length", {
                                          rules: [
                                            {
                                              required: false,
                                              message:
                                                "Vui lòng không để trống!",
                                            },
                                          ],
                                        })(
                                          <div
                                            style={{
                                              display: "flex",
                                              flexDirection: "row",
                                            }}
                                          >
                                            <IInputText
                                              onChange={(e) => {
                                                item.max_length =
                                                  e.target.value;
                                                setRender(!render);
                                              }}
                                              placeholder="nhập tối đa"
                                              type="number"
                                              defaultValue={item.max_length}
                                            />
                                            <div
                                              style={{
                                                width: 45,
                                                alignItems: "center",
                                                display: "flex",
                                                paddingLeft: 5,
                                              }}
                                            >
                                              <span style={{ color: "black" }}>
                                                Ký tự
                                              </span>
                                            </div>
                                          </div>
                                        )}
                                      </IFormItem>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col
                                    span={24}
                                    style={{
                                      display: "flex",
                                      justifyContent: "flex-end",
                                      paddingTop: 23,
                                    }}
                                  >
                                    {/* <IButton
                                color={colors.main}
                                label="Xác nhận"
                                width={20}
                                height={15}
                                icon={svgs.IconCheckMark}
                                minWidth="150px"
                              /> */}
                                    <IButton
                                      color={colors.oranges}
                                      title="Xóa"
                                      style={{ width: 140 }}
                                      icon={ISvg.NAME.DELETE}
                                      onClick={() => {
                                        setList(false);
                                        let newData = [...dataInfoAction];
                                        newData.splice(index, 1); // xóa 1 phần tử từ vị trí index;
                                        setDataInfoAction(newData);
                                      }}
                                    />
                                  </Col>
                                </Row>
                              </div>
                            </div>
                          );
                        })
                      : null}

                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        paddingBottom: 40,
                        paddingTop: 20,
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          const obj = {
                            max_length: null,
                            min_length: null,
                            name: null,
                            type: null,
                            valid: true,
                          };
                          dataInfoAction.push(obj);
                          setRender(!render);
                        }}
                      >
                        <span style={{ color: colors.main, fontWeight: 500 }}>
                          Thêm thông tin
                        </span>
                        <div
                          style={{
                            background: "rgba(19, 167, 81, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            marginLeft: 10,
                          }}
                        >
                          <ISvg
                            name={ISvg.NAME.ARROWUP}
                            fill={colors.main}
                            width={20}
                          />
                        </div>
                      </div>
                    </div>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const createActionPage = Form.create({ name: "CreateActionPage" })(
  CreateActionPage
);

export default createActionPage;
