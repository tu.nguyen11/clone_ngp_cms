import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router'
import { APIService } from '../../../../services'
import { BackTop, Col, Empty, Row, Skeleton } from 'antd'
import {
  StyledITileHeading,
  StyledITitle
} from '../../../components/common/Font/font'
import { ISvg } from '../../../components'
import { colors } from '../../../../assets'
import FormatterDay from '../../../../utils/FormatterDay'

export default function DetailActionPage () {
  const params = useParams()
  const [isLoading, setLoading] = useState(true)
  const [dataDetail, setDataDetail] = useState({ list_action_detail: [] })

  const { id } = params

  const fetchAPIDetail = async router_id => {
    try {
      const data = await APIService._getDetailTacVu(router_id)
      setDataDetail(data.data)
      setLoading(false)
    } catch (error) {
      setLoading(false)
      console.log(error)
    }
  }

  useEffect(() => {
    fetchAPIDetail(id)
  }, [id])

  return (
    <div style={{ padding: '18px 0px 0px 0px', width: '100%' }}>
      <Row>
        <Col flex='auto'>
          <div>
            <StyledITitle style={{ color: colors.main, marginTop: 8 }}>
              Chi tiết tác vụ
            </StyledITitle>
          </div>
        </Col>
      </Row>

      <BackTop description={1000} />

      <div style={{ margin: '64px 0px' }}>
        <div
          style={{
            margin: '32px 0px 12px 0px',
            width: 615,
            background: 'white',
            boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.1)'
          }}
        >
          <div>
            <Row gutter={[24, 0]}>
              <Col span={24}>
                <div
                  style={{
                    padding: '30px 40px 120px 40px',
                    borderRight: '1px solid #D8DBDC'
                  }}
                >
                  <Skeleton loading={isLoading} active paragraph={{ rows: 12 }}>
                    <Row gutter={[0, 24]}>
                      <Col span={24}>
                        <StyledITileHeading
                          style={{ marginTop: 8 }}
                          minFont='12px'
                          maxFont='16px'
                        >
                          Thông tin công việc
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <div style={{ marginTop: 12 }}>
                          <p style={{ fontWeight: 500 }}>Tên tác vụ</p>
                          <span>{dataDetail.action_name}</span>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 500 }}>Trạng thái</p>
                                <span>{dataDetail.route_dvnn_name}</span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 500 }}>Tuyến ạp dụng</p>
                                <span>{dataDetail.route_code}</span>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 500 }}>Từ ngày</p>
                                <span>
                                  {FormatterDay.dateFormatWithString(
                                    dataDetail.start_date,
                                    '#DD#/#MM#/#YYYY#'
                                  )}
                                </span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{ fontWeight: 500 }}>Đến ngày</p>
                                <span>
                                  {FormatterDay.dateFormatWithString(
                                    dataDetail.end_date,
                                    '#DD#/#MM#/#YYYY#'
                                  )}
                                </span>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <Row>
                          <Col span={24}>
                            <StyledITileHeading
                              style={{
                                marginTop: 12,
                                color: colors.main,
                                marginBottom: 36
                              }}
                              minFont='12px'
                              maxFont='16px'
                            >
                              Thông tin Tác vụ
                            </StyledITileHeading>
                          </Col>
                        </Row>
                      </Col>
                      {dataDetail.list_action_detail.map(item => {
                        return (
                          <div>
                            <Row style={{}}>
                              <Col span={12}>
                                <div>
                                  <p style={{ fontWeight: 500 }}>Thông tin</p>
                                  <span>{item.name}</span>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p style={{ fontWeight: 500 }}>
                                    Loại dữ liệu
                                  </p>
                                  <span>
                                    {item.type == 2
                                      ? 'Số'
                                      : item.type == 1
                                      ? 'Chữ'
                                      : 'Đoạn văn'}
                                  </span>
                                </div>
                              </Col>
                            </Row>
                            <Row style={{ paddingTop: 25 }}>
                              <Col span={12}>
                                <div>
                                  <p style={{ fontWeight: 500 }}>Tối thiểu</p>
                                  <span>{item.min_lenght_value}</span>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p style={{ fontWeight: 500 }}>Tối đa</p>
                                  <span>{item.max_lenght_value}</span>
                                </div>
                              </Col>
                            </Row>
                          </div>
                        )
                      })}
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </div>
  )
}
