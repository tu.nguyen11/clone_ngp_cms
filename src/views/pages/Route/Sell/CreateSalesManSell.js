import {Col, Row, Form, Skeleton, message, Tooltip, Empty} from "antd";
import React, {useState, useEffect, useRef} from "react";
import {useHistory, useParams} from "react-router";
import {APIService} from "../../../../services";
import {colors, images} from "../../../../assets";
import moment from "moment";
import {ISvg, IButton, ITitle, ISelect, ISearch, ITable} from "../../../components";
import {
  IDatePickerFrom,
  IInputText,
  IFormItem,
} from "../../../components/common";
import {StyledITileHeading} from "../../../components/common/Font/font";
import FormatterDay from "../../../../utils/FormatterDay";

const NewDate = new Date();

function CreateSalesManSell(props) {
  const history = useHistory();
  const {id, type} = useParams();
  const isEdit = type === "edit";

  const {getFieldDecorator} = props.form;
  const reduxForm = props.form;
  const [dataTable, setDataTable] = useState({data: [], total: 0, page: 10});

  const [isLoading, setLoading] = useState(isEdit);
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [isLoadingSalesman, setIsLoadingSalesman] = useState(false);
  const [isLoadingShowroom, setIsLoadingShowroom] = useState(false);
  const [listBusiness, setLstBusiness] = useState([])

  const [dataDetail, setDataDetail] = useState({});
  const [dataDropSalesman, setDataDropSalesman] = useState([]);
  const [listShowroom, setListShowroom] = useState([]);
  const [isLoadingTable, setIsLoadingTable] = useState(true);

  const disabledStartDateApply = (startValue) => {
    if (!startValue || !NewDate) {
      return false;
    }
    return startValue.valueOf() < NewDate.valueOf();
  };
  const getListCustomInRoute = async (filter) => {
    try {
      const data = await APIService._getListCustomInRoute(filter);
      data.data.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        return item;
      });
      setDataTable({data: data.data, total: data.total, page: data.limit});
      setIsLoadingTable(false);
    } catch (error) {
      console.log({error});
      setIsLoadingTable(false);
    }
  };
  const [filterTable, setFilterTable] = useState({
    route_id: Number(id),
    keyword: "",
    page: 1,
  });

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !reduxForm.getFieldValue("time_start")) {
      return false;
    }
    return endValue.valueOf() <= reduxForm.getFieldValue("time_start");
  };

  const getListShowroom = async () => {
    try {
      const data = await APIService._getListShowroom();
      let dataNew = data.data.map((item, index) => {
        return {key: item.id, value: item.name};
      });
      setListShowroom([...dataNew]);
      setIsLoadingShowroom(false);
    } catch (error) {
      console.log({error});
      setIsLoadingShowroom(false);
    }
  };
  const _fetchLstBusinessChanel = async () => {
    try {
      const res = await APIService._getLstBussinesChanel()
      let newArr = res.data.map(item => {
        return {
          key: item.id,
          value: item.name
        }
      })
      setLstBusiness(newArr)
    } catch (e) {
      console.log(e)
    }
  }
  const getListSalesmanNotRoute = async (showroom_id, bussiness_id) => {
    try {
      if (!showroom_id || !bussiness_id) return
      setIsLoadingSalesman(true);
      const data = await APIService._getListSalesmanNotRoute(showroom_id, bussiness_id);
      let dataNew = data.data.map((item) => {
        return {
          key: item.id,
          value: item.name,
        };
      });

      setDataDropSalesman(dataNew);
      setIsLoadingSalesman(false);
      if (!isEdit) {
        if (data.data.length === 0) {
          reduxForm.setFieldsValue({
            saleman_under: undefined
          })
        } else {
          reduxForm.setFieldsValue({
            saleman_under: dataNew[0].key
          })
        }
      } else {
        dataNew.unshift({
          key: 0,
          value: "Chưa có nhân viên"
        })
      }
      return dataNew
    } catch (error) {
      console.log({error});
      setIsLoadingSalesman(false);
    }
  };

  const fetchAPIDetail = async (router_id) => {
    try {
      const data = await APIService._detailRouteSell(router_id);
      const objData = data.route_detail;
      reduxForm.setFieldsValue({
        name_route: objData.route_name,
        status_route: objData.status,
        code_route: objData.route_code,
        time_start: objData.start_date,
        time_end: objData.end_date,
        showroom: objData.show_rom_id,
        saleman_under: objData.sale_man_id,
        _business_chanel_id: objData.business_channel.id || undefined
      });
      setDataDetail({...objData});
      const dataSale = await getListSalesmanNotRoute(objData.show_rom_id, objData.business_channel.id);
      if (dataSale.filter(item => item.key === objData.sale_man_id).length === 0) {
        dataSale.unshift({
          key: objData.sale_man_id,
          value: objData.sale_man_name
        })
        setDataDropSalesman(dataSale)
        reduxForm.setFieldsValue({
          saleman_under: objData.sale_man_id
        })
      }
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const fetchUpdateRoute = async (obj) => {
    try {
      const data = await APIService._postUpdateRouteSell(obj);
      setLoadingCreate(false);
      message.success("Cập nhập thành công");
      history.goBack();
    } catch (error) {
      console.log(error);
      setLoadingCreate(false);
    }
  };

  const fetchAddRoute = async (obj) => {
    try {
      const data = await APIService._postAddRouteSell(obj);
      setLoadingCreate(false);
      message.success("Thêm thành công");
      history.push("/route/sell/list");
    } catch (error) {
      console.log(error);
      setLoadingCreate(false);
    }
  };

  useEffect(() => {
    getListShowroom();
    _fetchLstBusinessChanel()
    if (isEdit) {
      fetchAPIDetail(id);
    }
  }, []);

  useEffect(() => {
    if (isEdit) {
      getListCustomInRoute(filterTable)
    }
  }, [filterTable]);

  const onSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields((err, obj) => {
      if (!err) {
        let _end_date = '';
        if (typeof obj.time_end == "string") {
          _end_date = Date.parse(obj.time_end)
        } else _end_date = obj.time_end
        if (isEdit) {
          setLoadingCreate(true);
          const objEdit = {
            business_chanel_id: obj._business_chanel_id,
            route_id: Number(id),
            route_name: obj.name_route,
            route_code: obj.code_route,
            status: obj.status_route,
            sale_man_id: obj.saleman_under,
            start_date: obj.time_start,
            end_date: _end_date,
            show_rom_id: obj.showroom,
          };
          fetchUpdateRoute(objEdit);
        } else {
          const objAdd = {
            business_chanel_id: obj._business_chanel_id,
            route_name: obj.name_route,
            route_code: obj.code_route,
            status: obj.status_route,
            sale_man_id: obj.saleman_under,
            start_date: obj.time_start,
            end_date: obj.time_end,
            show_rom_id: obj.showroom,
          };
          fetchAddRoute(objAdd);
        }
      }
    });
  };
  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tên khách hàng",
      dataIndex: "name",
      key: "name",
      render: (name) => <span>{!name ? "-" : name}</span>,
    },
    {
      title: "Mã khách hàng",
      dataIndex: "user_code",
      key: "user_code",
      render: (user_code) => <span>{!user_code ? "-" : user_code}</span>,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <span>{!phone ? "-" : phone}</span>,
    },
    {
      title: "Mẫu xe quan tâm",
      dataIndex: "interested_car",
      key: "interested_car",
      render: (interested_car) => (
        <span style={{textTransform: "capitalize"}}>
          {!interested_car ? "-" : interested_car}
        </span>
      ),
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
      render: (address) => <span>{!address ? "-" : address}</span>,
    },
    {
      title: "Khách hàng",
      dataIndex: "type",
      key: "type",
      render: (type) => <span>{!type ? "-" : type}</span>,
    },
    {
      title: "NV phụ trách",
      dataIndex: "sale_man_name",
      key: "sale_man_name",
      render: (sale_man_name) => (
        <span>{!sale_man_name ? "-" : sale_man_name}</span>
      ),
    },
    {
      title: "Ngày hẹn",
      dataIndex: "appointment_date",
      key: "appointment_date",
      render: (appointment_date) => (
        <span>
          {!appointment_date || appointment_date < 0
            ? "-"
            : FormatterDay.dateFormatWithString(
              appointment_date,
              "#DD#-#MM#-#YYYY#"
            )}
        </span>
      ),
    },
  ];
  return (
    <div style={{width: "100%"}}>
      <Row>
        <Col span={24}>
          <ITitle
            level={1}
            title={
              isEdit ? "Chỉnh sửa tuyến bán hàng" : "Tạo tuyến bán hàng mới"
            }
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        <Col span={24}>
          <div className="flex justify-end">
            <IButton
              color={colors.main}
              title="Lưu"
              loading={loadingCreate}
              icon={ISvg.NAME.SAVE}
              style={{marginRight: 15}}
              htmlType="submit"
              form='my-form'
            />

            <IButton
              color={colors.oranges}
              title="Hủy"
              onClick={() => history.goBack()}
              icon={ISvg.NAME.CROSS}
            />
          </div>
        </Col>
        <Col span={24} style={{margin: "30px 0px"}}>
          <Row style={{
            width: isEdit ? '100%' : 700,
            background: "white",
            boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
          }} gutter={[16, 20]}>
            <Col style={{
              padding: 30,
            }} span={isEdit ? 10 : 24}>
              <Form id='my-form' onSubmit={onSubmit}>
                <Row gutter={[0, 24]}>
                  <Col span={24}>
                    <Skeleton
                      loading={isLoading}
                      active
                      paragraph={{rows: 12}}
                    >
                      <Row gutter={[0, 24]}>
                        <Col span={24}>
                          <StyledITileHeading
                            style={{marginTop: 8}}
                            minFont="12px"
                            maxFont="16px"
                          >
                            Thông tin tuyến
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div style={{marginTop: 12}}>
                                <p style={{fontWeight: 500}}>
                                  Tên tuyến
                                  <span style={{marginLeft: 5, color: "red"}}>
                                  (*)
                                </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("name_route", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "nhập tên tuyến",
                                      },
                                    ],
                                  })(
                                    <IInputText
                                      placeholder="nhập tên tuyến"
                                      value={reduxForm.getFieldValue(
                                        "name_route"
                                      )}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div style={{marginTop: 12}}>
                                <p style={{fontWeight: 500}}>
                                  Loại Kênh
                                  <span style={{marginLeft: 5, color: "red"}}>
                                  (*)
                                </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_business_chanel_id", {
                                    rules: [
                                      {
                                        required: true,
                                        message:
                                          "vui lòng chọn kênh kinh doanh",
                                      },
                                    ],
                                  })(
                                    <ISelect
                                      select={!isEdit}
                                      className="clear-pad"
                                      type="write"
                                      value={reduxForm.getFieldValue(
                                        "_business_chanel_id"
                                      )}
                                      placeholder="Vui lòng chọn"
                                      data={listBusiness}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                      }}
                                      isBackground={false}
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          _business_chanel_id: key,
                                        });
                                        getListSalesmanNotRoute(reduxForm.getFieldValue(
                                          "showroom"
                                        ), key)
                                      }}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24}>
                          <div>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <p style={{fontWeight: 500}}>
                                  Trạng thái
                                  <span
                                    style={{
                                      marginLeft: 5,
                                      color: "red",
                                    }}
                                  >
                                      (*)
                                    </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("status_route", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "chọn trạng thái",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <ISelect
                                        className="clear-pad"
                                        placeholder="chọn trạng thái"
                                        type="write"
                                        select={true}
                                        style={{
                                          background: colors.white,
                                          borderStyle: "solid",
                                          borderWidth: 0,
                                          borderColor: colors.line,
                                          borderBottomWidth: 1,
                                          marginTop: 1,
                                        }}
                                        isBackground={false}
                                        onChange={(key) => {
                                          reduxForm.setFieldsValue({
                                            status_route: key,
                                          });
                                        }}
                                        value={
                                          reduxForm.getFieldValue(
                                            "status_route"
                                          ) === undefined ||
                                          reduxForm.getFieldValue(
                                            "status_route"
                                          ) === null
                                            ? undefined
                                            : reduxForm.getFieldValue(
                                            "status_route"
                                            )
                                        }
                                        data={[
                                          {
                                            key: 1,
                                            value: "Đang hoạt động",
                                          },
                                          {key: 0, value: "Tạm ngưng"},
                                        ]}
                                      />
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>
                                    Mã tuyến
                                    <span
                                      style={{
                                        marginLeft: 5,
                                        color: "red",
                                      }}
                                    >
                                        (*)
                                      </span>
                                  </p>
                                  <IFormItem>
                                    {getFieldDecorator("code_route", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "nhập mã tuyến đúng định dạng",
                                          pattern: new RegExp(/^[a-zA-Z0-9]*$/),
                                        },
                                      ],
                                    })(
                                      <IInputText
                                        disabled={isEdit}
                                        placeholder="nhập mã tuyến"
                                        value={reduxForm.getFieldValue(
                                          "code_route"
                                        )}
                                      />
                                    )}
                                  </IFormItem>
                                </div>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                        <Col span={24}>
                          <div>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>
                                    Showroom trực thuộc
                                    <span
                                      style={{
                                        marginLeft: 5,
                                        color: "red",
                                      }}
                                    >
                                        (*)
                                      </span>
                                  </p>
                                  <IFormItem>
                                    {getFieldDecorator("showroom", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn showroom trực thuộc",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <ISelect
                                          className="clear-pad"
                                          placeholder="chọn showroom trực thuộc"
                                          type="write"
                                          select={
                                            isEdit || isLoadingShowroom
                                              ? false
                                              : true
                                          }
                                          loading={isLoadingShowroom}
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                          }}
                                          isBackground={false}
                                          onChange={async (key, option) => {
                                            reduxForm.setFieldsValue({
                                              showroom: Number(option.key),
                                            });

                                            await getListSalesmanNotRoute(
                                              key, reduxForm.getFieldValue(
                                                "_business_chanel_id"
                                              )
                                            );
                                          }}
                                          value={
                                            !reduxForm.getFieldValue(
                                              "showroom"
                                            )
                                              ? undefined
                                              : reduxForm.getFieldValue(
                                              "showroom"
                                              )
                                          }
                                          data={listShowroom}
                                        />
                                      </div>
                                    )}
                                  </IFormItem>
                                </div>
                              </Col>
                              <Col span={12}>
                                <p style={{fontWeight: 500}}>
                                  Nhân viên phụ trách
                                  <span
                                    style={{
                                      marginLeft: 5,
                                      color: "red",
                                    }}
                                  >
                                      (*)
                                    </span>
                                </p>
                                <div>
                                  <IFormItem>
                                    {getFieldDecorator("saleman_under", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "gán nhân viên phụ trách",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <ISelect
                                          className="clear-pad"
                                          placeholder="gán nhân viên phụ trách"
                                          type="write"
                                          select={true}
                                          loading={isLoadingSalesman}
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                          }}
                                          isBackground={false}
                                          onChange={(key, option) => {
                                            reduxForm.setFieldsValue({
                                              saleman_under: Number(
                                                option.key
                                              ),
                                            });
                                          }}
                                          value={reduxForm.getFieldValue("saleman_under")
                                          }
                                          data={dataDropSalesman}
                                        />
                                      </div>
                                    )}
                                  </IFormItem>
                                </div>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                    </Skeleton>
                  </Col>
                  <Col span={24}>
                    <div
                      style={{
                        marginTop: 20,
                      }}
                    >
                      <Skeleton
                        loading={isLoading}
                        active
                        paragraph={{rows: 12}}
                      >
                        <Row gutter={[0, 24]}>
                          <Col span={24}>
                            <StyledITileHeading
                              style={{marginTop: 8}}
                              minFont="12px"
                              maxFont="16px"
                            >
                              Thời gian hiệu lực
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>
                                    Ngày bắt đầu
                                    <span
                                      style={{
                                        marginLeft: 5,
                                        color: "red",
                                      }}
                                    >
                                        *
                                      </span>
                                  </p>
                                  <IFormItem>
                                    {getFieldDecorator("time_start", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn ngày bắt đầu",
                                        },
                                      ],
                                    })(
                                      <div
                                        style={{
                                          borderBottom: "1px solid #d8dbdc",
                                        }}
                                      >
                                        <IDatePickerFrom
                                          bordered={false}
                                          paddingInput="0px"
                                          disabledDate={
                                            disabledStartDateApply
                                          }
                                          onChange={(date, dateString) => {
                                            let timeStart = date._d.getTime();
                                            reduxForm.setFieldsValue({
                                              time_start: timeStart,
                                            });
                                          }}
                                          showTime
                                          showToday={false}
                                          defaultValue={
                                            isEdit
                                              ? moment(
                                              new Date(
                                                reduxForm.getFieldValue(
                                                  "time_start"
                                                )
                                              ),
                                              "hh:mm:ss DD-MM-YYYY"
                                              )
                                              : undefined
                                          }
                                          disabled={isEdit}
                                          placeholder="chọn ngày bắt đầu"
                                        />
                                      </div>
                                    )}
                                  </IFormItem>
                                </div>
                              </Col>
                              <Col span={12}>
                                <p style={{fontWeight: 500}}>
                                  Ngày kết thúc
                                  <span
                                    style={{
                                      marginLeft: 5,
                                      color: "red",
                                    }}
                                  >
                                      *
                                    </span>
                                </p>

                                <IFormItem>
                                  {getFieldDecorator("time_end", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "chọn ngày kết thúc",
                                      },
                                    ],
                                  })(
                                    <div
                                      style={{
                                        borderBottom: "1px solid #d8dbdc",
                                      }}
                                    >
                                      <IDatePickerFrom
                                        bordered={false}
                                        paddingInput="0px"
                                        disabledDate={disabledEndDateApply}
                                        onChange={(date, dateString) => {
                                          let timeEnd = date._d.getTime();

                                          reduxForm.setFieldsValue({
                                            time_end: timeEnd,
                                          });
                                        }}
                                        showTime
                                        showToday={false}
                                        defaultValue={
                                          isEdit
                                            ? moment(
                                            new Date(
                                              reduxForm.getFieldValue(
                                                "time_end"
                                              )
                                            ),
                                            "hh:mm:ss DD-MM-YYYY"
                                            )
                                            : undefined
                                        }
                                        placeholder="chọn ngày kết thúc"
                                      />
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Skeleton>
                    </div>
                  </Col>
                </Row>
              </Form>
            </Col>
            {isEdit && (
              <Col style={{
                // height: 600,
                padding: 30,
                borderLeft: "1px solid #D8DBDC",
              }} span={14}>
                <div
                >
                  <Skeleton loading={isLoading} active paragraph={{rows: 15}}>
                    <Row>
                      <Col span={24}>
                        <div
                          className="flex justify-end"
                          style={{marginBottom: 20}}
                        >
                          <Tooltip title="Tìm kiếm tên khách hàng hoặc số điện thoại">
                            <ISearch
                              className="cursor"
                              placeholder="Tìm kiếm tên khách hàng hoặc số điện thoại"
                              onPressEnter={(e) => {
                                setIsLoadingTable(true);
                                setFilterTable({
                                  ...filterTable,
                                  keyword: e.target.value,
                                  page: 1,
                                });
                              }}
                              icon={
                                <div
                                  style={{
                                    display: "flex",
                                    width: 42,
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}
                                >
                                  <img
                                    src={images.icSearch}
                                    style={{width: 16, height: 16}}
                                    alt=""
                                  />
                                </div>
                              }
                            />
                          </Tooltip>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div style={
                          {height: 500}
                        }>
                          <ITable
                            style={{width: "100%"}}
                            bordered={true}
                            backgroundWhite={true}
                            scroll={{
                              x: !dataTable.data.length ? 0 : 1600,
                              y: 360,
                            }}
                            data={dataTable.data}
                            columns={columns}
                            defaultCurrent={1}
                            indexPage={filterTable.page}
                            sizeItem={dataTable.size}
                            maxpage={dataTable.total}
                            loading={isLoadingTable}
                            onChangePage={(page) => {
                              setIsLoadingTable(true);
                              setFilterTable({...filterTable, page: page});
                            }}
                            locale={{
                              emptyText: (
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "center",
                                    alignItems: "center",
                                  }}
                                >
                                  <Empty description="Không có khách hàng"/>
                                </div>
                              ),
                            }}
                          />
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            )}
          </Row>
        </Col>
      </Row>
    </div>
  );
}

const createSalesManSell = Form.create({name: "CreateSalesManSell"})(
  CreateSalesManSell
);

export default createSalesManSell;
