import { Col, Row, Tooltip, message, Popconfirm } from "antd";
import React, { useEffect, useState } from "react";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import { ISearch, IButton, ISvg, ITable } from "../../../components";
import { StyledITitle } from "../../../components/common/Font/font";
import { useHistory } from "react-router";

export default function ListSellRoute() {
  const history = useHistory();
  const [filterTable, setFilterTable] = useState({
    key: "",
    page: 1,
  });

  const [dataTable, setDataTable] = useState({});
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingRemove, setLoadingRemove] = useState(false);
  const getListRouteSell = async (filter) => {
    try {
      const data = await APIService._getListRouteSell(filter);
      data.data.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
        });
        return item;
      });
      setDataTable(data);
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };
  const fetchRemoveRouter = async (obj) => {
    try {
      const data = await APIService._postRemoveSell(obj);
      setLoadingRemove(false);
      message.success("Xóa thành công");
      setLoadingTable(true);
      await getListRouteSell(filterTable);
    } catch (error) {
      console.log(error);
      setLoadingRemove(false);
    }
  };

  useEffect(() => {
    getListRouteSell(filterTable);
  }, [filterTable]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Mã tuyến",
      dataIndex: "route_code",
      key: "route_code",
      render: (route_code) => <span>{!route_code ? "-" : route_code}</span>,
    },
    {
      title: "Tên tuyến",
      dataIndex: "route_name",
      key: "route_name",
      render: (route_name) => <span>{!route_name ? "-" : route_name}</span>,
    },
    {
      title: "Showroom trực thuộc",
      dataIndex: "show_rom",
      key: "show_rom",
      render: (show_rom) => (
        <span style={{ textTransform: "capitalize" }}>
          {!show_rom ? "-" : show_rom}
        </span>
      ),
    },
    {
      title: "Số KH trực thuộc",
      dataIndex: "total_customer",
      key: "total_customer",
      align: "center",
      render: (total_customer) => (
        <span>{!total_customer ? 0 : total_customer}</span>
      ),
    },
    {
      title: "Nhân viên bán hàng",
      dataIndex: "sale_man_name",
      key: "sale_man_name",
      render: (sale_man_name) => (
        <span>{!sale_man_name ? "-" : sale_man_name}</span>
      ),
    },
    {
      title: "Mã NV",
      dataIndex: "sale_man_code",
      key: "sale_man_code",
      render: (sale_man_code) => (
        <span>{!sale_man_code ? "-" : sale_man_code}</span>
      ),
    },
    {
      title: "Số điện thoại",
      dataIndex: "sale_man_phone",
      key: "sale_man_phone",
      render: (sale_man_phone) => (
        <span>{!sale_man_phone ? "-" : sale_man_phone}</span>
      ),
    },

    {
      title: "Trạng thái",
      dataIndex: "sale_man_status_name",
      key: "sale_man_status_name",
      render: (sale_man_status_name) => (
        <span>{!sale_man_status_name ? "-" : sale_man_status_name}</span>
      ),
    },
  ];

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRowKeys(selectedRowKeys);
    },
    getCheckboxProps: (record) => {
      const rowIndex = dataTable.data.find(item1 => {
        return item1.id == record.id && item1.status == 1
      })
      return {
        disabled: rowIndex
      };
    },

  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Quản lý tuyến bán hàng
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã, tên tuyến, mã, tên nhân viên bán hàng">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo mã, tên tuyến, mã, tên nhân viên bán hàng"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    key: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "20px 0px" }}>
        <Row gutter={[0, 40]}>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Tạo mới"
                    color={colors.main}
                    icon={ISvg.NAME.ARROWUP}
                    styleHeight={{
                      width: 140,
                      marginRight: 15,
                    }}
                    isRight={true}
                    onClick={() => {
                      history.push(`/route/sell/create/0`);
                    }}
                  />

                  <Popconfirm
                    placement="bottom"
                    title="Nhấn đồng ý để xóa"
                    okText="Đồng ý"
                    cancelText="Hủy"
                    onConfirm={() => {
                      if (selectedRowKeys.length === 0) {
                        message.error("Vui lòng chọn tuyến để xóa");
                        return;
                      }
                      const arrId = selectedRowKeys.map((item) => {
                        const itemParse = JSON.parse(item);
                        return itemParse.id;
                      });
                      const isActive = arrId.filter(item =>{
                        return dataTable.data.find(item1=>{
                            return item1.id == item && item1.status == 1
                          })
                      })
                      if(isActive.length !== 0){
                        message.error("Bạn không thể xóa tuyến đang hoạt động!");
                        return;
                      }
                      setLoadingRemove(true);
                      const objRemove = {
                        route_id: arrId,
                      };
                      fetchRemoveRouter(objRemove);
                    }}
                  >
                    <IButton
                      color={colors.oranges}
                      title="Xóa"
                      icon={ISvg.NAME.DELETE}
                      styleHeight={{ width: 140 }}
                      loading={loadingRemove}
                    />
                  </Popconfirm>
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24} style={{ paddingTop: 0 }}>
            <ITable
              columns={columns}
              data={dataTable.data}
              style={{ width: "100%" }}
              defaultCurrent={1}
              sizeItem={dataTable.size}
              indexPage={filterTable.page}
              maxpage={dataTable.total}
              loading={loadingTable}
              scroll={{ x: 1600 }}
              rowKey="rowTable"
              rowSelection={rowSelection}
              onRow={(record) => {
                return {
                  onClick: () => {
                    history.push(`/detail/route/sell/${record.id}`);
                  },
                  onMouseDown: (event) => {
                    if (event.button == 2 || event == 4) {
                      window.open(`/detail/route/sell/${record.id}`, "_blank");
                    }
                  },
                };
              }}
              onChangePage={(page) => {
                setLoadingTable(true);
                setFilterTable({ ...filterTable, page: page });
              }}
            ></ITable>
          </Col>
        </Row>
      </div>
    </div>
  );
}
