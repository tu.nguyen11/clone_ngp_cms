import { SearchOutlined } from "@ant-design/icons";
import { Checkbox, Col, Row, List, message } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import { IButton, ISelect, ISvg, ITableHtml } from "../../../components";
import {
  ADD_S2,
  SELECT_FREQUENCY_ID_SELL,
  SAVE_S2,
  CONFIRM_S2,
  DEAFUAR_S2,
  REMOVE_S2,
  UPDATE_ACTIVELISTSELL,
  CANCEL_S2,
} from "../../../store/reducers";
import { StyledSearchCustom } from "../../../components/common";
import { StyledITileHeading } from "../../../components/common/Font/font";

let headerTable = [
  {
    name: "STT",
    align: "center",
  },
  {
    name: "Mã đại lý",
    align: "left",
  },
  {
    name: "Tên đại lý",
    align: "left",
  },
  {
    name: "Tần suất",
    align: "left",
  },
  {
    name: "",
    align: "center",
  },
];

export default function useModalListS2Frequency(
  callbackClose = () => {},
  agencyId
) {
  const [loadingList, setLoadingList] = useState(true);
  const [selectFrequency, setSelectFrenquency] = useState([]);
  const [filterFrequency, setFilterFrequency] = useState({
    from_limit: 0,
    to_limit: 10,
  });

  const dispatch = useDispatch();
  const dataProduct = useSelector((state) => state);
  const { listS2Show, listS2, listS2AddClone } = dataProduct;

  const [filterListS2, setfilterListS2] = useState({
    keySearch: "",
    from_limit: 0,
    to_limit: 10,
    agencyId: 0,
  });

  const _getAPIListAgencyS2 = async (filterListS2) => {
    try {
      debugger;
      const data = await APIService._getDropListSuppliers(filterListS2);
      debugger;
      let suppliers = [...data.listSupplier];
      suppliers.map((item, index) => {
        suppliers[index].status = 0;
      });
      dispatch(DEAFUAR_S2(suppliers));
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  const fetchAPISelectFrequency = async (obj) => {
    try {
      const { from_limit, to_limit } = obj;

      const data = await APIService._getSelectFrequency(
        from_limit,
        to_limit,
        agencyId
      );

      let dataNew = data.frequency_plan.map((item) => {
        const key = item.id;
        const value = item.code;
        return { key, value };
      });
      setSelectFrenquency(dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    _getAPIListAgencyS2(filterListS2);
  }, [filterListS2]);

  useEffect(() => {
    if (!agencyId) {
      return;
    }
    fetchAPISelectFrequency({ ...filterFrequency, agencyId: agencyId });
  }, [filterFrequency, agencyId]);

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="tr-table">
        {headerTable.map((item, index) => (
          <th className={"th-table"} style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr className="tr-table">
        <td className="td-table" style={{ textAlign: "center" }}>
          {index + 1}
        </td>
        <td className="td-table">{item.id}</td>

        <td className="td-table">{item.supplierName}</td>
        <td className="td-table">
          <ISelect
            bordered={false}
            isBorderBottom="1px solid #D8DBDC"
            minWidth="100%"
            fontWeight="500"
            select={true}
            defaultValue={
              !item.frequency_id ? "Chọn tần suất" : item.frequency_code
            }
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) => {
              return (
                option.props.dataProps.value
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0
              );
            }}
            onChange={(key, option) => {
              debugger;
              dispatch(
                SELECT_FREQUENCY_ID_SELL({
                  data: item,
                  key: Number(option.key),
                  name: option.props.children,
                })
              );
            }}
            data={selectFrequency}
          />
        </td>
        <td className="td-table">
          <div
            style={{
              background: "rgba(227, 95, 75, 0.1)",
              width: 20,
              height: 20,
              borderRadius: 10,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            onClick={() => {
              dispatch(REMOVE_S2(index));
              dispatch(
                UPDATE_ACTIVELISTSELL({
                  id: item.id,
                  status: 0,
                })
              );
            }}
            className="cursor-push remove"
          >
            <ISvg width={8} height={8} name={ISvg.NAME.CROSS} fill="#E35F4B" />
          </div>
        </td>
      </tr>
    ));
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Gán đại lý cấp 2 vào tuyến bán hàng
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={10}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: "100%" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <div style={{ display: "flex" }}>
                          <SearchOutlined
                            style={{
                              fontSize: 20,
                              color: colors.svg_default,
                            }}
                          />
                          <StyledSearchCustom
                            style={{ width: 400 }}
                            suffix={() => {
                              return null;
                            }}
                            placeholder="nhập kí tự tìm kiếm"
                            onPressEnter={(e) => {
                              let keySearch = e.target.value;
                              setLoadingList(true);
                              setfilterListS2({
                                ...filterListS2,
                                keySearch: keySearch,
                              });
                            }}
                            prefix={() => {
                              return null;
                            }}
                          />
                        </div>
                      </div>
                    </Col>

                    <Col span={24}>
                      <div
                        style={{
                          height: 400,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        <List
                          dataSource={listS2}
                          bordered={false}
                          loading={loadingList}
                          renderItem={(item, index) => {
                            debugger;
                            return (
                              <List.Item>
                                <div style={{ width: "100%", height: "100%" }}>
                                  <Row>
                                    <Col span={3}>
                                      <Checkbox
                                        defaultChecked={item.status === 1}
                                        checked={item.status === 1}
                                        onChange={(e) => {
                                          let checked = e.target.checked;
                                          dispatch(
                                            ADD_S2({
                                              data: item,
                                              status: checked ? 1 : 0,
                                            })
                                          );
                                          dispatch(
                                            UPDATE_ACTIVELISTSELL({
                                              status: checked ? 1 : 0,
                                              id: item.id,
                                            })
                                          );
                                        }}
                                      />
                                    </Col>
                                    <Col span={21}>
                                      <span>{item.supplierName}</span>
                                    </Col>
                                  </Row>
                                </div>
                              </List.Item>
                            );
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          color={colors.main}
                          title="Thêm"
                          icon={ISvg.NAME.ARROWRIGHT}
                          styleHeight={{ width: 140 }}
                          isRight={true}
                          onClick={() => {
                            if (listS2AddClone.length === 0) {
                              message.error("Chưa chọn đại lý cấp 2");
                              return;
                            }
                            dispatch(SAVE_S2());
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col
                span={14}
                style={{
                  border: "1px solid rgba(122, 123, 123, 0.5)",
                  borderTop: "none",
                  padding: 0,
                }}
              >
                <div
                  style={{
                    height: 506,
                  }}
                >
                  <ITableHtml
                    childrenBody={bodyTableProduct(listS2Show)}
                    childrenHeader={headerTableProduct(headerTable)}
                    isBorder={false}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <div
        size={20}
        style={{ display: "flex", position: "absolute", bottom: -55, right: 0 }}
      >
        <IButton
          color={colors.main}
          title="Lưu"
          icon={ISvg.NAME.SAVE}
          styleHeight={{ width: 140, marginRight: 15 }}
          onClick={() => {
            let error = 0;
            listS2Show.map((item) => {
              if (!item.frequency_id) {
                error = error + 1;
                return;
              }
            });
            if (error !== 0) {
              message.error("Chưa chọn tuần suất");
              return;
            }
            dispatch(CONFIRM_S2());
            callbackClose();
          }}
        />
        <IButton
          color={colors.oranges}
          title="Hủy bỏ"
          styleHeight={{ width: 140 }}
          icon={ISvg.NAME.CROSS}
          onClick={() => {
            dispatch(CANCEL_S2());
            callbackClose();
          }}
        />
      </div>
    </div>
  );
}
