const Add_S2 = (state, action) => {
  let arrActive = state.listS2Add.map((item) => item.id);
  let arrActiveAddClone = state.listS2AddClone.map((item) => item.id);
  if (action.payload.status) {
    if (arrActive.indexOf(action.payload.data.id) === -1) {
      let obj = {
        ...action.payload.data,
        frequency_id: 0,
        frequency_code: "",
      };
      state.listS2AddClone.push(obj);
    } else {
      if (arrActiveAddClone.indexOf(action.payload.data.id) === -1) {
        let obj = {
          ...action.payload.data,
          frequency_id: 0,
          frequency_code: "",
        };
        state.listS2AddClone.push(obj);
      }
    }
  } else {
    let arrActiveClone = state.listS2AddClone.map((item) => item.id);
    let idx = arrActiveClone.indexOf(action.payload.data.id);
    state.listS2AddClone.splice(idx, 1);
  }
  return state;
};

const Remove_S2 = (state, action) => {
  state.listS2Show.splice(action.payload, 1);
  state.listS2AddClone.splice(action.payload, 1);
  return state;
};

const Update_ActiveListSell = (state, action) => {
  let arrActive = state.listS2.map((item) => item.id);
  let idx = arrActive.indexOf(action.payload.id);
  if (idx !== -1) {
    state.listS2[idx].status = action.payload.status;
  }

  return state;
};

const Select_frequency_id_sell = (state, action) => {
  let arrActive = state.listS2Show.map((item) => item.id);
  let idx = arrActive.indexOf(action.payload.data.id);

  state.listS2Show[idx].frequency_id = action.payload.key;
  state.listS2Show[idx].frequency_code = action.payload.name;

  let arrActiveAddClone = state.listS2AddClone.map((item) => item.id);
  let idxAddClone = arrActiveAddClone.indexOf(action.payload.data.id);

  state.listS2AddClone[idxAddClone].frequency_id = action.payload.key;
  state.listS2AddClone[idxAddClone].frequency_code = action.payload.name;
  return state;
};

const Save_S2 = (state, action) => {
  let arrTmp = state.listS2Show.concat(state.listS2AddClone);
  for (var i = 0; i < arrTmp.length; ++i) {
    for (var j = i + 1; j < arrTmp.length; ++j) {
      if (arrTmp[i].id == arrTmp[j].id) {
        arrTmp.splice(j--, 1);
      }
    }
  }
  state.listS2Show = arrTmp;
  return state;
};

const Confirm_S2 = (state, action) => {
  state.listS2Add = state.listS2Show;
  state.listS2Clone = state.listS2;
  return state;
};

const Array_S2 = (state, action) => {
  state.listS2Add = action.payload;
  state.listS2Show = action.payload;
  return state;
};

const Reset_S2 = (state, action) => {
  state.listS2AddClone = [];
  state.listS2Add = [];
  state.listS2Show = [];
  return state;
};

const Deafuat_S2 = (state, action) => {
  let arrActive = state.listS2Show.map((item) => item.id);
  action.payload.map((item) => {
    if (arrActive.indexOf(item.id) !== -1) {
      item.status = true;
    }
  });
  state.listS2AddClone = state.listS2Show;
  state.listS2Before = action.payload;
  state.listS2 = action.payload;
  state.listS2Clone = action.payload;
  return state;
};

const Cancel_S2 = (state, action) => {
  state.listS2Show = state.listS2Add;
  state.listS2 = state.listS2Clone;
  state.listS2AddClone = [];
  return state;
};

export {
  Add_S2,
  Array_S2,
  Select_frequency_id_sell,
  Save_S2,
  Confirm_S2,
  Reset_S2,
  Deafuat_S2,
  Remove_S2,
  Update_ActiveListSell,
  Cancel_S2,
};
