import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router";
import {APIService} from "../../../../services";
import {Col, Row, Skeleton, message, Popconfirm, Tooltip, Empty} from "antd";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";
import {IButton, ITable, ISearch} from "../../../components";
import {ISvg} from "../../../components";
import {colors, images} from "../../../../assets";
import FormatterDay from "../../../../utils/FormatterDay";
import {useSelector} from "react-redux";

export default function DetailSellRoute() {
  const history = useHistory();
  const {id} = useParams();

  const [filterTable, setFilterTable] = useState({
    route_id: Number(id),
    keyword: "",
    page: 1,
  });

  const [isLoading, setLoading] = useState(true);
  const [isLoadingTable, setIsLoadingTable] = useState(false);
  const [loadingRemove, setLoadingRemove] = useState(false);
  const {menu} = useSelector((state) => state)

  const [dataTable, setDataTable] = useState({data: [], total: 0, page: 10});
  const [dataDetail, setDataDetail] = useState({});

  const fetchAPIDetail = async (router_id) => {
    try {
      const data = await APIService._detailRouteSell(router_id);
      setDataDetail(data.route_detail);
      setLoading(false);
    } catch (error) {
      console.log({error});
      setLoading(false);
    }
  };

  const getListCustomInRoute = async (filter) => {
    try {
      const data = await APIService._getListCustomInRoute(filter);
      data.data.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        return item;
      });
      setDataTable({data: data.data, total: data.total, page: data.limit});
      setIsLoadingTable(false);
    } catch (error) {
      console.log({error});
      setIsLoadingTable(false);
    }
  };

  const fetchRemoveRouter = async (obj) => {
    try {
      const data = await APIService._postRemoveSell(obj);
      setLoadingRemove(false);
      message.success("Xóa thành công");
      history.push("/route/sell/list");
    } catch (error) {
      console.log(error);
      setLoadingRemove(false);
    }
  };

  useEffect(() => {
    getListCustomInRoute(filterTable);
  }, [filterTable]);

  useEffect(() => {
    fetchAPIDetail(id);
  }, []);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tên khách hàng",
      dataIndex: "name",
      key: "name",
      render: (name) => <span>{!name ? "-" : name}</span>,
    },
    {
      title: "Mã khách hàng",
      dataIndex: "user_code",
      key: "user_code",
      render: (user_code) => <span>{!user_code ? "-" : user_code}</span>,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <span>{!phone ? "-" : phone}</span>,
    },
    {
      title: "Mẫu xe quan tâm",
      dataIndex: "interested_car",
      key: "interested_car",
      render: (interested_car) => (
        <span style={{textTransform: "capitalize"}}>
          {!interested_car ? "-" : interested_car}
        </span>
      ),
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
      render: (address) => <span>{!address ? "-" : address}</span>,
    },
    {
      title: "Khách hàng",
      dataIndex: "type",
      key: "type",
      render: (type) => <span>{!type ? "-" : type}</span>,
    },
    {
      title: "NV phụ trách",
      dataIndex: "sale_man_name",
      key: "sale_man_name",
      render: (sale_man_name) => (
        <span>{!sale_man_name ? "-" : sale_man_name}</span>
      ),
    },
    {
      title: "Ngày hẹn",
      dataIndex: "appointment_date",
      key: "appointment_date",
      render: (appointment_date) => (
        <span>
          {!appointment_date || appointment_date < 0
            ? "-"
            : FormatterDay.dateFormatWithString(
              appointment_date,
              "#DD#-#MM#-#YYYY#"
            )}
        </span>
      ),
    },
  ];

  return (
    <div style={{padding: "18px 0px 0px 0px", width: "100%"}}>
      <Row>
        <Col span={24}>
          <div>
            <StyledITitle style={{color: colors.main, marginTop: 8}}>
              Chi tiết tuyến bán hàng
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div className="flex justify-end">
                <IButton
                  color={colors.main}
                  title="Chỉnh sửa"
                  icon={ISvg.NAME.WRITE}
                  style={{ marginRight: 15}}
                  onClick={() => {
                    history.push(`/route/sell/edit/${id}`);
                  }}
                />
                {dataDetail.status !== 1 && (<Popconfirm
                  placement="bottom"
                  title="Nhấn đồng ý để xóa"
                  okText="Đồng ý"
                  cancelText="Hủy"
                  onConfirm={() => {
                    setLoadingRemove(true);
                    const objRemove = {
                      route_id: [Number(id)],
                    };
                    fetchRemoveRouter(objRemove);
                  }}
                >
                  <IButton
                    color={colors.oranges}
                    title="Xóa"
                    icon={ISvg.NAME.DELETE}
                    styleHeight={{width: 140}}
                    loading={loadingRemove}
                  />
                </Popconfirm>)}
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              margin: "20px 0px",
              background: "white",
              boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
            }}
          >
            <Row gutter={[24, 0]}>
              <Col span={8}>
                <div
                  style={{
                    padding: 30,
                  }}
                >
                  <Row gutter={[0, 24]}>
                    <Col span={24}>
                      <Skeleton
                        loading={isLoading}
                        active
                        paragraph={{rows: 10}}
                      >
                        <Row gutter={[0, 24]}>
                          <Col span={24}>
                            <StyledITileHeading minFont="12px" maxFont="16px">
                              Thông tin tuyến
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>Tên tuyến</p>
                                  <span>
                                {!dataDetail.route_name
                                  ? "-"
                                  : dataDetail.route_name}
                              </span>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>Loại kênh</p>
                                  <span>
                                {!dataDetail.business_channel
                                  ? "-"
                                  : dataDetail.business_channel.name}
                              </span>
                                </div>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>Trạng thái</p>
                                  <span>
                                    {dataDetail.status === 1
                                      ? "Đang hoạt động"
                                      : dataDetail.status === 0
                                        ? "Tạm ngưng"
                                        : "-"}
                                  </span>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>Mã tuyến</p>
                                  <span>
                                    {!dataDetail.route_code
                                      ? "-"
                                      : dataDetail.route_code}
                                  </span>
                                </div>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>
                                    Showroom trực thuộc
                                  </p>
                                  <span style={{textTransform: "capitalize"}}>
                                    {!dataDetail.show_rom_name
                                      ? "-"
                                      : dataDetail.show_rom_name}
                                  </span>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>
                                    Nhân viên phụ trách
                                  </p>
                                  <span>
                                    {!dataDetail.sale_man_name
                                      ? "-"
                                      : dataDetail.sale_man_name}
                                  </span>
                                </div>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Skeleton>
                    </Col>
                    <Col span={24}>
                      <Skeleton
                        loading={isLoading}
                        active
                        paragraph={{rows: 3}}
                      >
                        <Row gutter={[0, 24]} style={{marginTop: 20}}>
                          <Col span={24}>
                            <StyledITileHeading minFont="12px" maxFont="16px">
                              Thời gian hiệu lực
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[12, 0]}>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>Bắt đầu</p>
                                  <span>
                                    {FormatterDay.dateFormatWithString(
                                      dataDetail.start_date,
                                      "#hhh#:#mm#:#ss# #DD#-#MM#-#YYYY#"
                                    )}
                                  </span>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>Kết thúc</p>
                                  <span>
                                    {FormatterDay.dateFormatWithString(
                                      dataDetail.end_date,
                                      "#hhh#:#mm#:#ss# #DD#-#MM#-#YYYY#"
                                    )}
                                  </span>
                                </div>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Skeleton>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={16}>
                <div
                  style={{
                    height: 600,
                    padding: 30,
                    borderLeft: "1px solid #D8DBDC",
                  }}
                >
                  <Skeleton loading={isLoading} active paragraph={{rows: 15}}>
                    <Row>
                      <Col span={24}>
                        <div
                          className="flex justify-end"
                          style={{marginBottom: 20}}
                        >
                          <Tooltip title="Tìm kiếm tên khách hàng hoặc số điện thoại">
                            <ISearch
                              className="cursor"
                              placeholder="Tìm kiếm tên khách hàng hoặc số điện thoại"
                              onPressEnter={(e) => {
                                setIsLoadingTable(true);
                                setFilterTable({
                                  ...filterTable,
                                  keyword: e.target.value,
                                  page: 1,
                                });
                              }}
                              icon={
                                <div
                                  style={{
                                    display: "flex",
                                    width: 42,
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}
                                >
                                  <img
                                    src={images.icSearch}
                                    style={{width: 16, height: 16}}
                                    alt=""
                                  />
                                </div>
                              }
                            />
                          </Tooltip>
                        </div>
                      </Col>
                      <Col span={24}>
                        <ITable
                          style={{width: "100%"}}
                          bordered={true}
                          backgroundWhite={true}
                          scroll={{
                            x: !dataTable.data.length ? 0 : 1600,
                            y: 360,
                          }}
                          data={dataTable.data}
                          columns={columns}
                          defaultCurrent={1}
                          indexPage={filterTable.page}
                          sizeItem={dataTable.size}
                          maxpage={dataTable.total}
                          loading={isLoadingTable}
                          onChangePage={(page) => {
                            setIsLoadingTable(true);
                            setFilterTable({...filterTable, page: page});
                          }}
                          locale={{
                            emptyText: (
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                <Empty description="Không có khách hàng"/>
                              </div>
                            ),
                          }}
                        ></ITable>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}
