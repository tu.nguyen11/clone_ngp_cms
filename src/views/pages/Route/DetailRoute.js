import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  message,
} from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  IImage,
  ILoading,
  ICascader,
  IDatePicker,
  ISelectAllStatus,
} from "../../components";
import { priceFormat } from "../../../utils";

import { colors } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
// import HistoryVisitSalesman from './HistoryVisitSalesman';
const widthScreen = window.innerWidth;

export default class DetailRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      visible: false,
      loading: false,
      url: "",
    };
    this.idRouter = Number(this.props.match.params.id);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentDidMount() {
    this._APIgetDetailSalesman(this.idRouter);
  }
  _APIRemoveRouter = async (obj) => {
    try {
      const data = APIService._postRemoveRouter(obj);
      message.success("Xóa thành công");
      this.props.history.push("/routePage");
    } catch (err) {
      console.log(err);
    }
  };
  _APIgetDetailSalesman = async (idAgency) => {
    try {
      const data = await APIService._getDetailRouter(idAgency);

      this.setState({
        data: data.route_detail,
        url: data.url,
        loading: true,
      });
    } catch (error) {}
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: true,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  render() {
    const { data } = this.state;
    return (
      <Container fluid>
        <div>
          <Row className="mt-3" xs="auto">
            <ITitle
              level={1}
              title="Chi tiết tuyến"
              style={{ color: colors.main, fontWeight: "bold" }}
            />
          </Row>
          {!this.state.loading ? (
            <ILoading />
          ) : (
            <Row>
              <Col>
                <Row
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    paddingTop: 32,
                    paddingBottom: 32,
                  }}
                  xs="auto"
                  sm={12}
                  md={12}
                >
                  <IButton
                    icon={ISvg.NAME.WRITE}
                    title="Chỉnh sửa"
                    onClick={() =>
                      this.props.history.push(
                        "/createRouter/edit/" + this.idRouter
                      )
                    }
                    color={colors.main}
                    style={{ marginRight: 20 }}
                  />

                  <IButton
                    icon={ISvg.NAME.DELETE}
                    title="Xóa"
                    color={colors.oranges}
                    onClick={() => {
                      let idRemove = [];
                      idRemove.push(this.idRouter);
                      const objRemove = {
                        route_id: idRemove,
                      };

                      this._APIRemoveRouter(objRemove);
                    }}
                    style={{}}
                  />
                </Row>
                <Row
                  style={{
                    maxWidth: 815,
                    minWidth: 615,
                    flex: 1,
                    overflow: "hidden",
                  }}
                  className="p-0"
                >
                  <Col
                    style={{ background: colors.white }}
                    className="pl-5 ml-0"
                  >
                    <Row className="p-0 m-0" style={{}}>
                      <Col
                        className="pl-0 pr-0 pt-0 m-0"
                        style={{ paddingBottom: 40 }}
                      >
                        <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                          <ITitle
                            level={2}
                            title="Thông tin tuyến"
                            style={{ color: colors.black, fontWeight: "bold" }}
                          />
                        </Row>
                        <Row className="p-0 m-0" xs="auto">
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Tên tuyến"}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ITitle
                                style={{ marginTop: 10 }}
                                level={4}
                                title={data.route_name}
                              />
                            </Row>
                          </Col>
                          <Col
                            className="p-0"
                            style={{ marginLeft: 28, marginTop: 0 }}
                          >
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Trạng thái"}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ITitle
                                style={{ marginTop: 10 }}
                                level={4}
                                title={data.status_name}
                              />
                            </Row>
                          </Col>
                        </Row>

                        <Row className="p-0 m-0 mt-3" xs="auto">
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Mã tuyến"}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ITitle
                                style={{ marginTop: 10 }}
                                level={4}
                                title={data.route_code}
                              />
                            </Row>
                          </Col>
                          <Col
                            className="p-0"
                            style={{ marginLeft: 28, marginTop: 0 }}
                          >
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Kho trực thuộc"}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ITitle
                                style={{ marginTop: 10 }}
                                level={4}
                                title={data.agency_under}
                              />
                            </Row>
                          </Col>
                        </Row>

                        <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                          <ITitle
                            level={4}
                            title="Danh sách đại lý"
                            style={{
                              color: colors.black,
                              fontWeight: "bold",
                              flex: 1,
                            }}
                          />
                          <div
                            style={{ marginRight: 30 }}
                            // onClick={() => alert('ahihi')}
                          >
                            <ISvg
                              name={ISvg.NAME.VIEWLIST}
                              width={20}
                              height={18}
                            />
                          </div>
                        </Row>
                        {data.listShop.length === 0 ? (
                          <div style={{}}>
                            <span>Không có danh sách đại lý trực thuộc</span>
                          </div>
                        ) : (
                          data.listShop.map((item, index) => {
                            return (
                              <Row
                                className="p-0 mt-3 ml-0 mb-0 mr-0"
                                xs="auto"
                              >
                                <Col className="p-0 m-0" xs={7}>
                                  <ITitle
                                    level={4}
                                    title={item.shop_name}
                                    style={{ fontWeight: 500 }}
                                  />
                                </Col>

                                <Col
                                  className="p-0"
                                  style={{
                                    marginLeft: 28,
                                    marginTop: 0,
                                    marginRight: 30,
                                    textAlign: "right",
                                  }}
                                >
                                  <ITitle
                                    level={4}
                                    title={item.frequency_code}
                                    style={{
                                      fontWeight: 600,
                                      textAlign: "right",
                                    }}
                                  />
                                </Col>
                              </Row>
                            );
                          })
                        )}
                      </Col>
                      <Col className="m-0 p-0 " xs="auto">
                        <div
                          style={{
                            width: 1,
                            height: "100%",
                            background: colors.gray._300,
                          }}
                        ></div>
                      </Col>
                      <Col
                        className="pt-0 pl-4 pr-0 pb-0 ml-3 mr-3 mt-0 mb-0"
                        style={{
                          paddingBottom: 40,
                        }}
                        xs={3}
                      >
                        <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                          <ITitle
                            level={2}
                            title="Thời hạn hiệu lực"
                            style={{ color: colors.black, fontWeight: "bold" }}
                          />
                        </Row>
                        <Row className="p-0 mt-4 ml-0 mr-0">
                          <ITitle
                            level={4}
                            title="Bắt đầu"
                            style={{ color: colors.black, fontWeight: "bold" }}
                          />
                        </Row>
                        <Row className="p-0 mt-1 ml-0 mr-0">
                          <ITitle
                            title={FormatterDay.dateFormatWithString(
                              data.start_date,
                              "#YYYY#-#MM#-#DD# #hhhh#:#mm#:#ss#"
                            )}
                          />
                        </Row>
                        <Row className="p-0 mt-4 ml-0 mr-0">
                          <ITitle
                            level={4}
                            title="Kết thúc"
                            style={{ color: colors.black, fontWeight: "bold" }}
                          />
                        </Row>
                        <Row className="p-0 mt-1  ml-0 mr-0">
                          <ITitle
                            title={FormatterDay.dateFormatWithString(
                              data.end_date,
                              "#YYYY#-#MM#-#DD# #hhhh#:#mm#:#ss#"
                            )}
                          />
                        </Row>
                        <Row className="p-0 mt-4 ml-0 mr-0">
                          <div
                            style={{ display: "flex", flexDirection: "column" }}
                          >
                            <ITitle
                              title="Nhân viên phụ trách"
                              level={4}
                              style={{
                                color: colors.black,
                                fontWeight: "bold",
                                marginBottom: 10,
                              }}
                            />
                            <ITitle
                              title={
                                data.sale_man_name === "null"
                                  ? "-"
                                  : data.sale_man_name
                              }
                              level={4}
                              style={{ color: colors.black }}
                            />
                          </div>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              {/* <Modal
                title="Lịch sử viếng thăm"
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                width={widthScreen - 300}
                footer={null}
              >
                <HistoryVisitSalesman idSalesman={this.IDsalesMan} />
              </Modal> */}
            </Row>
          )}
        </div>
      </Container>
    );
  }
}
