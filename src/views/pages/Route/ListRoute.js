import { Table, Typography, Avatar, Button, message } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
  ISelectAllWareHouse,
  ISelectAllStatus,
} from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { APIService } from "../../../services";
import { thisExpression } from "@babel/types";
import QueueAnim from "rc-queue-anim";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 6;
  } else widthColumn = (widthScreen - 300) / 6;
  return widthColumn * type;
}

export default class ListRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      width: 0,
      loading: false,
      bool: false,
      status: 1,
      dataTable: [],
      data: {},
      keySearch: "",
      category_id: 0,
      type: 0,
      loadingTable: true,
      page: 1,
      value: "", // Check here to configure the default column
      arrayRemove: [],
      warehouse_id: 0,
      key: "",
      keyStatus: 2,
    };
    this.handleResize = this.handleResize.bind(this);
  }
  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }
  componentDidMount() {
    this._APIgetListRouter(
      this.state.warehouse_id,
      this.state.keyStatus,
      this.state.key,
      this.state.page
    );
    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }
  _APIgetListRouter = async (warehouse_id, status, key, page) => {
    try {
      const data = await APIService._getListRouter(
        this.state.warehouse_id,
        this.state.keyStatus,
        this.state.key,
        this.state.page
      );

      let router = data.manage_route_response;
      router.map((item, index) => {
        router[index].stt = (this.state.page - 1) * 10 + index + 1;
      });
      this.setState({
        dataTable: router,
        data: data,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];
    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[item].id);
    });

    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys, arrayRemove: arrayID });
  };
  _APIRemoveRouter = async (obj) => {
    try {
      const data = APIService._postRemoveRouter(obj);
      message.success("Xóa thành công");

      this.setState(
        {
          selectedRowKeys: [],
        },
        () =>
          this._APIgetListRouter(
            this.state.warehouse_id,
            this.state.keyStatus,
            this.state.key,
            this.state.page
          )
      );
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Mã tuyến"),
        dataIndex: "route_code",
        key: "route_code",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (route_code) => _renderColumns(route_code),
      },
      {
        title: _renderTitle("Tên tuyến"),
        dataIndex: "route_name",
        key: "route_name",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (route_name) => _renderColumns(route_name),
      },
      {
        title: _renderTitle("Kho trực thuộc"),
        dataIndex: "agency_under",
        key: "agency_under",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (agency_under) => _renderColumns(agency_under),
      },
      {
        title: _renderTitle("Số đại lý trực thuộc"),
        dataIndex: "shop_under",
        key: "shop_under",
        align: "center",
        width: _widthColumn(this.state.width, 1),

        render: (shop_under) => <ITitle level={4} title={shop_under} />,
      },
      {
        title: _renderTitle("Mã nhân viên"),
        dataIndex: "staff_code",
        key: "staff_code",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (staff_code) => _renderColumns(staff_code),
      },

      {
        title: _renderTitle("Nhân viên phụ trách"),
        dataIndex: "staff_charge",
        key: "staff_charge",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (staff_charge) =>
          _renderColumns(staff_charge === "null" ? "-" : staff_charge),
      },
      {
        title: _renderTitle("Trạng Thái"),
        dataIndex: "status_name",
        key: "status_name",
        align: "center",
        width: _widthColumn(this.state.width, 1),

        render: (status_name) => _renderColumns(status_name),
      },
      // {
      //   title: "",
      //   dataIndex: "id",
      //   key: "id",
      //   align: "right",
      //   width: 100,
      //   fixed: widthScreen <= 1400 ? "right" : "",

      //   render: (id) => (
      //     <Row>
      //       <Col
      //         onClick={() =>
      //           this.props.history.push("/createRouter/edit/" + id)
      //         } // idAndStt.index vị trí của mảng
      //         className="cell"
      //       >
      //         <ISvg
      //           name={ISvg.NAME.WRITE}
      //           width={20}
      //           height={20}
      //           fill={colors.icon.default}
      //         />
      //       </Col>
      //       <Col
      //         onClick={() =>
      //           // this.props.history.push({
      //           //   pathname: "/productDetail/" + id,
      //           //   // search: "?query=abc",
      //           //   state: { productId: id }
      //           // })
      //           this.props.history.push("/detailRoute/" + id)
      //         }
      //         className="cursor"
      //       >
      //         <ISvg
      //           name={ISvg.NAME.CHEVRONRIGHT}
      //           width={11}
      //           height={20}
      //           fill={colors.icon.default}
      //         />
      //       </Col>
      //     </Row>
      //   ),
      // },
    ];

    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Quản lý tuyến"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <ISearch
            onChange={(e) => {
              this.state.key = e.target.value;

              this.setState({
                key: this.state.key,
              });
            }}
            placeholder=""
            onPressEnter={() =>
              this._APIgetListRouter(
                this.state.warehouse_id,
                this.state.keyStatus,
                this.state.key,
                this.state.page
              )
            }
            icon={
              <div
                style={{
                  display: "flex",
                  width: 42,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                className="cursor"
                onClick={() =>
                  this._APIgetListRouter(
                    this.state.warehouse_id,
                    this.state.keyStatus,
                    this.state.key,
                    this.state.page
                  )
                }
                className="cursor"
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>
        <Row className="p-0 mt-4">
          <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
            <Row className="m-0 p-0 center">
              <ISvg
                name={ISvg.NAME.EXPERIMENT}
                width={20}
                height={20}
                fill={colors.icon.default}
              />
              <div style={{ marginLeft: 25 }}>
                <ITitle title="Kho hàng" level={4} />
              </div>
            </Row>
            <Row className="m-0 p-0 ml-4">
              <ISelectAllWareHouse
                // style={{
                //   background: colors.white,
                //   borderStyle: 'solid',
                //   borderWidth: 1,
                //   borderColor: colors.line,
                // }}
                // className="ml-4"
                onSearch={() => {}}
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
                value={this.state.warehouse_id}
                onChange={(key) => {
                  this.setState(
                    {
                      warehouse_id: key,
                      loadingTable: true,
                    },
                    () =>
                      this._APIgetListRouter(
                        this.state.warehouse_id,
                        this.state.keyStatus,
                        this.state.key,
                        this.state.page
                      )
                  );
                }}
                showSearch
              />
            </Row>
            <Row className="m-0 p-0 ml-4">
              <ISelectAllStatus
                // style={{
                //   background: colors.white,
                //   borderStyle: 'solid',
                //   borderWidth: 1,
                //   borderColor: colors.line,
                // }}
                // className="ml-4"
                all={true}
                value={this.state.keyStatus}
                onChange={(key) => {
                  this.state.keyStatus = key;
                  this.setState({}, () =>
                    this._APIgetListRouter(
                      this.state.warehouse_id,
                      this.state.keyStatus,
                      this.state.key,
                      this.state.page
                    )
                  );
                }}
                typeRouter
              />
            </Row>
          </Row>
          <Row className="m-0 p-0">
            <IButton
              icon={ISvg.NAME.ARROWUP}
              title="Tạo mới"
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => this.props.history.push("/createRouter/create/0")}
            />
            <IButton
              icon={ISvg.NAME.DOWLOAND}
              title="Xuất file"
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => {
                message.warning("Chức năng đang cập nhật");
              }}
            />

            <IButton
              icon={ISvg.NAME.DELETE}
              title="Xóa"
              color={colors.oranges}
              style={{}}
              onClick={() => {
                if (this.state.arrayRemove.length == 0) {
                  message.warning("Bạn chưa chọn mã tuyến.");
                  return;
                }
                const objRemove = {
                  route_id: this.state.arrayRemove,
                };

                this._APIRemoveRouter(objRemove);
              }}
            />
          </Row>
        </Row>

        <Row className="mt-4">
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%" }}
            rowSelection={rowSelection}
            sizeItem={this.state.data.size}
            indexPage={this.state.page}
            maxpage={this.state.data.total}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                  loadingTable: true,
                },
                () =>
                  this._APIgetListRouter(
                    this.state.warehouse_id,
                    this.state.status,
                    this.state.key,
                    this.state.page
                  )
              );
            }}
            // scroll={{ x: this.state.width }}
          />
        </Row>
      </Container>
    );
  }
}
