import React, { useState, useEffect } from "react";
import { Row, Col, Modal, Empty, BackTop, message, Skeleton } from "antd";
import { colors } from "../../../assets";
import { priceFormat } from "../../../utils";
import { useHistory, useParams } from "react-router-dom";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import { ITitle, IButton, ITableHtml, ISvg } from "../../components";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../components/common/Font/font";
import { IInputTextArea } from "../../components/common";

export default function DetailOrderS1(props) {
  const { id } = useParams();
  const history = useHistory();
  const [dataDetail, setDataDetail] = useState({
    orderS1: {
      debt: {},
    },
    admin_action: {},
    image: "",
    product_gift: [],
    product: [],
  });
  const [isLoadingAPI, setLoadingAPI] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);

  const [reasons, setReasons] = useState("");
  const [isModal, setIsModal] = useState(false);
  const [isModalResponse, setIsModalResponse] = useState(false);
  const [isModalNote, setIsModalNote] = useState(false);

  const [listHistoryResponse, setListHistoryResponse] = useState([]);

  const _getAPIDetail = async (id) => {
    try {
      const data = await APIService._getDetailOrderS1(id);
      setDataDetail({ ...data });
      setLoadingAPI(false);
    } catch (err) {
      console.log(err);
    }
  };

  const getListOrderC1NoteResponse = async (obj) => {
    try {
      const data = await APIService._getListOrderC1NoteResponse(obj);

      setListHistoryResponse([...data.list_note_response_pkd]);
    } catch (err) {
      console.log(err);
    }
  };

  const postRejectOrderS1 = async (obj) => {
    try {
      const data = await APIService._postRejectOrderS1(obj);
      setIsModal(false);
      setLoadingButton(false);
      message.success("Hủy đơn hàng thành công");
      history.push("/orderPageS1");
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postApprovalOrderS1 = async (obj) => {
    try {
      const data = await APIService._postApprovalOrderS1(obj);
      setLoadingButton(false);
      message.success("Duyệt thành công");
      history.push("/orderPageS1");
    } catch (error) {
      setLoadingButton(false);
      console.log(error);
    }
  };

  useEffect(() => {
    _getAPIDetail(id);
  }, [id]);

  let headerTable = [
    {
      name: "Tên phiên bản",
      align: "left",
    },
    {
      name: "Mã phiên bản",
      align: "left",
    },
    {
      name: "ĐVT",
      align: "left",
    },
    {
      name: "Số lượng",
      align: "center",
    },
    {
      name: "Đơn giá",
      align: "right",
    },
    {
      name: "Thành tiền",
      align: "right",
    },
    {
      name: "Giảm giá/Chiết khấu",
      align: "center",
    },
    {
      name: "Tổng cộng",
      align: "tight",
    },
    // {
    //   name: "Nợ trong hạn",
    //   align: "right",
    // },
    // {
    //   name: "Nợ quá hạn",
    //   align: "right",
    // },
    // {
    //   name: "Nợ đến hạn",
    //   align: "right",
    // },
  ];

  const footMoney = () => (
    <tfoot style={{ border: "none" }}>
      <tr>
        <td
          style={{
            borderLeft: "1px solid white",
            background: "white",
            width: 300,
          }}
        ></td>
        <td
          className="td-table"
          style={{ fontStyle: "italic", fontWeight: "bold" }}
          colSpan={5}
        >
          Tổng tạm tính
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
          }}
        >
          {!dataDetail.orderS1.total_buy
            ? "0"
            : priceFormat(dataDetail.orderS1.total_buy)}
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white", background: "white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            borderBottom: "1px solid #7A7B7B",
          }}
          colSpan={5}
        >
          Tổng khuyến mãi
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            borderBottom: "1px solid #7A7B7B",
          }}
        >
          {!dataDetail.orderS1.total_money_discount
            ? "0"
            : priceFormat(dataDetail.orderS1.total_money_discount)}
        </td>
      </tr>
      <tr>
        <td
          style={{
            borderLeft: "1px solid white",
            borderRight: "1px solid white",
            background: "white",
          }}
        ></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            color: colors.main,
          }}
          colSpan={5}
        >
          Tổng tiền
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            color: colors.main,
          }}
        >
          {!dataDetail.orderS1.total_money
            ? "0"
            : priceFormat(dataDetail.orderS1.total_money)}
        </td>
      </tr>
    </tfoot>
  );

  const bodyTableProduct = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        className="tr-table"
        className="td-table"
        style={{ textAlign: "center" }}
      >
        <td
          colSpan="8"
          style={{
            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
            padding: "24px 0px",
          }}
        >
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table-1"
            id="id_name"
            style={{ width: 300, wordBreak: "break-all" }}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <div
                style={{
                  padding: 6,
                  border: "1px solid #D8DBDC",
                  marginRight: 10,
                }}
              >
                <img
                  src={dataDetail.image + item.image}
                  style={{
                    width: 40,
                    height: 40,
                  }}
                />
              </div>
              <span style={{ fontWeight: 600 }}>
                {!item.name ? "-" : item.name}
              </span>
            </div>
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", wordBreak: "break-all", width: 150 }}
          >
            {!item.product_code ? "-" : item.product_code}
          </td>
          <td className="td-table" style={{ textAlign: "left" }}>
            {!item.attribute_name ? "-" : item.attribute_name}
          </td>
          <td className="td-table" style={{ textAlign: "center" }}>
            {!item.quantity ? "0" : priceFormat(item.quantity)}
          </td>
          <td className="td-table" style={{ textAlign: "right" }}>
            {!item.price ? "0" : priceFormat(item.price)}
          </td>
          <td className="td-table" style={{ textAlign: "right" }}>
            {!item.total_buy ? "0" : priceFormat(item.total_buy)}
          </td>
          <td className="td-table" style={{ textAlign: "center" }}>
            {!item.total_money_discount
              ? "0"
              : priceFormat(item.total_money_discount)}
          </td>
          <td className="td-table" style={{ textAlign: "right" }}>
            {!item.total_money ? "0" : priceFormat(item.total_money)}
          </td>
        </tr>
      ))
    );
  };

  const headerTableProduct = (headerTable) => {
    return (
      <tr className=" scroll">
        {headerTable.map((item, index) => (
          <th
            className={index === 0 ? "th-table-1" : "th-table"}
            style={{ textAlign: item.align, fontWeight: "700" }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  let headerTableGift = [
    { name: "Hàng tặng", align: "center" },
    {
      name: "Số lượng",
      name1: "(Quy cách nhỏ nhất)",
      align: "center",
      isBr: true,
    },
  ];

  const headerTableGiftUI = (headerTableGift) => {
    return (
      <tr>
        {headerTableGift.map((item, index) =>
          item.isBr ? (
            <th
              className={index === 0 ? "th-table-1" : "th-table"}
              style={{ textAlign: item.align, fontWeight: 700 }}
            >
              {item.name}
              <br />
              <span style={{ fontSize: 12, fontWeight: "normal" }}>
                {item.name1}
              </span>
            </th>
          ) : (
            <th
              className={index === 0 ? "th-table-1" : "th-table"}
              style={{ textAlign: item.align, fontWeight: 700 }}
            >
              {item.name}
            </th>
          )
        )}
      </tr>
    );
  };

  const bodyTable = (contentTableGift) => {
    return contentTableGift.length === 0 ? (
      <tr className="tr-table" style={{ textAlign: "center" }}>
        <td
          colSpan="2"
          style={{
            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
            padding: "24px 0px",
          }}
        >
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      contentTableGift.map((item, index) => (
        <tr className="tr-table">
          <td className="td-table-1 outline">
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <div
                style={{
                  padding: 6,
                  border: "1px solid #D8DBDC",
                  marginRight: 12,
                }}
              >
                <img
                  src={dataDetail.image + item.image}
                  alt="sản phẩm"
                  style={{
                    width: 40,
                    height: 40,
                  }}
                />
              </div>
              <span style={{ fontWeight: 500 }}>
                {!item.name ? "-" : item.name}
              </span>
            </div>
          </td>
          <td className="td-table" style={{ textAlign: "center" }}>
            {!item.quantity ? "0" : priceFormat(item.quantity)}
          </td>
        </tr>
      ))
    );
  };

  let headerTableHistory = [
    { name: "Ngày tạo", align: "center" },
    {
      name: "Nội dung",
      align: "center",
    },
    { name: "Người ghi chú", align: "center" },
  ];

  const headerTableHistoryRender = (headerTableHistory) => {
    return (
      <tr>
        {headerTableHistory.map((item, index) => (
          <th
            className={"th-table-1"}
            style={{
              textAlign: item.align,
              fontWeight: 700,
              width: index === 1 ? "50%" : "25%",
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableHistory = (contentTableHistory) => {
    return contentTableHistory.length === 0 ? (
      <tr className="tr-table" style={{ textAlign: "center" }}>
        <td
          colSpan="3"
          style={{
            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
            padding: "24px 0px",
          }}
        >
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      contentTableHistory.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table-1"
            style={{ textAlign: "center", width: "25%" }}
          >
            {!item.create_date
              ? "-"
              : FormatterDay.dateFormatWithString(
                  item.create_date,
                  "#DD#/#MM#/#YYYY# #hhhh#:#mm#:#ss#"
                )}
          </td>
          <td
            className="td-table-1"
            style={{
              width: "50%",
              wordBreak: "break-all",
            }}
          >
            {!item.history_note ? "-" : item.history_note}
          </td>
          <td
            className="td-table"
            style={{ width: "25%", textAlign: "center" }}
          >
            {!item.user_note ? "-" : item.user_note}
          </td>
        </tr>
      ))
    );
  };

  if (!dataDetail) {
    return null;
  }

  return (
    <div style={{ width: "100%", overflow: "auto", padding: "36px 36px" }}>
      <div style={{ minWidth: 1600 }}>
        <Row>
          <Col flex="auto">
            <div>
              <ITitle
                level={1}
                title="Chi tiết đơn hàng ĐLC1"
                style={{ color: colors.main, fontWeight: "bold" }}
              />
            </div>
          </Col>
        </Row>
        <BackTop description={1000} />
        <div style={{ margin: "32px 0px" }}>
          <Row>
            <Col flex="auto">
              <div className="flex justify-end">
                {dataDetail.admin_action.approve !== 1 ? null : (
                  <IButton
                    title={"Duyệt đơn hàng"}
                    styleHeight={{
                      width: 160,
                    }}
                    color={colors.main}
                    onClick={() => {
                      const obj = {
                        id: Number(id),
                      };
                      postApprovalOrderS1(obj);
                    }}
                  />
                )}
                {dataDetail.admin_action.reject !== 1 ? null : (
                  <IButton
                    title={"Từ chối"}
                    styleHeight={{
                      width: 160,
                      marginLeft: 15,
                    }}
                    loading={loadingButton}
                    color={colors.oranges}
                    icon={ISvg.NAME.CROSS}
                    onClick={() => {
                      setIsModal(true);
                    }}
                  />
                )}
              </div>
            </Col>
          </Row>
          <div style={{ margin: "32px 0px 12px 0px" }}>
            <Row gutter={[25, 12]} type="flex">
              <Col
                span={4}
                style={{
                  background: colors.white,
                  marginTop: 6,
                  paddingLeft: isLoadingAPI ? 40 : 0,
                  paddingRight: isLoadingAPI ? 40 : 0,
                  paddingTop: isLoadingAPI ? 30 : 0,
                  paddingBottom: isLoadingAPI ? 30 : 0,
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                }}
              >
                <Skeleton
                  active
                  loading={isLoadingAPI}
                  paragraph={{ rows: 16 }}
                >
                  <div style={{ paddingBottom: 24 }}>
                    <Row gutter={[0, 20]}>
                      <Col>
                        <div
                          style={{
                            padding: "22px 40px 0px 40px",
                          }}
                        >
                          <StyledITileHeading minFont="10px" maxFont="18px">
                            Thông tin đơn hàng
                          </StyledITileHeading>
                        </div>
                      </Col>
                      <Col>
                        <div
                          style={{
                            padding: "0px 40px",
                          }}
                        >
                          <p
                            style={{
                              fontWeight: 600,
                            }}
                          >
                            PKD yêu cầu duyệt
                          </p>
                          <div
                            style={{
                              maxHeight: 150,
                              overflowY: "auto",
                              overflowX: "hidden",
                              color: colors.darkRed,
                            }}
                          >
                            {!dataDetail.orderS1.reason_request_approve
                              ? "-"
                              : dataDetail.orderS1.reason_request_approve}
                          </div>
                        </div>
                      </Col>
                      <Col
                        style={{
                          background: colors.green._3,
                          paddingLeft: 0,
                          paddingRight: 0,
                        }}
                      >
                        <div
                          style={{
                            padding: "0px 40px",
                            paddingTop: 8,
                          }}
                        >
                          <p style={{ fontWeight: 600 }}>Trạng thái đơn hàng</p>
                          <span style={{ color: colors.main, fontWeight: 700 }}>
                            {!dataDetail.orderS1.status_name
                              ? "-"
                              : dataDetail.orderS1.status_name}
                          </span>
                        </div>
                      </Col>

                      <div style={{ padding: "0px 40px 0px 40px" }}>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Người tạo đơn</p>
                          <span>
                            {!dataDetail.orderS1.order_creator
                              ? "-"
                              : dataDetail.orderS1.order_creator}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Mã đơn hàng</p>
                          <span>
                            {!dataDetail.orderS1.order_child_code
                              ? "-"
                              : dataDetail.orderS1.order_child_code}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Đại lý đặt hàng</p>
                          <span>
                            {!dataDetail.orderS1.user_agency_shop_name
                              ? "-"
                              : dataDetail.orderS1.user_agency_shop_name}
                          </span>
                        </Col>
                        <Col>
                          <div style={{ marginTop: 6 }}>
                            <p style={{ fontWeight: 600 }}>Mã đại lý</p>
                            <span>
                              {!dataDetail.orderS1.user_agency_code
                                ? "-"
                                : dataDetail.orderS1.user_agency_code}
                            </span>
                          </div>
                        </Col>
                        <Col>
                          <div style={{ marginTop: 6 }}>
                            <p style={{ fontWeight: 600 }}>Cấp bậc</p>
                            <span>
                              {!dataDetail.orderS1.user_agency_membership
                                ? "-"
                                : dataDetail.orderS1.user_agency_membership}
                            </span>
                          </div>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Ngày tạo đơn</p>
                          <span>
                            {!dataDetail.orderS1.create_date
                              ? "-"
                              : FormatterDay.dateFormatWithString(
                                  dataDetail.orderS1.create_date,
                                  "#DD#/#MM#/#YYYY# #hhh#:#mm#:#ss#"
                                )}
                          </span>
                        </Col>

                        <Col>
                          <p style={{ fontWeight: 600 }}>Tên người nhận hàng</p>
                          <span>
                            {!dataDetail.orderS1.delivery_name ||
                            dataDetail.orderS1.delivery_name === ""
                              ? "-"
                              : dataDetail.orderS1.delivery_name}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Số điện thoại</p>
                          <span>
                            {!dataDetail.orderS1.phone
                              ? "-"
                              : dataDetail.orderS1.phone}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Biển số xe</p>
                          <span>
                            {!dataDetail.orderS1.license_plates
                              ? "-"
                              : dataDetail.orderS1.license_plates}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Địa chỉ nhận hàng</p>
                          <span>
                            {!dataDetail.orderS1.address
                              ? "-"
                              : dataDetail.orderS1.address}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Nhân viên phụ trách</p>
                          <span>
                            {!dataDetail.orderS1.sale_man_name
                              ? "-"
                              : dataDetail.orderS1.sale_man_name}
                          </span>
                        </Col>
                        <Col>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                            }}
                          >
                            <p style={{ fontWeight: 600 }}>Ghi chú</p>
                            {/* <span
                              style={{
                                fontStyle: "italic",
                                textDecoration: "underline",
                                cursor: "pointer",
                                color: colors.main,
                              }}
                              onClick={async () => {
                                setIsModalNote(true);
                                const obj = {
                                  type: 1,
                                  order_child_code:
                                    dataDetail.orderS1.order_child_code,
                                };
                                await getListOrderC1NoteResponse(obj);
                              }}
                            >
                              Lịch sử
                            </span> */}
                          </div>

                          <div
                            style={{
                              maxHeight: 150,
                              overflow: "auto",
                              color: "#22232b",
                            }}
                          >
                            {!dataDetail.orderS1.note
                              ? "-"
                              : dataDetail.orderS1.note}
                          </div>
                        </Col>

                        <Col>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                            }}
                          >
                            <p style={{ fontWeight: 600 }}>PĐV phản hồi</p>
                          </div>
                          <div
                            style={{
                              maxHeight: 150,
                              overflowY: "auto",
                              overflowX: "hidden",
                              color: colors.darkRed,
                            }}
                          >
                            {!dataDetail.orderS1.center_tower_response
                              ? "-"
                              : dataDetail.orderS1.center_tower_response}
                          </div>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "flex-end",
                            }}
                          >
                            <span
                              style={{
                                fontStyle: "italic",
                                textDecoration: "underline",
                                cursor: "pointer",
                                color: colors.main,
                              }}
                              onClick={async () => {
                                setIsModalResponse(true);
                                const obj = {
                                  type: 2,
                                  order_child_code:
                                    dataDetail.orderS1.order_child_code,
                                };
                                await getListOrderC1NoteResponse(obj);
                              }}
                            >
                              Lịch sử ghi chú/phản hồi
                            </span>
                          </div>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontWeight: 600,
                            }}
                          >
                            Lý do hủy
                          </p>
                          <div
                            style={{
                              maxHeight: 150,
                              overflowY: "auto",
                              overflowX: "hidden",
                              color: colors.darkRed,
                            }}
                          >
                            {!dataDetail.orderS1.reason_agency_cancel
                              ? "-"
                              : dataDetail.orderS1.reason_agency_cancel}
                          </div>
                        </Col>
                      </div>
                    </Row>
                  </div>
                </Skeleton>
              </Col>
              <Col span={20} style={{ paddingBottom: 0 }}>
                <div
                  style={{
                    padding: "0px 40px",
                    paddingTop: 22,
                    background: colors.white,
                    height: "100%",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <Skeleton
                    active
                    loading={isLoadingAPI}
                    paragraph={{ rows: 16 }}
                  >
                    <Row>
                      {/* {dataDetail.orderS1.book_order_type === 1 ? null : (
                        <> */}
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="18px">
                          Thông tin công nợ
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <Row
                          gutter={[18, 0]}
                          style={{ marginTop: 22, marginBottom: 25 }}
                        >
                          <Col span={3}>
                            <p style={{ fontWeight: 600 }}>Hạn mức khả dụng</p>
                            <span
                              style={{
                                color:
                                  dataDetail.orderS1.book_order_type === 2
                                    ? "#fb2626"
                                    : null,
                              }}
                            >
                              {!dataDetail.orderS1.debt.hmkd
                                ? "0"
                                : priceFormat(dataDetail.orderS1.debt.hmkd)}
                            </span>
                          </Col>
                          <Col span={3}>
                            <p style={{ fontWeight: 600 }}>Công nợ cuối kỳ</p>
                            <span>
                              {!dataDetail.orderS1.debt.noCuoiKy
                                ? "0"
                                : priceFormat(dataDetail.orderS1.debt.noCuoiKy)}
                            </span>
                          </Col>
                          <Col span={3}>
                            <p style={{ fontWeight: 600 }}>Nợ quá hạn</p>
                            <span
                              style={{
                                color:
                                  dataDetail.orderS1.book_order_type === 3
                                    ? "#fb2626"
                                    : null,
                              }}
                            >
                              {!dataDetail.orderS1.debt.noQuaHan
                                ? "0"
                                : priceFormat(dataDetail.orderS1.debt.noQuaHan)}
                            </span>
                          </Col>
                          <Col span={3}>
                            <p style={{ fontWeight: 600 }}>Hạn mức gối đầu</p>
                            <span>
                              {!dataDetail.orderS1.debt.hmGoiDau
                                ? "0"
                                : priceFormat(dataDetail.orderS1.debt.hmGoiDau)}
                            </span>
                          </Col>
                          <Col span={3}>
                            <p style={{ fontWeight: 600 }}>Phần vượt HMKD</p>
                            <span>
                              {!dataDetail.orderS1.over_hmkd
                                ? "0"
                                : priceFormat(dataDetail.orderS1.over_hmkd)}
                            </span>
                          </Col>
                          {dataDetail.orderS1.book_order_type !== 3 ? null : (
                            <>
                              <Col span={3}>
                                <p style={{ fontWeight: 600 }}>
                                  Số tiền cam kết
                                </p>
                                <span>
                                  {!dataDetail.orderS1.debt.price_commit
                                    ? "0"
                                    : priceFormat(
                                        dataDetail.orderS1.debt.price_commit
                                      )}
                                </span>
                              </Col>
                              <Col span={3}>
                                <p style={{ fontWeight: 600 }}>
                                  Thời gian cam kết
                                </p>
                                <span>
                                  {!dataDetail.orderS1.debt.appointment_date
                                    ? "-"
                                    : FormatterDay.dateFormatWithString(
                                        dataDetail.orderS1.debt
                                          .appointment_date,
                                        "#DD#/#MM#/#YYYY#"
                                      )}
                                </span>
                              </Col>
                            </>
                          )}
                        </Row>
                      </Col>
                      {/* </>
                      )} */}
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="18px">
                          Phiên bản đặt mua
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[18, 0]}>
                          <Col span={19}>
                            <div
                              style={{
                                marginTop: 20,
                              }}
                            >
                              <Row>
                                <Col span={24}>
                                  <ITableHtml
                                    childrenBody={bodyTableProduct(
                                      dataDetail.product
                                    )}
                                    childrenHeader={headerTableProduct(
                                      headerTable
                                    )}
                                    isfoot={
                                      dataDetail.product.length === 0
                                        ? false
                                        : true
                                    }
                                    childrenFoot={footMoney()}
                                  />
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col span={5}>
                            <div style={{ marginTop: 20 }}>
                              <Row>
                                <Col span={24}>
                                  <ITableHtml
                                    childrenBody={bodyTable(
                                      dataDetail.product_gift
                                    )}
                                    childrenHeader={headerTableGiftUI(
                                      headerTableGift
                                    )}
                                  />
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
      <Modal
        visible={isModal}
        footer={null}
        width={350}
        centered={true}
        closable={false}
        style={{ paddingTop: 0 }}
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Duyệt đơn hàng
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div style={{ width: "100%", marginTop: 5 }}>
                <IInputTextArea
                  border={true}
                  style={{
                    minHeight: 100,
                    maxHeight: 300,
                    border: "1px solid #rgba(255, 255, 255, 0.5)",
                  }}
                  value={reasons}
                  placeholder="Nhập lý do từ chối"
                  onChange={(e) => {
                    setReasons(e.target.value);
                  }}
                />
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -60,
              right: -1,
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <div style={{ width: 165 }}>
              <IButton
                color={colors.oranges}
                title="Hủy bỏ"
                styleHeight={{ minWidth: 0 }}
                onClick={() => {
                  setReasons("");
                  setIsModal(false);
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{ width: 165 }}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                styleHeight={{ minWidth: 0 }}
                icon={ISvg.NAME.CHECKMARK}
                loading={loadingButton}
                onClick={() => {
                  if (reasons === "") {
                    message.error("Vui lòng nhập lý do từ chối!");
                    return;
                  }

                  const obj = {
                    id: Number(id),
                    reason: reasons,
                  };
                  postRejectOrderS1(obj);
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
      {/* <Modal
        visible={isModalNote}
        footer={null}
        width={800}
        centered={true}
        closable={false}
        onCancel={async () => {
          setIsModalNote(false);
        }}
        style={{ paddingTop: 0 }}
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Lịch sử ghi chú
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div style={{ width: "100%", marginTop: 5 }}>
                <ITableHtml
                  childrenBody={bodyTableHistory(listHistoryNote)}
                  childrenHeader={headerTableHistoryRender(headerTableHistory)}
                />
              </div>
            </Col>
          </Row>
        </div>
      </Modal> */}
      <Modal
        visible={isModalResponse}
        footer={null}
        width={800}
        centered={true}
        closable={false}
        onCancel={async () => {
          setIsModalResponse(false);
        }}
        style={{ paddingTop: 0 }}
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Lịch sử ghi chú/Phản hồi
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div style={{ width: "100%", marginTop: 5 }}>
                <ITableHtml
                  childrenBody={bodyTableHistory(listHistoryResponse)}
                  childrenHeader={headerTableHistoryRender(headerTableHistory)}
                />
              </div>
            </Col>
          </Row>
        </div>
      </Modal>
    </div>
  );
}
