import React, { useState, useEffect } from "react";
import { Row, Col, message, Tooltip, Modal } from "antd";
import { useHistory } from "react-router-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IDatePicker,
} from "../../components";
import { IInputTextArea } from "../../components/common";
import FormatterDay from "../../../utils/FormatterDay";
import { images, colors } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { APIService } from "../../../services";
import { priceFormat } from "../../../utils";
const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

export default function OrderDLC1Page() {
  const history = useHistory();
  const [data, setData] = useState({
    order: [],
  });
  const [dataBranch, setDataBranch] = useState([]);
  const [loadingSelect, setLoadingSelect] = useState(true);
  const [loadingTable, setLoadingTable] = useState(true);
  const [filterTable, setFilterTable] = useState({
    startValue: startValue,
    endValue: NewDate.setHours(23, 59, 59, 999),
    page: 1,
    status: 0,
    search: "",
    user_agency_id: 0,
  });

  const [filterListS1, setFilterListS1] = useState({
    status: 2,
    page: 1,
    city_id: 0,
    // district_id: 0,
    agency_id: -1,
    keySearch: "",
    membership_id: 0,
  });

  const [reasons, setReasons] = useState("");
  const [isModal, setIsModal] = useState(false);
  const [checkReview, setCheckReview] = useState(1);
  const [dataReview, setDataReview] = useState([
    {
      name: "Giao các đơn hàng đã chọn",
      active: true,
      type: 1,
    },
    {
      name: "Từ chối đơn hàng đã chọn",
      active: false,
      type: 2,
    },
  ]);

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },

    getCheckboxProps: (record) => ({
      disabled: record.disabled,
    }),
  };

  const [listStatus, setListStatus] = useState([]);

  const _fetchAPIListOrderS1 = async (filter) => {
    try {
      const data = await APIService._getListOrderS1(
        filter.page,
        filter.startValue,
        filter.endValue,
        filter.user_agency_id,
        filter.status,
        filter.search
      );
      data.order.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        item.disabled = item.status !== 2 ? true : false;
        item.rowTable = JSON.stringify({
          id: item.id,
          status: item.status,
        });
      });
      setData(data);
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIListOrderS1(filterTable);
  }, [filterTable]);

  useEffect(() => {
    _getListStatus();
  }, []);

  const _getListStatus = async () => {
    try {
      const data = await APIService._getListStatusAppSerice(1);

      var dataNew = data.order_status.map((item, index) => {
        const value = item.name;
        const key = item.id;
        // StatusPare.push({ value, key });
        return {
          key,
          value,
        };
      });
      let dataNewTemp = [];
      dataNew.map((item, index) => {
        if (item.value === "" || !item.value) {
          return;
        }
        dataNewTemp.push(item);
      });

      dataNewTemp.unshift({
        value: "Tất cả",
        key: 0,
      });
      setListStatus(dataNewTemp);
      setLoadingSelect(false);
    } catch (error) {}
  };

  // const _getAPIListAgencyS1 = async () => {
  //   try {
  //     const data = await APIService._getListAgencyS1(
  //       filterListS1.status,
  //       filterListS1.page,
  //       filterListS1.city_id,
  //       filterListS1.agency_id,
  //       filterListS1.keySearch,
  //       filterListS1.membership_id
  //     );
  //     const dataNew = data.useragency.map((item, index) => {
  //       const key = item.id;
  //       const value = item.shop_name;
  //       return {
  //         key,
  //         value,
  //       };
  //     });
  //     dataNew.unshift({
  //       value: "Tất cả",
  //       key: 0,
  //     });
  //     setDataBranch(dataNew);
  //   } catch (error) {}
  // };

  const _getAPIListS1 = async () => {
    try {
      const data = await APIService._getListS1(0);
      let agencyS1 = data.list_s1.map((item, index) => {
        let key = item.id;
        let value = item.shop_name;
        return {
          key,
          value,
        };
      });
      agencyS1.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDataBranch(agencyS1);
    } catch (error) {
      console.log(error);
    }
  };

  const postApprovalListOrderS1 = async (obj) => {
    try {
      const data = await APIService._postApprovalListOrderS1(obj);
      setSelectedRowKeys([]);
      setIsModal(false);
      message.success("Duyệt thành công");
      // history.push("/orderPageS1");
      await _fetchAPIListOrderS1(filterTable);
    } catch (error) {
      console.log(error);
    }
  };

  const postRejectListOrderS1 = async (obj) => {
    try {
      const data = await APIService._postRejectListOrderS1(obj);
      setSelectedRowKeys([]);
      setIsModal(false);
      message.success("Hủy đơn hàng thành công");
      // history.push("/orderPageS1");
      await _fetchAPIListOrderS1(filterTable);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    _getAPIListS1();
  }, []);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Mã đơn",
      dataIndex: "order_code",
      key: "order_code",
      render: (order_code) =>
        !order_code ? (
          "-"
        ) : (
          <Tooltip title={order_code}>
            <span>{order_code}</span>
          </Tooltip>
        ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "create_date",
      key: "create_date",
      render: (create_date) => (
        <span>
          {!create_date || create_date <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
                create_date,
                "#DD#/#MM#/#YYYY# #hhh#:#mm#"
              )}
        </span>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
    },

    {
      title: "Tên đại lý",
      dataIndex: "user_agency_name",
      key: "user_agency_name",
      render: (user_agency_name) =>
        !user_agency_name ? (
          "-"
        ) : (
          <Tooltip title={user_agency_name}>
            <span>{user_agency_name}</span>
          </Tooltip>
        ),
    },
    {
      title: "Mã ĐLC1",
      dataIndex: "user_agency_code",
      key: "user_agency_code",
      render: (user_agency_code) =>
        !user_agency_code ? (
          "-"
        ) : (
          <Tooltip title={user_agency_code}>
            <span>{user_agency_code}</span>
          </Tooltip>
        ),
    },
    {
      title: "Người tạo đơn",
      dataIndex: "order_creator",
      key: "order_creator",
      render: (order_creator) =>
        !order_creator ? (
          "-"
        ) : (
          <Tooltip title={order_creator}>
            <span>{order_creator}</span>
          </Tooltip>
        ),
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <span>{!phone ? "-" : phone}</span>,
    },
    {
      title: "Tổng tiền",
      dataIndex: "total_money",
      key: "total_money",
      render: (total_money) => (
        <span>{!total_money ? "-" : priceFormat(total_money)}</span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Danh sách đơn hàng Đại lý Cấp 1
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã đơn hàng, tên đại lý, mã đại lý">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo mã đơn hàng, tên đại lý, mã đại lý"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    search: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row>
          <Col span={24}>
            <Row>
              <Col span={21}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{
                      marginLeft: 25,
                      marginRight: 25,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filterTable.startValue}
                      to={filterTable.endValue}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          startValue: date[0]._d.setHours(0, 0, 0),
                          endValue: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>
                  <ITitle
                    title="Lọc theo"
                    level={4}
                    style={{
                      marginLeft: 25,
                      marginRight: 25,
                    }}
                  />

                  <div style={{ width: 200 }}>
                    <ISelect
                      defaultValue="Đại lý"
                      data={dataBranch}
                      select={true}
                      // minWidthDropdown={200}
                      loading={loadingSelect}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          user_agency_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{ margin: "0px 15px 0px 15px", width: 200 }}>
                    <ISelect
                      defaultValue="Trạng thái"
                      data={listStatus}
                      select={true}
                      // minWidthDropdown={200}
                      loading={loadingSelect}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          status: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>
              <Col span={3}>
                <div className="flex justify-end">
                  <IButton
                    title={"Duyệt đơn hàng"}
                    styleHeight={{
                      width: 160,
                    }}
                    color={colors.main}
                    onClick={() => {
                      let arrParse = selectedRowKeys.map((item) => {
                        let itemParse = JSON.parse(item);
                        return itemParse;
                      });
                      let arr = arrParse.filter((item) => {
                        return item.status !== 2;
                      });
                      if (arr.length > 0) {
                        message.warning(
                          "Vui lòng chọn các đơn hàng ở trạng thái chờ duyệt!"
                        );
                      } else {
                        let arrId = arrParse.map((item) => item.id);
                        console.log([...arrId]);
                        setIsModal(true);
                      }
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <div style={{ marginTop: 50 }}>
              <Row>
                <ITable
                  columns={columns}
                  data={data.order}
                  style={{ width: "100%" }}
                  defaultCurrent={1}
                  sizeItem={data.page_size}
                  rowKey="rowTable"
                  indexPage={filterTable.page}
                  maxpage={data.total_record}
                  loading={loadingTable}
                  rowSelection={rowSelection}
                  onRow={(record, rowIndex) => {
                    return {
                      onClick: (event) => {
                        const dataJSON =
                          event.currentTarget.attributes[1].value;
                        const obj = JSON.parse(dataJSON);

                        history.push(`/orderPageS1/detail/${obj.id}`);
                      }, // click row
                      onMouseDown: (event) => {
                        const dataJSON =
                          event.currentTarget.attributes[1].value;
                        const obj = JSON.parse(dataJSON);
                        if (event.button == 2 || event == 4) {
                          window.open(
                            `/orderPageS1/detail/${obj.id}`,
                            "_blank"
                          );
                        }
                      },
                    };
                  }}
                  onChangePage={(page) => {
                    setLoadingTable(true);
                    setFilterTable({ ...filterTable, page: page });
                  }}
                ></ITable>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
      <Modal
        visible={isModal}
        footer={null}
        width={350}
        centered={true}
        closable={false}
        style={{ paddingTop: 0 }}
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Duyệt đơn hàng
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div style={{ padding: "0px 50px", marginTop: 15 }}>
                {dataReview.map((item, index) => {
                  return (
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        paddingTop: index === 0 ? 0 : 15,
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        let data = [...dataReview];
                        data.map((item, index1) => {
                          if (index == index1) {
                            item.active = true;
                          } else {
                            item.active = false;
                          }
                          setCheckReview(data[index].type);
                          setDataReview(data);
                          setReasons("");
                        });
                      }}
                    >
                      <div
                        style={{
                          width: 20,
                          height: 20,
                          border: item.active
                            ? "5px solid" + `${colors.main}`
                            : "1px solid" + `${colors.line_2}`,
                          borderRadius: 10,
                        }}
                      />
                      <div style={{ flex: 1, marginLeft: 10 }}>
                        <span
                          style={{ fontSize: 14, color: colors.text.black }}
                        >
                          {item.name}
                        </span>
                      </div>
                    </div>
                  );
                })}
              </div>
            </Col>

            <Col span={24}>
              <div style={{ width: "100%", marginTop: 5 }}>
                <IInputTextArea
                  disabled={checkReview === 1 ? true : false}
                  border={true}
                  value={reasons}
                  style={{
                    minHeight: 100,
                    maxHeight: 300,
                    border: "1px solid #rgba(255, 255, 255, 0.5)",
                  }}
                  placeholder="Nhập lý do từ chối"
                  onChange={(e) => {
                    setReasons(e.target.value);
                  }}
                />
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -60,
              right: -1,
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <div style={{ width: 165 }}>
              <IButton
                color={colors.oranges}
                title="Hủy bỏ"
                styleHeight={{ minWidth: 0 }}
                onClick={() => {
                  setReasons("");
                  setIsModal(false);
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{ width: 165 }}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                styleHeight={{ minWidth: 0 }}
                icon={ISvg.NAME.CHECKMARK}
                onClick={() => {
                  let arrParse = selectedRowKeys.map((item) => {
                    let itemNew = JSON.parse(item);
                    return itemNew;
                  });

                  let arrID = arrParse.map((item) => item.id);
                  if (checkReview === 2) {
                    if (reasons === "") {
                      message.error("Vui lòng nhập lý do từ chối!");
                      return;
                    }
                    const obj = {
                      list_id: arrID,
                      reason: reasons,
                    };
                    postRejectListOrderS1(obj);
                  } else {
                    const obj = {
                      list_id: arrID,
                    };
                    postApprovalListOrderS1(obj);
                  }
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
