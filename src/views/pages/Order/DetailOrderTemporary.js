import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton, Empty } from "antd";
import { ITitle, ITableHtml } from "../../components";
import { colors } from "../../../assets";
import { priceFormat } from "../../../utils";
import { StyledITileHeading } from "../../components/common/Font/font";
import { APIService } from "../../../services";

export default function DetailOrderTemporary(props) {
  const order_code = Number(props.match.params.id);

  const [isLoadingAPI, setLoadingAPI] = useState(false);

  const [dataDetailTemporary, setDataDetailTemporary] = useState({
    order_info: {
      list_order_product: [],
    },
  });

  const _getAPIDetailOrderTemporary = async (order_code) => {
    try {
      const data = await APIService._getDetailOrderTemporary(order_code);

      setDataDetailTemporary({ ...data });
      setLoadingAPI(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    _getAPIDetailOrderTemporary(order_code);
  }, []);

  const headerTable = [
    {
      name: "Tên phiên bản",
      align: "left",
    },
    {
      name: "Mã phiên bản",
      align: "left",
    },
    {
      name: "ĐVT",
      align: "left",
    },
    {
      name: "Số lượng",
      align: "center",
    },
    {
      name: "Đơn giá",
      align: "center",
    },
    {
      name: "Thành tiền",
      align: "right",
    },
  ];

  const bodyTableProduct = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        height="100px"
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
          flexDirection: "row",
        }}
      >
        <td colspan="6" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => (
        <tr className="tr-table">
          <td className="td-table-1" style={{ width: 350 }}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <div
                style={{
                  padding: 6,
                  marginRight: 12,
                  border: "1px solid #D8DBDC",
                }}
              >
                <img
                  src={item.image}
                  style={{ width: 35, height: 35, align: "center" }}
                />
              </div>
              <span style={{ fontWeight: 500 }}>
                {!item.product_name ? "-" : item.product_name}
              </span>
            </div>
          </td>
          <td className="td-table">
            <div style={{ textAlign: "left" }}>
              <span style={{ color: colors.blackChart }}>
                {!item.product_code ? "-" : item.product_code}
              </span>
            </div>
          </td>
          <td className="td-table">
            <div style={{ textAlign: "left" }}>
              <span style={{ color: colors.blackChart }}>
                {!item.attribuite ? "-" : item.attribuite}
              </span>
            </div>
          </td>
          <td className="td-table">
            <div style={{ textAlign: "center" }}>
              <span style={{ color: colors.blackChart }}>
                {!item.quantity ? "-" : priceFormat(item.quantity)}
              </span>
            </div>
          </td>
          <td className="td-table">
            <div style={{ textAlign: "center" }}>
              <span style={{ color: colors.blackChart }}>
                {!item.price ? "0đ" : priceFormat(item.price) + "đ"}
              </span>
            </div>
          </td>
          <td className="td-table">
            <div style={{ textAlign: "right" }}>
              <span style={{ color: colors.blackChart }}>
                {!item.total_buy ? "0đ" : priceFormat(item.total_buy) + "đ"}
              </span>
            </div>
          </td>
        </tr>
      ))
    );
  };

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="scroll" style={{ borderBottom: "0px solid white" }}>
        {headerTable.map((item, index) => (
          <th
            className={index === 0 ? "th-table-1" : "th-table"}
            style={{
              textAlign: item.align,
              color: colors.blackChart,
              fontWeight: "bold",
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const footMoney = () => {
    return (
      <tfoot style={{ border: "none" }}>
        <tr>
          <td style={{ borderLeft: "1px solid white", width: 350 }}></td>
          <td
            className="td-table"
            colSpan={3}
            style={{
              color: colors.main,
              fontWeight: "bold",
              fontStyle: "italic",
            }}
          >
            Tổng thành tiền
          </td>
          <td
            className="td-table"
            style={{
              fontWeight: "bold",
              textAlign: "right",
              borderRight: "1px solid white",
              color: colors.main,
            }}
          >
            {priceFormat(dataDetailTemporary.order_info.total_money + "đ")}
          </td>
        </tr>
      </tfoot>
    );
  };

  return (
    <div style={{ width: "100%" }}>
      <Row>
        <Col>
          <div>
            <ITitle
              style={{
                color: colors.main,
                marginTop: 8,
                fontSize: 20,
                fontWeight: "bold",
              }}
              title="Thông tin đơn hàng tạm ĐLC1"
            />
          </div>
        </Col>
      </Row>
      <div style={{ margin: "32px 0px" }}>
        <div style={{ margin: "50px 0px 12px 0px" }}>
          <Row gutter={[30, 12]} type="flex">
            <Col span={6}>
              <div
                style={{
                  background: colors.white,
                  marginTop: 6,
                  paddingLeft: isLoadingAPI ? 12 : 0,
                  paddingRight: isLoadingAPI ? 12 : 0,
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  height: "100%",
                  minHeight: "480px",
                }}
              >
                <Skeleton active loading={isLoadingAPI} padding={{ rows: 16 }}>
                  <div style={{ paddingBottom: 24 }}>
                    <Row gutter={[0, 20]}>
                      <div style={{ padding: "0px 40px", paddingTop: 22 }}>
                        <Col
                          span={24}
                          style={{ paddingLeft: 0, paddingRight: 0 }}
                        >
                          <StyledITileHeading minFont="10px" maxFont="18px">
                            Thông tin đơn hàng
                          </StyledITileHeading>
                        </Col>
                      </div>
                      <div style={{ padding: "0px 40px" }}>
                        <Col>
                          <div style={{ marginTop: 50 }}>
                            <p style={{ fontWeight: 600 }}> Đại lý đặt hàng </p>
                            <span>
                              {!dataDetailTemporary.order_info.shop_name
                                ? "-"
                                : dataDetailTemporary.order_info.shop_name}
                            </span>
                          </div>
                        </Col>
                        <Col>
                          <div>
                            <p style={{ fontWeight: 600 }}> Mã đại lý </p>
                            <span>
                              {!dataDetailTemporary.order_info.dms_code
                                ? "-"
                                : dataDetailTemporary.order_info.dms_code}
                            </span>
                          </div>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}> Cấp bậc </p>
                          <span>
                            {!dataDetailTemporary.order_info.membership_name
                              ? "-"
                              : dataDetailTemporary.order_info.membership_name}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}> Số điện thoại </p>
                          <span>
                            {!dataDetailTemporary.order_info.phone
                              ? "-"
                              : dataDetailTemporary.order_info.phone}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}> Ghi chú </p>
                          <span>
                            {!dataDetailTemporary.order_info.note
                              ? "-"
                              : dataDetailTemporary.order_info.note}
                          </span>
                        </Col>
                      </div>
                    </Row>
                  </div>
                </Skeleton>
              </div>
            </Col>
            <Col span={18}>
              <div
                style={{
                  background: colors.white,
                  marginTop: 6,
                  paddingLeft: isLoadingAPI ? 12 : 0,
                  paddingRight: isLoadingAPI ? 12 : 0,
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  height: "100%",
                  minHeight: "480px",
                }}
              >
                <Skeleton active loading={isLoadingAPI} padding={{ rows: 16 }}>
                  <div style={{ paddingBottom: 24 }}>
                    <Row gutter={[0, 20]}>
                      <div style={{ padding: "0px 40px", paddingTop: 22 }}>
                        <Col
                          span={24}
                          style={{ paddingLeft: 0, paddingRight: 0 }}
                        >
                          <StyledITileHeading minFont="10px" maxFont="18px">
                            Danh sách phiên bản
                          </StyledITileHeading>
                          <ITableHtml
                            style={{ marginTop: "30px" }}
                            childrenHeader={headerTableProduct(headerTable)}
                            childrenBody={bodyTableProduct(
                              dataDetailTemporary.order_info.list_order_product
                            )}
                            isfoot={
                              dataDetailTemporary.order_info.list_order_product
                                .length === 0
                                ? false
                                : true
                            }
                            childrenFoot={footMoney()}
                          ></ITableHtml>
                        </Col>
                      </div>
                    </Row>
                  </div>
                </Skeleton>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}
