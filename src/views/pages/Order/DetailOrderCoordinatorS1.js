import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton, Modal } from "antd";
import { colors } from "../../../assets";
import { useWindowSize, priceFormat } from "../../../utils";
import { Link, useHistory, useParams } from "react-router-dom";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import { ITitle, ITableHtml, IButton } from "../../components";
import { StyledITileHeading } from "../../components/common/Font/font";
import UseModalViewInfo from "../../containers/hookcustom/useModalViewInfo";

export default function DetailOrderCoordinatorS1(props) {
  const { order_code, length_S2, id, order_codeS2 } = useParams();
  const history = useHistory();
  const [data, setdata] = useState({
    orderS1: {},
    image: "",
    product: [],
    totalProduct: {},
    totalShipMent: {},
    product_gift: [],
  });
  const [isModal, setIsModal] = useState(false);
  const [isLoadingAPI, setLoadingAPI] = useState(true);
  const [isLogout, setIsLogout] = useState(false);
  const [url_img, setUrl_img] = useState("");

  const _getAPIDetail = async (id) => {
    try {
      const data = await APIService._getDetailCoorDinatiorOrderS1(id);
      setdata(data);
      setLoadingAPI(false);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    _getAPIDetail(id);
  }, [id]);

  let headerTable = [
    {
      name: "Tên sản phẩm",
      align: "left",
    },
    {
      name: "Quy cách",
      align: "left",
    },
    {
      name: "Số lượng",
      align: "left",
    },
    {
      name: "Đơn giá",
      align: "right",
    },
    {
      name: "Thành tiền",
      align: "right",
    },
    {
      name: "Lít /Kg",
      align: "left",
    },
    {
      name: "Giảm giá",
      align: "right",
    },
    {
      name: "Thanh toán",
      align: "right",
    },
    {
      name: "Lô /HSD",
      align: "left",
    },
    {
      name: "Số xuất kho",
      align: "left",
    },
    {
      name: "SL Xác nhận",
      align: "left",
    },
    {
      name: "Thành tiền",
      align: "right",
    },
    {
      name: "Lít /Kg",
      align: "left",
    },
    {
      name: "Giảm giá",
      align: "right",
    },
    {
      name: "Thanh toán",
      align: "right",
    },
  ];

  const headerTitleProduct = (headerTitle) => {
    return (
      <tr className="tr-table">
        {headerTitle.map((item, index) => (
          <th
            className="title-head"
            colSpan={index === 1 ? "7" : index === 2 ? "7" : "1"}
          >
            {item}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr className="tr-table">
        <td className="td-table-1">
          <img
            src={data.image + item.image}
            style={{ width: 50, height: 50 }}
            alt="sản phẩm"
          />
          <span style={{ fontWeight: 600, marginLeft: 12 }}>{item.name}</span>
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "left",
            // verticalAlign: item.list_shipment.length > 1 ? "top" : null,
          }}
        >
          {item.attribute_name}
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "left",
            // verticalAlign: item.list_shipment.length > 1 ? "top" : null,
          }}
        >
          {item.quantity}
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "right",
            // verticalAlign: item.list_shipment.length > 1 ? "top" : null,
          }}
        >
          {priceFormat(item.price)} đ
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "right",
            // verticalAlign: item.list_shipment.length > 1 ? "top" : null,
          }}
        >
          {priceFormat(item.total_money)} đ
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "left",
            // verticalAlign: item.list_shipment.length > 1 ? "top" : null,
          }}
        >
          {item.weight}
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "right",
            // verticalAlign: item.list_shipment.length > 1 ? "top" : null,
          }}
        >
          {priceFormat(item.discount)} đ
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "right",
            borderRight: "1px solid rgba(122, 123, 123, 0.5)",
            // verticalAlign: item.list_shipment.length > 1 ? "top" : null,
          }}
        >
          {priceFormat(item.payment_price)} đ
        </td>
        <td className="td-table" style={{ textAlign: "left" }}>
          {item.list_shipment.map((Item, Index) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: Index > 0 ? 30 : 0,
                }}
              >
                <span className="text_table_order">{Item.shipment_name} </span>
              </div>
            );
          })}
        </td>
        <td className="td-table" style={{ textAlign: "left" }}>
          {item.list_shipment.map((Item, Index) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: Index > 0 ? 30 : 0,
                }}
              >
                <span className="text_table_order">
                  {Item.product_quantity_warehouse}
                </span>
              </div>
            );
          })}
        </td>
        <td className="td-table" style={{ textAlign: "left" }}>
          {item.list_shipment.map((Item, Index) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: Index > 0 ? 30 : 0,
                }}
              >
                <span className="text_table_order">
                  {Item.product_quantity_confirm}
                </span>
              </div>
            );
          })}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {item.list_shipment.map((Item, Index) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: Index > 0 ? 30 : 0,
                }}
              >
                <span className="text_table_order">
                  {priceFormat(Item.total_money_product)} đ
                </span>
              </div>
            );
          })}
        </td>
        <td className="td-table" style={{ textAlign: "left" }}>
          {item.list_shipment.map((Item, Index) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: Index > 0 ? 30 : 0,
                }}
              >
                <span className="text_table_order" className="td-table">
                  {Item.total_weight}
                </span>
              </div>
            );
          })}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {item.list_shipment.map((Item, Index) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: Index > 0 ? 30 : 0,
                }}
              >
                <span className="text_table_order">
                  {priceFormat(Item.product_money_discount)} đ
                </span>
              </div>
            );
          })}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {item.list_shipment.map((Item, Index) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: Index > 0 ? 30 : 0,
                }}
              >
                <span className="text_table_order">
                  {priceFormat(Item.total_product_payment)} đ
                </span>
              </div>
            );
          })}
        </td>
      </tr>
    ));
  };

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="tr-table" style={{ background: "#EEFFF6" }}>
        {headerTable.map((item, index) => (
          <td
            className={index === 0 ? "th-table-1" : "th-table"}
            style={{ textAlign: item.align }}
          >
            {item.name}
          </td>
        ))}
      </tr>
    );
  };

  const headerTitle = [
    "Danh sách sản phẩm",
    "Số lượng yêu cầu",
    "Số lượng phản hồi",
  ];

  const headerTitleGift = ["Danh sách quà tặng", "Số lượng yêu cầu"];

  let headerTableGift = [
    {
      name: "Tên sản phẩm",
      align: "left",
    },
    {
      name: "Quy cách",
      align: "left",
    },
    {
      name: "Số lượng",
      align: "left",
    },
    {
      name: "Đơn giá",
      align: "right",
    },
    {
      name: "Thành tiền",
      align: "right",
    },
    {
      name: "Lít /Kg",
      align: "left",
    },
    {
      name: "Giảm giá",
      align: "right",
    },
    {
      name: "Thanh toán",
      align: "right",
    },
  ];

  const bodyTableGift = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr
        className="tr-table"
        rows={item.list_shipment.length === 0 ? 1 : item.list_shipment.length}
      >
        <td className="td-table-1">
          <img
            src={data.image + item.image}
            style={{ width: 50, height: 50 }}
            alt="sản phẩm"
          />
          <span style={{ fontWeight: 600, marginLeft: 12 }}>{item.name}</span>
        </td>
        <td className="td-table" style={{ textAlign: "left" }}>
          {item.attribute_name}
        </td>
        <td className="td-table" style={{ textAlign: "left" }}>
          {item.quantity}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {priceFormat(item.price)}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {priceFormat(item.total_money)}
        </td>
        <td className="td-table" style={{ textAlign: "left" }}>
          {item.weight}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {item.discount}
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "right",
            borderRight: "1px solid rgba(122, 123, 123, 0.5)",
          }}
        >
          {priceFormat(item.payment_price) + "đ"}
        </td>
      </tr>
    ));
  };

  const footMoney = (totalProduct, totalShipment) => (
    <tfoot style={{ border: "none" }}>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{ fontStyle: "italic", fontWeight: "bold" }}
          colSpan={3}
        >
          Tổng tạm tính
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
          }}
          colSpan={4}
        >
          {priceFormat(totalProduct.total_price)} đ
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
          }}
          colSpan={7}
        >
          {priceFormat(totalShipment.total_price)} đ
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            borderBottom: "1px solid #7A7B7B",
          }}
          colSpan={3}
        >
          Tổng khuyến mãi
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            borderBottom: "1px solid #7A7B7B",
          }}
          colSpan={4}
        >
          {priceFormat(totalProduct.discount)} đ
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            borderBottom: "1px solid #7A7B7B",
          }}
          colSpan={7}
        >
          {priceFormat(totalShipment.discount)} đ
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            color: colors.main,
          }}
          colSpan={3}
        >
          Tổng thành tiền
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            color: colors.main,
          }}
          colSpan={4}
        >
          {priceFormat(totalProduct.total_buy_temporary)} đ
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            color: colors.main,
          }}
          colSpan={7}
        >
          {priceFormat(totalShipment.total_buy_temporary)} đ
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            borderBottom: "1px solid #7A7B7B",
            color: colors.main,
          }}
          colSpan={3}
        >
          Thuế VAT
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            borderBottom: "1px solid #7A7B7B",
            color: colors.main,
          }}
          colSpan={4}
        >
          {priceFormat(totalProduct.vat)} đ
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            borderBottom: "1px solid #7A7B7B",
            color: colors.main,
          }}
          colSpan={7}
        >
          {priceFormat(totalShipment.vat)} đ
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            color: colors.main,
          }}
          colSpan={3}
        >
          Tổng thanh toán
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            color: colors.main,
          }}
          colSpan={4}
        >
          {priceFormat(totalProduct.total_buy)} đ
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            color: colors.main,
          }}
          colSpan={7}
        >
          {priceFormat(totalShipment.total_buy)} đ
        </td>
      </tr>
    </tfoot>
  );

  if (!data) {
    return null;
  }
  return (
    <div style={{ overflow: "auto", padding: "36px 36px" }}>
      <div style={{ minWidth: 2000 }}>
        <Row>
          <Col flex="auto">
            <div>
              <ITitle
                level={1}
                title="Chi tiết đơn hàng Đại lý Cấp 1"
                style={{ color: colors.main, fontWeight: "bold" }}
              />
            </div>
          </Col>
        </Row>
        <div style={{ margin: "32px 0px" }}>
          <Row>
            <Col flex="auto"></Col>
          </Row>
          <div style={{ margin: "32px 0px 12px 0px" }}>
            <Row gutter={[25, 12]} type="flex">
              <Col
                span={4}
                style={{
                  background: colors.white,
                  marginTop: 6,
                  paddingLeft: isLoadingAPI ? 12 : 0,
                  paddingRight: isLoadingAPI ? 12 : 0,
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                }}
              >
                <Skeleton
                  active
                  loading={isLoadingAPI}
                  paragraph={{ rows: 16 }}
                >
                  <div style={{ paddingBottom: 24 }}>
                    <Row gutter={[0, 20]}>
                      <div style={{ padding: "0px 40px", paddingTop: 22 }}>
                        <Col
                          span={24}
                          style={{ paddingLeft: 0, paddingRight: 0 }}
                        >
                          <StyledITileHeading minFont="10px" maxFont="18px">
                            Thông tin đơn hàng
                          </StyledITileHeading>
                        </Col>
                      </div>
                      <Col
                        span={24}
                        style={{
                          background: colors.green._3,
                          paddingLeft: 0,
                          paddingRight: 0,
                        }}
                      >
                        <div style={{ padding: "0px 40px", paddingTop: 8 }}>
                          <p style={{ fontWeight: 600 }}>Trạng thái</p>
                          <span style={{ color: colors.main, fontWeight: 700 }}>
                            {data.orderS1.status_name}
                          </span>
                        </div>
                      </Col>
                      <div style={{ padding: "0px 40px" }}>
                        <Col>
                          <div style={{ marginTop: 6 }}>
                            <p style={{ fontWeight: 600 }}>Mã đơn hàng</p>
                            <span> {data.orderS1.code}</span>
                          </div>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Ngày tạo đơn</p>
                          <span>
                            {FormatterDay.dateFormatWithString(
                              data.orderS1.create_date,
                              "#DD#/#MM#/#YYYY# #hhhh#:#mm#:#ss#"
                            )}
                          </span>{" "}
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Đại lý đặt hàng</p>
                          <span>{data.orderS1.user_agency_fullname}</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Địa chỉ</p>
                          <span>{data.orderS1.address}</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>
                            Phòng kinh doanh tiếp nhận
                          </p>
                          <span>{data.orderS1.agency_name}</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Nhân viên xử lý</p>
                          <span>{data.orderS1.sale_man_name}</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Số điện thoai</p>

                          <span>{data.orderS1.phone}</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>
                            Hình thức thanh toán
                          </p>
                          <span>{data.orderS1.payment_name}</span>
                        </Col>

                        <Col>
                          <p style={{ fontWeight: 600 }}>Kho giao hàng</p>
                          <span>{data.orderS1.warehouse_name}</span>
                        </Col>

                        {Number(length_S2) === 0 ? null : (
                          <Col>
                            <IButton
                              title="Xem danh sách đơn S2"
                              color={colors.main}
                              onClick={() => {
                                setIsModal(true);
                              }}
                            />
                          </Col>
                        )}
                      </div>
                    </Row>
                  </div>
                </Skeleton>
              </Col>

              <Col span={20} style={{ paddingBottom: 0 }}>
                <div
                  style={{
                    padding: "30px 40px",
                    background: colors.white,
                    height: "100%",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <Skeleton
                    active
                    loading={isLoadingAPI}
                    paragraph={{ rows: 16 }}
                  >
                    <Row>
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="18px">
                          Sản phẩm đặt mua
                        </StyledITileHeading>
                      </Col>
                      <Col span={24} style={{ minWidth: 1600 }}>
                        <Row gutter={[18, 0]}>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <Row>
                                <Col span={24}>
                                  <ITableHtml
                                    childrenBody={bodyTableProduct(
                                      data.product
                                    )}
                                    childrenHeader={headerTableProduct(
                                      headerTable
                                    )}
                                    isfoot={true}
                                    isBorder={true}
                                    childrenTitle={headerTitleProduct(
                                      headerTitle
                                    )}
                                    childrenFoot={footMoney(
                                      data.totalProduct,
                                      data.totalShipMent
                                    )}
                                  />
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      {data.product_gift.length === 0 ? null : (
                        <Col span={24} style={{ minWidth: 1600 }}>
                          <Row gutter={[18, 0]}>
                            <Col span={24}>
                              <div style={{ marginTop: 12 }}>
                                <Row>
                                  <Col span={24}>
                                    <ITableHtml
                                      childrenBody={bodyTableGift(
                                        data.product_gift
                                      )}
                                      childrenHeader={headerTableProduct(
                                        headerTableGift
                                      )}
                                      isBorder={true}
                                      childrenTitle={headerTitleProduct(
                                        headerTitleGift
                                      )}
                                    />
                                  </Col>
                                </Row>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      )}
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
      <Modal
        visible={isModal}
        footer={null}
        width={1200}
        onCancel={() => setIsModal(false)}
        centered={true}
        closable={false}
      >
        {UseModalViewInfo(
          "Danh sách đơn S2",
          () => {
            setIsModal(false);
          },
          id,
          order_codeS2
        )}
      </Modal>
    </div>
  );
}
