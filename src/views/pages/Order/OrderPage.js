import { Table, Typography, Avatar, Button, message, Tooltip } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IExportExecl,
  IDatePicker,
} from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import { throwStatement, thisExpression } from "@babel/types";
import DetailOrder from "./DetailOrder";
import { OrderStatus } from "../../../constant";
const { Paragraph, Title } = Typography;
const widthScreen = window.innerWidth;
function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 12;
  } else widthColumn = (widthScreen - 300) / 12;
  return widthColumn * type;
}

const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

export default class OrderPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      loading: false,
      loadingTable: true,
      value: "",
      dataTable: [],
      width: 0,
      id: [],
      data: {},
      Cate: [],
      start_date: startValue,
      user_agency: "",
      end_date: NewDate.setHours(23, 59, 59, 999),
    };

    this.key = 0;
    this.page = 1;
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.onChangeButton = this.onChangeButton.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.onClickDetail = this.onClickDetail.bind(this);
  }
  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }

  onClickDetail = (index) => {
    // this.props.history.push({
    //   pathname: "/detailOrder/" + dataDetail.id,
    //   // search: "?query=abc",
    //   state: { orderId: dataDetail.id }
    // });
    this.props.history.push("/detailOrder/" + index);
  };
  _getListStatus = async () => {
    try {
      const data = await APIService._getListStatus();
      var StatusPare = [];
      StatusPare.push({
        key: 0,
        value: "Trạng thái",
      });
      var dataNew = data.order_status.map((item, index) => {
        const value = item.name;
        const key = item.id;
        // StatusPare.push({ value, key });
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      this.setState({
        Cate: dataNew,
      });
    } catch (error) {}
  };
  componentDidMount() {
    this._get();

    window.addEventListener("resize", this.handleResize);
  }

  _get = async () => {
    await this.getDate();
    await this._getListStatus();
    this._getAPIListOrder();
  };
  _getAPIListOrder = async () => {
    try {
      const data = await APIService._getListOrder(
        this.key,
        this.page,
        this.state.value,
        this.state.start_date,
        this.state.end_date
      );
      this.setState({
        loadingTable: true,
      });
      const dataParse = data.order.map((item, index) => {
        const stt = index + 1;
        const id = item.id;
        const dateCreate = item.dateCreate;
        const createDate = item.createDate;
        const statusName = item.statusName;
        const orderCode = item.orderCode;
        const deliveryName = item.deliveryName;
        const phone = item.phone;
        const totalMoney = item.totalMoney;
        const deliveryAddress = item.deliveryAddress;
        const userAgency = item.user_agency;
        const note = item.note;
        const saleManUser = {
          name: item.saleManName,
          src:
            item.avatar == ""
              ? "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
              : item.avatar,
        };
        const sttAndId = {
          stt: index,
          id: item.id,
        };
        return {
          stt,
          id,
          dateCreate,
          createDate,
          statusName,
          orderCode,
          deliveryName,
          deliveryAddress,
          phone,
          totalMoney,
          saleManUser,
          note,
          sttAndId,
          userAgency,
        };
      });
      this.setState({
        dataTable: dataParse,
        loadingTable: false,
        data: data,
      });
    } catch (err) {
      this.setState({
        loadingTable: false,
      });
    }
  };

  onSelectChange = (selectedRowKeys) => {
    // const Id = this.state.dataTable[selectedRowKeys[0]].id;
    let arrID = [];
    selectedRowKeys.map((item, index) => {
      const Id = this.state.dataTable[item].id;
      arrID.push(Id);
    });
    this.state.id = arrID;
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  onChangeSearch = ({ target: { value } }) => {
    this.setState({ value });
  };
  onChangeButton = () => {
    this.setState({
      loading: true,
    });

    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 3000);
  };
  filterOrder = async () => {
    try {
      const data = await APIService._getListOrder(
        this.state.value,
        this.page,
        this.state.value,
        this.state.start_date,
        this.state.end_date
      );

      const dataParse = data.order.map((item, index) => {
        const stt = (this.page - 1) * 10 + index + 1;
        const id = item.id;
        const dateCreate = item.dateCreate;
        const createDate = item.createDate;
        const statusName = item.statusName;
        const orderCode = item.orderCode;
        const deliveryName = item.deliveryName;
        const phone = item.phone;

        const totalMoney = item.totalMoney;
        const deliveryAddress = item.deliveryAddress;
        const note = item.note;
        const saleManUser = {
          name: item.saleManName,
          src:
            item.avatar == ""
              ? "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
              : item.avatar,
        };
        const sttAndId = {
          stt: index,
          id: item.id,
        };
        return {
          stt,
          id,
          dateCreate,
          createDate,
          statusName,
          orderCode,
          deliveryName,
          deliveryAddress,
          phone,
          totalMoney,
          saleManUser,
          note,
          sttAndId,
        };
      });
      this.setState({ dataTable: dataParse, data: data });
      console.log(data);
    } catch (error) {}
  };
  _updateOrder = async (status) => {
    // 1 là hủy ---- 2 là xóa
    try {
      const id = this.state.id;
      if (id.length != 0) {
        await APIService._UpdateOrder({ id, status });
        this.setState({
          selectedRowKeys: [],
        });
        this._getAPIListOrder();
        if (status == OrderStatus.CANCEL) {
          message.success("Đã hủy đơn hàng thành công");
        } else message.success("Đã xóa đơn hàng thành công");
      }
    } catch (error) {}
  };
  getDate = () => {
    let from = new Date(this.state.start_date);
    // from.setDate(1);
    from.setHours(0, 0, 0, 0);

    let to = new Date(this.state.end_date);
    to.setHours(23, 59, 59, 59);
    this.setState({
      end_date: to.getTime(),
      start_date: from.getTime(),
    });
  };

  _postAPIExportExecl = async (obj) => {
    const data = await APIService._postExportExecl(obj);
    let url = data.url;
    window.open(url);
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 80,
        render: (index) => _renderColumns((this.page - 1) * 10 + index),
      },
      {
        title: _renderTitle("Ngày tạo"),
        dataIndex: "dateCreate",
        key: "dateCreate",
        align: "center",
        render: (dateCreate) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(dateCreate, "#DD#/#MM#/#YYYY#")
          ),
      },
      {
        title: _renderTitle("Trạng thái"),
        dataIndex: "statusName",
        key: "statusName",
        align: "left",

        render: (statusName) => _renderColumns(statusName),
      },
      {
        title: _renderTitle("Mã đơn"),
        dataIndex: "orderCode",
        key: "orderCode",
        align: "left",

        render: (orderCode) => _renderColumns(orderCode),
      },
      {
        title: _renderTitle("Tên người nhận"),
        dataIndex: "deliveryName",
        key: "deliveryName",
        align: "left",

        render: (deliveryName) => _renderColumns(deliveryName),
      },
      {
        title: _renderTitle("Đại lý cấp 1"),
        dataIndex: "userAgency",
        key: "userAgency",
        align: "left",

        render: (userAgency) => _renderColumns(userAgency),
      },
      {
        title: _renderTitle("Địa chỉ"),
        dataIndex: "deliveryAddress",
        key: "deliveryAddress",
        align: "left",
        width: 200,
        render: (deliveryAddress) => _renderColumns(deliveryAddress),
      },
      {
        title: _renderTitle("Số điện thoại"),
        dataIndex: "phone",
        key: "phone",
        align: "left",

        render: (phone) => _renderColumns(phone),
      },
      {
        title: _renderTitle("Tổng tiền"),
        dataIndex: "totalMoney",
        key: "totalMoney",
        align: "left",

        render: (totalMoney) => _renderColumns(priceFormat(totalMoney) + "đ"),
      },
      {
        title: _renderTitle("Người tạo đơn"),
        dataIndex: "saleManUser",
        key: "saleManUser",
        align: "center",

        render: (saleManUser) => (
          <Row className="center">
            <ITitle level={4} title={saleManUser.name} />
          </Row>
        ),
      },
      {
        title: _renderTitle("Ghi chú"),
        dataIndex: "note",
        key: "note",
        align: "left",
        render: (note) => {
          return (
            <div
              style={{
                wordWrap: "break-word",
                wordBreak: "break-word",
                overflow: "hidden",
              }}
              className="ellipsis-text"
            >
              <ITitle level={4} title={note} />
            </div>
          );
        },
      },
      {
        title: "",
        dataIndex: "sttAndId",
        key: "sttAndId",
        align: "right",
        width: 60,
        fixed: widthScreen <= 1500 ? "right" : "",

        render: (sttAndId) => (
          <div
            style={{}}
            onClick={(e) => {
              e.stopPropagation();
              this.onClickDetail(sttAndId.id);
            }}
            className="cursor"
          >
            <ISvg
              name={ISvg.NAME.CHEVRONRIGHT}
              width={11}
              height={20}
              fill={colors.icon.default}
            />
          </div>
        ),
      },
    ];

    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Col className="m-0 p-0" style={{}}>
          <Row>
            <ITitle
              level={1}
              title="Danh sách đơn hàng Đại lý Cấp 2"
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                flex: 1,
                display: "flex",
                alignItems: "flex-end",
              }}
            />
            <Tooltip title="Tìm kiếm theo mã đơn hàng, tên đại lý cấp 1, mã đại lý cấp 1, số điện thoại đại lý cấp 1">
              <ISearch
                onChange={this.onChangeSearch}
                placeholder="Tìm kiếm theo mã đơn hàng, tên đại lý cấp 2, mã đại lý cấp 2, số điện thoại đại lý cấp 2"
                onPressEnter={() => {
                  this.page = 1;
                  this._getAPIListOrder();
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      alt=""
                      style={{ width: 16, height: 16 }}
                    />
                  </div>
                }
              />
            </Tooltip>
          </Row>

          <Row className=" mt-4">
            <Col style={{}}>
              <Row style={{ display: "flex", alignItems: "center" }}>
                <Col lg="auto" className="p-0 m-0">
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <ISvg
                      name={ISvg.NAME.EXPERIMENT}
                      width={16}
                      height={16}
                      fill={colors.icon.default}
                    />
                    <div style={{ marginLeft: 25 }}>
                      <ITitle title="Từ ngày" level={4} />
                    </div>
                  </div>
                </Col>
                <Col style={{}}>
                  <Row>
                    <Col md={"auto"}>
                      <IDatePicker
                        isBackground
                        from={this.state.start_date}
                        to={this.state.end_date}
                        onChange={(a, b) => {
                          if (a.length <= 1) {
                            console.log("aa");
                            return;
                          }
                          this.state.start_date = a[0]._d.setHours(0, 0, 0);
                          this.state.end_date = a[1]._d.setHours(23, 59, 59);
                          this.setState(
                            {
                              loadingTable: true,
                              start_date: this.state.start_date,
                              end_date: this.state.end_date,
                            },
                            () => {
                              this.page = 1;
                              this._getAPIListOrder();
                            }
                          );
                        }}
                        style={{
                          background: colors.white,
                          borderWidth: 1,
                          borderColor: colors.line,
                          borderStyle: "solid",
                        }}
                      />
                    </Col>
                    <Col md={"auto"}>
                      {this.state.Cate.length === 0 ? null : (
                        <ISelect
                          isBackground
                          defaultValue="Trạng thái"
                          select={true}
                          onChange={(value) => {
                            this.key = value;
                            this.setState(
                              {
                                loadingTable: true,
                              },
                              () => {
                                this.page = 1;
                                this._getAPIListOrder();
                              }
                            );
                          }}
                          data={this.state.Cate}
                        />
                      )}
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            {/* <Col lg='auto' style={{}}>
              <Row>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-around',
                    flexDirection: 'row'
                  }}
                >
                  <IButton
                    icon={ISvg.NAME.DOWLOAND}
                    title={'Xuất file'}
                    color={colors.main}
                    style={{}}
                    onClick={() => {
                      message.warning('Chức năng đang cập nhật')
                    }}
                  />
                </div>
              </Row>
            </Col> */}
          </Row>
          <Row className="mt-4 mb-5">
            <ITable
              data={this.state.dataTable}
              columns={columns}
              loading={this.state.loadingTable}
              style={{ width: "100%" }}
              onRow={(record, rowIndex) => {
                return {
                  onClick: (event) => {
                    const indexRow = Number(
                      event.currentTarget.attributes[1].ownerElement.dataset
                        .rowKey
                    );
                    const id = this.state.dataTable[indexRow].id;
                    this.onClickDetail(id);
                  }, // click row
                };
              }}
              defaultCurrent={1}
              maxpage={this.state.data.total}
              sizeItem={this.state.data.size}
              indexPage={this.page}
              onChangePage={(page) => {
                this.page = page;
                this.setState({ loadingTable: true }, () =>
                  this._getAPIListOrder()
                );
              }}
            />
          </Row>
        </Col>
      </Container>
    );
  }
}
