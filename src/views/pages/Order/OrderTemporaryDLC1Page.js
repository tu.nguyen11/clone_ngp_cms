import React, { useState, useEffect } from "react";
import { Col, Row, Tooltip } from "antd";
import { colors, images } from "../../../assets";
import {
  IDatePicker,
  ISearch,
  ISelect,
  ISvg,
  ITitle,
  ITable,
} from "../../components";
import FormatterDay from "../../../utils/FormatterDay";
import { priceFormat } from "../../../utils";
import { useHistory } from "react-router";
import { APIService } from "../../../services";
import { StyledITitle } from "../../components/common/Font/font";

const NewDate = new Date();

const StartDate = new Date();

let startValue = StartDate.setMonth(StartDate.getMonth() - 1);

export default function OrderTemporaryDLC1Page() {
  const history = useHistory();

  // const [selectedRows, setSelectedRows] = useState();

  const [dataOption, setDataOption] = useState([]);
  const [dataNote, setDataNote] = useState([]);

  const [filterAgency, setFilterAgen] = useState({
    status: 1,
    page: 1,
    city_id: 0,
    agency_id: -1,
    keySearch: "",
    membership_id: 0,
  });

  // const rowSelection = {
  //   selectedRows,
  //   onChange: (key) => {
  //     setSelectedRows(key);
  //   },
  // };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      width: 80,
      align: "center",
      render: (stt) => <span> {stt} </span>,
    },
    {
      title: "Ngày tạo",
      dataIndex: "create_date",
      key: "create_date",
      render: (create_date) =>
        !create_date || create_date <= 0 ? (
          "-"
        ) : (
          <span>
            {FormatterDay.dateFormatWithString(
              create_date,
              "#DD#/#MM#/#YYYY# #hhh#:#mm#"
            )}
          </span>
        ),
    },
    {
      title: "Tên đại lý",
      dataIndex: "shop_name",
      key: "shop_name",
      render: (shop_name) => {
        return (
          <Tooltip title={shop_name}>
            <span style={{ maxWidth: "200px" }}>{shop_name}</span>
          </Tooltip>
        );
      },
    },
    {
      title: "Mã đại lý C1",
      dataIndex: "dms_code",
      key: "dms_code",
      render: (dms_code) => {
        return (
          <Tooltip title={dms_code}>
            <span style={{ maxWidth: "200px" }}>{dms_code}</span>
          </Tooltip>
        );
      },
    },
    {
      title: "Cấp bậc",
      dataIndex: "membership_name",
      key: "membership_name",
      render: (membership_name) =>
        !membership_name ? "-" : <span> {membership_name} </span>,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => (!phone ? "-" : <span> {phone} </span>),
    },
    {
      title: "Tổng tiền",
      dataIndex: "total_money",
      key: "total_money",
      render: (total_money) =>
        !total_money ? "-" : <span> {priceFormat(total_money) + "đ"} </span>,
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      align: "left",
      render: (note) => (!note ? "-" : <span>{note}</span>),
    },
  ];

  const [dataList, setDataList] = useState({
    list_order: [],
    size: 10,
    total: 0,
  });

  const [isLoading, setLoading] = useState(true);

  const [filterTable, setFilterTable] = useState({
    startValue: startValue,
    endValue: NewDate.setHours(23, 59, 59, 999),
    search: "",
    page: 1,
    user_agency_id: 0,
    note: "",
  });

  const _fetchAPIListOrderS1New = async (filter) => {
    try {
      const data = await APIService._getListOrderS1New(
        filter.startValue,
        filter.endValue,
        filter.user_agency_id,
        filter.search,
        filter.page,
        filter.note
      );

      data.list_order.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
        });
      });

      setDataList(data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const _getAPIListS1 = async () => {
    try {
      const data = await APIService._getListS1(0);
      let agencyS1 = data.list_s1.map((item, index) => {
        let key = item.id;
        let value = item.shop_name;
        return {
          key,
          value,
        };
      });
      agencyS1.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDataOption(agencyS1);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  const _getAPINote = async () => {
    try {
      const data = await APIService._getListDropListNote();
      let listNote = data.data.map((item, index) => {
        let key = index + 1;
        let value = item.note;
        return {
          key,
          value,
        };
      });
      listNote.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDataNote(listNote);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  useEffect(() => {
    _fetchAPIListOrderS1New(filterTable);
  }, [filterTable]);

  const callAPI = async () => {
    await _getAPIListS1();
    await _getAPINote();
  };

  useEffect(() => {
    callAPI();
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <div>
            <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
              Danh sách đơn hàng tạm ĐLC1
            </StyledITitle>
          </div>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo tên đại lý, mã đại lý">
              <ISearch
                placeholder="Tìm kiếm theo tên đại lý, mã đại lý"
                onPressEnter={(e) => {
                  setLoading(true);
                  setFilterTable({
                    ...filterTable,
                    search: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "24px 0px" }}>
        <Row>
          <Col span={24}>
            <Row>
              <Col span={21}>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    width={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{ marginLeft: 25, marginRight: 25 }}
                  />
                  <IDatePicker
                    isBackground
                    from={filterTable.startValue}
                    to={filterTable.endValue}
                    onChange={(date, dateString) => {
                      if (date.length <= 1) {
                        return;
                      }
                      setLoading(true);
                      setFilterTable({
                        ...filterTable,
                        startValue: date[0]._d.setHours(0, 0, 0),
                        endValue: date[1]._d.setHours(23, 59, 59),
                        page: 1,
                      });
                    }}
                  />
                  <ITitle
                    title="Lọc theo"
                    level={4}
                    style={{ marginLeft: 25, marginRight: 25 }}
                  />
                  <div style={{ width: "180px" }}>
                    <ISelect
                      defaultValue="Đại lý"
                      minWidth="100px"
                      data={dataOption}
                      select={true}
                      onChange={(key, option) => {
                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          user_agency_id: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div
                    style={{
                      width: "200px",
                      marginLeft: 25,
                    }}
                  >
                    <ISelect
                      defaultValue="Ghi chú"
                      minWidth="100px"
                      data={dataNote}
                      select={true}
                      onChange={(key, option) => {
                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          note: key === 0 ? "" : option.props.children,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <ITable
        columns={columns}
        data={dataList.list_order}
        style={{ width: "100%" }}
        // rowSelection={rowSelection}
        defaultCurrent={1}
        sizeItem={dataList.size}
        rowKey="rowTable"
        indexPage={filterTable.page}
        loading={isLoading}
        maxpage={dataList.total}
        scroll={{ x: 0 }}
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => {
              const rowTable = event.currentTarget.attributes[1].value;
              const obj = JSON.parse(rowTable);
              const id = obj.id;
              history.push(`/orderTemporaryPageS1/detail/${id}`);
            },
            onMouseDown: (event) => {
              const rowTable = event.currentTarget.attributes[1].value;
              const obj = JSON.parse(rowTable);
              const id = obj.id;
              if (event.button == 2 || event == 4) {
                window.open(`/orderTemporaryPageS1/detail/${id}`, "_blank");
              }
            },
          };
        }}
        onChangePage={(page) => {
          setLoading(true);
          setFilterTable({ ...filterTable, page: page });
        }}
      />
    </div>
  );
}
