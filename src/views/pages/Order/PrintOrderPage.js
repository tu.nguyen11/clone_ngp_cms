import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import { images, } from "../../../assets";
import FormatterDay from "../../../utils/FormatterDay";
import { priceFormat } from "../../../utils";
import writtenNumber from "written-number";
import { APIService } from "../../../services";

class PrintOrderPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        gift: [],
        product: [],
        order_info: {},
        price: {},
      },
    };
    this.orderId = this.props.match.params.id;
    this.sumQuantity = 0;
  }
  _getDetailOrder = async (orderId) => {
    try {
      const data = await APIService._getDetailOrder(orderId);
      data.product.map((item, index) => {
        this.sumQuantity = this.sumQuantity + Number(item.quantity);
      });
      this.setState({ data: data });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingList: false,
      });
    }
  };
  async componentDidMount() {
    await this._getDetailOrder(this.orderId);
    window.print();
  }
  render() {
    const { data } = this.state;
    const { gift, product, order_info, price } = data;
    let priceString = writtenNumber(price.total, { lang: "vi" }) + " đồng";
    return (
      <Container
        id="body-print"
        style={{
          paddingTop: 20,
          paddingBottom: 20,
          paddingLeft: 0,
          paddingRight: 0,
          minWidth: 1280,
        }}
      >
        {/* HEADER */}
        <Row className="d-flex justify-content-center align-items-center p-0 m-0">
          <Col sm="auto" style={{}}>
            <img src={images.logo} style={{ width: 78, height: 70 }} />
          </Col>
          <Col style={{}}>
            <p
              style={{
                fontSize: 14,
                fontWeight: "600",
                color: "black",
              }}
            >
              CÔNG TY CỔ PHẦN TẬP ĐOÀN LỘC TRỜI
            </p>
            <p
              style={{
                fontSize: 14,
                fontWeight: "600",

                color: "black",
              }}
            >
              41 Đinh Tiên Hoàng, Phường Diên Hồng, TP.Pleiku, Gia Lai
            </p>
            <p
              style={{
                fontSize: 14,
                fontWeight: "600",

                color: "black",
              }}
            >
              Tel: 0269.222.1868
            </p>
          </Col>
        </Row>

        {/* TITLE */}
        <Row
          style={{ padding: 26 }}
          className="d-flex justify-content-center align-items-center p-4 m-0"
        >
          <p
            style={{
              fontSize: 26,
              fontWeight: "600",
              color: "black",
            }}
          >
            PHIẾU BÁN HÀNG
          </p>
        </Row>

        <Col className="">
          <Row className="p-0 m-0">
            <table border={1} borderColor="black" style={{ width: "100%" }}>
              <tr>
                <td colSpan={2}>
                  <p
                    style={{
                      fontSize: 16,
                      padding: 12,
                      color: "black",
                      fontWeight: "bold",
                    }}
                  >
                    Thông tin gửi hàng
                  </p>
                </td>

                <td colSpan={2}>
                  <p
                    style={{
                      fontSize: 16,
                      padding: 12,
                      color: "black",
                      fontWeight: "bold",
                    }}
                  >
                    Thông tin nhận đơn
                  </p>
                </td>
              </tr>
              <tr>
                <td width="25%">{this._renderCell("Mã đơn hàng")}</td>
                <td width="25%">{this._renderCell(order_info.orderCode)}</td>
                <td width="25%">{this._renderCell("Thanh toán")}</td>
                <td width="25%">{this._renderCell(order_info.paymentName)}</td>
              </tr>
              <tr>
                <td width="25%">{this._renderCell("Ngày tạo")}</td>
                <td width="25%">
                  {this._renderCell(
                    FormatterDay.dateFormatWithString(
                      order_info.createDate,
                      "#DD#/#MM#/#YYYY#"
                    )
                  )}
                </td>
                <td width="25%">{this._renderCell("Tên người nhận")}</td>
                <td width="25%">{this._renderCell(order_info.deliveryName)}</td>
              </tr>
              <tr>
                <td width="25%">{this._renderCell("Người tạo đơn")}</td>
                <td width="25%">{this._renderCell(order_info.saleManName)}</td>
                <td width="25%">{this._renderCell("Số điện thoại")}</td>
                <td width="25%">{this._renderCell(order_info.phone)}</td>
              </tr>
              <tr>
                <td width="25%">{this._renderCell("Ghi chú")}</td>
                <td width="25%">{this._renderCell(order_info.note)}</td>
                <td width="25%">{this._renderCell("Địa chỉ")}</td>
                <td width="25%">
                  {this._renderCell(order_info.deliveryAddress)}
                </td>
              </tr>
            </table>
          </Row>
          <Row
            className="p-0 m-0"
            style={{
              borderLeftWidth: 1,
              borderRightWidth: 1,
              borderTopWidth: 0,
              borderBottomWidth: 0,
              borderColor: "black",
              borderStyle: "solid",
            }}
          >
            <p
              style={{
                fontSize: 16,
                padding: 12,
                color: "black",
                fontWeight: "bold",
              }}
            >
              Sản phẩm đặt mua
            </p>
          </Row>
          <table border={1} borderColor="black" style={{ width: "100%" }}>
            <tr>
              <th width={50}>{this._renderCell("STT")}</th>
              <th width={120}>{this._renderCell("Mã hàng")}</th>
              <th width={260}>{this._renderCell("Tên hàng")}</th>
              <th width="10%">{this._renderCell("Đơn vị")}</th>
              <th width="10%">{this._renderCell("Giá trị")}</th>
              <th width="10%">{this._renderCell("Số lượng")}</th>
              <th width="10%">{this._renderCell("Giá thành")}</th>
              <th width={200}>{this._renderCell("Ghi chú")}</th>
            </tr>
            {data.product.map((item, index) => {
              return this._renderItemProduct(item, index + 1);
            })}

            <tr>
              <td colspan="2">{this._renderCell("")}</td>
              <td colspan="2" width={260}>
                {this._renderCell("Cộng")}
              </td>

              <td>
                {this._renderCell(priceFormat(data.price.total + "đ"), true)}
              </td>
              <td>{(this._renderCell(data.price.sumQuantity), true)}</td>
              <td>
                {this._renderCell(
                  priceFormat(data.price.sumProduct + "đ"),
                  true
                )}
              </td>
              <td width={200}>{this._renderCell("")}</td>
            </tr>
            <tr>
              <td colspan="2">{this._renderCell("")}</td>
              <td colspan="4" width={260}>
                {this._renderCell("Chiết khấu")}
              </td>
              <td colspan="1">
                {this._renderCell(priceFormat(price.sumSale + "đ"), true)}
              </td>
              <td colspan="1">{this._renderCell("")}</td>
            </tr>
            <tr>
              <td colspan="2">{this._renderCell("")}</td>
              <td colspan="4" width={260}>
                <p
                  style={{
                    fontSize: 14,
                    padding: 12,
                    color: "black",
                    fontWeight: "bold",
                  }}
                >
                  Tổng cộng số tiền đơn hàng
                </p>
              </td>
              <td colspan="2">
                <p
                  style={{
                    fontSize: 14,
                    padding: 12,
                    color: "black",
                    fontWeight: "bold",
                  }}
                >
                  {priceFormat(price.total) + "đ"}
                </p>
              </td>
            </tr>
            <tr>
              <td colspan="2">{this._renderCell("")}</td>
              <td width={260}>
                <p
                  style={{
                    fontSize: 14,
                    padding: 12,
                    color: "black",
                    fontWeight: "bold",
                  }}
                >
                  Bằng chữ
                </p>
              </td>
              <td colspan="5">
                {this._renderCell(
                  priceString.charAt(0).toUpperCase() + priceString.substring(1)
                )}
              </td>
            </tr>
          </table>

          <Row className="mt-2 p-0">
            <Col className="d-flex justify-content-center  p-4">
              <p
                style={{
                  fontSize: 14,
                  padding: 12,
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                Khách hàng xác nhận
              </p>

              <div style={{ height: 120 }}></div>
            </Col>
            <Col className="d-flex justify-content-center  p-4">
              <p
                style={{
                  fontSize: 14,
                  padding: 12,
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                Người lập đơn
              </p>

              <div style={{ height: 120 }}></div>
            </Col>
            <Col className="d-flex justify-content-center  p-4">
              <p
                style={{
                  fontSize: 14,
                  padding: 12,
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                Nhân viên vận chuyển
              </p>

              <div style={{ height: 120 }}></div>
            </Col>
          </Row>
        </Col>
      </Container>
    );
  }

  _renderCell = (content, right) => {
    return (
      <p
        style={{
          fontSize: 14,
          padding: 8,
          color: "black",
          textAlign: right ? "right" : "left",
        }}
      >
        {content}
      </p>
    );
  };

  _renderItemProduct = (item, index) => {
    return (
      <tr>
        <td width={50}>
          <p
            style={{
              fontSize: 14,
              padding: 8,
              color: "black",
              textAlign: "center",
            }}
          >
            {index}
          </p>
        </td>
        <td width={120}>{this._renderCell(item.product_code)}</td>
        <td width={260}>{this._renderCell(item.name)}</td>
        <td>{this._renderCell(item.variationName)}</td>
        <td>{this._renderCell(priceFormat(item.price) + "đ", true)}</td>
        <td>{this._renderCell(item.quantity, true)}</td>
        <td>
          {this._renderCell(
            priceFormat(item.quantity * item.price) + "đ",
            true
          )}
        </td>
        <td width={200}>{this._renderCell(item.note)}</td>
      </tr>
    );
  };
}

export default PrintOrderPage;
