import { Table, Typography, Avatar, Button, List, Modal, message } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IUpload,
  IImage,
} from "../../components";
import { priceFormat } from "../../../utils";
import ReactToPrint from "react-to-print";
import InfiniteScroll from "react-infinite-scroller";
import { colors } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import { Link } from "react-router-dom";

class ComponentToPrint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { data, loadingList } = this.props;
    if (!data) {
      return null;
    }
    var { gift, product, order_info, price } = data;
    // product = product.concat(product).concat(product);
    return (
      <Row
        style={{
          height: "70vh",
        }}
        className="m-0 p-0"
      >
        <Col
          className="m-0 p-4"
          md={3}
          lg={2}
          xl={2}
          style={{
            background: colors.white,
            borderWidth: 0,
            borderRightWidth: 8,
            borderRightColor: colors.background,
            borderStyle: "solid",
          }}
        >
          <ITitle
            level={2}
            title="Thông tin đơn"
            style={{ color: colors.black, fontWeight: "bold" }}
          />

          <Col className="m-0 p-0">
            <Col
              className="ml-0 pl-0"
              style={{ marginTop: 27, marginBottom: 20 }}
            >
              <Row className="p-0 m-0">
                <ITitle
                  level={4}
                  title={"Ngày tạo"}
                  style={{ fontWeight: 500 }}
                />
              </Row>
              <Row className="p-0 m-0">
                <ITitle
                  level={4}
                  title={FormatterDay.dateFormatWithString(
                    order_info.dateCreate,
                    "#DD#/#MM#/#YYYY#"
                  )}
                  style={{ marginTop: 10 }}
                />
              </Row>
            </Col>
            <Col
              className="ml-0 pl-0"
              style={{ marginTop: 20, marginBottom: 20 }}
            >
              <ITitle
                level={4}
                title={"Trạng thái"}
                style={{ fontWeight: 500 }}
              />
              <ITitle
                level={4}
                title={order_info.statusName}
                style={{ marginTop: 10 }}
              />
            </Col>
            <Col
              className="ml-0 pl-0"
              style={{ marginTop: 20, marginBottom: 20 }}
            >
              <ITitle
                level={4}
                title={"Mã đơn hàng"}
                style={{ fontWeight: 500 }}
              />
              <ITitle
                level={4}
                title={order_info.orderCode}
                style={{ marginTop: 10 }}
                style={{ marginTop: 10 }}
              />
            </Col>
            <Col
              className="ml-0 pl-0"
              style={{ marginTop: 20, marginBottom: 20 }}
            >
              <ITitle
                level={4}
                title={"Người tạo đơn"}
                style={{ fontWeight: 500 }}
              />
              <ITitle
                level={4}
                title={order_info.saleManName}
                style={{ marginTop: 10 }}
              />
            </Col>
            <Col
              className="ml-0 pl-0"
              style={{ marginTop: 20, marginBottom: 20 }}
            >
              <ITitle
                level={4}
                title={"Đại lý cấp 1"}
                style={{ fontWeight: 500 }}
              />
              <ITitle
                level={4}
                title={order_info.user_agency}
                style={{ marginTop: 10 }}
              />
            </Col>
          </Col>
        </Col>
        <Col
          md={6}
          lg={8}
          xl={8}
          className="m-0 p-0"
          style={{
            background: colors.white,
            height: "100%",
            borderWidth: 0,
            borderRightWidth: 8,
            borderRightColor: colors.background,
            borderStyle: "solid",
          }}
        >
          <Row className="p-0 m-0" style={{ height: "100%" }}>
            <Col
              // sm={8}
              className="p-4 m-0"
              style={{
                height: "100%",
                borderWidth: 0,
                borderRightWidth: 1,
                // borderColor: "#eee",
                // borderStyle: "solid"
              }}
            >
              <ITitle
                level={2}
                title="Sản phẩm đã mua"
                style={{ color: colors.black, fontWeight: "bold" }}
              />
              <Col
                className="d-flex flex-column p-0 m-0 pr-4"
                style={{ height: "100%" }}
              >
                <div
                  style={{
                    flex: 1,
                    overflowY: "auto",
                    marginTop: 20,
                    // marginRight: 27,
                  }}
                >
                  {
                    <div>
                      {product.map((item) => {
                        return (
                          <div
                            className="d-flex flex-row mt-4"
                            style={{ width: "100%" }}
                          >
                            <IImage
                              src={
                                this.props.data.image_url + item.product_image
                              }
                              style={{ width: 60, height: 60 }}
                            />
                            <div className="ml-3" style={{ flex: 1 }}>
                              <ITitle
                                title={item.name}
                                level="6"
                                style={{ fontWeight: 500 }}
                              />
                              <ITitle
                                title={
                                  priceFormat(item.price) +
                                  "đ / " +
                                  item.variationName
                                }
                                level="6"
                              />
                              <div
                                className="d-flex flex-row "
                                style={{ flex: 1, paddingRight: 35 }}
                              >
                                <ITitle
                                  style={{ flex: 1 }}
                                  title={"Số lượng: " + item.quantity}
                                  level="6"
                                />
                                <ITitle
                                  title={
                                    priceFormat(item.quantity * item.price) +
                                    "đ"
                                  }
                                  level="6"
                                />
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  }
                </div>

                <div className="p-0 m-0 pb-4 pt-4" style={{}}>
                  <div
                    style={{
                      height: 1,
                      width: "100%",
                      background: colors.line,
                    }}
                  />

                  <Row>
                    <Col lg={2}></Col>
                    <Col lg={10}>
                      <div className="d-flex mt-4" style={{ marginRight: 40 }}>
                        <ITitle
                          title={"Cộng"}
                          level="4"
                          style={{ fontWeight: 600, flex: 1 }}
                        />
                        <ITitle
                          title={priceFormat(price.sumProduct) + "đ"}
                          level="4"
                          style={{ fontWeight: 600 }}
                        />
                      </div>

                      <div className="d-flex mt-2" style={{ marginRight: 40 }}>
                        <ITitle
                          title={"Khuyến mãi"}
                          level="4"
                          style={{ fontWeight: 600, flex: 1 }}
                        />

                        <ITitle
                          title={priceFormat(price.sumSale) + "đ"}
                          level="4"
                          style={{ fontWeight: 600 }}
                        />
                      </div>
                      <div
                        style={{
                          height: 1,
                          width: "100%",
                          background: colors.line,
                          marginTop: 16,
                          marginBottom: 16,
                        }}
                      />
                      <div className="d-flex mt-2" style={{ marginRight: 40 }}>
                        <ITitle
                          title={"Tổng cộng"}
                          level="4"
                          style={{
                            fontWeight: 600,
                            color: colors.main,
                            flex: 1,
                          }}
                        />
                        <ITitle
                          title={priceFormat(price.total) + "đ"}
                          level="4"
                          style={{ fontWeight: 600, color: colors.main }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Col>
            <Col className="m-0 p-0 " xs="auto">
              <div
                style={{
                  width: 1,
                  height: "100%",
                  background: colors.line_2,
                  // marginLeft: 25
                }}
              ></div>
            </Col>
            {!gift || gift.length == 0 ? null : (
              <Col
                xs={4}
                className="p-4 m-0"
                style={{
                  height: "100%",
                }}
              >
                <ITitle
                  level={2}
                  title="Quà tặng"
                  style={{ color: colors.black, fontWeight: "bold" }}
                />

                <div style={{ flex: 1, overflowY: "auto", marginTop: 20 }}>
                  {
                    <div>
                      {gift.map((item) => {
                        return (
                          <div
                            className="d-flex flex-row mt-4"
                            style={{ width: "100%" }}
                          >
                            <IImage
                              src={
                                this.props.data.image_url + item.product_image
                              }
                              style={{ width: 60, height: 60 }}
                            />
                            <div className="ml-3" style={{ flex: 1 }}>
                              <ITitle
                                title={item.name}
                                level="6"
                                style={{ fontWeight: 500 }}
                              />

                              <ITitle
                                style={{ flex: 1 }}
                                title={"Số lượng: " + item.quantity}
                                level="6"
                              />
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  }
                </div>
              </Col>
            )}
          </Row>
        </Col>

        <Col
          className="m-0 p-4"
          md={3}
          lg={2}
          xl={2}
          style={{ background: colors.white }}
        >
          <Col>
            <Row>
              <ITitle
                level={2}
                title="Thông tin nhận đơn"
                style={{ color: colors.black, fontWeight: "bold" }}
              />
            </Row>
            <Row>
              <Col className="ml-0 pl-0">
                <Col
                  className="ml-0 pl-0"
                  style={{ marginTop: 27, marginBottom: 20 }}
                >
                  <ITitle
                    level={4}
                    title={"Tên người nhận"}
                    style={{ fontWeight: 500 }}
                  />
                  <ITitle
                    level={4}
                    title={order_info.deliveryName}
                    style={{ marginTop: 10 }}
                  />
                </Col>
                <Col
                  className="ml-0 pl-0"
                  style={{ marginTop: 20, marginBottom: 20 }}
                >
                  <ITitle
                    level={4}
                    title={"Địa chỉ"}
                    style={{ fontWeight: 500 }}
                  />
                  <ITitle
                    level={4}
                    style={{ marginTop: 10 }}
                    title={order_info.deliveryAddress}
                  />
                </Col>
                <Col
                  className="ml-0 pl-0"
                  style={{ marginTop: 20, marginBottom: 20 }}
                >
                  <ITitle
                    level={4}
                    title={"Số điện thoại"}
                    style={{ fontWeight: 500 }}
                  />
                  <ITitle
                    level={4}
                    title={order_info.phone}
                    style={{ marginTop: 10 }}
                    style={{ marginTop: 10 }}
                  />
                </Col>
                <Col
                  className="ml-0 pl-0"
                  style={{ marginTop: 20, marginBottom: 20 }}
                >
                  <ITitle
                    level={4}
                    title={"Phương thức thanh toán"}
                    style={{ fontWeight: 500 }}
                  />
                  <ITitle
                    level={4}
                    title={order_info.paymentName}
                    style={{ marginTop: 10 }}
                  />
                </Col>
                <Col
                  className="ml-0 pl-0"
                  style={{ marginTop: 20, marginBottom: 20 }}
                >
                  <ITitle
                    level={4}
                    title={"Ghi chú"}
                    style={{ fontWeight: 500 }}
                  />
                  <ITitle
                    level={4}
                    title={order_info.note}
                    style={{ marginTop: 10 }}
                  />
                </Col>
              </Col>
            </Row>
          </Col>
        </Col>
      </Row>
    );
  }
}

export default class DetailOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      loading: false,
      // orderId: this.props.location.state.orderId, // params OrderPage ID
      visibleModale: false,
      visible: false, // visible của Img
      status: 1, // trạng thái kho
      page: 1, // page của kho,
      loadingList: true,
      data: {
        gift: [],
        product: [],
        order_info: {},
        price: {},
      },
      stock_id: 0,
      dataStock: [],
      value: "",
    };
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.onChangeButton = this.onChangeButton.bind(this);
    this.onChangeSelect = this.onChangeSelect.bind(this);
    this.orderID = Number(this.props.match.params.id);
  }

  onSelectChange = (selectedRowKeys) => {
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  componentDidMount() {
    this._getDetailOrder(this.orderID);
  }

  /* start API */

  _getDetailOrder = async (orderId) => {
    try {
      const data = await APIService._getDetailOrder(orderId);
      this.setState({ data: data, stock_id: data.stockId, loadingList: false });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingList: false,
      });
    }
  };

  _getListStock = async () => {
    try {
      const data = await APIService._getListStock();
      const dataSelect = data.list_type.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      this.setState({ dataStock: dataSelect });
    } catch (err) {
      console.log(err);
    }
  };

  _postUpdateStock = async (stock_id, order_id) => {
    try {
      const data = await APIService._updateStock(stock_id, order_id);
      message.success("Cập nhập kho thành công");
      this._getDetailOrder(this.orderID);
    } catch (error) {}
    // gọi làm hàm getDetail lấy lại tên stock
  };

  /* end API */

  onChangeSelect(key, obj) {
    this.setState({
      stock_id: key,
      nameStock: obj.props.children,
    });
  }

  openModalVisible(visibleModale) {
    this._getListStock();
    this.setState({ visibleModale });
  }

  setModalVisible(visibleModale, type) {
    // type 1: xác nhận , 2 : hủy bỏ
    if (type === 1) {
      this._postUpdateStock(this.state.stock_id, this.orderID);
    }
    this.setState({ visibleModale });
  }
  onChangeSearch = ({ target: { value } }) => {};
  onChangeButton = () => {
    this.setState({
      loading: true,
    });

    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 3000);
  };
  _updateOrder = async (status, popup) => {
    // 1 là hủy ---- 2 là xóa
    try {
      const id = [this.orderID];

      await APIService._UpdateOrder({ id, status });
      this._getDetailOrder(this.orderID);
      if (popup == 1) {
        message.success("Đã hủy đơn hàng thành công");
      } else message.success("Đã xóa đơn hàng thành công");
    } catch (error) {}
  };
  render() {
    const { data } = this.state;
    const { gift, product, order_info, price } = data;
    return (
      <div style={{ width: "100%" }}>
        <Modal
          title={
            <ITitle
              level={3}
              title="Chuyển kho thay thế"
              style={{ fontWeight: "bold" }}
            />
          }
          visible={this.state.visibleModale}
          style={{ borderRadius: 0, background: "#8c8c8c" }}
          centered
          footer={null}
          style={{ padding: 0, margin: 0 }}
          width={400}
          // onOk={() => this.setModalVisible(false, 1)}
          onCancel={() => this.setModalVisible(false, 2)}
        >
          <ISelect
            placeholder="Chọn kho"
            style={{ marginTop: 8 }}
            type="select"
            select={true}
            onChange={this.onChangeSelect}
            data={this.state.dataStock}
          />
          <div style={{}}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                marginTop: 20,
              }}
            >
              <IButton
                icon={ISvg.NAME.CHECKMARK}
                title="Xác nhận"
                onClick={() => {
                  this.setModalVisible(false, 1);
                }}
                color={colors.main}
                style={{ marginRight: 20, width: 120 }}
              />
              <IButton
                icon={ISvg.NAME.CROSS}
                title="Hủy bỏ"
                onClick={() => {
                  this.setModalVisible(false, 2);
                }}
                color={colors.oranges}
              />
            </div>
          </div>
        </Modal>
        <div fluid>
          <Row className="p-0 m-0 mt-3z" xs="auto" sm={12} md={12}>
            <ITitle
              level={1}
              title="Chi tiết đơn hàng"
              style={{ color: colors.main, fontWeight: "bold" }}
            />
          </Row>
          <Row
            className="p-0 m-0 mb-4"
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingTop: 32,
              paddingBottom: 32,
            }}
          >
            <Link
              to={{
                pathname: "/printOrder/" + this.orderID,
                id: this.orderID,
                orderID: "id ne",
                state: { fromDashboard: true },
              }}
              target="_blank"
              style={{
                width: 150,
                height: 42,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                border: "1px solid #004C91",
              }}
            >
              <div style={{ marginRight: 12 }}>
                <ISvg
                  name={ISvg.NAME.PRINT}
                  width={20}
                  height={20}
                  fill={colors.main}
                />
              </div>

              <ITitle
                title="In đơn"
                style={{ color: colors.main, fontWeight: 500 }}
              ></ITitle>
            </Link>

            {/*<IButton*/}
            {/*  icon={ISvg.NAME.SWAP}*/}
            {/*  title="Chuyển kho"*/}
            {/*  onClick={() => this.openModalVisible(true)}*/}
            {/*  color={colors.main}*/}
            {/*  style={{ marginRight: 20 }}*/}
            {/*/>*/}
            {/*<IButton*/}
            {/*  icon={ISvg.NAME.APPLICATION}*/}
            {/*  title="Hủy đơn"*/}
            {/*  color={colors.oranges}*/}
            {/*  onClick={() => this._updateOrder(0, 1)}*/}
            {/*/>*/}
          </Row>
          <ComponentToPrint
            data={this.state.data}
            loadingList={this.state.loadingList}
          />
        </div>
      </div>
    );
  }
}
