import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Space,
  Modal,
  Empty,
  BackTop,
  message,
  Popconfirm,
  Skeleton,
} from "antd";
import { colors } from "../../../assets";
import { useWindowSize, useModalViewInfo, priceFormat } from "../../../utils";
import { Link, useHistory, useParams, Redirect } from "react-router-dom";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import { ITitle, IButton, ITableHtml } from "../../components";

export default function DetailOrder(props) {
  const order_code = Number(props.match.params.id);
  const history = useHistory();
  const [dataDetail, setDataDetail] = useState({
    order_info: {},
    image_url: "",
    price: {},
    product: [],
    gift: [],
  });
  const [isModal, setIsModal] = useState(false);
  const [isLoadingAPI, setLoadingAPI] = useState(true);
  const [isLogout, setIsLogout] = useState(false);
  const _getAPIDetail = async (order_code) => {
    try {
      const data = await APIService._getDetailOrder(order_code);
      console.log(JSON.stringify(data));
      setDataDetail(data);
      setLoadingAPI(false);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    _getAPIDetail(order_code);
  }, []);

  let headerTable = [
    {
      name: "Tên sản phẩm",
      align: "left",
    },
    {
      name: "Quy cách 1",
      align: "center",
    },
    {
      name: "Quy cách 2",
      align: "center",
    },
    {
      name: "Lít /Kg",
      align: "center",
    },
    {
      name: "Đơn giá",
      align: "right",
    },
    {
      name: "Thành tiền",
      align: "right",
    },
    {
      name: "Giảm giá",
      align: "right",
    },
    {
      name: "Thanh toán",
      align: "right",
    },
  ];

  let headerTableGift = [
    { name: "Quà tặng kèm", align: "center" },
    {
      name: "Số lượng",
      name1: "(Quy cách nhỏ nhất)",
      align: "center",
      isBr: true,
    },
  ];

  const footMoney = () => (
    <tfoot style={{ border: "none" }}>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{ fontStyle: "italic", fontWeight: "bold" }}
          colSpan={3}
        >
          Tổng tạm tính
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
          }}
          colSpan={4}
        >
          {priceFormat(dataDetail.price.sumProduct + "đ")}
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            borderBottom: "1px solid #7A7B7B",
          }}
          colSpan={3}
        >
          Tổng khuyến mãi
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            borderBottom: "1px solid #7A7B7B",
          }}
          colSpan={4}
        >
          {priceFormat(dataDetail.price.sumSale + "đ")}
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            color: colors.main,
          }}
          colSpan={3}
        >
          Tổng thành tiền
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            color: colors.main,
          }}
          colSpan={4}
        >
          {priceFormat(dataDetail.price.total + "đ")}
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            borderBottom: "1px solid #7A7B7B",
            color: colors.main,
          }}
          colSpan={3}
        >
          Thuế VAT
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            borderBottom: "1px solid #7A7B7B",
            color: colors.main,
          }}
          colSpan={4}
        >
          {priceFormat(dataDetail.price.vat + "đ")}
        </td>
      </tr>
      <tr>
        <td style={{ borderLeft: "1px solid white" }}></td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            color: colors.main,
          }}
          colSpan={3}
        >
          Tổng thanh toán
        </td>
        <td
          className="td-table"
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            textAlign: "right",
            borderRight: "1px solid white",
            color: colors.main,
          }}
          colSpan={4}
        >
          {priceFormat(dataDetail.price.total_money_finish + "đ")}
        </td>
      </tr>
    </tfoot>
  );

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr className="tr-table">
        <td className="td-table-1">
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              width: 200,
            }}
          >
            <div
              style={{
                padding: 6,
                border: "1px solid #D8DBDC",
              }}
            >
              <img
                src={dataDetail.image_url + item.image_thumbnail}
                style={{
                  width: 40,
                  height: 40,
                }}
              />
            </div>

            <div style={{ marginLeft: 10, flex: 1 }}>
              <span style={{ fontWeight: 600 }}>{item.name}</span>
            </div>
          </div>
        </td>
        <td className="td-table" style={{ textAlign: "center" }}>
          {item.role_1}
        </td>
        <td className="td-table" style={{ textAlign: "center" }}>
          {item.role_2}
        </td>
        <td className="td-table" style={{ textAlign: "center" }}>
          {item.weight}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {priceFormat(item.price + "đ")}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {priceFormat(item.total_price + "đ")}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {priceFormat(item.discount + "đ")}
        </td>
        <td className="td-table" style={{ textAlign: "right" }}>
          {priceFormat(item.total_pay + "đ")}
        </td>
      </tr>
    ));
  };

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="tr-table scroll">
        {headerTable.map((item, index) => (
          <th
            className={index === 0 ? "th-table-1" : "th-table"}
            style={{ textAlign: item.align }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const headerTableGiftUI = (headerTableGift) => {
    return (
      <tr className="tr-table">
        {headerTableGift.map((item, index) =>
          item.isBr ? (
            <th
              className={index === 0 ? "th-table-1" : "th-table"}
              style={{ textAlign: item.align }}
            >
              {item.name}
              <br />
              <span style={{ fontSize: 12, fontWeight: "normal" }}>
                {item.name1}
              </span>
            </th>
          ) : (
            <th
              className={index === 0 ? "th-table-1" : "th-table"}
              style={{ textAlign: item.align }}
            >
              {item.name}
            </th>
          )
        )}
      </tr>
    );
  };

  const bodyTable = (contentTableGift) => {
    return contentTableGift.length === 0 ? (
      <tr
        className="tr-table"
        className="td-table"
        style={{ textAlign: "center" }}
      >
        <td
          colSpan="2"
          style={{
            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
            padding: "24px 0px",
          }}
        >
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      contentTableGift.map((item, index) => (
        <tr className="tr-table">
          <td className="td-table-1 outline">
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <div
                style={{
                  padding: 6,
                  border: "1px solid #D8DBDC",
                  marginRight: 12,
                }}
              >
                <img
                  src={dataDetail.image_url + item.image_thumbnail}
                  alt="quà tặng"
                  style={{
                    width: 40,
                    height: 40,
                  }}
                />
              </div>
              <span style={{ fontWeight: 500 }}>{item.name}</span>
            </div>
          </td>
          <td className="td-table" style={{ textAlign: "center" }}>
            {item.min_quantity}
          </td>
        </tr>
      ))
    );
  };
  if (!dataDetail) {
    return null;
  }
  return (
    <div style={{ width: "100%", overflow: "auto", padding: "36px 36px" }}>
      <div style={{ minWidth: 1500 }}>
        <Row>
          <Col flex="auto">
            <div>
              <ITitle
                level={1}
                title="Chi tiết đơn đặt hàng cấp 2"
                style={{ color: colors.main, fontWeight: "bold" }}
              />
            </div>
          </Col>
        </Row>
        <BackTop description={1000} />
        <div style={{ margin: "32px 0px" }}>
          <Row>
            <Col flex="auto"></Col>
          </Row>
          <div style={{ margin: "32px 0px 12px 0px" }}>
            <Row gutter={[25, 12]} type="flex">
              <Col
                span={4}
                style={{
                  background: colors.white,
                  marginTop: 6,
                  paddingLeft: isLoadingAPI ? 12 : 0,
                  paddingRight: isLoadingAPI ? 12 : 0,
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                }}
              >
                <Skeleton
                  active
                  loading={isLoadingAPI}
                  paragraph={{ rows: 16 }}
                >
                  <div style={{ paddingBottom: 22 }}>
                    <Row gutter={[0, 20]}>
                      <div style={{ padding: "0px 40px", paddingTop: 22 }}>
                        <Col
                          span={24}
                          style={{ paddingLeft: 0, paddingRight: 0 }}
                        >
                          <ITitle
                            level={2}
                            title="Thông tin đơn hàng"
                            style={{
                              color: colors.text.black,
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                      </div>
                      <Col
                        span={24}
                        style={{
                          background: colors.green._3,
                          paddingLeft: 0,
                          paddingRight: 0,
                        }}
                      >
                        <div style={{ padding: "0px 40px", paddingTop: 8 }}>
                          <p style={{ fontWeight: 600 }}>Trạng thái</p>
                          <span style={{ color: colors.main, fontWeight: 700 }}>
                            {dataDetail.order_info.statusName}
                          </span>
                        </div>
                      </Col>
                      <div style={{ padding: "0px 40px" }}>
                        <Col>
                          <div style={{ marginTop: 6 }}>
                            <p style={{ fontWeight: 600 }}>Mã đơn hàng</p>
                            <span>{dataDetail.order_info.orderCode}</span>
                          </div>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Ngày tạo đơn</p>
                          <span>
                            {FormatterDay.dateFormatWithString(
                              dataDetail.order_info.dateCreate,
                              "#DD#/#MM#/#YYYY# #hhh#:#mm#:#ss#"
                            )}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Người tạo </p>
                          <span>{dataDetail.order_info.saleManName}</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Đại lý cấp 1</p>
                          <span>{dataDetail.order_info.user_agency}</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Đại lý cấp 2</p>
                          <span>{dataDetail.order_info.shop_name}</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Phòng kinh doanh</p>
                          <span>{dataDetail.order_info.agency}</span>
                        </Col>
                      </div>
                    </Row>
                  </div>
                </Skeleton>
              </Col>

              <Col span={20} style={{ paddingBottom: 0 }}>
                <div
                  style={{
                    padding: "30px 40px",
                    background: colors.white,
                    height: "100%",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <Skeleton
                    active
                    loading={isLoadingAPI}
                    paragraph={{ rows: 16 }}
                  >
                    <Row>
                      <Col span={24}>
                        <ITitle
                          level={2}
                          title="Sản phẩm đặt mua "
                          style={{
                            color: colors.text.black,
                            fontWeight: "bold",
                          }}
                        />
                      </Col>
                      <Col span={24}>
                        <Row gutter={[18, 0]}>
                          <Col span={18}>
                            <div style={{ marginTop: 12 }}>
                              <Row>
                                <Col span={24}>
                                  <ITableHtml
                                    childrenBody={bodyTableProduct(
                                      dataDetail.product
                                    )}
                                    childrenHeader={headerTableProduct(
                                      headerTable
                                    )}
                                    isfoot={true}
                                    childrenFoot={footMoney()}
                                  />
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col span={6}>
                            <div style={{ marginTop: 12 }}>
                              <Row>
                                <Col span={24}>
                                  <ITableHtml
                                    childrenBody={bodyTable(dataDetail.gift)}
                                    childrenHeader={headerTableGiftUI(
                                      headerTableGift
                                    )}
                                  />
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </div>
  );
}
