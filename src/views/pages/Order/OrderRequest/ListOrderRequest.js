import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip } from "antd";
import { useHistory } from "react-router-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IDatePicker,
} from "../../../components";
import FormatterDay from "../../../../utils/FormatterDay";
import { images, colors } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import { priceFormat } from "../../../../utils";
const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

export default function ListOrderRequest() {
  const history = useHistory();
  const [data, setData] = useState({
    order: [],
    total: 0,
    size: 10,
  });

  const [listStatus, setListStatus] = useState([]);
  const [dataSalesMan, setDataSalesMan] = useState([]);

  const [loadingSalesMan, setLoadingSalesMan] = useState(true);
  const [loadingStatus, setLoadingStatus] = useState(true);
  const [loadingTable, setLoadingTable] = useState(true);

  const [filterTable, setFilterTable] = useState({
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
    page: 1,
    sale_man_id: 0,
    search: "",
    status: 0,
  });

  const _fetchAPIListOrderRequest = async (filter) => {
    try {
      const data = await APIService._getListOrderRequest(filter);
      data.data.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
      });
      setData({ order: data.data, total: data.total, size: data.limit });
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  const _getListStatus = async () => {
    try {
      const data = await APIService._getListStatusAppSerice(1);

      var dataNew = data.order_status.map((item, index) => {
        const value = item.name;
        const key = item.id;
        return {
          key,
          value,
        };
      });
      let dataNewTemp = [];
      dataNew.map((item, index) => {
        if (item.value === "" || !item.value) {
          return;
        }
        dataNewTemp.push(item);
      });

      dataNewTemp.unshift({
        value: "Tất cả",
        key: 0,
      });
      setListStatus(dataNewTemp);
      setLoadingStatus(false);
    } catch (error) {
      console.log(error);
      setLoadingStatus(false);
    }
  };

  const getListSalesManFilter = async () => {
    try {
      const data = await APIService._getListSalesManFilter();
      let arrNew = data.data.map((item, index) => {
        let key = item.id;
        let value = item.name;
        return {
          key,
          value,
        };
      });
      arrNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDataSalesMan([...arrNew]);
      setLoadingSalesMan(false);
    } catch (error) {
      console.log(error);
      setLoadingSalesMan(false);
    }
  };

  const callAPI = async () => {
    await _getListStatus();
    await getListSalesManFilter();
  };

  useEffect(() => {
    callAPI();
  }, []);

  useEffect(() => {
    _fetchAPIListOrderRequest(filterTable);
  }, [filterTable]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Mã phiếu yêu cầu",
      dataIndex: "code",
      key: "code",
      render: (code) =>
        !code ? (
          "-"
        ) : (
          <Tooltip title={code}>
            <span>{code}</span>
          </Tooltip>
        ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "create_date",
      key: "create_date",
      render: (create_date) => (
        <span>
          {!create_date || create_date <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
                create_date,
                "#DD#/#MM#/#YYYY#"
              )}
        </span>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
    },

    {
      title: "Nhân viên phụ trách",
      dataIndex: "sale_man",
      key: "sale_man",
      render: (sale_man) =>
        !sale_man ? (
          "-"
        ) : (
          <Tooltip title={sale_man}>
            <span>{sale_man}</span>
          </Tooltip>
        ),
    },
    {
      title: "Tên khách hàng",
      dataIndex: "customer",
      key: "customer",
      render: (customer) =>
        !customer ? (
          "-"
        ) : (
          <Tooltip title={customer}>
            <span>{customer}</span>
          </Tooltip>
        ),
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <span>{!phone ? "-" : phone}</span>,
    },
    {
      title: "Mẫu xe quan tâm",
      dataIndex: "interested_car",
      key: "interested_car",
      render: (interested_car) => (
        <span>{!interested_car ? "-" : interested_car}</span>
      ),
    },
    {
      title: "Giá trị",
      dataIndex: "price",
      key: "price",
      render: (price) => (
        <span>
          {[null, undefined, ""].includes(price) ? "-" : priceFormat(price)}
        </span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Danh sách phiếu yêu cầu
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã đơn hàng">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo mã phiếu yêu cầu"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    search: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row>
          <Col span={24}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <ISvg
                name={ISvg.NAME.EXPERIMENT}
                with={16}
                height={16}
                fill={colors.icon.default}
              />
              <ITitle
                title="Từ ngày"
                level={4}
                style={{
                  marginLeft: 15,
                  marginRight: 15,
                }}
              />
              <div>
                <IDatePicker
                  isBackground
                  from={filterTable.start_date}
                  to={filterTable.end_date}
                  onChange={(date, dateString) => {
                    if (date.length <= 1) {
                      return;
                    }

                    setLoadingTable(true);
                    setFilterTable({
                      ...filterTable,
                      start_date: date[0]._d.setHours(0, 0, 0),
                      end_date: date[1]._d.setHours(23, 59, 59),
                      page: 1,
                    });
                  }}
                  style={{
                    background: colors.white,
                    borderWidth: 1,
                    borderColor: colors.line,
                    borderStyle: "solid",
                  }}
                />
              </div>
              <ITitle
                title="Lọc theo"
                level={4}
                style={{
                  marginLeft: 15,
                  marginRight: 15,
                }}
              />

              <div style={{ width: 200 }}>
                <ISelect
                  defaultValue="Nhân viên phụ trách"
                  data={dataSalesMan}
                  select={true}
                  loading={loadingSalesMan}
                  isTooltip={true}
                  onChange={(value) => {
                    setLoadingTable(true);
                    setFilterTable({
                      ...filterTable,
                      sale_man_id: value,
                      page: 1,
                    });
                  }}
                />
              </div>
              <div style={{ margin: "0px 15px 0px 15px", width: 200 }}>
                <ISelect
                  defaultValue="Trạng thái"
                  data={listStatus}
                  select={true}
                  loading={loadingStatus}
                  onChange={(value) => {
                    setLoadingTable(true);
                    setFilterTable({
                      ...filterTable,
                      status: value,
                      page: 1,
                    });
                  }}
                />
              </div>
            </div>
          </Col>
          <Col span={24}>
            <div style={{ marginTop: 20 }}>
              <Row>
                <ITable
                  columns={columns}
                  data={data.order}
                  style={{ width: "100%" }}
                  defaultCurrent={1}
                  sizeItem={data.size}
                  indexPage={filterTable.page}
                  maxpage={data.total}
                  loading={loadingTable}
                  onRow={(record) => {
                    return {
                      onClick: () => {
                        history.push(`/order/request/detail/${record.id}`);
                      },
                      onMouseDown: (event) => {
                        if (event.button == 2 || event == 4) {
                          window.open(
                            `/order/request/detail/${record.id}`,
                            "_blank"
                          );
                        }
                      },
                    };
                  }}
                  onChangePage={(page) => {
                    setLoadingTable(true);
                    setFilterTable({ ...filterTable, page: page });
                  }}
                />
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
