import React, {useState, useEffect, useRef} from "react";
import {Row, Col, Modal, Empty, message, Skeleton} from "antd";
import styled from "styled-components";
import {useParams} from "react-router-dom";
import {APIService} from "../../../../services";
import {colors, images} from "../../../../assets";
import {priceFormat, renderAddress} from "../../../../utils";
import FormatterDay from "../../../../utils/FormatterDay";
import {ITitle, IButton, ISvg, ITable} from "../../../components";

import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";
import {IInputTextArea} from "../../../components/common";
import html2canvas from "html2canvas";
import {jsPDF} from "jspdf";

const ITableNotFooter = styled(ITable)`
  & .ant-table-footer {
    display: none !important;
  }

  & .ant-table-body {
    margin: 0px !important;
  }

  & .ant-table-thead {
    background: ${colors.green._3} !important;
  }
`;

const IModal = styled(Modal)`
  .ant-btn-primary {
    display: none !important;
  }
`;

export default function DetailOrderRequest(props) {
  const {id} = useParams();
  const rootRef = useRef(null);
  const [dataDetail, setDataDetail] = useState({
    product_info: {},
    price_info: {},
    url: "",
    order_info: {},
  });
  const [isLoadingAPI, setLoadingAPI] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);

  const [isModalApproved, setIsModalApproved] = useState(false);
  const [isModalForm, setIsModalForm] = useState(false);
  const [isModal, setIsModal] = useState(false);

  const [typeButton, setTypeButton] = useState(0);
  const [reasons, setReasons] = useState("");

  const getDetailOrderRequest = async (id) => {
    try {
      const data = await APIService._getDetailOrderRequest(id);

      const {price_info} = data.data;
      const dataTablePriceNew = dataTablePrice.map((item) => {
        return {...item, value: price_info[item.key]};
      });

      const dataTableFinanceNew = dataTableFinance.map((item) => {
        return {...item, value: price_info[item.key]};
      });

      setDataTablePrice([...dataTablePriceNew]);
      setDataTableFinance([...dataTableFinanceNew]);
      setDataDetail({...data.data});
      setLoadingAPI(false);
    } catch (err) {
      console.log(err);
      setLoadingAPI(false);
    }
  };

  const _postUpdateStatusOrderRequest = async (obj) => {
    try {
      const data = await APIService._postUpdateStatusOrderRequest(obj);
      setLoadingButton(false);
      if (obj.type === 1) {
        setIsModalApproved(true);
      } else {
        setIsModal(false);
        message.success(
          `${obj.type === 2 ? "Từ chối" : "Phản hồi"} đơn thành công`
        );
      }
      setLoadingAPI(true);
      await getDetailOrderRequest(id);
    } catch (error) {
      setLoadingButton(false);
      console.log(error);
    }
  };

  useEffect(() => {
    getDetailOrderRequest(id);
  }, [id]);

  let titleTableProduct = [
    {
      name: "Giá công bố",
      isName: true,
    },
    {
      name: "Giá khuyến mãi",
      isName: false,
    },
  ];

  const columnsTableProduct = [
    {
      title: "Tên sản phẩm",
      align: "center",
      render: (item) =>
        item.isName ? (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              minHeight: 59,
            }}
          >
            <div
              style={{
                marginRight: 10,
              }}
            >
              <img
                src={dataDetail.url + dataDetail.product_info.image}
                style={{
                  width: 60,
                  height: 40,
                }}
              />
            </div>
            <span style={{fontWeight: 600, textAlign: "left"}}>
              {!dataDetail.product_info.name
                ? "-"
                : dataDetail.product_info.name}
            </span>
          </div>
        ) : (
          <Col span={24}>
            <Row span={24}>
              <Col span={24}>
                <Row>
                  <Col span={8}>
                    <p
                      style={{
                        fontWeight: 600,
                        wordBreak: "break-word",
                        textAlign: "left",
                        padding: "5px 5px 5px 0px",
                      }}
                    >
                      Loại xe
                    </p>
                  </Col>
                  <Col span={16}>
                    <p
                      style={{
                        wordBreak: "break-word",
                        textAlign: "right",
                        padding: "5px 0px 5px 5px",
                      }}
                    >
                      {!dataDetail.product_info.product_type_parent
                        ? "-"
                        : dataDetail.product_info.product_type_parent}
                    </p>
                  </Col>
                </Row>
              </Col>

              <Col span={24}>
                <Row>
                  <Col span={8}>
                    <p
                      style={{
                        fontWeight: 600,
                        wordBreak: "break-word",
                        textAlign: "left",
                        padding: "5px 5px 5px 0px",
                      }}
                    >
                      Dòng xe
                    </p>
                  </Col>
                  <Col span={16}>
                    <p
                      style={{
                        wordBreak: "break-word",
                        textAlign: "right",
                        padding: "5px 0px 5px 5px",
                      }}
                    >
                      {!dataDetail.product_info.product_type
                        ? "-"
                        : dataDetail.product_info.product_type}
                    </p>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <Col span={8}>
                    <p
                      style={{
                        fontWeight: 600,
                        wordBreak: "break-word",
                        textAlign: "left",
                        padding: "5px 5px 5px 0px",
                      }}
                    >
                      Thương hiệu
                    </p>
                  </Col>
                  <Col span={16}>
                    <p
                      style={{
                        wordBreak: "break-word",
                        textAlign: "right",
                        padding: "5px 0px 5px 5px",
                      }}
                    >
                      {!dataDetail.product_info.cate
                        ? "-"
                        : dataDetail.product_info.cate}
                    </p>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <Col span={8}>
                    <p
                      style={{
                        fontWeight: 600,
                        wordBreak: "break-word",
                        textAlign: "left",
                        padding: "5px 5px 5px 0px",
                      }}
                    >
                      Loại thùng
                    </p>
                  </Col>
                  <Col span={16}>
                    <p
                      style={{
                        wordBreak: "break-word",
                        textAlign: "right",
                        padding: "5px 0px 5px 5px",
                      }}
                    >
                      {!dataDetail.product_info.box_type
                        ? "-"
                        : dataDetail.product_info.box_type}
                    </p>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <Col span={8}>
                    <p
                      style={{
                        fontWeight: 600,
                        wordBreak: "break-word",
                        textAlign: "left",
                        padding: "5px 5px 5px 0px",
                      }}
                    >
                      Thương hiệu
                    </p>
                  </Col>
                  <Col span={16}>
                    <p
                      style={{
                        wordBreak: "break-word",
                        textAlign: "right",
                        padding: "5px 0px 5px 5px",
                      }}
                    >
                      {!dataDetail.product_info.brand
                        ? "-"
                        : dataDetail.product_info.brand}
                    </p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        ),
    },
  ];

  const [dataTablePrice, setDataTablePrice] = useState([
    {
      name: "Giá công bố",
      key: "announced_price",
      isPrice: true,
    },
    {
      name: "Giá khuyến mãi",
      key: "promotion_price",
      isPrice: true,
    },
    {
      name: "Giá đóng thùng",
      key: "crating_price",
      isPrice: true,
    },
    {
      name: "Phí đăng kí đăng kiểm",
      key: "registry_check_fee",
      isPrice: true,
    },
    {
      name: "Bảo hiểm thân xe",
      key: "bao_hiem_than_xe",
      isPrice: true,
    },
    {
      name: "Phù hiệu định vị",
      key: "phu_hieu_dinh_vi",
      isPrice: true,
    },
    {
      name: "Ghi chú",
      key: "other",
      isPrice: false,
    },
    {
      name: "Giá bán trên hợp đồng",
      key: "price_on_contract",
      hightLight: true,
      isPrice: true,
    },
    {
      name: "Tổng số tiền thanh toán",
      key: "total_payment",
      hightLight: true,
      isPrice: true,
    },
    {
      name: "Giá đề xuất",
      key: "offer_price",
      hightLight: true,
      isPrice: true,
    },
  ]);

  const columnsTablePrice = [
    {
      title: "Báo giá",
      align: "center",
      width: 250,
      render: (item) => (
        <span
          style={{
            textAlign: "left",
            fontWeight: !item.hightLight ? "normal" : 500,
          }}
        >
          {item.name}
        </span>
      ),
    },
    {
      title: "Số tiền",
      align: "center",
      render: (item) => (
        <div
          style={{
            textAlign: "right",
            fontWeight: !item.hightLight ? "normal" : 500,
            color: !item.hightLight ? colors.blackChart : colors.main,
            wordBreak: "break-word",
            wordWrap: "break-word",
          }}
        >
          {item.value === undefined || item.value === null || item.value === ""
            ? "-"
            : item.isPrice
              ? `${priceFormat(item.value)} VNĐ`
              : item.value}
        </div>
      ),
    },
  ];

  const [dataTableFinance, setDataTableFinance] = useState([
    {
      name: "Gói tài chính áp dụng",
      key: "financial_package",
      isPrice: true,
    },
    {
      name: "Lãi xuất",
      key: "interest_rate",
      isPercent: true,
    },
    {
      name: "Số tiền trả trước",
      key: "prepayment",
      isPrice: true,
    },
    {
      name: "Số tiền vay",
      key: "total_money_borrow",
      isPrice: true,
    },
    {
      name: "Thời hạn vay",
      key: "warranty_period",
      isDate: true,
    },
    {
      name: "Số tiền trả cuối kỳ",
      key: "payment_end_period",
      isPrice: true,
    },
  ]);

  const columnsTableFinance = [
    {
      title: "Hỗ trợ tài chính",
      align: "center",
      width: 250,
      render: (item) => (
        <span
          style={{
            textAlign: "left",
            fontWeight: !item.hightLight ? "normal" : 500,
          }}
        >
          {item.name}
        </span>
      ),
    },
    {
      title: "Thông tin",
      align: "center",
      render: (item) => (
        <div
          style={{
            textAlign: "right",
            fontWeight: !item.hightLight ? "normal" : 500,
            color: !item.hightLight ? colors.blackChart : colors.main,
            wordBreak: "break-word",
            wordWrap: "break-word",
          }}
        >
          {item.value === null || item.value === undefined || item.value === ""
            ? "-"
            : item.isPrice
              ? `${priceFormat(item.value)} VNĐ`
              : item.isPercent
                ? `${item.value}%`
                : item.value}
        </div>
      ),
    },
  ];

  return (
    <div style={{width: "100%"}}>
      <Row>
        <Col spn={24}>
          <div>
            <ITitle
              level={1}
              title="Chi tiết phiếu yêu cầu"
              style={{color: colors.main, fontWeight: "bold"}}
            />
          </div>
        </Col>
      </Row>
      <div style={{margin: "25px 0px"}}>
        <Row>
          {isLoadingAPI || dataDetail.order_info.status !== 2 ? null : (
            <Col span={24}>
              <div className="flex justify-end">
                <IButton
                  title={"Duyệt phiếu"}
                  styleHeight={{
                    width: 150,
                  }}
                  color={colors.main}
                  onClick={() => {
                    setLoadingButton(true);
                    const obj = {
                      id: Number(id),
                      type: 1,
                      value: "",
                    };
                    _postUpdateStatusOrderRequest(obj);
                  }}
                />

                <IButton
                  title={"Phản hồi phiếu"}
                  styleHeight={{
                    width: 160,
                    marginLeft: 15,
                  }}
                  color={colors.main}
                  onClick={() => {
                    setTypeButton(3);
                    setReasons("");
                    setIsModal(true);
                  }}
                />

                <IButton
                  title={"Từ chối phiếu"}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                  }}
                  loading={loadingButton}
                  color={colors.oranges}
                  onClick={() => {
                    setTypeButton(2);
                    setReasons("");
                    setIsModal(true);
                  }}
                />
              </div>
            </Col>
          )}
        </Row>
        <div style={{margin: "25px 0px 12px 0px"}}>
          <Row gutter={[25, 12]} type="flex">
            <Col
              span={5}
              style={{
                background: colors.white,
                paddingLeft: isLoadingAPI ? 30 : 0,
                paddingRight: isLoadingAPI ? 30 : 0,
                paddingTop: isLoadingAPI ? 22 : 0,
                paddingBottom: isLoadingAPI ? 22 : 0,
                boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
              }}
            >
              {isLoadingAPI ? (
                <Skeleton active loading={true} paragraph={{rows: 16}}/>
              ) : (
                <Row gutter={[0, 20]}>
                  <Col span={24}>
                    <div
                      style={{
                        padding: "22px 30px 0px 30px",
                        height: "100%",
                      }}
                    >
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thông tin phiếu yêu cầu
                      </StyledITileHeading>
                    </div>
                  </Col>
                  <Col
                    span={24}
                    style={{
                      background: colors.green._3,
                      marginTop: 10,
                    }}
                  >
                    <div
                      style={{
                        padding: "0px 30px",
                      }}
                    >
                      <p style={{fontWeight: 600}}>Trạng thái</p>
                      <span style={{color: colors.main, fontWeight: 700}}>
                        {!dataDetail.order_info.status_name
                          ? "-"
                          : dataDetail.order_info.status_name}
                      </span>
                    </div>
                  </Col>
                  <Col span={24}>
                    <div style={{padding: "0px 30px 0px 30px"}}>
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Mã phiếu yêu cầu</p>
                        <span>
                          {!dataDetail.order_info.order_code_child
                            ? "-"
                            : dataDetail.order_info.order_code_child}
                        </span>
                      </Col>
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Ngày tạo phiếu</p>
                        <span>
                          {!dataDetail.order_info.create_date
                            ? "-"
                            : FormatterDay.dateFormatWithString(
                              dataDetail.order_info.create_date,
                              "#DD#/#MM#/#YYYY#"
                            )}
                        </span>
                      </Col>
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Nhân viên phụ trách</p>
                        <span>
                          {!dataDetail.order_info.sale_man
                            ? "-"
                            : dataDetail.order_info.sale_man}
                        </span>
                      </Col>
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Nguồn khách hàng</p>
                        <span>
                          {!dataDetail.order_info.customer_resource
                            ? "-"
                            : dataDetail.order_info.customer_resource}
                        </span>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            marginTop: 22,
                          }}
                        >
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Thông tin khách hàng
                          </StyledITileHeading>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div style={{marginTop: 6}}>
                          <p style={{fontWeight: 600}}>
                            {" "}
                            {dataDetail.order_info.user_type_id == 1
                              ? "Tên doanh nghiệp"
                              : "Tên khách hàng"}
                          </p>
                          <span>
                            {!dataDetail.order_info.customer_name
                              ? "-"
                              : dataDetail.order_info.customer_name}
                          </span>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div style={{marginTop: 6}}>
                          <p style={{fontWeight: 600}}>Mã khách hàng</p>
                          <span>
                            {!dataDetail.order_info.user_code
                              ? "-"
                              : dataDetail.order_info.user_code}
                          </span>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div style={{marginTop: 6}}>
                          <p style={{fontWeight: 600}}>Số điện thoại</p>
                          <span>
                            {!dataDetail.order_info.customer_phone
                              ? "-"
                              : dataDetail.order_info.customer_phone}
                          </span>
                        </div>
                      </Col>

                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Địa chỉ</p>
                        <span>
                          {renderAddress(
                            dataDetail.order_info.customer_address
                          )}
                        </span>
                      </Col>
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Lĩnh vực kinh doanh</p>
                        <span>
                          {!dataDetail.order_info.customer_job
                            ? "-"
                            : dataDetail.order_info.customer_job}
                        </span>
                      </Col>
                      {dataDetail.order_info.user_type_id === 1 ? (
                        <Col span={24}>
                          <p style={{fontWeight: 600}}>Mã số thuế</p>
                          <span>
                            {!dataDetail.order_info ||
                            dataDetail.order_info.customer_cmnd_date
                              ? "-"
                              : dataDetail.order_info.tax_code}
                          </span>
                        </Col>
                      ) : (
                        <>
                          <Col span={24}>
                            <p style={{fontWeight: 600}}>CMND/CCCD</p>
                            <span>
                              {!dataDetail.order_info.customer_cmnd
                                ? "-"
                                : dataDetail.order_info.customer_cmnd}
                            </span>
                          </Col>
                          <Col span={24}>
                            <p style={{fontWeight: 600}}>Nơi cấp</p>
                            <span>
                              {!dataDetail.order_info.customer_cmnd_place
                                ? "-"
                                : dataDetail.order_info.customer_cmnd_place.replace(
                                  /^,/g,
                                  ""
                                )}
                            </span>
                          </Col>
                          <Col span={24}>
                            <p style={{fontWeight: 600}}>Ngày cấp</p>
                            <span>
                              {!dataDetail.order_info.customer_cmnd_date ||
                              /1970/g.test(
                                dataDetail.order_info.customer_cmnd_date
                              )
                                ? "-"
                                : FormatterDay.dateFormatWithString(
                                  dataDetail.order_info.customer_cmnd_date,
                                  "#DD#/#MM#/#YYYY#"
                                )}
                            </span>
                          </Col>
                        </>
                      )}
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Nhân viên đánh giá</p>
                        <span>
                          {!dataDetail.order_info.sale_man_rate
                            ? "-"
                            : dataDetail.order_info.sale_man_rate}
                        </span>
                      </Col>
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Hình thức thanh toán</p>
                        <span>
                          {!dataDetail.payment_stage ||
                          dataDetail.payment_stage.length == 0
                            ? "-"
                            : dataDetail.payment_stage[0].payment_type_id == 1
                              ? "Tiền mặt"
                              : "Ngân hàng"}
                        </span>
                      </Col>
                      {dataDetail.payment_stage &&
                      Array.isArray(dataDetail.payment_stage)
                        ? dataDetail.payment_stage.map((item) => {
                          return (
                            <Col span={24}>
                              <p style={{fontWeight: 600}}>
                                {item.name ? item.name : "-"}
                              </p>
                              <span
                                style={{fontStyle: "italic", fontSize: 14}}
                              >
                                  {item.description || null}
                                </span>
                              <p>
                                {!item.total_money_payment
                                  ? "-"
                                  : `${priceFormat(
                                    item.total_money_payment
                                  )} VNĐ`}
                              </p>
                            </Col>
                          );
                        })
                        : null}
                    </div>
                  </Col>
                </Row>
              )}
            </Col>
            <Col span={19} style={{paddingTop: 0, paddingBottom: 0}}>
              <div
                style={{
                  padding: "22px 30px",
                  background: colors.white,
                  height: "100%",
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                }}
              >
                {isLoadingAPI ? (
                  <Skeleton active loading={true} paragraph={{rows: 16}}/>
                ) : (
                  <Row>
                    <Col span={24}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Sản phẩm đặt mua
                        </StyledITileHeading>
                        <div>
                          <IButton
                            title={"Biểu mẫu"}
                            icon={ISvg.NAME.FORM}
                            color={colors.main}
                            styleHeight={{
                              width: 140,
                            }}
                            onClick={() => {
                              setIsModalForm(true);
                            }}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div style={{marginTop: 20, display: "flex"}}>
                        <ITableNotFooter
                          data={titleTableProduct}
                          columns={columnsTableProduct}
                          hidePage={true}
                          bordered={true}
                          backgroundWhite={true}
                          style={{
                            width: 450,
                            height: "100%",
                            borderLeft: "1px solid rgba(122, 123, 123, 0.3)",
                            borderTop: "1px solid rgba(122, 123, 123, 0.3)",
                          }}
                          scroll={{
                            y: 0,
                          }}
                          locale={{
                            emptyText: (
                              <div
                                style={{
                                  height: 385,
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                <Empty description="Không có dữ liệu"/>
                              </div>
                            ),
                          }}
                          size="small"
                          pagination={false}
                        />
                        <ITableNotFooter
                          data={dataTablePrice}
                          columns={columnsTablePrice}
                          hidePage={true}
                          bordered
                          backgroundWhite={true}
                          rows={100}
                          style={{
                            width: "100%",
                            height: "100%",
                            border: "1px solid rgba(122, 123, 123, 0.3)",
                            borderBottom: "none",
                          }}
                          scroll={{
                            y: 0,
                          }}
                          locale={{
                            emptyText: (
                              <div
                                style={{
                                  height: 385,
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                <Empty description="Không có dữ liệu"/>
                              </div>
                            ),
                          }}
                          size="small"
                          pagination={false}
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div style={{marginTop: 25, display: "flex"}}>
                        <div style={{width: 450}}></div>
                        <ITableNotFooter
                          data={dataTableFinance}
                          columns={columnsTableFinance}
                          hidePage={true}
                          bordered
                          backgroundWhite={true}
                          rows={100}
                          style={{
                            width: "100%",
                            height: "100%",
                            border: "1px solid rgba(122, 123, 123, 0.3)",
                            borderBottom: "none",
                          }}
                          scroll={{
                            y: 0,
                          }}
                          locale={{
                            emptyText: (
                              <div
                                style={{
                                  height: 385,
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                <Empty description="Không có dữ liệu"/>
                              </div>
                            ),
                          }}
                          size="small"
                          pagination={false}
                        />
                      </div>
                    </Col>
                  </Row>
                )}
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <IModal
        visible={isModalApproved}
        footer={null}
        width={450}
        centered={true}
        closable={false}
        onCancel={async () => {
          setIsModalApproved(false);
        }}
        style={{paddingTop: 0}}
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{textAlign: "center"}}>
                <StyledITitle style={{color: colors.main, fontSize: 17}}>
                  Thông báo
                </StyledITitle>
              </div>
            </Col>

            <Col span={24}>
              <div style={{textAlign: "center"}}>
                <span style={{fontWeight: 500}}>
                  Phiếu yêu cầu {dataDetail.order_info.order_code_child} đã được
                  duyệt
                </span>
              </div>
            </Col>
          </Row>
        </div>
      </IModal>

      <Modal
        visible={isModal}
        footer={null}
        width={350}
        centered={true}
        closable={false}
        style={{paddingTop: 0}}
        onCancel={() => {
          setIsModal(false);
        }}
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{textAlign: "center"}}>
                <StyledITitle style={{color: colors.main, fontSize: 17}}>
                  {typeButton === 3 ? "Thông tin phản hồi" : "Lý do từ chối"}
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div style={{width: "100%", marginTop: 5}}>
                <IInputTextArea
                  border={true}
                  style={{
                    minHeight: 100,
                    maxHeight: 300,
                    border: "1px solid #rgba(255, 255, 255, 0.5)",
                  }}
                  value={reasons}
                  placeholder={
                    typeButton === 3
                      ? "Nhập thông tin phản hồi"
                      : "Nhập lý do từ chối"
                  }
                  onChange={(e) => {
                    setReasons(e.target.value);
                  }}
                />
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -60,
              right: -1,
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <div style={{width: 165}}>
              <IButton
                color={colors.oranges}
                title="Hủy bỏ"
                styleHeight={{minWidth: 0}}
                onClick={() => {
                  setIsModal(false);
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{width: 165}}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                styleHeight={{minWidth: 0}}
                icon={ISvg.NAME.CHECKMARK}
                loading={loadingButton}
                onClick={() => {
                  if (reasons === "") {
                    message.error(
                      `Vui lòng nhập ${
                        typeButton === 3
                          ? "thông tin phản hồi"
                          : "lý do từ chối"
                      }`
                    );
                    return;
                  }

                  setLoadingButton(true);
                  const obj = {
                    id: Number(id),
                    type: typeButton,
                    value: reasons,
                  };
                  _postUpdateStatusOrderRequest(obj);
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        visible={isModalForm}
        footer={null}
        width={960}
        centered={true}
        closable={false}
        onCancel={() => {
          setIsModalForm(false);
        }}
        bodyStyle={{marginTop: 20}}
      >
        <div ref={rootRef}>
          <div
            style={{
              position: "relative",
              top: -25,
              right: -945,
              width: 100,
            }}
          >
            <IButton
              title="In"
              loading={loadingButton}
              icon={ISvg.NAME.PRINT}
              color={colors.main}
              onClick={async () => {
                try {
                  setLoadingButton(true)
                  var pdf = new jsPDF("p", "mm", 'a4');
                  const canvas = await html2canvas(rootRef.current, {
                    scale: 3,
                  });
                  const data = canvas.toDataURL('image/jpeg');
                  const imgProperties = pdf.getImageProperties(data);
                  const pdfWidth = pdf.internal.pageSize.getWidth();
                  const pdfHeight =
                    (imgProperties.height * pdfWidth) / imgProperties.width;
                  pdf.addImage(data, 'PNG', 0, 0, pdfWidth, pdfHeight);
                  pdf.autoPrint({variant: 'non-conform'});
                  setLoadingButton(false)
                  window.open(pdf.output('bloburl'), '_blank');
                } catch (e) {
                  setLoadingButton(false)
                  console.log(e)
                }
              }}
            />
          </div>

          <div
            style={{
              height: "100%",
              padding: "35px 75px 60px 75px",
              fontFamily: "Times New Roman, Times, serif",
            }}
          >
            <div style={{display: "flex", width: "100%"}}>
              <div style={{marginRight: 35}}>
                <img
                  src={images.logo_form}
                  style={{
                    width: 127,
                    objectFit: "contain",
                  }}
                />
              </div>
              <div style={{width: "100%"}}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <h4 style={{fontWeight: "bold", fontSize: 22}}>
                    CÔNG TY CỔ PHẦN NGUYÊN GIA PHÁT
                  </h4>
                </div>
                <div style={{display: "flex", marginTop: 11}}>
                  <p style={{width: 131, fontWeight: "bold", fontSize: 17}}>
                    Địa chỉ trụ sở:
                  </p>
                  <span style={{textAlign: "justify", fontSize: 17}}>
                    1/2B Đại lộ Bình Dương, KP. Hòa Long, P. Lái Thiêu, TX.
                    Thuận An, Tỉnh Bình Dương.
                  </span>
                </div>
                <div style={{display: "flex", marginTop: 1}}>
                  <p style={{width: 86, fontWeight: "bold", fontSize: 17}}>
                    Điện thoại:
                  </p>
                  <span style={{textAlign: "justify", fontSize: 17}}>
                    028 6295 0081 - 0906 919 639
                  </span>
                </div>
                <div style={{display: "flex", marginTop: 1}}>
                  <p style={{width: 69, fontWeight: "bold", fontSize: 17}}>
                    Website:
                  </p>
                  <span style={{textAlign: "justify", fontSize: 17}}>
                    http://nguyengiaphatauto.com/
                  </span>
                </div>
                <div style={{display: "flex", marginTop: 1}}>
                  <p style={{width: 55, fontWeight: "bold", fontSize: 17}}>
                    Email:
                  </p>
                  <span style={{textAlign: "justify", fontSize: 17}}>
                    maketing@nguyengiaphat.com
                  </span>
                </div>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                width: "100%",
                marginTop: 50,
              }}
            >
              <h1 style={{fontWeight: "bold"}}>PHIẾU YÊU CẦU</h1>
            </div>
            <div style={{marginTop: 50}}>
              <p style={{fontWeight: "bold", fontSize: 17}}>
                1. Thông tin khách hàng:
              </p>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.order_info.customer_name ||
                    dataDetail.order_info.customer_name === ""
                      ? "18px 0px 0px 18px"
                      : "6px 0px 0px 18px",
                }}
              >
                <p style={{width: 82, fontSize: 17, lineHeight: "11px"}}>
                  Họ và tên:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    justifyContent: "center",
                    textTransform: "uppercase",
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.order_info.customer_name}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.order_info.customer_address ||
                    dataDetail.order_info.customer_address === "" ||
                    dataDetail.order_info.customer_address === ", "
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 64, fontSize: 17, lineHeight: "11px"}}>
                  Địa chỉ:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {renderAddress(dataDetail.order_info.customer_address)}
                </div>
              </div>
              {dataDetail.order_info.user_type_id !== 1 ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-end",
                    margin:
                      !dataDetail.order_info.customer_cmnd ||
                      dataDetail.order_info.customer_cmnd === ""
                        ? "18px 0px 0px 18px"
                        : "10px 0px 0px 18px",
                  }}
                >
                  <p style={{width: 200, fontSize: 17, lineHeight: "11px"}}>
                    CMND:
                  </p>
                  <div
                    style={{
                      width: 510,
                      borderBottom: "2px dotted black",
                      textAlign: "justify",
                      display: "flex",
                      justifyContent: "center",
                      color: "black",
                      lineHeight: "18px",
                      fontSize: 17,
                    }}
                  >
                    {dataDetail.order_info.customer_cmnd}
                  </div>
                  <p
                    style={{
                      width: 248,
                      fontSize: 17,
                      lineHeight: "11px",
                      marginLeft: 4,
                    }}
                  >
                    Ngày cấp:
                  </p>
                  <div
                    style={{
                      width: 600,
                      borderBottom: "2px dotted black",
                      textAlign: "justify",
                      display: "flex",
                      justifyContent: "center",
                      color: "black",
                      lineHeight: "18px",
                      fontSize: 17,
                    }}
                  >
                    {!dataDetail.order_info.customer_cmnd_date ||
                    /1970/g.test(dataDetail.order_info.customer_cmnd_date)
                      ? null
                      : FormatterDay.dateFormatWithString(
                        dataDetail.order_info.customer_cmnd_date,
                        "#DD#/#MM#/#YYYY#"
                      )}
                  </div>
                  <p
                    style={{
                      width: 200,
                      fontSize: 16,
                      lineHeight: "11px",
                      marginLeft: 4,
                    }}
                  >
                    Nơi cấp:
                  </p>
                  <div
                    style={{
                      width: "100%",
                      borderBottom: "2px dotted black",
                      textAlign: "justify",
                      display: "flex",
                      justifyContent: "center",
                      color: "black",
                      lineHeight: "18px",
                      fontSize: 17,
                    }}
                  >
                    {dataDetail.order_info.customer_cmnd_place}
                  </div>
                </div>
              ) : (
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-end",
                    margin:
                      !dataDetail.order_info.tax_code ||
                      dataDetail.order_info.tax_code === ""
                        ? "18px 0px 0px 18px"
                        : "10px 0px 0px 18px",
                  }}
                >
                  <p style={{width: 88, fontSize: 16, lineHeight: "11px"}}>
                    Mã số thuế:
                  </p>
                  <div
                    style={{
                      width: "100%",
                      borderBottom: "2px dotted black",
                      textAlign: "justify",
                      display: "flex",
                      // justifyContent: "center",
                      paddingLeft: 10,
                      color: "black",
                      lineHeight: "18px",
                      fontSize: 17,
                    }}
                  >
                    {dataDetail.order_info.tax_code}
                  </div>
                </div>
              )}
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.order_info.customer_job ||
                    dataDetail.order_info.customer_job === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 205, fontSize: 16, lineHeight: "11px"}}>
                  Ngành hàng kinh doanh:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.order_info.customer_job}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.order_info.sale_man_rate ||
                    dataDetail.order_info.sale_man_rate === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 150, fontSize: 16, lineHeight: "11px"}}>
                  Nhóm khách hàng:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.order_info.sale_man_rate}
                </div>
              </div>
            </div>
            <div style={{marginTop: 30}}>
              <p style={{fontWeight: "bold", fontSize: 17}}>
                2. Thông tin mẫu xe:
              </p>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.product_info.name ||
                    dataDetail.product_info.name === ""
                      ? "18px 0px 0px 18px"
                      : "6px 0px 0px 18px",
                }}
              >
                <p style={{width: 65, fontSize: 17, lineHeight: "11px"}}>
                  Mẫu xe:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    textTransform: "uppercase",
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.product_info.name}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.product_info.color ||
                    dataDetail.product_info.color === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 64, fontSize: 17, lineHeight: "11px"}}>
                  Màu xe:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.product_info.color}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.product_info.product_type_parent ||
                    dataDetail.product_info.product_type_parent === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 62, fontSize: 16, lineHeight: "11px"}}>
                  Loại xe:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.product_info.product_type_parent}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.product_info.product_type ||
                    dataDetail.product_info.product_type === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 68, fontSize: 16, lineHeight: "11px"}}>
                  Dòng xe:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.product_info.product_type}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.product_info.cate ||
                    dataDetail.product_info.cate === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 103, fontSize: 16, lineHeight: "11px"}}>
                  Thương hiệu:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.product_info.cate}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.product_info.brand ||
                    dataDetail.product_info.brand === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 102, fontSize: 16, lineHeight: "11px"}}>
                  Thương hiệu:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.product_info.brand}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.product_info.box_type ||
                    dataDetail.product_info.box_type === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 86, fontSize: 16, lineHeight: "11px"}}>
                  Loại thùng:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.product_info.box_type}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.announced_price === null ||
                    dataDetail.price_info.announced_price === undefined ||
                    dataDetail.price_info.announced_price === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 96, fontSize: 16, lineHeight: "11px"}}>
                  Giá công bố:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(dataDetail.price_info.announced_price)} VNĐ`}
                </div>
              </div>
            </div>
            <div style={{marginTop: 30}}>
              <p style={{fontWeight: "bold", fontSize: 17}}>
                3. Thông tin dự đoán chi phí:
              </p>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.promotion_price === null ||
                    dataDetail.price_info.promotion_price === undefined ||
                    dataDetail.price_info.promotion_price === ""
                      ? "18px 0px 0px 18px"
                      : "6px 0px 0px 18px",
                }}
              >
                <p style={{width: 140, fontSize: 17, lineHeight: "11px"}}>
                  Giá khuyến mãi:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    textTransform: "uppercase",
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(dataDetail.price_info.promotion_price)} VNĐ`}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.crating_price === null ||
                    dataDetail.price_info.crating_price === undefined ||
                    dataDetail.price_info.crating_price === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 136, fontSize: 17, lineHeight: "11px"}}>
                  Giá đóng thùng:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(dataDetail.price_info.crating_price)} VNĐ`}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.registry_check_fee === null ||
                    dataDetail.price_info.registry_check_fee === undefined ||
                    dataDetail.price_info.registry_check_fee === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 199, fontSize: 16, lineHeight: "11px"}}>
                  Phí đăng ký đăng kiểm:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(
                    dataDetail.price_info.registry_check_fee
                  )} VNĐ`}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.bao_hiem_than_xe === null ||
                    dataDetail.price_info.bao_hiem_than_xe === undefined ||
                    dataDetail.price_info.bao_hiem_than_xe === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 145, fontSize: 16, lineHeight: "11px"}}>
                  Bảo hiểm thân xe:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(dataDetail.price_info.bao_hiem_than_xe)} VNĐ`}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.phu_hieu_dinh_vi === null ||
                    dataDetail.price_info.phu_hieu_dinh_vi === undefined ||
                    dataDetail.price_info.phu_hieu_dinh_vi === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 145, fontSize: 16, lineHeight: "11px"}}>
                  Phù hiệu định vị:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(dataDetail.price_info.phu_hieu_dinh_vi)} VNĐ`}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.price_on_contract === null ||
                    dataDetail.price_info.price_on_contract === undefined ||
                    dataDetail.price_info.price_on_contract === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 150, fontSize: 16, lineHeight: "11px"}}>
                  Giá trên hợp đồng:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(
                    dataDetail.price_info.price_on_contract
                  )} VNĐ`}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.total_payment === null ||
                    dataDetail.price_info.total_payment === undefined ||
                    dataDetail.price_info.total_payment === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 202, fontSize: 16, lineHeight: "11px"}}>
                  Tổng số tiền thanh toán:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(dataDetail.price_info.total_payment)} VNĐ`}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    !dataDetail.price_info.other ||
                    dataDetail.price_info.other === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 62, fontSize: 16, lineHeight: "11px"}}>
                  Ghi chú:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {dataDetail.price_info.other}
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  margin:
                    dataDetail.price_info.offer_price === null ||
                    dataDetail.price_info.offer_price === undefined ||
                    dataDetail.price_info.offer_price === ""
                      ? "18px 0px 0px 18px"
                      : "10px 0px 0px 18px",
                }}
              >
                <p style={{width: 93, fontSize: 16, lineHeight: "11px"}}>
                  Giá đề xuất:
                </p>
                <div
                  style={{
                    width: "100%",
                    borderBottom: "2px dotted black",
                    textAlign: "justify",
                    display: "flex",
                    // justifyContent: "center",
                    paddingLeft: 10,
                    color: "black",
                    lineHeight: "18px",
                    fontSize: 17,
                  }}
                >
                  {`${priceFormat(dataDetail.price_info.offer_price)} VNĐ`}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
