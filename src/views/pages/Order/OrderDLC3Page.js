import React, { useState, useEffect } from "react";
import { Row, Col, message, Tooltip } from "antd";
import { useHistory } from "react-router-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IDatePicker,
  ISelectCity,
} from "../../components";
import FormatterDay from "../../../utils/FormatterDay";
import { images, colors } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { APIService } from "../../../services";

import { priceFormat } from "../../../utils";
const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

export default function OrderDLC3Page() {
  const history = useHistory();
  const [data, setData] = useState({
    order: [],
  });
  const [dataBranch, setDataBranch] = useState([]);
  const [loadingSelect, setLoadingSelect] = useState(true);
  const [loadingTable, setLoadingTable] = useState(true);
  const [filterTable, setFilterTable] = useState({
    startValue: startValue,
    endValue: NewDate.setHours(23, 59, 59, 999),
    page: 1,
    search: "",
    user_agency_id: 0,
    city_id: 0,
    district_id: 0,
  });

  const [listStatus, setListStatus] = useState([]);
  const [listCounty, setListCounty] = useState([]);
  const [loadingCounty, setLoadingCounty] = useState(false);
  const _fetchAPIListOrderS3 = async (filter) => {
    try {
      const data = await APIService._getOrderFarmer(
        filter.user_agency_id,
        filter.city_id,
        filter.district_id,
        filter.startValue,
        filter.endValue,
        filter.search,
        filter.page
      );
      data.order.map(
        (item, index) => (item.stt = (filterTable.page - 1) * 10 + index + 1)
      );
      setData(data);
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIListOrderS3(filterTable);
  }, [filterTable]);

  useEffect(() => {
    _getListStatus();
  }, []);

  const _getListStatus = async () => {
    try {
      const data = await APIService._getListStatusAppSerice(1);

      var dataNew = data.order_status.map((item, index) => {
        const value = item.name_app_s1;
        const key = item.id;
        // StatusPare.push({ value, key });
        return {
          key,
          value,
        };
      });
      let dataNewTemp = [];
      dataNew.map((item, index) => {
        if (item.value === "" || !item.value) {
          return;
        }
        dataNewTemp.push(item);
      });

      dataNewTemp.unshift({
        value: "Tất cả",
        key: 0,
      });
      setListStatus(dataNewTemp);
      setLoadingSelect(false);
    } catch (error) {}
  };

  useEffect(() => {
    if (filterTable.city_id !== 0) {
      _getAPIListCounty(filterTable.city_id, "");
    }
  }, [filterTable.city_id]);

  const _getAPIListCounty = async (city_id, search) => {
    try {
      const data = await APIService._getListCounty(city_id, search);
      let dataNew = data.district.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });

      setListCounty(dataNew);
      loadingCounty(false);
    } catch (error) {
      setLoadingCounty(false);
    }
  };

  const _getAPIListBranch = async () => {
    try {
      const data = await APIService._getBranchDropList();
      const dataNew = data.agency.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDataBranch(dataNew);
    } catch (error) {}
  };

  useEffect(() => {
    _getAPIListBranch();
  }, []);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Mã đơn",
      dataIndex: "order_code",
      key: "order_code",
      render: (order_code) => <span>{order_code}</span>,
    },
    {
      title: "Ngày đặt",
      dataIndex: "order_day",
      key: "order_day",
      render: (order_day) => (
        <span>
          {FormatterDay.dateFormatWithString(order_day, "#DD#/#MM#/#YYYY#")}
        </span>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      render: (status) => <span>{status}</span>,
    },
    {
      title: "Người đặt",
      dataIndex: "person_order",
      key: "person_order",
      render: (person_order) => <span>{person_order}</span>,
    },
    {
      title: "Người mua",
      dataIndex: "person_buy",
      key: "person_buy",
      render: (person_buy) => <span>{person_buy}</span>,
    },

    {
      title: "Đại lý tiếp nhận",
      dataIndex: "name_s2",
      key: "name_s2",
      render: (name_s2) => <span>{name_s2}</span>,
    },
    {
      title: "Doanh số đặt",
      dataIndex: "order_price",
      key: "order_price",
      render: (order_price) => <span>{priceFormat(order_price) + "đ"}</span>,
    },
    {
      title: "Doanh số hoàn thành",
      dataIndex: "complete_price",
      key: "complete_price",
      render: (complete_price) => (
        <span>{priceFormat(complete_price) + "đ"}</span>
      ),
    },

    {
      title: "",
      width: 40,
      align: "center",
      render: (obj) => {
        return (
          <Tooltip title="Xem chi tiểt đơn hàng">
            <div
              className="flex item-center justify-end cursor-push"
              id="nextDetail"
              onClick={(e) => {
                e.stopPropagation();
                history.push(`/orderPageS3/detail/${obj.id}`);
              }}
            >
              <ISvg
                name={ISvg.NAME.CHEVRONRIGHT}
                width={12}
                fill={colors.icon.default}
              />
            </div>
          </Tooltip>
        );
      },
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Danh sách đơn hàng nông dân
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã đơn hàng, người đặt , người mua">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo mã đơn hàng, người đặt , người mua"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    search: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{
                      marginLeft: 25,
                      marginRight: 25,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filterTable.startValue}
                      to={filterTable.endValue}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          startValue: date[0]._d.setHours(0, 0, 0),
                          endValue: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>
                  <ITitle
                    title="Lọc theo"
                    level={4}
                    style={{
                      marginLeft: 60,
                      marginRight: 25,
                    }}
                  />
                  <div style={{ width: 200 }}>
                    <ISelect
                      defaultValue="Phòng kinh doanh"
                      data={dataBranch}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          user_agency_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{ margin: "0px 15px 0px 15px" }}>
                    <ISelectCity
                      defaultValue="Tỉnh / Thành"
                      fillerAll={true}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        if (value !== 0) {
                          setLoadingCounty(true);
                        } else {
                          setListCounty([{ key: 0, value: "Tất cả" }]);
                          setFilterTable({
                            ...filterTable,
                            city_id: value,
                            page: 1,
                            district_id: 0,
                          });
                          return;
                        }
                        setFilterTable({
                          ...filterTable,
                          city_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{ margin: "0px 15px 0px 15px" }}>
                    <ISelect
                      defaultValue="Quận / Huyện"
                      data={listCounty}
                      select={true}
                      value={
                        filterTable.district_id === 0
                          ? "Quận / Huyện"
                          : filterTable.district_id
                      }
                      loading={loadingCounty}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          district_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>
              {/* <Col span={4}>
                <Row gutter={[15, 0]}>
                  <div className='flex justify-end'>
                    <IButton
                      icon={ISvg.NAME.DOWLOAND}
                      title={'Xuất file'}
                      color={colors.main}
                      styleHeight={{
                        width: 120
                      }}
                      onClick={() => {
                        message.warning('Chức năng đang cập nhật')
                      }}
                    />
                  </div>
                </Row>
              </Col> */}
            </Row>
          </Col>
          <Col span={24}>
            <div style={{ marginTop: 50 }}>
              <Row>
                <ITable
                  columns={columns}
                  data={data.order}
                  style={{ width: "100%" }}
                  defaultCurrent={1}
                  sizeItem={data.size}
                  rowKey="id"
                  indexPage={filterTable.page}
                  maxpage={data.total}
                  loading={loadingTable}
                  onRow={(record, rowIndex) => {
                    return {
                      onClick: (event) => {
                        const indexRow = Number(
                          event.currentTarget.attributes[1].ownerElement.dataset
                            .rowKey
                        );
                        history.push(`/orderPageS3/detail/${indexRow}`);
                      }, // click row
                    };
                  }}
                  onChangePage={(page) => {
                    setLoadingTable(true);
                    setFilterTable({ ...filterTable, page: page });
                  }}
                ></ITable>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
