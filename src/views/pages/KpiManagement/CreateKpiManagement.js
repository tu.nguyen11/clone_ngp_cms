import React, {useEffect, useState} from 'react'
import {Checkbox, Col, Empty, List, Modal, Row, message} from "antd";
import {StyledITitle} from "../../components/common/Font/font";
import {colors} from "../../../assets";
import {Form} from 'antd'
import {useParams, useHistory} from 'react-router-dom'
import {IButton, IMonthPicker, IInput, ISvg, ITable, ISelect, ITableHtml} from "../../components";
import {priceFormat} from "../../../utils";
import styled from "styled-components";
import moment from 'moment'
import {IError, IFormItem, IInputText, IInputTextArea} from "../../components/common";
import ModalKpi from './ModalKpi'
import APIService from "../../../services/APIService";
import {useDispatch, useSelector} from "react-redux";
import {StyledInputNumber} from "../../components/common";
import {
  EDIT_COUNT,
  REMOVE_ITEM_APPROVE,
  CANCEL_LIST_KPI_MODAL_ALL
} from '../../store/reducers'

const StyledInputNumberNew = styled(StyledInputNumber)`
  .ant-input-number-input {
    text-align: center;
    height: 38px !important;
  }
`;

function CreateKpi(props) {
  const {type, id} = useParams();
  const history = useHistory()
  const [isModal, setIsModal] = useState(false);
  const reduxForm = props.form;
  const [listShowroom, setListShowRoom] = useState([])
  const [time, setTime] = useState()
  const {getFieldDecorator} = reduxForm;
  const monthFormat = 'MM/YYYY';
  const [loadingSelect, setLoadingSelect] = useState(true)
  const [filterDate, setFilterDate] = useState({
    year: new Date().getFullYear(),
    month: new Date().getMonth() + 1
  })
  const [loadingButton, setLoadingButton] = useState(false)
  const [listTargetEdit, setListTargetEdit] = useState([])
  const dispatch = useDispatch()
  const dataRoot = useSelector((state) => state);
  const {listTargetKpi} = dataRoot;

  async function getInfoCreater() {
    try {
      const response = await APIService._getListCreaterKpimanagement()
      reduxForm.setFieldsValue({
        _name: response.name,
      });
    } catch (error) {
      console.log(error)
    }

  }

  const getListShowRoom = async (filterDate) => {
    try {
      const response = await APIService._filterShowroomForDate(filterDate)
      if (!response.showrooms) {
        setListShowRoom([])
      } else setListShowRoom(response.showrooms)
      reduxForm.setFieldsValue({
        _showroom: response.showrooms && response.showrooms.length !== 0 ? response.showrooms[0].id : '',
      });
      setLoadingSelect(false)
      setTime(response.time)
    } catch (error) {
      setLoadingSelect(false)
    }
  }
  /**
   *
   * @param filterDate
   * @param id
   * @param isEditing
   * @returns {Promise<void>}
   */
  const getListShowRoomEdit = async (filterDate, id, isEditing) => {
    try {
      const response = await APIService._getListShowRoomEdit(filterDate, id)
      if (!response.showrooms) {
        setListShowRoom([])
      } else setListShowRoom(response.showrooms)
      if (isEditing) {
        reduxForm.setFieldsValue({
          _showroom: response.showrooms && response.showrooms.length !== 0 ? response.showrooms[0].id : '',
        });
      }
      setLoadingSelect(false)
      setTime(response.time)
    } catch (error) {
      setLoadingSelect(false)
    }
  }
  useEffect(() => {
    if (type !== 'edit') {
      getListShowRoom(filterDate)
    }
  }, [filterDate])
  const getDetailkpi = async (id) => {
    try {
      const dataResponse = await APIService._getDetailKpiManagement(id);
      setListTargetEdit(dataResponse.targets)
      setFilterDate({
        year: dataResponse.year,
        month: dataResponse.month,
      })
      reduxForm.setFieldsValue({
        _showroom: dataResponse.detail.agency_id,
        _note: dataResponse.detail.note,
      });
      return {
        year: dataResponse.year,
        month: dataResponse.month,
      }
    } catch (error) {
      console.log(error);
    }
  };
  useEffect( () => {
    getInfoCreater()
    if (type == 'edit') {
      const fetchData = async () =>{
        const res = await getDetailkpi(id)
        await getListShowRoomEdit(res, id, false)
      }
      fetchData()
    }
  }, [])
  const columns = [
    {
      title: "Tên các chỉ tiêu",
    },
    {
      title: "Mô tả",
    },
    {
      title: "Chỉ tiêu",
      align: "center",
    },
    {title: "", width: 110, textAlign: "center"},
  ];
  const headerTableProduct = (headerTable) => {
    return (
      <tr className="scroll" style={{background: "#E4EFFF"}}>
        {headerTable.map((item, index) => (
          <th className="th-table" style={{textAlign: item.align}}>
            {item.title}
          </th>
        ))}
      </tr>
    );
  };
  const bodyTableProduct = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        className="tr-table"
        style={{
          borderLeft:
            "1px solid rgba(122, 123, 123, 0.5)",
          borderRight:
            "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td className="td-table" colSpan="8">
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 4,
              paddingLeft: 10,
              justifyContent: 'center'
            }}
          >
            <div
              style={{
                height: 385,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Empty description="Không có dữ liệu"/>
            </div>
          </div>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => {
        return (
          <tr key={index} className="tr-table scroll">
            <td
              className="td-table"
              style={{fontWeight: "normal"}}
            >
              {item.name}
            </td>
            <td
              className="td-table"
              style={{textAlign: "left", fontWeight: "normal"}}
            >
              {item.description}
            </td>
            <td
              className="td-table"
              style={{textAlign: "left", fontWeight: "normal"}}
            >
              <div style={{textAlign: "center", width: "100%"}}>
                <StyledInputNumberNew
                  value={
                    !listTargetKpi.countTagrgetShow[item.id] ||
                    !listTargetKpi.countTagrgetShow[item.id].quantity
                      ? 0
                      : listTargetKpi.countTagrgetShow[item.id].quantity
                  }
                  style={{width: "100%", height: 40}}
                  onChange={(value) => {
                    const dataAction = {
                      keyRoot: "listTargetKpi",
                      countTagrgetShow: "countTagrgetShow",
                      countTagrgetApprove: "countTagrgetApprove",
                      idProduct: item.id,
                      valueUpdate: {quantity: value},
                    };
                    dispatch(EDIT_COUNT(dataAction))
                  }}
                  min={0}
                  formatter={(value) =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                  parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                />
              </div>
            </td>
            <td
              className="td-table"
              style={{
                textAlign: "center",
                maxWidth: 120,
                fontWeight: "normal",
              }}
            >
              <div
                className="cursor"
                style={{
                  textAlign: "center",
                  margin: 'auto',
                  background: "rgba(227, 95, 75, 0.1)",
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
                onClick={() => {
                  let dataAction = {
                    keyRoot: "listTargetKpi",
                    keyApprove: 'listTargetApprove',
                    keyShow: 'listTargetShow',
                    keyTargetClone: 'listTargetClone',
                    id: item.id,
                  }
                  dispatch(REMOVE_ITEM_APPROVE(dataAction))
                }}
              >
                <ISvg
                  name={ISvg.NAME.CROSS}
                  width={8}
                  height={8}
                  fill={colors.oranges}
                />
              </div>
            </td>
          </tr>
        )
      })
    );
  };
  const addTarget = async (obj) => {
    try {
      const data = await APIService._createTargetKpi(obj)
      setLoadingButton(false)
      message.success("Tạo chỉ tiêu thành công");
      dispatch(CANCEL_LIST_KPI_MODAL_ALL())
      history.push("/kpimanagement/list");
    } catch (e) {
      setLoadingButton(false)
    }
  }
  const updateTarget = async (obj) => {
    try {
      const data = await APIService._editTargetKpi(obj)
      setLoadingButton(false)
      message.success("Cập nhật chỉ tiêu thành công");
      dispatch(CANCEL_LIST_KPI_MODAL_ALL())
      history.push("/kpimanagement/detail/" + id);
    } catch (e) {
      setLoadingButton(false)
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields((err, obj) => {
      if (!err) {
        if (listTargetKpi.listTargetApprove.length === 0) {
          IError("Vui lòng thêm Chỉ tiêu");
          return;
        }
        if (type === 'create') {
          let target = listTargetKpi.listTargetApprove.reduce((previousValue, currentValue, currentIndex) => {
            let targetCount = listTargetKpi.countTagrgetApprove[currentValue.id] ? listTargetKpi.countTagrgetApprove[currentValue.id].quantity : 0
            return {
              ...previousValue, [currentValue.id]: targetCount
            }
          }, {})
          const objAdd = {
            "agency": obj._showroom, // id_agency
            "creator": obj._name,
            "list_target": target,
            "note": obj._note ? obj._note : '',
            "time": time
          };
          setLoadingButton(true)
          addTarget(objAdd)
        } else {
          let list_target_new = listTargetKpi.listTargetApprove.map(item => {
            return {
              id: listTargetEdit.find(item1 => item1.target_id === item.id) ? listTargetEdit.find(item1 => item1.target_id === item.id).id : 0,
              target_id: item.id,
              target: listTargetKpi.countTagrgetApprove[item.id] ? listTargetKpi.countTagrgetApprove[item.id].quantity : 0,
            }
          })
          const updateObj = {
            'id': id,
            "agency_id": obj._showroom,
            // "username": obj._name,
            "list_target_new": list_target_new,
            "note": obj._note ? obj._note : '',
            "time": time
          }
          setLoadingButton(true)
          updateTarget(updateObj)
        }
      }
    })
  }
  return (
    <div>
      <Form onSubmit={handleSubmit} style={{width: "100%"}}>
        <Row>
          <Col span={24}>
            <Row>
              <Col span={12}>
                <StyledITitle style={{color: colors.main, fontSize: 22}}>
                  {type == 'edit' ? 'Chỉnh sửa chỉ tiêu' : 'Tạo mới chỉ tiêu'}
                </StyledITitle>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row style={{marginBottom: 40}}>
          <Col style={{display: 'flex', justifyContent: 'flex-end'}} span={24}>
            <IButton
              color={colors.main}
              icon={ISvg.NAME.WRITE}
              title={'Lưu'}
              loading={loadingButton}
              htmlType="submit"
            />
            <IButton
              color={colors.oranges}
              style={{marginLeft: 15}}
              icon={ISvg.NAME.CROSS}
              title={'Hủy'}
              onClick={() => {
                dispatch(CANCEL_LIST_KPI_MODAL_ALL())
                if (type === "create") {
                  history.push('/kpimanagement/list/')
                } else history.push('/kpimanagement/detail/' + id)

              }}
            />
          </Col>
        </Row>

        <Row type='flex' gutter={[16, 16]}>
          <Col span={8}>
            <div style={{
              padding: '30px 40px',
              backgroundColor: "white",
              height: '100%'
            }}>
              <Row>
                <Col style={{
                  paddingBottom: 25,
                }} span={24}>
                  <StyledITitle style={{fontSize: 16}}>
                    Thông tin chỉ tiêu
                  </StyledITitle>
                </Col>
                <Col style={{
                  paddingBottom: 20,
                }} span={24}>
                  <div>
                    <p style={{fontWeight: 500}}>Người tạo</p>
                    <Form.Item>
                      {getFieldDecorator("_name", {
                        rules: [
                          {
                            required: true,
                            message: "Tên người tạo không tồn tại",
                          },
                        ],
                      })(
                        <IInput
                          placeholder={'Chọn tên người tạo'}
                          value={reduxForm.getFieldValue('_name')}
                          disabled
                        />
                      )}
                    </Form.Item>
                  </div>
                </Col>

                <Col style={{
                  paddingBottom: 20,
                }} span={24}>
                  <div>
                    <p style={{fontWeight: 500}}>Thời gian (*)</p>
                    <Form.Item>
                      <IMonthPicker
                        allowClear={false}
                        isBackground
                        from={moment(filterDate.month + '/' + filterDate.year, monthFormat)}
                        format={monthFormat}
                        value={moment(filterDate.month + '/' + filterDate.year, monthFormat)}
                        onChange={(date) => {
                          if (!date) {
                            setFilterDate({
                              year: new Date().getFullYear(),
                              month: new Date().getMonth() + 1
                            })
                            if(type === 'edit'){
                              getListShowRoomEdit({
                                year: new Date().getFullYear(),
                                month: new Date().getMonth() + 1
                              }, id, true)
                            }
                          } else {
                            setFilterDate({
                              month: new Date(date._d).getMonth() + 1,
                              year: new Date(date._d).getFullYear()
                            })
                            if(type === 'edit'){
                              getListShowRoomEdit({
                              month: new Date(date._d).getMonth() + 1,
                              year: new Date(date._d).getFullYear()
                            }, id, true)}
                          }
                          setLoadingSelect(true);
                        }}
                      />
                    </Form.Item>
                  </div>
                </Col>

                <Col style={{
                  paddingBottom: 20,
                }} span={24}>
                  <div style={{width: 300}}>
                    <p style={{fontWeight: 500}}>Showroom (*)</p>
                    <Form.Item>
                      {getFieldDecorator("_showroom", {
                        rules: [
                          {
                            required: true,
                            message: "Vui lòng chọn showroom",
                          },
                        ],
                      })(
                        <ISelect
                          data={listShowroom}
                          select={true}
                          loading={loadingSelect}
                          placeholder={'Showroom'}
                          value={reduxForm.getFieldValue('_showroom')}
                          onChange={(key) => {
                            reduxForm.setFieldsValue({
                              _showroom: key
                            });
                          }}
                        />
                      )}
                    </Form.Item>
                  </div>
                </Col>
                <Col style={{
                  paddingBottom: 20,
                }} span={24}>
                  <div>
                    <p style={{fontWeight: 500}}>Ghi chú</p>
                    <Form.Item>
                      {getFieldDecorator("_note", {})(
                        <IInputTextArea
                          value={reduxForm.getFieldValue('_note')}
                          placeholder={'Nhập ghi chú'}
                          onChange={(e) => {
                            reduxForm.setFieldsValue({
                              _note: e.target.value,
                            });
                          }}
                        />
                      )}
                    </Form.Item>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
          <Col span={14}>
            <div style={{
              padding: '30px 40px',
              backgroundColor: "white",
              height: '100%'
            }}>
              <Row>
                <Col style={{
                  paddingBottom: 25,
                }} span={12}>
                  <StyledITitle style={{fontSize: 16}}>
                    Thông tin chỉ tiêu: {listTargetKpi.listTargetApprove.length}
                  </StyledITitle>
                </Col>
                <Col span={12}>
                  <div style={{display: 'flex', justifyContent: 'flex-end', cursor: "pointer"}}
                       onClick={() => setIsModal(true)}>
                    <span>Thêm chỉ tiêu</span>
                    <div
                      style={{
                        width: 20,
                        height: 20,
                        marginLeft: 10,
                        borderRadius: 10,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        background:
                          "rgba(0, 76, 145, 0.1)",

                      }}
                    >
                      <ISvg
                        name={ISvg.NAME.ADD}
                        width={7}
                        height={7}
                        fill={colors.main}
                      />
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <ITableHtml
                  childrenBody={
                    bodyTableProduct(listTargetKpi.listTargetApprove)
                  }
                  childrenHeader={headerTableProduct(columns)}
                />
              </Row>
            </div>
          </Col>
        </Row>
      </Form>
      <Modal
        visible={isModal}
        footer={null}
        width={1200}
        onCancel={() => setIsModal(false)}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {ModalKpi(() => {
          setIsModal(false);
        })}
      </Modal></div>
  )
}

const createKpimanagement = Form.create({name: "CreateKpi"})(CreateKpi);

export default createKpimanagement
