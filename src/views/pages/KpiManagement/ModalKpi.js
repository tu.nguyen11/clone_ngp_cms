import React, {useEffect, useState} from 'react'
import styled from 'styled-components'
import {Checkbox, Col, Form, List, Row} from "antd";
import {StyledITitle} from "../../components/common/Font/font";
import {colors} from "../../../assets";
import {IButton, ISvg} from "../../components";
import APIService from "../../../services/APIService";
import {
    ASSIGN_LIST_TARGET_KPI,
    ADD_LIST_CLONE_CHOOSE,
    ADD_LIST_SHOW_CHOOSE,
    EDIT_COUNT,
    SAVE_LIST_APPROVE,
    CANCEL_LIST_KPI_MODAL,
    REMOVE_ITEM_SHOW
} from '../../store/reducers'
import {useDispatch, useSelector} from "react-redux";
import {StyledInputNumber} from "../../components/common";

const StyledInputNumberNew = styled(StyledInputNumber)`
  .ant-input-number-input {
    text-align: center;
    height: 38px !important;
  }
`;

function ModalKpi(callbackClose = () => {
}) {
    const dispatch = useDispatch()
    const dataRoot = useSelector((state) => state);
    const {listTargetKpi} = dataRoot;
    const getListTargetKpi = async () => {
        try {
            const response = await APIService._getListTargetKpi();
            const dataAction = {
                keyRoot: "listTargetKpi",
                key: "listTarget",
                value: response.targets,
            };
            dispatch(ASSIGN_LIST_TARGET_KPI(dataAction))
        } catch (err) {
            console.log(err)
        }

    }
    useEffect(() => {
        getListTargetKpi()
    }, [])
    return (
        <div style={{width: "100%", height: "100%"}}>
            <Row gutter={[26, 0]}>
                <Col span={24}>
                    <Row>
                        <Col style={{marginBottom: 40}} span={12}>
                            <StyledITitle style={{fontSize: 16,}}>
                                Thiết lập hiển thị chỉ tiêu
                            </StyledITitle>
                        </Col>
                    </Row>
                </Col>
                <Col span={13}> <Row>
                    <div
                        style={{
                            border: "1px solid rgba(122, 123, 123, 0.5)",
                            backgroundColor: "#E4EFFF",
                            display: "flex",
                            alignItems: "center",
                            paddingRight: 0
                        }}
                    >
                        <Col span={2} style={{
                            paddingTop: 10,
                            paddingBottom: 10,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                        }}>
                            {/*<Checkbox*/}
                            {/*    checked={true}*/}
                            {/*    // style={{ marginRight: 35 }}*/}
                            {/*    defaultChecked={*/}
                            {/*        false*/}
                            {/*    }*/}
                            {/*    onChange={(e) => {*/}

                            {/*    }}*/}
                            {/*/>*/}
                        </Col>
                        <Col span={7}>
                            <div
                                style={{
                                    fontWeight: "bold",
                                    padding: "6px 12px",
                                    color: colors.blackChart,
                                }}
                            >
                                Tên tiêu đề
                            </div>
                        </Col>
                        <Col span={15}>
                            <div
                                style={{
                                    fontWeight: "bold",
                                    padding: "6px 12px",
                                    color: colors.blackChart,
                                }}
                            >
                                Mô tả
                            </div>
                        </Col>
                    </div>
                    <div style={{
                        borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                        borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                        borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                        height: 400,
                        overflowY: 'scroll'
                    }}>
                        <List
                            dataSource={listTargetKpi.listTarget}
                            bordered={false}
                            renderItem={(item, index) => (
                                <Row key={index}>
                                    <List.Item>
                                        <Col span={2} style={{
                                            paddingTop: 10,
                                            paddingBottom: 10,
                                            display: "flex",
                                            alignItems: "center",
                                            justifyContent: "center",
                                        }}>
                                            <Checkbox
                                                value={item}
                                                checked={listTargetKpi.listTargetClone.length !== 0 ? listTargetKpi.listTargetClone.find(item1 => item1.id === item.id) ? true : false : false}
                                                onChange={(e) => {
                                                    let dataAction = {
                                                        keyRoot: "listTargetKpi",
                                                        key: "listTargetClone",
                                                        value: e.target.value,
                                                        id: item.id
                                                    }
                                                    dispatch(ADD_LIST_CLONE_CHOOSE(dataAction))
                                                }}
                                            />
                                        </Col>
                                        <Col span={7}>
                                            <div
                                                style={{
                                                    fontWeight: "bold",
                                                    padding: "6px 12px",
                                                    // color: colors.blackChart,
                                                }}
                                            >
                                                {item.name}
                                            </div>
                                        </Col>
                                        <Col span={15}>
                                            <div
                                                style={{
                                                    fontWeight: "bold",
                                                    padding: "6px 12px",
                                                    // color: colors.blackChart,
                                                }}
                                            >
                                                {item.description}
                                            </div>
                                        </Col>
                                        {index !== (listTargetKpi.listTarget.length - 1) && (
                                            <div style={{
                                                position: 'absolute',
                                                background: 'rgba(122, 123, 123, 0.3)',
                                                width: '95%',
                                                transform: ' translate(-50%, -50%)',
                                                height: 1,
                                                bottom: 0,
                                                left: '50%',
                                                right: '50%',
                                            }}></div>
                                        )}
                                    </List.Item>
                                </Row>
                            )}
                        />
                    </div>
                    <Col span={24} style={{display: 'flex', justifyContent: 'flex-end', marginTop: 20}}>
                        <IButton
                            title="Thêm"
                            color={colors.main}
                            icon={ISvg.NAME.BUTTONRIGHT}
                            styleHeight={{
                                width: 140,
                            }}
                            onClick={() => {
                                let dataAction = {
                                    keyRoot: "listTargetKpi",
                                    key: "listTargetShow",
                                    value: listTargetKpi.listTargetClone,
                                }
                                dispatch(ADD_LIST_SHOW_CHOOSE(dataAction))
                            }}
                        />
                    </Col>
                </Row>

                </Col>
                <Col span={11}>
                    <Row>
                        <Col span={24}>
                            <Row
                                style={{
                                    border: "1px solid rgba(122, 123, 123, 0.5)",
                                    backgroundColor: "#E4EFFF",
                                    paddingTop: 5,
                                    paddingBottom: 3,
                                    display: "flex",
                                    alignItems: "center",
                                    paddingRight: 0,
                                    fontWeight: 'bold'
                                }}
                            >
                                <Col span={3}>
                                    <div style={{
                                        fontWeight: "bold",
                                        padding: "6px 12px",
                                        color: colors.blackChart,
                                    }}>STT
                                    </div>
                                </Col>
                                <Col span={12}>
                                    <div style={{
                                        fontWeight: "bold",
                                        padding: "6px 12px",
                                        color: colors.blackChart,
                                    }}>Tên chỉ tiêu
                                    </div>
                                </Col>
                                <Col span={5}>
                                    <div
                                        style={{
                                            textAlign: "center", fontWeight: "bold",
                                            padding: "6px 12px",
                                            color: colors.blackChart,
                                        }}
                                    >
                                        Chỉ tiêu
                                    </div>
                                </Col>
                                <Col span={4}></Col>
                            </Row>
                        </Col>
                    </Row>
                    <div style={{
                        borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                        borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                        borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                        height: 400,
                        overflowY: 'scroll'
                    }}>
                        <List
                            dataSource={listTargetKpi.listTargetShow}
                            bordered={false}
                            renderItem={(item, index) => (
                                <Row key={index}>
                                    <List.Item>
                                        <Col span={3} style={{
                                            paddingTop: 10,
                                            paddingBottom: 10,
                                            display: "flex",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            // borderRight: ' 1px solid rgba(122, 123, 123, 0.3)'
                                        }}>
                                            {index + 1}
                                        </Col>
                                        <Col span={12}>
                                            <div
                                                style={{
                                                    fontWeight: "bold",
                                                    padding: "6px 12px",
                                                    // color: colors.blackChart,
                                                }}
                                            >
                                                {item.name}
                                            </div>
                                        </Col>
                                        <Col span={5}>
                                            <div style={{textAlign: "center", width: "100%"}}>
                                                <StyledInputNumberNew
                                                    value={
                                                        !listTargetKpi.countTagrgetShow[item.id] ||
                                                        !listTargetKpi.countTagrgetShow[item.id].quantity
                                                            ? 0
                                                            : listTargetKpi.countTagrgetShow[item.id].quantity
                                                    }
                                                    style={{width: "100%", height: 40}}
                                                    onChange={(value) => {
                                                        const dataAction = {
                                                            keyRoot: "listTargetKpi",
                                                            countTagrgetShow: "countTagrgetShow",
                                                            countTagrgetApprove: "countTagrgetApprove",
                                                            idProduct: item.id,
                                                            valueUpdate: {quantity: value},
                                                        };
                                                        dispatch(EDIT_COUNT(dataAction))
                                                    }}
                                                    min={0}
                                                    formatter={(value) =>
                                                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                                                    }
                                                    parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                                                />
                                            </div>
                                        </Col>
                                        <Col span={4}>
                                            <div
                                                className="cursor"
                                                style={{
                                                    margin: 'auto',
                                                    background: "rgba(227, 95, 75, 0.1)",
                                                    width: 20,
                                                    height: 20,
                                                    borderRadius: 10,
                                                    display: "flex",
                                                    justifyContent: "center",
                                                    alignItems: "center",
                                                    cursor: "pointer",
                                                }}
                                                onClick={() => {
                                                    let dataAction = {
                                                        keyRoot: "listTargetKpi",
                                                        key: 'listTargetShow',
                                                        keyClone: 'listTargetClone',
                                                        id: item.id,
                                                    }
                                                    dispatch(REMOVE_ITEM_SHOW(dataAction))
                                                }}
                                            >
                                                <ISvg
                                                    name={ISvg.NAME.CROSS}
                                                    width={8}
                                                    height={8}
                                                    fill={colors.oranges}
                                                />
                                            </div>
                                        </Col>
                                        {index !== (listTargetKpi.listTarget.length - 1) && (
                                            <div style={{
                                                position: 'absolute',
                                                background: 'rgba(122, 123, 123, 0.3)',
                                                width: '95%',
                                                transform: ' translate(-50%, -50%)',
                                                height: 1,
                                                bottom: 0,
                                                left: '50%',
                                                right: '50%',
                                            }}></div>
                                        )}
                                    </List.Item>
                                </Row>
                            )}
                        />
                    </div>


                </Col>

                <Col span={24}>
                    <div
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            position: "absolute",
                            top: 45,
                            right: -14,
                        }}
                    >
                        <IButton
                            title="Lưu"
                            color={colors.main}
                            icon={ISvg.NAME.SAVE}
                            style={{
                                marginRight: 15,
                            }}
                            onClick={() => {
                                let dataAction = {
                                    keyRoot: 'listTargetKpi',
                                    key: 'listTargetApprove',
                                    value: listTargetKpi.listTargetShow
                                }

                                dispatch(SAVE_LIST_APPROVE(dataAction))
                                callbackClose();
                            }}
                        />
                        <IButton
                            title="Hủy bỏ"
                            color={colors.oranges}
                            icon={ISvg.NAME.CROSS}
                            onClick={() => {
                                let dataAction = {
                                    keyRoot: 'listTargetKpi',
                                    keyShow: 'listTargetShow',
                                    keyTargetClone: 'listTargetClone',
                                    value: listTargetKpi.listTargetApprove
                                }
                                dispatch(CANCEL_LIST_KPI_MODAL(dataAction))
                                callbackClose();
                            }}
                        />
                    </div>
                </Col>
            </Row>

        </div>
    )
}

export default ModalKpi
