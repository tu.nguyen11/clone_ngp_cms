import React, {useEffect, useState} from 'react'
import {Col, Empty, Row, Tooltip, Skeleton} from "antd";
import {StyledITitle} from "../../components/common/Font/font";
import {colors} from "../../../assets";
import {IButton, ISvg, ITable, ITableHtml} from "../../components";
import {priceFormat} from "../../../utils";
import styled from "styled-components";
import {useHistory, useParams} from 'react-router-dom'
import APIService from "../../../services/APIService";
import {ASSIGNED_DATA_TARGET_KPI_EDIT} from "../../store/reducers";
import {useDispatch} from "react-redux";

function DetailKpi() {
    const dispatch = useDispatch()
    const history = useHistory()
    const {id} = useParams();
    const columns = [
        {
            title: "Tên các chỉ tiêu",
        },
        {
            title: "Mô tả",
        },
        {
            title: "Chỉ tiêu",
            align: "center",
        },
        {
            title: "Thực tế",
            align: "center",
        },
    ];
    const [dataKpi, setDataKpi] = useState({
        target_quantity: "",
        agency_name: "",
        month: '',
        year: '',
        status_name: "",
        detail: {},
        targets: []
    })
    const [isLoading, setIsLoading] = useState(true);
    const [editButton, setEditButton] = useState(false)
    const getDetailkpi = async (id) => {
        try {
            const dataResponse = await APIService._getDetailKpiManagement(id);
            setDataKpi({
                ...dataKpi,
                target_quantity: dataResponse.target_quantity,
                agency_name: dataResponse.agency_name,
                month: dataResponse.month,
                year: dataResponse.year,
                status_name: dataResponse.status_name,
                detail: dataResponse.detail,
                targets: dataResponse.targets,
            })
            if (dataResponse.detail.status == -1) {
                setEditButton(true)
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.log(error);
        }
    };
    useEffect(() => {
        getDetailkpi(id)
    }, [])
    const headerTableProduct = (headerTable) => {
        if (!editButton) {
            return (
                <tr className="scroll" style={{background: "#E4EFFF"}}>
                    {headerTable.map((item, index) => (
                        <th className="th-table" style={{textAlign: item.align}}>
                            {item.title}
                        </th>
                    ))}
                </tr>
            );
        } else {
            headerTable.pop()
            return (
                <tr className="scroll" style={{background: "#E4EFFF"}}>
                    {headerTable.map((item, index) => (

                        <th className="th-table" style={{textAlign: item.align}}>
                            {item.title}
                        </th>
                    ))}
                </tr>
            );
        }
    };
    const bodyTableProduct = (contentTable) => {
        return contentTable.length === 0 ? (
            <tr
                height="100px"
                style={{
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                }}
            >
                <td colspan="7" style={{padding: 20}}>
                    <Empty description="Không có dữ liệu"/>
                </td>
            </tr>
        ) : (
            contentTable.map((item, index) => {
                return (
                    <tr key={index} className="tr-table scroll">
                        <td
                            className="td-table"
                            style={{fontWeight: "normal"}}
                        >
                            {item.name}
                        </td>
                        <td
                            className="td-table"
                            style={{textAlign: "left", fontWeight: "normal"}}
                        >
                            <div style={{maxWidth: 400}}>
                                {item.description}
                            </div>
                        </td>
                        <td
                            className="td-table"
                            style={{
                                textAlign: "center",
                                fontWeight: "normal",
                            }}
                        >
                            {item.target}

                        </td>
                        {!editButton && (
                            <td
                                className="td-table"
                                style={{
                                    textAlign: "center",
                                    fontWeight: "normal",
                                }}
                            >
                                {item.thuc_te}
                            </td>
                        )}
                    </tr>
                )
            })
        );
    };
    return (
        <div style={{width: "100%"}}>
            <Row>
                <Col span={24}>
                    <Row>
                        <Col span={12}>
                            <StyledITitle style={{color: colors.main, fontSize: 22}}>
                                Chi tiết chỉ tiêu
                            </StyledITitle>
                        </Col>
                    </Row>
                </Col>
            </Row>

            <Row style={{marginBottom: 40}}>
                <Col style={{display: 'flex', justifyContent: 'flex-end'}} span={24}>
                    {editButton && (
                        <IButton
                            color={colors.main}
                            icon={ISvg.NAME.WRITE}
                            title={'Chỉnh sửa'}
                            onClick={() => {
                                const dataAction = {
                                    keyRoot: 'listTargetKpi',
                                    keyApprove: 'listTargetApprove',
                                    keyShow: 'listTargetShow',
                                    keyTargetClone: 'listTargetClone',
                                    keyCountTagrgetShow: 'countTagrgetShow',
                                    keyCountTagrgetApprove: 'countTagrgetApprove',
                                    value: dataKpi.targets,
                                };
                                dispatch(ASSIGNED_DATA_TARGET_KPI_EDIT(dataAction));
                                history.push('/kpimanagement/edit/' + id)
                            }}
                        />
                    )}
                </Col>
            </Row>

            <Row type='flex' gutter={[16, 16]}>
                <Col span={8}>
                    <div style={{
                        padding: '30px 40px',
                        backgroundColor: "white",
                        height: '100%'
                    }}>
                        <Skeleton paragraph={{rows: 6}} active loading={isLoading}>
                            <Row>
                                <Col style={{
                                    paddingBottom: 25,
                                }} span={24}>
                                    <StyledITitle style={{fontSize: 16}}>
                                        Thông tin chỉ tiêu
                                    </StyledITitle>
                                </Col>
                                <Col style={{
                                    paddingBottom: 20,
                                }} span={24}>
                                    <div>
                                        <p style={{fontWeight: 500}}>Người tạo</p>
                                        <span
                                            style={{
                                                wordWrap: "break-word",
                                                wordBreak: "break-all",
                                            }}
                                        >
                                            {dataKpi.detail.creator}
                                        </span>
                                    </div>
                                </Col>

                                <Col style={{
                                    paddingBottom: 20,
                                }} span={12}>
                                    <div>
                                        <p style={{fontWeight: 500}}>Thời gian (*)</p>
                                        <span
                                            style={{
                                                wordWrap: "break-word",
                                                wordBreak: "break-all",
                                            }}
                                        >
                                            {dataKpi.month + '/' + dataKpi.year}
                                        </span>
                                    </div>
                                </Col>

                                <Col style={{
                                    paddingBottom: 20,
                                }} span={12}>
                                    <div>
                                        <p style={{fontWeight: 500}}>Trạng thái (*)</p>
                                        <span
                                            style={{
                                                wordWrap: "break-word",
                                                wordBreak: "break-all",
                                            }}
                                        >
                                            {dataKpi.status_name}
                                        </span>
                                    </div>
                                </Col>
                                <Col style={{
                                    paddingBottom: 20,
                                }} span={24}>
                                    <div>
                                        <p style={{fontWeight: 500}}>Showroom (*)</p>
                                        <span
                                            style={{
                                                wordWrap: "break-word",
                                                wordBreak: "break-all",
                                            }}
                                        >
                                            {dataKpi.agency_name}
                                        </span>
                                    </div>
                                </Col>
                                <Col style={{
                                    paddingBottom: 20,
                                }} span={24}>
                                    <div>
                                        <p style={{fontWeight: 500}}>Ghi Chú</p>
                                        <span
                                            style={{
                                                wordWrap: "break-word",
                                                wordBreak: "break-all",
                                            }}
                                        >
                                            {dataKpi.detail.note}
                                        </span>
                                    </div>
                                </Col>
                            </Row>
                        </Skeleton>
                    </div>

                </Col>
                <Col span={14}>
                    <div style={{
                        padding: '30px 40px',
                        backgroundColor: "white",
                        height: '100%'
                    }}>
                        <Row>
                            <Col style={{
                                paddingBottom: 25,
                            }} span={12}>
                                <StyledITitle style={{fontSize: 16}}>
                                    Thông tin chỉ tiêu: {dataKpi.target_quantity}
                                </StyledITitle>
                            </Col>
                        </Row>
                        <Row>
                            <ITableHtml
                                childrenBody={bodyTableProduct(dataKpi.targets)}
                                childrenHeader={headerTableProduct(columns)}
                            />

                        </Row>
                    </div>
                </Col>
            </Row>
        </div>


    )
}


export default DetailKpi;
