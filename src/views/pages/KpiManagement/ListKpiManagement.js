import React, {useEffect, useState} from 'react'
import {Col, Row, Tooltip, DatePicker, message} from "antd";
import {IButton, IDatePicker, IMonthPicker, ISearch, ISelect, ISvg, ITable, ITitle} from "../../components";
import {StyledITitle} from "../../components/common/Font/font";
import {colors, images} from "../../../assets";
// import moment from 'moment';
import moment from 'moment'
import {APIService} from "../../../services";
import {useHistory} from 'react-router-dom'
import FormatterDay from "../../../utils/FormatterDay";
import {CANCEL_LIST_KPI_MODAL_ALL} from "../../store/reducers";
import {useDispatch} from "react-redux";

export default function ListKpi() {
    const monthFormat = 'MM/YYYY';
    const history = useHistory();
    const [listShowRoom, setListShowRoom] = useState([])
    const [listStatus, setListStatus] = useState([])
    const [dataTable, setDataTable] = useState({
        list: [],
        size: 15,
        total: 0,
    })
    const dispatch = useDispatch()
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const [loadingTable, setLoadingTable] = useState(true)
    const [loadingButton, setLoadingButton] = useState(false)
    const [filter, setFilter] = useState({
        month: 0,
        year: 0,
        agency_id: 0,
        status: 0,
        key: "",
        page: 1,
    });
    const columns = [
        {
            title: "STT",
            dataIndex: "stt",
            width: 80,
            key: "stt",
            align: "center",
        },
        {
            title: "Thời gian",
            dataIndex: "period",
            key: "period",
            render: (period) => <span>{!period ? "-" : period}</span>,
        },
        {
            title: "Showroom",
            dataIndex: "showroom",
            key: "showroom",
            render: (showroom) => (
                <Tooltip title={showroom}>
                    <span>{!showroom ? "-" : showroom}</span>
                </Tooltip>
            ),
        },
        {
            title: "Số lượng chỉ tiêu",
            dataIndex: "quantity_targets",
            key: "quantity_targets",
            render: (quantity_targets) => <span>{!quantity_targets ? "-" : quantity_targets}</span>,
            align: "center",
        },
        {
            title: "Trạng thái",
            dataIndex: "status",
            key: "status",
            render: (status) => <span>{!status ? "-" : status}</span>,
        },

        {
            title: "Ngày tạo",
            dataIndex: "ngay_tao",
            key: "ngay_tao",
            render: (ngay_tao) => (
                <span>
                      {!ngay_tao || ngay_tao <= 0
                          ? "-"
                          : FormatterDay.dateFormatWithString(
                              ngay_tao,
                              "#DD#/#MM#/#YYYY# #hhh#:#mm#"
                          )}
                    </span>
            )
        },
        {
            title: "Người tạo",
            dataIndex: "creator",
            key: "creator",
            render: (creator) => <span>{!creator ? "-" : creator}</span>,
        },
        {
            title: "Người cập nhật",
            dataIndex: "updater",
            key: "updater",
            render: (updater) => <span>{!updater ? "-" : updater}</span>,
        },
        {
            title: "Ngày cập nhật",
            dataIndex: "ngay_cap_nhat",
            key: "ngay_cap_nhat",
            render: (ngay_cap_nhat) => (
                <span>
                      {!ngay_cap_nhat || ngay_cap_nhat <= 0
                          ? "-"
                          : FormatterDay.dateFormatWithString(
                              ngay_cap_nhat,
                              "#DD#/#MM#/#YYYY# #hhh#:#mm#"
                          )}
                    </span>
            )
        },
    ];
    const getListShowroomDropdown = async () => {
        try {
            const dataResponse = await APIService._getListShowroomKpiManagement()
            let arrNew = dataResponse.showrooms.map((item, index) => {
                const key = item.id;
                const value = item.name;
                return {key, value};
            });

            arrNew.unshift({
                key:0,
                value:"Tất Cả"
            })
            setListShowRoom(arrNew)

        } catch (err) {
            console.log(err)
        }
    }

    const getListStatusDropdown = async () => {
        try {
            const dataResponse = await APIService._getListStatusKpiManagement()
            let arrNew = dataResponse.status.map((item, index) => {
                const key = item.id;
                const value = item.name;
                return {key, value};
            });
            setListStatus(arrNew)

        } catch (err) {
            console.log(err)
        }
    }
    const getApiListKpi = async (obj) => {
        try {
            const data = await APIService._getListKpiManagement(obj)
            data.list.map((item, index) => {
                item.stt = (filter.page - 1) * 10 + index + 1;
                item.rowTable = JSON.stringify({
                    id: item.id,
                });
                // return item
            });
            setLoadingTable(false);
            setDataTable(data);
        } catch (err) {
            setLoadingTable(false);
        }
    }
    const approveKpi = async (obj) => {
        try {
            const response = await APIService._postKpiApproves(obj)
            setLoadingButton(false)
            message.success('Duyệt chỉ tiêu thành công')
            setSelectedRowKeys([])
            await getApiListKpi(filter)
        } catch (err) {
            setLoadingButton(false)
        }
    }
    useEffect(() => {
        getListShowroomDropdown()
        getListStatusDropdown()
    }, [])
    useEffect(() => {
        getApiListKpi(filter)
    }, [filter])
    const rowSelection = {
        selectedRowKeys,
        onChange: (selectedRowKeys) => {
            setSelectedRowKeys(selectedRowKeys);
        },
        getCheckboxProps: (record) => {
            const rowIndex = dataTable.list.find(item1 => {
                return item1.id == record.id && item1.status !== 'Chờ chạy'
            })
            return {
                disabled: rowIndex
            };
        },
    };
    return (
        <div>
            <Row>
                <Col span={24}>
                    <Row>
                        <Col span={12}>
                            <StyledITitle style={{color: colors.main, fontSize: 22}}>
                                Danh sách chỉ tiêu
                            </StyledITitle>
                        </Col>
                        <Col span={12}>
                            <div className="flex justify-end">
                                <Tooltip title="Tìm kiếm tên người cập nhật">
                                    <ISearch
                                        className="cursor"
                                        placeholder="Tìm kiếm tên người cập nhật"
                                        icon={
                                            <div
                                                style={{
                                                    display: "flex",
                                                    width: 42,
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                }}
                                            >
                                                <img
                                                    src={images.icSearch}
                                                    style={{width: 16, height: 16}}
                                                    alt=""
                                                />
                                            </div>
                                        }
                                        onPressEnter={(e) => {
                                            setLoadingTable(true);
                                            setFilter({
                                                ...filter,
                                                key: e.target.value,
                                                page: 1,
                                            });
                                        }}
                                    />
                                </Tooltip>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <div style={{margin: "32px 0px"}}>
                <Row>
                    <Col span={16}>
                        <Row>
                            <Col>
                                <div
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                    }}
                                >
                                    <ISvg
                                        name={ISvg.NAME.EXPERIMENT}
                                        with={16}
                                        height={16}
                                        fill={colors.icon.default}
                                    />
                                    <ITitle
                                        title="Theo tháng"
                                        level={4}
                                        style={{
                                            marginLeft: 15,
                                            marginRight: 15,
                                            width: 100,
                                        }}
                                    />
                                    <div>
                                        <IMonthPicker
                                            isBackground
                                            from={moment(filter.year + '/' + filter.month, monthFormat)}
                                            format={monthFormat}
                                            onChange={(date) => {
                                                if (!date) {
                                                    setFilter({
                                                        ...filter,
                                                        month: 0,
                                                        year: 0
                                                    })
                                                } else {
                                                    setFilter({
                                                        ...filter,
                                                        month: new Date(date._d).getMonth() + 1,
                                                        year: new Date(date._d).getFullYear()
                                                    })
                                                }
                                                setLoadingTable(true);
                                            }}
                                        />

                                    </div>

                                    <div style={{width: 200, marginLeft: 15, marginRight: 15}}>
                                        <ISelect
                                            data={listShowRoom}
                                            defaultValue="Tất cả"
                                            select={true}
                                            onChange={(key) => {
                                                setLoadingTable(true);
                                                setFilter({
                                                    ...filter,
                                                    agency_id: key,
                                                    page: 1,
                                                });
                                            }}
                                        />
                                    </div>
                                    <div style={{width: 200, marginLeft: 15, marginRight: 15}}>
                                        <ISelect
                                            data={listStatus}
                                            defaultValue="Tất cả"
                                            select={true}
                                            onChange={(key) => {
                                                setLoadingTable(true);
                                                setFilter({
                                                    ...filter,
                                                    status: key,
                                                    page: 1,
                                                });
                                            }}
                                        />
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={8}>
                        <div className="flex justify-end">
                            <IButton
                                icon={ISvg.NAME.ARROWUP}
                                color={colors.main}
                                loading={loadingButton}
                                title="Duyệt"
                                style={{marginRight: 10}}
                                onClick={() => {
                                    if (selectedRowKeys.length === 0) {
                                        message.error("Vui lòng chọn chỉ tiêu");
                                        return;
                                    }
                                    const arrId = selectedRowKeys.map((item) => {
                                        const itemParse = JSON.parse(item);
                                        return itemParse.id;
                                    });
                                    const preRun = arrId.filter(item => {
                                        return dataTable.list.find(item1 => {
                                            return item1.id == item && item1.status !== 'Chờ chạy'
                                        })
                                    })
                                    if (preRun.length !== 0) {
                                        message.error("Bạn chỉ duyệt được chỉ tiêu chờ chạy!");
                                        return;
                                    }
                                    const objRemove = {
                                        ids: arrId,
                                    };
                                    setLoadingButton(true)
                                    approveKpi(objRemove);
                                }}
                            />
                            <IButton
                                icon={ISvg.NAME.ADD}
                                color={colors.main}
                                title="Tạo mới"
                                onClick={() => {
                                    dispatch(CANCEL_LIST_KPI_MODAL_ALL())
                                    history.push('/kpimanagement/create/0')
                                }}
                            />

                        </div>
                    </Col>
                </Row>
            </div>
            <Row>
                <Col span={24}>
                    <Row>
                        <ITable
                            columns={columns}
                            data={dataTable.list}
                            style={{width: "100%"}}
                            defaultCurrent={1}
                            sizeItem={dataTable.size}
                            indexPage={filter.page}
                            size="small"
                            rowKey="rowTable"
                            maxpage={dataTable.total}
                            loading={loadingTable}
                            scroll={{x: dataTable.list.length === 0 ? 0 : 1600}}
                            onRow={(record, rowIndex) => {
                                return {
                                    onClick: () => {
                                        history.push('/kpimanagement/detail/' + record.id);
                                    },
                                };
                            }}
                            rowSelection={rowSelection}
                            onChangePage={(page) => {
                                setLoadingTable(true);
                                setFilter({...filter, page: page});
                            }}
                        > </ITable>
                    </Row>
                </Col>
            </Row>

        </div>


    )
}
