import {Col, message, Row, Form, Input, Skeleton} from "antd";
import React, {useEffect, useRef, useState} from "react";
import {useHistory, useParams} from "react-router";
import {colors} from "../../../assets";
import {APIService} from "../../../services";
import {StyledITileHeading} from "../../components/common/Font/font";
import {
  ITitle,
  ISvg,
  ISelect,
  IButton,
  IUpload,
  IInput,
} from "../../components";
import {
  IInputText,
  IFormItem,
} from "../../components/common";
import {ImageType} from "../../../constant";
import styled from "styled-components";

const InputPassword = styled(Input.Password)`
.ant-input{
        height: 42px;
    width: 100%;
    border: unset;
    border-radius: 0px !important;
    padding: 0px !important;
    color: black
}
                                                
`

function CreateSalesman(props) {
  const history = useHistory();
  const {id, type} = useParams();
  const isEdit = type === "edit";
  const reduxForm = props.form;
  const {getFieldDecorator} = reduxForm;
  const [listSalesManType, setListSalesManType] = useState([]);
  const [listShowroom, setListShowroom] = useState([]);
  const [listManager, setListManager] = useState([]);
  const [dataDetail, setDataDetail] = useState({url: ""});
  const [datashowroom, setDataShowroom] = useState([])
  const [isManager, setIsManager] = useState(false);
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [isLoadingManager, setIsLoadingManager] = useState(false);
  const [listBusiness, setLstBusiness] = useState([])
  const [isEditing, setIsEditing] = useState(true)
  const [saleManType, setSaleManType] = useState(null)
  console.log(saleManType)
  const ref = useRef()
  const _fetchListShowroom = async () => {
    try {
      const response = await APIService._getListShowroom()
      let newArr = response.data.map(item => {
        return {
          key: item.id,
          value: item.name
        }
      })
      setDataShowroom(newArr)
    } catch (e) {
      throw e
    }
  }
  const _fetchLstBusinessChanel = async () => {
    try {
      const res = await APIService._getLstBussinesChanel()
      let newArr = res.data.map(item => {
        return {
          key: item.id,
          value: item.name
        }
      })
      setLstBusiness(newArr)
    } catch (e) {
      console.log(e)
    }
  }
  const getListSalesManType = async () => {
    try {
      const data = await APIService._getListSalesManType();
      let dataNew = data.data.map((item) => {
        let checked = item.id === 1;
        return {...item, checked};
      });
      let dataSaleManTypeUsed = dataNew.filter((item) => {
        return item.id !== 2;
      });
      setListSalesManType([...dataSaleManTypeUsed]);
    } catch (error) {
      console.log(error);
    }
  };

  const getListShowroom = async () => {
    try {
      const data = await APIService._getListShowroom();
      let dataNew = data.data.map((item) => {
        return {key: item.id, value: item.name};
      });
      setListShowroom([...dataNew]);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };
  const getListManager = async (showroom_id, channel_id) => {
    try {
      if (typeof showroom_id == 'number') showroom_id = [showroom_id]
      if (typeof channel_id == 'number') channel_id = [channel_id]
      if (isManager) return
      if (!showroom_id || !channel_id) return
      const data = await APIService._getListManagerWithBussinessChanel(showroom_id, channel_id);
      let dataNew = data.list.map((item) => {
        return {key: item.id, value: item.name};
      });
      // if (isEdit) {
      let indx = dataNew.findIndex(item => item.key === reduxForm.getFieldValue('_manager'))
      if (indx === -1) {
        reduxForm.setFieldsValue({
          _manager: dataNew.length !== 0 ? dataNew[0].key : undefined
        })
      }
      // }
      setListManager([...dataNew]);
      setIsLoadingManager(false);
    } catch (error) {
      console.log(error);
      setIsLoadingManager(false);
    }
  };

  const _apiGetDetail = async (id) => {
    console.log(id)
    try {
      const data = await APIService._getDetailMember(id);
      const objData = data.saleman;

      reduxForm.setFieldsValue({
        _img: objData.avatar,
        _name: objData.sale_name,
        _gender: objData.gender,
        _phone: objData.sale_phone,
        _email: objData.sale_email,
        _cmnd: objData.identity_card_number,
        _address: objData.sale_address,
        _code: objData.sale_code,
        _password: objData.password,
        _status: objData.status,
        _note: objData.note,
        _contract_code: objData.id_contract,
        _contract_start_date: objData.create_contract_date,
        _contract_end_date: objData.end_contract_date,
        _manager: objData.manager_id,
        _showroom: objData.sale_man_type !== 1 ? objData.list_showroom.map(item => item.id) : objData.showrom_id,
        _list_business: Array.isArray(objData.list_business) ? objData.list_business.map(item => item.id) : undefined
      });
      const dataSalesManType = await APIService._getListSalesManType();
      let listSalesManTypeTmp = dataSalesManType.data.map((item) => {
        let checked = item.id === 1;
        return {...item, checked};
      });
      setSaleManType(objData.sale_man_type)
      // let listSaleManagerUsed = listSalesManTypeTmp.filter(
      //   (item) => item.id !== 2
      // );
      listSalesManTypeTmp.map((type) => {
        if (type.id === objData.sale_man_type) {
          if (objData.sale_man_type !== 1) {
            setIsManager(true);
            if (objData.saleman_of_manager > 0) {
              setIsEditing(false)
            }
          } else {
            if (objData.status_route > 0) {
              setIsEditing(false)
            }
          }
          type.checked = true;
        } else {
          type.checked = false;
        }
      });
      if (objData.sale_man_type === 1 && objData.list_business.length !== 0) {
        await getListManager(objData.showrom_id, objData.list_business[0].id);
      }


      setListSalesManType([...listSalesManTypeTmp]);
      setDataDetail({...objData, url: data.url});


      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };
  const fetchUpdateSalesMan = async (obj) => {
    try {
      const data = await APIService._postUpdateSalesman(obj);
      setLoadingCreate(false);
      message.success("Cập nhập nhân viên thành công");
      history.goBack();
    } catch (error) {
      console.log(error);
      setLoadingCreate(false);
    }
  };

  const fetchAddSalesMan = async (obj) => {
    try {
      const data = await APIService._postAddSalesman(obj);
      setLoadingCreate(false);
      message.success("Tạo nhân viên thành công");
      history.push("/salesman/list");
    } catch (error) {
      console.log(error);
      setLoadingCreate(false);
    }
  };

  const callAPI = async () => {
    if (!isEdit) {
      await getListSalesManType();
    }
    await _fetchLstBusinessChanel()
    await _fetchListShowroom()
    await getListShowroom();
  };

  useEffect(() => {
    callAPI();
    if (isEdit) {
      _apiGetDetail(id);
    }
  }, []);

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !reduxForm.getFieldValue("_contract_start_date")) {
      return false;
    }
    return (
      endValue.valueOf() <= reduxForm.getFieldValue("_contract_start_date")
    );
  };

  const onSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, obj) => {
      if (!err) {
        let sales_man_type = listSalesManType.find(
          (type) => type.checked === true
        );
        if (isEdit) {
          const objEdit = {
            id: Number(id),
            avatar: obj._img,
            cmnd: obj._cmnd,
            create_contract_date: obj._contract_start_date,
            end_contract_date: obj._contract_end_date,
            gender: obj._gender,
            id_contract: obj._contract_code,
            identity_card_number: obj._cmnd,
            manager_id: obj._manager,
            note: obj._note,
            password: obj._password,
            sale_address: obj._address,
            sale_code: obj._code,
            sale_email: obj._email,
            sale_man_type: sales_man_type.id,
            sale_name: obj._name,
            sale_phone: obj._phone,
            status: obj._status,
            list_business: typeof obj._list_business == "number" ? [obj._list_business] : obj._list_business
          };
          if (sales_man_type.id !== 1) {
            objEdit.list_showroom_id = typeof obj._showroom == "object" ? [...obj._showroom] : [obj._showroom]
          } else {
            objEdit.list_showroom_id = typeof obj._showroom == "object" ? [obj._showroom[0]] : [obj._showroom]
            objEdit.showrom_id = typeof obj._showroom == "object" ? obj._showroom[0] : obj._showroom
          }
          setLoadingCreate(true);
          fetchUpdateSalesMan(objEdit);
        } else {
          const objAdd = {
            avatar: obj._img,
            cmnd: obj._cmnd,
            create_contract_date: obj._contract_start_date,
            end_contract_date: obj._contract_end_date,
            gender: obj._gender,
            id_contract: obj._contract_code,
            identity_card_number: obj._cmnd,
            manager_id: sales_man_type.id === 3 ? 0 : obj._manager,
            note: obj._note,
            password: obj._password,
            sale_address: obj._address,
            sale_code: obj._code,
            sale_email: obj._email,
            sale_man_type: sales_man_type.id,
            sale_name: obj._name,
            sale_phone: obj._phone,
            list_business: typeof obj._list_business == "number" ? [obj._list_business] : obj._list_business,
            list_showroom_id: typeof obj._showroom == "object" ? [...obj._showroom] : [obj._showroom],
            status: obj._status,
          };
          // if (isManager && obj._showroom.length < 2) {
          //   message.error("Vui lòng chọn ít nhất 2 showroom quản lý")
          //   return
          // }
          setLoadingCreate(true);
          fetchAddSalesMan(objAdd);
        }
      }
    });
  };

  return (
    <div style={{width: "100%"}}>
      <Row>
        <Col span={24}>
          <div>
            <ITitle
              level={1}
              title={isEdit ? "Chỉnh sửa nhân viên" : "Tạo nhân viên mới"}
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </div>
        </Col>
      </Row>
      <div style={{margin: "32px 0px"}}>
        <Form onSubmit={onSubmit}>
          <Row>
            <Col span={24}>
              <div className="flex justify-end">
                <IButton
                  color={colors.main}
                  title="Lưu"
                  loading={loadingCreate}
                  icon={ISvg.NAME.SAVE}
                  style={{marginRight: 15}}
                  htmlType="submit"
                />

                <IButton
                  color={colors.oranges}
                  title="Hủy"
                  icon={ISvg.NAME.CROSS}
                  onClick={() =>
                    history.push(
                      isEdit ? `/salesman/detail/${id}` : `/salesman/list`
                    )
                  }
                />
              </div>
            </Col>
          </Row>

          <div
            style={{
              marginTop: 30,
              width: 950,
              background: "white",
              boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
            }}
          >
            <Row gutter={[0, 0]}>
              <Col span={16}>
                <div
                  style={{
                    padding: "30px 40px",
                    borderRight: "1px solid #D8DBDC",
                  }}
                >
                  <Skeleton active paragraph={{rows: 12}} loading={isLoading}>
                    <Row gutter={[0, 24]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="12px" maxFont="16px">
                          Thông tin cá nhân
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>
                                Tên nhân viên
                                <span style={{marginLeft: 5, color: "red"}}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("_name", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "vui lòng nhập tên nhân viên",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    placeholder="nhập tên nhân viên"
                                    value={reduxForm.getFieldValue("_name")}
                                  />
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>
                                Giới tính
                                <span
                                  style={{
                                    marginLeft: 5,
                                    color: "red",
                                  }}
                                >
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("_gender", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "vui lòng chọn giới tính",
                                    },
                                  ],
                                })(
                                  <ISelect
                                    className="clear-pad"
                                    placeholder="chọn giới tính"
                                    type="write"
                                    select={true}
                                    style={{
                                      background: colors.white,
                                      borderStyle: "solid",
                                      borderWidth: 0,
                                      borderColor: colors.line,
                                      borderBottomWidth: 1,
                                      marginTop: 1,
                                    }}
                                    isBackground={false}
                                    onChange={(key) => {
                                      reduxForm.setFieldsValue({
                                        _gender: key,
                                      });
                                    }}
                                    value={reduxForm.getFieldValue("_gender")}
                                    data={[
                                      {key: 1, value: "Nam"},
                                      {key: 0, value: "Nữ"},
                                    ]}
                                  />
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>
                                Điện thoại
                                <span
                                  style={{
                                    marginLeft: 5,
                                    color: "red",
                                  }}
                                >
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("_phone", {
                                  rules: [
                                    {
                                      required: true,
                                      pattern: new RegExp(/0[0-9]([0-9]{8})\b/),
                                      message:
                                        "nhập tối đa 10 số, không được nhập chữ, khoảng cách và ký tự đặc biệt",
                                    },
                                  ],
                                })(
                                  <IInput
                                    style={{
                                      marginTop: 1,
                                    }}
                                    loai="input"
                                    maxLength={10}
                                    onKeyPress={(e) => {
                                      console.log(e.target.value);
                                      var char = String.fromCharCode(e.which);
                                      if (!/[0-9]/.test(char)) {
                                        e.preventDefault();
                                      }
                                    }}
                                    placeholder="nhập số điện thoại"
                                    value={reduxForm.getFieldValue("_phone")}
                                  />
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>
                                Email
                                <span style={{marginLeft: 5, color: "red"}}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("_email", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "vui lòng nhập Email",
                                      type: "email",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    placeholder="nhập Email"
                                    value={reduxForm.getFieldValue("_email")}
                                  />
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>
                                CMND/CCCD
                                <span style={{marginLeft: 5, color: "red"}}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("_cmnd", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "vui lòng  nhập số CMND/CCCD",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    placeholder="số CMND/CCCD"
                                    value={reduxForm.getFieldValue("_cmnd")}
                                  />
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                          <Col span={12}>
                            <p style={{fontWeight: 500}}>
                              Địa chỉ
                              <span style={{marginLeft: 5, color: "red"}}>
                                *
                              </span>
                            </p>
                            <IFormItem>
                              {getFieldDecorator("_address", {
                                rules: [
                                  {
                                    required: true,
                                    message: "vui lòng nhập địa chỉ",
                                  },
                                ],
                              })(
                                <IInputText
                                  placeholder="nhập địa chỉ"
                                  value={reduxForm.getFieldValue("_address")}
                                />
                              )}
                            </IFormItem>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <div style={{marginTop: 20}}>
                          <StyledITileHeading minFont="12px" maxFont="16px">
                            Thông tin nhân viên
                          </StyledITileHeading>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Mã nhân viên
                                  <span style={{marginLeft: 5, color: "red"}}>
                                    *
                                  </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_code", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "vui lòng nhập mã nhân viên",
                                      },
                                    ],
                                  })(
                                    <IInputText
                                      disabled={isEdit}
                                      placeholder="nhập mã nhân viên"
                                      value={reduxForm.getFieldValue("_code")}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Mật khẩu
                                  {isEdit ? null : (
                                    <span
                                      style={{marginLeft: 5, color: "red"}}
                                    >
                                      *
                                    </span>
                                  )}
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_password", {
                                    rules: [
                                      {
                                        required: isEdit ? false : true,
                                        message: "vui lòng nhập mật khẩu",
                                      },
                                    ],
                                  })(
                                    <InputPassword
                                      placeholder="nhập mật khẩu"
                                      value={reduxForm.getFieldValue(
                                        "_password"
                                      )}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Trạng thái
                                  <span style={{marginLeft: 5, color: "red"}}>
                                    *
                                  </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_status", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "vui lòng chọn trạng thái",
                                      },
                                    ],
                                  })(
                                    <ISelect
                                      className="clear-pad"
                                      type="write"
                                      select={!(isEdit && saleManType === 3)}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      isBackground={false}
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          _status: key,
                                        });
                                      }}
                                      value={reduxForm.getFieldValue("_status")}
                                      placeholder="chọn trạng thái"
                                      data={[
                                        {key: 1, value: "Đang hoạt động"},
                                        {key: 0, value: "Ngưng hoạt động"},
                                      ]}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            {isManager ? null : (
                              <Col span={12}>
                                <div>
                                  <p style={{fontWeight: 500}}>
                                    Trưởng phòng phụ trách
                                    <span style={{marginLeft: 5, color: "red"}}>
                              *
                              </span>
                                  </p>
                                  <IFormItem>
                                    {getFieldDecorator("_manager", {
                                      rules: [
                                        {
                                          required: (!isManager && !isEditing),
                                          message:
                                            "vui lòng chọn trưởng phòng phụ trách",
                                        },
                                      ],
                                    })(
                                      <ISelect
                                        className="clear-pad"
                                        type="write"
                                        select={
                                          !(!reduxForm.getFieldValue("_showroom") ||
                                            isLoadingManager ||
                                            (isEdit && !isManager && !isEditing))
                                        }
                                        loading={isLoadingManager}
                                        style={{
                                          background: colors.white,
                                          borderStyle: "solid",
                                          borderWidth: 0,
                                          borderColor: colors.line,
                                          borderBottomWidth: 1,
                                          marginTop: 1,
                                        }}
                                        isBackground={false}
                                        onChange={(key) => {
                                          reduxForm.setFieldsValue({
                                            _manager: key,
                                          });
                                        }}
                                        value={reduxForm.getFieldValue(
                                          "_manager"
                                        )}
                                        placeholder="chọn trưởng phòng phụ trách"
                                        data={listManager}
                                      />
                                    )}
                                  </IFormItem>
                                </div>
                              </Col>
                            )}
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>

                            <Col span={24}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Showroom
                                  <span style={{marginLeft: 5, color: "red"}}>
                                    *
                                  </span>
                                </p>
                                <IFormItem style={{border: '1px solid #BCBDBD', padding: 10}}>
                                  {getFieldDecorator("_showroom", {
                                    rules: [
                                      {
                                        required: true,
                                        message:
                                          "vui lòng chọn showroom",
                                      },
                                    ],
                                  })(
                                    <ISelect
                                      isBackground={false}
                                      select={!(saleManType === 3 && isEdit && !isEditing)}
                                      className="clear-pad"
                                      type="write"
                                      mode={isManager ? 'multiple' : "default"}
                                      value={reduxForm.getFieldValue(
                                        "_showroom"
                                      )}
                                      placeholder="Vui lòng chọn"
                                      data={datashowroom}
                                      loading={isLoadingManager}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      onChange={(key) => {
                                        console.log(key)
                                        reduxForm.setFieldsValue({
                                          _showroom: key,
                                        });
                                        if ((Array.isArray(key) && key.length !== 0) || !isManager) {
                                          getListManager(key, reduxForm.getFieldValue('_list_business'))
                                        } else {
                                          reduxForm.setFieldsValue({
                                            _manager: undefined,
                                          });
                                          setListManager([])
                                        }
                                      }}
                                      defaultValue={reduxForm.getFieldValue('_showroom') || []}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>

                            <Col span={24}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Kênh kinh doanh
                                  <span style={{marginLeft: 5, color: "red"}}>
                                    *
                                  </span>
                                </p>
                                <IFormItem style={{border: '1px solid #BCBDBD', padding: 10}}>
                                  {getFieldDecorator("_list_business", {
                                    rules: [
                                      {
                                        required: true,
                                        message:
                                          "vui lòng chọn kênh kinh doanh",
                                      },
                                    ],
                                  })(
                                    <ISelect
                                      isBackground={false}
                                      select={!((saleManType === 3 && isEdit && !isEditing) || (isEdit & saleManType == 1))}
                                      className="clear-pad"
                                      type="write"
                                      mode={isManager ? 'multiple' : "default"}
                                      value={reduxForm.getFieldValue(
                                        "_list_business"
                                      )}
                                      placeholder="Vui lòng chọn"
                                      data={listBusiness}
                                      loading={isLoadingManager}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          _list_business: key,
                                        });
                                        if ((Array.isArray(key) && key.length !== 0) || !isManager) {
                                          getListManager(reduxForm.getFieldValue('_showroom'), key)
                                        } else {
                                          reduxForm.setFieldsValue({
                                            _manager: undefined,
                                          });
                                          setListManager([])
                                        }
                                      }}
                                      defaultValue={reduxForm.getFieldValue('_list_business') || []}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div style={{marginTop: 20}}>
                          <StyledITileHeading minFont="12px" maxFont="16px">
                            Thông tin vận hành
                          </StyledITileHeading>
                        </div>
                      </Col>
                      <Col span={24} style={{paddingTop: 8}}>
                        <Row gutter={[0, 12]}>
                          {listSalesManType.map((item, index) => {
                            return (
                              <Col span={24}>
                                <div
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    opacity: isEdit ? '0.5' : 'unset'
                                  }}
                                >
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      cursor: "pointer",
                                    }}
                                    onClick={() => {
                                      if (!isEdit) {
                                        let listSalesManTypeTmp = [
                                          ...listSalesManType,
                                        ];
                                        listSalesManTypeTmp.map(
                                          (type, idxType) => {
                                            if (idxType === index) {
                                              type.checked = true;
                                              if (type.id !== 1) {
                                                setIsManager(true);
                                                reduxForm.setFieldsValue({
                                                  _manager: undefined,
                                                });
                                                if (typeof reduxForm.getFieldValue('_showroom') !== 'object') {
                                                  if (reduxForm.getFieldValue('_showroom')) {
                                                    reduxForm.setFieldsValue({
                                                      _showroom: [reduxForm.getFieldValue('_showroom')],
                                                    });
                                                  }
                                                }
                                                if (typeof reduxForm.getFieldValue('_list_business') !== 'object') {
                                                  if (reduxForm.getFieldValue('_list_business')) {
                                                    reduxForm.setFieldsValue({
                                                      _list_business: [reduxForm.getFieldValue('_list_business')],
                                                    });
                                                  }
                                                }
                                              } else {
                                                setIsManager(false);
                                                reduxForm.setFieldsValue({
                                                  _showroom: listShowroom[0].key
                                                })
                                                reduxForm.setFieldsValue({
                                                  _list_business: listBusiness[0].key
                                                })
                                                console.log('Thông tin vận hành')
                                                getListManager(listShowroom[0].key, listBusiness[0].key)
                                              }
                                            } else {
                                              type.checked = false;
                                            }
                                          }
                                        );

                                        setListSalesManType(listSalesManTypeTmp);
                                      }
                                    }}
                                  >
                                    <div
                                      style={{
                                        width: 20,
                                        height: 20,
                                        border: item.checked
                                          ? "5px solid" + `${colors.main}`
                                          : "1px solid" + `${colors.line_2}`,
                                        borderRadius: 10,
                                      }}
                                    />
                                    <div style={{flex: 1, marginLeft: 10}}>
                                      <span
                                        style={{
                                          fontSize: 14,
                                          color: colors.text.black,
                                        }}
                                      >
                                    {item.name}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </Col>
                            );
                          })}
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
              <Col span={8}>
                <div style={{padding: "30px 40px"}}>
                  <Skeleton active paragraph={{rows: 12}} loading={isLoading}>
                    <Row gutter={[0, 20]}>
                      <Col span={24}>
                        <StyledITileHeading
                          style={{marginTop: 8}}
                          minFont="12px"
                          maxFont="16px"
                        >
                          Hình ảnh
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <IFormItem>
                          {getFieldDecorator("_img", {
                            rules: [
                              {
                                required: true,
                                message: "vui lòng chọn hình",
                              },
                            ],
                          })(
                            <>
                              {!isEdit ? (
                                <IUpload
                                  type={ImageType.SALEMAN}
                                  callback={(array) => {
                                    reduxForm.setFieldsValue({
                                      _img: array[0],
                                    });
                                  }}
                                />
                              ) : (
                                <IUpload
                                  type={ImageType.SALEMAN}
                                  callback={(array) => {
                                    reduxForm.setFieldsValue({
                                      _img: array[0],
                                    });
                                  }}
                                  name={reduxForm.getFieldValue("_img")}
                                  url={
                                    !reduxForm.getFieldValue("_img")
                                      ? null
                                      : dataDetail.url
                                  }
                                />
                              )}
                            </>
                          )}
                        </IFormItem>
                      </Col>
                      <Col span={24}>
                        <div>
                          <p style={{fontWeight: 500}}>Ghi chú</p>
                          <IFormItem>
                            {getFieldDecorator("_note", {
                              rules: [
                                {
                                  required: false,
                                  message: "vui lòng nhập ghi chú",
                                },
                              ],
                            })(
                              <IInput
                                style={{
                                  color: "#000",
                                  marginTop: 12,
                                  minHeight: 200,
                                  maxWidth: 180,
                                  maxHeight: 540,
                                }}
                                className="input-border"
                                loai="textArea"
                                placeholder="Nhập ghi chú"
                                onChange={(e) => {
                                }}
                                value={reduxForm.getFieldValue("_note")}
                              />
                            )}
                          </IFormItem>
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </Form>
      </div>
    </div>
  );
}

const createSalesman = Form.create({name: "CreateSalesman"})(CreateSalesman);

export default createSalesman;
