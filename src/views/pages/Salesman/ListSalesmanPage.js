import React, {useState, useEffect} from "react";
import {Row, Col, Popconfirm, Tooltip, message} from "antd";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IButton,
} from "../../components";
import {colors, images} from "../../../assets";
import {StyledITitle} from "../../components/common/Font/font";
import {APIService} from "../../../services";
import {useHistory} from "react-router-dom";
import {useSelector} from "react-redux";

function ListSalesmanPage(props) {
  const history = useHistory();

  const [filter, setFilter] = useState({
    type: 0,
    keyword: "",
    page: 1,
  });

  const [dataTable, setDataTable] = useState({
    listSalesMan: [],
    total: 1,
    size: 10,
    url: "",
  });

  const [listSalesManType, setListSalesManType] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const [loadingDropList, setLoadingDropList] = useState(true);
  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingRemove, setLoadingRemove] = useState(false);
  const {menu} = useSelector((state) => state)

  const rowSelection = {
    selectedRowKeys,
    onChange: (key) => {
      setSelectedRowKeys(key);
    },
    getCheckboxProps: (record) => {
      const rowIndex = dataTable.listSalesMan.find(item1 => {
        return item1.id === record.id && (item1.delete === 1 || item1.sale_type_id !== 1)
      })
      return {
        disabled: rowIndex
      };
    },
  };

  const getListSalesManType = async () => {
    try {
      const data = await APIService._getListSalesManType();
      let dataNew = data.data.map((item, index) => {
        return {key: item.id, value: item.name};
      });

      dataNew.unshift({key: 0, value: "Tất cả"});

      setListSalesManType([...dataNew]);
      setLoadingDropList(false);
    } catch (error) {
      console.log(error);
      setLoadingDropList(false);
    }
  };

  const getListSalesMan = async (filter) => {
    try {
      const data = await APIService._getListSalesMan(filter);
      data.list_saleman.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({id: item.id});
        return item;
      });

      setDataTable({
        listSalesMan: data.list_saleman,
        size: data.size,
        total: data.total,
        url: data.url,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const deleteMember = async (dataDelete) => {
    try {
      const data = await APIService._deleteNhanVienSale(dataDelete);
      setSelectedRowKeys([]);
      setLoadingRemove(false);
      message.success("Xóa nhân viên thành công");
      setLoadingTable(true);
      await getListSalesMan(filter);
    } catch (error) {
      console.log(error);
      setLoadingRemove(false);
    }
  };

  useEffect(() => {
    getListSalesManType();
  }, []);

  useEffect(() => {
    getListSalesMan(filter);
  }, [filter]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Mã nhân viên",
      dataIndex: "sale_code",
      key: "sale_code",
      render: (sale_code) => <span>{!sale_code ? "-" : sale_code}</span>,
    },
    {
      title: "Tên nhân viên",
      dataIndex: "sale_name",
      key: "sale_name",
      render: (sale_name) => <span>{!sale_name ? "-" : sale_name}</span>,
    },
    {
      title: "Số điện thoại",
      dataIndex: "sale_phone",
      key: "sale_phone",
      render: (sale_phone) => <span>{!sale_phone ? "-" : sale_phone}</span>,
    },
    {
      title: "Email",
      dataIndex: "sale_email",
      key: "sale_email",
      render: (sale_email) => <span>{!sale_email ? "-" : sale_email}</span>,
    },
    {
      title: "Loại nhân viên",
      dataIndex: "sale_type",
      key: "sale_type",
      align: "center",
      render: (sale_type) => <span>{!sale_type ? "-" : sale_type}</span>,
    },
    {
      title: "Hình ảnh",
      dataIndex: "sale_avatar",
      key: "sale_avatar",
      align: "center",
      render: (sale_avatar) => {
        return (
          <div>
            <img
              src={dataTable.url + sale_avatar}
              alt="sale_avatar"
              style={{
                width: 35,
                height: 35,
                borderRadius: 20,
              }}
            />
          </div>
        );
      },
    },
    {
      title: "Trưởng phòng phụ trách",
      dataIndex: "sale_manager",
      key: "sale_manager",
      render: (sale_manager) => (
        <span>{!sale_manager ? "-" : sale_manager}</span>
      ),
    },
    {
      title: "Kênh kinh doanh",
      dataIndex: "business_channel",
      key: "business_channel",
      render: (business_channel) => (
        <span>{!business_channel ? "-" : business_channel}</span>
      ),
    },
    {
      title: "Showroom",
      dataIndex: "showrom",
      key: "showrom",
      render: (showrom) => <Tooltip placement="leftTop" title={showrom}>
        <span>{!showrom ? "-" : showrom}</span>
      </Tooltip>
    },
  ];

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{color: colors.main, fontSize: 22}}>
                Danh sách nhân viên
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên, mã nhân viên">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên, mã nhân viên"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        keyword: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{width: 16, height: 16}}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{marginTop: 26}}>
            <Row gutter={[0, 25]}>
              <Col span={24}>
                <div className="flex justify-end item-center">
                  <ISvg
                    name={ISvg.NAME.LOCATION}
                    with={20}
                    height={20}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Loại nhân viên"
                    level={4}
                    style={{
                      marginLeft: 15,
                      marginRight: 15,
                    }}
                  />
                  <div style={{width: 200, marginRight: 15}}>
                    <ISelect
                      defaultValue="Tất cả"
                      data={listSalesManType}
                      loading={loadingDropList}
                      select={loadingDropList ? false : true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          type: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                      <IButton
                        title="Tạo mới"
                        color={colors.main}
                        icon={ISvg.NAME.ARROWUP}
                        styleHeight={{
                          width: 140,
                          marginRight: 15,
                        }}
                        isRight={true}
                        onClick={() => {
                          history.push(`/salesman/create/0`);
                        }}
                      />

                      {/*<IButton*/}
                      {/*  title="Xuất file"*/}
                      {/*  color={colors.main}*/}
                      {/*  icon={ISvg.NAME.DOWLOAND}*/}
                      {/*  styleHeight={{*/}
                      {/*    width: 140,*/}
                      {/*    marginRight: 15,*/}
                      {/*  }}*/}
                      {/*  isRight={true}*/}
                      {/*  onClick={() => {*/}
                      {/*    let query = APIService.queryParams(filter);*/}

                      {/*    let url = `${localStorage.getItem(*/}
                      {/*      "local_domain_server_NGP_ADMIN"*/}
                      {/*    )}adminservice/saleman/export_list_salesman?${query}`;*/}

                      {/*    window.open(url, "_blank");*/}
                      {/*  }}*/}
                      {/*/>*/}
                      <Popconfirm
                        placement="bottom"
                        title="Nhấn đồng ý để xóa"
                        okText="Đồng ý"
                        cancelText="Hủy"
                        onConfirm={() => {
                          if (selectedRowKeys.length === 0) {
                            message.error("Chưa chọn nhân viên để xóa");
                            return;
                          }

                          const arrId = selectedRowKeys.map((item) => {
                            let itemParse = JSON.parse(item);
                            return itemParse.id;
                          });

                          setLoadingRemove(true);
                          const objRemove = {
                            idSaleMan: arrId,
                          };
                          deleteMember(objRemove);
                        }}
                      >
                        <IButton
                          color={colors.oranges}
                          title="Xóa"
                          icon={ISvg.NAME.DELETE}
                          loading={loadingRemove}
                          styleHeight={{width: 140}}
                        />
                      </Popconfirm>
                </div>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listSalesMan}
                    style={{width: "100%"}}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    rowKey="rowTable"
                    scroll={{
                      x:
                        !dataTable.listSalesMan ||
                        dataTable.listSalesMan.length === 0
                          ? 0
                          : 1600,
                    }}
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    onRow={(record) => {
                      return {
                        onClick: () => {
                          history.push(`/salesman/detail/${record.id}`);
                        }, // click row
                        onMouseDown: (event) => {
                          if (event.button == 2 || event == 4) {
                            window.open(
                              `/salesman/detail/${record.id}`,
                              "_blank"
                            );
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({...filter, page: page});
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default ListSalesmanPage;
