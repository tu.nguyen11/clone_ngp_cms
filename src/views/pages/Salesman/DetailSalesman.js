import React, {useState, useEffect} from "react";
import {Row, Col, Skeleton, message, Popconfirm} from "antd";
import {ISvg, IButton} from "../../components";
import {colors, images} from "../../../assets";
import {StyledITitle} from "../../components/common/Font/font";
import {APIService} from "../../../services";
import {useHistory, useParams} from "react-router-dom";
import FormatterDay from "../../../utils/FormatterDay";
import {useSelector} from "react-redux";

function DetailSalesman(props) {
  const history = useHistory();
  const params = useParams();
  const {id} = params;
  const {menu} = useSelector((state) => state)

  const [dataDetail, setDataDetail] = useState({
    url: "",
  });

  const [isLoading, setIsLoading] = useState(true);
  const [loadingRemove, setLoadingRemove] = useState(false);
  const [listSalesManType, setListSalesManType] = useState([]);
  const _apiGetDetail = async (id) => {
    try {
      const data = await APIService._getDetailMember(id);
      setDataDetail({...data.saleman, url: data.url});
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };
  const getListSalesManType = async () => {
    try {
      const data = await APIService._getListSalesManType();
      setListSalesManType(data.data);
    } catch (error) {
      console.log(error);
    }
  };
  const deleteMember = async (dataDelete) => {
    try {
      const data = await APIService._deleteNhanVien(dataDelete);
      setLoadingRemove(false);
      message.success("Xóa Nhân viên thành công");
      history.push("/salesman/list");
    } catch (error) {
      console.log(error);
      setLoadingRemove(false);
    }
  };

  useEffect(() => {
    _apiGetDetail(id);
    getListSalesManType()
  }, []);

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row gutter={[0, 30]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{color: colors.main, fontSize: 22}}>
              Chi tiết nhân viên
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div className="flex justify-end">
              <IButton
                color={colors.main}
                title="Chỉnh sửa"
                icon={ISvg.NAME.WRITE}
                style={{width: 140, marginRight: 15}}
                onClick={() => {
                  history.push(`/salesman/edit/${id}`);
                }}
              />
            {/*<Popconfirm*/}
            {/*  placement="bottom"*/}
            {/*  title="Nhấn đồng ý để xóa"*/}
            {/*  okText="Đồng ý"*/}
            {/*  cancelText="Hủy"*/}
            {/*  onConfirm={() => {*/}
            {/*    setLoadingRemove(true);*/}
            {/*    const objRemove = {*/}
            {/*      saleman_id: [Number(id)],*/}
            {/*    };*/}
            {/*    deleteMember(objRemove);*/}
            {/*  }}*/}
            {/*>*/}
            {/*  <IButton*/}
            {/*    color={colors.oranges}*/}
            {/*    title="Xóa"*/}
            {/*    icon={ISvg.NAME.DELETE}*/}
            {/*    styleHeight={{ width: 140 }}*/}
            {/*    loading={loadingRemove}*/}
            {/*  />*/}
            {/*</Popconfirm>*/}
          </div>
        </Col>

        <Col span={24}>
          <div style={{width: 850}} className="shadow">
            <Row type="flex">
              <Col
                span={16}
                style={{
                  backgroundColor: "white",
                }}
              >
                <div style={{padding: "30px 40px"}}>
                  <Skeleton paragraph={{rows: 6}} active loading={isLoading}>
                    <Row gutter={[0, 20]}>
                      <Col span={24}>
                        <StyledITitle style={{color: "black", fontSize: 16}}>
                          Thông tin cá nhân
                        </StyledITitle>
                      </Col>

                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>Tên nhân viên</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {!dataDetail.sale_name
                                  ? "-"
                                  : dataDetail.sale_name}
                              </span>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>Giới tính</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {dataDetail.gender === 1 ? "Nam" : "Nữ"}
                              </span>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>Điện thoại</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {!dataDetail.sale_phone
                                  ? "-"
                                  : dataDetail.sale_phone}
                              </span>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>Email</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {!dataDetail.sale_email
                                  ? "-"
                                  : dataDetail.sale_email}
                              </span>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>CMND/CCCD</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {!dataDetail.identity_card_number
                                  ? "-"
                                  : dataDetail.identity_card_number}
                              </span>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>Địa chỉ</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {!dataDetail.sale_address
                                  ? "-"
                                  : dataDetail.sale_address}
                              </span>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
                <div style={{padding: "20px 40px"}}>
                  <Skeleton paragraph={{rows: 6}} active loading={isLoading}>
                    <Row gutter={[0, 20]}>
                      <Col span={24}>
                        <StyledITitle style={{color: "black", fontSize: 16}}>
                          Thông tin nhân viên
                        </StyledITitle>
                      </Col>
                      <Col span={24}>
                        <Row>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>Mã nhân viên</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {!dataDetail.sale_code
                                  ? "-"
                                  : dataDetail.sale_code}
                              </span>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>Mật khẩu</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                ******
                              </span>
                            </div>
                          </Col>
                          {/* <Col span={12}>
                            <div>
                              <p style={{ fontWeight: 500 }}>Số hợp đồng</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {!dataDetail.id_contract
                                  ? "-"
                                  : dataDetail.id_contract}
                              </span>
                            </div>
                          </Col> */}
                        </Row>
                      </Col>

                      <Col span={24}>
                        <Row>
                          <Col span={24}>
                            <div>
                              <p style={{fontWeight: 500}}>Trạng thái</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {dataDetail.status === 1
                                  ? "Đang hoạt động"
                                  : "Ngưng hoạt động"}
                              </span>
                            </div>
                          </Col>
                          {/* <Col span={12}>
                            <div>
                              <p style={{ fontWeight: 500 }}>
                                Ngày ký hợp đồng
                              </p>
                              <span>
                                {!dataDetail.create_contract_date ||
                                dataDetail.create_contract_date < 0
                                  ? "-"
                                  : FormatterDay.dateFormatWithString(
                                      dataDetail.create_contract_date,
                                      "#DD#/#MM#/#YYYY#"
                                    )}
                              </span>
                            </div>
                          </Col> */}
                        </Row>
                      </Col>
                      {/* <Col span={24}>
                        <Row>
                          <Col span={12}>
                            <div>
                              <p style={{ fontWeight: 500 }}>Trạng thái</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {dataDetail.status === 1
                                  ? "Đang hoạt động"
                                  : "Ngưng hoạt động"}
                              </span>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{ fontWeight: 500 }}>
                                Ngày hết hạn hợp đồng
                              </p>
                              <span>
                                {!dataDetail.end_contract_date ||
                                dataDetail.end_contract_date < 0
                                  ? "-"
                                  : FormatterDay.dateFormatWithString(
                                      dataDetail.end_contract_date,
                                      "#DD#/#MM#/#YYYY#"
                                    )}
                              </span>
                            </div>
                          </Col>
                        </Row>
                      </Col> */}
                      <Col span={24}>
                        <Row>
                          <Col span={dataDetail.sale_man_type === 1 ? 12 : 24}>
                            <div>
                              <p style={{fontWeight: 500}}>Showroom</p>
                              <span
                                style={{
                                  wordWrap: "break-word",
                                  wordBreak: "break-all",
                                }}
                              >
                                {dataDetail.sale_man_type === 1 ? !dataDetail.showrom ? '-' : dataDetail.showrom : dataDetail.list_showroom && Array.isArray(dataDetail.list_showroom) ? dataDetail.list_showroom.map(item => item.name).toString() : '-'}
                              </span>
                            </div>
                          </Col>
                          {dataDetail.sale_man_type === 1 && (
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Trưởng phòng phụ trách
                                </p>
                                <span
                                  style={{
                                    wordWrap: "break-word",
                                    wordBreak: "break-all",
                                  }}
                                >
                                {!dataDetail.manager_name
                                  ? "-"
                                  : dataDetail.manager_name}
                              </span>
                              </div>
                            </Col>
                          )}
                        </Row>
                      </Col>
                      <Col span={24}>
                        <div>
                          <p style={{fontWeight: 500}}>Kênh kinh doanh</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                                {dataDetail.list_business && Array.isArray(dataDetail.list_business) ? dataDetail.list_business.map(item => item.name).join(', ') : '-'}
                              </span>
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
                <div style={{padding: "20px 40px 30px 40px"}}>
                  <Skeleton paragraph={{rows: 2}} active loading={isLoading}>
                    <Row gutter={[0, 20]}>
                      <Col span={24}>
                        <StyledITitle
                          style={{
                            color: "black",
                            fontSize: 16,
                            marginTop: 20,
                          }}
                        >
                          Thông tin vận hành
                        </StyledITitle>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{display: "flex", flexDirection: "column"}}
                        >
                          {/*{dataDetail.sale_man_type == 3 ? (*/}
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <div
                              style={{
                                width: 20,
                                height: 20,
                                border: "5px solid" + `${colors.main}`,
                                borderRadius: 10,
                              }}
                            />
                            <div style={{flex: 1, marginLeft: 10}}>
                                <span
                                  style={{
                                    fontSize: 14,
                                    color: colors.text.black,
                                  }}
                                >
                                  {listSalesManType.length !== 0 && (listSalesManType.find((item) => item.id === dataDetail.sale_man_type) ? listSalesManType.find((item) => item.id === dataDetail.sale_man_type).name : null)}
                                </span>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
              <Col
                span={8}
                style={{
                  borderLeft: "1px solid #A4A8A8",
                  backgroundColor: "white",
                }}
              >
                <div style={{padding: "30px 40px 20px 40px"}}>
                  <Skeleton paragraph={{rows: 8}} active loading={isLoading}>
                    <Row gutter={[0, 20]}>
                      <Col span={24}>
                        <StyledITitle style={{color: "black", fontSize: 16}}>
                          Hình ảnh
                        </StyledITitle>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            padding: 10,
                            border: "1px solid #D8DBDC",
                            marginBottom: 25,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: 4,
                          }}
                        >
                          <img
                            src={dataDetail.url + dataDetail.avatar}
                            alt="avatar"
                            style={{
                              width: 175,
                              height: 175,
                              borderRadius: 4,
                            }}
                          />
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
                <div style={{padding: "0px 40px 30px 40px"}}>
                  <Skeleton paragraph={{rows: 9}} active loading={isLoading}>
                    <Col span={24}>
                      <div>
                        <p style={{fontWeight: 500}}>Ghi chú</p>
                        <div
                          style={{
                            wordWrap: "break-word",
                            wordBreak: "break-all",
                            maxHeight: 340,
                            overflowY: "auto",
                            overflowX: "hidden",
                          }}
                        >
                          {!dataDetail.note ? "-" : dataDetail.note}
                        </div>
                      </div>
                    </Col>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default DetailSalesman;
