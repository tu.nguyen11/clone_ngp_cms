import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  IDatePicker,
  ISelectStatus,
  ISelectAllStatus,
  IBannerIMG,
} from "../../components";
import { colors } from "../../../assets";
import { message, Button, Modal, DatePicker, List, Icon } from "antd";
import { APIService } from "../../../services";
import { priceFormat } from "../../../utils";
import FormatterDay from "../../../utils/FormatterDay";
import moment from "moment";
import InfiniteScroll from "react-infinite-scroller";

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const NewDate = new Date();

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

class HistoryVisitSalesman extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start_date: NewDate.getTime(),
      end_date: NewDate.getTime(),
      toLimit: 2,
      loadingTable: true,
      dataTable: [],
      url: "",
      visible: false,
    };
    this.IDSalesman = this.props.idSalesman;
  }

  componentDidMount() {
    // this._APIgetHistoryVisit(
    //   this.IDSalesman,
    //   this.state.start_date,
    //   this.state.end_date,
    //   this.state.toLimit
    // );
    this._APIgetHistoryVisit(
      this.IDSalesman,
      this.state.start_date,
      this.state.end_date,
      this.state.toLimit
    );
    this.getDate();
  }

  _APIgetHistoryVisit = async (id, start_date, end_date, toLimit) => {
    try {
      const data = await APIService._getListHistoryVisit(
        id,
        start_date,
        end_date,
        toLimit
      );
      let history = data.list_history;

      history.map((item, index) => {
        history[index].stt = index + 1;
        history[index].avatar1 = item.listImage[0];
      });

      this.setState({
        dataTable: history,
        loadingTable: false,
        url: data.url,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };
  getDate = () => {
    let from = new Date();
    from.setDate(1);
    from.setHours(0, 0, 0, 0);

    let to = new Date();
    to.setHours(0, 0, 0, 0);
    this.setState({
      start_date: from.getTime(),
      end_date: to.getTime(),
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: true,
    });
  };

  render() {
    const format = "DD/MM/YYYY";
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        render: (stt) => _renderColumns(stt),
        //   _renderColumns(
        //     FormatterDay.dateFormatWithString(assignDate, "#DD#/#MM#/#YYYY#")
        //   )
      },
      {
        title: _renderTitle("Ngày VT"),
        dataIndex: "time",
        key: "time",
        render: (time) => time,
        //   _renderColumns(
        //     FormatterDay.dateFormatWithString(assignDate, "#DD#/#MM#/#YYYY#")
        //   )
      },
      {
        title: _renderTitle("Tên đại lý"),
        dataIndex: "agency_name",
        key: "agency_name",
        render: (agency_name) => _renderColumns(agency_name),
      },
      {
        title: _renderTitle("Địa chỉ"),
        dataIndex: "address",
        key: "address",
        align: "right",

        render: (address) => _renderColumns(address),
      },
      {
        title: _renderTitle("Bắt đầu"),
        dataIndex: "visit_shop_start",
        key: "visit_shop_start",
        align: "right",

        render: (visit_shop_start) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              visit_shop_start,
              "#hhhh#:#mm#:#ss#"
            )
          ),
      },
      {
        title: _renderTitle("Kết thúc"),
        dataIndex: "visit_shop_end",
        key: "visit_shop_end",
        align: "right",

        render: (visit_shop_end) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              visit_shop_end,
              "#hhhh#:#mm#:#ss#"
            )
          ),
      },
      {
        title: _renderTitle("Kinh độ"),
        dataIndex: "visit_lat",
        key: "visit_lat",
        align: "right",

        render: (visit_lat) => _renderColumns(visit_lat),
      },
      {
        title: _renderTitle("Vĩ độ"),
        dataIndex: "visit_long",
        key: "visit_long",
        align: "right",

        render: (visit_long) => _renderColumns(visit_long),
      },
      {
        title: _renderTitle("Đơn hàng"),
        dataIndex: "number_order",
        key: "number_order",
        align: "right",

        render: (number_order) => _renderColumns(number_order),
      },
      {
        title: _renderTitle("Doanh số"),
        dataIndex: "total_money",
        key: "total_money",
        align: "right",

        render: (total_money) => _renderColumns(total_money),
      },
      {
        title: _renderTitle("Hình ảnh"),
        dataIndex: "listImage",
        key: "listImage",
        render: (listImage) => (
          <div>
            <Button
              onClick={() => this.handleOk()}
              style={{
                display: "flex",
                justifyItems: "center",
                alignContent: "center",
              }}
            >
              <Icon type="eye" theme="filled" /> <span>Xem hình</span>
            </Button>
            <Modal
              visible={this.state.visible}
              onOk={this.handleOk}
              width={400}
              onCancel={this.handleCancel}
              footer={null}
            >
              <IBannerIMG arr={listImage} url={this.state.url} />
            </Modal>
          </div>
        ),
      },
      {
        title: _renderTitle("Loại viếng thăm"),
        dataIndex: "type_route",
        key: "type_route",

        render: (type_route) => _renderColumns(type_route),
      },
    ];
    return (
      <Container className="p-0 m-0" fluid style={{ flex: 1 }}>
        <Row
          style={{
            display: "flex",
            alignItems: "center",
            flex: 1,
            justifyContent: "flex-end",
          }}
        >
          <ITitle level={4} title="Từ ngày" style={{ marginRight: 28 }} />
          {/* <RangePicker
            size="large"
            format={format}
            value={[
              moment(new Date(this.state.start_date), format),
              moment(new Date(this.state.end_date), format)
            ]}
          /> */}
          <IDatePicker
            to={this.state.end_date}
            from={this.state.start_date}
            style={{ width: 250 }}
            onChange={(a, b) => {
              this.state.start_date = a[0]._d.getTime();
              this.state.end_date = a[1]._d.getTime();
              this.setState(
                {
                  state_date: this.state.start_date,
                  end_date: this.state.end_date,
                },
                () =>
                  this._APIgetHistoryVisit(
                    this.IDSalesman,
                    this.state.start_date,
                    this.state.end_date,
                    this.state.toLimit
                  )
              );
            }}
          />
        </Row>
        <Row style={{ marginTop: 24 }}>
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            bodyStyle={{
              height: this.state.tableHeight,
              //background: colors.background
            }}
            style={{ width: "100%" }}
            scroll={{ y: this.state.tableHeight - 50 }}
            hidePage={true}
          />
        </Row>
      </Container>
    );
  }
}

export default HistoryVisitSalesman;
