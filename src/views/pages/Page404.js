import { Result, Button } from "antd";
import React, { Component } from "react";
import { useHistory } from "react-router";

export default function Page404() {
  const history = useHistory();
  return (
    <div
      style={{
        height: "90vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Result
        status="500"
        title="500"
        subTitle="Bạn không có quyền truy cập."
        // extra={
        //   <Button
        //     onClick={() => history.replace("/orderPageS1")}
        //     type="primary"
        //   >
        //     Trang chính
        //   </Button>
        // }
      />
    </div>
  );
}
