import { Button, Col, Row, Tooltip } from 'antd';
import React from 'react';
import { useState } from 'react';
import { useHistory } from 'react-router';
import { colors, images } from '../../../../assets';
import FormatterDay from '../../../../utils/FormatterDay';
import { IButton, ISearch, ISvg, ITable } from '../../../components';
import { StyledITitle } from '../../../components/common/Font/font';

let data = [];
for (let i = 0; i < 10; i++) {
	data.push({
		stt: i + 1,
		group_user: i % 2 === 0 ? 'Admin' : 'Master Admin',
		code_group: 'ABC123456',
		create_date: 1613840400000,
	});
}

export default function ListDecentralization() {
	const history = useHistory();
	const [loadingTable, setLoadingTable] = useState(false);
	const [dataTable, setDataTable] = useState({
		list: data,
		size: 10,
		total: 22,
	});
	const [filter, setFilter] = useState({
		key: '',
		page: 1,
	});

	const columns = [
		{
			title: 'STT',
			dataIndex: 'stt',
			key: 'stt',
			align: 'center',
		},
		{
			title: 'Nhóm phân quyền',
			dataIndex: 'group_user',
			key: 'group_user',
		},
		{
			title: 'Mã nhóm',
			dataIndex: 'code_group',
			key: 'code_group',
		},
		{
			title: 'Ngày tạo',
			render: (obj) => <span>{FormatterDay.dateFormatWithString(obj.create_date, '#DD#/#MM#/#YYYY#')}</span>,
		},
		{
			title: 'Trạng thái',
			dataIndex: 'status',
			key: 'status',
			render: (status) => <span>{status === 0 ? "Tạm ngưng" : "Đang hoạt động"}</span>,
		},
		// {
		// 	title: '',
		// 	align: 'center',
		// 	width: 30,
		// 	render: (obj) => {
		// 		return (
		// 			<Tooltip title="Chỉnh sửa">
		// 				<Button
		// 					type="link"
		// 					className="cursor scale"
		// 					onClick={(e) => {
		// 						e.stopPropagation();
		// 						history.push(`/decentralization/ttpp/edit/0`);
		// 					}}
		// 				>
		// 					<ISvg name={ISvg.NAME.WRITE} width={20} fill={colors.icon.default} />
		// 				</Button>
		// 			</Tooltip>
		// 		);
		// 	},
		// },
		// {
		// 	title: '',

		// 	align: 'center',
		// 	width: 30,
		// 	render: (obj) => {
		// 		return (
		// 			<Tooltip title="Xem chi tiết">
		// 				<Button
		// 					type="link"
		// 					className="cursor scale"
		// 					onClick={(e) => {
		// 						e.stopPropagation();
		// 						history.push(`/decentralization/ttpp/detail/${obj.id}`);
		// 					}}
		// 				>
		// 					<ISvg name={ISvg.NAME.CHEVRONRIGHT} width={12} fill={colors.icon.default} />
		// 				</Button>
		// 			</Tooltip>
		// 		);
		// 	},
		// },
	];

	return (
		<div style={{ width: '100%', padding: '18px 0px 0px 0px' }}>
			<Row>
				<Col span={12}>
					<StyledITitle style={{ color: colors.main, fontSize: 22 }}>
						Danh sách tài khoản PĐV
					</StyledITitle>
				</Col>
				<Col span={12}>
					<div className="flex justify-end">
						<Tooltip title="Tìm kiếm theo nhóm phân quyền , mã nhóm">
							<ISearch
								className="cursor"
								placeholder="Tìm kiếm theo nhóm phân quyền , mã nhóm"
								onPressEnter={(e) => {
									setLoadingTable(true);
									setFilter({ ...filter, key: e.target.value, page: 1 });
								}}
								icon={
									<div
										style={{
											display: 'flex',
											width: 42,
											alignItems: 'center',
											justifyContent: 'center',
										}}
									>
										<img src={images.icSearch} style={{ width: 16, height: 16 }} alt="" />
									</div>
								}
							/>
						</Tooltip>
					</div>
				</Col>
			</Row>
			<div style={{ margin: '26px 0px' }}>
				<Row gutter={[0, 50]}>
					<Col span={24}>
						<Row>
							<Col span={24}>
								<Row gutter={[15, 0]}>
									<div className="flex justify-end">
										<IButton
											icon={ISvg.NAME.ARROWUP}
											title={'Tạo mới'}
											onClick={() => {
												history.push(`/decentralization/ttpp/create/0`);
											}}
											styleHeight={{
												width: 120,
											}}
											color={colors.main}
										/>
									</div>
								</Row>
							</Col>
						</Row>
					</Col>
					<Col span={24}>
						<Row>
							<ITable
								columns={columns}
								data={dataTable.list}
								style={{ width: '100%' }}
								defaultCurrent={1}
								sizeItem={dataTable.size}
								indexPage={filter.page}
								maxpage={dataTable.total}
								size="small"
								scroll={{ x: 0 }}
								loading={loadingTable}
								onRow={(record, rowIndex) => {
									return {
										onClick: (event) => {
											const indexRow = Number(
												event.currentTarget.attributes[1].ownerElement.dataset.rowKey
											);
											const id = dataTable.list[indexRow].id;
											history.push(`/decentralization/ttpp/detail/${id}`);
										}, // click row
									};
								}}
								onChangePage={(page) => {
									setLoadingTable(true);
									setFilter({ ...filter, page: page });
								}}
							></ITable>
						</Row>
					</Col>
				</Row>
			</div>
		</div>
	);
}
