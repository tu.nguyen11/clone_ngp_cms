import { Col, Row, Table, message, Skeleton } from "antd";
import React from "react";
import { Fragment } from "react";
import { useState } from "react";
import { useHistory, useParams } from "react-router";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import { IButton, ISvg } from "../../../components";
import { StyledITitle } from "../../../components/common/Font/font";

const columns = [
  {
    title: <th>Danh sách tính năng</th>,
    dataIndex: "name",
    key: "name",
  },
  {
    title: <th>Phân quyền</th>,
    align: "center",
    children: [
      {
        title: <th>Có</th>,
        align: "center",
        render: (obj) =>
          obj.isCheck ? (
            <ISvg name={ISvg.NAME.CheckTrue} width={20} height={20} />
          ) : (
            <ISvg name={ISvg.NAME.CheckFalse} width={20} height={20} />
          ),
      },
    ],
  },
];

export default function DetailDecentralization() {
  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);
  const [dataDetail, setDataDetail] = useState({ lsgroup: [] });
  const { id, type } = useParams();
  const history = useHistory();

  const _renderTitle = (type) => {
    if (type === "admin") return "Chi tiết Nhóm phân quyền admim";
    if (type === "chinhanh") return "Chi tiết Nhóm phân quyền PKD";
    if (type === "ttpp") return "Chi tiết Nhóm phân quyền PĐV";
  };

  const typeUser = (type) => {
    if (type === "admin") return 1;
    if (type === "chinhanh") return 2;
    if (type === "ttpp") return 3;
  };

  const _fetchAPIDetail = async (id, type) => {
    try {
      const data = await APIService._getDetailGroupPermisstion(id, type);
      setDataDetail({ ...data.data });
    } catch (error) {
    } finally {
      setLoadingTable(false);
    }
  };

  const postUpdateStatusGroupPermissionAdmin = async (obj) => {
    try {
      const data = await APIService._postUpdateStatusGroupPermissionAdmin(obj);
      setLoadingButton(false);
      message.success("Kích hoạt thành công");
      await _fetchAPIDetail(id, typeUser(type));
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postUpdateUnStatusGroupPermissionAdmin = async (obj) => {
    try {
      const data = await APIService._postUpdateUnStatusGroupPermissionAdmin(
        obj
      );
      setLoadingButton(false);
      message.success("Tạm dừng thành công");
      await _fetchAPIDetail(id, typeUser(type));
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postUpdateStatusGroupPermissionPKD = async (obj) => {
    try {
      const data = await APIService._postUpdateStatusGroupPermissionPKD(obj);
      setLoadingButton(false);
      message.success("Kích hoạt thành công");
      await _fetchAPIDetail(id, typeUser(type));
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postUpdateUnStatusGroupPermissionPKD = async (obj) => {
    try {
      const data = await APIService._postUpdateUnStatusGroupPermissionPKD(obj);
      setLoadingButton(false);
      message.success("Tạm dừng thành công");
      await _fetchAPIDetail(id, typeUser(type));
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postUpdateStatusGroupPermissionPDV = async (obj) => {
    try {
      const data = await APIService._postUpdateStatusGroupPermissionPDV(obj);
      setLoadingButton(false);
      message.success("Kích hoạt thành công");
      await _fetchAPIDetail(id, typeUser(type));
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postUpdateUnStatusGroupPermissionPDV = async (obj) => {
    try {
      const data = await APIService._postUpdateUnStatusGroupPermissionPDV(obj);
      setLoadingButton(false);
      message.success("Tạm dừng thành công");
      await _fetchAPIDetail(id, typeUser(type));
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  React.useEffect(() => {
    _fetchAPIDetail(id, typeUser(type));
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            {_renderTitle(type)}
          </StyledITitle>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <Row gutter={[15, 0]}>
                  <div className="flex justify-end">
                    {dataDetail.status === 0 ? (
                      <IButton
                        title="Kích hoạt"
                        styleHeight={{
                          width: 120,
                          marginRight: 15,
                        }}
                        loading={loadingButton}
                        onClick={() => {
                          setLoadingButton(true);
                          const obj = {
                            group_id: dataDetail.id,
                            group_name: dataDetail.name,
                            lsIdType: [Number(id)],
                            status: 1,
                          };

                          switch (type) {
                            case "admin": {
                              postUpdateStatusGroupPermissionAdmin(obj);
                              break;
                            }
                            case "chinhanh": {
                              postUpdateStatusGroupPermissionPKD(obj);
                              break;
                            }
                            case "ttpp": {
                              postUpdateStatusGroupPermissionPDV(obj);
                              break;
                            }
                          }
                        }}
                        color={colors.main}
                      />
                    ) : (
                      <IButton
                        title="Tạm dừng"
                        styleHeight={{
                          width: 120,
                          marginRight: 15,
                        }}
                        loading={loadingButton}
                        onClick={() => {
                          setLoadingButton(true);
                          const obj = {
                            group_id: dataDetail.id,
                            group_name: dataDetail.name,
                            lsIdType: [Number(id)],
                            status: 0,
                          };

                          switch (type) {
                            case "admin": {
                              postUpdateUnStatusGroupPermissionAdmin(obj);
                              break;
                            }
                            case "chinhanh": {
                              postUpdateUnStatusGroupPermissionPKD(obj);
                              break;
                            }
                            case "ttpp": {
                              postUpdateUnStatusGroupPermissionPDV(obj);
                              break;
                            }
                          }
                        }}
                        color={colors.oranges}
                      />
                    )}
                    <IButton
                      icon={ISvg.NAME.WRITE}
                      title="Điều chỉnh"
                      styleHeight={{
                        width: 120,
                      }}
                      onClick={() =>
                        history.push(
                          `/group/decentralization/${type}/edit/${id}`
                        )
                      }
                      color={colors.main}
                    />
                  </div>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <div style={{ width: 665 }} className="shadow">
              <Row type="flex">
                <Col
                  span={24}
                  style={{
                    backgroundColor: "white",
                  }}
                >
                  <div style={{ padding: "31px 40px" }}>
                    <Skeleton paragraph={{ rows: 12 }} loading={loadingTable}>
                      <Col span={24}>
                        <Fragment>
                          <Row gutter={[30, 30]}>
                            <Col span={24}>
                              <StyledITitle
                                style={{
                                  color: "black",
                                  fontSize: 16,
                                }}
                              >
                                Thông tin nhóm phân quyền
                              </StyledITitle>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p className="titleForm">Tên nhóm phân quyền</p>
                                <span
                                  style={{
                                    overflowWrap: "break-word",
                                  }}
                                >
                                  {!dataDetail.name ? "-" : dataDetail.name}
                                </span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p className="titleForm">Mã nhóm</p>
                                <span>
                                  {!dataDetail.groupCode
                                    ? "-"
                                    : dataDetail.groupCode}
                                </span>
                              </div>
                            </Col>
                            <Col span={24}>
                              <div>
                                <p className="titleForm">Trạng thái</p>
                                <span>
                                  {dataDetail.status === null ||
                                  dataDetail.status === undefined
                                    ? "-"
                                    : dataDetail.status === 1
                                    ? "Đang hoạt động"
                                    : "Tạm ngưng"}
                                </span>
                              </div>
                            </Col>
                            <Col span={24}>
                              <StyledITitle
                                style={{
                                  color: "black",
                                  fontSize: 16,
                                }}
                              >
                                Phân quyền
                              </StyledITitle>
                            </Col>
                            <Col span={24}>
                              <Table
                                columns={columns}
                                dataSource={dataDetail.lsChild}
                                bordered
                                expandRowByClick={true}
                                style={{ paddingBottom: 24 }}
                                pagination={false}
                                rowKey="id"
                                childrenColumnName="lsChild"
                              />
                            </Col>
                          </Row>
                        </Fragment>
                      </Col>
                    </Skeleton>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
