import { Col, Row, Table, message, Icon, Skeleton } from "antd";
import React, { useEffect } from "react";
import { Fragment } from "react";
import { useState } from "react";
import { useHistory, useParams } from "react-router";
import { colors } from "../../../../assets";
import { IButton, IEditor, IInput, ISvg } from "../../../components";
import { StyledITitle } from "../../../components/common/Font/font";
import { useForm, Controller } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";
import { APIService } from "../../../../services";
import { IError } from "../../../components/common";

export default function CreateDecentralization() {
  const { id, type, type_page } = useParams();
  const [loading, setLoading] = useState(type_page === "edit");
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [treePermission, setTreePermission] = useState([]);
  const [loadingTree, setLoadingTree] = useState(true);
  const [selectedCBKeys, setSelectedCBKeys] = useState(new Set());
  const [dataDetail, setDataDetail] = useState({});
  const { errors, handleSubmit, control } = useForm({});
  const history = useHistory();
  const _renderTitle = (type, type_page) => {
    if (type === "admin" && type_page === "create")
      return "Tạo Nhóm phân quyền admin";
    if (type === "chinhanh" && type_page === "create")
      return "Tạo Nhóm phân quyền PKD";
    if (type === "ttpp" && type_page === "create")
      return "Tạo Nhóm phân quyền PĐV";
    if (type === "admin" && type_page === "edit")
      return "Chỉnh sửa Nhóm phân quyền admin";
    if (type === "chinhanh" && type_page === "edit")
      return "Chỉnh sửa Nhóm phân quyền PKD";
    if (type === "ttpp" && type_page === "edit")
      return "Chỉnh sửa Nhóm phân quyền PĐV";
  };

  const _fetchAPIDetail = async (id) => {
    try {
      const data = await APIService._getDetailGroupPermisstion(
        id,
        typeUser(type)
      );
      setDataDetail({ ...data.data });
      setTreePermission([...data.data.lsChild]);
      _loopKeyActive(data.data.lsChild);
      console.log(selectedCBKeys);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (type_page === "edit") {
      _fetchAPIDetail(id);
    }
  }, []);

  const columns = [
    {
      title: <th>Danh sách tính năng</th>,
      dataIndex: "name",
      key: "name",
    },
    {
      title: <th>Phân quyền</th>,
      align: "center",
      children: [
        {
          title: <th>Có</th>,
          align: "center",
          render: (obj, record, index) => {
            return selectedCBKeys.has(obj.id) ? (
              <div
                className="cursor scale"
                onClick={() => {
                  getKey(record, false);
                }}
              >
                <ISvg name={ISvg.NAME.CheckTrue} width={20} height={20} />
              </div>
            ) : (
              <div
                className="cursor scale"
                onClick={() => {
                  getKey(record, true);
                }}
              >
                <ISvg name={ISvg.NAME.CheckFalse} width={20} height={20} />
              </div>
            );
          },
        },
      ],
    },
  ];

  const typeUser = (type) => {
    if (type === "admin") return 1;
    if (type === "chinhanh") return 2;
    if (type === "ttpp") return 3;
  };

  const _fetchAPITreePermission = async (typeUser) => {
    try {
      const data = await APIService._getTreePermission(typeUser);

      setTreePermission([...data.data]);
    } catch (error) {
      console.log(error);
    } finally {
      setLoadingTree(false);
    }
  };

  useEffect(() => {
    _fetchAPITreePermission(typeUser(type));
  }, []);

  const onSubmit = (data) => {
    if (selectedCBKeys.size === 0) {
      const content = (
        <span>
          Vui lòng chọn PHÂN QUYỀN . Ít nhất phải có 1 tính năng Phân Quyền
        </span>
      );
      return IError(content);
    }
    const dataAdd = {
      group_code: data.code.toUpperCase(),
      group_name: data.name,
      lsIdType: [...selectedCBKeys],
      type: typeUser(type),
    };
    setLoadingSubmit(true);
    if (type_page === "edit") {
      const dataUpdate = {
        group_id: id,
        group_name: data.name,
        lsIdType: [...selectedCBKeys],
        type: typeUser(type),
      };
      _fetchUpdatePermisstion(dataUpdate);
    } else {
      _fetchCreatePermisstion(dataAdd);
    }
  };

  const _fetchCreatePermisstion = async (dataAdd) => {
    try {
      const data = await APIService._postCreatePermisstion(dataAdd);

      message.success("Tạo phân quyền thành công.");
      history.replace(`/decentralization/${type}/list`);
    } catch (error) {
    } finally {
      setLoadingSubmit(false);
    }
  };

  const _fetchUpdatePermisstion = async (dataUpdate) => {
    try {
      const data = await APIService._postUpdatePermisstion(dataUpdate);
      message.success("Chỉnh sửa thành công.");
      history.replace(`/decentralization/${type}/list`);
    } catch (error) {
    } finally {
      setLoadingSubmit(false);
    }
  };

  const _loopKeyActive = (data) => {
    data.map((item) => {
      if (item.lsChild && item.lsChild.length) {
        _loopKeyActive(item.lsChild);
      }
      if (!item.isCheck) {
      } else {
        setSelectedCBKeys(new Set(selectedCBKeys.add(item.id)));
      }
    });
  };

  const storeKey = (key, checked) => {
    //If the key is already present in "state" just delete it.
    //It helps in toggling the checkboxes.. right?
    
    if (selectedCBKeys.has(key) && checked === false) {
      const newSet = selectedCBKeys;
      newSet.delete(key);
      setSelectedCBKeys(new Set([...newSet]));
      
      return;
    }
    if (selectedCBKeys.has(key) && checked === true) {
      const newSet = selectedCBKeys;

      setSelectedCBKeys(new Set([...newSet]));
      
      return;
    }
    if (!selectedCBKeys.has(key) && checked === true) {
      setSelectedCBKeys(new Set(selectedCBKeys.add(key)));
    }
    console.log("new keys: ", key);
  };

  const getKey = (data, checked) => {
    
    storeKey(data.id, checked); //it just stores the key in "state"
    if (data.lsChild) {
      data.lsChild.map((item) => getKey(item, checked));
    }
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col span={24}>
            <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
              {_renderTitle(type, type_page)}
            </StyledITitle>
          </Col>
        </Row>
        <div style={{ margin: "26px 0px" }}>
          <Row gutter={[0, 50]}>
            <Col span={24}>
              <Row>
                <Col span={24}>
                  <Row gutter={[15, 0]}>
                    <div className="flex justify-end">
                      <IButton
                        icon={ISvg.NAME.SAVE}
                        title="Lưu"
                        loading={loadingSubmit}
                        styleHeight={{
                          width: 120,
                        }}
                        style={{ marginRight: 12 }}
                        color={colors.main}
                        htmlType="submit"
                      />
                      <IButton
                        icon={ISvg.NAME.CROSS}
                        title="Hủy"
                        styleHeight={{
                          width: 120,
                        }}
                        onClick={() => {
                          history.goBack();
                        }}
                        color={colors.oranges}
                      />
                    </div>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col span={24}>
              <div style={{ width: 665 }} className="shadow">
                <Row type="flex">
                  <Col
                    span={24}
                    style={{
                      backgroundColor: "white",
                    }}
                  >
                    <div style={{ padding: "31px 40px" }}>
                      <Skeleton paragraph={{ rows: 12 }} loading={loading}>
                        <Col span={24}>
                          <Fragment>
                            <Row gutter={[24, 30]}>
                              <Col span={24}>
                                <StyledITitle
                                  style={{ color: "black", fontSize: 16 }}
                                >
                                  Thông tin nhóm phân quyền
                                </StyledITitle>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p className="titleForm">
                                    Tên nhóm phân quyền <span style={{color: "red"}}>*</span>
                                  </p>

                                  <Controller
                                    as={
                                      <IInput placeholder="nhập tên nhóm phân quyền" />
                                    }
                                    defaultValue={dataDetail.name}
                                    name="name"
                                    control={control}
                                    rules={{
                                      required:
                                        <span style={{color: "red"}}>Vui lòng nhập tên nhóm phân quyền</span>,
                                      maxLength: {
                                        value: 60,
                                        message:
                                          <span style={{color: "red"}}>Nhập text không quá 60 ký tự bao gồm khoảng trắng</span>,
                                      },
                                      pattern: {
                                        // value: /^[A-Z0-9 ]+$/i,
                                        value:
                                          /^[A-Za-z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+(?: +[A-Za-z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+)*$/i,
                                        message:
                                          <span style={{color: "red"}}>Không nhập ký tự đặc biệt và có khoảng trắng đầu tiên và có khoảng trắng cuối cùng</span>,
                                      },
                                    }}
                                  />
                                  <ErrorMessage errors={errors} name="name">
                                    {({ messages }) =>
                                      messages &&
                                      Object.entries(messages).map(
                                        ([type, message]) => (
                                          <p key={type}>{message}</p>
                                        )
                                      )
                                    }
                                  </ErrorMessage>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p className="titleForm">Mã nhóm <span style={{color: "red"}}>*</span></p>
                                  <Controller
                                    as={
                                      <IInput
                                        placeholder="nhập mã nhóm phân quyền"
                                        disabled={type_page === "edit"}
                                      />
                                    }
                                    name="code"
                                    defaultValue={dataDetail.groupCode}
                                    control={control}
                                    rules={{
                                      required:
                                        <span style={{color: "red"}}>Vui lòng nhập mã nhóm phân quyền</span>,
                                      maxLength: {
                                        value: 30,
                                        message:
                                          <span style={{color: "red"}}>Nhập text không quá 30 ký tự</span>,
                                      },
                                      pattern: {
                                        value: /^[A-Z0-9]+$/,
                                        message: <span style={{color: "red"}}>Chỉ nhập kí tự in hoa và số</span>,
                                      },
                                    }}
                                  />
                                  <ErrorMessage errors={errors} name="code">
                                    {({ messages }) =>
                                      messages &&
                                      Object.entries(messages).map(
                                        ([type, message]) => (
                                          <p key={type}>{message}</p>
                                        )
                                      )
                                    }
                                  </ErrorMessage>
                                </div>
                              </Col>
                              <Col span={24}>
                                <StyledITitle
                                  style={{ color: "black", fontSize: 16 }}
                                >
                                  Phân quyền
                                </StyledITitle>
                              </Col>

                              <Col span={24}>
                                <Table
                                  style={{ marginBottom: 24 }}
                                  columns={columns}
                                  loading={loadingTree}
                                  dataSource={treePermission}
                                  rowKey="id"
                                  childrenColumnName="lsChild"
                                  bordered
                                  pagination={false}
                                />
                              </Col>
                            </Row>
                          </Fragment>
                        </Col>
                      </Skeleton>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </div>
      </form>
    </div>
  );
}
