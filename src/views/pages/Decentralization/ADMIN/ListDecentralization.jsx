import { Button, Col, Row, Tooltip } from 'antd';
import React, { useEffect } from 'react';
import { useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { colors, images } from '../../../../assets';
import { APIService } from '../../../../services';
import { truncate } from '../../../../utils';
import FormatterDay from '../../../../utils/FormatterDay';
import { IButton, ISearch, ISvg, ITable } from '../../../components';
import { StyledITitle } from '../../../components/common/Font/font';

export default function ListDecentralization() {
	const [loadingTable, setLoadingTable] = useState(true);
	const history = useHistory();
	const [keySearch, setKeySearch] = useState('');
	const [dataTable, setDataTable] = useState({
		data: [],
		size: 10,
		total: 10,
	});
	const { type, type_id } = useParams();
	const _renderID = (type) => {
		if (type === 'admin') return 1;
		if (type === 'chinhanh') return 2;
		if (type === 'ttpp') return 3;
	};
	const [filter, setFilter] = useState(() => {
		return {
			key: '',
			page: 1,
			type: _renderID(type),
		};
	});

	const columns = [
		{
			title: 'STT',
			dataIndex: 'stt',
			key: 'stt',
			align: 'center',
		},
		{
			title: 'Nhóm phân quyền',
			dataIndex: 'groupName',
			key: 'groupName',
			with: 100,
			render: (groupName) => <span>{groupName}</span>,
		},
		{
			title: 'Mã nhóm',
			dataIndex: 'groupCode',
			key: 'groupCode',
			render: (groupCode) => <span>{groupCode}</span>,
		},
		{
			title: 'Ngày tạo',
			render: (obj) => <span>{FormatterDay.dateFormatWithString(obj.createDate, '#DD#/#MM#/#YYYY#')}</span>,
		},
		{
			title: 'Trạng thái',
			dataIndex: 'status',
			key: 'status',
			render: (status) => <span>{status === 0 ? "Tạm ngưng" : "Đang hoạt động"}</span>,
		},
		// {
		// 	title: '',
		// 	align: 'center',
		// 	width: 30,
		// 	render: (obj) => {
		// 		return (
		// 			<Tooltip title="Chỉnh sửa">
		// 				<Button
		// 					type="link"
		// 					className="cursor scale"
		// 					onClick={(e) => {
		// 						e.stopPropagation();
		// 						history.push(`/group/decentralization/${type}/edit/${obj.id}`);
		// 					}}
		// 				>
		// 					<ISvg name={ISvg.NAME.WRITE} width={20} fill={colors.icon.default} />
		// 				</Button>
		// 			</Tooltip>
		// 		);
		// 	},
		// },
		// {
		// 	title: '',

		// 	align: 'center',
		// 	width: 30,
		// 	render: (obj) => {
		// 		return (
		// 			<Tooltip title="Xem chi tiết">
		// 				<Button
		// 					type="link"
		// 					className="cursor scale"
		// 					onClick={(e) => {
		// 						e.stopPropagation();
		// 						history.push(`/decentralization/${type}/detail/${obj.id}`);
		// 					}}
		// 				>
		// 					<ISvg name={ISvg.NAME.CHEVRONRIGHT} width={12} fill={colors.icon.default} />
		// 				</Button>
		// 			</Tooltip>
		// 		);
		// 	},
		// },
	];

	useEffect(() => {
		setLoadingTable(true);
		setFilter({ ...filter, type: _renderID(type), page: 1, key: '' });
		setKeySearch('');
	}, [_renderID(type)]);

	const _renderTitle = (type) => {
		if (type === 'admin') return 'Danh sách Nhóm phân quyền Admin';
		if (type === 'chinhanh') return 'Danh sách Nhóm phân quyền PKD';
		if (type === 'ttpp') return 'Danh sách Nhóm phân quyền PĐV';
	};

	const _fetchAPIList = async (filter) => {
		try {
			const data = await APIService._getListGroupPermisstion(filter);
			data.data.map((item, index) => (item.stt = (filter.page - 1) * 10 + index + 1));
			data.data.map((item) => {
				Object.keys(item).forEach((item1) => {
					item[item1] = truncate(item[item1], 40);
				});
			});
			setDataTable({
				...dataTable,
				data: data.data,
				size: data.size,
				total: data.total,
			});
		} catch (error) {
			console.log(error);
		} finally {
			setLoadingTable(false);
		}
	};

	useEffect(() => {
		_fetchAPIList(filter);
	}, [filter]);

	return (
		<div style={{ width: '100%', padding: '18px 0px 0px 0px' }}>
			<Row>
				<Col span={12}>
					<StyledITitle style={{ color: colors.main, fontSize: 22 }}>{_renderTitle(type)}</StyledITitle>
				</Col>
				<Col span={12}>
					<div className="flex justify-end">
						<Tooltip title="Tìm kiếm theo nhóm phân quyền , mã nhóm">
							<ISearch
								className="cursor"
								id="search"
								placeholder="Tìm kiếm theo nhóm phân quyền , mã nhóm"
								onChange={(e) => {
									setKeySearch(e.target.value);
								}}
								onPressEnter={(e) => {
									setLoadingTable(true);
									setFilter({
										...filter,
										key: e.target.value,
										page: 1,
									});
								}}
								value={keySearch}
								icon={
									<div
										style={{
											display: 'flex',
											width: 42,
											alignItems: 'center',
											justifyContent: 'center',
										}}
									>
										<img src={images.icSearch} style={{ width: 16, height: 16 }} alt="" />
									</div>
								}
							/>
						</Tooltip>
					</div>
				</Col>
			</Row>
			<div style={{ margin: '26px 0px' }}>
				<Row gutter={[0, 50]}>
					<Col span={24}>
						<Row>
							<Col span={24}>
								<Row gutter={[15, 0]}>
									<div className="flex justify-end">
										<IButton
											icon={ISvg.NAME.ARROWUP}
											title={'Tạo mới'}
											onClick={() => {
												history.push(`/group/decentralization/${type}/create/0`);
											}}
											styleHeight={{
												width: 120,
											}}
											color={colors.main}
										/>
									</div>
								</Row>
							</Col>
						</Row>
					</Col>
					<Col span={24}>
						<Row>
							<ITable
								columns={columns}
								data={dataTable.data}
								style={{ width: '100%' }}
								defaultCurrent={1}
								sizeItem={dataTable.size}
								indexPage={filter.page}
								maxpage={dataTable.total}
								size="small"
								scroll={{ x: 0 }}
								loading={loadingTable}
								onRow={(record, rowIndex) => {
									return {
										onClick: (event) => {
											const indexRow = Number(
												event.currentTarget.attributes[1].ownerElement.dataset.rowKey
											);
											const id = dataTable.data[indexRow].id;
											history.push(`/decentralization/${type}/detail/${id}`);
										}, // click row
									};
								}}
								onChangePage={(page) => {
									setLoadingTable(true);
									setFilter({ ...filter, page: page });
								}}
							></ITable>
						</Row>
					</Col>
				</Row>
			</div>
		</div>
	);
}
