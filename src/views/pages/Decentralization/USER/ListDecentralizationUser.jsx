import { Button, Col, Row, Tooltip } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import { useHistory, useParams } from "react-router";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import { truncate } from "../../../../utils";
import { IButton, ISearch, ISelect, ISvg, ITable } from "../../../components";
import { StyledITitle } from "../../../components/common/Font/font";
import styled from "styled-components";

const StyleTable = styled(ITable)`
  .ant-table-tbody > tr > td {
    height: 67px;
  }
  .ant-table-tbody > tr > td > span {
    -webkit-line-clamp: 2;
  }
`;
export default function ListDecentralizationUser() {
  const [loadingTable, setLoadingTable] = useState(true);
  const history = useHistory();
  const params = useParams();
  const [dataTable, setDataTable] = useState({
    list: [],
    page_size: 10,
    total_record: 10,
  });

  const { type } = useParams();

  const _renderID = (type) => {
    if (type === "admin") return 1;
    if (type === "chinhanh") return 2;
    if (type === "ttpp") return 3;
  };

  const [filter, setFilter] = useState({
    key: "",
    page: 1,
  });

  // const [keySearch, setKeySearch] = useState('');
  //
  // const [filterManagement, setFilterManagement] = useState({
  //   key: '',
  // });
  //
  // const [selectManagement, setSelectManagement] = useState([]);
  // const [selectGroup, setSelectGroup] = useState([]);

  const _renderTitle = (type) => {
    if (type === "admin") return "Danh sách tài khoản Admin";
    if (type === "chinhanh") return "Danh sách tài khoản PKD";
    if (type === "ttpp") return "Danh sách tài khoản PĐV";
  };

  // useEffect(() => {
  // 	setLoadingTable(true);
  // 	// setFilter({
  // 	// 	...filter,
  // 	// 	type: _renderID(type),
  // 	// 	page: 1,
  // 	// 	key: '',
  // 	// 	groupPermissionId: 0,
  // 	// 	managementUnitId: 0,
  // 	// });
  // 	// setKeySearch('');
  // 	_fetchAPI(_renderID(type));
  // }, [_renderID(type)]);

  const _fetchAPIListAdminAccount = async (filter) => {
    try {
      const data = await APIService._getListAdminAccountNGP(filter);
      data.list.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        // Object.keys(item).forEach((item1) => {
        //   item[item1] = truncate(!item[item1] ? "" : item[item1], 70);
        // });
      });
      setDataTable({
        ...dataTable,
        list: data.list,
        total_record: data.total,
      });
    } catch (error) {
    } finally {
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIListAdminAccount(filter);
  }, [filter]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 100,
    },
    {
      title: "Tên nhân viên",
      dataIndex: "name",
      key: "name",
      render: (name) => <span>{name}</span>,
    },
    {
      title: "Mã nhân viên",
      dataIndex: "code",
      key: "code",
      render: (code) => <span>{code}</span>,
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
      render: (address) => <span>{address}</span>,
      width: 400,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <span>{phone}</span>,
    },
    {
      title: "Tên đăng nhập",
      dataIndex: "username",
      key: "username",
      render: (username) => <span>{username}</span>,
    },
    {
      title: "Showroom áp dụng",
      dataIndex: "show_rooms",
      key: "show_rooms",
      render: (show_rooms) => <span>{show_rooms}</span>,
      width: 300,
    },
  ];

  // const _fetchListManagement = async (filter) => {
  // 	try {
  // 		const data = await APIService._getListManagementUnit(filter);
  // 		data.data.map((item) => {
  // 			item.value = item.name;
  // 			item.key = item.id;
  // 		});
  // 		data.data.unshift({
  // 			key: 0,
  // 			value: 'Đơn vị quản lý',
  // 		});
  // 		setSelectManagement([...data.data]);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  //
  // const _fetchListGroup = async (type) => {
  // 	try {
  // 		const data = await APIService._getListGroupDropdown(type);
  // 		data.data.map((item) => {
  // 			item.value = item.name;
  // 			item.key = item.id;
  // 		});
  // 		data.data.unshift({
  // 			key: 0,
  // 			value: 'Nhóm phân quyền',
  // 		});
  // 		setSelectGroup([...data.data]);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  //
  // const _fetchAPI = async (type1) => {
  // 	await _fetchListManagement({ ...filterManagement, type: type1 });
  // 	await _fetchListGroup(_renderID(type));
  // };
  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            {_renderTitle(type)}
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo tên , mã nhân viên">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo tên , mã nhân viên"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilter({
                    ...filter,
                    key: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <Row gutter={[15, 0]}>
                  <div
                    className="flex justify-end"
                    style={{ alignItems: "center" }}
                  >
                    <IButton
                      icon={ISvg.NAME.ARROWUP}
                      title={"Tạo mới"}
                      onClick={() => {
                        history.push(`/decentralization/user/${type}/create/0`);
                      }}
                      styleHeight={{
                        width: 120,
                      }}
                      color={colors.main}
                    />
                  </div>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <StyleTable
                columns={columns}
                data={dataTable.list}
                style={{ width: "100%" }}
                defaultCurrent={1}
                scroll={{
                  x: dataTable.list.length === 0 ? 0 : 1400,
                  y: 600,
                }}
                sizeItem={dataTable.page_size}
                indexPage={filter.page}
                maxpage={dataTable.total_record}
                size="middle"
                loading={loadingTable}
                onRow={(record, rowIndex) => {
                  return {
                    onClick: (event) => {
                      const indexRow = Number(
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey
                      );
                      const id = dataTable.list[indexRow].id;
                      history.push(
                        `/account/decentralization/user/${type}/detail/${id}`
                      );
                    }, // click row
                  };
                }}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilter({ ...filter, page: page });
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}
