
import { Col, Row, Skeleton, message } from "antd";
import React, { useEffect } from "react";
import { Fragment } from "react";
import { useState } from "react";
import { useHistory, useParams } from "react-router";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import {IButton, ISelect, ISvg} from "../../../components";
import { StyledITitle } from "../../../components/common/Font/font";
import styled from "styled-components";

const SelectStyle = styled(ISelect)`
    .ant-select-selection__choice__content{
     color:#333;
    }
}
`
export default function DetailDecentralizationUser() {
  const [loading, setLoading] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);
  const { id, type } = useParams();
  const [dataDetail, setDataDetail] = useState({});
  const [listShowroom,setListShowroom] = useState([])
  const history = useHistory();
  const _renderTitle = (type) => {
    if (type === "admin") return "Chi tiết tài khoản quản trị admin";
    if (type === "chinhanh") return "Chi tiết tài khoản quản trị PKD";
    if (type === "ttpp")
      return "Chi tiết tài khoản quản trị PĐV";
  };

  // const _renderID = (type) => {
  //   if (type === "admin") return 1;
  //   if (type === "chinhanh") return 2;
  //   if (type === "ttpp") return 3;
  // };

  const _fetchAPIDetailAccount = async (id) => {
    try {
      const data = await APIService._detailAccountNGP(id)
      setDataDetail(data);
      setListShowroom(data.showroom.map(item=>item.name))
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  //
  /**
   * Pending
   */
  // const postUpdateStatusDecentralizationUserAdmin = async (arr,id) => {
  //   try {
  //     const data = await APIService._postUpdateStatusDecentralizationUserAdmin(arr);
  //     setLoadingButton(false)
  //     message.success( "Kích hoạt thành công" )
  //     await _fetchAPIDetailAccount(id);
  //   } catch (error) {
  //     console.log(error);
  //     setLoadingButton(false)
  //   }
  // };
  //
  //  const postDeActiveDecentralizationUserAdmin = async (arr,id) => {
  //   try {
  //     const data = await APIService._postDeActiveDecentralizationUserAdmin(arr);
  //     setLoadingButton(false)
  //     message.success( "Tạm dừng thành công" )
  //     await _fetchAPIDetailAccount(id);
  //   } catch (error) {
  //     console.log(error);
  //     setLoadingButton(false)
  //   }
  // };
  // const postUpdateStatusDecentralizationUserPKD = async (arr) => {
  //   try {
  //     const data = await APIService._postUpdateStatusDecentralizationUserPKD(arr);
  //     setLoadingButton(false)
  //     message.success( "Kích hoạt thành công")
  //     await _fetchAPIDetailAccount();
  //   } catch (error) {
  //     console.log(error);
  //     setLoadingButton(false)
  //   }
  // };
  //
  // const postUpdateStatusDecentralizationUserPDV = async (arr) => {
  //   try {
  //     const data = await APIService._postUpdateStatusDecentralizationUserPDV(arr);
  //     setLoadingButton(false)
  //     message.success("Kích hoạt thành công" )
  //     await _fetchAPIDetailAccount();
  //   } catch (error) {
  //     console.log(error);
  //     setLoadingButton(false)
  //   }
  // };
  //  const postDeActiveDecentralizationUserPKD = async (arr) => {
  //   try {
  //     const data = await APIService._postDeActiveDecentralizationUserPKD(arr);
  //     setLoadingButton(false)
  //     message.success( "Tạm dừng thành công")
  //     await _fetchAPIDetailAccount();
  //   } catch (error) {
  //     console.log(error);
  //     setLoadingButton(false)
  //   }
  // };
  //
  //  const postDeActiveDecentralizationUserPDV = async (arr) => {
  //   try {
  //     const data = await APIService._postDeActiveDecentralizationUserPDV(arr);
  //     setLoadingButton(false)
  //     message.success("Tạm dừng thành công" )
  //     await _fetchAPIDetailAccount();
  //   } catch (error) {
  //     console.log(error);
  //     setLoadingButton(false)
  //   }
  // };

  useEffect(() => {
    _fetchAPIDetailAccount(id);
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            {_renderTitle(type)}
          </StyledITitle>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <Row gutter={[15, 0]}>
                  <div className="flex justify-end">
                    <IButton
                      icon={ISvg.NAME.WRITE}
                      title="Chỉnh sửa"
                      styleHeight={{
                        width: 120,
                      }}
                      onClick={(e) => {
                        history.push(
                          `/decentralization/user/${type}/edit/${id}`
                        );
                      }}
                      color={colors.main}
                    />
                  </div>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <div style={{ width: 665 }} className="shadow">
              <Row gutter={[0, 32]}>
                <Col
                  span={24}
                  style={{
                    backgroundColor: "white",
                  }}
                >
                  <div style={{ padding: "0px 40px" }}>
                    <Skeleton paragraph={{ rows: 12 }} loading={loading}>
                      <Col span={24}>
                        <Fragment>
                          <Row gutter={[0, 24]}>
                            <Col span={24}>
                              <StyledITitle
                                style={{ color: "black", fontSize: 16 }}
                              >
                                Thông tin nhân viên
                              </StyledITitle>
                            </Col>
                            <Col span={24}>
                              <div>
                                <p className="titleForm" style={{fontWeight:700}}>Tên nhân viên</p>
                                <span>{dataDetail.name ? dataDetail.name : '-' }</span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p className="titleForm" style={{fontWeight:700}}>Mã nhân viên</p>
                                <span>{dataDetail.code ? dataDetail.code : '-'}</span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p className="titleForm" style={{fontWeight:700}}>
                                  Số điện thoại
                                </p>
                                <span>{dataDetail.phone ? dataDetail.phone : '-'}</span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p className="titleForm" style={{fontWeight:700}}>Tên đăng nhập</p>
                                <span>{dataDetail.username ? dataDetail.username : '-'}</span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p className="titleForm" style={{fontWeight:700}}>Email</p>
                                <span>{dataDetail.email ? dataDetail.email : '-'}</span>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p className="titleForm" style={{fontWeight:700}}>Mật khẩu</p>
                                <span>{dataDetail.pass ? dataDetail.pass : '-'}</span>
                              </div>
                            </Col>
                            <Col span={24}>
                              <div>
                                <p className="titleForm" style={{fontWeight:700}}>Địa chỉ</p>
                                <span>{dataDetail.address ? dataDetail.address : '-'}</span>
                              </div>
                            </Col>
                          </Row>
                        </Fragment>
                      </Col>
                      <Col span={24}>
                        <Fragment>
                          <Row gutter={[0, 24]}>
                            <Col span={24}>
                              <Row gutter={[12, 8]}>
                                <Col span={24} style={{ marginBottom: 10 }}>
                                  <StyledITitle style={{ color: 'black', fontSize: 16 }}>
                                    Showroom áp dụng
                                  </StyledITitle>
                                </Col>
                                <Col span={24}>
                                  <div style={{border:'1px solid #BCBDBD',padding:10}}>
                                    <label style={{borderBottom:'1px solid #BCBDBD',width:'100%'}}>Showroom</label>
                                    <SelectStyle
                                      select={false}
                                      mode="multiple"
                                      style={{ width: '100%' }}
                                      data={[]}
                                      defaultValue={listShowroom}
                                    />
                                  </div>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        </Fragment>
                      </Col>
                    </Skeleton>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
