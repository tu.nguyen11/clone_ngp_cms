import {Col, message, Row, Skeleton, Input} from 'antd';
import React, {useEffect} from 'react';
import {Fragment} from 'react';
import {useState} from 'react';
import {useHistory, useParams} from 'react-router';
import {colors} from '../../../../assets';
import {IButton, IInput, ISelect, ISvg} from '../../../components';
import {ErrorMessage} from '@hookform/error-message';
import {StyledITitle} from '../../../components/common/Font/font';
import {useForm, Controller} from 'react-hook-form';
import {APIService} from '../../../../services';
import styled from 'styled-components'

const InputStyle = styled(Input.Password)`
  border-radius: 0;
   font-size: "1em";
   width: "100%";
   height: 40;
   background: "white";
   color: "black";
      input{
         padding-left: 0;
      }
`
const _renderID = (type) => {
  if (type === 'admin') return 1;
  if (type === 'chinhanh') return 2;
  if (type === 'ttpp') return 3;
};
export default function CreateDecentralizationUser() {
  const {id, type, type_page} = useParams();
  const {errors, handleSubmit, control, register} = useForm();
  const [loading, setLoading] = useState(type_page === 'edit');
  const [dataDetail, setDataDetail] = useState({});
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const history = useHistory();
  const [datashowroom, setDataShowroom] = useState([])
  const _renderTitle = (type, type_page) => {
    if (type === 'admin' && type_page === 'create') return 'Tạo tài khoản quản trị Admin';
    if (type === 'chinhanh' && type_page === 'create') return 'Tạo tài khoản quản trị PKD';
    if (type === 'ttpp' && type_page === 'create') return 'Tạo tài khoản quản trị PĐV';
    if (type === 'admin' && type_page === 'edit') return 'Chỉnh sửa tài khoản quản trị Admin';
    if (type === 'chinhanh' && type_page === 'edit') return 'Chỉnh sửa tài khoản quản trị PKD';
    if (type === 'ttpp' && type_page === 'edit') return 'Chỉnh sửa tài khoản quản trị PĐV';
  };
  const [filterManagement, setFilterManagement] = useState({
    type: _renderID(type),
    key: '',
  });
  const [selectManagement, setSelectManagement] = useState([]);
  const [selectGroup, setSelectGroup] = useState([]);

  // const _fetchListManagement = async (filter) => {
  //   try {
  //     const data = await APIService._getListManagementUnit(filter);
  //     data.data.map((item) => {
  //       item.value = item.name;
  //       item.key = item.id;
  //     });
  //     setSelectManagement([...data.data]);
  //   } catch (error) {
  //   } finally {
  //   }
  // };
  //
  // const _fetchListGroup = async (type) => {
  //   try {
  //     const data = await APIService._getListGroupDropdown(type);
  //     data.data.map((item) => {
  //       item.value = item.name;
  //       item.key = item.id;
  //     });
  //     setSelectGroup([...data.data]);
  //   } catch (error) {
  //   } finally {
  //   }
  // };'
  // const regex1 = new RegExp('/^[A-Za-z0-9]+$/i');
  //
  // console.log( 'đâsdasd'.test('/^[A-Za-z0-9]+$/i'))
  const _fetchListShowroom = async () => {
    try {
      const response = await APIService._getListShowroom()
      let newArr = response.data.map(item => {
        return {
          key: item.id,
          value: item.name
        }
      })
      setDataShowroom(newArr)
    } catch (e) {
      throw e
    }
  }
  const _fetchAPI = async () => {
    await _fetchListShowroom()
    // await _fetchListManagement(filterManagement);
    // await _fetchListGroup(_renderID(type));
  };

  useEffect(() => {
    _fetchAPI();
  }, []);

  const _fetchAPIDetailAccount = async () => {
    try {
      const response = await APIService._detailAccountNGP(id)
      setDataDetail(response);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (type_page === 'edit') {
      _fetchAPIDetailAccount(id);
    }
  }, []);
  const onSubmit = (data) => {
    const dataAdd = {
      address: data.address,
      code_admin: data.code,
      email: data.email,
      list_id_showroom: data.list_id_showroom,
      name_admin: data.name,
      pass: data.pass,
      phone: data.phone,
      username: data.username,
    };
    if (type_page == 'edit') {
      dataAdd.id = id
      setLoadingSubmit(true);
      _handlePostEditAccount(dataAdd);
    } else {
      setLoadingSubmit(true);
      _handlePostCreateAccount(dataAdd);
    }
  };

  const _handlePostCreateAccount = async (dataAdd) => {
    try {
      const data = await APIService._postCreateAccountAdminNgp(dataAdd);
      message.success('Tạo tài khoản thành công.');
      history.push(`/decentralization/user/${type}/list`);
    } catch (error) {
    } finally {
      setLoadingSubmit(false);
    }
  };
  const _handlePostEditAccount = async (dataAdd) => {
    try {
      const data = await APIService._postEditAccountAdminNgp(dataAdd);
      message.success('Chỉnh sửa tài khoản thành công');
      history.push(`/account/decentralization/user/${type}/detail/${id}`)
    } catch (error) {
    } finally {
      setLoadingSubmit(false);
    }
  };
  return (
    <div style={{width: '100%', padding: '18px 0px 0px 0px'}}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col span={24}>
            <StyledITitle style={{color: colors.main, fontSize: 22}}>
              {_renderTitle(type, type_page)}
            </StyledITitle>
          </Col>
        </Row>

        <div style={{margin: '26px 0px'}}>
          <Row gutter={[0, 50]}>
            <Col span={24}>
              <Row>
                <Col span={24}>
                  <Row gutter={[15, 0]}>
                    <div className="flex justify-end">
                      <IButton
                        icon={ISvg.NAME.SAVE}
                        title="Lưu"
                        styleHeight={{
                          width: 120,
                        }}
                        loading={loadingSubmit}
                        style={{marginRight: 12}}
                        color={colors.main}
                        htmlType="submit"
                      />
                      <IButton
                        icon={ISvg.NAME.CROSS}
                        title="Hủy"
                        styleHeight={{
                          width: 120,
                        }}
                        onClick={() => {
                          history.goBack();
                        }}
                        color={colors.oranges}
                      />
                    </div>
                  </Row>
                </Col>
              </Row>
            </Col>

            <Col span={24}>
              <div style={{width: 565}} className="shadow">
                <Row gutter={[0, 32]}>
                  <Col
                    span={24}
                    style={{
                      backgroundColor: 'white',
                    }}
                  >
                    <div style={{padding: '0px 40px'}}>
                      <Skeleton paragraph={{rows: 12}} loading={loading}>
                        <Col span={24}>
                          <Fragment>
                            <Row gutter={[24, 24]}>
                              <Col span={24}>
                                <StyledITitle style={{color: 'black', fontSize: 16}}>
                                  Thông tin nhân viên
                                </StyledITitle>
                              </Col>
                              <Col span={24}>
                                <div>
                                  <p className="titleForm" style={{fontWeight: 700}}>Tên nhân viên <span
                                    style={{color: "red"}}>*</span></p>
                                  <Controller
                                    as={<IInput placeholder="nhập tên nhân viên"/>}
                                    control={control}
                                    rules={{
                                      required: <span style={{color: "red"}}>Vui lòng nhập tên nhân viên</span>,
                                      maxLength: {
                                        value: 60,
                                        message:
                                          <span
                                            style={{color: "red"}}>Nhập text không quá 60 ký tự bao gồm khoảng trắng</span>,
                                      },
                                      pattern: {
                                        // value: /^[A-Z0-9 ]+$/i,
                                        value: /^[A-Za-z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/i,
                                        message: <span style={{color: "red"}}>Không nhập ký tự đặc biệt</span>,
                                      },
                                    }}
                                    defaultValue={dataDetail.name}
                                    name="name"
                                  />

                                  <ErrorMessage errors={errors} name="name">
                                    {({messages}) =>
                                      messages &&
                                      Object.entries(messages).map(
                                        ([type, message]) => (
                                          <p key={type}>{message}</p>
                                        )
                                      )
                                    }
                                  </ErrorMessage>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p className="titleForm" style={{fontWeight: 700}}>Mã nhân viên <span
                                    style={{color: "red"}}>*</span></p>
                                  <Controller
                                    as={<IInput placeholder="nhập mã nhân viên"/>}
                                    control={control}
                                    rules={{
                                      required: <span style={{color: "red"}}>Vui lòng nhập mã nhân viên</span>,
                                      maxLength: {
                                        value: 30,
                                        message:
                                          <span style={{color: "red"}}>Nhập mã nhân viên không quá 30 ký tự</span>,
                                      },
                                      pattern: {
                                        value: /^[A-Z0-9]+$/,
                                        message: <span style={{color: "red"}}>Chỉ nhập kí tự in hoa và số</span>,
                                      },
                                    }}
                                    defaultValue={dataDetail.code}
                                    name="code"
                                  />
                                  <ErrorMessage errors={errors} name="code">
                                    {({messages}) =>
                                      messages &&
                                      Object.entries(messages).map(
                                        ([type, message]) => (
                                          <p key={type}>{message}</p>
                                        )
                                      )
                                    }
                                  </ErrorMessage>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p className="titleForm" style={{fontWeight: 700}}>Số điện thoại <span
                                    style={{color: "red"}}>*</span></p>
                                  <Controller
                                    as={
                                      <IInput placeholder="nhập số điện thoại đăng nhập"/>
                                    }
                                    control={control}
                                    rules={{
                                      required:
                                        <span style={{color: "red"}}>Vui lòng nhập số điện thoại</span>,
                                      pattern: {
                                        value: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
                                        message: <span style={{color: "red"}}>Số điện thoại không hợp lệ</span>,
                                      },
                                    }}
                                    name="phone"
                                    defaultValue={dataDetail.phone}
                                  />
                                  <ErrorMessage errors={errors} name="phone">
                                    {({messages}) =>
                                      messages &&
                                      Object.entries(messages).map(
                                        ([type, message]) => (
                                          <p key={type}>{message}</p>
                                        )
                                      )
                                    }
                                  </ErrorMessage>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p className="titleForm" style={{fontWeight: 700}}>Tên đăng nhập <span
                                    style={{color: "red"}}>*</span></p>

                                  <Controller
                                    as={<IInput placeholder="nhập tên đăng nhập"/>}
                                    control={control}
                                    rules={{
                                      required: <span style={{color: "red"}}>Vui lòng nhập tên đăng nhập</span>,
                                      maxLength: {
                                        value: 60,
                                        message:
                                          <span style={{color: "red"}}>Nhập tên đăng nhập không quá 60 ký tự</span>,
                                      },
                                      pattern: {
                                        value: /^[a-zA-Z0-9]+$/,
                                        message: <span style={{color: "red"}}>Chỉ nhập kí tự chữ và số</span>,
                                      },
                                    }}
                                    name="username"
                                    defaultValue={dataDetail.username}
                                  />
                                  <ErrorMessage errors={errors} name="username">
                                    {({messages}) =>
                                      messages &&
                                      Object.entries(messages).map(
                                        ([type, message]) => (
                                          <p key={type}>{message}</p>
                                        )
                                      )
                                    }
                                  </ErrorMessage>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div>
                                  <p className="titleForm" style={{fontWeight: 700}}>Email <span
                                    style={{color: "red"}}>*</span></p>

                                  <Controller
                                    as={<IInput placeholder="nhập email"/>}
                                    control={control}
                                    rules={{
                                      required: <span style={{color: "red"}}>Vui lòng nhập email</span>,
                                      maxLength: {
                                        value: 60,
                                        message:
                                          <span style={{color: "red"}}>Nhập email không quá 60 ký tự bao gồm khoảng trắng</span>,
                                      },
                                      pattern: {
                                        value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                        message: <span style={{color: "red"}}>Định dạng email không đúng</span>,
                                      },
                                    }}
                                    name="email"
                                    defaultValue={dataDetail.email}
                                  />
                                  <ErrorMessage errors={errors} name="email">
                                    {({messages}) =>
                                      messages &&
                                      Object.entries(messages).map(
                                        ([type, message]) => (
                                          <p key={type}>{message}</p>
                                        )
                                      )
                                    }
                                  </ErrorMessage>
                                </div>
                              </Col>
                              <Col span={24}>
                                <div>
                                  <p className="titleForm" style={{fontWeight: 700}}>Mật khẩu <span
                                    style={{color: "red"}}>*</span></p>
                                  <Controller
                                    as={<InputStyle placeholder={type_page === 'edit' ? 'cập nhật mật khẩu hoặc để trống nếu không thay đổi' : 'nhập mật khẩu'}/>}
                                    control={control}
                                    rules={{
                                      validate: {
                                        maxLength: value => {
                                          if(type_page === 'edit'){
                                            if (!value) return
                                          }
                                          return value && value.length < 20 && value.length >= 5
                                        },
                                        valueRegex: value => {
                                          if(type_page === 'edit'){
                                            if (!value) return
                                          }
                                          return value && value.match(/^[A-Za-z0-9]+$/i) !== null
                                        }
                                      }
                                    }}
                                    name="pass"
                                    defaultValue={''}
                                  />
                                  {errors.pass && errors.pass.type === "maxLength" && (
                                    <span style={{color: 'red'}}>{type_page === 'edit' ? 'Mật khẩu từ 5 tới 20 ký tự hoặc để trống' : 'Mật khẩu từ 5 tới 20 ký tự'}</span>
                                  )}
                                  {errors.pass && errors.pass.type === "valueRegex" && (
                                    <span style={{color: "red"}}>Nhập mật khẩu chỉ nhập ký tự tiếng anh và số</span>
                                  )}
                                </div>
                              </Col>
                              <Col span={24}>
                                <div>
                                  <p className="titleForm" style={{fontWeight: 700}}>Địa chỉ <span
                                    style={{color: "red"}}>*</span></p>

                                  <Controller
                                    as={<IInput placeholder="nhập địa chỉ"/>}
                                    control={control}
                                    rules={{
                                      required: <span style={{color: "red"}}>Vui lòng nhập địa chỉ</span>,
                                      maxLength: {
                                        value: 250,
                                        message:
                                          <span style={{color: "red"}}>Nhập text không quá 250 ký tự bao gồm khoảng trắng</span>,
                                      },
                                    }}
                                    name="address"
                                    defaultValue={dataDetail.address}
                                  />
                                  <ErrorMessage errors={errors} name="address">
                                    {({messages}) =>
                                      messages &&
                                      Object.entries(messages).map(
                                        ([type, message]) => (
                                          <p key={type}>{message}</p>
                                        )
                                      )
                                    }
                                  </ErrorMessage>
                                </div>
                              </Col>
                            </Row>
                          </Fragment>
                        </Col>
                        {/*Pending*/}
                        {/*<Col span={12}>*/}
                        {/*	<Fragment>*/}
                        {/*		<Row gutter={[12, 8]}>*/}
                        {/*			<Col span={24} style={{ marginBottom: 24 }}>*/}
                        {/*				<StyledITitle style={{ color: 'black', fontSize: 16 }}>*/}
                        {/*					Thông tin account*/}
                        {/*				</StyledITitle>*/}
                        {/*			</Col>*/}
                        {/*			<Col span={24}>*/}
                        {/*				<div>*/}
                        {/*					<p className="titleForm" style={{fontWeight:700}}>Nhóm phân quyền <span style={{color: "red"}}>*</span></p>*/}

                        {/*					<Controller*/}
                        {/*						as={*/}
                        {/*							<ISelect*/}
                        {/*								isBackground={false}*/}
                        {/*								placeholder="chọn nhóm phân quyền"*/}
                        {/*								select={true}*/}
                        {/*								data={selectGroup}*/}
                        {/*								showSearch*/}
                        {/*								filterOption={(input, option) => {*/}
                        {/*									return (*/}
                        {/*										option.props.children*/}
                        {/*											.toLowerCase()*/}
                        {/*											.indexOf(*/}
                        {/*												input.toLowerCase()*/}
                        {/*											) >= 0*/}
                        {/*									);*/}
                        {/*								}}*/}
                        {/*							/>*/}
                        {/*						}*/}
                        {/*						control={control}*/}
                        {/*						rules={{*/}
                        {/*							required: <span style={{color: "red"}}>Vui lòng chọn phân quyền</span>,*/}
                        {/*						}}*/}
                        {/*						defaultValue={dataDetail.group_permission_id}*/}
                        {/*						name="group_user"*/}
                        {/*					/>*/}
                        {/*					<ErrorMessage errors={errors} name="group_user">*/}
                        {/*						{({ messages }) =>*/}
                        {/*							messages &&*/}
                        {/*							Object.entries(messages).map(*/}
                        {/*								([type, message]) => (*/}
                        {/*									<p key={type}>{message}</p>*/}
                        {/*								)*/}
                        {/*							)*/}
                        {/*						}*/}
                        {/*					</ErrorMessage>*/}
                        {/*				</div>*/}
                        {/*			</Col>*/}
                        {/*			<Col span={24}>*/}
                        {/*				<div>*/}
                        {/*					<p className="titleForm" style={{fontWeight:700}}>Đơn vị quản lý <span style={{color: "red"}}>*</span></p>*/}

                        {/*					<Controller*/}
                        {/*						as={*/}
                        {/*							<ISelect*/}
                        {/*								isBackground={false}*/}
                        {/*								placeholder="chọn đơn vị quản lý"*/}
                        {/*								select={true}*/}
                        {/*								data={selectManagement}*/}
                        {/*								showSearch*/}
                        {/*								filterOption={(input, option) => {*/}
                        {/*									return (*/}
                        {/*										option.props.children*/}
                        {/*											.toLowerCase()*/}
                        {/*											.indexOf(*/}
                        {/*												input.toLowerCase()*/}
                        {/*											) >= 0*/}
                        {/*									);*/}
                        {/*								}}*/}
                        {/*							/>*/}
                        {/*						}*/}
                        {/*						control={control}*/}
                        {/*						defaultValue={dataDetail.manage_unit_id}*/}
                        {/*						rules={{*/}
                        {/*							required: <span style={{color: "red"}}>Vui lòng chọn đơn vị quản lý</span>,*/}
                        {/*						}}*/}
                        {/*						name="dv_user"*/}
                        {/*					/>*/}
                        {/*					<ErrorMessage errors={errors} name="dv_user">*/}
                        {/*						{({ messages }) =>*/}
                        {/*							messages &&*/}
                        {/*							Object.entries(messages).map(*/}
                        {/*								([type, message]) => (*/}
                        {/*									<p key={type}>{message}</p>*/}
                        {/*								)*/}
                        {/*							)*/}
                        {/*						}*/}
                        {/*					</ErrorMessage>*/}
                        {/*				</div>*/}
                        {/*			</Col>*/}
                        {/*			/!* <Col span={24}>*/}
                        {/*				<div>*/}
                        {/*					<p className="titleForm" style={{fontWeight:700}}>Trạng thái</p>*/}

                        {/*					<Controller*/}
                        {/*						as={*/}
                        {/*							<ISelect*/}
                        {/*								isBackground={false}*/}
                        {/*								placeholder="chọn trạng thái"*/}
                        {/*								select={true}*/}
                        {/*								data={[*/}
                        {/*									{*/}
                        {/*										key: 0,*/}
                        {/*										value: 'Ngưng hoạt động',*/}
                        {/*									},*/}
                        {/*									{*/}
                        {/*										key: 1,*/}
                        {/*										value: 'Đang hoạt động',*/}
                        {/*									},*/}
                        {/*								]}*/}
                        {/*							/>*/}
                        {/*						}*/}
                        {/*						defaultValue={dataDetail.status_id}*/}
                        {/*						control={control}*/}
                        {/*						rules={{ required: 'Vui lòng trạng thái' }}*/}
                        {/*						name="status"*/}
                        {/*					/>*/}
                        {/*					<ErrorMessage errors={errors} name="status">*/}
                        {/*						{({ messages }) =>*/}
                        {/*							messages &&*/}
                        {/*							Object.entries(messages).map(*/}
                        {/*								([type, message]) => (*/}
                        {/*									<p key={type}>{message}</p>*/}
                        {/*								)*/}
                        {/*							)*/}
                        {/*						}*/}
                        {/*					</ErrorMessage>*/}
                        {/*				</div>*/}
                        {/*			</Col>*/}
                        {/*		 *!/*/}
                        {/*		</Row>*/}
                        {/*	</Fragment>*/}
                        {/*</Col>*/}
                        <Col span={24}>
                          <Row gutter={[12, 8]}>
                            <Col span={24} style={{marginBottom: 10}}>
                              <StyledITitle style={{color: 'black', fontSize: 16}}>
                                Showroom áp dụng
                              </StyledITitle>
                            </Col>
                            <Col span={24}>
                              <div style={{border: '1px solid #BCBDBD', padding: 10}}>
                                <label style={{borderBottom: '1px solid #BCBDBD', width: '100%'}}>Showroom</label>
                                <Controller
                                  as={
                                    <ISelect
                                      isBackground={false}
                                      select={true}
                                      mode="multiple"
                                      allowClear
                                      style={{width: '100%'}}
                                      placeholder="Vui lòng chọn"
                                      data={datashowroom}
                                    />}
                                  control={control}
                                  defaultValue={dataDetail.showroom && Array.isArray(dataDetail.showroom) ? dataDetail.showroom.map(item => item.agency_id) : []}
                                  name="list_id_showroom"
                                  rules={{
                                    validate: (value) => value && value.length !== 0
                                  }}
                                />
                                {errors.list_id_showroom &&
                                <p style={{color: 'red'}}>Vui lòng chọn ít nhất 1 showroom</p>}
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Skeleton>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </div>
      </form>
    </div>
  );
}
