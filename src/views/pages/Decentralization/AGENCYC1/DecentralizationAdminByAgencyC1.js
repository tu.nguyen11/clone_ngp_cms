import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message, Modal } from "antd";
import { ISearch, ISvg, ISelect, ITable, IButton } from "../../../components";
import { colors, images } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import { useHistory } from "react-router-dom";

function DecentralizationAdminByAgencyC1(props) {
  const history = useHistory();

  const [filter, setFilter] = useState({
    donviquanly: 0,
    status: 2,
    search: "",
    type: 1,
    page: 1,
  });

  const [dataTable, setDataTable] = useState({
    lstRs: [],
    total: 1,
    size: 10,
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);

  const [isModal, setIsModal] = useState(false);
  const [checkReview, setCheckReview] = useState(1);

  const [dataReview, setDataReview] = useState([
    {
      name: "Kích hoạt phân quyền",
      active: true,
      type: 1,
    },
    {
      name: "Vô hiệu hóa phân quyền",
      active: false,
      type: 2,
    },
  ]);

  const dataStatus = [
    {
      key: 2,
      value: "Tất cả",
    },
    {
      key: 1,
      value: "Đang hoạt động",
    },
    {
      key: 0,
      value: "Tạm dừng",
    },
  ];

  const getListPermissionAdminByAgencyC1 = async (filter) => {
    try {
      const data = await APIService._getListPermissionAdminByAgencyC1(filter);
      data.lstRs.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
          status: item.trangthai,
        });
        return item;
      });

      setDataTable({
        lstRs: data.lstRs,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const postActiveAdminPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postActiveAdminPermissionsDLC1(obj);
      setSelectedRowKeys([]);
      setLoadingButton(false);
      setIsModal(false);
      message.success("Kích hoạt thành công");
      await getListPermissionAdminByAgencyC1(filter);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postDeActiveAdminPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postDeActiveAdminPermissionsDLC1(obj);
      setSelectedRowKeys([]);
      setLoadingButton(false);
      setIsModal(false);
      message.success("Tạm dừng thành công");
      await getListPermissionAdminByAgencyC1(filter);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  useEffect(() => {
    getListPermissionAdminByAgencyC1(filter);
  }, [filter]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },
    {
      title: "Đơn vị quản lý",
      dataIndex: "donviquanly",
      key: "donviquanly",
      render: (donviquanly) =>
        !donviquanly ? (
          "-"
        ) : (
          <Tooltip title={donviquanly}>
            <span>{donviquanly}</span>
          </Tooltip>
        ),
    },
    {
      title: "Tên tài khoản",
      dataIndex: "tentaikhoan",
      key: "tentaikhoan",
      render: (tentaikhoan) =>
        !tentaikhoan ? (
          "-"
        ) : (
          <Tooltip title={tentaikhoan}>
            <span>{tentaikhoan}</span>
          </Tooltip>
        ),
    },
    {
      title: "Trạng thái",
      dataIndex: "trangthai",
      key: "trangthai",
      align: "center",
      render: (trangthai) => (
        <span>{trangthai === 0 ? "Tạm dừng" : "Đang hoạt động"}</span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Phân quyền Admin theo ĐLC1
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên tài khoản">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên tài khoản"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setSelectedRowKeys([]);
                      setFilter({
                        ...filter,
                        search: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={18}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <div style={{ width: 160 }}>
                        <ISelect
                          defaultValue="Trạng thái"
                          data={dataStatus}
                          select={true}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setSelectedRowKeys([]);
                            setFilter({
                              ...filter,
                              status: value,
                              page: 1,
                            });
                          }}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className="flex justify-end">
                      <IButton
                        title={"Duyệt"}
                        color={colors.main}
                        styleHeight={{
                          width: 120,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          if (selectedRowKeys.length === 0) {
                            message.error("Vui lòng chọn tài khoản");
                            return;
                          }

                          setIsModal(true);
                        }}
                      />
                      <IButton
                        title={"Tạo mới"}
                        color={colors.main}
                        styleHeight={{
                          width: 120,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          history.push(
                            "/decentralization/user/admin/s1/create/0"
                          );
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.lstRs}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    rowKey="rowTable"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          const rowValue =
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey;
                          const objRow = JSON.parse(rowValue);
                          history.push(
                            `/decentralization/user/admin/s1/detail/${objRow.id}`
                          );
                        },
                        onMouseDown: (event) => {
                          const rowValue =
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey;
                          const objRow = JSON.parse(rowValue);
                          if (event.button == 2 || event == 4) {
                            window.open(
                              `/decentralization/user/admin/s1/detail/${objRow.id}`,
                              "_blank"
                            );
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isModal}
        footer={null}
        width={320}
        onCancel={() => setIsModal(false)}
        centered={true}
        closable={false}
      >
        <div style={{ height: "100%", padding: "0px 42px" }}>
          <Row gutter={[0, 18]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 18 }}>
                  Duyệt phân quyền
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div>
                {dataReview.map((item, index) => {
                  return (
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        paddingTop: index === 0 ? 0 : 15,
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        let data = [...dataReview];
                        data.map((item, index1) => {
                          if (index == index1) {
                            item.active = true;
                          } else {
                            item.active = false;
                          }
                          setCheckReview(data[index].type);
                          setDataReview(data);
                        });
                      }}
                    >
                      <div
                        style={{
                          width: 20,
                          height: 20,
                          border: item.active
                            ? "5px solid" + `${colors.main}`
                            : "1px solid" + `${colors.line_2}`,
                          borderRadius: 10,
                        }}
                      />
                      <div style={{ flex: 1, marginLeft: 10 }}>
                        <span
                          style={{ fontSize: 14, color: colors.text.black }}
                        >
                          {item.name}
                        </span>
                      </div>
                    </div>
                  );
                })}
              </div>
            </Col>
          </Row>
          <Row
            gutter={[17, 0]}
            style={{
              position: "absolute",
              bottom: -60,
              left: 0,
            }}
          >
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.oranges}
                  title="Hủy bỏ"
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    setIsModal(false);
                  }}
                  icon={ISvg.NAME.CROSS}
                />
              </div>
            </Col>
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.main}
                  title="Xác nhận"
                  icon={ISvg.NAME.CHECKMARK}
                  loading={loadingButton}
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    let arrayActive = [];
                    let arrayDeActive = [];
                    let arrFinal = [];

                    let arrayTmp = selectedRowKeys.map((item, index) => {
                      const itemParse = JSON.parse(item);
                      return itemParse;
                    });

                    arrayTmp.forEach((item) => {
                      if (item.status === 1) {
                        arrayActive.push(item.id);
                      }
                      if (item.status === 0) {
                        arrayDeActive.push(item.id);
                      }
                      arrFinal.push(item.id);
                    });

                    if (arrayDeActive.length > 0 && arrayActive.length > 0) {
                      message.warning(
                        "Vui lòng chọn các tài khoản có cùng trạng thái"
                      );
                      return;
                    }
                    if (checkReview === 2) {
                      if (arrayDeActive.length > 0) {
                        message.warning(
                          "Các tài khoản đang ở trạng thái 'Tạm dừng'"
                        );
                        return;
                      } else {
                        const objReview = {
                          status: 0,
                          id: arrFinal,
                          type: "admin",
                        };

                        postDeActiveAdminPermissionsDLC1(objReview);
                      }
                    }

                    if (checkReview === 1) {
                      if (arrayActive.length > 0) {
                        message.warning(
                          "Các tài khoản đang ở trạng thái 'Đang hoạt động"
                        );
                        return;
                      } else {
                        const objReview = {
                          status: 1,
                          id: arrFinal,
                          type: "admin",
                        };

                        postActiveAdminPermissionsDLC1(objReview);
                      }
                    }
                  }}
                />
              </div>
            </Col>
          </Row>
        </div>
      </Modal>
    </div>
  );
}

export default DecentralizationAdminByAgencyC1;
