import { Col, Row, Modal, Empty, message, Form, Skeleton } from "antd";
import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import {
  IButton,
  ISvg,
  ITableHtml,
  ITitle,
  ISelect,
} from "../../../components";
import {
  IError,
  useModalAgencyS1FilterState,
  IFormItem,
} from "../../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import styled from "styled-components";

const ISelectSearch = styled(ISelect)`
  input {
    padding: 0px !important;
  }
`;

function CreateDecentralizationByAgencyC1(props) {
  const params = useParams();
  const { type_user, type, id } = params;
  const history = useHistory();
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;

  const [loadingButton, setLoadingButton] = useState(false);
  const [loading, setLoading] = useState(false);

  const [isVisibleAgency1, setVisibleAgency1] = useState(false);
  const [clearS1, setClearS1] = useState(false);

  const [listS1Confirm, setListS1Confirm] = useState([]);

  const [filterManagement, setFilterManagement] = useState({
    type: type_user === "admin" ? 1 : 2,
    key: "",
  });

  const [selectManagement, setSelectManagement] = useState([]);
  const [loadingManagerList, setLoadingManagerList] = useState(false);

  const [selectAccount, setSelectAccount] = useState([]);
  const [loadingAccount, setLoadingAccount] = useState(false);

  const _fetchListManagementUnit = async (filter) => {
    try {
      setLoadingManagerList(true);
      const data = await APIService._getListManagementUnit(filter);
      data.data.map((item) => {
        item.value = item.name;
        item.key = item.id;
      });
      setSelectManagement([...data.data]);
      setLoadingManagerList(false);
    } catch (error) {
      console.log(error);
      setLoadingManagerList(false);
    }
  };

  const _fetchListAccount = async (filter) => {
    try {
      setLoadingAccount(true);
      const data = await APIService._getListAccountCheckExist(filter);
      data.lst.map((item) => {
        item.value = item.username;
        item.key = item.id;
      });
      setSelectAccount([...data.lst]);
      setLoadingAccount(false);
    } catch (error) {
      console.log(error);
      setLoadingAccount(false);
    }
  };

  const getDetailPermissionAdminByAgencyC1 = async (id) => {
    try {
      setLoading(true);
      const data = await APIService._getDetailPermissionAdminByAgencyC1({
        id,
        type: 1,
      });

      await _fetchListAccount({
        donviquanly: 1,
        type: 1,
        id_edit: data.data.id_tentaikhoan,
      });

      reduxForm.getFieldDecorator("manage_unit_id", {
        initialValue: undefined,
      });
      reduxForm.getFieldDecorator("username", {
        initialValue: undefined,
      });

      reduxForm.setFieldsValue({
        manage_unit_id: 1,
        username: data.data.id_tentaikhoan,
      });

      setListS1Confirm([...data.data.lstDetail]);

      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const getDetailPermissionPKDByAgencyC1 = async (id) => {
    try {
      setLoading(true);
      const data = await APIService._getDetailPermissionPKDByAgencyC1({
        id,
        type: 2,
      });

      await _fetchListAccount({
        donviquanly: data.data.id_donviquanly,
        type: 2,
        id_edit: data.data.id_tentaikhoan,
      });

      reduxForm.getFieldDecorator("manage_unit_id", {
        initialValue: undefined,
      });
      reduxForm.getFieldDecorator("username", {
        initialValue: undefined,
      });

      reduxForm.setFieldsValue({
        manage_unit_id: data.data.id_donviquanly,
        username: data.data.id_tentaikhoan,
      });

      setListS1Confirm([...data.data.lstDetail]);

      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    _fetchListManagementUnit(filterManagement);
    if (type === "edit") {
      type_user === "admin"
        ? getDetailPermissionAdminByAgencyC1(id)
        : getDetailPermissionPKDByAgencyC1(id);
    }
  }, [filterManagement]);

  const renderTableAgencyC1 = () => {
    return (
      <Col span={24}>
        <Row>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Col span={24}>
              <ITableHtml
                childrenBody={bodyTable(listS1Confirm)}
                childrenHeader={headerTable(dataHeader)}
                style={{
                  border: "1px solid rgba(122, 123, 123, 0.5)",
                  borderTop: "none",
                  borderBottom:
                    listS1Confirm.length === 0
                      ? "1px solid rgba(122, 123, 123, 0.5)"
                      : "none",
                  maxHeight: 350,
                }}
                isBorder={false}
              />
            </Col>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                    marginTop: 15,
                  }}
                  onClick={() => {
                    setListS1Confirm([]);
                    setClearS1(true);
                  }}
                />
              </div>
            </Col>
          </div>
        </Row>
      </Col>
    );
  };

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp bậc",
      align: "center",
    },
  ];

  const headerTable = (dataHeader) => {
    return (
      <tr>
        {dataHeader.map((item, index) => (
          <th
            className="th-table"
            style={{
              textAlign: item.align,
              paddingLeft: 6,
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (data) => {
    return data.length === 0 ? (
      <tr height="100px">
        <td colspan="3" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      data.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.shop_name ? "-" : item.shop_name}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.dms_code ? "-" : item.dms_code}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "center", fontWeight: "normal" }}
          >
            {!item.membership ? "-" : item.membership}
          </td>
        </tr>
      ))
    );
  };

  const postCreateAdminPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      let data = await APIService._postCreateAdminPermissionsDLC1(obj);

      setLoadingButton(false);
      message.success(`Tạo mới phân quyền thành công`);
      history.push(`/decentralization/user/admin/s1/list`);
    } catch (err) {
      console.log(err);
      setLoadingButton(false);
    }
  };

  const postCreatePKDPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      let data = await APIService._postCreatePKDPermissionsDLC1(obj);

      setLoadingButton(false);
      message.success(`Tạo mới phân quyền thành công`);
      history.push(`/decentralization/user/agency/s1/list`);
    } catch (err) {
      console.log(err);
      setLoadingButton(false);
    }
  };

  const postUpdateAdminPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      let data = await APIService._postUpdateAdminPermissionsDLC1(obj);

      setLoadingButton(false);
      message.success(`Chỉnh sửa phân quyền thành công`);
      history.push(`/decentralization/user/admin/s1/list`);
    } catch (err) {
      console.log(err);
      setLoadingButton(false);
    }
  };

  const postUpdatePKDPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      let data = await APIService._postUpdatePKDPermissionsDLC1(obj);

      setLoadingButton(false);
      message.success(`Chỉnh sửa phân quyền thành công`);
      history.push(`/decentralization/user/agency/s1/list`);
    } catch (err) {
      console.log(err);
      setLoadingButton(false);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields((err, values) => {
      if (err) {
        return;
      }

      if (listS1Confirm.length === 0) {
        const content = (
          <span>
            Vui lòng chọn <span style={{ fontWeight: 600 }}>Đại lý cấp 1</span>!
          </span>
        );
        return IError(content);
      }

      const listS1ConfirmID = listS1Confirm.map((item) => item.id);

      if (type === "create") {
        const objAdd = {
          don_vi_quan_ly: !values.manage_unit_id ? 0 : values.manage_unit_id,
          id_tai_khoan: !values.username ? 0 : values.username,
          list_user: listS1ConfirmID,
          type: filterManagement.type,
        };

        filterManagement.type === 1
          ? postCreateAdminPermissionsDLC1(objAdd)
          : postCreatePKDPermissionsDLC1(objAdd);
      } else {
        const objEdit = {
          id: Number(id),
          don_vi_quan_ly: !values.manage_unit_id
            ? 0
            : filterManagement.type === 1
            ? 0
            : values.manage_unit_id,
          id_tai_khoan: !values.username ? 0 : values.username,
          list_user: listS1ConfirmID,
          type: filterManagement.type,
        };

        filterManagement.type === 1
          ? postUpdateAdminPermissionsDLC1(objEdit)
          : postUpdatePKDPermissionsDLC1(objEdit);
      }
    });
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Form onSubmit={handleSubmit}>
        <Row gutter={[0, 24]}>
          <Col span={24}>
            <div>
              <StyledITitle style={{ color: colors.main }}>
                {type === "create"
                  ? `Tạo mới phân quyền ${
                      type_user === "admin" ? "Admin" : "PKD"
                    } theo ĐLC1`
                  : `Chỉnh sửa phân quyền ${
                      type_user === "admin" ? "Admin" : "PKD"
                    } theo ĐLC1`}
              </StyledITitle>
            </div>
          </Col>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Lưu"
                    color={colors.main}
                    icon={ISvg.NAME.SAVE}
                    loading={loadingButton}
                    styleHeight={{
                      width: 140,
                    }}
                    onClick={handleSubmit}
                  />
                  <IButton
                    title="Hủy"
                    color={colors.oranges}
                    icon={ISvg.NAME.CROSS}
                    styleHeight={{
                      width: 140,
                      marginLeft: 15,
                    }}
                    onClick={() => {
                      history.push(
                        type === "create"
                          ? `/decentralization/user/${type_user}/s1/list`
                          : `/decentralization/user/${type_user}/s1/detail/${id}`
                      );
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>

          <Col span={10}>
            <div
              style={{
                padding: 24,
                height: 730,
              }}
              className="box-shadow"
            >
              {loading ? (
                <Skeleton loading={true} active paragraph={{ rows: 16 }} />
              ) : (
                <Row gutter={[0, 30]}>
                  <Col span={24}>
                    <Row gutter={[0, 20]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Thông tin phân quyền
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <IFormItem>
                          {getFieldDecorator("manage_unit_id", {
                            rules: [
                              {
                                required: true,
                                message: "Vui lòng chọn đơn vị quản lý",
                              },
                            ],
                          })(
                            <div>
                              <Row>
                                <ITitle
                                  level={4}
                                  title={"Đơn vị quản lý"}
                                  style={{
                                    fontWeight: 600,
                                  }}
                                />
                                <ISelectSearch
                                  data={selectManagement}
                                  select={loadingManagerList ? false : true}
                                  loading={loadingManagerList}
                                  isBackground={false}
                                  style={{ marginTop: 1 }}
                                  placeholder="Chọn đơn vị quản lý"
                                  onChange={async (key) => {
                                    reduxForm.setFieldsValue({
                                      manage_unit_id: key,
                                      username: undefined,
                                    });
                                    setClearS1(true);
                                    setListS1Confirm([]);
                                    const filter = {
                                      donviquanly: Number(key),
                                      type: filterManagement.type,
                                      id_edit: 0,
                                    };

                                    await _fetchListAccount(filter);
                                  }}
                                  value={reduxForm.getFieldValue(
                                    "manage_unit_id"
                                  )}
                                />
                              </Row>
                            </div>
                          )}
                        </IFormItem>
                      </Col>
                      <Col span={24}>
                        <IFormItem>
                          {getFieldDecorator("username", {
                            rules: [
                              {
                                required: true,
                                message: "Vui lòng chọn tên tài khoản",
                              },
                            ],
                          })(
                            <div>
                              <Row>
                                <ITitle
                                  level={4}
                                  title={"Tên tài khoản"}
                                  style={{
                                    fontWeight: 600,
                                  }}
                                />
                                <ISelectSearch
                                  data={selectAccount}
                                  select={
                                    !reduxForm.getFieldValue(
                                      "manage_unit_id"
                                    ) || loadingAccount
                                      ? false
                                      : true
                                  }
                                  isBackground={false}
                                  loading={loadingAccount}
                                  style={{ marginTop: 1 }}
                                  placeholder="Chọn tên tài khoản"
                                  onChange={(key) => {
                                    reduxForm.setFieldsValue({
                                      username: key,
                                    });
                                  }}
                                  value={reduxForm.getFieldValue("username")}
                                />
                              </Row>
                            </div>
                          )}
                        </IFormItem>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={24}>
                    <Row gutter={[0, 20]}>
                      {listS1Confirm.length === 0 ? (
                        <Col span={24}>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Đối tượng phân quyền
                          </StyledITileHeading>
                        </Col>
                      ) : (
                        <Col span={24}>
                          <Row>
                            <Col span={12}>
                              <div
                                style={{
                                  padding: "11px 0px",
                                }}
                              >
                                <StyledITileHeading
                                  minFont="10px"
                                  maxFont="16px"
                                >
                                  Đối tượng phân quyền
                                </StyledITileHeading>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div className="flex justify-end">
                                <IButton
                                  title="Chỉnh sửa ĐLC1"
                                  color={colors.main}
                                  styleHeight={{
                                    borderRadius: 18,
                                    fontWeight: "bold",
                                    width: 150,
                                    background: "white",
                                  }}
                                  onClick={() => {
                                    setClearS1(true);
                                    setVisibleAgency1(true);
                                  }}
                                />
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      )}

                      {listS1Confirm.length === 0 ? (
                        <Col span={24}>
                          <Row gutter={[0, 15]}>
                            <Col span={24}>
                              <IButton
                                title="THEO ĐẠI LÝ"
                                color={colors.main}
                                styleHeight={{
                                  borderRadius: 18,
                                  fontWeight: "bold",
                                  background: "white",
                                }}
                                onClick={() => {
                                  if (
                                    !reduxForm.getFieldValue(
                                      "manage_unit_id"
                                    ) ||
                                    !reduxForm.getFieldValue(
                                      "manage_unit_id"
                                    ) === ""
                                  ) {
                                    message.warning(
                                      "Vui lòng chọn đơn vị quản lý trước"
                                    );
                                    return;
                                  }
                                  setVisibleAgency1(true);
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                      ) : (
                        renderTableAgencyC1()
                      )}
                    </Row>
                  </Col>
                </Row>
              )}
            </div>
          </Col>
        </Row>
        <Modal
          visible={isVisibleAgency1}
          footer={null}
          width={1200}
          centered={true}
          closable={false}
          maskClosable={false}
        >
          {useModalAgencyS1FilterState(
            (listS1Confirm) => {
              if (listS1Confirm.length === 0) {
                message.warning("Vui lòng chọn đại lý cấp 1");
                return;
              }
              setListS1Confirm([...listS1Confirm]);
              setClearS1(false);
              setVisibleAgency1(false);
            },
            () => {
              setClearS1(false);
              setVisibleAgency1(false);
            },
            listS1Confirm,
            clearS1,
            filterManagement.type === 1
              ? -1
              : reduxForm.getFieldValue("manage_unit_id"),
            isVisibleAgency1
          )}
        </Modal>
      </Form>
    </div>
  );
}

const createDecentralizationByAgencyC1 = Form.create({
  name: "CreateDecentralizationByAgencyC1",
})(CreateDecentralizationByAgencyC1);

export default createDecentralizationByAgencyC1;
