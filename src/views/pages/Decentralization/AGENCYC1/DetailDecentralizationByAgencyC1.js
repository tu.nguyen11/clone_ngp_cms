import { Col, Row, Empty, message, Skeleton } from "antd";
import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import { IButton, ISvg, ITableHtml } from "../../../components";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../components/common/Font/font";
import { APIService } from "../../../../services";

function DetailDecentralizationByAgencyC1(props) {
  //   const typingTimeoutRef = useRef(null);
  const params = useParams();
  const { type_user, id } = params;
  const history = useHistory();

  const [loading, setLoading] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);
  const [dataDetail, setDataDetail] = useState({
    lstDetail: [],
  });

  const getDetailPermissionAdminByAgencyC1 = async (id) => {
    try {
      const data = await APIService._getDetailPermissionAdminByAgencyC1({
        id,
        type: 1,
      });

      setDataDetail({
        ...data.data,
      });
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const getDetailPermissionPKDByAgencyC1 = async (id) => {
    try {
      const data = await APIService._getDetailPermissionPKDByAgencyC1({
        id,
        type: 2,
      });

      setDataDetail({
        ...data.data,
      });
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    type_user === "admin"
      ? getDetailPermissionAdminByAgencyC1(id)
      : getDetailPermissionPKDByAgencyC1(id);
  }, [id, type_user]);

  const postActiveAdminPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postActiveAdminPermissionsDLC1(obj);
      setLoadingButton(false);
      message.success("Kích hoạt thành công");
      await getDetailPermissionAdminByAgencyC1(id);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postDeActiveAdminPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postDeActiveAdminPermissionsDLC1(obj);
      setLoadingButton(false);
      message.success("Tạm dừng thành công");
      await getDetailPermissionAdminByAgencyC1(id);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postActivePKDPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postActivePKDPermissionsDLC1(obj);
      setLoadingButton(false);
      message.success("Kích hoạt thành công");
      await getDetailPermissionPKDByAgencyC1(id);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postDeActivePKDPermissionsDLC1 = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postDeActivePKDPermissionsDLC1(obj);
      setLoadingButton(false);
      message.success("Tạm dừng thành công");
      await getDetailPermissionPKDByAgencyC1(id);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const renderTableAgencyC1 = () => {
    return (
      <Col span={24}>
        <Row>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Col span={24}>
              <ITableHtml
                childrenBody={bodyTable(dataDetail.lstDetail)}
                childrenHeader={headerTable(dataHeader)}
                style={{
                  border: "1px solid rgba(122, 123, 123, 0.5)",
                  borderTop: "none",
                  borderBottom:
                    dataDetail.lstDetail.length === 0
                      ? "1px solid rgba(122, 123, 123, 0.5)"
                      : "none",
                  maxHeight: 370,
                }}
                isBorder={false}
              />
            </Col>
          </div>
        </Row>
      </Col>
    );
  };

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp bậc",
      align: "center",
    },
  ];

  const headerTable = (dataHeader) => {
    return (
      <tr>
        {dataHeader.map((item, index) => (
          <th
            className="th-table"
            style={{
              textAlign: item.align,
              paddingLeft: 6,
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (data) => {
    return data.length === 0 ? (
      <tr height="100px">
        <td colspan="3" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      data.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.shop_name ? "-" : item.shop_name}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.dms_code ? "-" : item.dms_code}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "center", fontWeight: "normal" }}
          >
            {!item.membership ? "-" : item.membership}
          </td>
        </tr>
      ))
    );
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              {`Chi tiết phân quyền ${
                type_user === "admin" ? "Admin" : "PKD"
              } theo ĐLC1`}
            </StyledITitle>
          </div>
        </Col>

        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title={dataDetail.status === 0 ? "Kích hoạt" : "Tạm dừng"}
                  color={dataDetail.status === 0 ? colors.main : colors.red}
                  loading={loadingButton}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    if (type_user === "admin") {
                      if (dataDetail.status === 0) {
                        const obj = {
                          status: 1,
                          id: [Number(id)],
                          type: "admin",
                        };

                        postActiveAdminPermissionsDLC1(obj);
                      } else {
                        const obj = {
                          status: 0,
                          id: [Number(id)],
                          type: "admin",
                        };
                        postDeActiveAdminPermissionsDLC1(obj);
                      }
                    } else {
                      if (dataDetail.status === 0) {
                        const obj = {
                          status: 1,
                          id: [Number(id)],
                          type: "agency",
                        };

                        postActivePKDPermissionsDLC1(obj);
                      } else {
                        const obj = {
                          status: 0,
                          id: [Number(id)],
                          type: "agency",
                        };
                        postDeActivePKDPermissionsDLC1(obj);
                      }
                    }
                  }}
                />
                <IButton
                  title="Chỉnh sửa"
                  color={colors.main}
                  icon={ISvg.NAME.WRITE}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                  }}
                  onClick={() => {
                    history.push(
                      `/decentralization/user/${type_user}/s1/edit/${id}`
                    );
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={10}>
          <div
            style={{
              padding: 24,
              height: 650,
            }}
            className="box-shadow"
          >
            {loading ? (
              <Skeleton loading={true} active paragraph={{ rows: 16 }} />
            ) : (
              <Row gutter={[0, 30]}>
                <Col span={24}>
                  <Row gutter={[0, 20]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thông tin phân quyền
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[15, 0]}>
                        <Col span={12}>
                          <div style={{ marginBottom: 5 }}>
                            <p style={{ fontWeight: 500 }}>Đơn vị quản lý</p>
                          </div>
                          <span>
                            {!dataDetail.donviquanly
                              ? "-"
                              : dataDetail.donviquanly}
                          </span>
                        </Col>
                        <Col span={12}>
                          <div style={{ marginBottom: 5 }}>
                            <p style={{ fontWeight: 500 }}>Trạng thái</p>
                          </div>
                          <span>
                            {dataDetail.status === 0
                              ? "Tạm dừng"
                              : "Đang hoạt động"}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginBottom: 5 }}>
                        <p style={{ fontWeight: 500 }}>Tên tài khoản</p>
                      </div>
                      <span>
                        {!dataDetail.tentaikhoan ? "-" : dataDetail.tentaikhoan}
                      </span>
                    </Col>
                  </Row>
                </Col>
                <Col span={24}>
                  <Row gutter={[0, 20]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Đối tượng phân quyền
                      </StyledITileHeading>
                    </Col>

                    {renderTableAgencyC1()}
                  </Row>
                </Col>
              </Row>
            )}
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default DetailDecentralizationByAgencyC1;
