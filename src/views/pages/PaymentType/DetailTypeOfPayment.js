import React, {useEffect, useState} from 'react'
import {Col, Empty, Row, Tooltip, Skeleton} from "antd";
import {StyledITitle} from "../../components/common/Font/font";
import {colors} from "../../../assets";
import {IButton, ISvg, ITable, ITableHtml} from "../../components";
import {useHistory, useParams} from 'react-router-dom'
import APIService from "../../../services/APIService";


function DetailTypeOfPayment(props) {
  const {id} = useParams();
  const [data, setData] = useState({
    listPayment: []
  })
  const [isLoading, setIsLoading] = useState(true);
  const getDetailPayment = async (id) => {
    try {
      const dataResponse = await APIService._getDetailTypeOfPayment(id);
      setData({
        ...dataResponse.detail,
        listPayment: dataResponse.detail.listPaymentStage
      })
      setIsLoading(false)
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getDetailPayment(id)
  }, [])
  const columns = [
    {
      title: "STT",
      align: "center",
      width: 50
    },
    {
      title: "Đợt thanh toán",
      width: 140
    },
    {
      title: "Mô tả",
    },
  ];
  const headerTable = (headerTable) => {
    return (
      <tr className="scroll" style={{background: "#E4EFFF"}}>
        {headerTable.map((item, index) => (
          <th className="th-table" style={{textAlign: item.align, width: item.width}}>
            {item.title}
          </th>
        ))}
      </tr>
    )
  };
  const bodyTable = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        height="100px"
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td colspan="7" style={{padding: 20}}>
          <Empty description="Không có dữ liệu"/>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => {
        return (
          <tr key={index} className="tr-table scroll">
            <td
              className="td-table"
              style={{fontWeight: "normal", textAlign: 'center'}}
            >
              {index + 1}
            </td>
            <td
              className="td-table"
              style={{textAlign: "left", fontWeight: "normal"}}
            >
              <div style={{maxWidth: 400}}>
                {item.name}
              </div>
            </td>
            <td
              className="td-table"
              style={{
                fontWeight: "normal",
              }}
            >
              {item.description}
            </td>
          </tr>
        )
      })
    );
  };
  return (
    <div style={{width: "100%"}}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{color: colors.main, fontSize: 22}}>
                Chi tiết hình thức thanh toán
              </StyledITitle>
            </Col>
          </Row>
        </Col>
      </Row>

      <Row style={{marginBottom: 40}}>
        <Col style={{display: 'flex', justifyContent: 'flex-end'}} span={24}>
          <IButton
            color={colors.main}
            icon={ISvg.NAME.WRITE}
            title={'Chỉnh sửa'}
            onClick={() => {
              props.history.push('/typeofpayment/edit/' + id)
            }}
          />
        </Col>
      </Row>

      <Row type='flex' gutter={[16, 16]}>
        <Col span={8}>
          <div style={{
            padding: '30px 40px',
            backgroundColor: "white",
            height: 500
          }}>
            <Skeleton paragraph={{rows: 6}} active loading={isLoading}>
              <Row>
                <Col style={{
                  paddingBottom: 25,
                }} span={24}>
                  <StyledITitle style={{fontSize: 16}}>
                    Thông tin hình thức
                  </StyledITitle>
                </Col>
                <Col span={24}>
                  <div>
                    <p style={{fontWeight: 500}}>Hình thức thanh toán (*)</p>
                    <span
                      style={{
                        wordWrap: "break-word",
                        wordBreak: "break-all",
                      }}
                    >
                                            {data.payment_name || '-'}
                                        </span>
                  </div>
                </Col>
                <Col style={{marginTop: 20}} span={24}>
                  <div>
                    <p style={{fontWeight: 500}}>Ghi chú</p>
                    <span
                      style={{
                        wordWrap: "break-word",
                        wordBreak: "break-all",
                      }}
                    >
                                            {data.note || '-'}
                                        </span>
                  </div>
                </Col>
              </Row>
            </Skeleton>
          </div>
        </Col>
        <Col span={10}>
          <div style={{
            padding: '30px 40px',
            backgroundColor: "white",
            height: 500
          }}>
            <StyledITitle style={{fontSize: 16, paddingBottom: 40}}>
              Các đợt thanh toán
            </StyledITitle>

            <ITableHtml
              childrenBody={bodyTable(data.listPayment)}
              childrenHeader={headerTable(columns)}
            />
          </div>
        </Col>
      </Row>
    </div>


  )
}


export default DetailTypeOfPayment;
