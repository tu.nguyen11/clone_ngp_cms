import React, {useEffect, useRef, useState} from 'react'
import {Col, Empty, Row, Form, Skeleton, message} from "antd";
import {StyledITitle} from "../../components/common/Font/font";
import {colors} from "../../../assets";
import {IButton, IInput, ISvg, ITable, ITableHtml} from "../../components";
import {useHistory, useParams} from 'react-router-dom'
import APIService from "../../../services/APIService";
import {IInputTextArea} from "../../components/common";


function EditPaymentType(props) {
  const {id} = useParams();
  const [data, setData] = useState({})
  const [loadingButton, setLoadingButton] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const getDetailPayment = async (id) => {
    try {
      const dataResponse = await APIService._getDetailTypeOfPayment(id);
      setData({
        ...dataResponse.detail,
      })
      setIsLoading(false)
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getDetailPayment(id)
  }, [])
  const columns = [
    {
      title: "STT",
      align: "center",
      width: 50
    },
    {
      title: "Đợt thanh toán",
      width: 140
    },
    {
      title: "Mô tả",
    },
  ];
  const headerTable = (headerTable) => {
    return (
      <tr className="scroll" style={{background: "#E4EFFF"}}>
        {headerTable.map((item, index) => (
          <th className="th-table" style={{textAlign: item.align, width: item.width}}>
            {item.title}
          </th>
        ))}
      </tr>
    )
  };
  const bodyTable = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        height="100px"
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td colspan="7" style={{padding: 20}}>
          <Empty description="Không có dữ liệu"/>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => {
        return (
          <tr key={index} className="tr-table scroll">
            <td
              className="td-table"
              style={{fontWeight: "normal", textAlign: 'center'}}
            >
              {index + 1}
            </td>
            <td
              className="td-table"
              style={{textAlign: "left", fontWeight: "normal"}}
            >
              {item.name}
            </td>
            <td
              className="td-table"
              style={{
                fontWeight: "normal",
              }}
            >
              <IInput
                value={item.description}
                onChange={e => {
                  let newData = [...data.listPaymentStage]
                  let objIndex = newData.findIndex((obj => obj.id === item.id));
                  newData[objIndex].description = e.target.value
                  setData({
                    ...data,
                    listPaymentStage: newData
                  })
                }}
                placeholder={'Nhập Mô tả'}
              />
            </td>
          </tr>
        )
      })
    );
  };
  const updatePayment = async (obj) => {
    try {
      const respose = await APIService._updateTypeOfPayment(obj)
      message.success('Cập nhật thành công')
      props.history.push('/typeofpayment/detail/' + id)
      setLoadingButton(false)
    } catch (e) {
      setLoadingButton(false)
      console.log(e)
    }
  }
  const submit = (e) => {
    e.preventDefault()
    let isChecked = true
    data.listPaymentStage.map(item => {
      if (item.description === '') isChecked = false
    })
    if (!isChecked) {
      message.error('Vui lòng nhập đầy đủ field ở bảng')
    } else {
      let obj = {
        "id": id,
        "listPaymentStage": data.listPaymentStage,
        "note": data.note
      }
      setLoadingButton(true)
      updatePayment(obj)
    }
  }
  return (
    <div style={{width: "100%"}}>
      <Form onSubmit={submit}>
        <Row>
          <Col span={24}>
            <Row>
              <Col span={12}>
                <StyledITitle style={{color: colors.main, fontSize: 22}}>
                  Chi tiết hình thức thanh toán
                </StyledITitle>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row style={{marginBottom: 40}}>
          <Col style={{display: 'flex', justifyContent: 'flex-end'}} span={24}>
            <IButton
              color={colors.main}
              icon={ISvg.NAME.WRITE}
              loading={loadingButton}
              title={'Lưu'}
              style={{marginRight: 10}}
              htmlType={'submit'}
            />
            <IButton
              color={colors.oranges}
              loading={loadingButton}
              icon={ISvg.NAME.CROSS}
              title={'Hủy'}
              onClick={() => {
                props.history.push('/typeofpayment/detail/' + id)
              }}
            />
          </Col>
        </Row>

        <Row type='flex' gutter={[16, 16]}>
          <Col span={8}>
            <div style={{
              padding: '30px 40px',
              backgroundColor: "white",
              height: 500
            }}>
              <Skeleton paragraph={{rows: 6}} active loading={isLoading}>
                <Row>
                  <Col style={{
                    paddingBottom: 25,
                  }} span={24}>
                    <StyledITitle style={{fontSize: 16}}>
                      Thông tin hình thức
                    </StyledITitle>
                  </Col>
                  <Col span={24}>
                    <div>
                      <p style={{fontWeight: 500}}>Hình thức thanh toán (*)</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                                            {data.payment_name || '-'}
                                        </span>
                    </div>
                  </Col>
                  <Col style={{marginTop: 20}} span={24}>
                    <div>
                      <p style={{fontWeight: 500}}>Ghi chú</p>
                      <IInputTextArea
                        placeholder={'Nhập ghi chú'}
                        value={data.note}
                        onChange={e => {
                          setData({
                            ...data,
                            note: e.target.value
                          })
                        }}
                      />

                    </div>
                  </Col>
                </Row>
              </Skeleton>
            </div>
          </Col>
          <Col span={10}>
            <div style={{
              padding: '30px 40px',
              backgroundColor: "white",
              height: 500
            }}>
              <StyledITitle style={{fontSize: 16, paddingBottom: 40}}>
                Các đợt thanh toán
              </StyledITitle>
              <ITableHtml
                childrenBody={bodyTable(data.listPaymentStage ? data.listPaymentStage : [])}
                childrenHeader={headerTable(columns)}
              />
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  )
}

const editPayment = Form.create(
  {
    name: "EditPaymentType"
  }
)(EditPaymentType);
export default editPayment;
