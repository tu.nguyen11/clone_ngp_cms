import React, {useState, useEffect} from 'react'
import FormatterDay from "../../../utils/FormatterDay";
import {Col, message, Row, Tooltip} from "antd";
import {ISearch, ITable} from "../../components";
import {StyledITitle} from "../../components/common/Font/font";
import {colors, images} from "../../../assets";
import ApiService from '../../../services/APIService'
import {useHistory} from 'react-router-dom';

export default function ListPaymentType(props) {
  const {history} = useHistory()
  const [loading, setIsLoading] = useState(true);
  const [filter, setFilter] = useState({
    page: 1,
    key: '',
  })
  const [data, setData] = useState({})
  const getListPayment = async (filter) => {
    try {
      const dataResponse = await ApiService._getListTypeOfPayment(filter);
      dataResponse.payment.map((item, index) => {
        item.stt = index + 1
        return item
      })
      setIsLoading(false)
      setData(dataResponse)
    } catch (err) {
      message.error(err)
    }

  }
  useEffect(() => {
    getListPayment(filter)
  }, [filter])
  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      width: 80,
      key: "stt",
      align: "center",
    },
    {
      title: "Hình thức thanh toán",
      dataIndex: "payment_name",
      key: "payment_name",
      render: (payment_name) => <span>{!payment_name ? "-" : payment_name}</span>,
    },
    {
      title: "Số đợt thanh toán",
      dataIndex: "count_payment",
      key: "count_payment",
      render: (count_payment) => (
        <Tooltip title={count_payment}>
          <span>{!count_payment ? "-" : count_payment}</span>
        </Tooltip>
      ),
      align: "center",
    },
    {
      title: "Người tạo",
      align: "creator_payment",
      dataIndex: "creator_payment",
      key: "creator_payment",
      render: (creator_payment) => (
        <Tooltip title={creator_payment}>
          <span>{!creator_payment ? "-" : creator_payment}</span>
        </Tooltip>
      ),
    },
    {
      title: "Thời gian tạo",
      dataIndex: "create_date",
      key: "create_date",
      render: (create_date) => (
        <span>
                    {!create_date || create_date <= 0
                      ? "-"
                      : FormatterDay.dateFormatWithString(
                        create_date,
                        "#DD#/#MM#/#YYYY# #hhh#:#mm#"
                      )}
                </span>
      ),
      align: "center",
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      render: (note) => <span>{!note ? "-" : note}</span>,
    },

  ];

  return (
    <React.Fragment>
      <div>
        <Row>
          <Col span={24} style={{paddingBottom: 147}}>
            <Row>
              <Col span={12}>
                <StyledITitle style={{color: colors.main, fontSize: 22}}>
                  Danh sách hình thức thanh toán
                </StyledITitle>
              </Col>
              <Col span={12}>
                <div className="flex justify-end">
                  <Tooltip title="Tìm kiếm hình thức thanh toán">
                    <ISearch
                      className="cursor"
                      placeholder="Tìm kiếm hình thức thanh toán"
                      icon={
                        <div
                          style={{
                            display: "flex",
                            width: 42,
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <img
                            src={images.icSearch}
                            style={{width: 16, height: 16}}
                            alt=""
                          />
                        </div>
                      }
                      onPressEnter={(e) => {
                        setIsLoading(true);
                        setFilter({
                          ...filter,
                          key: e.target.value,
                          page: 1,
                        });
                      }}
                    />
                  </Tooltip>
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={data.payment}
                style={{width: "100%"}}
                defaultCurrent={1}
                size="middle"
                maxpage={data.total}
                loading={loading}
                // hidePage={true}
                onRow={(record, rowIndex) => {
                  return {
                    onClick: (event) => {
                      const indexRow = Number(
                        event.currentTarget.attributes[1].ownerElement
                          .dataset.rowKey
                      );
                      const id = data.payment[indexRow].id;
                      const pathname = "/typeofpayment/detail/" + id;
                      console.log(id)
                      props.history.push(pathname);
                    },
                    onMouseDown: (event) => {
                      const indexRow = Number(
                        event.currentTarget.attributes[1].ownerElement
                          .dataset.rowKey
                      );
                      const id = data.payment[indexRow].id;
                      const pathname = "/typeofpayment/detail/" + id;
                      if (event.button == 2 || event == 4) {
                        window.open(pathname, "_blank");
                      }
                    },
                  };
                }}
              />
            </Row>
          </Col>
        </Row>


      </div>
    </React.Fragment>
  )
}
