import React, { Component } from 'react'
import { Row, Col, Form, DatePicker, Modal, message, Skeleton } from 'antd'
import {
  ITitle,
  ISvg,
  IButton,
  ISelect,
  IInput,
  IUpload,
  IEditor,
  ISelectCateNews
} from '../../components'
import { colors } from '../../../assets'
import '../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import PreviewNews from './PreviewNews'
import { ImageType } from '../../../constant'
import { APIService } from '../../../services'
import locale from 'antd/es/date-picker/locale/vi_VN'
import moment from 'moment'
import 'moment/locale/vi'
import FormatterDay from '../../../utils/FormatterDay'

const DateNow = new Date()

let millisiconNow = DateNow.getTime()

class CreateNews extends Component {
  constructor (props) {
    super(props)
    const html = '<p>Hey this rocks</p>'
    this.state = {
      type: 'NEWS',
      visible: false,
      title: '',
      content: '',
      loadingButtonAdd: false,
      loadingButtonDraft: false,
      urlImg: '',
      isSkeleton: true,
      dataDetail: {}
    }
    this.typeParams = this.props.match.params.type
    this.idNews = Number(this.props.match.params.id)
    this.onSubmit = this.onSubmit.bind(this)
    this.isAdd = this.typeParams == 'add' && this.idNews == 0
  }

  componentDidMount () {
    if (this.isAdd) {
      this.setState({
        isSkeleton: false
      })
      this.props.form.setFieldsValue({
        classifyNews: 'NEWS',
        content: ''
      })
    }
    if (!this.isAdd) {
      this._APIgetDetailNews(this.idNews)
    }
  }

  _APIgetDetailNews = async idNews => {
    try {
      const data = await APIService._getDetailNews(idNews)
      this.props.form.setFieldsValue({
        classifyNews:
          data.news.type_name == 'Bài viết'
            ? 'NEWS'
            : data.news.type_name.toUpperCase(),
        content:
          data.news.type_code == 'NEWS'
            ? data.news.description
            : data.news.video_url,
        title: data.news.title,
        summaryContent: data.news.summary_content,
        video_url: data.news.video_url,
        postTime: data.news.post_time,
        status: data.news.status_id,
        categories: data.news.cate_id,
        tags: data.news.tags,
        images: data.news.thumb
      })
      this.setState({
        urlImg: data.image_url,
        isSkeleton: false,
        dataDetail: data.news
      })
    } catch (err) {
      console.log(err)
    }
  }

  handleCancel = e => {
    this.setState({
      visible: false
    })
  }
  handleOk = e => {
    console.log(e)
    this.setState({
      visible: false
    })
  }
  onEditorStateChange = editorState => {
    this.state.editorState = editorState
    this.setState({
      editorState
    })
  }

  _checkVideoEmbed = content => {
    if (content.search('embed') == -1) {
      const url = content
      let arrUrl = url.split('/')
      const idURL = arrUrl[3]
      const urlVideo =
        'https://www.youtube.com/embed/' + idURL + '?modestbranding=1'
      return urlVideo
    } else {
      return content
    }
  }

  onSubmit = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (err) {
        return
      }
      if (this.isAdd) {
        if (values.classifyNews == 'NEWS') {
          const objAdd = {
            type_code: values.classifyNews,
            title: values.title,
            summary_content: values.summaryContent,
            description: values.content,
            video_url: '',
            post_time:
              typeof values.postTime == 'number'
                ? values.postTime
                : moment(values.postTime, 'DD/MM/YYYY HH:mm')._d.getTime(),
            status: values.status,
            cate_id: values.categories,
            tags: values.tags,
            thumb: values.images,
            is_draft: 0
          }
          this.setState(
            {
              loadingButtonAdd: true
            },
            () => this._APIpostAddNews(objAdd)
          )
        } else {
          const url = values.content
          let arrUrl = url.split('/')
          const idURL = arrUrl[3]

          const objAdd = {
            type_code: values.classifyNews,
            title: values.title,
            summary_content: values.summaryContent,
            description: '',
            video_url:
              'https://www.youtube.com/embed/' + idURL + '?modestbranding=1',
            post_time:
              typeof values.postTime == 'number'
                ? values.postTime
                : moment(values.postTime, 'DD/MM/YYYY HH:mm')._d.getTime(),
            status: values.status,
            cate_id: values.categories,
            tags: values.tags,
            thumb: values.images,
            is_draft: 0
          }
          this.setState(
            {
              loadingButtonAdd: true
            },
            () => this._APIpostAddNews(objAdd)
          )
        }
      } else {
        if (values.classifyNews == 'NEWS') {
          const objEdit = {
            id: this.idNews,
            type_code: values.classifyNews,
            title: values.title,
            summary_content: values.summaryContent,
            description: values.content,
            video_url: '',
            post_time:
              typeof values.postTime == 'number'
                ? values.postTime
                : moment(values.postTime, 'DD/MM/YYYY HH:mm')._d.getTime(),
            status: values.status,
            cate_id: values.categories,
            tags: values.tags,
            thumb: values.images,
            is_draft: 0
          }
          this.setState(
            {
              loadingButtonAdd: true
            },
            () => this._APIpostUpdateNews(objEdit)
          )
        } else {
          const objEdit = {
            id: this.idNews,
            type_code: values.classifyNews,
            title: values.title,
            summary_content: values.summaryContent,
            description: '',
            video_url: this._checkVideoEmbed(values.content),
            post_time:
              typeof values.postTime == 'number'
                ? values.postTime
                : moment(values.postTime, 'DD/MM/YYYY HH:mm')._d.getTime(),
            status: values.status,
            cate_id: values.categories,
            tags: values.tags,
            thumb: values.images,
            is_draft: 0
          }
          this.setState(
            {
              loadingButtonAdd: true
            },
            () => this._APIpostUpdateNews(objEdit)
          )
        }
      }
    })
  }

  _APIpostAddNews = async obj => {
    try {
      const data = await APIService._postAddNews(obj)
      this.setState(
        {
          loadingButtonAdd: false,
          loadingButtonDraft: false
        },
        () => {
          message.success('Thêm bài viết thành công.')
          this.props.history.push('/newsPage')
        }
      )
    } catch (err) {
      console.log(err)
      this.setState({
        loadingButtonAdd: false,
        loadingButtonDraft: false
      })
    }
  }

  _APIpostUpdateNews = async obj => {
    try {
      const data = await APIService._postUpdateNews(obj)
      this.setState(
        {
          loadingButtonAdd: false
        },
        () => {
          message.success('Chỉnh sửa bài viết thành công.')
          this.props.history.push('/newsDetail/' + this.idNews)
        }
      )
    } catch (err) {
      console.log(err)
      this.setState({
        loadingButtonAdd: false
      })
    }
  }

  _isDisable = reduxForm => {
    if (
      (this.state.dataDetail.is_draft == 0 &&
        this.state.dataDetail.post_time > millisiconNow) ||
      this.state.dataDetail.is_draft == 1
    ) {
      return false
    }
    return true
  }

  render () {
    const { getFieldDecorator } = this.props.form
    const { editorState } = this.state
    const reduxForm = this.props.form

    return (
      <div style={{ width: '100%', height: '100%' }}>
        <Row>
          <ITitle
            level={1}
            title={this.isAdd ? 'Tạo tin tức mới' : 'Chỉnh sửa tin tức'}
            style={{
              color: colors.mainDark,
              fontWeight: 'bold',
              flex: 1,
              display: 'flex',
              alignItems: 'flex-end'
            }}
          />
        </Row>
        <Form
          onSubmit={this.onSubmit}
          labelCol={{
            sm: { span: 0, offset: 0 },
            span: { span: 0, offset: 0 }
          }}
          initialValues={{
            title: 'abc'
          }}
        >
          <Row
            style={{
              marginTop: 20,
              width: '100%',
              display: 'flex'
            }}
          >
            <Col
              style={{
                display: 'flex',
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'flex-end'
              }}
            >
              <IButton
                icon={ISvg.NAME.Preview}
                title={'Xem trước'}
                color={colors.blackChart}
                onClick={() => {
                  this.setState({
                    visible: true
                  })
                }}
              />
              {this.isAdd ? (
                <IButton
                  icon={ISvg.NAME.SAVE}
                  title={'Lưu nháp'}
                  color={colors.main}
                  style={{ marginLeft: 20 }}
                  onClick={() => {
                    const objDraft = {
                      type_code: reduxForm.getFieldValue('classifyNews'),
                      title:
                        typeof reduxForm.getFieldValue('title') == 'undefined'
                          ? ''
                          : reduxForm.getFieldValue('title'),
                      summary_content:
                        typeof reduxForm.getFieldValue('summaryContent') ==
                        'undefined'
                          ? ''
                          : reduxForm.getFieldValue('summaryContent'),
                      description:
                        reduxForm.getFieldValue('classifyNews') === 'NEWS'
                          ? typeof reduxForm.getFieldValue('content') ==
                            'undefined'
                            ? ''
                            : reduxForm.getFieldValue('content')
                          : '',
                      video_url:
                        reduxForm.getFieldValue('classifyNews') === 'VIDEO'
                          ? typeof reduxForm.getFieldValue('content') ==
                            'undefined'
                            ? ''
                            : reduxForm.getFieldValue('content')
                          : '',
                      post_time:
                        typeof reduxForm.getFieldValue('postTime') ==
                        'undefined'
                          ? millisiconNow
                          : moment(
                              reduxForm.getFieldValue('postTime'),
                              'DD/MM/YYYY HH:mm'
                            )._d.getTime(),
                      status:
                        typeof reduxForm.getFieldValue('status') == 'undefined'
                          ? 0
                          : reduxForm.getFieldValue('status'),
                      cate_id:
                        typeof reduxForm.getFieldValue('categories') ==
                        'undefined'
                          ? 0
                          : reduxForm.getFieldValue('categories'),
                      tags:
                        typeof reduxForm.getFieldValue('tags') == 'undefined'
                          ? ''
                          : reduxForm.getFieldValue('tags'),
                      thumb:
                        typeof reduxForm.getFieldValue('images') == 'undefined'
                          ? ''
                          : reduxForm.getFieldValue('images'),
                      is_draft: 1
                    }
                    this.setState(
                      {
                        loadingButtonDraft: true
                      },
                      () => this._APIpostAddNews(objDraft)
                    )
                  }}
                />
              ) : null}
              <IButton
                icon={ISvg.NAME.Post}
                title={this.isAdd ? 'Đăng bài' : 'Lưu bài'}
                color={colors.main}
                loading={this.state.loadingButtonAdd}
                style={{
                  marginRight: 20,
                  marginLeft: 20
                }}
                htmlType='submit'
              />
              <IButton
                icon={ISvg.NAME.CROSS}
                title='Hủy'
                color={colors.oranges}
                onClick={() => this.props.history.goBack()}
              />
            </Col>
          </Row>
          <Row style={{ marginTop: 38, height: '100%' }}>
            <div
              style={{ width: 870, background: 'white', height: '100%' }}
              className='shadow'
            >
              <Row style={{ height: '100%' }}>
                <Col
                  span={16}
                  style={{
                    borderRight: '1px solid #F3F7F5',
                    height: '100%',
                    paddingTop: 30,
                    paddingBottom: 30,
                    paddingLeft: 40,
                    paddingRight: 40
                  }}
                >
                  <Skeleton
                    active
                    loading={this.state.isSkeleton}
                    paragraph={{ rows: 6 }}
                  >
                    <Row gutter={[0, 20]}>
                      <Col style={{ marginTop: 6, marginBottom: 24 }}>
                        <ITitle
                          level={3}
                          title='Thông tin tin tức'
                          style={{ fontWeight: 'bold' }}
                        />
                      </Col>
                      <Form.Item>
                        {getFieldDecorator('classifyNews', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'chọn phân loại tin tức'
                            }
                          ]
                        })(
                          <div>
                            <p>Phân loại tin tức</p>
                            <ISelect
                              isBackground={false}
                              placeholder='chọn loại'
                              select={this.isAdd}
                              value={reduxForm.getFieldValue('classifyNews')}
                              onChange={key => {
                                let type = key == 'VIDEO' ? 'Video' : 'Bài viết'
                                this.setState({
                                  type: type
                                })
                                reduxForm.setFieldsValue({
                                  classifyNews: key
                                })
                              }}
                              data={[
                                {
                                  key: 'VIDEO',
                                  value: 'Video'
                                },
                                { key: 'NEWS', value: 'Bài viết' }
                              ]}
                            />
                          </div>
                        )}
                      </Form.Item>
                      <Form.Item>
                        {getFieldDecorator('title', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message:
                                'nhập tiêu đề cho bài viết, và không nhập kí tự đặc biệt',
                              pattern: new RegExp(
                                `^[a-zA-Z0-9\\sàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐ\\!\\&\\,\\.\\;\\-\\*\\%\\@\\"\\(\\)]*$`
                              )
                            }
                          ]
                        })(
                          <div>
                            <p>Tiêu đề</p>
                            <IInput
                              value={reduxForm.getFieldValue('title')}
                              loai='input'
                              onChange={e => {
                                reduxForm.setFieldsValue({
                                  title: e.target.value
                                })
                              }}
                              placeholder='nhập tiêu đề cho bài viết'
                            />
                          </div>
                        )}
                      </Form.Item>
                      <Form.Item>
                        {getFieldDecorator('summaryContent', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'nhập nội dung tóm tắt'
                            }
                          ]
                        })(
                          <div>
                            <p>Nội dung tóm tắt</p>
                            <input
                              placeholder='nhập nội dung tóm tắt'
                              value={reduxForm.getFieldValue('summaryContent')}
                              onChange={e => {
                                reduxForm.setFieldsValue({
                                  summaryContent: e.target.value
                                })
                              }}
                              style={{
                                height: 50,
                                width: '100%',
                                paddingLeft: 20,
                                paddingRight: 20,
                                border: '1px solid rgba(122, 123, 123, 0.5)',
                                marginTop: 6,
                                borderRadius: 2
                              }}
                            />
                          </div>
                        )}
                      </Form.Item>
                      <Form.Item>
                        {getFieldDecorator('content', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'nhập nội dung'
                            }
                          ]
                        })(
                          reduxForm.getFieldValue('classifyNews') == 'NEWS' ? (
                            <div>
                              <p>Nội dung</p>

                              <div
                                className='editor'
                                style={{
                                  // height: 330,
                                  marginTop: 6,
                                  width: '100%',
                                  border: '1px solid rgba(122, 123, 123, 0.5)',
                                  borderRadius: 2,
                                  padding: 20
                                }}
                              >
                                <IEditor
                                  toolbar={{
                                    options: [
                                      'inline',
                                      'blockType',
                                      'fontSize',
                                      'fontFamily',
                                      'list',
                                      'textAlign',
                                      'colorPicker',
                                      'history'
                                    ],
                                    inline: { inDropdown: true },
                                    list: { inDropdown: true },
                                    textAlign: { inDropdown: true },
                                    link: { inDropdown: true },
                                    history: { inDropdown: true }
                                  }}
                                  callback={value => {
                                    reduxForm.setFieldsValue({
                                      content: value
                                    })
                                  }}
                                  html={reduxForm.getFieldValue('content')}
                                  // html="<p>Hey this rocks</p>"
                                />
                              </div>
                            </div>
                          ) : (
                            <div>
                              <p>Liên kết video</p>
                              <IInput
                                placeholder='nhập liên kết đường dẫn cho video'
                                value={reduxForm.getFieldValue('content')}
                              />
                            </div>
                          )
                        )}
                      </Form.Item>
                    </Row>
                  </Skeleton>
                </Col>
                <Col
                  span={8}
                  style={{
                    paddingTop: 30,
                    paddingBottom: 30,
                    paddingLeft: 40,
                    paddingRight: 40,
                    borderLeft: '1px solid #F3F7F5'
                  }}
                >
                  <Skeleton
                    active
                    loading={this.state.isSkeleton}
                    paragraph={{ rows: 6 }}
                  >
                    <Row gutter={[0, 20]}>
                      <Col style={{ marginTop: 6, marginBottom: 24 }}>
                        <ITitle
                          level={3}
                          title='Tùy chỉnh hiển thị'
                          style={{ fontWeight: 'bold' }}
                        />
                      </Col>
                      <Form.Item>
                        {getFieldDecorator('postTime', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'nhập thời gian đăng '
                            }
                          ]
                        })(
                          this.isAdd ? (
                            <div>
                              <p>Thời gian đăng</p>
                              <DatePicker
                                className='clear-pad'
                                placeholder='nhập thời gian đăng'
                                size='large'
                                showTime={true}
                                format='DD/MM/YYYY HH:mm'
                                locale={locale}
                                onChange={(date, dateString) => {
                                  let time = date._d
                                  let millis = time.getTime()
                                  reduxForm.setFieldsValue({
                                    postTime: millis
                                  })
                                }}
                                // onOk={() => {}}
                                // defaultValue={moment(
                                //   new Date(reduxForm.getFieldValue("postTime")),
                                //   "DD/MM/YYYY hh:mm"
                                // )}
                                style={{
                                  width: '100%',
                                  // height: 42,
                                  background: colors.white,
                                  borderStyle: 'solid',
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1
                                }}
                              />
                            </div>
                          ) : (
                            <div>
                              <p>Thời gian đăng</p>
                              <DatePicker
                                className='clear-pad'
                                placeholder='nhập thời gian đăng'
                                size='large'
                                showTime={true}
                                format='DD/MM/YYYY HH:mm'
                                locale={locale}
                                onChange={(date, dateString) => {
                                  let time = date._d
                                  let millis = time.getTime()
                                  reduxForm.setFieldsValue({
                                    postTime: millis
                                  })
                                }}
                                // onOk={() => {}}
                                disabled={this._isDisable(reduxForm)}
                                defaultValue={moment(
                                  new Date(reduxForm.getFieldValue('postTime')),
                                  'DD/MM/YYYY hh:mm'
                                )}
                                style={{
                                  width: '100%',
                                  // height: 42,
                                  background: colors.white,
                                  borderStyle: 'solid',
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1
                                }}
                              />
                            </div>
                          )
                        )}
                      </Form.Item>
                      <Form.Item>
                        {getFieldDecorator('status', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'chọn trạng thái'
                            }
                          ]
                        })(
                          this.isAdd ? (
                            <div>
                              <p>Trạng thái</p>
                              <ISelect
                                isBackground={false}
                                placeholder='chọn trạng thái'
                                select={true}
                                onChange={key => {
                                  reduxForm.setFieldsValue({
                                    status: key
                                  })
                                }}
                                data={[
                                  {
                                    key: 1,
                                    value: 'Đăng'
                                  },
                                  { key: 0, value: 'Ẩn' }
                                ]}
                              />
                            </div>
                          ) : (
                            <div>
                              <p>Trạng thái</p>
                              <ISelect
                                isBackground={false}
                                placeholder='chọn trạng thái'
                                select={true}
                                value={reduxForm.getFieldValue('status')}
                                onChange={key => {
                                  reduxForm.setFieldsValue({
                                    status: key
                                  })
                                }}
                                data={[
                                  {
                                    key: 1,
                                    value: 'Đăng'
                                  },
                                  { key: 0, value: 'Ẩn' }
                                ]}
                              />
                            </div>
                          )
                        )}
                      </Form.Item>
                      <Form.Item>
                        {getFieldDecorator('categories', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'chọn chuyên mục bài viết'
                            }
                          ]
                        })(
                          <div>
                            <p>Chuyên mục</p>

                            {this.isAdd ? (
                              <ISelectCateNews
                                isBackground={false}
                                placeholder='chọn chuyên mục bài viết'
                                select={true}
                                all={false}
                                type={reduxForm.getFieldValue('classifyNews')}
                                onChange={key => {
                                  reduxForm.setFieldsValue({
                                    categories: key
                                  })
                                }}
                              />
                            ) : (
                              <ISelectCateNews
                                isBackground={false}
                                placeholder='chọn chuyên mục bài viết'
                                select={true}
                                all={false}
                                value={reduxForm.getFieldValue('categories')}
                                type={reduxForm.getFieldValue('classifyNews')}
                                onChange={key => {
                                  reduxForm.setFieldsValue({
                                    categories: key
                                  })
                                }}
                              />
                            )}
                          </div>
                        )}
                      </Form.Item>

                      <Form.Item>
                        {getFieldDecorator('tags', {
                          rules: [{}]
                        })(
                          <div>
                            <p>Thẻ tag</p>
                            <IInput
                              value={reduxForm.getFieldValue('tags')}
                              loai='input'
                              onChange={e => {
                                reduxForm.setFieldsValue({
                                  tags: e.target.value
                                })
                              }}
                              placeholder='nhập các thẻ tag cho bài viết'
                            />
                          </div>
                        )}
                      </Form.Item>
                      <Form.Item>
                        {getFieldDecorator('images', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'chọn hình ảnh'
                            }
                          ]
                        })(
                          <div style={{ marginTop: 36 }}>
                            <ITitle
                              level={3}
                              title='Hình ảnh'
                              style={{ fontWeight: 'bold' }}
                            />
                            <div
                              style={{
                                marginTop: 22
                              }}
                            >
                              {this.isAdd ? (
                                <IUpload
                                  className='border3'
                                  sizeImg={true}
                                  type={ImageType.NEWS}
                                  callback={(array, url) => {
                                    this.setState({
                                      urlImg: url
                                    })
                                    reduxForm.setFieldsValue({
                                      images: array[0]
                                    })
                                  }}
                                />
                              ) : (
                                <IUpload
                                  className='border3'
                                  sizeImg={true}
                                  type={ImageType.NEWS}
                                  url={this.state.urlImg}
                                  name={reduxForm.getFieldValue('images')}
                                  callback={array => {
                                    reduxForm.setFieldsValue({
                                      images: array[0]
                                    })
                                  }}
                                />
                              )}
                            </div>
                          </div>
                        )}
                      </Form.Item>
                    </Row>
                  </Skeleton>
                </Col>
              </Row>
            </div>
          </Row>
        </Form>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={null}
          bodyStyle={{ padding: 0, margin: 0 }}
          style={{ borderRadius: 30 }}
          closable={false}
          centered={true}
          className='modal-phone'
          width={320}
        >
          <PreviewNews
            type={
              this.boolean
                ? this.state.type
                : reduxForm.getFieldValue('classifyNews')
            }
            content={reduxForm.getFieldValue('content')}
            title={reduxForm.getFieldValue('title')}
            summaryContent={reduxForm.getFieldValue('summaryContent')}
            time={reduxForm.getFieldValue('postTime')}
            img={this.state.urlImg + reduxForm.getFieldValue('images')}
          />
        </Modal>
      </div>
    )
  }
}

const createNews = Form.create({ name: 'CreateNews' })(CreateNews)

export default createNews
