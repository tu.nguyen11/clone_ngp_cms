import React, { useState, use } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import InfiniteScroll from "react-infinite-scroller";
import { Row, Col, Button, message } from "antd";
import { ISvg, ITitle } from "../../components";
import { colors } from "../../../assets";
import { APIService } from "../../../services";

export default function ModalCateNews(props) {
  const { callback, dataRows, type, callbackReloadSort } = props;
  const dataRowsNews = [...dataRows];
  const [dataModalCateNews, changleDataModalCateNews] = useState({
    columns: [
      { title: "STT", key: "stt", width: 100 },
      { title: "Mã chuyên mục", key: "code_category", width: 250 },
      { title: "Tên chuyên mục", key: "name_category", width: 350 },
      { title: "Mô tả", key: "description", width: 500 },
    ],
    rows: [...dataRowsNews],
  });
  const [prevRow, setPrevRow] = useState(null);
  const [isLoadingSave, handleLoading] = useState(false);

  if (JSON.stringify(dataRows) !== JSON.stringify(prevRow)) {
    changleDataModalCateNews({ ...dataModalCateNews, rows: dataRowsNews });
    setPrevRow(dataRowsNews);
  }

  let dataRows1 = [...dataModalCateNews.rows];

  let dataColumns = dataModalCateNews.columns;
  const tableHeaders = (
    <thead>
      <div>
        <tr>
          {dataColumns.map(function (column) {
            return <th width={column.width}>{column.title}</th>;
          })}
        </tr>
      </div>
    </thead>
  );

  const reorder = (list, startIndex, endIndex) => {
    let result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };

  const onDragEnd = (result, provided) => {
    console.log(result);
    // // dropped outside the list
    if (!result.destination) {
      return;
    }
    let items = reorder(
      dataModalCateNews.rows,
      result.source.index,
      result.destination.index
    );
    items.map((item, index) => {
      item.stt = index + 1;
    });
    changleDataModalCateNews({ ...dataModalCateNews, rows: items });
  };

  async function _APIpostUpdateSortCate(obj) {
    const data = await APIService._postUpdateSortCate(obj);
    await handleLoading(false);
    message.success("Cập nhập thành công.");
    await callbackReloadSort();
    await callback();
    // this.props.callback1();

    try {
    } catch (err) {
      console.log(err);
      handleLoading(false);
    }
  }

  return (
    <div>
      <div>
        <ITitle
          level={4}
          title="Sắp xếp thứ tự hiển thị chuyên mục"
          style={{ fontWeight: "bold", marginBottom: 20 }}
        />
        <div
          style={{
            height: 470,
            border: "1px solid rgba(122, 123, 123, 0.5)",
            width: "100%",
            overflow: "scroll",
          }}
        >
          <Row>
            <Col>
              <DragDropContext
                onDragEnd={(result, provided) => onDragEnd(result, provided)}
              >
                <Droppable droppableId="droppable">
                  {(droppableProvided, droppableSnapshot) => (
                    <div ref={droppableProvided.innerRef}>
                      <table
                        className="table table-bordered table-hover"
                        width="100%"
                      >
                        {tableHeaders}

                        {dataRows1.map((row, index) => {
                          return (
                            <Draggable
                              key={row.id}
                              draggableId={row.code_category}
                              index={index}
                            >
                              {(draggableProvided, draggableSnapshot) => (
                                <div
                                  className="table-edit"
                                  ref={draggableProvided.innerRef}
                                  {...draggableProvided.draggableProps}
                                  {...draggableProvided.dragHandleProps}
                                >
                                  <tr>
                                    {dataColumns.map(function (column) {
                                      return (
                                        <td width={column.width}>
                                          {row[column.key]}
                                        </td>
                                      );
                                    })}
                                  </tr>
                                </div>
                              )}
                            </Draggable>
                          );
                        })}
                        {droppableProvided.placeholder}
                      </table>
                    </div>
                  )}
                </Droppable>
              </DragDropContext>
            </Col>
          </Row>
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-end",
          position: "absolute",
          bottom: -70,
          right: 0,
        }}
      >
        <Button
          type="primary"
          style={{
            flexDirection: "row",
            display: "flex",
            minWidth: 160,
            paddingLeft: 30,
            paddingRight: 30,
            height: 42,
            borderRadius: 0,
            borderColor: colors.main,
            borderWidth: 1,
            background: colors.white,
            justifyContent: "center",
            alignItems: "center",
          }}
          loading={isLoadingSave}
          onClick={() => {
            handleLoading(true);
            let idArray = dataModalCateNews.rows.map((item, index) => {
              return item.id;
            });
            let objStatusNews = {
              type: type,
              id: idArray,
            };
            // this._APIpostStatusPrioritizeNews(
            //   this.props.type == "NEWS" ? objStatusNews : objStatusVdieo
            // );
            _APIpostUpdateSortCate(objStatusNews);
          }}
        >
          <div style={{ marginRight: 30 }}>
            <ISvg
              name={ISvg.NAME.SAVE}
              width={20}
              height={20}
              fill={colors.main}
            />
          </div>
          <span
            style={{
              color: colors.main,
              fontSize: 14,
              fontWeight: 500,
              marginLeft: 10,
            }}
          >
            Lưu
          </span>
        </Button>
        <Button
          type="primary"
          style={{
            flexDirection: "row",
            display: "flex",
            minWidth: 160,
            paddingLeft: 30,
            paddingRight: 30,
            height: 42,
            borderRadius: 0,
            borderColor: "#E35F4B",
            borderWidth: 1,
            background: colors.white,
            marginLeft: 12,
            justifyContent: "center",
            alignItems: "center",
          }}
          onClick={async () => {
            const dataBefore = [...dataRowsNews];
            dataBefore.map((item, index) => {
              item.stt = index + 1;
            });
            await changleDataModalCateNews({
              ...dataModalCateNews,
              rows: dataBefore,
            });
            await callback();
          }}
        >
          <div style={{ marginRight: 30 }}>
            <ISvg
              name={ISvg.NAME.CROSS}
              width={20}
              height={20}
              fill={"#E35F4B"}
            />
          </div>
          <span
            style={{
              color: "#E35F4B",
              fontSize: 14,
              fontWeight: 500,
              marginLeft: 10,
            }}
          >
            Hủy bỏ
          </span>
        </Button>
      </div>
    </div>
  );
}
