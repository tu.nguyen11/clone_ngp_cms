import { Table, Typography, Avatar, Button, message, Modal } from "antd";
import React, { Component } from "react";
import { ISvg, IButton, ITitle, ITable } from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import ModalPrioritizeNews from "./ModalPrioritizeNews";
import FormatterDay from "../../../utils/FormatterDay";
import { APIService } from "../../../services";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

export default class NewsPrioritizePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTableNews: [],
      dataTableVideo: [],
      dataIDNEWS: [],
      dataIDVIDEO: [],
      loadingTableNews: true,
      loadingTableVideo: true,
      visible: false,
      page: 1,
      visible1: false,
    };
    this.showModal = this.showModal.bind(this);
  }

  _APIgetListPrioritizeNews = async (
    type,
    dataTable,
    loadingTable,
    dataIDTYPE
  ) => {
    try {
      const data = await APIService._getListPrioritizeNews(type);
      data.news_prioritize.map((item, index) => {
        item.stt = index + 1;
      });
      let dataID = data.news_prioritize.map((item, index) => {
        return item.id;
      });
      this.setState({
        [dataTable]: data.news_prioritize,
        [dataIDTYPE]: dataID,
        [loadingTable]: false,
      });
      console.log({
        dataTable: data.news_prioritize,
        dataIDTYPE: dataID,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        [loadingTable]: false,
      });
    }
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel1 = (e) => {
    console.log(e);
    this.setState({
      visible1: false,
    });
  };

  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }
  componentDidMount() {
    this._APIgetListPrioritizeNews(
      "NEWS",
      "dataTableNews",
      "loadingTableNews",
      "dataIDNEWS"
    );
    this._APIgetListPrioritizeNews(
      "VIDEO",
      "dataTableVideo",
      "loadingTableVideo",
      "dataIDVIDEO"
    );
  }

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];

    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Ngày tạo"),
        dataIndex: "post_time",
        key: "post_time",
        align: "left",
        width: 150,

        render: (post_time) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(post_time, "#DD#/#MM#/#YYYY#")
          ),
      },
      {
        title: _renderTitle("Mã tin tức"),
        dataIndex: "code",
        key: "code",
        align: "left",
        width: 120,

        render: (code) => _renderColumns(code),
      },
      {
        title: _renderTitle("Chuyên mục"),
        dataIndex: "cate_name",
        key: "cate_name",
        align: "left",
        width: 100,
        render: (cate_name) => _renderColumns(cate_name),
      },
      {
        title: _renderTitle("Tiêu đề"),
        dataIndex: "title",
        key: "title",
        align: "left",
        width: 160,
        render: (title) => _renderColumns(title),
      },
      {
        title: _renderTitle("Nội dung"),
        dataIndex: "summary_content",
        key: "summary_content",
        align: "left",

        render: (summary_content) => (
          <span
            style={{
              wordWrap: "break-word",
              wordBreak: "break-word",
              overflow: "hidden",
            }}
            className="ellipsis-text"
          >
            {summary_content}
          </span>
        ),
      },
      {
        title: _renderTitle("Thời gian đăng"),
        dataIndex: "post_time",
        key: "post_time",
        align: "left",
        width: 100,

        render: (post_time) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              post_time,
              "#DD#-#MM#-#YYYY# #hh#:#mm#:#ss#"
            )
          ),
      },
      {
        title: _renderTitle("Phân loại"),
        dataIndex: "type_name",
        key: "type_name",
        align: "left",
        width: 80,

        render: (type_name) => _renderColumns(type_name),
      },
      {
        title: _renderTitle("Trạng thái"),
        dataIndex: "status_name",
        key: "status_name",
        align: "left",
        width: 120,

        render: (status_name) => _renderColumns(status_name),
      },

      {
        title: "",
        dataIndex: "id",
        key: "id",
        align: "center",
        width: 80,
        fixed: widthScreen <= 1400 ? "right" : "",

        render: (id) => (
          <Row>
            <Col
              onClick={(e) => {
                e.stopPropagation();
                this.props.history.push("/newsDetail/" + id);
              }}
              className="cursor"
            >
              <ISvg
                name={ISvg.NAME.CHEVRONRIGHT}
                width={11}
                height={20}
                fill={colors.icon.default}
              />
            </Col>
          </Row>
        ),
      },
    ];

    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Tin tức ưu tiên"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
        </Row>
        <Row className="mt-4">
          <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
            <Row className="m-0 p-0 center">
              <ITitle
                title="TIN TỨC BÀI VIÊT"
                level={4}
                style={{ fontWeight: "bold" }}
              />
            </Row>
          </Row>
          <Row className="m-0 p-0">
            <IButton
              icon={ISvg.NAME.WRITE}
              title="Chỉnh sửa"
              color={colors.main}
              onClick={() => this.showModal()}
            />
          </Row>
        </Row>

        <Row className="mt-4">
          <ITable
            data={this.state.dataTableNews}
            columns={columns}
            loading={this.state.loadingTableNews}
            style={{ width: "100%", background: "white" }}
            hidePage={true}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );
                  const id = this.state.dataTableNews[indexRow].id;
                  this.props.history.push("/newsDetail/" + id);
                }, // click row
              };
            }}
            // scroll={{ x: 1170 }}

            // scroll={{ x: this.state.width }}
          />
        </Row>
        <Row className="mt-4">
          <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
            <Row className="m-0 p-0 center">
              <ITitle
                title="TIN TỨC VIDEO"
                level={4}
                style={{ fontWeight: "bold" }}
              />
            </Row>
          </Row>
          <Row className="m-0 p-0">
            <IButton
              icon={ISvg.NAME.WRITE}
              title="Chỉnh sửa"
              color={colors.main}
              onClick={() =>
                this.setState({
                  visible1: true,
                })
              }
            />
          </Row>
        </Row>

        <Row className="mt-4">
          <ITable
            data={this.state.dataTableVideo}
            columns={columns}
            loading={this.state.loadingTableVideo}
            style={{ width: "100%", background: "white" }}
            hidePage={true}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );
                  const id = this.state.dataTableVideo[indexRow].id;
                  this.props.history.push("/newsDetail/" + id);
                }, // click row
              };
            }}
            // scroll={{ x: 1170 }}

            // scroll={{ x: this.state.width }}
          />
        </Row>
        <Modal
          visible={this.state.visible}
          footer={null}
          width={1200}
          onCancel={this.handleCancel}
          closable={false}
          maskClosable={false}
        >
          <ModalPrioritizeNews
            dataSource={this.state.dataIDNEWS}
            dataRows={this.state.dataTableNews}
            callback={this.handleCancel}
            type="NEWS"
            callback1={() =>
              this._APIgetListPrioritizeNews(
                "NEWS",
                "dataTableNews",
                "loadingTableNews",
                "dataIDNEWS"
              )
            }
          />
        </Modal>
        <Modal
          visible={this.state.visible1}
          footer={null}
          width={1200}
          onCancel={this.handleCancel1}
          closable={false}
          maskClosable={false}
        >
          <ModalPrioritizeNews
            dataSource={this.state.dataIDVIDEO}
            dataRows={this.state.dataTableVideo}
            callback={this.handleCancel1}
            type="VIDEO"
            callback1={() =>
              this._APIgetListPrioritizeNews(
                "VIDEO",
                "dataTableVideo",
                "loadingTableVideo",
                "dataIDVIDEO"
              )
            }
          />
        </Modal>
      </Container>
    );
  }
}
