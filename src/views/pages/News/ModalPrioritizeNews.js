import {
  Row,
  Col,
  List,
  Checkbox,
  Input,
  Button,
  notification,
  message,
} from "antd";
import React, { Component } from "react";
import { ITitle, ISvg } from "../../components";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { colors } from "../../../assets";
import InfiniteScroll from "react-infinite-scroller";
import { APIService } from "../../../services";

const { Search } = Input;

const reorder = (list, startIndex, endIndex) => {
  let result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const openNotification = () => {
  notification.error({
    message: "Thông báo",
    description:
      "Bạn chưa chọn tin tức hoặc bạn đã chọn tối đa 5 tin tức . Mời bạn chọn lại.",
    onClick: () => {
      console.log("Notification Clicked!");
    },
  });
};

export default class ModalPrioritizeNews extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isLoadingSave: false, //
      type: this.props.type,
      page: 1, //
      dataList: [], //
      key: "", //
      dataSource: [], //
      dataTable: {
        columns: [
          { title: "STT", key: "stt", width: 60 },
          { title: "Mã tin tức", key: "code", width: 120 },
          { title: "Chuyên mục", key: "cate_name", width: 200 },
          { title: "Tiêu đề", key: "title", width: 320 },
          { title: "", key: "id", width: 60 },
        ],
        rows: [...this.props.dataRows],
        loadingList: true,
        dataListBefore: [],
      }, //
      dataRows: [], //
    };
    this.handleInfiniteOnLoad = this.handleInfiniteOnLoad.bind(this);
  }
  onDragEnd = (result) => {
    console.log(result);
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    let items = reorder(
      this.state.dataTable.rows,
      result.source.index,
      result.destination.index
    );
    items.map((item, index) => {
      item.stt = index + 1;
    });
    this.state.dataTable.rows = items;
    this.setState({
      dataTable: this.state.dataTable,
    });
  };

  _APIpostStatusPrioritizeNews = async (obj) => {
    const data = await APIService._postStatusPrioritizeNews(obj);
    this.setState(
      {
        isLoadingSave: false,
      },
      () => {
        message.success("Cập nhập thành công.");
        this.props.callback();
        this.props.callback1();
      }
    );
    try {
    } catch (err) {
      console.log(err);
      this.setState({
        isLoadingSave: false,
      });
    }
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    console.warn("hook", nextProps, prevState);
    if (
      nextProps.dataSource !== prevState.dataSource ||
      nextProps.dataRows !== prevState.dataRows
    ) {
      return {
        dataSource: nextProps.dataSource,
        dataRows: nextProps.dataRows,
      };
    } else return null;
  }

  _APIgetAllNewsPrioritize = async (type, key, page) => {
    try {
      const data = await APIService._getSelectListPrioritizeNews(
        type,
        key,
        page
      );

      data.news.map((item, index) => {
        if (this.state.dataSource.indexOf(Number(item.code)) != -1) {
          item.active = true;
          return;
        }
        return (item.active = false);
      });

      const dataNew = data.news;
      let dataAfter = JSON.parse(JSON.stringify(dataNew));
      let dataBefor = JSON.parse(JSON.stringify(dataNew));
      this.setState({
        dataList: dataAfter,
        dataListBefore: dataBefor,
      });
    } catch (err) {
      this.setState({
        loadingList: false,
      });
    }
  };

  handleInfiniteOnLoad = async (e) => {
    // const element = e.target;
    let page = this.state.page + 1;
    const data = await APIService._getSelectListPrioritizeNews(
      this.state.type,
      this.state.key,
      page
    );
    data.news.map((item, index) => {
      if (this.state.dataSource.indexOf(Number(item.code)) != -1) {
        item.active = true;
        return;
      }
      return (item.active = false);
    });
    let dataNew = this.state.dataList.concat(data.news);
    this.setState({
      page: page,
      dataList: dataNew,
    });
  };

  async componentDidMount() {
    await this._APIgetAllNewsPrioritize(
      this.state.type,
      this.state.key,
      this.state.page
    );
    // this.state.dataList.map((item, index) => {
    //   if (item.active) {
    //     this.state.dataTable.rows.push(item);
    //   }
    // });

    this.state.dataRows.map((item, index) => {
      item.stt = index + 1;
    });
    // this.state.dataTable.rows = [...this.state.dataRows];
    this.setState({
      dataTable: this.state.dataTable,
    });
  }

  handleRemove = (id) => {
    let index = this.state.dataTable.rows
      .map((item, index) => {
        return item.id;
      })
      .indexOf(id);
    this.state.dataTable.rows.splice(index, 1);
    this.state.dataList.map((item, index) => {
      if (item.id == id) {
        item.active = false;
      }
    });
    this.setState({
      dataTable: this.state.dataTable,
      dataList: this.state.dataList,
    });
  };

  render() {
    let dataRows = this.state.dataTable.rows;
    let dataColumns = this.state.dataTable.columns;
    var tableHeaders = (
      <thead>
        <div>
          <tr>
            {dataColumns.map(function (column) {
              return <th width={column.width}>{column.title}</th>;
            })}
          </tr>
        </div>
      </thead>
    );

    return (
      <div>
        <div style={{ marginBottom: 26 }}>
          <ITitle
            title={
              this.props.type == "NEWS"
                ? "Chỉnh sửa bài viết ưu tiên"
                : "Chỉnh sửa video ưu tiên"
            }
            level={4}
            style={{ fontWeight: "bold" }}
          />
        </div>
        <div style={{ width: 1150, background: "white" }}>
          <Row gutter={[24, 12]}>
            <Col span={8}>
              <Row>
                <Search
                  placeholder="tìm sản phẩm"
                  onSearch={(value) => {
                    this.setState(
                      {
                        key: value,
                      },
                      () =>
                        this._APIgetAllNewsPrioritize(
                          this.state.type,
                          this.state.key,
                          this.state.page
                        )
                    );
                  }}
                  style={{ marginBottom: 12 }}
                />
                <div
                  style={{
                    height: 350,
                    overflow: "auto",
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}
                >
                  <InfiniteScroll
                    initialLoad={false}
                    pageStart={0}
                    loadMore={this.handleInfiniteOnLoad}
                    hasMore={true}
                    useWindow={false}
                  >
                    <List
                      dataSource={this.state.dataList}
                      bordered={false}
                      renderItem={(item, index) => (
                        <List.Item>
                          <Checkbox
                            // value={{ ...item, ...{ stt: index } }}
                            checked={item.active}
                            style={{ marginRight: 35 }}
                            defaultChecked={item.active}
                            onChange={(e) => {
                              this.state.dataList[index].active =
                                e.target.checked;
                              this.setState({
                                dataList: this.state.dataList.slice(0),
                              });
                            }}
                          />
                          <ITitle level={4} title={item.title} />
                        </List.Item>
                      )}
                      // columns={columnsLeft}
                      // bodyStyle={{ height: 400 }}
                      // rowSelection={rowSelection}
                    />
                  </InfiniteScroll>
                </div>
              </Row>

              <div
                style={{
                  flex: 1,
                  display: "flex",
                  justifyContent: "flex-end",
                  marginTop: 30,
                }}
              >
                <Button
                  type="primary"
                  style={{
                    flexDirection: "row",
                    display: "flex",
                    minWidth: 160,
                    paddingLeft: 30,
                    paddingRight: 30,
                    height: 42,
                    borderRadius: 0,
                    borderColor: colors.main,
                    borderWidth: 1,
                    background: colors.white,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  onClick={() => {
                    let dataCheck = this.state.dataList;
                    let sumCheck = 0;
                    dataCheck.map((item, index) => {
                      if (item.active) {
                        sumCheck = sumCheck + 1;
                      }
                    });
                    if (sumCheck > 5 || sumCheck == 0) {
                      openNotification();
                      return;
                    } else {
                      let dataNew = [];
                      dataCheck.map((item, index) => {
                        if (item.active) {
                          dataNew.push(item);
                        }
                      });
                      dataNew.map((item, index) => {
                        item.stt = index + 1;
                      });
                      this.state.dataTable.rows = dataNew;
                      this.setState({
                        dataTable: this.state.dataTable,
                      });
                      return;
                    }
                  }}
                >
                  <span
                    style={{
                      color: colors.main,
                      fontSize: 14,
                      fontWeight: 500,
                      marginLeft: 10,
                    }}
                  >
                    Thêm
                  </span>
                  <div style={{ marginLeft: 30 }}>
                    <ISvg
                      name={ISvg.NAME.BUTTONRIGHT}
                      width={20}
                      height={20}
                      fill={colors.main}
                    />
                  </div>
                </Button>
              </div>
            </Col>
            <Col span={16}>
              <div
                style={{
                  height: 470,
                  border: "1px solid rgba(122, 123, 123, 0.5)",
                }}
              >
                <DragDropContext onDragEnd={this.onDragEnd}>
                  <Droppable droppableId="droppable">
                    {(droppableProvided, droppableSnapshot) => (
                      <div ref={droppableProvided.innerRef}>
                        <table
                          className="table table-bordered table-hover"
                          width="100%"
                        >
                          {tableHeaders}

                          {dataRows.map((row, index) => {
                            let handleRemove = this.handleRemove.bind(this);

                            return (
                              <Draggable
                                key={row.id}
                                draggableId={row.code}
                                index={index}
                              >
                                {(draggableProvided, draggableSnapshot) => (
                                  <div
                                    className="table-edit"
                                    ref={draggableProvided.innerRef}
                                    {...draggableProvided.draggableProps}
                                    {...draggableProvided.dragHandleProps}
                                  >
                                    <tr>
                                      {dataColumns.map(function (column) {
                                        return column.key == "id" ? (
                                          <td width={column.width}>
                                            <div
                                              style={{
                                                width: 20,
                                                height: 20,
                                                borderRadius: 10,
                                                display: "flex",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                background: colors.gray._300,
                                              }}
                                              onClick={() => {
                                                handleRemove(row[column.key]);
                                              }}
                                            >
                                              <ISvg
                                                name={ISvg.NAME.CROSS}
                                                width={7}
                                                height={7}
                                                fill={colors.oranges}
                                              />
                                            </div>
                                          </td>
                                        ) : (
                                          <td width={column.width}>
                                            {row[column.key]}
                                          </td>
                                        );
                                      })}
                                    </tr>
                                  </div>
                                )}
                              </Draggable>
                            );
                          })}
                          {droppableProvided.placeholder}
                        </table>
                      </div>
                    )}
                  </Droppable>
                </DragDropContext>
              </div>
            </Col>
          </Row>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            position: "absolute",
            bottom: -60,
            right: 0,
          }}
        >
          <Button
            type="primary"
            style={{
              flexDirection: "row",
              display: "flex",
              minWidth: 160,
              paddingLeft: 30,
              paddingRight: 30,
              height: 42,
              borderRadius: 0,
              borderColor: colors.main,
              borderWidth: 1,
              background: colors.white,
              justifyContent: "center",
              alignItems: "center",
            }}
            loading={this.state.isLoadingSave}
            onClick={() => {
              this.setState({
                isLoadingSave: true,
              });
              let idArray = this.state.dataTable.rows.map((item, index) => {
                return item.id;
              });
              let objStatusNews = {
                type: "NEWS",
                id: idArray,
              };
              let objStatusVdieo = {
                type: "VIDEO",
                id: idArray,
              };
              this._APIpostStatusPrioritizeNews(
                this.props.type == "NEWS" ? objStatusNews : objStatusVdieo
              );
            }}
          >
            <div style={{ marginRight: 30 }}>
              <ISvg
                name={ISvg.NAME.SAVE}
                width={20}
                height={20}
                fill={colors.main}
              />
            </div>
            <span
              style={{
                color: colors.main,
                fontSize: 14,
                fontWeight: 500,
                marginLeft: 10,
              }}
            >
              Lưu
            </span>
          </Button>
          <Button
            type="primary"
            style={{
              flexDirection: "row",
              display: "flex",
              minWidth: 160,
              paddingLeft: 30,
              paddingRight: 30,
              height: 42,
              borderRadius: 0,
              borderColor: "#E35F4B",
              borderWidth: 1,
              background: colors.white,
              marginLeft: 12,
              justifyContent: "center",
              alignItems: "center",
            }}
            onClick={async () => {
              await this.props.callback();
              this.state.dataTable.rows = [...this.props.dataRows];
              this.setState({
                dataTable: this.state.dataTable,
                dataList: this.state.dataListBefore,
              });
            }}
          >
            <div style={{ marginRight: 30 }}>
              <ISvg
                name={ISvg.NAME.CROSS}
                width={20}
                height={20}
                fill={"#E35F4B"}
              />
            </div>
            <span
              style={{
                color: "#E35F4B",
                fontSize: 14,
                fontWeight: 500,
                marginLeft: 10,
              }}
            >
              Hủy bỏ
            </span>
          </Button>
        </div>
      </div>
    );
  }
}
