import React, { Component } from 'react'
import { Row, Col, Form, message, Skeleton } from 'antd'
import { ITitle, ISvg, IButton, ISelect, IInput } from '../../components'
import { colors } from '../../../assets'

import PreviewNews from './PreviewNews'
import { APIService } from '../../../services'

class CreateNewsCategories extends Component {
  constructor (props) {
    super(props)
    this.typeParams = this.props.match.params.type
    this.idParams = Number(this.props.match.params.id)
    this.boolean = this.typeParams == 'add' && this.idParams == 0
    this.onSubmit = this.onSubmit.bind(this)
    this.state = {
      isLoadingButton: false,
      idCate: 0,
      isSkeleton: !this.boolean
    }
  }

  onSubmit = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (err) {
        return
      }
      if (this.boolean) {
        const objAdd = {
          name: values.nameCategories,
          type_code: values.type,
          description: values.mota,
          status: values.status
        }

        this.setState(
          {
            isLoadingButton: true
          },
          () => this._APIpostAddCateNews(objAdd)
        )
      } else {
        const objUpdate = {
          id: this.state.idCate,
          name: values.nameCategories,
          type_code: values.type,
          description: values.mota,
          status: values.status
        }
        this.setState(
          {
            isLoadingButton: true
          },
          () => this._APIpostUpdateCateNews(objUpdate)
        )
      }
    })
  }

  componentDidMount () {
    if (!this.boolean) {
      this._APIgetDetailCateNews(this.idParams)
    }
  }

  _APIpostAddCateNews = async obj => {
    try {
      const data = await APIService._postAddCateNews(obj)
      this.setState(
        {
          isLoadingButton: false
        },
        () => {
          message.success('Tạo chuyên mục thành công.')
          let type = this.props.form.getFieldValue('type')
          this.props.history.push('/new/categories/' + type + '/page')
        }
      )
    } catch (err) {
      console.log(err)
      this.setState({
        isLoadingButton: false
      })
    }
  }

  _APIpostUpdateCateNews = async obj => {
    try {
      const data = await APIService._postUpdateCateNews(obj)
      this.setState(
        {
          isLoadingButton: false
        },
        () => {
          message.success('Chỉnh sửa chuyên mục thành công.')
          this.props.history.push('/new/categories/detail/' + this.state.idCate)
        }
      )
    } catch (err) {
      console.log(err)
      this.setState({
        isLoadingButton: false
      })
    }
  }

  _APIgetDetailCateNews = async id => {
    try {
      const data = await APIService._getDetailCateNews(id)
      this.setState({
        isSkeleton: false,
        idCate: data.cate.id
      })
      this.props.form.setFieldsValue({
        nameCategories: data.cate.name,
        type: data.cate.type_code,
        mota: data.cate.description,
        status: data.cate.status
      })
    } catch (err) {
      console.log(err)
      this.setState({
        isSkeleton: false
      })
    }
  }

  render () {
    const { getFieldDecorator } = this.props.form
    const reduxForm = this.props.form

    let regexString =
      '^[a-zA-Z0-9\\sàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐ]*$'
    return (
      <div style={{ width: '100%' }}>
        <Row>
          <ITitle
            level={1}
            title={this.boolean ? 'Tạo chuyên mục mới' : 'Chỉnh sửa chuyên mục'}
            style={{
              color: colors.mainDark,
              fontWeight: 'bold',
              flex: 1,
              display: 'flex',
              alignItems: 'flex-end'
            }}
          />
        </Row>
        <Form
          onSubmit={this.onSubmit}
          labelCol={{
            sm: { span: 0, offset: 0 },
            span: { span: 0, offset: 0 }
          }}
        >
          <Row
            style={{
              marginTop: 20,
              width: '100%',
              display: 'flex'
            }}
          >
            <Col
              style={{
                display: 'flex',
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'flex-end'
              }}
            >
              <IButton
                icon={ISvg.NAME.SAVE}
                title={'Lưu'}
                loading={this.state.isLoadingButton}
                color={colors.main}
                style={{ marginRight: 20 }}
                htmlType='submit'
              />
              <IButton
                icon={ISvg.NAME.CROSS}
                title='Hủy'
                onClick={() => {
                  this.props.history.goBack()
                }}
                color={colors.oranges}
              />
            </Col>
          </Row>
          <Row style={{ marginTop: 38 }}>
            <div style={{ width: 555, background: 'white' }} className='shadow'>
              <Row>
                <Col
                  span={24}
                  style={{
                    height: '100%',
                    paddingTop: 30,
                    paddingBottom: 30,
                    paddingLeft: 40,
                    paddingRight: 40
                  }}
                >
                  <Skeleton
                    active
                    loading={this.state.isSkeleton}
                    paragraph={{ rows: 6 }}
                  >
                    <Row gutter={[0, 20]}>
                      <Col style={{ marginTop: 6, marginBottom: 24 }}>
                        <ITitle
                          level={3}
                          title='Thông tin chuyên mục'
                          style={{ fontWeight: 'bold' }}
                        />
                      </Col>
                      <Form.Item>
                        {getFieldDecorator('nameCategories', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              type: 'string',
                              required: true,
                              message:
                                'không để trống , chỉ 30 kí tự và không nhập kí tự đặc biệt',
                              max: 30,
                              pattern: new RegExp(regexString)
                            }
                          ]
                        })(
                          <div>
                            <p>Tên chuyên mục</p>
                            <IInput
                              loai='input'
                              value={reduxForm.getFieldValue('nameCategories')}
                              placeholder='nhập tên gọi cho chuyên mục'
                            />
                          </div>
                        )}
                      </Form.Item>

                      <Form.Item>
                        {getFieldDecorator('type', {
                          rules: [
                            {
                              required: true,
                              message: 'chọn loại chuyên mục'
                            }
                          ]
                        })(
                          <div>
                            <p>Phân loại</p>
                            {this.boolean ? (
                              <ISelect
                                isBackground={false}
                                placeholder='chọn loại chuyên mục'
                                select={true}
                                onChange={key => {
                                  reduxForm.setFieldsValue({
                                    type: key
                                  })
                                }}
                                data={[
                                  {
                                    key: 'NEWS',
                                    value: 'Bài viết'
                                  },
                                  { key: 'VIDEO', value: 'Video' }
                                ]}
                              />
                            ) : (
                              <ISelect
                                isBackground={false}
                                placeholder='chọn loại chuyên mục'
                                value={reduxForm.getFieldValue('type')}
                                select={false}
                                onChange={key => {
                                  reduxForm.setFieldsValue({
                                    type: key
                                  })
                                }}
                                data={[
                                  {
                                    key: 'NEWS',
                                    value: 'Bài viết'
                                  },
                                  { key: 'VIDEO', value: 'Video' }
                                ]}
                              />
                            )}
                          </div>
                        )}
                      </Form.Item>

                      <Form.Item>
                        {getFieldDecorator('status', {
                          rules: [
                            {
                              required: true,
                              message: 'chọn trạng thái'
                            }
                          ]
                        })(
                          <div>
                            <p>Trạng thái</p>
                            <ISelect
                              isBackground={false}
                              placeholder='chọn trạng thái'
                              select={true}
                              value={reduxForm.getFieldValue('status')}
                              onChange={key => {
                                reduxForm.setFieldsValue({
                                  status: key
                                })
                              }}
                              data={[
                                { key: 0, value: 'Ẩn' },
                                { key: 1, value: 'Hiện' }
                              ]}
                            />
                          </div>
                        )}
                      </Form.Item>

                      <Form.Item>
                        {getFieldDecorator('mota', {
                          rules: [
                            {
                              required: true,
                              message: 'nhập mô tả cho chuyên mục'
                            }
                          ]
                        })(
                          <div>
                            <p>Mô tả</p>
                            <textarea
                              value={reduxForm.getFieldValue('mota')}
                              placeholder='nhập mô tả cho chuyên mục...'
                              style={{
                                height: 330,
                                width: '100%',
                                padding: 20,
                                border: '1px solid rgba(122, 123, 123, 0.5)',
                                marginTop: 6,
                                borderRadius: 2
                              }}
                            />
                          </div>
                        )}
                      </Form.Item>
                    </Row>
                  </Skeleton>
                </Col>
              </Row>
            </div>
          </Row>
        </Form>
      </div>
    )
  }
}

const createNewsCategories = Form.create({ name: 'CreateNewsCategories' })(
  CreateNewsCategories
)

export default createNewsCategories
