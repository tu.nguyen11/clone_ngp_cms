import { message, Popconfirm, Modal } from "antd";
import React, { Component } from "react";
import { ISvg, ISearch, IButton, ITitle, ITable } from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import ModalCateNews from "./ModalCateNews";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

export default class NewCategoriesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      dataTable: [],
      loadingTable: true,
      keySearch: "",
      page: 1,
      type: this.props.match.params.type,
      arrayStatus: [],
      isLoadingRemove: false,
      isLoadingShow: false,
      isLoadingHiden: false,
      visible: false,
      dataSort: [],
    };
  }

  componentDidMount() {
    this._APIgetListCateNews(
      this.state.keySearch,
      this.state.page,
      this.state.type
    );
    this._APIgetListSortCategoryPriorityNews(this.state.type);
  }

  componentDidUpdate() {
    if (this.props.match.params.type != this.state.type) {
      this.setState(
        {
          type: this.props.match.params.type,
          loadingTable: true,
        },
        () => {
          this._APIgetListCateNews(
            this.state.keySearch,
            this.state.page,
            this.state.type
          );
          this._APIgetListSortCategoryPriorityNews(this.state.type);
        }
      );
    }
  }

  _APIpostRemoveCateNews = async (obj) => {
    try {
      const data = await APIService._postRemoveCateNews(obj);
      message.success("Xóa thành công");
      this.setState(
        {
          loadingTable: true,
          arrayStatus: [],
          selectedRowKeys: [],
          isLoadingRemove: false,
        },
        () =>
          this._APIgetListCateNews(
            this.state.keySearch,
            this.state.page,
            this.state.type
          )
      );
    } catch (err) {
      this.setState({
        loadingTable: false,
        isLoadingRemove: false,
      });
    }
  };

  _APIgetListCateNews = async (key, page, type) => {
    try {
      const data = await APIService._getListCateNews(key, page, type);

      data.cate.map((item, index) => {
        item.stt = (this.state.page - 1) * 10 + index + 1;
      });

      this.setState({
        dataTable: data.cate,
        loadingTable: false,
        sizeTable: data.size,
        totalTable: data.total,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[item].id);
    });
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ arrayStatus: arrayID, selectedRowKeys });
  };

  _APIpostUpdateSatatusCateNew = async (obj, title, loading) => {
    try {
      const data = await APIService._postUpdateSatatusCateNew(obj);
      message.success(title);
      this.setState(
        {
          loadingTable: true,
          arrayStatus: [],
          selectedRowKeys: [],
          [loading]: false,
        },
        () =>
          this._APIgetListCateNews(
            this.state.keySearch,
            this.state.page,
            this.state.type
          )
      );
    } catch (err) {
      this.setState({
        loadingTable: false,
        [loading]: false,
      });
    }
  };

  _APIgetListSortCategoryPriorityNews = async (type) => {
    try {
      const data = await APIService._getListSortCategoryPriorityNews(type);
      data.cate.map((item, index) => {
        item.stt = index + 1;
        item.code_category = `${item.code_category}`;
      });

      this.setState({
        dataSort: data.cate,
      });
    } catch (err) {
      console.log(err);
    }
  };

  _handleCloseModal = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Ngày tạo"),
        dataIndex: "create_time",
        key: "create_time",
        align: "left",
        width: 150,

        render: (create_time) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(create_time, "#DD#/#MM#/#YYYY#")
          ),
      },
      {
        title: _renderTitle("Mã chuyên mục"),
        dataIndex: "code",
        key: "code",
        align: "left",
        width: 150,

        render: (code) => _renderColumns(code),
      },
      {
        title: _renderTitle(" Tên chuyên mục"),
        dataIndex: "name",
        key: "name",
        align: "left",

        render: (name) => _renderColumns(name),
      },
      {
        title: _renderTitle("Mô tả"),
        dataIndex: "description",
        key: "description",
        align: "left",

        render: (description) => (
          <span
            style={{
              wordWrap: "break-word",
              wordBreak: "break-word",
              overflow: "hidden",
            }}
            className="ellipsis-text"
          >
            {description}
          </span>
        ),
      },
      {
        title: _renderTitle("Trạng thái"),
        dataIndex: "status",
        key: "status",
        align: "left",
        width: 120,

        render: (status) => (
          <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
            <ITitle level={4} title={status == 1 ? "Hiện" : "Ẩn"} />
          </div>
        ),
      },
      {
        title: _renderTitle("Số bài tin tức"),
        dataIndex: "total_news",
        key: "total_news",
        align: "left",
        width: 120,
        render: (total_news) => _renderColumns(total_news),
      },

      {
        title: "",

        align: "right",
        width: 100,
        fixed: widthScreen <= 1400 ? "right" : "",

        render: (obj) =>
          obj.code == 0 ? null : (
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div
                onClick={(e) => {
                  e.stopPropagation();

                  this.props.history.push("/new/edit/categories/" + obj.id);
                }} // idAndStt.index vị trí của mảng
                className="cell"
                style={{ width: 50 }}
              >
                <ISvg
                  name={ISvg.NAME.WRITE}
                  width={20}
                  height={20}
                  fill={colors.icon.default}
                />
              </div>
              <div
                className="cursor"
                style={{ width: 50 }}
                onClick={(e) => {
                  e.stopPropagation();
                  this.props.history.push("/new/categories/detail/" + obj.id);
                }}
              >
                <ISvg
                  name={ISvg.NAME.CHEVRONRIGHT}
                  width={11}
                  height={20}
                  fill={colors.icon.default}
                />
              </div>
            </div>
          ),
      },
    ];

    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,

      getCheckboxProps: (record) => ({
        id: `check-${record.id}`,
      }),
      // getCheckboxProps: (record) => {
      //
      //   return {
      //     disabled: record.id === 0, // Column configuration not to be checked
      //     name: record.name,
      //   };
      // },
    };
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title={
              this.props.match.params.type == "VIDEO"
                ? "Chuyên mục video"
                : "Chuyên mục bài viết"
            }
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <ISearch
            placeholder=""
            onChange={(e) =>
              this.setState({
                keySearch: e.target.value,
              })
            }
            onPressEnter={() =>
              this.setState(
                {
                  loadingTable: true,
                  page: 1,
                },
                () =>
                  this._APIgetListCateNews(
                    this.state.keySearch,
                    this.state.page,
                    this.state.type
                  )
              )
            }
            icon={
              <div
                style={{
                  display: "flex",
                  width: 42,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                onClick={() =>
                  this.setState(
                    {
                      loadingTable: true,
                    },
                    () =>
                      this._APIgetListCateNews(
                        this.state.keySearch,
                        this.state.page,
                        this.state.type
                      )
                  )
                }
                className="cursor"
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>
        <Row
          className="mt-4"
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
          <Row className="m-0 p-0">
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để hiện chuyên mục."}
              onConfirm={() => {
                if (this.state.arrayStatus.length == 0) {
                  message.warning("Chưa chọn chuyên mục để hiển thị.");
                  return;
                }
                this.setState(
                  {
                    isLoadingShow: true,
                  },
                  async () => {
                    const arrNew = [...this.state.arrayStatus];
                    const index = arrNew.indexOf(0);
                    if (index !== -1) arrNew.splice(index, 1);
                    let objShow = {
                      id: arrNew,
                      status: 1,
                      type: this.state.type,
                    };
                    await this._APIpostUpdateSatatusCateNew(
                      objShow,
                      "Hiện chuyên mục thành công.",
                      "isLoadingShow"
                    );
                    await this._APIgetListSortCategoryPriorityNews(
                      this.state.type
                    );
                  }
                );
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.Preview}
                title="Hiện"
                color={colors.main}
                loading={this.state.isLoadingShow}
              />
            </Popconfirm>
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để ẩn chuyên mục."}
              onConfirm={() => {
                if (this.state.arrayStatus.length == 0) {
                  message.warning("Chưa chọn chuyên mục để ẩn.");
                  return;
                }
                this.setState(
                  {
                    isLoadingHiden: true,
                  },
                  async () => {
                    const arrNew = [...this.state.arrayStatus];
                    const index = arrNew.indexOf(0);
                    if (index !== -1) arrNew.splice(index, 1);

                    let objShow = {
                      id: arrNew,
                      status: 0,
                      type: this.state.type,
                    };
                    await this._APIpostUpdateSatatusCateNew(
                      objShow,
                      "Ẩn chuyên mục thành công.",
                      "isLoadingHiden"
                    );
                    await this._APIgetListSortCategoryPriorityNews(
                      this.state.type
                    );
                  }
                );
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.Preview}
                title="Ẩn"
                style={{ marginRight: 20, marginLeft: 20 }}
                color={"#22232B"}
                loading={this.state.isLoadingHiden}
              />
            </Popconfirm>
            <IButton
              icon={ISvg.NAME.WRITE}
              title={"Sắp xếp"}
              color={colors.main}
              onClick={() =>
                this.setState({
                  visible: true,
                })
              }
            />
            <IButton
              icon={ISvg.NAME.ARROWUP}
              title={"Tạo mới"}
              color={colors.main}
              style={{ marginRight: 20, marginLeft: 20 }}
              onClick={() => this.props.history.push("/new/add/categories/0")}
            />
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để xóa chuyên mục."}
              onConfirm={() => {
                if (this.state.arrayStatus.length == 0) {
                  message.warning("Chưa chọn chuyên mục để xóa.");
                  return;
                }
                this.setState(
                  {
                    isLoadingRemove: true,
                  },
                  async () => {
                    const arrNew = [...this.state.arrayStatus];
                    const index = arrNew.indexOf(0);
                    if (index !== -1) arrNew.splice(index, 1);
                    let objRemove = {
                      id: arrNew,
                      status: -1,
                    };
                    await this._APIpostRemoveCateNews(objRemove);
                    await this._APIgetListSortCategoryPriorityNews(
                      this.state.type
                    );
                  }
                );
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                color={colors.oranges}
                loading={this.state.isLoadingRemove}
              />
            </Popconfirm>
          </Row>
        </Row>

        <Row className="mt-4">
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%" }}
            rowSelection={rowSelection}
            sizeItem={this.state.sizeTable}
            indexPage={this.state.page}
            maxpage={this.state.totalTable}
            onRow={(record, rowIndex) => {
              return rowIndex === 0
                ? null
                : {
                    onClick: (event) => {
                      const indexRow = Number(
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey
                      );
                      const id = this.state.dataTable[indexRow].id;
                      this.props.history.push("/new/categories/detail/" + id);
                    }, // click row
                  };
            }}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                  loadingTable: true,
                },
                () =>
                  this._APIgetListCateNews(
                    this.state.keySearch,
                    this.state.page,
                    this.state.type
                  )
              );
            }}
          />
        </Row>

        <Modal
          visible={this.state.visible}
          footer={null}
          width={1200}
          onCancel={this._handleCloseModal}
          closable={false}
          maskClosable={false}
        >
          <ModalCateNews
            callback={this._handleCloseModal}
            dataRows={this.state.dataSort}
            type={this.state.type}
            callbackReloadSort={() =>
              this._APIgetListSortCategoryPriorityNews(this.state.type)
            }
          />
        </Modal>
      </Container>
    );
  }
}
