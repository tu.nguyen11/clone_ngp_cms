import React, { Component } from "react";
import { Container } from "reactstrap";
import { Row, Col, Skeleton, message, Popconfirm } from "antd";
import { ITitle, ISvg, IButton } from "../../components";
import { colors } from "../../../assets";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import parse from "html-react-parser";

export default class DetailNews extends Component {
  constructor(props, nextProps) {
    super(props, nextProps);
    this.state = {
      dataDetail: { description: "" },
      isLoading: true,
      urlImg: "",
      isSkeleton: true,
      loadingRemoveButton: false,
    };
    this.idNews = Number(this.props.match.params.id);
  }

  componentDidMount() {
    this._APIgetDetailNews(this.idNews);
  }

  _APIgetDetailNews = async (idNews) => {
    try {
      const data = await APIService._getDetailNews(idNews);

      this.setState({
        dataDetail: data.news,
        urlImg: data.image_url,
        isSkeleton: false,
      });
    } catch (err) {
      console.log(err);
    }
  };

  _APIpostRemoveNews = async (obj) => {
    try {
      const data = await APIService._postRemoveNews(obj);
      message.success("Xóa thành công");
      this.setState(
        {
          loadingRemoveButton: false,
        },
        () => this.props.history.goBack()
      );
    } catch (err) {
      this.setState({
        loadingRemoveButton: false,
      });
    }
  };

  render() {
    const { dataDetail } = this.state;
    return (
      <div style={{ width: "100%" }}>
        <Row>
          <ITitle
            level={1}
            title="Chi tiết tin tức"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
        </Row>
        <Row
          style={{
            marginTop: 20,
            width: "100%",
            display: "flex",
          }}
        >
          <Col
            style={{
              display: "flex",
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            <IButton
              icon={ISvg.NAME.Chat}
              title={"Xem bình luận"}
              color={colors.main}
              onClick={() =>
                this.props.history.push("/new/" + this.idNews + "/comment")
              }
              hideColor={true}
            />
            <IButton
              icon={ISvg.NAME.WRITE}
              title={"Chỉnh sửa"}
              color={colors.main}
              style={{ marginRight: 20, marginLeft: 20 }}
              onClick={() =>
                this.props.history.push("/news/edit/" + this.idNews)
              }
            />
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để xóa tin."}
              onConfirm={() => {
                const objRemove = {
                  id: [this.idNews],
                  status: -1,
                };
                this.setState(
                  {
                    loadingRemoveButton: true,
                  },
                  () => this._APIpostRemoveNews(objRemove)
                );
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                loading={this.state.loadingRemoveButton}
                color={colors.oranges}
              />
            </Popconfirm>
          </Col>
        </Row>
        <Row style={{ marginTop: 38 }}>
          <div style={{ width: 870, background: "white" }} className="shadow">
            <Row>
              <Col
                span={16}
                style={{
                  borderRight: "1px solid #F3F7F5",
                  paddingTop: 30,
                  paddingBottom: 30,
                  paddingLeft: 40,
                  paddingRight: 40,
                }}
              >
                <Skeleton
                  active
                  loading={this.state.isSkeleton}
                  paragraph={{ rows: 6 }}
                >
                  <Row gutter={[0, 20]}>
                    <Col style={{ marginTop: 6 }}>
                      <ITitle
                        level={3}
                        title="Thông tin tin tức"
                        style={{ fontWeight: "bold" }}
                      />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Phân loại tin tức"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={dataDetail.type_name} />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Tiêu đề"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={dataDetail.title} />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Nội dung tóm tắt"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={dataDetail.summary_content} />
                    </Col>
                    {dataDetail.type_name == "Video" ? (
                      <Col>
                        <ITitle
                          level={3}
                          title="Liên kết video"
                          style={{ fontWeight: 500 }}
                        />
                        <a
                          href={dataDetail.video_url}
                          style={{ marginTop: 12 }}
                        >
                          {dataDetail.video_url}
                        </a>
                        <div style={{ width: "100%", marginTop: 12 }}>
                          {/* <video width="100%" controls>
                            <source
                              src="http://clips.vorwaerts-gmbh.de/VfE_html5.mp4"
                              type="video/mp4"
                            />
                          </video> */}
                          <iframe
                            width="100%"
                            height="250"
                            src={dataDetail.video_url}
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                          ></iframe>
                        </div>
                      </Col>
                    ) : (
                      <Col style={{ overflow: "hidden" }}>
                        <ITitle
                          level={3}
                          title="Nội dung"
                          style={{ fontWeight: 500 }}
                        />
                        <ITitle
                          level={4}
                          title={parse(dataDetail.description)}
                        />
                      </Col>
                    )}
                  </Row>
                </Skeleton>
              </Col>
              <Col
                span={8}
                style={{
                  paddingTop: 30,
                  paddingBottom: 30,
                  paddingLeft: 40,
                  paddingRight: 40,
                  borderLeft: "1px solid #F3F7F5",
                }}
              >
                <Skeleton
                  active
                  loading={this.state.isSkeleton}
                  paragraph={{ rows: 6 }}
                >
                  <Row gutter={[0, 20]}>
                    <Col style={{ marginTop: 6 }}>
                      <ITitle
                        level={3}
                        title="Tùy chỉnh hiển thị"
                        style={{ fontWeight: "bold" }}
                      />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Thời gian đăng"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle
                        level={4}
                        title={FormatterDay.dateFormatWithString(
                          dataDetail.post_time,
                          "#DD#-#MM#-#YYYY# #hhh#:#mm#"
                        )}
                      />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Trạng thái"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={dataDetail.status_name} />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Chuyên mục"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={dataDetail.cate_name} />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Thẻ tag"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={dataDetail.tags} />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Hình ảnh"
                        style={{ fontWeight: 500 }}
                      />
                      <img
                        src={this.state.urlImg + dataDetail.thumb}
                        style={{ width: "100%", height: 100, marginTop: 15 }}
                      />
                    </Col>
                  </Row>
                </Skeleton>
              </Col>
            </Row>
          </div>
        </Row>
      </div>
    );
  }
}
