import React, { Component } from "react";
import { Container } from "reactstrap";
import { Row, Col, Skeleton, message, Popconfirm } from "antd";
import { ITitle, ISvg, IButton } from "../../components";
import { colors } from "../../../assets";
import { APIService } from "../../../services";

export default class DetailNewsCategories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataDetail: {},
      isSkeleton: true,
    };
    this.cateNews = Number(this.props.match.params.id);
  }

  componentDidMount() {
    this._APIgetDetailCateNews(this.cateNews);
  }

  _APIgetDetailCateNews = async (id) => {
    try {
      const data = await APIService._getDetailCateNews(id);

      this.setState({
        dataDetail: data.cate,
        isSkeleton: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        isSkeleton: false,
      });
    }
  };

  _APIpostRemoveCateNews = async (obj) => {
    try {
      const data = await APIService._postRemoveCateNews(obj);
      message.success("Xóa thành công");
      this.setState(
        {
          isLoadingRemove: false,
        },
        () =>
          this.props.history.push(
            "/new/categories/" + this.state.dataDetail.type_code + "/page"
          )
      );
    } catch (err) {
      this.setState({
        isLoadingRemove: false,
      });
    }
  };

  render() {
    const { isSkeleton } = this.props;
    return (
      <div style={{ width: "100%" }}>
        <Row>
          <ITitle
            level={1}
            title="Chi tiết chuyên mục"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
        </Row>
        <Row
          style={{
            marginTop: 20,
            width: "100%",
            display: "flex",
          }}
        >
          <Col
            style={{
              display: "flex",
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            <IButton
              icon={ISvg.NAME.WRITE}
              title={"Chỉnh sửa"}
              onClick={() =>
                this.props.history.push("/new/edit/categories/" + this.cateNews)
              }
              color={colors.main}
              style={{ marginRight: 20 }}
            />
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để xóa chuyên mục."}
              onConfirm={() => {
                const objRemove = {
                  id: [this.cateNews],
                  status: -1,
                };
                this.setState(
                  {
                    isLoadingRemove: true,
                  },
                  () => this._APIpostRemoveCateNews(objRemove)
                );
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                loading={this.state.isLoadingRemove}
                color={colors.oranges}
              />
            </Popconfirm>
          </Col>
        </Row>
        <Row style={{ marginTop: 38 }}>
          <div style={{ width: 555, background: "white" }} className="shadow">
            <Row>
              <Col
                span={24}
                style={{
                  paddingTop: 30,
                  paddingBottom: 30,
                  paddingLeft: 40,
                  paddingRight: 40,
                }}
              >
                <Skeleton active loading={isSkeleton} paragraph={{ rows: 12 }}>
                  <Row gutter={[0, 20]}>
                    <Col style={{ marginTop: 6 }}>
                      <ITitle
                        level={3}
                        title="Thông tin chuyên mục"
                        style={{ fontWeight: "bold" }}
                      />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Tên chuyên mục"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={this.state.dataDetail.name} />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Chuyên mục cha"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle
                        level={4}
                        title={this.state.dataDetail.cate_parent_name}
                      />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Phân loại"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle
                        level={4}
                        title={
                          this.state.dataDetail.type_code == "NEWS"
                            ? "Bài viết"
                            : "Video"
                        }
                      />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Trạng thái"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle
                        level={4}
                        title={this.state.dataDetail.status_name}
                      />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Mô tả"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle
                        level={4}
                        title={this.state.dataDetail.description}
                      />
                    </Col>
                  </Row>
                </Skeleton>
              </Col>
            </Row>
          </div>
        </Row>
      </div>
    );
  }
}
