import React, { Component } from "react";
import { ISvg, IButton, ITitle, ITable } from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import NewsPage from "./NewsPage";
import { Skeleton, message, Popconfirm } from "antd";

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

export default class CommentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTable: {},
      loadingTable: true,
      page: 1,
      image_url: "",
      totalTable: 0,
      sizeTable: 0,
      arrayRemove: [],
      selectedRowKeys: [],
    };
    this.idNews = Number(this.props.match.params.id);
    this.onSelectChange = this.onSelectChange.bind(this);
  }

  _APIgetListComment = async (news_id, page) => {
    try {
      const data = await APIService._getListComment(news_id, page);
      data.news.comments.map((item, index) => {
        item.stt = (this.state.page - 1) * 10 + index + 1;
      });
      this.setState({
        dataTable: data.news,
        loadingTable: false,
        image_url: data.image_url,
        isSkeleton: false,
        totalTable: data.total,
        sizeTable: data.size,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
        isSkeleton: false,
      });
    }
  };

  componentDidMount() {
    this._APIgetListComment(this.idNews, this.state.page);
  }

  _APIpostSatusCommentNews = async (obj, title) => {
    try {
      const data = await APIService._postSatusCommentNews(obj);
      message.success(title);
      this.setState(
        {
          loadingTable: true,
          arrayRemove: [],
          selectedRowKeys: [],
        },
        () => this._APIgetListComment(this.idNews, this.state.page)
      );
    } catch (err) {
      console.log(err);
    }
  };

  _updateComment = async (type, title, warning) => {
    try {
      if (this.state.arrayRemove.length == 0) {
        message.warning(warning);
        return;
      }
      const dataObj = {
        id: this.state.arrayRemove,
        status: type,
      };

      this._APIpostSatusCommentNews(dataObj, title);
    } catch (err) {
      console.log(err);
    }
  };

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable.comments[item].id);
    });

    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ arrayRemove: arrayID, selectedRowKeys });
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Thời gian đăng"),
        dataIndex: "create_date",
        key: "create_date",
        align: "left",
        width: 150,
        render: (create_date) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              create_date,
              "#DD#/#MM#/#YYYY# #hhh#:#mm#"
            )
          ),
      },
      {
        title: _renderTitle("Người dùng"),

        align: "left",
        width: 200,

        render: (obj) => {
          return (
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <img
                src={this.state.image_url + obj.user_avatar}
                style={{
                  width: 40,
                  height: 40,
                  borderRadius: 20,
                  marginRight: 8,
                }}
              />
              <ITitle level={4} title={obj.user_name} />
            </div>
          );
        },
      },

      {
        title: _renderTitle("Bình luận"),
        dataIndex: "description",
        key: "description",
        align: "left",

        render: (description) => _renderColumns(description),
      },
      {
        title: _renderTitle("Trạng thái"),
        dataIndex: "status",
        key: "status",
        align: "left",
        width: 100,
        render: (status) => _renderColumns(status == 1 ? "Hiện" : "Ẩn"),
      },
    ];

    const { dataTable, selectedRowKeys } = this.state;

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Danh sách bình luận"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
        </Row>
        <Row className="mt-4">
          <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
            <Skeleton
              active
              loading={this.state.isSkeleton}
              paragraph={{ rows: 1 }}
            >
              <Col>
                <p>Tiêu đề viết</p>
                <ITitle level={4} title={dataTable.title} />
              </Col>
              <Col>
                <p>Chuyên mục</p>
                <ITitle level={4} title={dataTable.cate_name} />
              </Col>
            </Skeleton>
          </Row>
          <Row className="m-0 p-0">
            <IButton
              icon={ISvg.NAME.Preview}
              title={"Hiện"}
              color={colors.main}
              onClick={() =>
                this._updateComment(
                  1,
                  "Hiển thị bình luận thành công",
                  "Chưa chọn bình luận để hiển thị."
                )
              }
            />
            <IButton
              icon={ISvg.NAME.Preview}
              title={"Ẩn"}
              color={colors.blackChart}
              style={{ marginRight: 20, marginLeft: 20 }}
              onClick={() =>
                this._updateComment(
                  0,
                  "Ẩn bình luận thành công",
                  "Chưa chọn bình luận để ẩn."
                )
              }
            />
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để xóa bình luận."}
              onConfirm={() => {
                this._updateComment(
                  -1,
                  "Xóa thành công",
                  "Chưa chọn bình luận để xóa."
                );
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                color={colors.oranges}
              />
            </Popconfirm>
          </Row>
        </Row>

        <Row className="mt-4">
          <ITable
            data={dataTable.comments}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%" }}
            rowSelection={rowSelection}
            sizeItem={this.state.sizeTable}
            indexPage={this.state.page}
            maxpage={this.state.totalTable}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                  loadingTable: true,
                },
                () => this._APIgetListComment(this.idNews, this.state.page)
              );
            }}
          />
        </Row>
      </Container>
    );
  }
}
