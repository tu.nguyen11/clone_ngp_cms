import { message, Popconfirm } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ITable,
  IDatePicker,
  ISelectTypeNews,
  ISelectStatusNews,
  ISelectCateNews,
} from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
const widthScreen = window.innerWidth;

const NewDate = new Date();
const DateStart = new Date();
// let startValue = DateStart.setMonth(DateStart.getMonth() - 1);
let startValue = DateStart;
function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 7;
  } else widthColumn = (widthScreen - 500) / 7;
  return widthColumn * type;
}

export default class NewsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTable: [],
      loadingTable: true,
      keySelectcategories: 0,
      keySelectClassify: 0,
      keySelectStatus: 0,
      totalTable: 0,
      sizeTable: 0,
      page: 1,
      toDate: NewDate.setHours(23, 59, 59, 999),
      fromDate: NewDate.setHours(0, 0, 0, 0),
      cate_id: -1,
      type_code: "",
      keySearch: "",
      status: 4,
      arrayRemove: [],
      selectedRowKeys: [],
      isLoadingRemove: false,
    };
  }
  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }
  componentDidMount() {
    this._APIgetListAllNews(
      this.state.fromDate,
      this.state.toDate,
      this.state.cate_id,
      this.state.type_code,
      this.state.status,
      this.state.page,
      this.state.keySearch
    );
  }

  _APIgetListAllNews = async (
    start_date,
    end_date,
    cate_id,
    type_code,
    status,
    page,
    key
  ) => {
    try {
      const data = await APIService._getListAllNews(
        start_date,
        end_date,
        cate_id,
        type_code,
        status,
        page,
        key
      );
      data.news.map((item, index) => {
        item.stt = (this.state.page - 1) * 10 + index + 1;
      });
      this.setState({
        dataTable: data.news,
        totalTable: data.total,
        sizeTable: data.size,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  _APIpostRemoveNews = async (obj) => {
    try {
      const data = await APIService._postRemoveNews(obj);
      message.success("Xóa thành công");
      this.setState(
        {
          loadingTable: true,
          arrayRemove: [],
          selectedRowKeys: [],
          isLoadingRemove: false,
        },
        () =>
          this._APIgetListAllNews(
            this.state.fromDate,
            this.state.toDate,
            this.state.cate_id,
            this.state.type_code,
            this.state.status,
            this.state.page,
            this.state.keySearch
          )
      );
    } catch (err) {
      this.setState({
        loadingTable: false,
      });
    }
  };

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[item].id);
    });
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ arrayRemove: arrayID, selectedRowKeys });
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Ngày tạo"),
        dataIndex: "create_time",
        key: "create_time",
        align: "left",

        render: (create_time) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(create_time, "#DD#/#MM#/#YYYY#")
          ),
      },
      {
        title: _renderTitle("Mã tin tức"),
        dataIndex: "code",
        key: "code",
        align: "left",

        render: (code) => _renderColumns(code),
      },
      {
        title: _renderTitle("Chuyên mục"),
        dataIndex: "cate_name",
        key: "cate_name",
        align: "left",

        render: (cate_name) => _renderColumns(cate_name),
      },
      {
        title: _renderTitle("Tiêu đề"),
        dataIndex: "title",
        key: "title",
        align: "left",

        render: (title) => (
          <span
            style={{
              wordWrap: "break-word",
              wordBreak: "break-word",
              overflow: "hidden",
            }}
            className="ellipsis-text"
          >
            {title}
          </span>
        ),
      },
      {
        title: _renderTitle("Nội dung"),
        dataIndex: "summary_content",
        key: "summary_content",
        align: "left",

        render: (summary_content) => (
          <span
            style={{
              wordWrap: "break-word",
              wordBreak: "break-word",
              overflow: "hidden",
            }}
            className="ellipsis-text"
          >
            {summary_content}
          </span>
        ),
      },
      {
        title: _renderTitle("Thời gian đăng"),
        dataIndex: "post_time",
        key: "post_time",
        align: "left",

        render: (post_time) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              post_time,
              "#DD#/#MM#/#YYYY# #hhh#:#mm# "
            )
          ),
      },
      {
        title: _renderTitle("Phân loại"),
        dataIndex: "type_name",
        key: "type_name",
        align: "left",

        render: (type_name) => _renderColumns(type_name),
      },
      {
        title: _renderTitle("Trạng thái"),
        dataIndex: "status_name",
        key: "status_name",
        align: "left",

        render: (status_name) => _renderColumns(status_name),
      },
      {
        title: _renderTitle("Thích"),
        dataIndex: "interactive",
        key: "like",
        align: "center",

        render: (interactive) => _renderColumns(interactive.like),
      },
      {
        title: _renderTitle("Chia sẻ"),
        dataIndex: "interactive",
        key: "share",
        align: "center",

        render: (interactive) => _renderColumns(interactive.share),
      },
      {
        title: _renderTitle("Bình luận"),
        dataIndex: "interactive",
        key: "comment",
        align: "center",

        render: (interactive) => _renderColumns(interactive.comment),
      },
      {
        title: "",
        dataIndex: "id",
        key: "id",
        align: "right",
        width: 100,
        fixed: widthScreen <= 1400 ? "right" : "",

        render: (id) => (
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div
              onClick={(e) => {
                e.stopPropagation();
                this.props.history.push("/news/edit/" + id);
              }} // idAndStt.index vị trí của mảng
              className="cell"
              style={{ width: 50 }}
            >
              <ISvg
                name={ISvg.NAME.WRITE}
                width={20}
                height={20}
                fill={colors.icon.default}
              />
            </div>
            <div
              className="cursor"
              style={{ width: 50 }}
              onClick={(e) => {
                e.stopPropagation();
                this.props.history.push("/newsDetail/" + id);
              }}
            >
              <ISvg
                name={ISvg.NAME.CHEVRONRIGHT}
                width={11}
                height={20}
                fill={colors.icon.default}
              />
            </div>
          </div>
        ),
      },
    ];

    const { value, selectedRowKeys } = this.state;

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Tin tức"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <ISearch
            placeholder=""
            onPressEnter={() =>
              this.setState(
                {
                  loadingTable: true,
                  page: 1,
                },
                () =>
                  this._APIgetListAllNews(
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.cate_id,
                    this.state.type_code,
                    this.state.status,
                    this.state.page,
                    this.state.keySearch
                  )
              )
            }
            onChange={(e) =>
              this.setState({
                keySearch: e.target.value,
              })
            }
            icon={
              <div
                style={{
                  display: "flex",
                  width: 42,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                onClick={() =>
                  this.setState(
                    {
                      loadingTable: true,
                    },
                    () =>
                      this._APIgetListAllNews(
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.cate_id,
                        this.state.type_code,
                        this.state.status,
                        this.state.page,
                        this.state.keySearch
                      )
                  )
                }
                className="cursor"
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>
        <Row className="mt-4">
          <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
            <Row className="m-0 p-0 center">
              <ISvg
                name={ISvg.NAME.EXPERIMENT}
                width={20}
                height={20}
                fill={colors.icon.default}
              />
              <div style={{ marginLeft: 25 }}>
                <ITitle title="Từ ngày" level={4} />
              </div>
            </Row>
            <Row className="m-0 p-0 ml-4" style={{}}>
              <IDatePicker
                isBackground
                from={this.state.fromDate}
                to={this.state.toDate}
                style={{
                  background: colors.white,
                  borderWidth: 1,
                  borderColor: colors.line,
                  borderStyle: "solid",
                }}
                onChange={(date, dateString) => {
                  if (date.length <= 1) {
                    return;
                  }
                  this.state.fromDate = date[0]._d.setHours(0, 0, 0);

                  this.state.toDate = date[1]._d.setHours(23, 59, 59);
                  this.setState(
                    {
                      fromDate: this.state.fromDate,
                      toDate: this.state.toDate,
                      page: 1,
                    },
                    () =>
                      this._APIgetListAllNews(
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.cate_id,
                        this.state.type_code,
                        this.state.status,
                        this.state.page,
                        this.state.keySearch
                      )
                  );
                }}
              />
            </Row>
            <Row className="m-0 p-0 ml-4">
              <ISelectCateNews
                defaultValue="Chuyên mục"
                type=""
                onChange={(value) => {
                  this.setState(
                    {
                      cate_id: value,
                      loadingTable: true,
                      page: 1,
                    },
                    () =>
                      this._APIgetListAllNews(
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.cate_id,
                        this.state.type_code,
                        this.state.status,
                        this.state.page,
                        this.state.keySearch
                      )
                  );
                }}
              />
            </Row>

            <Row className="m-0 p-0 ml-4 ">
              <ISelectTypeNews
                defaultValue="Phân loại"
                onChange={(value) => {
                  this.setState(
                    {
                      type_code: value,
                      loadingTable: true,
                      page: 1,
                    },
                    () =>
                      this._APIgetListAllNews(
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.cate_id,
                        this.state.type_code,
                        this.state.status,
                        this.state.page,
                        this.state.keySearch
                      )
                  );
                }}
              />
            </Row>
            <Row className="m-0 p-0 ml-4 ">
              <ISelectStatusNews
                defaultValue="Trạng thái"
                onChange={(value) => {
                  this.setState(
                    {
                      status: value,
                      loadingTable: true,
                      page: 1,
                    },
                    () =>
                      this._APIgetListAllNews(
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.cate_id,
                        this.state.type_code,
                        this.state.status,
                        this.state.page,
                        this.state.keySearch
                      )
                  );
                }}
              />
            </Row>
          </Row>
          <Row className="m-0 p-0">
            <IButton
              icon={ISvg.NAME.ARROWUP}
              title={"Tạo mới"}
              color={colors.main}
              styleHeight={{ height: 44 }}
              style={{ marginRight: 20 }}
              onClick={() => this.props.history.push("/news/add/0")}
            />
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để xóa tin."}
              onConfirm={() => {
                if (this.state.arrayRemove.length === 0) {
                  message.warning("Chưa chọn tin tức để xóa.");
                  return;
                }
                this.setState({
                  isLoadingRemove: true,
                });
                let objRemove = {
                  id: this.state.arrayRemove,
                  status: -1,
                };
                this._APIpostRemoveNews(objRemove);
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                color={colors.oranges}
                style={{
                  background: this.state.isLoadingRemove
                    ? colors.oranges
                    : "white",
                }}
                styleHeight={{ height: 44 }}
                loading={this.state.isLoadingRemove}
              />
            </Popconfirm>
          </Row>
        </Row>

        <Row className="mt-4">
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%" }}
            rowSelection={rowSelection}
            sizeItem={this.state.sizeTable}
            indexPage={this.state.page}
            maxpage={this.state.totalTable}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );
                  const id = this.state.dataTable[indexRow].id;
                  this.props.history.push("/newsDetail/" + id);
                }, // click row
              };
            }}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                  loadingTable: true,
                },
                () =>
                  this._APIgetListAllNews(
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.cate_id,
                    this.state.type_code,
                    this.state.status,
                    this.state.page,
                    this.state.keySearch
                  )
              );
            }}
          />
        </Row>
      </Container>
    );
  }
}
