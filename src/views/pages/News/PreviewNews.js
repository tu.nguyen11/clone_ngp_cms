import React, { Component } from "react";
import { Formatter } from "tinymce";
import FormatterDay from "../../../utils/FormatterDay";
import parse from "html-react-parser";
export default class PreviewNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "NEWS",
    };
  }

  _checkVideoEmbed = (content) => {
    if (content.search("embed") == -1) {
      const url = content;
      let arrUrl = url.split("/");
      const idURL = arrUrl[3];
      const urlVideo =
        "https://www.youtube.com/embed/" + idURL + "?modestbranding=1";
      return urlVideo;
    } else {
      return content;
    }
  };

  render() {
    const { title, time, summaryContent, content, img, type } = this.props;
    const urlVideo = this._checkVideoEmbed(content);
    return (
      <div class="container1">
        <div class="phone_border">
          <div class="phone_speaker">
            <span class="speaker_cyrcle"></span>
            <span class="speaker_line"></span>
          </div>
          <div class="phone_screen">
            <div class="mobile_unlocked"></div>
            <div class="locked_screen">
              <div class="mobile_locked">
                <span class="mobile_locked_disabled">iPhone is disabled</span>
                <span class="mobile_locked_try_again">
                  try again in 3 minutes
                </span>
              </div>
              <div class="top_screen">
                <div class="information">
                  <span class="connection">IPhone 6s LTE</span>
                  <span class="lock">
                    <i class="fa fa-lock"></i>
                  </span>
                  <span class="battery">
                    97%
                    <i class="fa fa-battery-full"></i>
                  </span>
                </div>
                {type == "NEWS" ? (
                  <div
                    style={{
                      height: 450,
                      overflow: "auto",
                    }}
                  >
                    <div style={{ padding: 14 }}>
                      <p
                        style={{
                          fontSize: 14,
                          fontFamily: "Roboto",
                          fontWeight: "bold",
                        }}
                      >
                        {title}
                      </p>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          paddingTop: 8,
                        }}
                      >
                        <svg
                          width="14"
                          height="14"
                          viewBox="0 0 14 14"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M7 0C3.1402 0 0 3.1402 0 7C0 10.8598 3.1402 14 7 14C10.8598 14 14 10.8598 14 7C14 3.1402 10.8598 0 7 0ZM7 12.5105C3.96153 12.5105 1.48934 10.0386 1.48934 7C1.48934 3.96138 3.96153 1.48948 7 1.48948C10.0385 1.48948 12.5107 3.96138 12.5107 7C12.5107 10.0386 10.0385 12.5105 7 12.5105Z"
                            fill="#A8ACAD"
                          />
                          <path
                            d="M10.648 6.78359H7.50216V3.00117C7.50216 2.68287 7.24409 2.4248 6.9258 2.4248C6.6075 2.4248 6.34943 2.68287 6.34943 3.00117V7.35996C6.34943 7.67826 6.6075 7.93633 6.9258 7.93633H10.648C10.9663 7.93633 11.2244 7.67826 11.2244 7.35996C11.2244 7.04166 10.9663 6.78359 10.648 6.78359Z"
                            fill="#A8ACAD"
                          />
                        </svg>
                        <span style={{ marginLeft: 6 }}>
                          {FormatterDay.dateFormatWithString(
                            time,
                            "#DD#/#MM#/#YYYY#"
                          )}
                        </span>
                      </div>
                    </div>

                    <img src={img} style={{ width: "100%", height: 200 }} />

                    <div style={{ padding: 8 }}>
                      <p
                        style={{
                          fontSize: 12,
                          color: "#A8ACAD",
                          fontWeight: "normal",
                          fontFamily: "Roboto",
                        }}
                      >
                        {summaryContent}
                      </p>
                      <div style={{ marginTop: 8 }}>
                        <p
                          style={{
                            fontSize: 14,
                            fontWeight: "normal",
                            fontFamily: "Roboto",
                          }}
                        >
                          {parse(content)}
                        </p>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div
                    style={{
                      height: 450,
                      overflow: "auto",
                    }}
                  >
                    <iframe
                      width="100%"
                      height="200"
                      src={urlVideo}
                      frameborder="0"
                      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                      allowfullscreen
                    ></iframe>
                    <div style={{ padding: 14 }}>
                      <p
                        style={{
                          fontSize: 14,
                          fontFamily: "Roboto",
                          fontWeight: "bold",
                        }}
                      >
                        {title}
                      </p>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          marginTop: 8,
                        }}
                      >
                        <svg
                          width="14"
                          height="14"
                          viewBox="0 0 14 14"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M7 0C3.1402 0 0 3.1402 0 7C0 10.8598 3.1402 14 7 14C10.8598 14 14 10.8598 14 7C14 3.1402 10.8598 0 7 0ZM7 12.5105C3.96153 12.5105 1.48934 10.0386 1.48934 7C1.48934 3.96138 3.96153 1.48948 7 1.48948C10.0385 1.48948 12.5107 3.96138 12.5107 7C12.5107 10.0386 10.0385 12.5105 7 12.5105Z"
                            fill="#A8ACAD"
                          />
                          <path
                            d="M10.648 6.78359H7.50216V3.00117C7.50216 2.68287 7.24409 2.4248 6.9258 2.4248C6.6075 2.4248 6.34943 2.68287 6.34943 3.00117V7.35996C6.34943 7.67826 6.6075 7.93633 6.9258 7.93633H10.648C10.9663 7.93633 11.2244 7.67826 11.2244 7.35996C11.2244 7.04166 10.9663 6.78359 10.648 6.78359Z"
                            fill="#A8ACAD"
                          />
                        </svg>
                        <span style={{ marginLeft: 6, color: "#A8ACAD" }}>
                          {FormatterDay.dateFormatWithString(
                            time,
                            "#DD#/#MM#/#YYYY#"
                          )}
                        </span>
                      </div>
                    </div>
                    <div style={{ paddingLeft: 14, paddingRight: 14 }}>
                      <p
                        style={{
                          fontSize: 12,
                          // color: "#A8ACAD",
                          fontWeight: "normal",
                          fontFamily: "Roboto",
                        }}
                      >
                        {summaryContent}
                      </p>
                    </div>
                  </div>
                )}
              </div>
              <div class="bottom_screen">
                <span>Press home to unlock</span>
              </div>
            </div>
          </div>
          <div class="phone_button">
            <div class="button"></div>
          </div>
        </div>
      </div>
    );
  }
}
