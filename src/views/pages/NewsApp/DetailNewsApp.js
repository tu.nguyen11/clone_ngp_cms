import React, { Component } from "react";
import { Row, Col, Button, Skeleton } from "antd";
import { colors } from "../../../assets";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import parse from "html-react-parser";

export default class DetailNewsApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urlImg: "",
      dataDetail: { description: "" },

      isSkeleton: true,
    };
    this.id = Number(this.props.match.params.id);
    this.type = this.props.match.params.type;
    this.boolean = this.type == "NEWS" || this.type == "news" ? true : false;
  }

  componentDidMount() {
    this._APIgetDetailNews(this.id);
  }

  _APIgetDetailNews = async (idNews) => {
    try {
      const data = await APIService._getDetailNews(idNews);

      this.setState({
        dataDetail: data.news,
        urlImg: data.image_url,
        isSkeleton: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        isSkeleton: false,
      });
    }
  };

  render() {
    const { dataDetail } = this.state;
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        {this.boolean ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              paddingTop: 24,
              fontFamily: "Roboto",
              width: 600,
              boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
              background: colors.white,
            }}
          >
            <Skeleton
              active
              loading={this.state.isSkeleton}
              paragraph={{ rows: 6 }}
            >
              <Row
                style={{ fontFamily: "Roboto", width: "100%", height: "100%" }}
                gutter={[0, 12]}
              >
                <Col>
                  <div style={{ paddingLeft: 24, paddingRight: 24 }}>
                    <h3>{dataDetail.title}</h3>
                  </div>
                </Col>

                <Col>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingLeft: 24,
                      paddingRight: 24,
                    }}
                  >
                    <svg
                      style={{ marginLeft: 12, marginRight: 12 }}
                      width="14"
                      height="14"
                      viewBox="0 0 14 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7 0C3.1402 0 0 3.1402 0 7C0 10.8598 3.1402 14 7 14C10.8598 14 14 10.8598 14 7C14 3.1402 10.8598 0 7 0ZM7 12.5105C3.96153 12.5105 1.48934 10.0386 1.48934 7C1.48934 3.96138 3.96153 1.48948 7 1.48948C10.0385 1.48948 12.5107 3.96138 12.5107 7C12.5107 10.0386 10.0385 12.5105 7 12.5105Z"
                        fill="#A8ACAD"
                      />
                      <path
                        d="M10.648 6.78359H7.50216V3.00117C7.50216 2.68287 7.24409 2.4248 6.9258 2.4248C6.6075 2.4248 6.34943 2.68287 6.34943 3.00117V7.35996C6.34943 7.67826 6.6075 7.93633 6.9258 7.93633H10.648C10.9663 7.93633 11.2244 7.67826 11.2244 7.35996C11.2244 7.04166 10.9663 6.78359 10.648 6.78359Z"
                        fill="#A8ACAD"
                      />
                    </svg>
                    <span
                      style={{
                        fontSize: 14,
                        fontStyle: "normal",
                        fontWeight: "normal",
                        color: "#A8ACAD",
                      }}
                    >
                      {FormatterDay.dateFormatWithString(
                        dataDetail.post_time,
                        "#DD#/#MM#/#YYYY#"
                      )}
                    </span>
                  </div>
                </Col>
                <Col>
                  <img
                    src={this.state.urlImg + dataDetail.thumb}
                    style={{
                      width: "100%",
                      height: 250,
                    }}
                  />
                </Col>
                <Col>
                  <span
                    style={{
                      fontSize: 12,
                      fontStyle: "normal",
                      fontWeight: "normal",
                      color: "#A8ACAD",
                      paddingLeft: 24,
                      paddingRight: 24,
                    }}
                  >
                    {dataDetail.summary_content}
                  </span>
                </Col>
                <Col>
                  <span
                    style={{
                      fontSize: 14,
                      fontStyle: "normal",
                      fontWeight: "normal",
                      color: "#3C3F3D",
                      paddingLeft: 24,
                      paddingRight: 24,
                    }}
                  >
                    {parse(dataDetail.description)}
                  </span>
                </Col>
                <Col>
                  <div
                    style={{
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      background: colors.white,
                      paddingBottom: 16,
                      paddingTop: 16,
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <Button
                      style={{
                        marginRight: 12,
                      }}
                      className="gradient"
                    >
                      <span
                        style={{ color: colors.background, fontWeight: "bold" }}
                      >
                        Tải App IOS
                      </span>
                    </Button>
                    <Button className="gradient">
                      <span
                        style={{ color: colors.background, fontWeight: "bold" }}
                      >
                        Tải App Android
                      </span>
                    </Button>
                  </div>
                </Col>
              </Row>
            </Skeleton>
          </div>
        ) : (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              fontFamily: "Roboto",
              width: 600,
              boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
              background: colors.white,
            }}
          >
            <Skeleton
              active
              loading={this.state.isSkeleton}
              paragraph={{ rows: 6 }}
            >
              <Row
                style={{ fontFamily: "Roboto", width: "100%", height: "100%" }}
                gutter={[0, 12]}
              >
                <Col>
                  <iframe
                    width="100%"
                    height="300"
                    src={dataDetail.video_url}
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe>
                </Col>

                <Col>
                  <div
                    style={{
                      paddingLeft: 24,
                      paddingRight: 24,
                    }}
                  >
                    <span
                      style={{
                        fontSize: 14,
                        fontStyle: "normal",
                        fontWeight: 900,
                        color: "#3C3F3D",
                      }}
                    >
                      {dataDetail.title}
                    </span>
                  </div>
                </Col>
                <Col>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingLeft: 24,
                      paddingRight: 24,
                    }}
                  >
                    <svg
                      style={{ marginLeft: 12, marginRight: 12 }}
                      width="14"
                      height="14"
                      viewBox="0 0 14 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7 0C3.1402 0 0 3.1402 0 7C0 10.8598 3.1402 14 7 14C10.8598 14 14 10.8598 14 7C14 3.1402 10.8598 0 7 0ZM7 12.5105C3.96153 12.5105 1.48934 10.0386 1.48934 7C1.48934 3.96138 3.96153 1.48948 7 1.48948C10.0385 1.48948 12.5107 3.96138 12.5107 7C12.5107 10.0386 10.0385 12.5105 7 12.5105Z"
                        fill="#A8ACAD"
                      />
                      <path
                        d="M10.648 6.78359H7.50216V3.00117C7.50216 2.68287 7.24409 2.4248 6.9258 2.4248C6.6075 2.4248 6.34943 2.68287 6.34943 3.00117V7.35996C6.34943 7.67826 6.6075 7.93633 6.9258 7.93633H10.648C10.9663 7.93633 11.2244 7.67826 11.2244 7.35996C11.2244 7.04166 10.9663 6.78359 10.648 6.78359Z"
                        fill="#A8ACAD"
                      />
                    </svg>
                    <span
                      style={{
                        fontSize: 14,
                        fontStyle: "normal",
                        fontWeight: "normal",
                        color: "#A8ACAD",
                      }}
                    >
                      {FormatterDay.dateFormatWithString(
                        dataDetail.post_time,
                        "#DD#/#MM#/#YYYY#"
                      )}
                    </span>
                  </div>
                </Col>

                <Col>
                  <span
                    style={{
                      fontSize: 14,
                      fontStyle: "normal",
                      fontWeight: "normal",
                      color: "#3C3F3D",
                      paddingLeft: 24,
                      paddingRight: 24,
                    }}
                  >
                    {dataDetail.summary_content}
                  </span>
                </Col>
                <Col style={{}}>
                  <div
                    style={{
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      background: colors.white,
                      paddingBottom: 16,
                      paddingTop: 16,
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <Button
                      style={{
                        marginRight: 12,
                      }}
                      className="gradient"
                    >
                      <span
                        style={{ color: colors.background, fontWeight: "bold" }}
                      >
                        Tải App IOS
                      </span>
                    </Button>
                    <Button className="gradient">
                      <span
                        style={{ color: colors.background, fontWeight: "bold" }}
                      >
                        Tải App Android
                      </span>
                    </Button>
                  </div>
                </Col>
              </Row>
            </Skeleton>
          </div>
        )}
      </div>
    );
  }
}
