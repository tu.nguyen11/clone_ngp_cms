import React, {useState, useEffect} from "react";
import {Row, Col, Popconfirm, Tooltip, message} from "antd";
import {ISearch, ISvg, ISelect, ITable, IButton} from "../../components";
import {colors, images} from "../../../assets";
import {StyledITitle} from "../../components/common/Font/font";
import FormatterDay from "../../../utils/FormatterDay";
import {APIService} from "../../../services";
import {useHistory} from "react-router-dom";
import {useSelector} from "react-redux";

export default function ListCustomer(props) {
  const history = useHistory();
  const {menu} = useSelector((state) => state)

  const [filter, setFilter] = useState({
    key: "",
    sale_man_id: -1,
    page: 1,
  });

  const [dataTable, setDataTable] = useState({
    listCustomer: [],
    total: 1,
    size: 10,
  });

  const [listSalesMan, setListSalesMan] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const [loadingSalesMan, setLoadingSalesMan] = useState(true);
  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingRemove, setLoadingRemove] = useState(false);

  const rowSelection = {
    selectedRowKeys,
    onChange: (key) => {
      setSelectedRowKeys(key);
    },
  };

  const getListSalesMan = async () => {
    try {
      const data = await APIService._getListSalesManDropList();
      let dataNew = [{key: -1, value: "Tất cả"}, {
        key: 0,
        value: "Chưa có nhân viên phụ trách"
      }].concat(data.data.map((item) => {
        return {key: item.id, value: item.name};
      }));

      setListSalesMan(dataNew);
      setLoadingSalesMan(false);
    } catch (error) {
      console.log(error);
      setLoadingSalesMan(false);
    }
  };

  const getListCustomer = async (filter) => {
    try {
      const data = await APIService._getListCustomer(filter);
      data.users.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        return item;
      });

      setDataTable({
        listCustomer: data.users,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const deleteMember = async (dataDelete) => {
    try {
      const data = await APIService._deleteNhanVien(dataDelete);
      setSelectedRowKeys([]);
      setLoadingRemove(false);
      message.success("Xóa khách hàng thành công");
      setLoadingTable(true);
      await getListCustomer(filter);
    } catch (error) {
      console.log(error);
      setLoadingRemove(false);
    }
  };

  useEffect(() => {
    getListSalesMan();
  }, []);

  useEffect(() => {
    getListCustomer(filter);
  }, [filter]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tên khách hàng",
      dataIndex: "customer_name",
      key: "customer_name",
      render: (customer_name) => (
        <span>{!customer_name ? "-" : customer_name}</span>
      ),
    },
    {
      title: "Mã khách hàng",
      dataIndex: "user_code",
      key: "user_code",
      render: (user_code) => (
        <span>{!user_code ? "-" : user_code}</span>
      ),
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <span>{!phone ? "-" : phone}</span>,
    },
    {
      title: "Mẫu xe quan tâm",
      dataIndex: "product_name_care",
      key: "product_name_care",
      render: (product_name_care) => (
        <span>{!product_name_care ? "-" : product_name_care}</span>
      ),
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
      render: (address) => <span>{!address || address === ', ' ? "-" : address.replace((/^,/g), '')}</span>,
    },
    {
      title: "Khách hàng",
      dataIndex: "customer_type",
      key: "customer_type",
      align: "center",
      render: (customer_type) => (
        <span>{!customer_type ? "-" : customer_type}</span>
      ),
    },
    {
      title: "NV phụ trách",
      align: "center",
      dataIndex: "sale_man_name",
      key: "sale_man_name",
      render: (sale_man_name) => (
        <span>{!sale_man_name ? "-" : sale_man_name}</span>
      ),
    },
    {
      title: "Ngày hẹn",
      align: "center",
      dataIndex: "appointment_date",
      key: "appointment_date",
      render: (appointment_date) => (
        <span>{!appointment_date ? "-" : appointment_date}</span>
      ),
    },
    {
      title: "Kênh kinh doanh",
      align: "left",
      dataIndex: "business_chanel_name",
      key: "business_chanel_name",
      render: (business_chanel_name) => (
        <span>{!business_chanel_name ? "-" : business_chanel_name}</span>
      ),
    },
    {
      title: "Mã tuyến",
      align: "left",
      dataIndex: "route_code",
      key: "route_code",
      render: (route_code) => (
        <span>{!route_code ? "-" : route_code}</span>
      ),
    },
  ];

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{color: colors.main, fontSize: 22}}>
                Danh sách khách hàng
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên khách hàng">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên khách hàng"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{width: 16, height: 16}}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{marginTop: 26}}>
            <Row gutter={[0, 25]}>
              <Col span={24}>
                <div className="flex justify-end item-center">
                  <ISvg
                    name={ISvg.NAME.LOCATION}
                    with={20}
                    height={20}
                    fill={colors.icon.default}
                  />

                  <div style={{width: 200, margin: "0px 15px"}}>
                    <ISelect
                      defaultValue="Nhân viên phụ trách"
                      isTooltip={true}
                      data={listSalesMan}
                      loading={loadingSalesMan}
                      select={loadingSalesMan ? false : true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          sale_man_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <IButton
                    title="Tạo mới"
                    color={colors.main}
                    icon={ISvg.NAME.ARROWUP}
                    styleHeight={{
                      width: 140,
                      marginRight: 15,
                    }}
                    isRight={true}
                    onClick={() => {
                      history.push(`/customer/create/0`);
                    }}
                  />
                  {/*<IButton*/}
                  {/*  title="Xuất file"*/}
                  {/*  color={colors.main}*/}
                  {/*  icon={ISvg.NAME.DOWLOAND}*/}
                  {/*  styleHeight={{*/}
                  {/*    width: 140,*/}
                  {/*    marginRight: 15,*/}
                  {/*  }}*/}
                  {/*  isRight={true}*/}
                  {/*  onClick={() => {*/}
                  {/*    message.warning("Tính năng cập nhật sau");*/}
                  {/*  }}*/}
                  {/*/>*/}

                  {/*<Popconfirm*/}
                  {/*  placement="bottom"*/}
                  {/*  title="Nhấn đồng ý để xóa"*/}
                  {/*  okText="Đồng ý"*/}
                  {/*  cancelText="Hủy"*/}
                  {/*  onConfirm={() => {*/}
                  {/*    if (selectedRowKeys.length === 0) {*/}
                  {/*      message.error("Chưa chọn khách hàng để xóa");*/}
                  {/*      return;*/}
                  {/*    }*/}
                  {/*    const objRemove = {*/}
                  {/*      saleman_id: selectedRowKeys,*/}
                  {/*    };*/}
                  {/*    setLoadingRemove(true);*/}
                  {/*    deleteMember(objRemove);*/}
                  {/*  }}*/}
                  {/*>*/}
                  {/*  <IButton*/}
                  {/*    color={colors.oranges}*/}
                  {/*    title="Xóa"*/}
                  {/*    icon={ISvg.NAME.DELETE}*/}
                  {/*    styleHeight={{ width: 140 }}*/}
                  {/*    loading={loadingRemove}*/}
                  {/*  />*/}
                  {/*</Popconfirm>*/}
                </div>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listCustomer}
                    style={{width: "100%"}}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    scroll={{x: 1600}}
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    rowKey='id'
                    onRow={(record) => {
                      return {
                        onClick: () => {
                          history.push(`/detail/customer/${record.id}`);
                        }, // click row
                        onMouseDown: (event) => {
                          if (event.button == 2 || event == 4) {
                            window.open(
                              `/detail/customer/${record.id}`,
                              "_blank"
                            );
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({...filter, page: page});
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}
