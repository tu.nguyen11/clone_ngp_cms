import React, { useState, useEffect } from "react";
import { Row, Col, message, Tooltip } from "antd";
import {
  ISearch,
  ISvg,
  ITable,
  IButton,
  ITitle,
  IDatePicker,
} from "../../components";
import { colors, images } from "../../../assets";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import { useParams } from "react-router-dom";
import { priceFormat } from "../../../utils";
const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

export default function ListHistorySurvey(props) {
  const { id } = useParams();
  const [data, setData] = useState({
    history_survey: [],
    size: 10,
    total: 0,
  });

  const [isLoading, setLoading] = useState(true);

  const [filterTable, setFilterTable] = useState({
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
    user_id: id,
    key: "",
    page: 1,
  });

  const getListSurveyCustomer = async (filterTable) => {
    try {
      const data = await APIService._getListSurveyCustomer(filterTable);
      const newArr = data.history_survey.map((item, index) => {
        return {
          cau_hoi: Object.keys(item)[0],
          cau_tra_loi_ban_dau:  item[Object.keys(item)[0]].ban_dau  ?  !isNaN(Number(item[Object.keys(item)[0]].ban_dau.cau_tra_loi))  ? priceFormat(item[Object.keys(item)[0]].ban_dau.cau_tra_loi)  : item[Object.keys(item)[0]].ban_dau.cau_tra_loi : null,
          thoi_gian_ban_dau:  item[Object.keys(item)[0]].ban_dau  ? item[Object.keys(item)[0]].ban_dau.thoi_gian : null,
          cau_tra_loi_hien_tai: !isNaN(Number(item[Object.keys(item)[0]].hien_tai.cau_tra_loi)) ? priceFormat(item[Object.keys(item)[0]].hien_tai.cau_tra_loi) : item[Object.keys(item)[0]].hien_tai.cau_tra_loi,
          thoi_gian_hien_tai: item[Object.keys(item)[0]].hien_tai.thoi_gian,
        };
      });
      setData({ history_survey: newArr, total: data.total, size: data.size });
      setLoading(false);
    } catch (error) {
      console.log({ error });
      setLoading(false);
    }
  };

  useEffect(() => {
    getListSurveyCustomer(filterTable);
  }, [filterTable]);

  const columns = [
    {
      title: "Câu hỏi khảo sát",
      dataIndex: "cau_hoi",
      align: "left",
      key: "cau_hoi",
      width:550,
      render: (cau_hoi) =>
        !cau_hoi ? (
          "-"
        ) : (
          <Tooltip title={cau_hoi}>
            <span>{cau_hoi}</span>
          </Tooltip>
        ),
    },
    {
      title: "Câu trả lời ban đầu",
      dataIndex: "cau_tra_loi_ban_dau",
      key: "cau_tra_loi_ban_dau",
      align: "left",
      render: (cau_tra_loi_ban_dau) =>
        !cau_tra_loi_ban_dau ? (
          "-"
        ) : (
          <Tooltip title={cau_tra_loi_ban_dau}>
            <span>{cau_tra_loi_ban_dau}</span>
          </Tooltip>
        ),
    },
    {
      title: "Thời gian",
      dataIndex: "thoi_gian_ban_dau",
      key: "thoi_gian_ban_dau",
      width:200,
      align: "center",
      render: (thoi_gian_ban_dau) => (
        <span>
          {!thoi_gian_ban_dau || thoi_gian_ban_dau <= 0
            ? "-"
            : thoi_gian_ban_dau}
        </span>
      ),
    },

    {
      title: "Câu trả lời hiện tại",
      dataIndex: "cau_tra_loi_hien_tai",
      key: "cau_tra_loi_hien_tai",
      align: "left",
      render: (cau_tra_loi_hien_tai) =>
        !cau_tra_loi_hien_tai ? (
          "-"
        ) : (
          <Tooltip title={cau_tra_loi_hien_tai}>
            <span>{cau_tra_loi_hien_tai}</span>
          </Tooltip>
        ),
    },
    {
      title: "Thời gian",
      dataIndex: "thoi_gian_hien_tai",
      key: "thoi_gian_hien_tai",
      align: "center",
      width:200,
      render: (thoi_gian_hien_tai) => (
        <span>
          {!thoi_gian_hien_tai || thoi_gian_hien_tai <= 0
            ? "-"
            : thoi_gian_hien_tai}
        </span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%" }}>
      <Row>
        <Col span={12}>
          <div>
            <ITitle
              level={1}
              title="Lịch sử khảo sát nhu cầu"
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </div>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo tên câu hỏi">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo tên câu hỏi"
                onPressEnter={(e) => {
                  setLoading(true);
                  setFilterTable({
                    ...filterTable,
                    key: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "32px 0px" }}>
        <Row>
          <Col span={20}>
            <Row>
              <Col>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{
                      marginLeft: 15,
                      marginRight: 15,
                      width: 50,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filterTable.start_date}
                      to={filterTable.end_date}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          start_date: date[0]._d.setHours(0, 0, 0),
                          end_date: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>

      <ITable
        hidePage={true}
        data={data.history_survey}
        columns={columns}
        rowKey={"rowTable"}
        sizeItem={data.size}
        loading={isLoading}
        indexPage={filterTable.page}
        maxpage={data.total}
        bordered={true}
        backgroundWhite={true}
        scroll={{ x: 1500 }}
        onChangePage={(page) => {
          setLoading(true);
          setFilterTable({ ...filterTable, page: page });
        }}
      />
    </div>
  );
}
