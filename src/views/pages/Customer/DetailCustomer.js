import React, {useState, useEffect} from "react";
import {Row, Col, Skeleton, message, Popconfirm, Empty} from "antd";
import {ISvg, IButton, ITableNotFooter} from "../../components";
import {colors} from "../../../assets";
import {StyledITitle} from "../../components/common/Font/font";
import {APIService} from "../../../services";
import {useHistory, useParams} from "react-router-dom";
import "../input.css";
import {priceFormat, renderAddress} from "../../../utils";
import moment from "moment";
import FormatterDay from "../../../utils/FormatterDay";
import {useSelector} from "react-redux";

export default function DetailCustomer(props) {
  const history = useHistory();
  const {id} = useParams();
  const {menu} = useSelector((state) => state)

  const [dataDetail, setDataDetail] = useState({
    calls: [],
    appointments: [],
    surveys: [],
    contracts: [],
  });

  const [isLoading, setIsLoading] = useState(true);
  const [loadingRemove, setLoadingRemove] = useState(false);

  const getDetailCustomer = async (id) => {
    try {
      const data = await APIService._getDetailCustomer(id);

      setDataDetail({...data.detail_user});
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const deleteMember = async (dataDelete) => {
    try {
      const data = await APIService._deleteNhanVien(dataDelete);
      setLoadingRemove(false);
      message.success("Xóa Nhân viên thành công");
      history.push("/salesman/list");
    } catch (error) {
      console.log(error);
      setLoadingRemove(false);
    }
  };

  useEffect(() => {
    getDetailCustomer(id);
  }, []);

  const renderTitle = (title) => {
    return (
      <span
        style={{
          fontWeight: 500,
          fontSize: 14,
        }}
      >
        {title}
      </span>
    );
  };

  const columnsCalls = [
    {
      title: renderTitle("Ngày giờ"),
      key: "date_time_detail",
      dataIndex: "date_time_detail",
      align: "center",
      render: (date_time_detail) => (
        <span style={{textAlign: "left"}}>
          {!date_time_detail ? "-" : date_time_detail}
        </span>
      ),
    },
    {
      title: renderTitle("Thông tin cuộc hẹn"),
      key: "naminfomation_detaile",
      dataIndex: "infomation_detail",
      align: "center",
      render: (infomation_detail) => (
        <span style={{textAlign: "left"}}>
          {!infomation_detail ? "-" : infomation_detail}
        </span>
      ),
    },
  ];

  const columnsSurveys = [
    {
      title: renderTitle("Câu hỏi khảo sát"),
      key: "cau_hoi_khao_sat",
      dataIndex: "cau_hoi_khao_sat",
      align: "center",
      render: (cau_hoi_khao_sat) => (
        <span style={{textAlign: "left"}}>
          {!cau_hoi_khao_sat ? "-" : cau_hoi_khao_sat}
        </span>
      ),
    },
    {
      title: renderTitle("Kết quả"),
      key: "ket_qua",
      dataIndex: "ket_qua",
      align: "center",
      render: (ket_qua) => (
        <span style={{textAlign: "left"}}>
          {!ket_qua
            ? "-"
            : !isNaN(Number(ket_qua))
              ? priceFormat(ket_qua)
              : ket_qua}
        </span>
      ),
    },
  ];

  const columnsAppointments = [
    {
      title: renderTitle("Thời gian hẹn"),
      key: "thoi_gian_hen",
      dataIndex: "thoi_gian_hen",
      align: "center",
      render: (thoi_gian_hen) => (
        <span style={{textAlign: "left"}}>
          {!thoi_gian_hen ? "-" : thoi_gian_hen}
        </span>
      ),
    },
    {
      title: renderTitle("Trạng thái"),
      key: "trang_thai",
      dataIndex: "trang_thai",
      align: "center",
      render: (trang_thai) => (
        <span style={{textAlign: "left"}}>
          {!trang_thai ? "-" : trang_thai}
        </span>
      ),
    },
    {
      title: renderTitle("Ghi chú"),
      key: "ly_do_huy",
      dataIndex: "ly_do_huy",
      align: "center",
      render: (text, row) => (
        <>
          {row.lat !== 0 && row.long_1 !== 0 ? (
            <a
              target="_blank"
              href={
                "https://www.google.com/maps/search/?api=1&query=" +
                row.lat +
                "%2C" +
                row.long_1
              }
            >
              {text}
            </a>
          ) : (
            <span style={{textAlign: "left"}}>{!text ? "-" : text}</span>
          )}
        </>
      ),
    },
  ];

  const columnsContracts = [
    {
      title: renderTitle("Thời gian"),
      key: "thoi_gian",
      dataIndex: "thoi_gian",
      align: "center",
      render: (thoi_gian) => (
        <span style={{textAlign: "left"}}>
          {!thoi_gian ? "-" : thoi_gian}
        </span>
      ),
    },
    {
      title: renderTitle("Trạng thái"),
      key: "trang_thai",
      dataIndex: "trang_thai",
      align: "center",
      render: (trang_thai) => (
        <span style={{textAlign: "left"}}>
          {!trang_thai ? "-" : trang_thai}
        </span>
      ),
    },
    {
      title: renderTitle("Lý do"),
      key: "ly_do",
      dataIndex: "ly_do",
      align: "center",
      render: (ly_do) => (
        <span style={{textAlign: "left"}}>{!ly_do ? "-" : ly_do}</span>
      ),
    },
  ];

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row gutter={[0, 50]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{color: colors.main, fontSize: 22}}>
              Chi tiết khách hàng
            </StyledITitle>
          </div>
        </Col>
        {dataDetail.is_check_update == 1 && (
          <Col span={24}>
              <div className="flex justify-end">
                <IButton
                  color={colors.main}
                  title="Chỉnh sửa"
                  icon={ISvg.NAME.WRITE}
                  onClick={() => {
                    history.push(`/customer/edit/${id}`);
                  }}
                />
              </div>
          </Col>
        )}
        <Col span={24}>
          <Row gutter={[15, 0]} type="flex">
            <Col span={5}>
              <div
                style={{
                  padding: 30,
                  backgroundColor: "white",
                  height: "100%",
                }}
              >
                <Skeleton paragraph={{rows: 6}} active loading={isLoading}>
                  <Row gutter={[0, 20]}>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        Thông tin cá nhân
                      </StyledITitle>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Tên khách hàng</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.name ? "-" : dataDetail.name}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Mã khách hàng</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.user_code ? "-" : dataDetail.user_code}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Điện thoại</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.phone ? "-" : dataDetail.phone}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Địa chỉ</p>
                      <span>{renderAddress(dataDetail.address)}</span>
                    </Col>
                    {dataDetail.user_type_id === 1 ? (
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Mã số thuế</p>
                        <span
                          style={{
                            wordWrap: "break-word",
                            wordBreak: "break-all",
                          }}
                        >
                          {!dataDetail.tax_code ? "-" : dataDetail.tax_code}
                        </span>
                      </Col>
                    ) : (
                      <>
                        <Col span={24}>
                          <p style={{fontWeight: 600}}>CMND/CCCD</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {!dataDetail.cmnd ? "-" : dataDetail.cmnd}
                          </span>
                        </Col>
                        <Col span={24}>
                          <p style={{fontWeight: 600}}>Ngày cấp</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {!dataDetail.date_cmnd ||
                            /1970/g.test(dataDetail.date_cmnd)
                              ? "-"
                              : FormatterDay.dateFormatWithString(
                                dataDetail.date_cmnd,
                                "#DD#/#MM#/#YYYY#"
                              )}
                          </span>
                        </Col>
                        <Col span={24}>
                          <p style={{fontWeight: 600}}>Nơi cấp</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {!dataDetail.issue_cmnd
                              ? "-"
                              : dataDetail.issue_cmnd}
                          </span>
                        </Col>
                      </>
                    )}
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Ngành nghề kinh doanh</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.job ? "-" : dataDetail.job}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Mẫu xe quan tâm</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.product_name_care
                          ? "-"
                          : dataDetail.product_name_care}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>NV phụ trách</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.staff_name ? "-" : dataDetail.staff_name}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Nguồn</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.source ? "-" : dataDetail.source}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Ngày hẹn</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.appointment_date
                          ? "-"
                          : dataDetail.appointment_date}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Phân loại khách hàng</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.type_user ? "-" : dataDetail.type_user}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Kênh kinh doanh</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.business_chanel_name ? "-" : dataDetail.business_chanel_name}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Mã tuyến</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {!dataDetail.route_code ? "-" : dataDetail.route_code}
                      </span>
                    </Col>
                  </Row>
                </Skeleton>
              </div>
            </Col>
            <Col span={19}>
              <div
                style={{
                  padding: 30,
                  backgroundColor: "white",
                  height: "100%",
                }}
              >
                <Skeleton paragraph={{rows: 8}} active loading={isLoading}>
                  <Row gutter={[0, 20]}>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        Lịch sử
                      </StyledITitle>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[15, 0]}>
                        <Col span={12}>
                          <div style={{height: 360}}>
                            <Row gutter={[0, 10]}>
                              <Col span={24}>
                                <div
                                  style={{
                                    height: 42,
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <StyledITitle
                                    style={{
                                      color: "black",
                                      fontSize: 14,
                                      fontWeight: 500,
                                    }}
                                  >
                                    Cuộc gọi
                                  </StyledITitle>
                                </div>
                              </Col>
                              <Col span={24}>
                                <ITableNotFooter
                                  data={dataDetail.calls}
                                  columns={columnsCalls}
                                  hidePage={true}
                                  backgroundWhite={true}
                                  backgroundHeader={colors.green._3}
                                  bordered
                                  hidePage={true}
                                  size="small"
                                  pagination={false}
                                  style={{
                                    width: "100%",
                                    height: 320,
                                    // border:
                                    //   "1px solid rgba(122, 123, 123, 0.3)",
                                    borderRadius: 4,
                                  }}
                                  scroll={{
                                    y: 282,
                                  }}
                                  locale={{
                                    emptyText: (
                                      <Empty description="Không có cuộc gọi"/>
                                    ),
                                  }}
                                />
                              </Col>
                            </Row>
                          </div>
                        </Col>
                        <Col span={12}>
                          <div style={{height: 360}}>
                            <Row gutter={[0, 10]}>
                              <Col span={24}>
                                <Row
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <Col span={12}>
                                    <StyledITitle
                                      style={{
                                        color: "black",
                                        fontSize: 14,
                                        fontWeight: 500,
                                      }}
                                    >
                                      Khảo sát nhu cầu
                                    </StyledITitle>
                                  </Col>
                                  <Col span={12}>
                                    <div className="flex justify-end">
                                      <IButton
                                        color={colors.main}
                                        title="Xem lịch sử"
                                        styleHeight={{width: 140}}
                                        onClick={() => {
                                          history.push(
                                            `/detail/customer/${id}/survey/list`
                                          );
                                        }}
                                      />
                                    </div>
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={24}>
                                <ITableNotFooter
                                  data={dataDetail.surveys}
                                  columns={columnsSurveys}
                                  hidePage={true}
                                  backgroundWhite={true}
                                  backgroundHeader={colors.green._3}
                                  bordered
                                  size="small"
                                  pagination={false}
                                  style={{
                                    width: "100%",
                                    height: 320,
                                    border:
                                      "1px solid rgba(122, 123, 123, 0.3)",
                                    borderRadius: 4,
                                  }}
                                  scroll={{
                                    y: 282,
                                  }}
                                  locale={{
                                    emptyText: (
                                      <Empty description="Không có khảo sát"/>
                                    ),
                                  }}
                                />
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[15, 0]}>
                        <Col span={12}>
                          <div style={{height: 360}}>
                            <Row gutter={[0, 10]}>
                              <Col span={24}>
                                <div
                                  style={{
                                    height: 42,
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <StyledITitle
                                    style={{
                                      color: "black",
                                      fontSize: 14,
                                      fontWeight: 500,
                                    }}
                                  >
                                    Cuộc hẹn
                                  </StyledITitle>
                                </div>
                              </Col>
                              <Col span={24}>
                                <ITableNotFooter
                                  data={dataDetail.appointments}
                                  columns={columnsAppointments}
                                  hidePage={true}
                                  backgroundWhite={true}
                                  backgroundHeader={colors.green._3}
                                  bordered
                                  hidePage={true}
                                  size="small"
                                  pagination={false}
                                  style={{
                                    width: "100%",
                                    height: 320,
                                    border:
                                      "1px solid rgba(122, 123, 123, 0.3)",
                                    borderRadius: 4,
                                  }}
                                  scroll={{
                                    y: 282,
                                  }}
                                  locale={{
                                    emptyText: (
                                      <Empty description="Không có cuộc hẹn"/>
                                    ),
                                  }}
                                />
                              </Col>
                            </Row>
                          </div>
                        </Col>
                        <Col span={12}>
                          <div style={{height: 360}}>
                            <Row gutter={[0, 10]}>
                              <Col span={24}>
                                <div
                                  style={{
                                    height: 42,
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <StyledITitle
                                    style={{
                                      color: "black",
                                      fontSize: 14,
                                      fontWeight: 500,
                                    }}
                                  >
                                    Trạng thái phiếu yêu cầu
                                  </StyledITitle>
                                </div>
                              </Col>
                              <Col span={24}>
                                <ITableNotFooter
                                  data={dataDetail.contracts}
                                  columns={columnsContracts}
                                  hidePage={true}
                                  backgroundWhite={true}
                                  backgroundHeader={colors.green._3}
                                  bordered
                                  hidePage={true}
                                  size="small"
                                  pagination={false}
                                  style={{
                                    width: "100%",
                                    height: 320,
                                    border:
                                      "1px solid rgba(122, 123, 123, 0.3)",
                                    borderRadius: 4,
                                  }}
                                  scroll={{
                                    y: 282,
                                  }}
                                  locale={{
                                    emptyText: (
                                      <Empty description="Không có dữ liệu"/>
                                    ),
                                  }}
                                />
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Skeleton>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
