import {
  Col,
  message,
  Row,
  Form,
  Checkbox,
  Input,
  Skeleton,
  Tag,
  Modal,
  List,
} from "antd";
import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router";
import {colors} from "../../../assets";
import {APIService} from "../../../services";
import moment from "moment";
import {StyledITileHeading} from "../../components/common/Font/font";
import {ITitle, ISvg, ISelect, IButton, IInput} from "../../components";
import {
  IDatePickerFrom,
  IInputText,
  IFormItem,
} from "../../components/common";

const {Search} = Input;

function CreateCustomer(props) {
  const history = useHistory();
  const {id, type, agencyId} = useParams();
  const isCheck = type === "edit";
  const reduxForm = props.form;
  const {getFieldDecorator} = reduxForm;
  const [listQuestion, setListQuestion] = useState([]);
  const [listAns, setListAns] = useState([]);
  const [listdata, setListData] = useState({});
  const [dataCities, setDataCities] = useState([]);
  const [dataDistricts, setDataDistricts] = useState([]);
  const [dataWards, setDataWards] = useState([]);
  const [listSources, setListSources] = useState([]);
  const [listSalesMan, setListSalesMan] = useState([]);
  const [loadingDistricts, setLoadingDistricts] = useState(false);
  const [loadingWards, setLoadingWards] = useState(false);
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [dataProduct, setDataProduct] = useState([]);
  const [keyFindProduct, setKeyFindProduct] = useState("");
  const [isVisiblePopupProduct, setVisiblePopupProduct] = useState(false);
  const [typeUser, setTypeUser] = useState(2);
  const [idOptionProduct, setidOptionProduct] = useState(-1);
  const [listBusiness, setLstBusiness] = useState([])
  const [lstRouter, setLstRouter] = useState([])
  const typeCustomer = [
    {
      key: 1,
      value: "Khách hàng doanh nghiệp",
    },
    {
      key: 2,
      value: "Khách hàng cá nhân",
    },
  ];
  const getListProduct = async (key) => {
    try {
      const response = await APIService._getListProductForCustomer(key);

      let arrProduct = response.data.map((item) => {
        return {
          value: item.name,
          key: item.id,
          checked: false,
        };
      });
      // item.id === idOptionProduct ? true :
      setDataProduct([...arrProduct]);
    } catch (error) {
      console.log(error);
    }
  };
  const _fetchLstBusinessChanel = async () => {
    try {
      const res = await APIService._getLstBussinesChanel()
      let newArr = res.data.map(item => {
        return {
          key: item.id,
          value: item.name
        }
      })
      setLstBusiness(newArr)
    } catch (e) {
      console.log(e)
    }
  }
  const getListQuestion = async () => {
    try {
      const data = await APIService._getListQuestion();

      let questions = data.questions;

      let arr = [];
      let arrAns = [];
      Object.keys(questions).forEach((item) => {
        let idx = -1;
        if (!questions[item].list_question_child.length) {
          idx = 0;
        } else {
          idx = questions[item].list_question_child.findIndex(
            (ans) => ans.radio_or_text_type === 3
          );
        }

        arr.push({
          ...questions[item],
          is_other: idx === 0 ? true : false,
          other_description: "",
          is_have_other: idx === -1 ? false : true,
        });
        arrAns.push({
          answer_id: !questions[item].list_question_child.length
            ? questions[item].id
            : 0,
          question_id: questions[item].id,
          text: "",
          type_question: !questions[item].list_question_child.length ? 2 : 0,
        });
      });

      setListAns([...arrAns]);
      setListQuestion([...arr]);
      return {
        Q: arr,
        A: arrAns,
      };
    } catch (error) {
      console.log({error});
      setLoading(false);
    }
  };
  const getListSource = async () => {
    try {
      const data = await APIService._getListSource();
      const dataNew = data.sources.map((item) => {
        return {key: item.id, value: item.name};
      });
      setListSources([...dataNew]);
    } catch (error) {
      console.log({error});
    }
  };
  const getListSalesMan = async () => {
    try {
      const data = await APIService._getListSalesManDropListHasRouter();
      const dataNew = data.data.map((item) => {
        return {key: item.sale_man_code, value: item.name};
      });

      setListSalesMan(dataNew);
    } catch (error) {
      console.log({error});
    }
  };

  const getListCity = async (search) => {
    try {
      const data = await APIService._getListCities(search);
      const dataNew = data.cities.map((item) => {
        return {
          key: item.id,
          value: item.name,
        };
      });
      setDataCities(dataNew);
    } catch (error) {
      console.log({error});
    }
  };

  const getListDistrict = async (city_id, search) => {
    try {
      setLoadingDistricts(true);
      const data = await APIService._getListCounty(city_id, search);
      const dataNew = data.district.map((item) => {
        return {
          key: item.id,
          value: item.name,
        };
      });
      setDataDistricts(dataNew);
      setLoadingDistricts(false);
    } catch (error) {
      console.log({error});
      setLoadingDistricts(false);
    }
  };

  const getListWard = async (district_id, search) => {
    try {
      setLoadingWards(true);
      const data = await APIService._getListWard(district_id, search);
      const dataNew = data.wards.map((item) => {
        return {
          key: item.id,
          value: item.name,
        };
      });
      setDataWards(dataNew);
      setLoadingWards(false);
    } catch (error) {
      console.log({error});
      setLoadingWards(false);
    }
  };
  const getDetailCustomer = async (id) => {
    try {
      const data = await APIService._getDetailCustomer(id);
      const obj = data.detail_user;
      obj.city_id && (await getListDistrict(obj.city_id, ""));
      obj.district_id && (await getListWard(obj.district_id, ""));
      setListData(data.detail_user);
      setidOptionProduct(obj.product_id);
      setTypeUser(obj.user_type_id);
      reduxForm.setFieldsValue({
        _address: obj.address.split(",")[0],
        _cmnd: obj.cmnd,
        _cccd_create_date:
          obj.date_cmnd && !/1970/g.test(obj.date_cmnd) && obj.date_cmnd !== '-' ? obj.date_cmnd : null,
        // _gender: obj.ten_gioi_tinh == 'Nam' ? 1 : 0,
        _cmnd_create_location:
          obj.issued_cmnd_id == 0 ? null : obj.issued_cmnd_id,
        _email: obj.mail,
        _name: obj.name,
        _phone: obj.phone,
        _route_id: obj.route_id,
        _product_id: obj.product_id,
        _model_interested_name: obj.product_name_care,
        _job: obj.job,
        _sales_man_id: obj.sale_man_code,
        _business_chanel_id: obj.business_chanel_id,
        _source: obj.source_id,
        user_evaluate_id: obj.user_evaluate_id,
        _city_id: obj.city_id == 0 ? null : obj.city_id,
        _district_id: obj.district_id == 0 ? null : obj.district_id,
        _ward_id: obj.ward_id == 0 ? null : obj.ward_id,
        _user_type_id: obj.user_type_id,
        _tax_code: obj.tax_code,
      });
      let dataCheckRouter = []
      if (obj.business_chanel_id) dataCheckRouter = await _fetchApiRouter(obj.business_chanel_id)
      if (dataCheckRouter.filter(item => item.key === obj.route_id).length === 0) {
        message.warning("Mã tuyến đã ngưng hoạt động, vui lòng chọn mã tuyến khác!")
        reduxForm.setFieldsValue({
          _route_id: undefined
        })
        reduxForm.setFieldsValue({
          _sales_man_id: undefined
        });
      }
      return obj.surveys;
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };
  const _fetchApiRouter = async (id, editing) => {
    try {
      const res = await APIService._getLstRouterCreateUser(id)
      let newData = res.list.map(item => {
        return {
          key: item.id,
          value: item.route_code,
          sale_code: item.sale_code,
          sale_name: item.sale_name
        }
      })
      if (editing) {
        reduxForm.setFieldsValue({
          _route_id: undefined
        })
        reduxForm.setFieldsValue({
          _sales_man_id: undefined
        });
      }
      setLstRouter(newData)
      return newData
    } catch (e) {
      console.log(e)
    }
  }
  const callAPI = async () => {
    let question = await getListQuestion();
    await getListSource();
    await _fetchLstBusinessChanel()
    await getListSalesMan();
    await getListCity("");
    if (isCheck) {
      let data = await getDetailCustomer(id);
      if (typeof question == "object" && typeof data == "object") {
        let newArr = data.map((item) => {
          return {
            answer_id: item.answer_id,
            question_id: item.question_id,
            text: item.ket_qua,
            type_question: item.radio_or_text_type,
          };
        });
        let ids = new Set(newArr.map((d) => d.question_id));
        let merged = [
          ...newArr,
          ...question.A.filter((d) => !ids.has(d.question_id)),
        ];
        let road = question.Q.map((item) => {
          if (item.radio_or_text_type === 1) {
            if (
              merged.find(
                (item1) =>
                  item1.question_id === item.id && item1.type_question === 1
              ) &&
              item.list_question_child.find(
                (item2) =>
                  item2.id ===
                  merged.find(
                    (item1) =>
                      item1.question_id === item.id &&
                      item1.type_question === 1
                  ).answer_id && item2.radio_or_text_type == 3
              )
            ) {
              item.is_other = true;
            }
          }
          return item;
        });
        setListAns(merged);
        setListQuestion(road);
      }
    }

    setLoading(false);
  };
  useEffect(() => {
    callAPI();
  }, []);

  useEffect(() => {
    getListProduct(keyFindProduct);
  }, [keyFindProduct]);

  const postCreateCustomer = async (obj) => {
    try {
      setLoadingCreate(true);
      const data = await APIService._postCreateCustomer(obj);
      message.success("Thêm khách hàng thành công");
      setLoadingCreate(false);
      history.push("/customer/list");
    } catch (error) {
      console.log(error);
      setLoadingCreate(false);
    }
  };
  const editSaleManCustomer = async (obj) => {
    try {
      setLoadingCreate(true);
      const data = await APIService._editCustomer(obj);
      setLoadingCreate(false);
      message.success("Chỉnh sửa khách hàng thành công");
      history.push(`/detail/customer/${id}`);
    } catch (err) {
      setLoadingCreate(false);
      console.log(err);
    }
  };
  const onSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields((err, obj) => {
      if (!isCheck && idOptionProduct === -1) {
        return;
      }
      if (!err) {
        let listAnsNew = listAns.map((item) => {
          if (item.text.match(/[a-z]/i)) {
            return item;
          } else {
            return {...item, text: Number(item.text.split(",").join(""))};
          }
        });
        if (isCheck) {
          let objAdd = {
            id,
            sale_man_code: obj._sales_man_id ? obj._sales_man_id : "",
            address: obj._address ? obj._address : "",
            mail: obj._email,
            name: obj._name,
            phone: obj._phone,
            product_name_care: idOptionProduct,
            profession: obj._job ? obj._job : "",
            quests: listAnsNew,
            source_id: obj._source,
            route_id: obj._route_id,
            user_evaluate_id: 1,
            city_id: obj._city_id ? obj._city_id : "",
            district_id: obj._district_id ? obj._district_id : "",
            ward_id: obj._ward_id ? obj._ward_id : "",
            user_type_id: obj._user_type_id,
            cmnd: obj._user_type_id !== 1 ? (obj._cmnd ? obj._cmnd : "") : "",
            date_cmnd: obj._user_type_id !== 1 ? /-/g.test(obj._cccd_create_date) ? new Date(obj._cccd_create_date).getTime() : obj._cccd_create_date : "",
            issus_cmnd_id:
              obj._user_type_id !== 1
                ? obj._cmnd_create_location
                ? obj._cmnd_create_location
                : ""
                : "",
            tax_code: obj._user_type_id !== 1 ? "" : obj._tax_code,
          };
          editSaleManCustomer(objAdd);
        } else {
          const objAdd = {
            sale_man_code: obj._sales_man_id ? obj._sales_man_id : "",
            address: obj._address ? obj._address : "",
            mail: obj._email,
            name: obj._name,
            route_id: obj._route_id,
            phone: obj._phone,
            product_name_care: idOptionProduct,
            profession: obj._job ? obj._job : "",
            quests: listAnsNew,
            source_id: obj._source,
            // user_evaluate_id: 1,
            city_id: obj._city_id ? obj._city_id : "",
            district_id: obj._district_id ? obj._district_id : "",
            ward_id: obj._ward_id ? obj._ward_id : "",
            user_type_id: obj._user_type_id,
            cmnd: obj._user_type_id !== 1 ? obj._cmnd : "",
            date_cmnd: obj._user_type_id !== 1 ? obj._cccd_create_date : "",
            issus_cmnd_id:
              obj._user_type_id !== 1
                ? obj._cmnd_create_location
                ? obj._cmnd_create_location
                : ""
                : "",
            tax_code: obj._user_type_id !== 1 ? "" : obj._tax_code,
          };
          postCreateCustomer(objAdd);
        }
      }
    });
  };

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate());
    return startValue.valueOf() > dateNow.getTime();
  };
  return (
    <div style={{width: "100%"}}>
      <Row>
        <Col span={24}>
          <ITitle
            level={1}
            title={isCheck ? "Chỉnh sửa khách hàng" : "Tạo khách hàng mới"}
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
      </Row>
      <div style={{margin: "20px 0px"}}>
        <Form onSubmit={onSubmit}>
          <Row>
            <Col span={24}>
              <div className="flex justify-end">
                <IButton
                  color={colors.main}
                  title="Lưu"
                  loading={loadingCreate}
                  icon={ISvg.NAME.SAVE}
                  style={{marginRight: 15}}
                  htmlType="submit"
                />

                <IButton
                  color={colors.oranges}
                  title="Hủy"
                  icon={ISvg.NAME.CROSS}
                  onClick={() =>
                    history.push(
                      isCheck ? `/detail/customer/${id}` : `/customer/list`
                    )
                  }
                />
              </div>
            </Col>
          </Row>
          <div
            style={{
              marginTop: 20,
              width: 1100,
            }}
          >
            <Row gutter={[12, 0]} type="flex">
              <Col span={14}>
                <div
                  style={{
                    height: "100%",
                    padding: 30,
                    background: "white",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <Skeleton active paragraph={{rows: 12}} loading={isLoading}>
                    <Row gutter={[0, 24]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="12px" maxFont="16px">
                          Thông tin khách hàng
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>
                                Loại khách hàng
                                <span style={{marginLeft: 5, color: "red"}}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("_user_type_id", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "vui lòng chọn loại khách hàng",
                                    },
                                  ],
                                })(
                                  <ISelect
                                    placeholder="chọn loại khách hàng"
                                    select={true}
                                    data={typeCustomer}
                                    onChange={(e) => {
                                      setTypeUser(e);
                                      reduxForm.setFieldsValue({
                                        _user_type_id: e,
                                      });
                                    }}
                                    style={{
                                      background: colors.white,
                                      borderStyle: "solid",
                                      borderWidth: 0,
                                      borderColor: colors.line,
                                      borderBottomWidth: 1,
                                      marginTop: 1,
                                    }}
                                    className="clear-pad"
                                    type="write"
                                    isBackground={false}
                                  />
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>
                                Tên khách hàng
                                <span style={{marginLeft: 5, color: "red"}}>
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("_name", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "vui lòng nhập tên khách hàng",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    placeholder="nhập tên khách hàng"
                                    value={reduxForm.getFieldValue("_name")}
                                    disabled={loadingCreate}
                                  />
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <div>
                              <p style={{fontWeight: 500}}>
                                Điện thoại
                                <span
                                  style={{
                                    marginLeft: 5,
                                    color: "red",
                                  }}
                                >
                                  *
                                </span>
                              </p>
                              <IFormItem>
                                {getFieldDecorator("_phone", {
                                  rules: [
                                    {
                                      required: true,
                                      pattern: new RegExp(/0[0-9]([0-9]{8})\b/),
                                      message:
                                        "nhập tối đa 10 số, không được nhập chữ, khoảng cách và ký tự đặc biệt",
                                    },
                                  ],
                                })(
                                  <IInput
                                    disabled={loadingCreate}
                                    loai="input"
                                    maxLength={10}
                                    onKeyPress={(e) => {
                                      var char = String.fromCharCode(e.which);
                                      if (!/[0-9]/.test(char)) {
                                        e.preventDefault();
                                      }
                                    }}
                                    placeholder="nhập số điện thoại"
                                    value={reduxForm.getFieldValue("_phone")}
                                  />
                                )}
                              </IFormItem>
                            </div>
                          </Col>
                          {typeUser === 1 ? (
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Mã số thuế
                                  <span style={{marginLeft: 5, color: "red"}}>
                                    *
                                  </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_tax_code", {
                                    initialValue: listdata.tax_code,
                                    rules: [
                                      {
                                        required: typeUser === 1,
                                        message: "vui lòng nhập mã số thuế",
                                      },
                                    ],
                                  })(
                                    <IInput
                                      loai="input"
                                      disabled={loadingCreate}
                                      // onKeyPress={(e) => {
                                      //   var char = String.fromCharCode(e.which);
                                      //   if (!/[0-9]/.test(char)) {
                                      //     e.preventDefault();
                                      //   }
                                      // }}
                                      placeholder="nhập mã số thuế"
                                      value={reduxForm.getFieldValue(
                                        "_tax_code"
                                      )}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          ) : (
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  CMND/CCCD
                                  {/*  <span style={{marginLeft: 5, color: "red"}}>*/}
                                  {/*  **/}
                                  {/*</span>*/}
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_cmnd", {
                                    initialValue: listdata.cmnd,
                                    // rules: [
                                    //   {
                                    //     required: true,
                                    //     message: "vui lòng nhập CMND/CCCD",
                                    //   },
                                    // ],
                                  })(
                                    <IInput
                                      loai="input"
                                      disabled={loadingCreate}
                                      onKeyPress={(e) => {
                                        var char = String.fromCharCode(e.which);
                                        if (!/[0-9]/.test(char)) {
                                          e.preventDefault();
                                        }
                                      }}
                                      placeholder="nhập CMND/CCCD"
                                      value={reduxForm.getFieldValue("_cmnd")}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          )}
                        </Row>
                      </Col>
                      {typeUser !== 1 && (
                        <Col span={24}>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>Ngày cấp</p>
                                <IFormItem>
                                  {getFieldDecorator("_cccd_create_date", {
                                    initialValue: listdata.date_cmnd
                                      ? new Date(listdata.date_cmnd).getTime()
                                      : 0,
                                  })(
                                    <div>
                                      <IDatePickerFrom
                                        bordered={false}
                                        paddingInput="0px"
                                        isBorderBottom
                                        style={{
                                          paddingBottom: 5,
                                          marginTop: 4,
                                        }}
                                        onChange={(date, dateString) => {
                                          reduxForm.setFieldsValue({
                                            _cccd_create_date: date._d.setHours(
                                              0,
                                              0,
                                              0
                                            ),
                                          });
                                        }}
                                        showTime={false}
                                        defaultValue={
                                          isCheck
                                            ? reduxForm.getFieldValue(
                                            "_cccd_create_date"
                                            ) !== null
                                            ? moment(
                                              new Date(
                                                reduxForm.getFieldValue(
                                                  "_cccd_create_date"
                                                )
                                              ),
                                              "DD-MM-YYYY"
                                            )
                                            : ""
                                            : undefined
                                        }
                                        disabled={loadingCreate}
                                        disabledDate={disabledStartDateApply}
                                        placeholder="nhập ngày cấp CMND/CCCD"
                                      />
                                    </div>
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            <Col span={12}>
                              <p style={{fontWeight: 500}}>Nơi cấp</p>
                              <IFormItem>
                                {getFieldDecorator("_cmnd_create_location", {
                                  initialValue: listdata.issued_cmnd_id,
                                })(
                                  <ISelect
                                    className="clear-pad"
                                    type="write"
                                    select={true}
                                    style={{
                                      background: colors.white,
                                      borderStyle: "solid",
                                      borderWidth: 0,
                                      borderColor: colors.line,
                                      borderBottomWidth: 1,
                                      marginTop: 1,
                                    }}
                                    isBackground={false}
                                    onChange={(key) => {
                                      reduxForm.setFieldsValue({
                                        _cmnd_create_location: key,
                                      });
                                    }}
                                    value={reduxForm.getFieldValue(
                                      "_cmnd_create_location"
                                    )}
                                    placeholder="chọn nơi cấp CMND/CCCD"
                                    data={dataCities}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                      )}
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>Tỉnh/Thành</p>
                                <IFormItem>
                                  {getFieldDecorator(
                                    "_city_id",
                                    {}
                                  )(
                                    <ISelect
                                      className="clear-pad"
                                      type="write"
                                      select={true}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      isBackground={false}
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          _city_id: key,
                                          _district_id: undefined,
                                          _ward_id: undefined,
                                        });

                                        getListDistrict(key, "");
                                      }}
                                      value={reduxForm.getFieldValue(
                                        "_city_id"
                                      )}
                                      placeholder="chọn tỉnh/thành"
                                      data={dataCities}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>Quận/Huyện</p>
                                <IFormItem>
                                  {getFieldDecorator(
                                    "_district_id",
                                    {}
                                  )(
                                    <ISelect
                                      className="clear-pad"
                                      type="write"
                                      select={
                                        loadingDistricts ||
                                        !reduxForm.getFieldValue("_city_id")
                                          ? false
                                          : true
                                      }
                                      loading={loadingDistricts}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      isBackground={false}
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          _district_id: key,
                                          _ward_id: undefined,
                                        });

                                        getListWard(key, "");
                                      }}
                                      value={reduxForm.getFieldValue(
                                        "_district_id"
                                      )}
                                      placeholder="chọn quận/huyện"
                                      data={dataDistricts}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>Phường/Xã</p>
                                <IFormItem>
                                  {getFieldDecorator(
                                    "_ward_id",
                                    {}
                                  )(
                                    <ISelect
                                      className="clear-pad"
                                      type="write"
                                      select={
                                        loadingWards ||
                                        !reduxForm.getFieldValue("_district_id")
                                          ? false
                                          : true
                                      }
                                      loading={loadingWards}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      isBackground={false}
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          _ward_id: key,
                                        });
                                      }}
                                      value={reduxForm.getFieldValue(
                                        "_ward_id"
                                      )}
                                      placeholder="chọn phường/xã"
                                      data={dataWards}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Ngành nghề kinh doanh
                                </p>
                                <IFormItem>
                                  {getFieldDecorator(
                                    "_job",
                                    {}
                                  )(
                                    <IInputText
                                      disabled={loadingCreate}
                                      placeholder="nhập ngành nghề kinh doanh"
                                      value={reduxForm.getFieldValue("_job")}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>Địa chỉ</p>
                                <IFormItem>
                                  {getFieldDecorator(
                                    "_address",
                                    {}
                                  )(
                                    <IInputText
                                      disabled={loadingCreate}
                                      placeholder="nhập địa chỉ"
                                      value={reduxForm.getFieldValue(
                                        "_address"
                                      )}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Mẫu xe quan tâm
                                  <span
                                    style={{
                                      marginLeft: 5,
                                      marginRight: 5,
                                      color: "red",
                                    }}
                                  >
                                    *
                                  </span>
                                  <Tag
                                    color="geekblue"
                                    onClick={() => {
                                      setVisiblePopupProduct(true);
                                      if (isCheck) {
                                        let arrProduct = dataProduct.map(
                                          (item) => {
                                            item.checked =
                                              item.key === idOptionProduct
                                                ? true
                                                : false;
                                            return item;
                                          }
                                        );
                                        setDataProduct(arrProduct);
                                      }
                                    }}
                                  >
                                    Chọn sản phẩm
                                  </Tag>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_model_interested_name", {
                                    rules: [
                                      {
                                        required: true,
                                        message:
                                          "vui lòng nhập mẫu xe quan tâm",
                                      },
                                    ],
                                  })(
                                    <IInputText
                                      disabled={true}
                                      placeholder="chọn mẫu xe quan tâm"
                                      value={reduxForm.getFieldValue(
                                        "_model_interested_name"
                                      )}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>

                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>Kênh kinh doanh
                                  <span
                                    style={{
                                      marginLeft: 5,
                                      marginRight: 5,
                                      color: "red",
                                    }}
                                  >
                                    *
                                  </span></p>
                                <IFormItem>
                                  {getFieldDecorator(
                                    "_business_chanel_id",
                                    {}
                                  )(
                                    <ISelect
                                      data={listBusiness}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      isBackground={false}
                                      select={true}
                                      placeholder="Chọn kênh kinh doanh"
                                      onChange={key => {
                                        _fetchApiRouter(key, true)
                                      }}
                                      value={reduxForm.getFieldValue(
                                        "_business_chanel_id"
                                      )}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Mã tuyến
                                  <span
                                    style={{
                                      marginLeft: 5,
                                      marginRight: 5,
                                      color: "red",
                                    }}
                                  >
                                    *
                                  </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_route_id", {
                                    rules: [
                                      {
                                        required: true,
                                        message:
                                          "vui lòng chọn mã tuyến",
                                      },
                                    ],
                                  })(
                                    <ISelect
                                      data={lstRouter}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      isBackground={false}
                                      select={reduxForm.getFieldValue('_business_chanel_id')}
                                      onChange={e => {
                                        reduxForm.setFieldsValue({
                                          _route_id: e
                                        })
                                        let indx = lstRouter.find(item => item.key === e)
                                        if (indx) {
                                          reduxForm.setFieldsValue({
                                            _sales_man_id: indx.sale_code,
                                          });
                                        }
                                      }}
                                      placeholder="chọn mã tuyến"
                                      value={reduxForm.getFieldValue(
                                        "_route_id"
                                      )}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>

                      <Col span={24}>
                        <div>
                          <Row gutter={[12, 0]}>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Nhân viên phụ trách
                                  <span style={{marginLeft: 5, color: "red"}}>
                                    *
                                  </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_sales_man_id", {
                                    rules: [
                                      {
                                        required: true,
                                        message:
                                          "vui lòng chọn nhân viên phụ trách",
                                      },
                                    ],
                                  })(
                                    <ISelect
                                      className="clear-pad"
                                      type="write"
                                      select={false}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      isBackground={false}
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          _sales_man_id: key,
                                        });
                                      }}
                                      value={reduxForm.getFieldValue(
                                        "_sales_man_id"
                                      )}
                                      placeholder="chọn nhân viên phụ trách"
                                      data={listSalesMan}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                            <Col span={12}>
                              <div>
                                <p style={{fontWeight: 500}}>
                                  Nguồn
                                  <span style={{marginLeft: 5, color: "red"}}>
                                    *
                                  </span>
                                </p>
                                <IFormItem>
                                  {getFieldDecorator("_source", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "vui lòng chọn nguồn",
                                      },
                                    ],
                                  })(
                                    <ISelect
                                      className="clear-pad"
                                      type="write"
                                      select={true}
                                      style={{
                                        background: colors.white,
                                        borderStyle: "solid",
                                        borderWidth: 0,
                                        borderColor: colors.line,
                                        borderBottomWidth: 1,
                                        marginTop: 1,
                                      }}
                                      isBackground={false}
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          _source: key,
                                        });
                                      }}
                                      value={reduxForm.getFieldValue("_source")}
                                      placeholder="chọn nguồn"
                                      data={listSources}
                                    />
                                  )}
                                </IFormItem>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
              {/*{!isCheck && (*/}
              <Col span={10}>
                <div
                  style={{
                    height: "100%",
                    padding: 30,
                    background: "white",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <Skeleton active paragraph={{rows: 12}} loading={isLoading}>
                    <Row gutter={[0, 20]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="12px" maxFont="16px">
                          Khảo sát nhu cầu
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <Row>
                          <div
                            style={{
                              maxHeight: 900,
                              overflowY: "auto",
                              overflowX: "hidden",
                            }}
                          >
                            {listQuestion.map((question, idxQuestion) => {
                              return (
                                <Col span={24}>
                                  <div
                                    style={{
                                      display: "flex",
                                    }}
                                  >
                                    <div
                                      style={{
                                        width: 20,
                                        display: "flex",
                                        flexDirection: "column",
                                        alignItems: "center",
                                        marginRight: 8,
                                      }}
                                    >
                                      <div
                                        style={{
                                          display: "flex",
                                          justifyContent: "center",
                                        }}
                                      >
                                        <div
                                          style={{
                                            background: "white",
                                            border: "1px solid black",
                                            width: 20,
                                            height: 20,
                                            borderRadius: 10,
                                            display: "flex",
                                            justifyContent: "center",
                                            alignItems: "center",
                                            fontWeight: 500,
                                          }}
                                        >
                                          {idxQuestion + 1}
                                        </div>
                                      </div>
                                      <div
                                        style={
                                          idxQuestion < listQuestion.length - 1
                                            ? {
                                              width: 1,
                                              borderLeft: "1px solid black",
                                              height: "100%",
                                            }
                                            : null
                                        }
                                      ></div>
                                    </div>
                                    <div
                                      style={{
                                        width: "100%",
                                        paddingRight: 10,
                                      }}
                                    >
                                      <div>
                                        <p style={{fontWeight: 500}}>
                                          {question.description}
                                        </p>
                                      </div>
                                      <div
                                        style={{
                                          paddingBottom:
                                            !question.is_other &&
                                            listQuestion.length - 1 !==
                                            idxQuestion
                                              ? 10
                                              : 0,
                                        }}
                                      >
                                        {question.list_question_child.map(
                                          (answer, idxAnswer) => {
                                            return (
                                              <div
                                                style={{
                                                  display: "flex",
                                                  flexDirection: "row",
                                                  paddingTop: "15px",
                                                }}
                                              >
                                                <div
                                                  style={{
                                                    display: "flex",
                                                    cursor: "pointer",
                                                  }}
                                                  onClick={() => {
                                                    let listAnsTmp = [
                                                      ...listAns,
                                                    ];

                                                    let listQuestionTmp = [
                                                      ...listQuestion,
                                                    ];

                                                    if (
                                                      answer.radio_or_text_type ===
                                                      3
                                                    ) {
                                                      listQuestionTmp[
                                                        idxQuestion
                                                        ].is_other = true;
                                                      setListQuestion([
                                                        ...listQuestionTmp,
                                                      ]);
                                                    } else {
                                                      listQuestionTmp[
                                                        idxQuestion
                                                        ].is_other = false;

                                                      listAnsTmp[
                                                        idxQuestion
                                                        ].text = "";
                                                      setListQuestion([
                                                        ...listQuestionTmp,
                                                      ]);
                                                    }

                                                    listAnsTmp[
                                                      idxQuestion
                                                      ].answer_id = answer.id;

                                                    listAnsTmp[
                                                      idxQuestion
                                                      ].type_question =
                                                      answer.radio_or_text_type;
                                                    setListAns([...listAnsTmp]);
                                                  }}
                                                >
                                                  <div
                                                    style={{
                                                      width: 20,
                                                      height: 20,
                                                      border:
                                                        answer.id ===
                                                        listAns[idxQuestion]
                                                          .answer_id
                                                          ? "5px solid" +
                                                          `${colors.main}`
                                                          : "1px solid" +
                                                          `${colors.line_2}`,
                                                      borderRadius: 10,
                                                    }}
                                                  />
                                                  <div
                                                    style={{
                                                      flex: 1,
                                                      marginLeft: 10,
                                                    }}
                                                  >
                                                    <span
                                                      style={{
                                                        fontSize: 14,
                                                        color:
                                                        colors.text.black,
                                                      }}
                                                    >
                                                      {answer.description}
                                                    </span>
                                                  </div>
                                                </div>
                                              </div>
                                            );
                                          }
                                        )}
                                      </div>
                                      <div>
                                        {!question.is_have_other ||
                                        !question.is_other ? null : (
                                          <div style={{marginBottom: 5}}>
                                            <IInput
                                              style={{
                                                color: "#000",
                                                marginTop: 12,
                                                minHeight: 50,
                                                resize: question.is_other
                                                  ? ""
                                                  : "none",
                                              }}
                                              disabled={
                                                question.is_other ? false : true
                                              }
                                              className="input-border"
                                              loai="textArea"
                                              placeholder="Câu trả lời của bạn..."
                                              onChange={(e) => {
                                                let listAnsTmp = [...listAns];

                                                listAnsTmp[idxQuestion].text =
                                                  e.target.value;
                                                setListAns([...listAnsTmp]);
                                              }}
                                              value={
                                                !listAns[
                                                  idxQuestion
                                                  ].text.match(/[a-z]/i)
                                                  ? listAns[idxQuestion].text
                                                    .replace(/\D/g, "")
                                                    .replace(
                                                      /\B(?=(\d{3})+(?!\d))/g,
                                                      ","
                                                    )
                                                  : listAns[idxQuestion].text
                                              }
                                            />
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                </Col>
                              );
                            })}
                          </div>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
              {/*)}*/}
            </Row>
          </div>
        </Form>
      </div>
      <Modal
        visible={isVisiblePopupProduct}
        width={400}
        footer={null}
        closable={false}
        maskClosable={false}
        height={500}
        bodyStyle={{padding: 25, borderRadius: 0}}
        centered
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{textAlign: "center"}}>
                <ITitle
                  style={{
                    color: colors.blackChart,
                    fontSize: 16,
                    fontWeight: 600,
                  }}
                  title="Tìm kiếm sản phẩm"
                />
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: "100%",
                  marginTop: 5,
                  textAlign: "center",
                  fontWeight: 600,
                  color: "#000",
                }}
              >
                <Search
                  placeholder="nhập tên sản phẩm"
                  onChange={(e) => {
                    let keySearch = e.target.value;
                    setKeyFindProduct(keySearch);
                  }}
                />
                <div
                  style={{
                    border: "1px solid #bfbfbf",
                    height: 370,
                    overflow: "auto",
                    marginTop: 20,
                  }}
                >
                  <List
                    itemLayout="horizontal"
                    dataSource={dataProduct}
                    renderItem={(item, index) => (
                      <List.Item>
                        <List.Item.Meta
                          style={{
                            textAlign: "left",
                            marginLeft: 6,
                            marginRight: 6,
                          }}
                          avatar={
                            <Checkbox
                              checked={item.checked ? true : false}
                              onChange={(e) => {
                                let valueChecked = e.target.checked;
                                // debugger;
                                let lstProduct = dataProduct.map(
                                  (itemPro, indexPro) => {
                                    itemPro.checked = false;
                                    if (itemPro.key === item.key) {
                                      itemPro.checked = valueChecked;
                                    }
                                    return itemPro;
                                  }
                                );

                                setDataProduct(lstProduct);
                              }}
                            />
                          }
                          title={item.value}
                        />
                      </List.Item>
                    )}
                  />
                </div>
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -60,
              right: -1,
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              width: "100%",
            }}
          >
            <IButton
              color={colors.oranges}
              title="Hủy bỏ"
              styleHeight={{minWidth: 120}}
              style={{borderRadius: 5}}
              onClick={(e) => {
                e.preventDefault();

                let arr = dataProduct.map((item, index) => {
                  item.checked = false;
                  if (item.key === idOptionProduct) {
                    item.checked = true;
                  }
                  return item;
                });

                let arrProductCheck = arr.filter(
                  (item) => item.checked === true
                );

                if (arrProductCheck.length === 0) {
                  reduxForm.setFieldsValue({
                    _model_interested_name: "",
                  });
                  setidOptionProduct(-1);
                }

                setDataProduct(arr);
                setVisiblePopupProduct(false);
              }}
              icon={ISvg.NAME.CROSS}
            />
            <IButton
              color={colors.main}
              title="Xác nhận"
              styleHeight={{minWidth: 120}}
              icon={ISvg.NAME.CHECKMARK}
              style={{borderRadius: 5}}
              onClick={() => {
                let arrProductCheck = dataProduct.filter(
                  (item) => item.checked === true
                );

                if (arrProductCheck.length > 1) {
                  message.warning("Vui lòng chỉ chọn 1 sản phẩm!");
                } else if (arrProductCheck.length === 0) {
                  message.warning("Vui lòng chọn 1 sản phẩm!");
                } else {
                  let idProduct = arrProductCheck[0].key;
                  setidOptionProduct(idProduct);

                  let nameProduct = arrProductCheck[0].value;
                  reduxForm.setFieldsValue({
                    _model_interested_name: nameProduct,
                  });
                  setVisiblePopupProduct(false);
                }
              }}
            />
          </div>
        </div>
      </Modal>
    </div>
  );
}

const createCustomer = Form.create({
  name: "CreateCustomer",
  mapPropsToFields: (props) => {
    return {
      _user_type_id: Form.createFormField({
        value: 2,
      }),
    };
  },
})(CreateCustomer);

export default createCustomer;
