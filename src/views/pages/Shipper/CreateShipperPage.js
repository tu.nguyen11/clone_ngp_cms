import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  IInput,
  IUpload,
  ISelectStatus,
  ISelectAllStatus,
  ISelectWareHouse,
} from "../../components";
import { colors } from "../../../assets";
import { message, Button, Form, DatePicker, Upload } from "antd";
import { ImageType } from "../../../constant";
import { APIService } from "../../../services";

class CreateShipperPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errEnd: true,
      errStart: true,
      dateEnd: 0,
      dateStart: 0,
      errStatus: true,
      status: 0,
      errWareHouseConTract: true,
      type_warehouse: 0,
      imgAdd: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onOkDatePickerStart = this.onOkDatePickerStart.bind(this);
    this.onOkDatePickerEnd = this.onOkDatePickerEnd.bind(this);
    this.onChangeStatus = this.onChangeStatus.bind(this);
  }
  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      const ObjectAdd = {
        shipper: {
          id: 0,
          name: fieldsValue.nameShipper,
          code: fieldsValue.codeShipper,
          phone: fieldsValue.phoneShipper,
          email: fieldsValue.emailShipper,
          cmnd: fieldsValue.CMNDShipper,
          address: fieldsValue.addressShipper,
          image: this.state.imgAdd,
          status: 1, // chó nghĩa
        },
        contract: {
          id: 0,
          contract_number: fieldsValue.codeContract,
          start_date: this.state.dateStart,
          end_date: this.state.dateEnd,
          status: this.state.status,
          warehouse_id: this.state.type_warehouse,
        },
      };

      this._postAPIAddShipper(ObjectAdd);
    });
  };

  _postAPIAddShipper = async (obj) => {
    try {
      const data = await APIService._postAddShipper(obj);

      message.success("Thêm shipper thành công");
      this.props.history.push("/shipperPage");
    } catch (err) {
      console.log(err);
    }
  };

  onOkDatePickerStart = (date, dateString) => {
    this.setState(
      {
        dateStart: Date.parse(dateString),
        errStart: false,
      },
      () => {
        this.props.form.validateFields(["dateBeginContract"], { force: true });
      }
    );
  };

  onOkDatePickerEnd = (date, dateString) => {
    this.setState(
      {
        dateEnd: Date.parse(dateString),
        errEnd: false,
      },
      () => {
        this.props.form.validateFields(["dateEndContract"], { force: true });
      }
    );
  };

  onChangeStatus = (value) => {
    // this.state.objAdd.status = Number(value);
    this.setState({ status: value, errStatus: false }, () =>
      this.props.form.validateFields(["statusContract"], { force: true })
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Container fluid style={{ flex: 1 }}>
        {/* <Col className="d-flex flex-column" style={{ height: "100%" }}> */}
        <Row className="mt-3">
          <ITitle
            level={1}
            title="Tạo shipper mới"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
            }}
          />
        </Row>
        <Form onSubmit={this.handleSubmit}>
          <Row className="m-0 p-0 mt-4">
            <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}></Row>
            <Row>
              <IButton
                icon={ISvg.NAME.TIMEREVERSE}
                title={"Lưu"}
                htmlType="submit"
                color={colors.main}
              />
              <IButton
                icon={ISvg.NAME.CROSS}
                title={"Hủy"}
                color={colors.oranges}
                style={{ marginLeft: 20 }}
                onClick={() => {
                  this.props.history.push("/shipperPage");
                }}
              />
            </Row>
          </Row>
          <div style={{}}>
            <Row style={{ height: "100%" }} className="p-0">
              <Col xs={5} style={{ background: colors.white }}>
                <div style={{ marginLeft: 40, marginRight: 40 }}>
                  <ITitle
                    level={4}
                    title="Thông tin Shipper"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      marginTop: 30,
                      marginBottom: 26,
                    }}
                  />
                  <Row className="p-0 m-0 mt-0">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("nameShipper", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập tên shipper",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Tên shipper"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <IInput placeholder="nhập tên shipper" />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>

                    <Col xs="auto">
                      <div style={{ width: 4 }}></div>
                    </Col>
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("codeShipper", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập mã shipper",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0" style={{ flex: 1 }}>
                            <ITitle
                              level={4}
                              title={"Mã shipper"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <IInput placeholder="nhập mã shipper" />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="p-0 m-0 mt-0">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("phoneShipper", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập số điện thoại",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Điện thoại"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <IInput placeholder="nhập số điện thoại" />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>

                    <Col xs="auto">
                      <div style={{ width: 4 }}></div>
                    </Col>
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("emailShipper", {
                          rules: [
                            {
                              type: "email",
                              message: "không đúng định dạng (abc@gmail.com)",
                            },
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập địa chỉ email",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Email"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <IInput placeholder="nhập địa chỉ email" />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="p-0 m-0 mt-0">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("CMNDShipper", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập số chứng minh",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"CMND"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <IInput placeholder="nhập số chứng minh" />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>

                    <Col xs="auto">
                      <div style={{ width: 4 }}></div>
                    </Col>
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("addressShipper", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập địa chỉ liên hệ",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Địa chỉ"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <IInput placeholder="nhập địa chỉ liên hệ" />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>

                  <div style={{ height: 32 }}></div>
                  <ITitle
                    level={4}
                    title="Thông tin hợp đồng"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      marginBottom: 26,
                    }}
                  />

                  <Row className="p-0 m-0 mt-0">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("codeContract", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập số hợp đồng",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Số hợp đồng"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <IInput placeholder="nhập số hợp đồng" />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>

                    <Col xs="auto">
                      <div style={{ width: 4 }}></div>
                    </Col>
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("warehouseContract", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errWareHouseConTract,
                              message: "chọn tên kho",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Kho phụ trách"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <ISelectWareHouse
                              style={{
                                background: colors.white,
                                borderStyle: "solid",
                                borderWidth: 0,
                                borderColor: colors.line,
                                borderBottomWidth: 1,
                                width: 100,
                              }}
                              onSearch={() => {}}
                              filterOption={(input, option) =>
                                option.props.children
                                  .toLowerCase()
                                  .indexOf(input.toLowerCase()) >= 0
                              }
                              onChange={(key) => {
                                this.setState(
                                  {
                                    type_warehouse: key,
                                    errWareHouseConTract: false,
                                  },
                                  () =>
                                    this.props.form.validateFields(
                                      ["warehouseContract"],
                                      { force: true }
                                    )
                                );
                              }}
                              showSearch
                              isBackground={false}
                            />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="p-0 m-0 mt-0">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("dateBeginContract", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errStart,
                              message: "nhập ngày kí hợp đồng",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Ngày ký hợp đồng"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <DatePicker
                              id="datePicker"
                              showTime
                              placeholder="nhập ngày ký hợp đồng"
                              // suffixIcon={() => null}
                              size="large"
                              style={{
                                width: "100%",
                                height: 42,
                                background: colors.white,
                                borderStyle: "solid",
                                borderWidth: 0,
                                borderColor: colors.line,
                                borderBottomWidth: 1,
                              }}
                              onChange={this.onOkDatePickerStart}
                            />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>

                    <Col xs="auto">
                      <div style={{ width: 4 }}></div>
                    </Col>
                    <Col className="m-0 p-0"></Col>
                  </Row>
                  <Row className="p-0 m-0 mt-0">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("dateEndContract", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errEnd,
                              message: "nhập ngày hết hợp đồng",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Ngày hết hợp đồng"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <DatePicker
                              id="datePicker"
                              showTime
                              placeholder="nhập ngày hết hợp đồng"
                              // suffixIcon={() => null}
                              size="large"
                              style={{
                                width: "100%",
                                height: 42,
                                background: colors.white,
                                borderStyle: "solid",
                                borderWidth: 0,
                                borderColor: colors.line,
                                borderBottomWidth: 1,
                              }}
                              onChange={this.onOkDatePickerEnd}
                            />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>

                    <Col xs="auto">
                      <div style={{ width: 4 }}></div>
                    </Col>
                    <Col className="m-0 p-0"></Col>
                  </Row>
                  <Row className="p-0 m-0 mt-0">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("statusContract", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errStatus,
                              message: "chọn trạng thái",
                            },
                          ],
                        })(
                          <Col className="m-0 p-0">
                            <ITitle
                              level={4}
                              title={"Trạng thái"}
                              style={{
                                color: colors.black,
                                fontWeight: 600,
                                marginBottom: 10,
                              }}
                            />
                            <ISelectStatus
                              onChange={this.onChangeStatus}
                              style={{
                                width: "100%",
                                background: colors.white,
                                borderStyle: "solid",
                                borderWidth: 0,
                                borderColor: colors.line,
                                borderBottomWidth: 1,
                              }}
                              placeholder="Chọn trạng thái"
                              isBackground={false}
                            />
                          </Col>
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs="auto">
                      <div style={{ width: 4 }}></div>
                    </Col>
                    <Col className="m-0 p-0"></Col>
                  </Row>
                </div>
              </Col>
              <Col className="m-0 p-0 " xs="auto">
                <div
                  style={{
                    width: 4,
                    height: "100%",
                    background: colors.background,
                  }}
                ></div>
              </Col>

              <Col
                className="m-0 p-0"
                style={{ background: colors.white }}
                xs="auto"
              >
                <div style={{ marginRight: 40, marginLeft: 40, marginTop: 30 }}>
                  <ITitle
                    level={4}
                    title="Hình ảnh"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      marginBottom: 26,
                    }}
                  />
                  <Form.Item>
                    {getFieldDecorator("image", {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: "chọn hình",
                          valuePropName: "fileList",
                          getValueFromEvent: this.normFile,
                        },
                      ],
                    })(
                      <Row className="m-0 p-0">
                        <IUpload
                          type={ImageType.SHIPPER}
                          callback={(array) => {
                            this.state.imgAdd = array;

                            this.setState({});
                          }}
                          remove={(index) => {
                            this.setState({});
                          }}
                        />
                      </Row>
                    )}
                  </Form.Item>
                </div>
              </Col>
            </Row>
          </div>
        </Form>
        {/* </Col> */}
      </Container>
    );
  }

  componentDidMount() {}
}
const createShipperPage = Form.create({ name: "CreateShipperPage" })(
  CreateShipperPage
);

export default createShipperPage;
