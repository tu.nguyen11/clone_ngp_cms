import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  ILoading
} from "../../components";
import { colors } from "../../../assets";
import { message, Button } from "antd";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";

class DetailShipperPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shipper: { contract: { warehouse: {} }, image: [] },
      url: "",
      loading: false
    };
    this.IDShipper = this.props.match.params.id;
  }
  componentDidMount() {
    this._APIgetDetailShipper(this.IDShipper);
  }

  _APIgetDetailShipper = async shipper_id => {
    try {
      const data = await APIService._getDetailShipper(shipper_id);

      let url = data.image_url + data.shipper.image[0];

      this.setState({
        shipper: data.shipper,
        url: url,
        loading: true
      });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { shipper } = this.state;
    const { contract, image } = shipper;
    return (
      <Container fluid style={{ flex: 1 }}>
        {/* <Col className="d-flex flex-column" style={{ height: "100%" }}> */}
        <Row className="mt-3">
          <ITitle
            level={1}
            title="Chi tiết shipper"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex"
              // marginBottom: 100,
              // marginLeft: 5,
            }}
          />
        </Row>

        {!this.state.loading ? (
          <ILoading />
        ) : (
          <div>
            <Row className="m-0 p-0 mt-4">
              <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}></Row>
              <Row>
                <IButton
                  icon={ISvg.NAME.TIMEREVERSE}
                  title={"Lịch sử giao"}
                  color={colors.main}
                  onClick={() => {
                    this.props.history.push(
                      "/historyShipperPage/" + shipper.phone
                    );
                  }}
                />
                <IButton
                  icon={ISvg.NAME.WRITE}
                  title={"Chỉnh sửa"}
                  color={colors.main}
                  style={{ marginLeft: 20 }}
                  onClick={() => {
                    this.props.history.push(
                      "/editShipperPage/" + this.IDShipper
                    );
                  }}
                />
              </Row>
            </Row>

            <div style={{ marginTop: 20 }}>
              <Row style={{ height: "100%" }} className="p-0">
                <Col xs={5} style={{ background: colors.white }}>
                  <div style={{ marginLeft: 40, marginRight: 40 }}>
                    <ITitle
                      level={4}
                      title="Thông tin Shipper"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        marginTop: 30,
                        marginBottom: 26
                      }}
                    />
                    <Row className="p-0 m-0 mb-3">
                      <Col className="m-0 p-0">
                        <ITitle
                          level={4}
                          title={"Tên shipper"}
                          style={{ color: colors.black, fontWeight: 600 }}
                        />
                        <ITitle
                          level={4}
                          title={shipper.name}
                          style={{ color: colors.black }}
                        />
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"Mã shipper"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={shipper.phone}
                            style={{ color: colors.black }}
                          />{" "}
                        </Col>
                      </Col>
                    </Row>
                    <Row className="p-0 m-0 mb-3" style={{ marginBottom: 20 }}>
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"Điện thoại"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={shipper.code}
                            style={{ color: colors.black }}
                          />{" "}
                        </Col>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="m-0 p-0">
                        <ITitle
                          level={4}
                          title={"Email"}
                          style={{ color: colors.black, fontWeight: 600 }}
                        />
                        <ITitle
                          level={4}
                          title={shipper.email}
                          style={{ color: colors.black }}
                        />
                      </Col>
                    </Row>
                    <Row className="p-0 m-0 mb-3">
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"CMND"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={shipper.cmnd}
                            style={{ color: colors.black }}
                          />{" "}
                        </Col>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"Địa chỉ"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={shipper.address}
                            style={{ color: colors.black }}
                          />{" "}
                        </Col>
                      </Col>
                    </Row>

                    <div style={{ height: 32 }}></div>
                    <ITitle
                      level={4}
                      title="Thông tin hợp đồng"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        marginBottom: 26
                      }}
                    />

                    <Row className="p-0 m-0 mb-3">
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"Số hợp đồng"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={contract.numberContract}
                            style={{ color: colors.black }}
                          />{" "}
                        </Col>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"Kho phụ trách"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={contract.warehouse.name}
                            style={{ color: colors.black }}
                          />
                        </Col>
                      </Col>
                    </Row>
                    <Row className="p-0 m-0 mb-3">
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"Ngày ký hợp đồng"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={FormatterDay.dateFormatWithString(
                              contract.start_date,
                              "#DD#/#MM#/#YYYY#"
                            )}
                            style={{ color: colors.black }}
                          />
                        </Col>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                    </Row>
                    <Row className="p-0 m-0 mb-3">
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"Ngày hết hợp đồng"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={FormatterDay.dateFormatWithString(
                              contract.end_date,
                              "#DD#/#MM#/#YYYY#"
                            )}
                            style={{ color: colors.black }}
                          />
                        </Col>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="m-0 p-0"></Col>
                    </Row>
                    <Row className="p-0 m-0 mb-3">
                      <Col className="p-0 m-0">
                        <Col className="m-0 p-0">
                          <ITitle
                            level={4}
                            title={"Trạng thái"}
                            style={{ color: colors.black, fontWeight: 600 }}
                          />
                          <ITitle
                            level={4}
                            title={contract.statusName}
                            style={{ color: colors.black }}
                          />
                        </Col>
                      </Col>
                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="m-0 p-0"></Col>
                    </Row>
                  </div>
                </Col>
                <Col className="m-0 p-0 " xs="auto">
                  <div
                    style={{
                      width: 4,
                      height: "100%",
                      background: colors.background
                    }}
                  ></div>
                </Col>

                <Col
                  className="m-0 p-0"
                  style={{ background: colors.white }}
                  xs="auto"
                >
                  <div
                    style={{ marginRight: 40, marginLeft: 40, marginTop: 30 }}
                  >
                    <ITitle
                      level={4}
                      title="Hình ảnh"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        marginBottom: 26
                      }}
                    />

                    <Row className="m-0 p-0">
                      <img
                        src={this.state.url}
                        style={{ width: 180, height: 180 }}
                      />
                    </Row>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        )}
        {/* </Col> */}
      </Container>
    );
  }
}

export default DetailShipperPage;
