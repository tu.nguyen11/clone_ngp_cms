import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  IDatePicker,
  ISelectStatus,
  ISelectAllStatus,
} from "../../components";
import { colors, images } from "../../../assets";
import { message, Button, Modal } from "antd";
import { APIService } from "../../../services";
import { priceFormat } from "../../../utils";
import FormatterDay from "../../../utils/FormatterDay";

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}
const NewDate = new Date();

const SCROLL_HEIGHT = 540;
class HistoryShipperPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHeight: 0,
      start_date: NewDate.getTime(),
      end_date: NewDate.getTime(),
      page: 1,
      keySearch: "",
      shippers: [],
      data: {},
      dataDetail: {},
      keyStatus: 0,
      url: "",
      loadingTable: true,
      Cate: [],
      visible: false,
    };
    this.Id = Number(this.props.match.params.id);
    this.columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",

        width: 120,
        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Ngày gán đơn"),
        dataIndex: "assignDate",
        key: "assignDate",
        render: (assignDate) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(assignDate, "#DD#/#MM#/#YYYY#")
          ),
      },
      {
        title: _renderTitle("Mã đơn"),
        dataIndex: "orderCode",
        key: "orderCode",
        render: (orderCode) => {
          return (
            <div
              style={{
                wordWrap: "break-word",
                wordBreak: "break-word",
                cursor: "pointer",
              }}
              className="cursor"
              onClick={() => {
                this._getListHistoryShipperDetail(orderCode);
                this.setState({
                  visible: true,
                });
              }}
            >
              <ITitle
                level={4}
                title={orderCode}
                style={{ color: colors.main, fontWeight: "bold" }}
              />
            </div>
          );
        },
      },
      {
        title: _renderTitle("Kho xuất hàng"),
        dataIndex: "warehouseExportName",
        key: "warehouseExportName",

        render: (warehouseExportName) => _renderColumns(warehouseExportName),
      },
      {
        title: _renderTitle("Tên đại lý"),
        dataIndex: "agencyName",
        key: "agencyName",
        render: (agencyName) => _renderColumns(agencyName),
      },
      {
        title: _renderTitle("Doanh số"),
        dataIndex: "price",
        key: "price",

        render: (price) => _renderColumns(priceFormat(price)),
      },

      {
        title: _renderTitle("Trạng thái"),
        dataIndex: "statusName",
        key: "statusName",

        render: (statusName) => _renderColumns(statusName),
      },
    ];
    this.tableContainer = React.createRef();
  }
  getDate = () => {
    let from = new Date();
    from.setDate(1);
    from.setHours(0, 0, 0, 0);

    let to = new Date();
    to.setHours(0, 0, 0, 0);
    this.setState({
      start_date: from.getTime(),
      end_date: to.getTime(),
    });
  };
  render() {
    return (
      <Container className="p-0 m-0" fluid style={{ flex: 1 }}>
        <Col className="d-flex flex-column" style={{ height: "100%" }}>
          <Row className="p-0 m-0">
            <ITitle
              level={1}
              title="Lịch sử giao nhận"
              style={{
                color: colors.main,
                fontWeight: "bold",
                flex: 1,
                display: "flex",
              }}
            />
          </Row>
          <Row className="m-0 p-0 mt-4">
            <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
              <Row className="m-0 p-0 center">
                <ISvg
                  name={ISvg.NAME.EXPERIMENT}
                  width={20}
                  height={20}
                  fill={colors.icon.default}
                />
                <div style={{ marginLeft: 25 }}>
                  <ITitle title="Từ ngày" level={4} />
                </div>
              </Row>
              <Row className="m-0 p-0 ">
                <IDatePicker
                  className="ml-4"
                  from={this.state.start_date}
                  to={this.state.end_date}
                  onChange={(a, b) => {
                    this.state.start_date = a[0]._d.getTime();
                    this.state.end_date = a[1]._d.getTime();
                    this.setState(
                      {
                        state_date: this.state.start_date,
                        end_date: this.state.end_date,
                      },
                      () =>
                        this._APIListHistoryShipper(
                          this.Id,
                          this.state.start_date,
                          this.state.end_date,
                          this.state.keyStatus,
                          this.state.keySearch,
                          this.state.page
                        )
                    );
                  }}
                  style={{
                    background: colors.white,
                    borderWidth: 1,
                    borderColor: colors.line,
                    borderStyle: "solid",
                  }}
                />

                <Row className="m-0 p-0 ">
                  {/* <ISelectAllStatus
                    className="ml-4"
                    all={true}
                    value={this.state.keyStatus}
                    onChange={key => {
                      this.setState(
                        {
                          keyStatus: key
                        },
                        () =>
                          this._APIListHistoryShipper(
                            this.Id,
                            this.state.start_date,
                            this.state.end_date,
                            this.state.keyStatus,
                            this.state.keySearch,
                            this.state.page
                          )
                      );
                    }}
                  /> */}
                  {this.state.Cate.length == 0 ? null : (
                    <ISelect
                      className="ml-4"
                      defaultValue={this.state.Cate[0].key}
                      style={{ width: "100%" }}
                      select={true}
                      onChange={(value, key) => {
                        this.setState(
                          {
                            keyStatus: value,
                          },
                          () =>
                            this._APIListHistoryShipper(
                              this.Id,
                              this.state.start_date,
                              this.state.end_date,
                              this.state.keyStatus,
                              this.state.keySearch,
                              this.state.page
                            )
                        );
                      }}
                      data={this.state.Cate}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  )}
                </Row>
              </Row>
            </Row>
            <Row className="m-0 p-0">
              <ISearch
                onChange={(e) => {
                  this.state.keySearch = e.target.value;
                  this.setState({
                    keySearch: this.state.keySearch,
                  });
                }}
                style={{ width: 400 }}
                placeholder=""
                onPressEnter={() => {
                  this.setState(
                    {
                      loadingTable: true,
                    },
                    () =>
                      this._APIListHistoryShipper(
                        this.Id,
                        this.state.start_date,
                        this.state.end_date,
                        this.state.keyStatus,
                        this.state.keySearch,
                        this.state.page
                      )
                  );
                }}
                icon={
                  <div
                    style={{
                      height: 42,
                      display: "flex",
                      alignItems: "center",
                    }}
                    className="cursor"
                    onClick={() =>
                      this.setState(
                        {
                          loadingTable: true,
                        },
                        () =>
                          this._APIListHistoryShipper(
                            this.Id,
                            this.state.start_date,
                            this.state.end_date,
                            this.state.keyStatus,
                            this.state.keySearch,
                            this.state.page
                          )
                      )
                    }
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 25, height: 25 }}
                    />
                  </div>
                }
              />
            </Row>
          </Row>
          <div
            className="m-0 p-0 mt-4 "
            style={{ flex: 1 }}
            ref={this.tableContainer}
          >
            <ITable
              data={this.state.shippers}
              columns={this.columns}
              loading={this.state.loadingTable}
              // bodyStyle={{
              //   height: this.state.tableHeight,
              //   //background: colors.background
              // }}
              style={{ width: "100%" }}
              scroll={{ x: 1170, y: this.state.tableHeight - 50 }}
              defaultCurrent={1}
              sizeItem={this.state.data.size}
              indexPage={this.state.page}
              maxpage={this.state.data.total}
              onChangePage={(page) => {
                this.setState(
                  {
                    page: page,
                    loadingTable: true,
                  },
                  () =>
                    this._APIListHistoryShipper(
                      this.Id,
                      this.state.start_date,
                      this.state.end_date,
                      this.state.keyStatus,
                      this.state.keySearch,
                      this.state.page
                    )
                );
              }}
            />
          </div>
        </Col>
        {this.state.visible ? (
          <ModalOrder
            data={this.state.dataDetail}
            onCancel={() => {
              this.setState({
                visible: false,
              });
            }}
          />
        ) : null}
      </Container>
    );
  }
  _APIListHistoryShipper = async (
    id,
    start_date,
    end_date,
    status,
    key,
    page
  ) => {
    try {
      const data = await APIService._getListHistoryShipper(
        id,
        start_date,
        end_date,
        status,
        key,
        page
      );
      data.shipper.map((item, index) => {
        data.shipper[index].stt = (this.state.page - 1) * 10 + index + 1;
      });
      this.setState({
        data: data,
        shippers: data.shipper,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  _getListHistoryShipperDetail = async (order_code_child) => {
    try {
      const data = await APIService._getListHistoryShipperDetail(
        order_code_child
      );
      this.setState({
        dataDetail: data,
        url: data.url,
      });
    } catch (err) {
      console.log(err);
    }
  };

  _getListStatus = async () => {
    try {
      const data = await APIService._getListStatus();
      var StatusPare = [];
      StatusPare.push({
        key: 0,
        value: "Tất cả",
      });
      data.order_status.map((item, index) => {
        const value = item.name;
        const key = item.id;
        StatusPare.push({ value, key });
      });
      this.setState({
        Cate: StatusPare,
      });
    } catch (error) {}
  };

  async componentDidMount() {
    await this._getListStatus();

    await this._APIListHistoryShipper(
      this.Id,
      this.state.start_date,
      this.state.end_date,
      this.state.keyStatus,
      this.state.keySearch,
      this.state.page
    );

    setTimeout(() => {
      try {
        this.setState({
          tableHeight:
            this.tableContainer.current.parentElement.clientHeight - 200,
        }); // 360
      } catch (error) {}
    }, 1);
    this.getDate();
  }
}

class ModalOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.visible,
    });
  }
  render() {
    const { data } = this.props;
    const { gift = [], product = [], price = {} } = data;
    return (
      <Modal
        title={null}
        centered
        visible={true}
        //onOk={() => this.setModal1Visible(false)}
        onCancel={() => {
          this.props.onCancel();
        }}
        closeIcon={
          <ISvg
            name={ISvg.NAME.CLOSEMODAL}
            width={24}
            height={24}
            // fill={colors.icon.default}
          />
        }
        width={1025}
        footer={null}
      >
        <Container>
          <Row className="m-0 p-0 ">
            <Col
              xs={7}
              className="m-0 p-0 d-flex flex-column"
              style={{ maxHeight: SCROLL_HEIGHT }}
            >
              <ITitle
                level={4}
                title="Sản phẩm đặt mua"
                style={{ color: colors.black, fontWeight: "bold" }}
              />
              <div
                className="m-0 p-0 mt-4"
                style={{ flex: 1, overflowY: "scroll" }}
              >
                {product.map((item, index) => {
                  return this._renderItemProduct(item);
                })}
              </div>

              {this._renderTotal(price)}
            </Col>
            <Col xs="auto" className="m-0 p-0 center" style={{ width: 80 }}>
              <div
                style={{
                  width: 4,
                  background: colors.background,
                  height: "100%",
                }}
              ></div>
            </Col>
            <Col
              className="m-0 p-0 d-flex flex-column"
              style={{ maxHeight: SCROLL_HEIGHT }}
            >
              <ITitle
                level={4}
                title="Quà tặng"
                style={{ color: colors.black, fontWeight: "bold" }}
              />
              <div
                className="mt-4"
                style={{
                  flex: 1,
                  overflowY: "scroll",
                }}
              >
                {gift.map((item, index) => {
                  return this._renderItemGift(item);
                })}
              </div>
            </Col>
          </Row>
        </Container>
      </Modal>
    );
  }

  _renderTotal = (price) => {
    return (
      <div className="m-0 p-0 d-flex flex-column mr-4" style={{}}>
        <div
          style={{
            width: "100%",
            height: 1,
            background: colors.background,
            marginBottom: 20,
          }}
        ></div>

        <div className="m-0 p-0 d-flex">
          <div style={{ width: 70 }}></div>
          <div className="m-0 p-0 ml-4" style={{ flex: 1 }}>
            <div className="d-flex">
              <ITitle
                level={4}
                title="Cộng"
                style={{
                  color: colors.black,
                  fontWeight: "500",

                  flex: 1,
                }}
              />
              <ITitle
                level={4}
                title={priceFormat(price.total) + " VNĐ"}
                style={{ color: colors.black, fontWeight: "500" }}
              />
            </div>
            <div className="d-flex mt-2">
              <ITitle
                level={4}
                title="Khuyến mãi"
                style={{
                  color: colors.black,
                  fontWeight: "500",
                  flex: 1,
                }}
              />
              <ITitle
                level={4}
                title={priceFormat(price.sumSale) + " VNĐ"}
                style={{ color: colors.black, fontWeight: "500" }}
              />
            </div>
            <div
              className="mt-2 mb-2"
              style={{
                width: "100%",
                height: 2,

                background: colors.background,
              }}
            ></div>

            <div className="d-flex mt-2">
              <ITitle
                level={4}
                title="Tổng cộng"
                style={{
                  color: colors.main,
                  fontWeight: "500",
                  flex: 1,
                }}
              />
              <ITitle
                level={4}
                title={priceFormat(price.sumProduct) + " VNĐ"}
                style={{ color: colors.main, fontWeight: "500" }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  _renderItemProduct = (data) => {
    return (
      <div className="d-flex flex-row mb-4 mr-4">
        <div>
          <IImage
            style={{ width: 70, height: 70 }}
            src={this.state.url + data.image}
          />
        </div>
        <Col className="m-0 p-0 ml-4 ">
          <ITitle
            level={4}
            title={data.name}
            style={{ color: colors.black, fontWeight: "500" }}
          />
          <ITitle
            level={4}
            title={data.quantity + " " + data.variationName}
            style={{ color: colors.black }}
          />
          <div className="d-flex">
            <ITitle
              level={4}
              title={"Số lượng:" + data.quantity}
              style={{ color: colors.black, flex: 1 }}
            />
            <ITitle
              level={4}
              title={priceFormat(data.price) + " VNĐ"}
              style={{ color: colors.black }}
            />
          </div>
        </Col>
      </div>
    );
  };

  _renderItemGift = (data) => {
    return (
      <div className="d-flex flex-row mb-4 mr-4">
        <div>
          <IImage
            style={{ width: 70, height: 70 }}
            src={this.state.url + data.image}
          />
        </div>
        <Col className="m-0 p-0 ml-4 ">
          <ITitle
            level={4}
            title={data.name}
            style={{ color: colors.black, fontWeight: "500" }}
          />
          <ITitle
            level={4}
            title={data.quantity}
            style={{ color: colors.black, flex: 1 }}
          />
        </Col>
      </div>
    );
  };
}

export default HistoryShipperPage;
