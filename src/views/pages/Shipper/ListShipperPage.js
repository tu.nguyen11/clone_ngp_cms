import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  ISelectWareHouse,
  ISelectStatus,
  ISelectAllStatus,
  ISelectAllWareHouse,
} from "../../components";
import { colors, images } from "../../../assets";
import { message, Button } from "antd";
import { APIService } from "../../../services";
function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 13;
  } else widthColumn = (widthScreen - 300) / 13;
  return widthColumn * type;
}

class ListShipperPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      tableHeight: 0,
      data: {},
      shippers: [],
      data: {},
      type_warehouse: 0,
      loadingTable: true,
      keySearch: "",
      page: 1,
      url: "",
      arrayRemove: [],
      keyStatus: 2, // lấy tất cả
    };
    this.columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",

        width: 80,
        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Mã Shipper"),
        dataIndex: "code",
        key: "code",
        width: 160,
        render: (code) => _renderColumns(code),
      },
      {
        title: _renderTitle("Tên Shipper"),
        dataIndex: "name",
        key: "name",
        width: 200,
        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Số điện thoại"),
        dataIndex: "phone",
        key: "phone",
        width: 200,
        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Email"),
        dataIndex: "email",
        key: "email",
        width: 200,
        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Hình ảnh"),
        dataIndex: "image",
        key: "image",
        align: "center",
        width: 200,
        render: (image) => {
          if (!image) {
            return null;
          }
          return (
            <IImage
              src={this.state.url + image[0]}
              style={{ width: 40, height: 40, borderRadius: 100 }}
            />
          );
        },
      },
      {
        title: _renderTitle("Kho phụ trách"),
        dataIndex: "warehouseName",
        key: "warehouseName",
        width: 200,
        render: (warehouseName) => _renderColumns(warehouseName),
      },
      {
        title: _renderTitle("Trạng thái"),
        dataIndex: "statusName",
        key: "statusName",
        // align: "center",
        width: 200,
        render: (statusName) => _renderColumns(statusName),
      },
      {
        title: "",
        dataIndex: "id",
        key: "id",
        align: "center",
        width: 60,

        render: (id) => (
          <Button
            type="link"
            onClick={() => this.props.history.push("/editShipperPage/" + id)}
          >
            <ISvg
              name={ISvg.NAME.WRITE}
              width={20}
              fill={colors.icon.default}
            />
          </Button>
        ),
      },
      {
        title: "",
        dataIndex: "id",
        key: "id",
        align: "center",
        width: 60,

        render: (id) => (
          <Button
            type="link"
            onClick={() => {
              this.props.history.push("/detailShipperPage/" + id);
            }}
          >
            <ISvg
              name={ISvg.NAME.CHEVRONRIGHT}
              width={12}
              fill={colors.icon.default}
            />
          </Button>
        ),
      },
    ];
    this.tableContainer = React.createRef();
    this.onSelectChange = this.onSelectChange.bind(this);
  }

  _APIpostRemoveShipper = async (obj) => {
    try {
      const data = APIService._postRemoveShipper(obj);
      message.success("Xóa thành công");

      this.setState(
        {
          selectedRowKeys: [],
        },
        () =>
          this._APIgetListShipper(
            this.state.type_warehouse,
            this.state.keyStatus,
            this.state.keySearch,
            this.state.page
          )
      );
    } catch (err) {
      console.log(err);
    }
  };

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.shippers[item].id);
    });
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys, arrayRemove: arrayID });
  };
  render() {
    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container className="p-0 m-0 mt-3" fluid style={{ flex: 1 }}>
        <Col className="d-flex flex-column" style={{ height: "100%" }}>
          <Row>
            <ITitle
              level={1}
              title="Shipper"
              style={{
                color: colors.main,
                fontWeight: "bold",
                flex: 1,
                display: "flex",
              }}
            />
            <ISearch
              onChange={(e) => {
                this.state.keySearch = e.target.value;
                this.setState({
                  keySearch: this.state.keySearch,
                });
              }}
              placeholder=""
              onPressEnter={() => {
                this.setState(
                  {
                    loadingTable: true,
                  },
                  () =>
                    this._APIgetListShipper(
                      this.state.type_warehouse,
                      this.state.keyStatus,
                      this.state.keySearch,
                      this.state.page
                    )
                );
              }}
              icon={
                <div
                  style={{
                    display: "flex",
                    width: 42,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  className="cursor"
                  onClick={() =>
                    this.setState(
                      {
                        loadingTable: true,
                      },
                      () =>
                        this._APIgetListShipper(
                          this.state.type_warehouse,
                          this.state.keyStatus,
                          this.state.keySearch,
                          this.state.page
                        )
                    )
                  }
                >
                  <img
                    src={images.icSearch}
                    style={{ width: 20, height: 20 }}
                  />
                </div>
              }
            />
          </Row>
          <Row className="mt-4">
            <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
              <Row className="m-0 p-0 center">
                <ISvg
                  name={ISvg.NAME.EXPERIMENT}
                  width={20}
                  height={20}
                  fill={colors.icon.default}
                />
                <div style={{ marginLeft: 25 }}>
                  <ITitle title="Kho hàng" level={4} />
                </div>
              </Row>
              <Row className="m-0 p-0 ml-4">
                <ISelectAllWareHouse
                  // className="ml-4"
                  onSearch={() => {}}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                  value={this.state.type_warehouse}
                  onChange={(key) => {
                    this.setState(
                      {
                        type_warehouse: key,
                        loadingTable: true,
                      },
                      () =>
                        this._APIgetListShipper(
                          this.state.type_warehouse,
                          this.state.keyStatus,
                          this.state.keySearch,
                          this.state.page
                        )
                    );
                  }}
                  showSearch
                />
              </Row>
              <Row className="m-0 p-0 ml-4 ">
                <ISelectAllStatus
                  style={{
                    background: colors.white,
                    borderStyle: "solid",
                    borderWidth: 1,
                    borderColor: colors.line,
                    paddingLeft: 12,
                  }}
                  all={true}
                  value={this.state.keyStatus}
                  typeRouter
                  onChange={(key) => {
                    this.setState(
                      {
                        keyStatus: key,
                      },
                      () =>
                        this._APIgetListShipper(
                          this.state.type_warehouse,
                          this.state.keyStatus,
                          this.state.keySearch,
                          this.state.page
                        )
                    );
                  }}
                />
              </Row>
            </Row>
            <Row className="m-0 p-0">
              <IButton
                icon={ISvg.NAME.ADD}
                title={"Tạo mới"}
                color={colors.main}
                onClick={() => {
                  this.props.history.push("/createShipperPage/" + "add");
                  // const obj = {
                  //   start_date: this.state.start_date,
                  //   end_date: this.state.end_date,
                  //   status: this.key,
                  //   key: this.state.value
                  // };
                  // this._postAPIExportExecl(obj);
                }}
              />
              <IButton
                icon={ISvg.NAME.DOWLOAND}
                title={"Xuất file"}
                color={colors.main}
                style={{ marginRight: 20, marginLeft: 20 }}
                onClick={() => {
                  // const obj = {
                  //   start_date: this.state.start_date,
                  //   end_date: this.state.end_date,
                  //   status: this.key,
                  //   key: this.state.value
                  // };
                  // this._postAPIExportExecl(obj);
                  message.warning("Chức năng đang cập nhật");
                }}
              />
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                color={colors.oranges}
                onClick={() => {
                  if (this.state.arrayRemove.length == 0) {
                    message.warning("Bạn chưa chọn shipper.");
                    return;
                  }
                  const objRemove = {
                    id: this.state.arrayRemove,
                    status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                  };
                  this._APIpostRemoveShipper(objRemove);
                }}
              />
            </Row>
          </Row>

          <div style={{ flex: 1 }} ref={this.tableContainer}>
            <Row className="mt-4 ">
              <ITable
                data={this.state.shippers}
                columns={this.columns}
                loading={this.state.loadingTable}
                bodyStyle={{
                  height: this.state.tableHeight,
                }}
                style={{ width: "100%" }}
                rowSelection={rowSelection}
                scroll={{
                  // y: 600,
                  x: 1000,
                }}
                defaultCurrent={1}
                sizeItem={this.state.data.size}
                indexPage={this.state.page}
                maxpage={this.state.data.total}
                onChangePage={(page) => {
                  this.setState(
                    {
                      page: page,
                      loadingTable: true,
                    },
                    () =>
                      this._APIgetListShipper(
                        this.state.type_warehouse,
                        this.state.keyStatus,
                        this.state.keySearch,
                        this.state.page
                      )
                  );
                }}
              />
            </Row>
          </div>
        </Col>
      </Container>
    );
  }

  componentDidMount() {
    this._APIgetListShipper(
      this.state.type_warehouse,
      this.state.keyStatus,
      this.state.keySearch,
      this.state.page
    );

    // for (var i = 0; i < 12; i++) {
    //   this.state.shippers.push({
    //     id: i + 1,
    //     stt: i,
    //     code: "VH323131",
    //     name: "Trần Dần",
    //     phone: "01234567890",
    //     email: "TranTiger@gmail.com",
    //     warehouseName: "Kho A Quận Phú Nhuận Kho D Quận Phú Nhuận",
    //     image: [
    //       "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/0e/0ea5631114b012f29e87531a1878af5c232e30ae_full.jpg"
    //     ],
    //     statusName: "Hoàn thành"
    //   });
    // }
    // this.setState({});
    setTimeout(() => {
      try {
        this.setState({
          tableHeight:
            this.tableContainer.current.parentElement.clientHeight - 200,
        }); // 360
      } catch (error) {}
    }, 1);
  }

  _APIgetListShipper = async (warehouse_id, status, key, page) => {
    try {
      const data = await APIService._getListShipper(
        warehouse_id,
        status,
        key,
        page
      );
      data.shipper.map((item, index) => {
        data.shipper[index].stt = (this.state.page - 1) * 10 + index + 1;
        data.shipper[index].objSdt = item.phone;
      });

      this.setState({
        data: data,
        shippers: data.shipper,
        loadingTable: false,
        url: data.image_url,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };
}

export default ListShipperPage;
