import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  IInput,
  IUpload,
  ISelectStatus,
  ISelectAllStatus,
  ISelectWareHouse,
  ILoading
} from "../../components";
import moment from "moment";

import { colors } from "../../../assets";
import { message, Button, Form, DatePicker, Upload } from "antd";
import { ImageType } from "../../../constant";
import { APIService } from "../../../services";
let dateFormat = "dd/MM/yyyy";

class EditShipperPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errEnd: true,
      errStart: true,
      dateEnd: 0,
      dateStart: 0,
      errStatus: true,
      status: 0,
      errWareHouseConTract: true,
      type_warehouse: 0,
      imgAdd: [],
      url: "",
      data: {
        shipper: {
          id: 1,
          name: "",
          code: "",
          phone: "",
          email: "",
          cmnd: "",
          address: "",
          image: "",
          status: 1 // chó nghĩa
        },
        contract: {
          id: 1,
          contract_number: "",
          start_date: 0,
          end_date: 0,
          status: "",
          warehouse_id: ""
        }
      },
      loading: false
    };
    this.IDShipper = this.props.match.params.id;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onOkDatePickerStart = this.onOkDatePickerStart.bind(this);
    this.onOkDatePickerEnd = this.onOkDatePickerEnd.bind(this);
    this.onChangeStatus = this.onChangeStatus.bind(this);
  }
  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      const ObjectEdit = {
        shipper: {
          id: this.state.data.shipper.id,
          name: this.state.data.shipper.name,
          code: this.state.data.shipper.code,
          phone: this.state.data.shipper.phone,
          email: this.state.data.shipper.email,
          cmnd: this.state.data.shipper.cmnd,
          address:
            this.state.data.shipper.address == null
              ? ""
              : this.state.data.shipper.address,
          image: this.state.data.shipper.image,
          status: this.state.data.contract.status // chó nghĩa
        },
        contract: {
          id: this.state.data.contract.id,
          contract_number: this.state.data.contract.contract_number,
          start_date: this.state.data.contract.start_date,
          end_date: this.state.data.contract.end_date,
          status: this.state.data.contract.status,
          warehouse_id: this.state.data.contract.warehouse_id
        }
      };

      this._postAPIEditShipper(ObjectEdit);
    });
  };

  componentWillUpdate(nextProps) {}

  _postAPIEditShipper = async obj => {
    try {
      const data = await APIService._postEditShipper(obj);
      message.success("Sửa shipper thành công");
      this.props.history.push("/shipperPage");
    } catch (err) {
      console.log(err);
    }
  };

  onOkDatePickerStart = (date, dateString) => {
    this.state.data.contract.start_date = Date.parse(date._d);

    this.setState(
      {
        data: this.state.data,
        errStart: false
      },
      () => {
        this.props.form.validateFields(["dateBeginContract"], { force: true });
      }
    );
  };

  onOkDatePickerEnd = (date, dateString) => {
    this.state.data.contract.end_date = Date.parse(date._d);

    this.setState(
      {
        data: this.state.data,
        errEnd: false
      },
      () => {
        this.props.form.validateFields(["dateEndContract"], { force: true });
      }
    );
  };

  onChangeStatus = value => {
    // this.state.objAdd.status = Number(value);
    this.setState({ status: value, errStatus: false }, () =>
      this.props.form.validateFields(["statusContract"], { force: true })
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { shipper, contract } = this.state.data;
    return (
      <Container fluid style={{ flex: 1 }}>
        {/* <Col className="d-flex flex-column" style={{ height: "100%" }}> */}
        <Row className="mt-3">
          <ITitle
            level={1}
            title="Shipper"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex"
            }}
          />
        </Row>
        {!this.state.loading ? (
          <ILoading />
        ) : (
          <Form onSubmit={this.handleSubmit}>
            <Row className="m-0 p-0 mt-4">
              <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}></Row>
              <Row>
                <IButton
                  icon={ISvg.NAME.TIMEREVERSE}
                  title={"Lưu"}
                  htmlType="submit"
                  color={colors.main}
                  onClick={() => this.handleSubmit}
                />
                <IButton
                  icon={ISvg.NAME.CROSS}
                  title={"Hủy"}
                  color={colors.oranges}
                  style={{ marginLeft: 20 }}
                  onClick={() => {
                    this.props.history.push("/shipperPage");
                  }}
                />
              </Row>
            </Row>

            <div style={{}}>
              <Row style={{ height: "100%" }} className="p-0">
                <Col xs={5} style={{ background: colors.white }}>
                  <div style={{ marginLeft: 40, marginRight: 40 }}>
                    <ITitle
                      level={4}
                      title="Thông tin Shipper"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        marginTop: 30,
                        marginBottom: 26
                      }}
                    />
                    <Row className="p-0 m-0 mt-0">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("nameShipper", {
                            rules: [
                              {
                                message: "nhập tên shipper"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Tên shipper"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <IInput
                                placeholder="Nhập tên shipper"
                                value={shipper.name}
                                onChange={e => {
                                  shipper.name = e.target.value;
                                  this.setState({});
                                }}
                              />{" "}
                            </Col>
                          )}
                        </Form.Item>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("codeShipper", {
                            rules: [
                              {
                                message: "nhập mã shipper"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0" style={{ flex: 1 }}>
                              <ITitle
                                level={4}
                                title={"Mã shipper"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <IInput
                                placeholder="Nhập mã shipper"
                                value={shipper.code}
                                onChange={e => {
                                  shipper.code = e.target.value;
                                  this.setState({});
                                }}
                              />{" "}
                            </Col>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row className="p-0 m-0 mt-0">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("phoneShipper", {
                            rules: [
                              {
                                message: "nhập số điện thoại"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Điện thoại"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <IInput
                                placeholder="Nhập số điện thoại"
                                value={shipper.phone}
                                onChange={e => {
                                  shipper.phone = e.target.value;
                                  this.setState({});
                                }}
                              />{" "}
                            </Col>
                          )}
                        </Form.Item>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("emailShipper", {
                            rules: [
                              {
                                message: "nhập địa chỉ email"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Email"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <IInput
                                placeholder="Nhập địa chỉ email"
                                value={shipper.email}
                                onChange={e => {
                                  shipper.email = e.target.value;
                                  this.setState({});
                                }}
                              />{" "}
                            </Col>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row className="p-0 m-0 mt-0">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("CMNDShipper", {
                            rules: [
                              {
                                message: "nhập số chứng minh"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"CMND"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <IInput
                                placeholder="Nhập só chứng minh"
                                value={shipper.cmnd}
                                onChange={e => {
                                  shipper.cmnd = e.target.value;
                                  this.setState({});
                                }}
                              />{" "}
                            </Col>
                          )}
                        </Form.Item>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("addressShipper", {
                            rules: [
                              {
                                message: "nhập địa chỉ liên hệ"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Địa chỉ"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <IInput
                                placeholder="Nhập địa chỉ liên hệ"
                                value={shipper.address}
                                onChange={e => {
                                  shipper.address = e.target.value;
                                  this.setState({});
                                }}
                              />{" "}
                            </Col>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>

                    <div style={{ height: 32 }}></div>
                    <ITitle
                      level={4}
                      title="Thông tin hợp đồng"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        marginBottom: 26
                      }}
                    />

                    <Row className="p-0 m-0 mt-0">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("codeContract", {
                            rules: [
                              {
                                message: "nhập số hợp đồng"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Số hợp đồng"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <IInput
                                placeholder="Nhập số hợp đồng"
                                value={contract.contract_number}
                                onChange={e => {
                                  contract.contract_number = e.target.value;
                                  this.setState({});
                                }}
                              />{" "}
                            </Col>
                          )}
                        </Form.Item>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("warehouseContract", {
                            rules: [
                              {
                                message: "chọn tên kho"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Kho phụ trách"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <ISelectWareHouse
                                style={{
                                  background: colors.white,
                                  borderStyle: "solid",
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1,
                                  width: 100
                                }}
                                onSearch={() => {}}
                                filterOption={(input, option) =>
                                  option.props.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                                }
                                onChange={key => {
                                  this.state.data.contract.warehouse_id = key;
                                  this.setState({});
                                }}
                                value={contract.warehouse_id}
                                showSearch
                                isBackground={false}
                              />
                            </Col>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row className="p-0 m-0 mt-0">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("dateBeginContract", {
                            rules: [
                              {
                                message: "nhập ngày kí hợp đồng"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Ngày ký hợp đồng"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <DatePicker
                                format="DD/MM/YYYY"
                                value={moment(
                                  new Date(contract.start_date),
                                  dateFormat
                                )}
                                // suffixIcon={() => null}
                                size="large"
                                style={{
                                  width: "100%",
                                  height: 42,
                                  background: colors.white,
                                  borderStyle: "solid",
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1
                                }}
                                onChange={this.onOkDatePickerStart}
                              />
                            </Col>
                          )}
                        </Form.Item>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="m-0 p-0"></Col>
                    </Row>
                    <Row className="p-0 m-0 mt-0">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("dateEndContract", {
                            rules: [
                              {
                                message: "nhập ngày hết hợp đồng"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Ngày hết hợp đồng"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <DatePicker
                                format="DD/MM/YYYY"
                                placeholder="Nhập ngày chấm dứt"
                                // suffixIcon={() => null}
                                value={moment(
                                  new Date(contract.end_date),
                                  dateFormat
                                )}
                                size="large"
                                style={{
                                  width: "100%",
                                  height: 42,
                                  background: colors.white,
                                  borderStyle: "solid",
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1
                                }}
                                onChange={this.onOkDatePickerEnd}
                              />
                            </Col>
                          )}
                        </Form.Item>
                      </Col>

                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="m-0 p-0"></Col>
                    </Row>
                    <Row className="p-0 m-0 mt-0">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("statusContract", {
                            rules: [
                              {
                                message: "chọn trạng thái"
                              }
                            ]
                          })(
                            <Col className="m-0 p-0">
                              <ITitle
                                level={4}
                                title={"Trạng thái"}
                                style={{
                                  color: colors.black,
                                  fontWeight: 600,
                                  marginBottom: 10
                                }}
                              />
                              <ISelectStatus
                                style={{
                                  width: "100%",
                                  background: colors.white,
                                  borderStyle: "solid",
                                  borderWidth: 0,
                                  borderColor: colors.line,
                                  borderBottomWidth: 1
                                }}
                                onChange={this.onChangeStatus}
                                placeholder="Chọn trạng thái"
                                value={contract.status}
                                isBackground={false}
                              />
                            </Col>
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs="auto">
                        <div style={{ width: 4 }}></div>
                      </Col>
                      <Col className="m-0 p-0"></Col>
                    </Row>
                  </div>
                </Col>
                <Col className="m-0 p-0 " xs="auto">
                  <div
                    style={{
                      width: 4,
                      height: "100%",
                      background: colors.background
                    }}
                  ></div>
                </Col>

                <Col
                  className="m-0 p-0"
                  style={{ background: colors.white }}
                  xs="auto"
                >
                  <div
                    style={{ marginRight: 40, marginLeft: 40, marginTop: 30 }}
                  >
                    <ITitle
                      level={4}
                      title="Hình ảnh"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        marginBottom: 26
                      }}
                    />
                    <Form.Item>
                      {getFieldDecorator("image", {
                        rules: [
                          {
                            message: "chọn hình",
                            valuePropName: "fileList",
                            getValueFromEvent: this.normFile
                          }
                        ]
                      })(
                        <Row className="m-0 p-0">
                          <IUpload
                            type={ImageType.SHIPPER}
                            url={this.state.url}
                            name={shipper.image[0]}
                            callback={file => {
                              shipper.image = file;
                              this.setState({});
                            }}
                          />
                        </Row>
                      )}
                    </Form.Item>
                  </div>
                </Col>
              </Row>
            </div>
          </Form>
        )}
        {/* </Col> */}
      </Container>
    );
  }

  async componentDidMount() {
    await this._APIgetDetailShipper(this.IDShipper);
  }

  _APIgetDetailShipper = async shipper_id => {
    try {
      const data = await APIService._getDetailShipper(shipper_id);
      let url = data.image_url;
      const shipper = data.shipper;
      const contract = shipper.contract;
      this.state.data.shipper.id = shipper.id;
      this.state.data.shipper.name = shipper.name;
      this.state.data.shipper.code = shipper.code;
      this.state.data.shipper.phone = shipper.phone;
      this.state.data.shipper.email = shipper.email;
      this.state.data.shipper.cmnd = shipper.cmnd;
      this.state.data.shipper.address = shipper.address;
      this.state.data.shipper.image = shipper.image;
      this.state.data.contract.id = contract.id;
      this.state.data.contract.contract_number = contract.numberContract;
      this.state.data.contract.start_date = contract.start_date;
      this.state.data.contract.end_date = contract.end_date;
      this.state.data.contract.status = contract.status;
      this.state.data.contract.warehouse_id = contract.warehouse.id;

      this.setState({
        data: this.state.data,
        url: url,
        loading: true
      });
    } catch (err) {
      console.log(err);
    }
  };
}
const editShipperPage = Form.create({ name: "EditShipperPage" })(
  EditShipperPage
);

export default editShipperPage;
