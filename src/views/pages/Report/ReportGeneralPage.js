import React, { useState, useEffect } from "react";
import { Row, Col, Icon, message, Skeleton } from "antd";
import { ITitle, ISelect, IButton } from "../../components";
import { colors } from "../../../assets";
import {
  IDatePickerFrom,
  TagP,
  StyledSearchCustom,
  StyledISelect,
  IError,
} from "../../components/common";
import moment from "moment";
import { APIService } from "../../../services";

export default function ReportGeneralPage() {
  var timeSearch = null;
  const [dataTypeReport, setDataTypeReport] = useState([
    {
      name: "Báo cáo bán hàng",
      type: 1,
      active: true,
    },
    // {
    //   name: "Báo cáo Kinh doanh theo Tuyến bán hàng",
    //   type: 2,
    //   active: false,
    // },
    {
      name: "Báo cáo Kinh doanh theo Đại lý cấp 1",
      type: 3,
      active: false,
    },
    // {
    //   name: "Báo cáo Kinh doanh theo Đại lý cấp 2",
    //   type: 4,
    //   active: false,
    // },
    {
      name: "Báo cáo chi tiết Đơn hàng của Đại lý cấp 1",
      type: 5,
      active: false,
    },
    // {
    //   name: "Báo cáo chi tiết Đơn hàng của Đại lý cấp 2",
    //   type: 6,
    //   active: false,
    // },
    // {
    //   name: "Báo cáo kết quả ghé thăm Đại lý Cấp 2 của Nhân viên Bán hàng",
    //   type: 7,
    //   active: false,
    // },
  ]);
  const [isOpenPayThereWard, setIsOpenPayThereWard] = useState(false);
  const [isOpenApply, setIsOpenApply] = useState(false);

  const [render, setRender] = useState(false);
  const [filterData, setFillterData] = useState({
    start_date: 0,
    end_date: 0,
    type_report: 1,
    agency_id: 0,
    route_id: 0,
  });
  const [getListBranch, setGetListBranch] = useState({
    page: 1,
    keySearch: "",
  });
  const [getListRoute, setGetListRoute] = useState({
    page: 1,
    keySearch: "",
  });
  const [listBranch, setListBranch] = useState([]);
  const [listBranchNews, setListBranchNews] = useState([]);
  const [listRoute, setListRoute] = useState([]);
  const [list_agency_id, setListAgencyId] = useState([]);
  const [list_route_id, setListRouteId] = useState([]);
  const [list_route_name, setListRouteName] = useState([]);
  const [isSelect, setDisableSelect] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const format = "DD-MM-YYYY";

  function selectTypeReport(index1) {
    dataTypeReport.map((item, index) => {
      if (index1 == index) {
        item.active = true;

        setRender(!render);
      } else {
        item.active = false;
        setRender(!render);
      }
    });
  }
  function onScrollRoute(event) {
    var target = event.target;
    if (target.scrollHeight - target.scrollTop === target.clientHeight) {
      setGetListRoute({ ...getListRoute, page: getListRoute.page + 1 });
    }
  }
  function onScrollAgency(event) {
    var target = event.target;
    if (target.scrollHeight - target.scrollTop === target.clientHeight) {
      setGetListBranch({ ...getListBranch, page: getListBranch.page + 1 });
      console.log(getListBranch.page);
    }
  }

  const _getAPIListBranch = async () => {
    try {
      const data = await APIService._getDropListBranch(
        getListBranch.page,
        getListBranch.keySearch
      );
      var dataNew = data.agency.map((item, index) => {
        const value = item.name;
        const key = item.id;
        // StatusPare.push({ value, key });
        return {
          key,
          value,
        };
      });
      if (getListBranch.page > 1) {
        setListBranch([...listBranch, ...dataNew]);
      } else {
        setListBranch(dataNew);
        setIsLoading(false);
        setRender(!render);
      }
    } catch (error) {}
  };
  const _getAPIListRoute = async () => {
    try {
      var obj = {
        list_agency_id: list_agency_id,
        key: getListRoute.keySearch,
        page: getListRoute.page,
      };
      const data = await APIService._getDropListRoute(obj);
      var dataNew = data.route.map((item, index) => {
        const value = item.name;
        const key = item.id;
        const parent_id = 1;
        // StatusPare.push({ value, key });
        return {
          key,
          value,
        };
      });

      if (getListRoute.page > 1) {
        setListRoute([...listRoute, ...dataNew]);
      } else {
        setListRoute(dataNew);
      }
    } catch (error) {}
  };
  const handleStartOpenChange = (open) => {
    if (!open) {
      setIsOpenApply(true);
    }
  };

  const disabledStartDateApply = (startValue) => {
    if (!startValue || !filterData.end_date) {
      return false;
    }
    return startValue.valueOf() > filterData.end_date;
  };
  const disabledEndDateApply = (endValue) => {
    if (!endValue || !filterData.start_date) {
      return false;
    }
    return endValue.valueOf() <= filterData.start_date;
  };
  const handleEndOpenChange = (open) => {
    setIsOpenApply(open);
  };
  const getAPIReportSell = async () => {
    try {
      const data = await APIService._getDataReportSell(
        filterData.start_date,
        filterData.end_date
      );
      window.open(data.file_url + data.file_name);
      message.success("Xuất báo cáo thành công");
    } catch (error) {}
  };
  const getAPIReportTradingS1 = async () => {
    try {
      const data = await APIService._getDataReportTrandingS1(
        filterData.start_date,
        filterData.end_date
      );
      window.open(data.file_url + data.file_name);
      message.success("Xuất báo cáo thành công");
    } catch (error) {}
  };
  const getAPIReportTradingS2 = async () => {
    try {
      const data = await APIService._getDataReportTrandingS2(
        filterData.start_date,
        filterData.end_date
      );
      window.open(data.file_url + data.file_name);
      message.success("Xuất báo cáo thành công");
    } catch (error) {}
  };
  const getAPIReportRouteSell = async () => {
    try {
      const data = await APIService._getDataReportRouteSell(
        filterData.start_date,
        filterData.end_date
      );
      window.open(data.file_url + data.file_name);
      message.success("Xuất báo cáo thành công");
    } catch (error) {}
  };
  const getAPIReportDetailORderS1 = async () => {
    try {
      const data = await APIService._getDataReportDetailOrderS1(
        filterData.start_date,
        filterData.end_date
      );
      window.open(data.file_url + data.file_name);
      message.success("Xuất báo cáo thành công");
    } catch (error) {}
  };
  const getAPIReportDetailORderS2 = async () => {
    try {
      const data = await APIService._getDataReportDetailOrderS2(
        filterData.start_date,
        filterData.end_date
      );
      window.open(data.file_url + data.file_name);
      message.success("Xuất báo cáo thành công");
    } catch (error) {}
  };
  const getAPIReportNvbhVisitS2 = async () => {
    try {
      const data = await APIService._getDataReportNvbhVisitS2(
        filterData.start_date,
        filterData.end_date,
        list_agency_id,
        list_route_id
      );
      window.open(data.file_url + data.file_name);
      message.success("Xuất báo cáo thành công");
    } catch (error) {
      //
    }
  };
  function getAPI(type_report) {
    switch (type_report) {
      case 1:
        getAPIReportSell(); // API báo cáo bán hàng
        break;
      case 2:
        getAPIReportRouteSell(); // API báo cáo tuyến bán hàng
        break;
      case 3:
        getAPIReportTradingS1(); // API báo cáo kinh doanh S1
        break;
      case 4:
        getAPIReportTradingS2(); // API báo cáo kinh doanh S2
        break;
      case 5:
        getAPIReportDetailORderS1();
        break;
      case 6:
        getAPIReportDetailORderS2();
        break;
      case 7:
        getAPIReportNvbhVisitS2();
        break;
      default:
        break;
    }
  }

  async function _getAPIDropList() {
    await _getAPIListBranch();
    if (!list_agency_id || list_agency_id.length === 0) {
      return;
    }
    await _getAPIListRoute();
  }
  useEffect(() => {
    _getAPIDropList();
  }, [getListBranch, getListRoute]);

  return (
    <div style={{ width: "100%" }}>
      <Row>
        <Col span={24} style={{ marginTop: 12 }}>
          <ITitle
            level={1}
            title="Trích xuất báo cáo tổng hợp"
            style={{ color: colors.main, fontWeight: "bold" }}
          />
        </Col>
      </Row>
      <Row style={{ paddingTop: 100 }}>
        <Col
          span={24}
          style={{
            background: colors.white,
            paddingTop: 30,
            paddingBottom: 30,
            paddingLeft: 40,
            paddingRight: 40,
            width: 615,
          }}
          className="shadow"
        >
          <Row>
            <ITitle
              title="Thiết lập thông tin chu kỳ trích xuất báo cáo"
              level={3}
              style={{ color: colors.text.black, fontWeight: "bold" }}
            />
          </Row>
          <Row style={{ paddingTop: 30 }}>
            <Col span={24}>
              <Row gutter={[16, 0]}>
                <Col span={12}>
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                    </Col>
                    <Col span={24}>
                      <IDatePickerFrom
                        isBorderBottom={true}
                        paddingInput="0px"
                        placeholder="nhập thời gian bắt đầu"
                        disabledDate={disabledStartDateApply}
                        format={format}
                        onChange={(date) => {
                          setFillterData({
                            ...filterData,
                            start_date: date._d.getTime(),
                          });
                        }}
                        onOpenChange={handleStartOpenChange}
                        value={
                          filterData.start_date === 0
                            ? undefined
                            : moment(new Date(filterData.start_date), format)
                        }
                        allowClear={false}
                      />
                    </Col>
                  </Row>
                </Col>
                <Col span={12}>
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <TagP style={{ fontWeight: 500 }}>Kết thúc</TagP>
                    </Col>
                    <Col span={24}>
                      <IDatePickerFrom
                        isBorderBottom={true}
                        paddingInput="0px"
                        placeholder="nhập thời gian kết thúc"
                        disabledDate={disabledEndDateApply}
                        open={isOpenApply}
                        format={format}
                        onOpenChange={handleEndOpenChange}
                        onChange={(date) => {
                          setFillterData({
                            ...filterData,
                            end_date: date._d.getTime(),
                          });
                        }}
                        allowClear={false}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row style={{ paddingTop: 30 }}>
            <ITitle
              title="Chọn báo cáo trích xuất"
              level={3}
              style={{ color: colors.text.black, fontWeight: 500 }}
            />
          </Row>
          <Row style={{ paddingTop: 10 }}>
            {dataTypeReport.map((item, index) => {
              return (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    paddingTop: 5,
                  }}
                >
                  <div
                    style={{
                      width: 20,
                      height: 20,
                      border: item.active
                        ? "5px solid" + `${colors.main}`
                        : "1px solid" + `${colors.line_2}`,
                      borderRadius: 10,
                    }}
                    onClick={() => {
                      setFillterData({
                        ...filterData,
                        type_report: item.type,
                      });
                      console.log(filterData);
                      selectTypeReport(index);
                    }}
                  />
                  <div style={{ flex: 1, marginLeft: 10 }}>
                    <span style={{ fontSize: 14, color: colors.text.black }}>
                      {item.name}
                    </span>
                  </div>
                </div>
              );
            })}
          </Row>
          {/* <Row style={{ paddingTop: 15, paddingLeft: 30, paddingRight: 90 }}>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  width: "100%",
                  border: "1px solid" + `${colors.line_2}`,
                  paddingLeft: 12,
                  opacity: filterData.type_report == 7 ? 1 : 0.5,
                  justifyContent: "center",
                }}
              >
                <Icon
                  type="search"
                  style={{
                    fontSize: 20,
                    color: colors.icon,
                  }}
                />
                <StyledISelect
                  data={listBranch}
                  mode="multiple"
                  disabled={filterData.type_report != 7 ? true : false}
                  onSearch={async (value) => {
                    setIsLoading(true);
                    if (timeSearch) {
                      clearTimeout(timeSearch);
                    }
                    timeSearch = setTimeout(
                      () =>
                        setGetListBranch({
                          ...getListBranch,
                          keySearch: value,
                          page: 1,
                        }),
                      500
                    );
                  }}
                  placeholder="Phòng kinh doanh"
                  style={{ paddingLeft: 5, height: 60 }}
                  // isBackground={false}
                  // isBorderBottom={false}
                  onPopupScroll={onScrollAgency}
                  showSearch
                  onDropdownVisibleChange={(open) => {
                    if (!open) {
                      if (list_agency_id.length === 0) {
                        return null;
                      }
                      _getAPIListRoute();
                      setListRoute([]);
                      return;
                    }
                  }}
                  onChange={async (arrKey, option) => {
                    var array = option.map((obj) => {
                      return parseInt(obj.key);
                    });
                    await setListAgencyId(array);

                    setListRouteName([]);
                    setListRouteId([]);
                  }}
                  loading={isLoading}
                  isSearch={true}
                />
              </div>
            </Col>
          </Row>
           */}
          {/* <Row style={{ paddingTop: 5, paddingLeft: 30, paddingRight: 90 }}>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  width: "100%",
                  border: "1px solid" + `${colors.line_2}`,
                  paddingLeft: 12,
                  opacity: filterData.type_report == 7 ? 1 : 0.5,
                  justifyContent: "center",
                }}
              >
                <Icon
                  type="search"
                  style={{
                    fontSize: 20,
                    color: colors.icon,
                  }}
                />
                <StyledISelect
                  data={listRoute}
                  value={list_route_name}
                  mode="multiple"
                  disabled={filterData.type_report != 7 ? true : false}
                  onSearch={async (value) => {
                    if (timeSearch) {
                      clearTimeout(timeSearch);
                    }
                    timeSearch = setTimeout(
                      () =>
                        setGetListRoute({
                          ...getListRoute,
                          keySearch: value,
                          page: 1,
                        }),
                      500
                    );
                  }}
                  onDropdownVisibleChange={(open) => {
                    if (!open) {
                      if (list_agency_id.length === 0) {
                        return null;
                      }
                      if (timeSearch) {
                        clearTimeout(timeSearch);
                      }
                      timeSearch = setTimeout(
                        () =>
                          setGetListRoute({
                            ...getListRoute,
                            keySearch: "",
                            page: 1,
                          }),
                        500
                      );
                    }
                  }}
                  isSearch={true}
                  placeholder="Tuyến bán hàng"
                  style={{ paddingLeft: 5, height: 60 }}
                  // isBackground={false}
                  // isBorderBottom={false}
                  onPopupScroll={onScrollRoute}
                  showSearch
                  onChange={(arrKey, option) => {
                    var array = option.map((obj) => {
                      return parseInt(obj.key);
                    });
                    
                    setListRouteId(array);
                    setListRouteName(arrKey);
                    setGetListRoute({ ...getListRoute, keySearch: "" });
                  }}
                />
              </div>
            </Col>
          </Row>
           */}
          <Row style={{ paddingTop: 15, paddingBottom: 40 }}>
            <Col style={{ display: "flex", justifyContent: "center" }}>
              <IButton
                title="Xuất báo cáo"
                color={colors.main}
                styleHeight={{
                  width: 160,
                }}
                icon={""}
                onClick={async () => {
                  if (!filterData.start_date || !filterData.end_date) {
                    IError("Vui lòng chọn thời gian bắt đầu và kết thúc !");
                    return;
                  }
                  if (
                    filterData.type_report == 7 &&
                    (list_agency_id.length == 0 ||
                      !list_agency_id ||
                      list_route_id.length == 0 ||
                      !list_route_id)
                  ) {
                    IError(
                      "Vui lòng chọn phòng kinh doanh và tuyến bán hàng !"
                    );
                    return;
                  }
                  await getAPI(filterData.type_report);
                }}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
