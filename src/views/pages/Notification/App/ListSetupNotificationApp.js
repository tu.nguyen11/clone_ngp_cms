import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message, Modal } from "antd";
import { ISearch, ISvg, ISelect, ITable, IButton } from "../../../components";
import { colors, images } from "../../../../assets";
import FormatterDay from "../../../../utils/FormatterDay";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import { IInputTextArea } from "../../../components/common";

function ListSetupNotificationApp(props) {
  const [filter, setFilter] = useState({
    status: 2,
    search: "",
    page: 1,
    user_agency_id: 0,
  });

  const [dataTable, setDataTable] = useState({
    listProductType: [],
    total: 1,
    size: 10,
  });

  const [idEdit, setIdEdit] = useState(null);
  const [contentEdit, setContentEdit] = useState("");
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);

  const [isModal, setIsModal] = useState(false);

  const dataStatus = [
    {
      key: 2,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Chờ duyệt",
    },
    {
      key: 1,
      value: "Duyệt",
    },
  ];

  const getListCateBlock = async (filter) => {
    try {
      const data = await APIService._getListCateBlock(filter);
      data.list_product_type.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.disabled = item.status === 1 ? true : false;
        item.rowTable = JSON.stringify({
          id: item.id,
        });
        return item;
      });

      setDataTable({
        listProductType: data.list_product_type,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const postUpdateStatusCateBlock = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postUpdateStatusCateBlock(obj);
      setLoadingButton(false);
      setContentEdit("");
      setIdEdit(null);
      setIsModal(false);
      await getListCateBlock(filter);
      message.success(`Duyệt thành công!`);
    } catch (err) {
      message.error(err);
      setLoadingButton(false);
    }
  };

  useEffect(() => {
    getListCateBlock(filter);
  }, [filter]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },

    getCheckboxProps: (record) => ({
      disabled: record.disabled,
    }),
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },
    {
      title: "Tiêu đề",
      render: (obj) =>
        !obj.product_type_name ? (
          "-"
        ) : (
          <Tooltip title={obj.product_type_name}>
            <span>{obj.product_type_name}</span>
          </Tooltip>
        ),
    },

    {
      title: "Nội dung",
      render: (obj) =>
        !obj.username ? (
          "-"
        ) : (
          <Tooltip title={obj.username}>
            <span>{obj.username}</span>
          </Tooltip>
        ),
    },
    {
      title: "Cập nhật lúc",
      dataIndex: "dms_code",
      key: "dms_code",
      render: (dms_code) =>
        !dms_code ? (
          "-"
        ) : (
          <Tooltip title={dms_code}>
            <span>{dms_code}</span>
          </Tooltip>
        ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      align: "center",
      render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Thiết lập noti app
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tiêu đề">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tiêu đề"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setSelectedRowKeys([]);
                      setFilter({
                        ...filter,
                        search: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={18}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <div>
                        <ISelect
                          defaultValue="Trạng thái"
                          data={dataStatus}
                          select={true}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setSelectedRowKeys([]);
                            setFilter({
                              ...filter,
                              status: value,
                              page: 1,
                            });
                          }}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className="flex justify-end">
                      <IButton
                        title={"Duyệt"}
                        color={colors.main}
                        styleHeight={{
                          width: 120,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          if (selectedRowKeys.length === 0) {
                            message.warning("Vui lòng chọn thông báo");
                            return;
                          }
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listProductType}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    rowKey="rowTable"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    // scroll={{ x: 1600 }}
                    onRow={() => {
                      return {
                        onClick: (event) => {
                          const rowValue =
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey;
                          const objRow = JSON.parse(rowValue);
                          setIdEdit(objRow.id);
                          setIsModal(true);
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isModal}
        footer={null}
        width={600}
        onCancel={() => {
          setIdEdit(null);
          setContentEdit("");
          setIsModal(false);
        }}
        centered={true}
        closable={false}
      >
        <div style={{ height: "100%" }}>
          <Row gutter={[0, 12]}>
            <Col span={24}>
              <div style={{ textAlign: "left" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 18 }}>
                  Chỉnh sửa noti app
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <p style={{ fontWeight: 500 }}>Nội dung</p>
              <IInputTextArea
                style={{
                  minHeight: 200,
                  maxHeight: 300,
                  padding: 6,
                  marginTop: 8,
                  border: "none",
                }}
                placeholder="nhập nội dung chỉnh sửa..."
                value={contentEdit}
                onChange={(e) => {
                  setContentEdit(e.target.value);
                }}
              />
            </Col>
          </Row>
          <Row
            gutter={[17, 0]}
            style={{
              position: "absolute",
              bottom: -60,
              right: 0,
            }}
          >
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.oranges}
                  title="Hủy bỏ"
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    setIdEdit(null);
                    setContentEdit("");
                    setIsModal(false);
                  }}
                  icon={ISvg.NAME.CROSS}
                />
              </div>
            </Col>
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.main}
                  title="Lưu"
                  loading={loadingButton}
                  icon={ISvg.NAME.WRITE}
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    if (contentEdit === "") {
                      message.warning("Vui lòng nhập nội dung chỉnh sửa");
                      return;
                    }
                    let arrID = selectedRowKeys.map((item) => {
                      let itemNew = JSON.parse(item);
                      return itemNew.id;
                    });

                    let obj = {
                      list_id: arrID,
                      status: 1,
                    };

                    postUpdateStatusCateBlock(obj);
                  }}
                />
              </div>
            </Col>
          </Row>
        </div>
      </Modal>
    </div>
  );
}

export default ListSetupNotificationApp;
