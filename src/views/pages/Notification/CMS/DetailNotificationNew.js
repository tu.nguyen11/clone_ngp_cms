import React, { useEffect, useState } from "react";
import { Row, Col, message, Skeleton, Popconfirm } from "antd";
import { ITitle, ISvg, IButton } from "../../../components";
import { TagP } from "../../../components/common";
import { colors } from "../../../../assets";
import "moment/locale/vi";
import { APIService } from "../../../../services";
import { useParams, useHistory } from "react-router-dom";
import FormatterDay from "../../../../utils/FormatterDay";

function DetailNotificationNew(props) {
  const params = useParams();
  const history = useHistory();

  const { id } = params;

  const [dataDetail, setDataDetail] = useState({});
  const [isSkeleton, setIsSkeleton] = useState(true);
  const [urlImg, setUrlImg] = useState("");
  const [isLoadingRemove, setIsLoadingRemove] = useState(false);

  const _APIGetDetailNotification = async (id) => {
    try {
      const data = await APIService._getDetailNotification(id);

      let obj = {
        ...data.notification_response,
        sendHour: new Date(data.notification_response.sendTime).getHours(),
      };

      setIsSkeleton(false);
      setUrlImg(data.image_url);
      setDataDetail({ ...obj });
    } catch (err) {
      console.log(err);
      setIsSkeleton(false);
    }
  };

  const _APIPostRemoveNotification = async (obj) => {
    try {
      const data = await APIService._postRemoveNoti(obj);
      setIsLoadingRemove(false);
      message.success("Xóa thông báo thành công.");
      history.push("/notificationPage");
    } catch (err) {
      console.log(err);
      setIsLoadingRemove(false);
    }
  };

  useEffect(() => {
    _APIGetDetailNotification(id);
  }, [id]);

  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 25]}>
        <Col span={24}>
          <ITitle
            level={1}
            title={"Chi tiết thông báo"}
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            {dataDetail.key_status !== 1 ? null : (
              <IButton
                icon={ISvg.NAME.WRITE}
                title={"Chỉnh sửa"}
                color={colors.main}
                style={{ marginRight: 20, marginLeft: 20 }}
                onClick={() =>
                  history.push(
                    "/notifications/edit/" + id + "/" + dataDetail.key_category
                  )
                }
              />
            )}
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để xóa thông báo."}
              onConfirm={() => {
                setIsLoadingRemove(true);
                let objRemove = {
                  notification_id: [id],
                };
                _APIPostRemoveNotification(objRemove);
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                color={colors.oranges}
                loading={isLoadingRemove}
              />
            </Popconfirm>
          </div>
        </Col>
        <Col span={16}>
          <Row gutter={[20, 0]} type="flex">
            <Col span={12}>
              <div
                className="box-shadow"
                style={{
                  padding: "30px 40px",
                  background: colors.white,
                  height: "100%",
                }}
              >
                {isSkeleton ? (
                  <div>
                    <Skeleton active loading={true} paragraph={{ rows: 18 }} />
                  </div>
                ) : (
                  <Row gutter={[0, 20]}>
                    <Col span={24}>
                      <ITitle
                        level={3}
                        title="Thông tin hiển thị"
                        style={{
                          fontWeight: "bold",
                        }}
                      />
                    </Col>
                    <Col span={24}>
                      <Row gutter={[20, 0]}>
                        <Col span={12}>
                          <div>
                            <p style={{ fontWeight: 500 }}>Phân loại</p>
                            <span>
                              {!dataDetail.category_name
                                ? "-"
                                : dataDetail.category_name}
                            </span>
                          </div>
                        </Col>
                        <Col span={12}>
                          <div>
                            <p style={{ fontWeight: 500 }}>Trạng thái</p>
                            <span>
                              {dataDetail.key_status === 1
                                ? "Đã lên lịch"
                                : dataDetail.key_status === 2
                                ? "Đã gửi"
                                : "-"}
                            </span>
                          </div>
                        </Col>
                      </Row>
                    </Col>
                    {dataDetail.key_category ===
                    3 ? null : dataDetail.key_category === 5 ? (
                      <Col span={24}>
                        <div>
                          <p style={{ fontWeight: 500 }}>Tên phiên bản</p>
                          <span>
                            {!dataDetail.name ? "-" : dataDetail.name}
                          </span>
                        </div>
                      </Col>
                    ) : (
                      <Col span={24}>
                        <div>
                          <p style={{ fontWeight: 500 }}>Tên chính sách</p>
                          <span>
                            {!dataDetail.name ? "-" : dataDetail.name}
                          </span>
                        </div>
                      </Col>
                    )}

                    <Col span={24}>
                      <div>
                        <p style={{ fontWeight: 500 }}>Tiêu đề</p>
                        <span>
                          {!dataDetail.title ? "-" : dataDetail.title}
                        </span>
                      </div>
                    </Col>
                    {dataDetail.key_schedule !== 0 ? null : (
                      <Col span={24}>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "flex-start",
                          }}
                        >
                          <div
                            style={{
                              width: 20,
                              height: 20,
                              border: "5px solid" + `${colors.main}`,
                              borderRadius: 10,
                              marginRight: 10,
                              cursor: "pointer",
                            }}
                          />
                          <span style={{ fontWeight: 500 }}>Gửi ngay</span>
                        </div>
                      </Col>
                    )}
                    <Col span={24}>
                      <Row gutter={[20, 0]}>
                        <Col span={12}>
                          <div>
                            <p style={{ fontWeight: 500 }}>Ngày gửi</p>
                            <span>
                              {!dataDetail.sendTime || dataDetail.sendTime < 0
                                ? "-"
                                : FormatterDay.dateFormatWithString(
                                    dataDetail.sendTime,
                                    "#DD#-#MM#-#YYYY#"
                                  )}
                            </span>
                          </div>
                        </Col>
                        <Col span={12}>
                          <div>
                            <p style={{ fontWeight: 500 }}>Giờ gửi</p>
                            <span>{dataDetail.sendHour}</span>
                          </div>
                        </Col>
                      </Row>
                    </Col>

                    <Col span={24}>
                      <div>
                        <p style={{ fontWeight: 500 }}>Nội dung</p>
                        <span>
                          {!dataDetail.description
                            ? "-"
                            : dataDetail.description}
                        </span>
                      </div>
                    </Col>
                    <Col span={24}>
                      <p style={{ fontWeight: 500 }}>Hình ảnh</p>
                      {dataDetail.image === "" || !dataDetail.image ? (
                        "-"
                      ) : (
                        <img
                          src={urlImg + dataDetail.image}
                          alt="Hình thông báo"
                          style={{
                            marginTop: 10,
                            width: 300,
                            objectFit: "cover",
                            justifyContent: "center",
                          }}
                        />
                      )}
                    </Col>
                  </Row>
                )}
              </div>
            </Col>
            {dataDetail.key_category === 2 ? null : (
              <Col span={12}>
                <div
                  className="box-shadow"
                  style={{
                    height: "100%",
                    padding: "30px 40px",
                    background: colors.white,
                  }}
                >
                  {isSkeleton ? (
                    <div>
                      <Skeleton
                        active
                        loading={true}
                        paragraph={{ rows: 18 }}
                      />
                    </div>
                  ) : (
                    <Row>
                      <Col span={24}>
                        <ITitle
                          level={3}
                          title="Nhóm người nhận"
                          style={{
                            fontWeight: "bold",
                            marginBottom: "25px",
                          }}
                        />
                      </Col>

                      <Col span={24}>
                        <Row gutter={[0, 20]}>
                          <Col span={24}>
                            <div
                              style={{
                                padding: 6,
                                border: "1px solid rgba(122, 123, 123, 0.5)",
                              }}
                            >
                              <div
                                style={{
                                  padding: "10px 0px",
                                  borderBottom:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                }}
                              >
                                <TagP
                                  style={{
                                    paddingBottom: 6,
                                    fontWeight: 500,
                                  }}
                                >
                                  Cấp người dùng
                                </TagP>
                              </div>
                              <div
                                style={{
                                  padding: "6px 6px 0px 10px",
                                }}
                              >
                                <span>Đại lý cấp 1</span>
                              </div>
                            </div>
                          </Col>
                          <Col span={24}>
                            <div
                              style={{
                                padding: 6,
                                border: "1px solid rgba(122, 123, 123, 0.5)",
                              }}
                            >
                              <div
                                style={{
                                  padding: "10px 0px",
                                  borderBottom:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                }}
                              >
                                <TagP
                                  style={{
                                    paddingBottom: 6,
                                    fontWeight: 500,
                                  }}
                                >
                                  Cấp bậc
                                </TagP>
                              </div>
                              <div
                                style={{
                                  padding: "6px 6px 0px 10px",
                                }}
                              >
                                <span>
                                  {!dataDetail.list_membership
                                    ? "-"
                                    : dataDetail.list_membership.map(
                                        (item, index) =>
                                          `${item.name}${
                                            index ===
                                            dataDetail.list_membership.length -
                                              1
                                              ? ""
                                              : ", "
                                          }`
                                      )}
                                </span>
                              </div>
                            </div>
                          </Col>
                          <Col span={24}>
                            <div
                              style={{
                                padding: 6,
                                border: "1px solid rgba(122, 123, 123, 0.5)",
                              }}
                            >
                              <div
                                style={{
                                  padding: "10px 0px",
                                  borderBottom:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                }}
                              >
                                <TagP
                                  style={{
                                    paddingBottom: 6,
                                    fontWeight: 500,
                                  }}
                                >
                                  Vùng địa lý
                                </TagP>
                              </div>
                              <div
                                style={{
                                  padding: "6px 6px 0px 10px",
                                  overflow: "auto",
                                  maxHeight: 200,
                                }}
                              >
                                <span>
                                  {!dataDetail.list_region
                                    ? "-"
                                    : dataDetail.list_region.map(
                                        (item, index) =>
                                          `${item.name}${
                                            index ===
                                            dataDetail.list_region.length - 1
                                              ? ""
                                              : ", "
                                          }`
                                      )}
                                </span>
                              </div>
                            </div>
                          </Col>
                          <Col span={24}>
                            <div
                              style={{
                                padding: 6,
                                border: "1px solid rgba(122, 123, 123, 0.5)",
                              }}
                            >
                              <div
                                style={{
                                  padding: "10px 0px",
                                  borderBottom:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                }}
                              >
                                <TagP
                                  style={{
                                    paddingBottom: 6,
                                    fontWeight: 500,
                                  }}
                                >
                                  Tỉnh/ Thành phố
                                </TagP>
                              </div>
                              <div
                                style={{
                                  padding: "6px 6px 0px 10px",
                                  overflow: "auto",
                                  maxHeight: 200,
                                }}
                              >
                                <span>
                                  {!dataDetail.list_city
                                    ? "-"
                                    : dataDetail.list_city.map(
                                        (item, index) =>
                                          `${item.name}${
                                            index ===
                                            dataDetail.list_city.length - 1
                                              ? ""
                                              : ", "
                                          }`
                                      )}
                                </span>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  )}
                </div>
              </Col>
            )}
          </Row>
        </Col>
      </Row>
    </div>
  );
}

export default DetailNotificationNew;
