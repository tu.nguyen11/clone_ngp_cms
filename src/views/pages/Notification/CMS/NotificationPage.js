import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ITable,
  IDatePicker,
  ISelect,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { Container, Row, Col } from "reactstrap";

import FormatterDay from "../../../../utils/FormatterDay";
import { APIService } from "../../../../services";
import { message, Popconfirm, Tooltip } from "antd";

const NewDate = new Date();
const DateStart = new Date();
// let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return !title ? (
    "-"
  ) : (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 8;
  } else widthColumn = (widthScreen - 300) / 8;
  return widthColumn * type;
}

export default class NotificationPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      dataTable: [],
      loadingTable: true,
      keySearch: "",
      page: 1,
      toDate: NewDate.setHours(23, 59, 59, 999),
      fromDate: NewDate.setHours(0, 0, 0),
      isLoadingRemove: false,
      arrayRemove: [],
      listSelectTypeNotification: [],
      listSelectNotificationGroupUser: [],
      IDdistribute: 0,
      IDStatus: -1,
      listStatus: [],
    };
  }

  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }

  _APIgetAllNotification = async (
    distribute,
    status,
    start_date,
    end_date,
    key,
    page
  ) => {
    try {
      const data = await APIService._getAllNotification(
        distribute,
        status,
        start_date,
        end_date,
        key,
        page
      );

      data.notification_response.map((item, index) => {
        item.stt = (this.state.page - 1) * 10 + index + 1;
        return item;
      });
      this.setState({
        dataTable: data.notification_response,
        totalTable: data.total,
        sizeTable: data.size,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  componentDidMount() {
    this._APIgetConfigNotification();

    this._APIgetAllNotification(
      this.state.IDdistribute,
      this.state.IDStatus,
      this.state.fromDate,
      this.state.toDate,
      this.state.keySearch,
      this.state.page
    );
  }

  _APIgetConfigNotification = async () => {
    try {
      const data = await APIService._getConfigNotification();
      let dataSelectType = data.noti_config.notification_distribute
        .filter((item) => item.id === 2 || item.id === 3 || item.id === 5)
        .map((item, index) => {
          const key = item.id;
          const value = item.name;
          return {
            key,
            value,
          };
        });

      dataSelectType.unshift({
        key: 0,
        value: "Tất cả",
      });

      let notification_group_user =
        data.noti_config.notification_group_user.map((item, index) => {
          const key = item.id;
          const value = item.name;
          return {
            key,
            value,
          };
        });

      let listStatus = data.noti_config.notification_status.map((item) => {
        return {
          key: item.id,
          value: item.name,
        };
      });

      listStatus.unshift({
        key: -1,
        value: "Tất cả",
      });

      this.setState({
        listSelectTypeNotification: dataSelectType,
        listSelectNotificationGroupUser: notification_group_user,
        listStatus: listStatus,
      });
    } catch (err) {
      console.log(err);
    }
  };

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[item].id);
    });
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ arrayRemove: arrayID, selectedRowKeys });
  };

  _APIpostRemoveNoti = async (obj) => {
    try {
      const data = await APIService._postRemoveNoti(obj);
      message.success("Xóa thông báo thành công");
      this.setState(
        {
          loadingTable: true,
          arrayRemove: [],
          selectedRowKeys: [],
          isLoadingRemove: false,
        },
        () =>
          this._APIgetAllNotification(
            this.state.IDdistribute,
            this.state.IDStatus,
            this.state.fromDate,
            this.state.toDate,
            this.state.keySearch,
            this.state.page
          )
      );
    } catch (err) {
      this.setState({
        loadingTable: false,
      });
    }
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Ngày tạo"),
        dataIndex: "create_Date",
        key: "create_Date",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (create_Date) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              create_Date,
              "#DD#/#MM#/#YYYY# #hhh#:#mm#"
            )
          ),
      },
      {
        title: _renderTitle("Phân loại"),
        dataIndex: "category_name",
        key: "category_name",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (category_name) => _renderColumns(category_name),
      },
      {
        title: _renderTitle("Tiêu đề"),
        dataIndex: "title",
        key: "title",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (title) => _renderColumns(title),
      },

      {
        title: _renderTitle("Nội dung"),
        dataIndex: "description",
        key: "description",
        align: "left",
        width: 300,

        render: (description) => (
          <span
            style={{
              wordWrap: "break-word",
              wordBreak: "break-word",
              overflow: "hidden",
            }}
            className="ellipsis-text"
          >
            {description}
          </span>
        ),
      },
      {
        title: _renderTitle("Thời gian gửi"),
        dataIndex: "start_date",
        key: "start_date",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (start_date) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              start_date,
              "#DD#/#MM#/#YYYY# #hhh#:#mm#"
            )
          ),
      },
      {
        title: _renderTitle("Trạng thái"),
        dataIndex: "schedule_name",
        key: "schedule_name",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (schedule_name) => _renderColumns(schedule_name),
      },
      // {
      //   title: "",
      //   align: "right",
      //   width: 50,
      //   dataIndex: "id",
      //   key: "id",
      //   fixed: widthScreen <= 1400 ? "right" : "",

      //   render: (id) => (
      //     <Row>
      //       <Col
      //         onClick={(e) => {
      //           e.stopPropagation();
      //           this.props.history.push("/notification/" + id);
      //         }}
      //         className="cursor"
      //       >
      //         <ISvg
      //           name={ISvg.NAME.CHEVRONRIGHT}
      //           width={11}
      //           height={20}
      //           fill={colors.icon.default}
      //         />
      //       </Col>
      //     </Row>
      //   ),
      // },
    ];

    const { selectedRowKeys, value } = this.state;

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Thông báo"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <ISearch
            onPressEnter={() =>
              this.setState(
                {
                  loadingTable: true,
                  page: 1,
                },
                () =>
                  this._APIgetAllNotification(
                    this.state.IDdistribute,
                    this.state.IDStatus,
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.keySearch,
                    this.state.page
                  )
              )
            }
            onChange={(e) =>
              this.setState({
                keySearch: e.target.value,
              })
            }
            placeholder="Tìm kiếm theo tên thông báo"
            icon={
              <div
                style={{
                  display: "flex",
                  width: 42,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                className="cursor"
                onClick={() =>
                  this.setState(
                    {
                      loadingTable: true,
                    },
                    () =>
                      this._APIgetAllNotification(
                        this.state.IDdistribute,
                        this.state.IDStatus,
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.keySearch,
                        this.state.page
                      )
                  )
                }
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>
        <Row
          className="mt-4"
          style={{
            display: "flex",
          }}
        >
          <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
            <Row className="m-0 p-0 center">
              <ISvg
                name={ISvg.NAME.EXPERIMENT}
                width={20}
                height={20}
                fill={colors.icon.default}
              />
              <div style={{ marginLeft: 25 }}>
                <ITitle title="Từ ngày" level={4} />
              </div>
            </Row>
            <Row className="m-0 p-0 ml-4" style={{}}>
              <IDatePicker
                isBackground
                from={this.state.fromDate}
                to={this.state.toDate}
                style={{
                  background: colors.white,
                  borderWidth: 1,
                  borderColor: colors.line,
                  borderStyle: "solid",
                }}
                onChange={(date, dateString) => {
                  if (date.length <= 1) {
                    return;
                  }
                  this.state.fromDate = date[0]._d.setHours(0, 0, 0);
                  this.state.toDate = date[1]._d.setHours(23, 59, 59);
                  this.setState(
                    {
                      fromDate: this.state.fromDate,
                      toDate: this.state.toDate,
                      page: 1,
                    },
                    () =>
                      this._APIgetAllNotification(
                        this.state.IDdistribute,
                        this.state.IDStatus,
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.keySearch,
                        this.state.page
                      )
                  );
                }}
              />
            </Row>
            <Row className="m-0 p-0 ml-3">
              <div style={{ width: 160 }}>
                <ISelect
                  isBackground={true}
                  defaultValue="Phân loại"
                  select={true}
                  onChange={(value) => {
                    this.setState(
                      {
                        IDdistribute: value,
                        loadingTable: true,
                        page: 1,
                      },
                      () =>
                        this._APIgetAllNotification(
                          this.state.IDdistribute,
                          this.state.IDStatus,
                          this.state.fromDate,
                          this.state.toDate,
                          this.state.keySearch,
                          this.state.page
                        )
                    );
                  }}
                  data={this.state.listSelectTypeNotification}
                />
              </div>
            </Row>
            <Row className="m-0 p-0 ml-3">
              <ISelect
                isBackground={true}
                defaultValue="Trạng thái"
                select={true}
                onChange={(value) => {
                  this.setState(
                    {
                      IDStatus: value,
                      loadingTable: true,
                    },
                    () =>
                      this._APIgetAllNotification(
                        this.state.IDdistribute,
                        this.state.IDStatus,
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.keySearch,
                        1
                      )
                  );
                }}
                data={this.state.listStatus}
              />
            </Row>
          </Row>
          <Row className="m-0 p-0">
            <IButton
              icon={ISvg.NAME.ARROWUP}
              title={"Tạo mới"}
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => this.props.history.push("/notifications/add/0/0")}
            />
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để xóa thông báo."}
              onConfirm={() => {
                if (this.state.arrayRemove.length == 0) {
                  message.warning("Chưa chọn thông báo để xóa.");
                  return;
                }
                this.setState({
                  isLoadingRemove: true,
                });
                let objRemove = {
                  notification_id: this.state.arrayRemove,
                };
                this._APIpostRemoveNoti(objRemove);
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                color={colors.oranges}
                loading={this.state.isLoadingRemove}
              />
            </Popconfirm>
          </Row>
        </Row>

        <Row className="mt-4">
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%" }}
            rowSelection={rowSelection}
            defaultCurrent={this.state.page}
            maxpage={6}
            sizeItem={this.state.sizeTable}
            indexPage={this.state.page}
            maxpage={this.state.totalTable}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );
                  const id = this.state.dataTable[indexRow].id;
                  this.props.history.push("/notification/detail/" + id);
                }, // click row
                onMouseDown: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );
                  const id = this.state.dataTable[indexRow].id;
                  if (event.button == 2 || event == 4) {
                    window.open("/notification/detail/" + id, "_blank");
                  }
                },
              };
            }}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                  loadingTable: true,
                },
                () =>
                  this._APIgetAllNotification(
                    this.state.IDdistribute,
                    this.state.IDStatus,
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.keySearch,
                    this.state.page
                  )
              );
            }}
            // scroll={{ x: this.state.width }}
          />
        </Row>
      </Container>
    );
  }
}
