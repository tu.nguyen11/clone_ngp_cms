import React, { useEffect, useState, useRef } from "react";
import {
  Row,
  Col,
  Form,
  message,
  Skeleton,
  Tag,
  Popconfirm,
  DatePicker,
} from "antd";
import {
  ITitle,
  ISvg,
  IButton,
  ISelect,
  IInput,
  IUpload,
  IAutoComplete,
} from "../../../components";
import { StyledISelect } from "../../../components/common";
import { colors } from "../../../../assets";
import moment from "moment";
import "moment/locale/vi";
import { APIService } from "../../../../services";
import { ImageType } from "../../../../constant";
import { useParams, useHistory } from "react-router-dom";
import locale from "antd/es/date-picker/locale/vi_VN";

let listHours = [];

for (let i = 0; i <= 23; i++) {
  listHours.push({ key: i + 1, value: i });
}

function CreateNotification(props) {
  const typeTimeRef = useRef(null);
  const params = useParams();
  const history = useHistory();
  const format = "DD-MM-YYYY";

  const { type, id, typeNoti } = params;
  const [boolean, setBoolean] = useState(type === "add" ? true : false);
  const [sendNow, setSendNow] = useState(false);

  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;

  const [isLoadingSave, setIsLoadingSave] = useState(false);
  const [isSkeleton, setIsSkeleton] = useState(true);
  const [urlImg, setUrlImg] = useState("");

  const [optionsEvent, setOptionsEvent] = useState([]);
  const [optionsProduct, setOptionsProduct] = useState([]);
  const [search, setSearch] = useState("");
  const [classifyNoti, setClassifyNoti] = useState(5);

  const [dropdownRegion, setDropdownRegion] = useState([]);
  const [dropdownCity, setDropdownCity] = useState([]);
  const [dropdownMembership, setDropdownMembership] = useState([]);
  const [loadingCity, setLoadingCity] = useState(false);
  const [reload, setReload] = useState(true);

  const [arrayCity, setArrayCity] = useState([]);
  const [arrayCityClone, setArrayCityClone] = useState([]);
  const [arrayRegion, setArrayRegion] = useState([]);
  const [arrayMembership, setArrayMembership] = useState([]);

  const [checkConditionRegion, setCheckConditionRegion] = useState(false);
  const [checkConditionCity, setCheckConditionCity] = useState(false);
  const [checkConditionMembership, setCheckConditionMembership] =
    useState(false);

  const [iDProgramme, setIDProgramme] = useState(null);

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);

    return startValue.valueOf() < dateNow.getTime();
  };

  const onSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields((err, values) => {
      if (err) {
        if (arrayRegion.length === 0) {
          setCheckConditionRegion(true);
        }

        if (arrayMembership.length === 0) {
          setCheckConditionMembership(true);
        }

        if (arrayCity.length === 0) {
          setCheckConditionCity(true);
        }
        return;
      }

      if (values.classifyNoti !== 2) {
        if (
          arrayRegion.length === 0 ||
          arrayMembership.length === 0 ||
          arrayCity.length === 0
        ) {
          if (arrayRegion.length === 0) {
            setCheckConditionRegion(true);
          }

          if (arrayMembership.length === 0) {
            setCheckConditionMembership(true);
          }

          if (arrayCity.length === 0) {
            setCheckConditionCity(true);
          }
          message.warning("Vui lòng chọn nhóm người nhận");
          return;
        }
      }

      let startTimeNew;
      if (!sendNow) {
        let dateNow = new Date();
        // if (dateNow.getMinutes() > 0 || dateNow.getSeconds() > 0) {
        //   dateNow.setHours(dateNow.getHours() + 1);
        // }

        dateNow.setMinutes(0, 0, 0);

        startTimeNew = new Date(values.startTime);
        startTimeNew.setHours(Number(values.sendHour) - 1);

        if (startTimeNew.getTime() <= dateNow.getTime()) {
          message.warning(
            "Vui lòng chọn thời gian đặt lịch lớn hơn thời gian hiện tại!"
          );
          return;
        }
      }

      if (
        !values.classifyNoti ||
        values.classifyNoti === 2 ||
        values.classifyNoti === 5
      ) {
        if (!iDProgramme) {
          if (!values.classifyNoti || values.classifyNoti === 5) {
            message.warning("Vui lòng chọn phiên bản!");
            return;
          }

          message.warning("Vui lòng chọn chính sách!");
          return;
        }
      }

      if (boolean) {
        setIsLoadingSave(true);
        const objAdd = {
          push_type: !values.classifyNoti ? 5 : Number(values.classifyNoti),
          link_id: !values.classifyNoti
            ? Number(iDProgramme)
            : values.classifyNoti === 3
            ? 0
            : Number(iDProgramme),
          title: values.summaryContent,
          description: values.content,
          image: typeof values.img === "undefined" ? "" : values.img,
          send_time: sendNow ? 0 : startTimeNew.getTime(),
          schedule_id: sendNow ? 0 : 1,
          status: 1,
          type: !values.classifyNoti
            ? 2
            : values.classifyNoti === 2
            ? 0
            : values.classifyNoti === 5
            ? 2
            : 1,
          membership_id:
            values.classifyNoti === 2
              ? []
              : arrayMembership.length === 0
              ? []
              : [...arrayMembership],
          city_id:
            values.classifyNoti === 2
              ? []
              : arrayCity.length === 0
              ? []
              : [...arrayCity],
        };
        console.log("objAdd: ", objAdd);

        _APIPostAddNotification(objAdd);
      } else {
        setIsLoadingSave(true);
        const objEdit = {
          id: id,
          push_type: values.classifyNoti,
          link_id: Number(iDProgramme),
          title: values.summaryContent,
          description: values.content,
          image: typeof values.img === "undefined" ? "" : values.img,
          send_time: sendNow ? 0 : startTimeNew.getTime(),
          schedule_id: sendNow ? 0 : 1,
          status: 1,
          type:
            values.classifyNoti === 2 ? 0 : values.classifyNoti === 5 ? 2 : 1,
          membership_id:
            values.classifyNoti === 2
              ? []
              : arrayMembership.length === 0
              ? []
              : [...arrayMembership],
          city_id:
            values.classifyNoti === 2
              ? []
              : arrayCity.length === 0
              ? []
              : [...arrayCity],
        };
        console.log("objEdit: ", objEdit);
        _APIPostEditNotification(objEdit);
      }
    });
  };

  const _APIGetDetailNotification = async (id) => {
    try {
      const data = await APIService._getDetailNotification(id);
      let objData = { ...data.notification_response };
      props.form.getFieldDecorator("classifyNoti", {
        initialValue: 1,
      });
      props.form.getFieldDecorator("IDProgramme", {
        initialValue: 0,
      });
      props.form.getFieldDecorator("summaryContent", {
        initialValue: "",
      });
      props.form.getFieldDecorator("startTime", {
        initialValue: undefined,
      });
      props.form.getFieldDecorator("content", {
        initialValue: "",
      });
      props.form.getFieldDecorator("img", {
        initialValue: "",
      });
      props.form.getFieldDecorator("sendHour", {
        initialValue: "",
      });

      reduxForm.setFieldsValue({
        classifyNoti: objData.key_category,
        IDProgramme: objData.name,
        summaryContent: objData.title,
        startTime: objData.sendTime,
        content: objData.description,
        img: !objData.image ? null : objData.image,
        sendHour: new Date(objData.sendTime).getHours() + 1,
      });

      setIDProgramme(objData.link_id);
      if (objData.key_schedule === 0) {
        setSendNow(true);
      }

      if (
        data.notification_response.key_category === 5 ||
        data.notification_response.key_category === 3
      ) {
        let idRegion = data.notification_response.list_region.map(
          (item) => item.id
        );

        let idMembership = data.notification_response.list_membership.map(
          (item) => item.id
        );

        let idCity = data.notification_response.list_city.map(
          (item) => item.id
        );

        let arrCity = data.notification_response.list_city.map((item) => {
          const key = item.id;
          const value = item.name;
          const parent = item.parent;
          return { key, value, parent };
        });
        setArrayMembership([...idMembership]);
        setArrayRegion([...idRegion]);
        setArrayCity([...idCity]);
        setArrayCityClone([...arrCity]);
        await _fetchListCityByRegion(idRegion);
      }

      setUrlImg(data.image_url);
      setIsSkeleton(false);
    } catch (err) {
      console.log(err);
      setIsSkeleton(false);
    }
  };

  const _getAPIListMembership = async () => {
    try {
      const data = await APIService._getListMemberships();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDropdownMembership([...dataNew]);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchListEvent = async (search) => {
    try {
      const data = await APIService._getListAllEventRunning(search);
      const dataNew = data.listEvent.map((item) => {
        const id = item.id;
        const value = item.name.toLowerCase();
        const text = item.name;
        return { id, value, text };
      });
      setOptionsEvent(dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchListProduct = async (search) => {
    try {
      const data = await APIService._getListAllVersionProduct(search, 0);

      const dataNew = data.list_product.map((item) => {
        const id = item.id;
        const value = item.name.toLowerCase();
        const text = item.name;
        return { id, value, text };
      });
      setOptionsProduct(dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchGetListRegion = async () => {
    try {
      const data = await APIService._getAllAgencyGetRegion();
      let dataNew = data.agency.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      dataNew.unshift({
        key: 0,
        value: "Toàn quốc",
      });
      setDropdownRegion(() => dataNew);
      setReload(false);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchListCityByRegion = async (
    arrayRegion,
    all = false,
    remove = false
  ) => {
    try {
      const data = await APIService._getListCityByRegion(arrayRegion);

      let dataNew = data.list_cities.map((item) => {
        const key = item.id;
        const value = item.name;
        const parent = item.parent;
        return { key, value, parent };
      });

      if (!reload) {
        if (!remove) {
          if (all) {
            let arrID = data.list_cities.map((item) => item.id);

            setArrayCity([...arrID]);
            setArrayCityClone([...data.list_cities]);
          } else {
            if (arrayCity.length === 0) {
              let arrID = data.list_cities.map((item) => item.id);
              setArrayCity([...arrID]);
              setArrayCityClone([...data.list_cities]);
            } else {
              let dataCityNew = [];
              let dataIdNew = [];
              data.list_cities.forEach((item) => {
                if (item.parent === arrayRegion[arrayRegion.length - 1]) {
                  dataCityNew.push(item);
                  dataIdNew.push(item.id);
                }
              });

              let dataConcat = [...arrayCityClone, ...dataCityNew];
              let dataIdConcat = [...arrayCity, ...dataIdNew];

              setArrayCity([...dataIdConcat]);
              setArrayCityClone([...dataConcat]);
            }
          }
        }
      }

      setDropdownCity(dataNew);
      setLoadingCity(false);
      setReload(false);
    } catch (error) {
      console.log(error);
      setLoadingCity(false);
    }
  };

  const callAPI = async () => {
    await _fetchGetListRegion();
    await _getAPIListMembership();
    if (!boolean) {
      await _APIGetDetailNotification(id);
    } else {
      setIsSkeleton(false);
    }
  };

  useEffect(() => {
    callAPI();
  }, []);

  useEffect(() => {
    if (Number(typeNoti) === 2 || classifyNoti === 2) {
      _fetchListEvent(search);
    }
    if (Number(typeNoti) === 5 || classifyNoti === 5) {
      _fetchListProduct(search);
    }
  }, [search, classifyNoti]);

  const _APIPostAddNotification = async (obj) => {
    try {
      const data = await APIService._postAddNotification(obj);

      setIsLoadingSave(false);
      message.success("Thêm thông báo thành công.");
      history.push("/notificationPage");
    } catch (err) {
      console.log(err);
      setIsLoadingSave(false);
    }
  };

  const _APIPostEditNotification = async (obj) => {
    try {
      const data = await APIService._postEditNotification(obj);
      setIsLoadingSave(false);

      message.success("Chỉnh sửa thông báo thành công.");
      history.push("/notification/detail/" + id);
      await _APIGetDetailNotification(id);
    } catch (err) {
      console.log(err);
      setIsLoadingSave(false);
    }
  };

  return (
    <div style={{ width: "100%" }}>
      <Form onSubmit={onSubmit}>
        <Row gutter={[12, 25]}>
          <Col span={24}>
            <ITitle
              level={1}
              title={boolean ? "Tạo thông báo mới" : "Chỉnh sửa thông báo"}
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                flex: 1,
                display: "flex",
                alignItems: "flex-end",
              }}
            />
          </Col>
          <Col span={24}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                width: "100%",
                justifyContent: "flex-end",
              }}
            >
              <IButton
                icon={ISvg.NAME.SAVE}
                title={"Đặt lịch"}
                loading={isLoadingSave}
                color={colors.main}
                style={{ marginRight: 20, marginLeft: 20 }}
                htmlType="submit"
              />
              <IButton
                icon={ISvg.NAME.CROSS}
                title="Hủy"
                color={colors.oranges}
                onClick={() => {
                  if (boolean) {
                    history.push("/notificationPage");
                  } else {
                    history.push("/notification/detail/" + id);
                  }
                }}
              />
            </div>
          </Col>
          <Col span={16}>
            <Row gutter={[20, 0]} type="flex">
              <Col span={12}>
                <div
                  className="box-shadow"
                  style={{
                    padding: "30px 40px",
                    background: colors.white,
                    height: "100%",
                  }}
                >
                  {isSkeleton ? (
                    <div>
                      <Skeleton
                        active
                        loading={true}
                        paragraph={{ rows: 18 }}
                      />
                    </div>
                  ) : (
                    <Row>
                      <Col span={24}>
                        <ITitle
                          level={3}
                          title="Thông tin hiển thị"
                          style={{
                            fontWeight: "bold",
                            marginBottom: "25px",
                          }}
                        />
                      </Col>
                      <Col span={24}>
                        <Form.Item>
                          {getFieldDecorator("classifyNoti", {
                            rules: [
                              {
                                required: false,
                                message: "chọn loại thông báo",
                              },
                            ],
                          })(
                            <div>
                              <p style={{ fontWeight: 500 }}>Phân loại</p>
                              {boolean ? (
                                <ISelect
                                  isBackground={false}
                                  placeholder="chọn loại thông báo"
                                  select={true}
                                  defaultValue={5}
                                  onChange={async (key, value) => {
                                    setClassifyNoti(key);
                                    reduxForm.setFieldsValue({
                                      classifyNoti: key,
                                      IDProgramme: null,
                                    });
                                    setSearch("");
                                  }}
                                  data={[
                                    { key: 5, value: "Sản phẩm" },
                                    { key: 2, value: "Chính sách" },
                                    { key: 3, value: "Quảng bá" },
                                  ]}
                                />
                              ) : (
                                <ISelect
                                  isBackground={false}
                                  placeholder="chọn loại thông báo"
                                  select={true}
                                  // defaultValue={5}
                                  value={reduxForm.getFieldValue(
                                    "classifyNoti"
                                  )}
                                  onChange={(key, value) => {
                                    setClassifyNoti(key);
                                    reduxForm.setFieldsValue({
                                      classifyNoti: key,
                                      IDProgramme: null,
                                    });
                                    setSearch("");
                                  }}
                                  data={[
                                    { key: 5, value: "Sản phẩm" },
                                    { key: 2, value: "Chính sách" },
                                    { key: 3, value: "Quảng bá" },
                                  ]}
                                />
                              )}
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                      {reduxForm.getFieldValue("classifyNoti") ===
                      3 ? null : reduxForm.getFieldValue("classifyNoti") !==
                        2 ? (
                        <Col span={24}>
                          <Form.Item>
                            {getFieldDecorator("IDProgramme", {
                              rules: [
                                {
                                  required: true,
                                  message: "nhập tên phiên bản",
                                },
                              ],
                            })(
                              <div>
                                <p style={{ fontWeight: 500 }}>Tên phiên bản</p>
                                <IAutoComplete
                                  options={optionsProduct}
                                  search={search}
                                  optionLabelProp="text"
                                  placeholder="Nhập tên phiên bản"
                                  value={reduxForm.getFieldValue("IDProgramme")}
                                  onSelect={(idProduct) => {
                                    reduxForm.setFieldsValue({
                                      IDProgramme: idProduct,
                                    });

                                    setIDProgramme(idProduct);

                                    setSearch("");
                                  }}
                                  onSearch={(data) => {
                                    if (typeTimeRef.current) {
                                      clearTimeout(typeTimeRef.current);
                                    }
                                    typeTimeRef.current = setTimeout(() => {
                                      setSearch(data);
                                    }, 300);
                                  }}
                                />
                              </div>
                            )}
                          </Form.Item>
                        </Col>
                      ) : (
                        <Col span={24}>
                          <Form.Item>
                            {getFieldDecorator("IDProgramme", {
                              rules: [
                                {
                                  required: true,
                                  message: "nhập tên chính sách",
                                },
                              ],
                            })(
                              <div>
                                <p style={{ fontWeight: 500 }}>
                                  Tên chính sách
                                </p>
                                <IAutoComplete
                                  options={optionsEvent}
                                  search={search}
                                  optionLabelProp="text"
                                  placeholder="Nhập tên chính sách"
                                  value={reduxForm.getFieldValue("IDProgramme")}
                                  onSelect={(idEvent) => {
                                    reduxForm.setFieldsValue({
                                      IDProgramme: idEvent,
                                    });

                                    setIDProgramme(idEvent);

                                    setSearch("");
                                  }}
                                  onSearch={(data) => {
                                    if (typeTimeRef.current) {
                                      clearTimeout(typeTimeRef.current);
                                    }
                                    typeTimeRef.current = setTimeout(() => {
                                      setSearch(data);
                                      console.log(data);
                                    }, 500);
                                  }}
                                />
                              </div>
                            )}
                          </Form.Item>
                        </Col>
                      )}

                      <Col span={24}>
                        <Form.Item>
                          {getFieldDecorator("summaryContent", {
                            rules: [
                              {
                                type: "string",
                                required: true,
                                message:
                                  "nhập tiêu đề thông báo và chỉ tối đa 65 kí tự",
                                max: 65,
                              },
                            ],
                          })(
                            <div>
                              <p style={{ fontWeight: 500 }}>Tiêu đề</p>
                              <IInput
                                loai="input"
                                placeholder="nhập tiêu đề thông báo"
                                value={reduxForm.getFieldValue(
                                  "summaryContent"
                                )}
                              />
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "flex-start",
                          }}
                        >
                          <div
                            style={{
                              width: 20,
                              height: 20,
                              border: sendNow
                                ? "5px solid" + `${colors.main}`
                                : "1px solid" + `${colors.line_2}`,
                              borderRadius: 10,
                              marginRight: 10,
                              marginBottom: 20,
                              cursor: "pointer",
                            }}
                            onClick={() => {
                              setSendNow(!sendNow);
                            }}
                          />
                          <span style={{ fontWeight: 500 }}>Gửi ngay</span>
                        </div>
                      </Col>

                      {sendNow ? null : (
                        <Col span={24}>
                          <Row gutter={[20, 0]}>
                            <Col span={12}>
                              <Form.Item>
                                {getFieldDecorator("startTime", {
                                  rules: [
                                    {
                                      required: sendNow ? false : true,
                                      message: "chọn ngày gửi thông báo",
                                    },
                                  ],
                                })(
                                  <div>
                                    <p style={{ fontWeight: 500 }}>Ngày gửi</p>
                                    {boolean ? (
                                      <DatePicker
                                        // allowClear={false}
                                        className="clear-pad"
                                        locale={locale}
                                        showTime={false}
                                        suffixIcon={() => null}
                                        disabledDate={disabledStartDateApply}
                                        format={format}
                                        placeholder="chọn ngày gửi thông báo"
                                        size="large"
                                        style={{
                                          width: "100%",
                                          marginTop: 1,
                                          background: colors.white,
                                          borderStyle: "solid",
                                          borderWidth: 0,
                                          borderColor: colors.line,
                                          borderBottomWidth: 1,
                                        }}
                                        onChange={(date, dateString) => {
                                          let dateNew = date._d.setHours(
                                            0,
                                            0,
                                            0,
                                            0
                                          );
                                          reduxForm.setFieldsValue({
                                            startTime: dateNew,
                                          });
                                        }}
                                      />
                                    ) : (
                                      <DatePicker
                                        // allowClear={false}
                                        className="clear-pad"
                                        locale={locale}
                                        showTime={false}
                                        suffixIcon={() => null}
                                        format={format}
                                        disabledDate={disabledStartDateApply}
                                        placeholder="chọn ngày gửi thông báo"
                                        size="large"
                                        style={{
                                          width: "100%",
                                          marginTop: 1,
                                          background: colors.white,
                                          borderStyle: "solid",
                                          borderWidth: 0,
                                          borderColor: colors.line,
                                          borderBottomWidth: 1,
                                        }}
                                        onChange={(date, dateString) => {
                                          let dateNew = date._d.setHours(
                                            0,
                                            0,
                                            0,
                                            0
                                          );
                                          reduxForm.setFieldsValue({
                                            startTime: dateNew,
                                          });
                                        }}
                                        value={
                                          reduxForm.getFieldValue("startTime") <
                                            3600 ||
                                          !reduxForm.getFieldValue("startTime")
                                            ? undefined
                                            : moment(
                                                new Date(
                                                  reduxForm.getFieldValue(
                                                    "startTime"
                                                  )
                                                ),
                                                format
                                              )
                                        }
                                      />
                                    )}
                                  </div>
                                )}
                              </Form.Item>
                            </Col>
                            <Col span={12}>
                              <Form.Item>
                                {getFieldDecorator("sendHour", {
                                  rules: [
                                    {
                                      required: sendNow ? false : true,
                                      message: "chọn giờ gửi thông báo",
                                    },
                                  ],
                                })(
                                  <div>
                                    <p style={{ fontWeight: 500 }}>Giờ gửi</p>
                                    <ISelect
                                      data={listHours}
                                      select={true}
                                      isBackground={false}
                                      style={{ marginTop: 1 }}
                                      value={
                                        !reduxForm.getFieldValue("sendHour")
                                          ? undefined
                                          : reduxForm.getFieldValue("sendHour")
                                      }
                                      placeholder="chọn giờ gửi thông báo"
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          sendHour: key,
                                        });
                                      }}
                                    />
                                  </div>
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                        </Col>
                      )}
                      <Col span={24}>
                        <Form.Item>
                          {getFieldDecorator("content", {
                            rules: [
                              {
                                type: "string",
                                required: true,
                                message:
                                  "nhập nội dung và chỉ tối đa 240 kí tự",
                                max: 240,
                              },
                            ],
                          })(
                            <div>
                              <p style={{ fontWeight: 500 }}>Nội dung</p>
                              <IInput
                                style={{
                                  color: "#000",
                                  marginTop: 12,
                                  minHeight: 80,
                                }}
                                className="input-border"
                                loai="textArea"
                                placeholder="nhập nội dung thông báo..."
                                value={reduxForm.getFieldValue("content")}
                              />
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                      <Col span={24}>
                        <Form.Item>
                          {getFieldDecorator("img", {
                            rules: [],
                          })(
                            <div>
                              <ITitle
                                level={4}
                                title="Hình ảnh"
                                style={{
                                  fontWeight: "bold",
                                }}
                              />
                              <div
                                style={{
                                  marginTop: 12,
                                  width: 235,
                                  height: "100%",
                                  overflow: "hidden",
                                }}
                              >
                                {boolean ? (
                                  <IUpload
                                    className="border3"
                                    sizeImg={true}
                                    type={ImageType.NOTIFY}
                                    callback={(array, url) => {
                                      setUrlImg(url);
                                      reduxForm.setFieldsValue({
                                        img: array[0],
                                      });
                                    }}
                                  />
                                ) : (
                                  <IUpload
                                    className="border3"
                                    sizeImg={true}
                                    type={ImageType.NOTIFY}
                                    url={urlImg}
                                    name={reduxForm.getFieldValue("img")}
                                    callback={(array) => {
                                      reduxForm.setFieldsValue({
                                        img: array[0],
                                      });
                                    }}
                                  />
                                )}
                              </div>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  )}
                </div>
              </Col>
              {reduxForm.getFieldValue("classifyNoti") === 2 ? null : (
                <Col span={12}>
                  <div
                    className="box-shadow"
                    style={{
                      height: "100%",
                      padding: "30px 40px",
                      background: colors.white,
                    }}
                  >
                    {isSkeleton ? (
                      <div>
                        <Skeleton
                          active
                          loading={true}
                          paragraph={{ rows: 18 }}
                        />
                      </div>
                    ) : (
                      <Row>
                        <Col span={24}>
                          <ITitle
                            level={3}
                            title="Nhóm người nhận"
                            style={{
                              fontWeight: "bold",
                              marginBottom: "25px",
                            }}
                          />
                        </Col>

                        <Col span={24}>
                          <Row gutter={[0, 20]}>
                            <Col span={24}>
                              <div
                                style={{
                                  padding: "10px 12px 12px 12px",
                                  border: "1px solid #d9d9d9",
                                }}
                              >
                                <div
                                  style={{
                                    borderBottom: "1px solid #d9d9d9",
                                    paddingBottom: 10,
                                  }}
                                >
                                  <p style={{ fontWeight: 500 }}>
                                    Cấp người dùng
                                  </p>
                                </div>
                                <ISelect
                                  isBackground={false}
                                  placeholder="chọn cấp người dùng"
                                  select={false}
                                  value={1}
                                  // data={
                                  //   listSelectNotificationGroupUser
                                  // }
                                  data={[
                                    {
                                      key: 1,
                                      value: "Đại lý Cấp 1",
                                    },
                                    {
                                      key: 2,
                                      value: "Đại lý Cấp 2",
                                    },
                                  ]}
                                />
                              </div>
                            </Col>
                            <Col span={24}>
                              <div
                                style={{
                                  padding: "10px 12px 12px 12px",
                                  border:
                                    checkConditionMembership === true
                                      ? "1px solid red"
                                      : "1px solid #d9d9d9",
                                }}
                              >
                                <Row>
                                  <Col span={24}>
                                    <div
                                      style={{
                                        borderBottom: "1px solid #d9d9d9",
                                        paddingBottom: 10,
                                      }}
                                    >
                                      <Row>
                                        <Col span={12}>
                                          <p style={{ fontWeight: 500 }}>
                                            Cấp bậc
                                          </p>
                                        </Col>
                                        <Col span={12}>
                                          <div
                                            style={{
                                              display: "flex",
                                              justifyContent: "flex-end",
                                            }}
                                          >
                                            <Popconfirm
                                              placement="bottom"
                                              title={
                                                <span>
                                                  Xóa tất cả dữ liệu cấp bậc
                                                  đang chọn.
                                                </span>
                                              }
                                              onConfirm={() => {
                                                setArrayMembership([]);
                                              }}
                                              okText="Đồng ý"
                                              cancelText="Hủy"
                                            >
                                              <Tag color={colors.main}>Xóa</Tag>
                                            </Popconfirm>
                                          </div>
                                        </Col>
                                      </Row>
                                    </div>
                                  </Col>
                                  <Col span={24}>
                                    <div>
                                      <StyledISelect
                                        mode="multiple"
                                        isBorderBottom="1px solid #d9d9d9"
                                        placeholder="chọn cấp bậc"
                                        style={{
                                          maxHeight: 200,
                                          overflow: "auto",
                                          paddingTop: 10,
                                        }}
                                        loading={loadingCity}
                                        onChange={(arrKey) => {
                                          const isAll = arrKey.indexOf(0);
                                          if (isAll >= 0) {
                                            let arrayMembership = [
                                              ...dropdownMembership,
                                            ];
                                            arrayMembership.splice(0, 1);
                                            let idArray = arrayMembership.map(
                                              (item) => item.key
                                            );

                                            setArrayMembership([...idArray]);
                                          } else {
                                            setArrayMembership([...arrKey]);
                                          }

                                          setCheckConditionMembership(false);
                                        }}
                                        value={arrayMembership}
                                        data={dropdownMembership}
                                      />
                                    </div>
                                  </Col>
                                </Row>
                              </div>
                            </Col>

                            <Col span={24}>
                              <div
                                style={{
                                  padding: "10px 12px 12px 12px",
                                  border:
                                    checkConditionRegion === true
                                      ? "1px solid red"
                                      : "1px solid #d9d9d9",
                                }}
                              >
                                <Row>
                                  <Col span={24}>
                                    <div
                                      style={{
                                        borderBottom: "1px solid #d9d9d9",
                                        paddingBottom: 10,
                                      }}
                                    >
                                      <Row>
                                        <Col span={12}>
                                          <p style={{ fontWeight: 500 }}>
                                            Vùng địa lý
                                          </p>
                                        </Col>
                                        <Col span={12}>
                                          <div
                                            style={{
                                              display: "flex",
                                              justifyContent: "flex-end",
                                            }}
                                          >
                                            <Popconfirm
                                              placement="bottom"
                                              title={
                                                <span>
                                                  Xóa tất cả dữ liệu vùng địa lý
                                                  đang chọn,
                                                </span>
                                              }
                                              onConfirm={() => {
                                                setArrayCity([]);
                                                setArrayCityClone([]);
                                                setArrayRegion([]);
                                                // _fetchListCityByRegion([]);
                                              }}
                                              okText="Đồng ý"
                                              cancelText="Hủy"
                                            >
                                              <Tag color={colors.main}>Xóa</Tag>
                                            </Popconfirm>
                                          </div>
                                        </Col>
                                      </Row>
                                    </div>
                                  </Col>
                                  <Col span={24}>
                                    <div>
                                      <StyledISelect
                                        mode="multiple"
                                        isBorderBottom="1px solid #d9d9d9"
                                        placeholder="chọn khu vực"
                                        data={dropdownRegion}
                                        value={arrayRegion}
                                        style={{
                                          maxHeight: 200,
                                          overflow: "auto",
                                          paddingTop: 10,
                                        }}
                                        onChange={(arrKey) => {
                                          if (
                                            arrKey.length < arrayRegion.length
                                          ) {
                                            let arrCityAfterRemove = [];
                                            let arrIDCityAfterRemove = [];
                                            arrKey.forEach((key) => {
                                              arrayCityClone.forEach((item) => {
                                                if (item.parent === key) {
                                                  arrCityAfterRemove.push(item);
                                                  arrIDCityAfterRemove.push(
                                                    item.id || item.key
                                                  );
                                                }
                                              });
                                            });

                                            setArrayRegion([...arrKey]);
                                            setArrayCity([
                                              ...arrIDCityAfterRemove,
                                            ]);
                                            setArrayCityClone([
                                              ...arrCityAfterRemove,
                                            ]);

                                            setLoadingCity(true);
                                            _fetchListCityByRegion(
                                              arrKey,
                                              false,
                                              true
                                            );
                                            setCheckConditionRegion(false);
                                            setCheckConditionCity(false);
                                          } else {
                                            if (arrKey.length === 0) {
                                              setArrayCity([]);
                                              setArrayCityClone([]);
                                            }
                                            const isAll = arrKey.indexOf(0);
                                            if (isAll >= 0) {
                                              let arrayAddress = [
                                                ...dropdownRegion,
                                              ];
                                              arrayAddress.splice(0, 1);
                                              let idArray = arrayAddress.map(
                                                (item) => item.key
                                              );

                                              setArrayRegion([...idArray]);
                                              setLoadingCity(true);
                                              _fetchListCityByRegion(
                                                idArray,
                                                true
                                              );
                                              setCheckConditionRegion(false);
                                              setCheckConditionCity(false);
                                            } else {
                                              setArrayRegion([...arrKey]);
                                              setLoadingCity(true);
                                              _fetchListCityByRegion(arrKey);
                                              setCheckConditionRegion(false);
                                              setCheckConditionCity(false);
                                            }
                                          }
                                        }}
                                      />
                                    </div>
                                  </Col>
                                </Row>
                              </div>
                            </Col>
                            <Col span={24}>
                              <div
                                style={{
                                  padding: "10px 12px 12px 12px",
                                  border:
                                    checkConditionCity === true
                                      ? "1px solid red"
                                      : "1px solid #d9d9d9",
                                }}
                              >
                                <Row>
                                  <Col span={24}>
                                    <div
                                      style={{
                                        borderBottom: "1px solid #d9d9d9",
                                        paddingBottom: 10,
                                      }}
                                    >
                                      <Row>
                                        <Col span={12}>
                                          <p style={{ fontWeight: 500 }}>
                                            Tỉnh/Thành
                                          </p>
                                        </Col>
                                        <Col span={12}>
                                          <div
                                            style={{
                                              display: "flex",
                                              justifyContent: "flex-end",
                                            }}
                                          >
                                            <Popconfirm
                                              placement="bottom"
                                              title={
                                                <span>
                                                  Xóa tất cả dữ liệu tỉnh/thành
                                                  đang chọn.
                                                </span>
                                              }
                                              onConfirm={() => {
                                                setArrayCity([]);
                                                setArrayCityClone([]);
                                                setArrayRegion([]);
                                              }}
                                              okText="Đồng ý"
                                              cancelText="Hủy"
                                            >
                                              <Tag color={colors.main}>Xóa</Tag>
                                            </Popconfirm>
                                          </div>
                                        </Col>
                                      </Row>
                                    </div>
                                  </Col>
                                  <Col span={24}>
                                    <div>
                                      <StyledISelect
                                        mode="multiple"
                                        isBorderBottom="1px solid #d9d9d9"
                                        placeholder="chọn tỉnh/thành phố"
                                        style={{
                                          maxHeight: 200,
                                          overflow: "auto",
                                          paddingTop: 10,
                                        }}
                                        loading={loadingCity}
                                        onChange={(arrKey) => {
                                          if (arrKey.length === 0) {
                                            setDropdownCity([]);
                                          }
                                          if (
                                            arrKey.length < arrayCity.length
                                          ) {
                                            let arrCity = arrKey.map((item) => {
                                              return dropdownCity.find(
                                                (item1) => {
                                                  return item1.key === item;
                                                }
                                              );
                                            });
                                            arrayRegion.forEach(
                                              (item, index) => {
                                                let arrCityOfRegion =
                                                  arrCity.filter((key) => {
                                                    return key.parent === item;
                                                  });

                                                if (
                                                  arrCityOfRegion.length === 0
                                                ) {
                                                  let arrRegionNew = [
                                                    ...arrayRegion,
                                                  ];
                                                  arrRegionNew.splice(index, 1);

                                                  setArrayRegion([
                                                    ...arrRegionNew,
                                                  ]);

                                                  setLoadingCity(true);
                                                  _fetchListCityByRegion(
                                                    arrRegionNew,
                                                    false,
                                                    true
                                                  );
                                                }
                                              }
                                            );
                                          }
                                          const isAll = arrKey.indexOf(0);
                                          if (isAll >= 0) {
                                            let arrayCity = [...dropdownCity];
                                            arrayCity.splice(0, 1);
                                            let arrKeyID = [...arrayCity];
                                            let arrID = arrKeyID.map(
                                              (item) => item.key
                                            );

                                            setArrayCity([...arrID]);
                                            setArrayCityClone([...arrKeyID]);
                                            setCheckConditionCity(false);
                                          } else {
                                            let arrKeyID = arrKey.map(
                                              (item) => {
                                                return dropdownCity.find(
                                                  (item1) => {
                                                    return item1.key === item;
                                                  }
                                                );
                                              }
                                            );

                                            setArrayCity([...arrKey]);
                                            setArrayCityClone([...arrKeyID]);

                                            setCheckConditionCity(false);
                                          }
                                        }}
                                        value={arrayCity}
                                        data={dropdownCity}
                                      />
                                    </div>
                                  </Col>
                                </Row>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    )}
                  </div>
                </Col>
              )}
            </Row>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const createNotification = Form.create({ name: "CreateNotification" })(
  CreateNotification
);

export default createNotification;
