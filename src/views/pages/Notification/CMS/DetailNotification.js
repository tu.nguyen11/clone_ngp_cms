import React, { Component } from "react";
import { Row, Col, Skeleton, message, Popconfirm } from "antd";
import { ITitle, ISvg, IButton } from "../../../components";
import { colors } from "../../../../assets";
import FormatterDay from "../../../../utils/FormatterDay";
import { APIService } from "../../../../services";

export default class DetailNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSkeleton: true,
      objDetail: {},
      isLoadingRemove: false,
      url_img: "",
    };
    this.idNotification = Number(this.props.match.params.id);
  }

  _APIgetDetailNotification = async (id) => {
    try {
      const data = await APIService._getDetailNotification(id);

      let obj = {
        ...data.notification_response,
        sendHour: new Date(data.notification_response.sendTime).getHours(),
      };

      this.setState({
        isSkeleton: false,
        objDetail: obj,
        url_img: data.image_url,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        isSkeleton: false,
      });
    }
  };

  componentDidMount() {
    this._APIgetDetailNotification(this.idNotification);
  }

  render() {
    const { objDetail } = this.state;
    return (
      <div style={{ width: "100%" }}>
        <Row>
          <ITitle
            level={1}
            title="Chi tiết thông báo"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
        </Row>
        <Row
          style={{
            marginTop: 20,
            width: "100%",
            display: "flex",
          }}
        >
          <Col
            style={{
              display: "flex",
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            {objDetail.key_status == 2 ? null : (
              <IButton
                icon={ISvg.NAME.WRITE}
                title={"Chỉnh sửa"}
                color={colors.main}
                style={{ marginRight: 20 }}
                onClick={() =>
                  this.props.history.push(
                    "/notifications/edit/" + this.idNotification
                  )
                }
              />
            )}
            <Popconfirm
              placement="bottom"
              title={"Nhấn đồng ý để xóa thông báo."}
              onConfirm={async () => {
                try {
                  this.setState(
                    {
                      isLoadingRemove: true,
                    },
                    async () => {
                      let objRemove = {
                        notification_id: [this.idNotification],
                      };
                      const data = await APIService._postRemoveNoti(objRemove);
                      message.success("Xóa thành công");
                      this.props.history.push("/notificationPage");
                    }
                  );
                } catch (err) {
                  console.log(err);
                  this.setState({
                    isLoadingRemove: false,
                  });
                }
              }}
              okText="Đồng ý"
              cancelText="Hủy"
            >
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                color={colors.oranges}
              />
            </Popconfirm>
          </Col>
        </Row>
        <Row style={{ marginTop: 38 }}>
          <div style={{ width: 530, background: "white" }} className="shadow">
            <Row>
              <Col
                span={24}
                style={{
                  paddingTop: 30,
                  paddingBottom: 30,
                  paddingLeft: 40,
                  paddingRight: 40,
                }}
              >
                <Skeleton
                  active
                  loading={this.state.isSkeleton}
                  paragraph={{ rows: 6 }}
                >
                  <Row gutter={[0, 20]}>
                    <Col style={{ marginTop: 6 }}>
                      <ITitle
                        level={3}
                        title="Thông tin hiển thị"
                        style={{ fontWeight: "bold" }}
                      />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Phân loại"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={objDetail.category_name} />
                    </Col>
                    {objDetail.key_category == 1 ? null : (
                      <Col>
                        <ITitle
                          level={3}
                          title={"ID " + objDetail.category_name}
                          style={{ fontWeight: 500 }}
                        />
                        <ITitle level={4} title={objDetail.link_id} />
                      </Col>
                    )}
                    <Col>
                      <ITitle
                        level={3}
                        title="Nhóm người nhận"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={objDetail.group_user_receive} />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Tiêu đề"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={objDetail.title} />
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Nội dung"
                        style={{ fontWeight: 500 }}
                      />
                      <ITitle level={4} title={objDetail.description} />
                    </Col>
                    <Col>
                      <Row>
                        <Col span={12}>
                          <ITitle
                            level={3}
                            title="Ngày gửi"
                            style={{ fontWeight: 500 }}
                          />
                          <ITitle
                            level={4}
                            title={FormatterDay.dateFormatWithString(
                              objDetail.sendTime,
                              "#DD#/#MM#/#YYYY#"
                            )}
                          />
                        </Col>
                        <Col span={12}>
                          <ITitle
                            level={3}
                            title="Giờ gửi"
                            style={{ fontWeight: 500 }}
                          />
                          <ITitle level={4} title={objDetail.sendHour} />
                        </Col>
                      </Row>
                    </Col>
                    <Col>
                      <ITitle
                        level={3}
                        title="Hình ảnh"
                        style={{ fontWeight: 500 }}
                      />
                      <img
                        src={this.state.url_img + objDetail.image}
                        style={{ width: 235, height: 99 }}
                      />
                    </Col>
                  </Row>
                </Skeleton>
              </Col>
            </Row>
          </div>
        </Row>
      </div>
    );
  }
}
