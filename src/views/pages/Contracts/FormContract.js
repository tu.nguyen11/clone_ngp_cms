import React, { useEffect, useRef, useState } from "react";
import { Row, Col, message } from "antd";
import "./contractForm.css";
import { colors } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { ISvg, IButton } from "../../components";
import { priceFormat } from "../../../utils";
import numberToText from "./readPriceNumber.js";
import moment from "moment";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";
import { APIService } from "../../../services";
import { renderAddress } from "../../../utils";
function FormContract(props) {
  const [loadingButton, setLoadingButton] = useState(false);
  const [size, setSize] = useState({
    width: 0,
    height: 0,
  });
  const longPage = useRef(null);
  useEffect(() => {
    if (longPage.current) {
      const { offsetHeight, offsetWidth } = longPage.current;
      setSize({
        width: offsetWidth,
        height: offsetHeight,
      });
    }
  }, [longPage]);
  const {
    id,
    sale_name,
    datetime,
    ten_khach_hang,
    phone,
    nganh_nghe,
    address,
    thuong_hieu,
    nam_san_xuat,
    loai_xe,
    loai_thung,
    tong_gia_tri_hop_dong,
    quantity,
    don_gia_va_thanh_tien,
    gia_khuyen_mai,
    name, // màu sắc
  } = props.location.state.data;
  /**
   * Render Pdf file
   * @returns {Promise<Blob>}
   */
  const renderFile = async () => {
    let pages = document.getElementsByClassName("page");
    let i = 0;
    var pdf = new jsPDF("p", "mm", [size.width, size.height]);
    while (i < pages.length) {
      await html2canvas(pages[i], {
        width: size.width,
        height: size.height,
        scale: 4,
      }).then((canvas) => {
        let imgData = canvas.toDataURL("image/jpeg");
        pdf.addImage(
          imgData,
          "PNG",
          0,
          0,
          pdf.internal.pageSize.getWidth(),
          pdf.internal.pageSize.getHeight()
        );
        pdf.addPage();
      });
      i++;
    }
    pdf.deletePage(4);
    return pdf.output("blob");
  };
  return (
    <>
      <div
        className="no_print"
        style={{ width: "100%", padding: "18px 0px 0px 0px", marginBottom: 50 }}
      >
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Biểu mẫu hợp đồng
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div
                className="flex justify-end"
                style={{ position: "fixed", right: 38 }}
              >
                <IButton
                  color={colors.main}
                  title="In"
                  icon={ISvg.NAME.PRINT}
                  styleHeight={{ width: 140 }}
                  onClick={() => {
                    window.print();
                  }}
                />
                <IButton
                  color={colors.main}
                  title="Lưu"
                  style={{ marginLeft: 10 }}
                  icon={ISvg.NAME.UPLOAD}
                  styleHeight={{ width: 140 }}
                  loading={loadingButton}
                  onClick={async () => {
                    setLoadingButton(true);
                    try {
                      let data = await renderFile();
                      const pdf = new File([data], "file_PDF.pdf", {
                        type: "application/pdf",
                      });
                      const response = await APIService._uploadFile(
                        "ORDER",
                        pdf,
                        "file_PDF"
                      );

                      if (response.file.length !== 0) {
                        const obj = {
                          order_code_child: id,
                          pdf_name_order: response.file[0],
                        };
                        const responseFileName =
                          await APIService._postFileNameOrder(obj);
                        message.success("Upload thành công");
                      } else {
                        message.error("Upload thất bại");
                      }
                      setLoadingButton(false);
                    } catch (e) {
                      setLoadingButton(false);
                      console.log(e);
                    }
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </div>

      <div className="form">
        <div className="page ">
          <div className="subpage subpage_1">
            <div>
              <div className="flex_center header_content">
                <p className="ft0">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</p>
                <p className="happy">Độc lập – Tự do – Hạnh phúc</p>
                <p className="ft2">
                  <nobr>----o0o----</nobr>
                </p>
              </div>
              <div className="flex_center title_form">
                <p className="text_title_form">HỢP ĐỒNG KINH TẾ</p>
              </div>
              <div className="flex_center">
                <table className="table_info">
                  <tr>
                    <th>Số hợp đồng</th>
                    <th>Nhân viên bán hàng</th>
                  </tr>
                  <tr>
                    <td>
                      <nobr>.../.../20.../HĐ......./HDNGP-.......</nobr>
                    </td>
                    <td>
                      {sale_name ||
                        "............................/.........................."}
                    </td>
                  </tr>
                </table>
              </div>
              <div className="law_info">
                <p className="p7 ft7">
                  <span className="ft2">-</span>
                  <span className="ft6">
                    Căn cứ Bộ luật Dân sự số 33/2005/QH11 ngày 14/06/2005 và các
                    văn bản pháp luật liên quan;
                  </span>
                </p>
                <p className="p8 ft7">
                  <span className="ft2">-</span>
                  <span className="ft6">
                    Căn cứ Luật Thương mại số 36/2005/QH11 ngày 14/06/2005 và
                    các văn bản pháp luật liên quan;
                  </span>
                </p>
                <p className="p8 ft7">
                  <span className="ft2">-</span>
                  <span className="ft6">
                    Căn cứ vào nhu cầu và khả năng của các bên.
                  </span>
                </p>
                <div style={{ marginTop: 12 }}>
                  <p className="p9 ft2">
                    Hôm nay, ngày{" "}
                    {datetime ? moment(datetime).format("DD") : "...."} tháng{" "}
                    {datetime ? moment(datetime).format("MM") : "...."} năm{" "}
                    {datetime ? moment(datetime).format("YYYY") : "...."}, chúng
                    tôi gồm có:
                  </p>
                </div>
              </div>
              <div>
                <p className="p9 ft0">
                  BÊN BÁN (“BÊN A”): CÔNG TY CỔ PHẦN HYUNDAI NGUYÊN GIA PHÁT
                </p>
                <div className="info_A">
                  <p className="p1 ft8">Địa chỉ:</p>
                  <p className="address ft8">
                    39 Đại lộ Bình Dương, P. Vĩnh Phú, Thành phố Thuận An, Bình
                    Dương
                  </p>
                </div>
                <div className="info_A">
                  <p className="p1 ft8">Điện thoại:</p>
                  <p className="phone ft8">028 6295 0081</p>
                  <p className="p11 ft8">Fax:</p>
                  <p className="fax ft8">98098098</p>
                </div>
                <div className="info_A">
                  <p className="p1 ft8">Mã số thuế:</p>
                  <p className="tax ft8">3702748105</p>
                </div>
                <div style={{ marginLeft: 50 }}>
                  <p className="p12 ft8">Tài khoản VNĐ:</p>
                  <p className="p13 ft2">
                    <span className="ft9"></span>
                    <span className="ft10">
                      2751168793939 tại Ngân hàng MBbank – Chi nhánh Hóc Môn
                      (TPHCM)
                    </span>
                  </p>
                  <p className="p13 ft2">
                    <span className="ft9"></span>
                    <span className="ft10">
                      185882772 tại Ngân hàng Vpbank CN Thủ Đức (TPHCM)
                    </span>
                  </p>
                  <p className="p13 ft2">
                    <span className="ft9"></span>
                    <span className="ft10">
                      117002685990 tại Ngân hàng Vietinbank CN1 (TP. HCM)
                    </span>
                  </p>
                  <p className="p13 ft2">
                    <span className="ft9"></span>
                    <span className="ft10">
                      65310000491986 tại Ngân hàng BIDV CN Thủ Dầu Một (Bình
                      Dương)
                    </span>
                  </p>
                </div>
                <div className="flex info_A">
                  <p className="ft8">Đại diện bởi: Ông</p>
                  <p className="name_represent p1">NGUYỄN MINH THI</p>
                  <p className="p11 ft8">Chức vụ: Tổng Giám đốc.</p>
                </div>
              </div>
              :288
              <div style={{ marginTop: 10 }}>
                <div style={{ display: "flex" }}>
                  <p className="p15 ft0">BÊN MUA (“BÊN B”):</p>
                  <p
                    className="ft0"
                    style={{
                      textTransform: "uppercase",
                      wordBreak: "break-word",
                      wordWrap: "break-word",
                      paddingLeft: 4,
                    }}
                  >
                    {ten_khach_hang ||
                      "..........................................................................................................................."}
                  </p>
                </div>
                <div className="info_A">
                  <p className="p1 ft8">Địa chỉ:</p>
                  <p className="address ft8">
                    {renderAddress(address) === "-"
                      ? "......................................................................................................................."
                      : renderAddress(address)}
                  </p>
                </div>
                <div className="info_A">
                  <p className="p1 ft8">Điện thoại: </p>
                  <p className="phone ft8">
                    {phone ||
                      ".................................................."}
                  </p>
                  <p className="p11 ft8 dot_fax">Fax:</p>
                </div>
                <div className="info_A">
                  <p className="p1 ft8 dot_tax">Mã số thuế:</p>
                </div>
                <div className="flex info_A">
                  <p className="ft8 dot_represent">Đại diện bởi:</p>
                  <p className="p11_1 ft8">
                    Chức vụ:{" "}
                    {nganh_nghe ||
                      ".................................................."}
                  </p>
                </div>
              </div>
              <div style={{ margin: "10px 0px" }}>
                <p className="p20 ft2" style={{ textAlign: "justify" }}>
                  Trên cơ sở thỏa thuận, hai bên thống nhất ký kết hợp đồng mua
                  bán xe với các điều khoản sau đây:
                </p>
              </div>
              <div style={{ margin: "10px 0px" }}>
                <p className="p9 ft0">
                  ĐIỀU 1. TÊN HÀNG - SỐ LƯỢNG - CHẤT LƯỢNG - GIÁ TRỊ HỢP ĐỒNG
                </p>
              </div>
              <div className="flex_center">
                <table className="table_product">
                  <tr>
                    <th style={{ width: 50 }}>TT</th>
                    <th>Hàng hóa</th>
                    <th>
                      Đơn giá
                      <br />
                      (VNĐ)
                    </th>
                    <th>
                      Số lượng
                      <br />
                      (xe)
                    </th>
                    <th>
                      Thành tiền
                      <br />
                      (VNĐ)
                    </th>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td style={{ textAlign: "left", maxWidth: 170 }}>
                      <p>Xe ô tô tải</p>
                      <div>
                        <p className="p29 ft12">
                          - Nhãn hiệu:{" "}
                          {thuong_hieu || "........................."}
                        </p>
                      </div>
                      <div>
                        <p className="p32 ft12">
                          - Số loại: {loai_xe || "........................."}
                        </p>
                      </div>
                      <div>
                        <p className="p1 ft12">
                          - Năm sản xuất: {nam_san_xuat || "................"},
                          mới 100%
                        </p>
                      </div>
                      <div>
                        <p className="p32 ft12">
                          - Màu: {name || "............................."}
                        </p>
                      </div>
                      <div>
                        <p className="p32 ft12">
                          - Hồ sơ thùng: {loai_thung || "................."}
                        </p>
                      </div>
                    </td>
                    <td>
                      {don_gia_va_thanh_tien
                        ? priceFormat(don_gia_va_thanh_tien)
                        : "........................."}
                    </td>
                    <td>
                      {quantity
                        ? priceFormat(quantity)
                        : "........................."}
                    </td>
                    <td>
                      {don_gia_va_thanh_tien
                        ? priceFormat(don_gia_va_thanh_tien)
                        : "........................."}
                    </td>
                  </tr>
                  <tr>
                    <td colSpan={5} style={{ textAlign: "left" }}>
                      <p style={{ fontWeight: "bold" }}>
                        Tổng giá trị hợp đồng:
                        <span
                          style={{
                            fontWeight: tong_gia_tri_hop_dong ? 600 : 500,
                            marginLeft: 4,
                          }}
                        >
                          {tong_gia_tri_hop_dong
                            ? priceFormat(tong_gia_tri_hop_dong)
                            : "..............................................................................................................................................."}
                        </span>
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td colSpan={5} style={{ textAlign: "left" }}>
                      <p style={{ fontWeight: "bold" }}>
                        Bằng chữ:
                        <span
                          style={{
                            fontWeight: tong_gia_tri_hop_dong ? 600 : 500,
                            marginLeft: 4,
                          }}
                        >
                          {tong_gia_tri_hop_dong
                            ? numberToText(tong_gia_tri_hop_dong, "đồng")
                            : "...................................................................................................................................................................../."}
                        </span>
                      </p>
                    </td>
                  </tr>
                </table>
              </div>
              <div style={{ marginTop: 10, marginLeft: 20 }}>
                <p className="ft7">
                  <span className="ft7">-</span>
                  <span className="ft22">
                    Khuyến mãi:{" "}
                    {gia_khuyen_mai
                      ? priceFormat(gia_khuyen_mai)
                      : "......................................................."}
                  </span>
                </p>
                <p className="ft7">
                  <span className="ft7">-</span>
                  <span className="ft22">
                    Phụ tùng kèm theo: Theo tiêu chuẩn của nhà sản xuất.
                  </span>
                </p>
                <p className="ft2">
                  <span className="ft2">-</span>
                  <span className="ft23">
                    Giá nêu trên bao gồm: thuế giá trị gia tăng (VAT) 10%, nhưng
                    chưa bao gồm lệ phí trước bạ, chi phí đăng ký, lưu hành, bảo
                    hiểm xe và các chi phí khác.
                  </span>
                </p>
              </div>
            </div>
            <div>
              <div className="border_footer"></div>
              <div className="text_footer">
                <p className="p48 ft24">
                  Chuyên bán Xe tải – Dịch vụ sửa chữa - bảo dưỡng - đóng thùng
                </p>
              </div>
            </div>
          </div>
        </div>
        <div ref={longPage} className="page">
          <div className="subpage subpage_2">
            <div>
              <p className="p49 ft0">ĐIỀU 2. THANH TOÁN</p>
              <p className="p8 ft2">
                Bên B thanh toán cho Bên A bằng tiền mặt hoặc chuyển khoản bằng
                tiền đồng Việt Nam, cụ thể như sau:
              </p>
              <div className="rule">
                <p className="content_law">
                  <span className="ft0">-</span>
                  <span className="ft25">Lần 1: </span>Bên B đặt cọc cho bên A
                  10% giá trị hợp đồng ngay sau khi ký hợp đồng . Số tiền này
                  được xem là tiền đã đặt cọc nhằm đảm bảo việc thực hiện hợp
                  đồng. Nếu Bên B có nhu cầu mua bằng hình thức vay vốn ngân
                  hàng thì Bên A tiến hành liên hệ ngân hàng làm hồ sơ cho Bên B
                  vay mua xe.
                </p>
                <p className="content_law">
                  Trong trường hợp Ngân Hàng đồng ý tài trợ, trong vòng 5 ngày
                  Bên B không thanh toán nhận xe như các quy định trong Hợp đồng
                  thì Bên A tùy theo quyết định của mình có thể đơn phương chấm
                  dứt hợp đồng này và khi đó Bên B sẽ mất khoản tiền cọc nói
                  trên.
                </p>
                <p className="content_law">
                  Trong trường hợp Ngân Hàng không đồng ý tài trợ, Bên A sẽ trả
                  lại 100% tiền đặt cọc cho Bên B.
                </p>
                <p className="content_law">
                  <span className="ft0">-</span>
                  <span className="ft27">Lần 2: </span>Bên B thanh toán số tiền
                  tương đương 70% giá trị của hợp đồng để Bên A tiến hành phân
                  xe và đóng thùng cho Bên B.
                </p>
                <p className="content_law">
                  <span className="ft28">
                    Đối với khách mua bằng hình thức vay vốn ngân hàng:{" "}
                  </span>
                  Bên B thanh toán số tiền tương đương 20% giá trị của hợp đồng
                  ngay khi ngân hàng có thông báo cho vay để Bên A tiến hành
                  phân xe và đóng thùng cho Bên B.
                </p>
                <p className="content_law">
                  <span className="ft0">-</span>
                  <span className="ft27">Lần 3: </span>Bên B hoặc Ngân Hàng tài
                  trợ cho Bên B phải thanh toán đủ 100% giá trị hợp đồng cho Bên
                  A trước khi nhận xe và các giấy tờ xe.
                </p>
                <p className="content_law">
                  Trong thời gian Bên B chưa xác lập được quyền sở hữu toàn vẹn
                  (chưa hoàn tất nghĩa vụ thanh toán) thì xe vẫn thuộc quyền sở
                  hữu của Bên A. Trước khi thanh toán tiền lần 2 của hợp đồng
                  này mà giá xe có sự thay đổi thì Bên B phải thanh toán khoản
                  chênh lệch đó mà không có bất kì sự từ chối hay chậm trễ nào.
                </p>
              </div>
              <div className="space_rule">
                <p className="p57 ft0">
                  ĐIỀU 3: THỜI GIAN, ĐỊA ĐIỂM VÀ PHƯƠNG THỨC GIAO HÀNG
                </p>
              </div>
              <div className="rule">
                <p className="content_law">
                  <span className="ft2">1.</span>
                  <span className="ft23">
                    Việc giao xe cho Bên B sẽ được thực hiện trong vòng
                    ..................... ngày kể từ ngày thanh toán lần 2.
                  </span>
                </p>
                <p className="content_law">
                  <span className="ft8">2.</span>
                  <span className="ft30">
                    Địa điểm giao nhận xe: Tại địa chỉ Bên Bán
                  </span>
                </p>
                <p className="content_law">
                  3. Quyền sở hữu và rủi ro đối với hàng hóa được coi là đã
                  chuyển từ Bên A sang Bên B kể từ thời điểm xe được giao cho
                  Bên B.
                </p>
              </div>
              <div className="space_rule">
                <p className="p9 ft0">ĐIỀU 4: BẢO HÀNH</p>
              </div>
              <div className="rule">
                <p className="content_law">
                  <span className="ft2">1.</span>
                  <span className="ft23">
                    Tất cả các xe do Bên A bán ra được bảo hành theo chính sách
                    bảo hành của Nhà sản xuất được nêu trong sổ bảo hành.
                  </span>
                </p>
                <p className="content_law">
                  <span className="ft2">2.</span>
                  <span className="ft23">
                    Trường hợp nếu phát sinh hư hỏng do lỗi thiết kế của nhà sản
                    xuất thì Bên A và Nhà sản xuất phải phối hợp sửa chữa và lắp
                    đặt phụ tùng thay thế miễn phí cho bên B.
                  </span>
                </p>
              </div>
              <div className="space_rule">
                <p className="p7 ft0">ĐIỀU 5: TRÁCH NHIỆM CỦA CÁC BÊN</p>
              </div>
              <div className="rule">
                <p className="content_law">
                  <span className="ft2">1.</span>
                  <span className="ft31">
                    Bên A có nghĩa vụ giao xe đúng chủng loại, thông số kỹ
                    thuật, toàn bộ phụ tùng xe theo tiêu chuẩn của Nhà sản xuất
                    và hồ sơ xe đầy đủ pháp lý cho bên B đăng ký lưu hành.
                  </span>
                </p>
                <p className="content_law">
                  <span className="ft2">2.</span>
                  <span className="ft23">
                    Bên B thực hiện thanh toán cho Bên A theo đúng Điều 2 của
                    Hợp đồng.
                  </span>
                </p>
              </div>
              <div className="space_rule">
                <p className="p7 ft0">
                  ĐIỀU 6: BẤT KHẢ KHÁNG VÀ GIẢI QUYẾT TRANH CHẤP
                </p>
              </div>
              <div className="rule">
                <p className="content_law">
                  <span className="ft2">1.</span>
                  <span className="ft23">
                    Bất khả kháng có nghĩa là các sự kiện xảy ra một cách khách
                    quan, không thể lường trước được và không thể khắc phục được
                    mặc dù đã áp dụng mọi biện pháp cần thiết trong khả năng cho
                    phép, một trong các Bên vẫn không có khả năng thực hiện được
                    nghĩa vụ của mình theo Hợp đồng này, bao gồm nhưng không
                    giới hạn ở: thiên tai, hỏa hoạn, lũ lụt, chiến tranh, can
                    thiệp của chính quyền bằng vũ trang, cản trở giao thông vận
                    tải và các sư kiện khác tương tự.
                  </span>
                </p>
                <p className="content_law">
                  <span className="ft2">2.</span>
                  <span className="ft32">
                    Khi xảy ra sự kiện bất khả kháng, bên gặp phải bất khả kháng
                    phải không chậm chễ, thông báo cho bên kia tình trạng thực
                    tế, đề xuất phương án xử lý và nỗ lực giảm thiểu tổn thất,
                    thiệt hại đến mức thấp nhất có thể.
                  </span>
                </p>
              </div>
              <div className="space_rule">
                <p className="p7 ft0">ĐIỀU 7: ĐIỀU KHOẢN CHUNG</p>
              </div>
              <div className="rule">
                <p className="content_law">
                  1. Hai bên cam kết thực hiện đúng các điều khoản đã ghi trong
                  Hợp đồng này. Hai bên không được đơn phương hủy bỏ Hợp đồng.
                  Nếu một trong hai Bên tự ý hủy bỏ Hợp đồng thì coi như đã vi
                  phạm Hợp đồng và sẽ chịu trách nhiệm như đã thỏa thuận như
                  trên. Nếu có khó khăn trở ngại, phải báo trước 07 ngày để hai
                  Bên bàn bạc cùng nhau giải quyết. Trong trường hợp hai Bên có
                  tranh chấp không thể giải quyết được bằng đàm phán, hai Bên sẽ
                  thống nhất đưa ra tòa án kinh tế tại Bình Dương xử lý theo
                  pháp luật hiện hành. Án phí do Bên thua gánh chịu.
                </p>
                <p className="content_law">
                  2. Hợp đồng này có hiệu lực kể từ ngày ký và chỉ có giá trị
                  trong trường hợp Bên B đặt cọc ngay khi ký hợp đồng. Hợp đồng
                  này được coi là đã thanh lý khi Bên A đã nhận đủ tiền và Bên B
                  đã nhận xe.
                </p>
                <p className="content_law">
                  3. Hợp đồng có thời hạn 30 ngày kể từ ngày Bên B đặt cọc.
                </p>
              </div>
            </div>
            <div>
              <div className="border_footer"></div>
              <div className="text_footer">
                <p className="p48 ft24">
                  Chuyên bán Xe tải – Dịch vụ sửa chữa - bảo dưỡng - đóng thùng
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="page">
          <div className="subpage subpage_3">
            <div>
              <div className="rule">
                <p className="content_law">
                  4. Hợp đồng này được lập thành{" "}
                  <span className="ft7">02 (hai) </span>bản. Mỗi bên giữ{" "}
                  <span className="ft7">01 (một) </span>bản, có giá trị pháp lý
                  như nhau.
                </p>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  margin: "30px 60px 0px 60px",
                }}
              >
                <div>
                  <p className="p1 ft0">ĐẠI DIỆN BÊN A</p>
                </div>
                <div>
                  <p className="p1 ft0">ĐẠI DIỆN BÊN B</p>
                </div>
              </div>
            </div>
            <div>
              <div className="border_footer"></div>
              <div className="text_footer">
                <p className="p48 ft24">
                  Chuyên bán Xe tải – Dịch vụ sửa chữa - bảo dưỡng - đóng thùng
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default FormContract;
