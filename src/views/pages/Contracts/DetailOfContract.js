import React, {useState, useEffect} from "react";
import {
  Row,
  Col,
  Skeleton,
  Modal,
  Empty,
  Checkbox,
  Table,
  message,
} from "antd";
import {ISvg, IButton, ITitle} from "../../components";
import {colors} from "../../../assets";
import {StyledITitle} from "../../components/common/Font/font";
import {APIService} from "../../../services";
import {useHistory, useParams, Link} from "react-router-dom";
import "../input.css";
import styled from "styled-components";
import {IInputTextArea, Fancybox} from "../../components/common";
import FormatterDay from "../../../utils/FormatterDay";
import {renderAddress} from "../../../utils";

const StyledTable = styled(Table)`
  padding: 0;
  .ant-table-thead {
    background: #e4efff !important;
  }
  .ant-table-header {
    background-color: #e4efff !important;
    /* overflow-y: hidden !important; */
  }import { parse } from 'html-react-parser';

`;
const IModal = styled(Modal)`
  .ant-btn-primary {
    display: none !important;
  }
`;
const ruining = 6;
const deliveried = 9;
export default function DetailOfContract(props) {
  const history = useHistory();
  const {id} = useParams();
  const [isLoadingCallAPIDetail, setIsLoadingAPIDetail] = useState(false);
  const [loadingRemove, setLoadingRemove] = useState(false);
  const [isApprove, setApprove] = useState(false);
  const [isShowCancelContract, setIsShowCancelContract] = useState(false);
  const [showButton, setShowButton] = useState(false);
  const [reasonCancel, setReasonCancel] = useState("");
  const [dataDetail, setDataDetail] = useState({
    contracts: [],
    tien_do_hop_dong: [],
    ho_so_giai_ngan: [],
    url: "",
  });

  const getDetailContract = async (id) => {
    try {
      setIsLoadingAPIDetail(true);
      const data = await APIService._getDetailContract(id);
      const {detail, link_image} = data;
      setDataDetail({
        ...detail,
        contracts: detail.giay_to,
        tien_do_hop_dong: detail.tien_do_hop_dong,
        ho_so_giai_ngan: detail.ho_so_giai_ngan,
        url: link_image,
      });
      setIsLoadingAPIDetail(false);
      setShowButton(true);
    } catch (error) {
      console.log({error});
      setIsLoadingAPIDetail(false);
    }
  };
  const postCancelContract = async (obj) => {
    try {
      setLoadingRemove(true);
      const data = await APIService._postCancelContract(obj);
      setLoadingRemove(false);
      setIsShowCancelContract(false);
      message.success("Hủy hợp đồng thành công");
      history.push("/contract/list");
    } catch (error) {
      console.log({error});
      setLoadingRemove(false);
    }
  };
  useEffect(() => {
    getDetailContract(id);
  }, [id]);
  const updateCloseContract = async (id) => {
    try {
      setLoadingRemove(true);
      const res = await APIService._postUpdateStatusOrderRequest({
        id,
        type: 5,
        value: "",
      });
      setLoadingRemove(false);
      setApprove(true);
    } catch (e) {
      setLoadingRemove(false);
      console.log(e);
    }
  };
  const renderTitle = (title) => {
    return (
      <span
        style={{
          fontWeight: 500,
          fontSize: 14,
        }}
      >
        {title}
      </span>
    );
  };

  const columns_hs = [
    {
      title: "Hồ sơ giải ngân",
      align: "left",
      key: "name",
      dataIndex: "name",
      render: (name) => <span>{name || "-"}</span>,
    },
    {
      title: renderTitle("Ngày dự kiến"),
      key: "expected_date",
      dataIndex: "expected_date",
      align: "center",
      render: (expected_date) => (
        <span>
          {!expected_date || expected_date < 0
            ? "-"
            : FormatterDay.dateFormatWithString(
              expected_date,
              "#DD#/#MM#/#YYYY#"
            )}
        </span>
      ),
    },
  ];

  const columns_td = [
    {
      title: "",
      align: "center",
      width: 40,
      render: (obj) => {
        return (
          <Checkbox
            disabled
            checked={obj.status === 1 ? true : false}
          ></Checkbox>
        );
      },
    },
    {
      title: renderTitle("Hồ sơ giấy tờ xe"),
      key: "name",
      dataIndex: "name",
      render: (name) => <span>{name || "-"}</span>,
    },
  ];
  const columns = [
    {
      title: "",
      align: "center",
      width: 40,
      render: (obj) => {
        return (
          <Checkbox
            disabled
            checked={obj.status === 1 ? true : false}
          ></Checkbox>
        );
      },
    },
    {
      title: renderTitle("Hồ sơ, giấy tờ xe"),
      key: "name",
      dataIndex: "name",
      render: (name) => <span>{name || "-"}</span>,
    },
    {
      title: renderTitle("Ngày dự kiến"),
      key: "expected_date",
      dataIndex: "expected_date",
      align: "center",
      render: (expected_date) => (
        <span>
          {!expected_date || expected_date < 0
            ? "-"
            : FormatterDay.dateFormatWithString(
              expected_date,
              "#DD#/#MM#/#YYYY#"
            )}
        </span>
      ),
    },
  ];

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row gutter={[0, 30]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{color: colors.main, fontSize: 22}}>
              Thông tin chi tiết hợp đồng
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div className="flex justify-end">
            {showButton && (
              <>
                <IButton
                  color={colors.main}
                  title="Lịch giao xe"
                  icon={ISvg.NAME.SVGContract}
                  styleHeight={{
                    width: "100%",
                  }}
                  onClick={() => {
                    history.push({
                      pathname: `/contract/tradingSchedule/detail/${id}`,
                      state: dataDetail.contract_status_id,
                    });
                  }}
                  style={{marginRight: 12}}
                />
                <Link
                  to={{
                    pathname: `/contract/form/${id}`,
                    state: {data: dataDetail},
                  }}
                >
                  <IButton
                    color={colors.main}
                    title="Biểu mẫu"
                    icon={ISvg.NAME.SVGContractWrite}
                    styleHeight={{width: "100%"}}
                  />
                </Link>
                {ruining === dataDetail.contract_status_id && (
                  <>
                    <IButton
                      color={colors.oranges}
                      title="Hủy hợp đồng"
                      loading={loadingRemove}
                      icon={ISvg.NAME.CROSS}
                      style={{marginLeft: 12}}
                      styleHeight={{width: "100%"}}
                      onClick={() => {
                        setReasonCancel("");
                        setIsShowCancelContract(true);
                      }}
                    />
                    <IButton
                      color={colors.main}
                      title="Chỉnh sửa"
                      icon={ISvg.NAME.WRITE}
                      style={{marginLeft: 12}}
                      styleHeight={{width: "100%"}}
                      onClick={() => {
                        history.push(`/contract/edit/${id}`);
                      }}
                    />
                  </>
                )}
                {deliveried === dataDetail.contract_status_id && (
                  <>
                    <IButton
                      color={colors.main}
                      loading={loadingRemove}
                      title="Đóng hợp đồng"
                      icon={ISvg.NAME.CHECKMARK}
                      style={{marginLeft: 12}}
                      styleHeight={{width: "100%"}}
                      onClick={() => {
                        updateCloseContract(Number(id));
                      }}
                    />
                  </>
                )}
              </>
            )}
          </div>
        </Col>

        <Col span={24}>
          <Row gutter={[6, 6]} type="flex">
            <Col span={8}>
              <div
                style={{
                  padding: 20,
                  backgroundColor: "white",
                  height: "100%",
                }}
              >
                <Skeleton
                  paragraph={{rows: 14}}
                  active
                  loading={isLoadingCallAPIDetail}
                >
                  <Row gutter={[0, 20]}>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        Thông tin khách hàng
                      </StyledITitle>
                    </Col>
                    <Col span={12}>
                      <p style={{fontWeight: 600}}>
                        {dataDetail.user_type_id === 1
                          ? "Tên doanh nghiệp"
                          : "Tên khách hàng"}
                      </p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.ten_khach_hang || "-"}
                      </span>
                    </Col>
                    <Col span={12}>
                      <p style={{fontWeight: 600}}>Mã khách hàng</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.user_code || "-"}
                      </span>
                    </Col>
                    {dataDetail.user_type_id === 1 ? (
                      <Col span={24}>
                        <p style={{fontWeight: 600}}>Mã số thuế</p>
                        <span
                          style={{
                            wordWrap: "break-word",
                            wordBreak: "break-all",
                          }}
                        >
                          {dataDetail.tax_code || "-"}
                        </span>
                      </Col>
                    ) : (
                      <Col span={24}>
                        <Row gutter={[12, 12]}>
                          <Col span={8}>
                            <p style={{fontWeight: 600}}>CMND</p>
                            <span
                              style={{
                                wordWrap: "break-word",
                                wordBreak: "break-all",
                              }}
                            >
                              {dataDetail.cmnd || "-"}
                            </span>
                          </Col>
                          <Col span={8}>
                            <p style={{fontWeight: 600}}>Ngày cấp</p>
                            <span
                              style={{
                                wordWrap: "break-word",
                                wordBreak: "break-all",
                              }}
                            >
                              {!dataDetail.date_cmnd ||
                              /1970/g.test(dataDetail.date_cmnd)
                                ? "-"
                                : FormatterDay.dateFormatWithString(
                                  dataDetail.date_cmnd,
                                  "#DD#/#MM#/#YYYY#"
                                )}
                            </span>
                          </Col>
                          <Col span={8}>
                            <p style={{fontWeight: 600}}>Nơi cấp</p>
                            <span
                              style={{
                                wordWrap: "break-word",
                                wordBreak: "break-all",
                              }}
                            >
                              {dataDetail.issued_cmnd || "-"}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                    )}
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Địa chỉ</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {renderAddress(dataDetail.address)}
                      </span>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[12, 12]}>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Điện thoại</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.phone || "-"}
                          </span>
                        </Col>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>
                            Ngành nghề kinh doanh
                          </p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.nganh_nghe || "-"}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Trạng thái</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.contract_status_id === 6
                          ? "Đang chạy"
                          : dataDetail.contract_status_id === 9
                            ? "Đã giao"
                            : "Đã đóng"}
                      </span>
                    </Col>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        Thông tin xe
                      </StyledITitle>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Tên sản phẩm</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.product_name || "-"}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Loại xe</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.nganh_hang || "-"}
                      </span>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[12, 12]}>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Dòng xe</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.dong_xe || "-"}
                          </span>
                        </Col>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Thương hiệu</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.loai_xe || "-"}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[12, 12]}>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Thương hiệu</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.thuong_hieu || "-"}
                          </span>
                        </Col>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Loại thùng</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.loai_thung || "-"}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[12, 12]}>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Số khung</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.frame_seri ? dataDetail.frame_seri.frame_number ? dataDetail.frame_seri.frame_number : '-' : "-"}
                          </span>
                        </Col>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Số seri</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                         {dataDetail.frame_seri ? dataDetail.frame_seri.seri_number ? dataDetail.frame_seri.seri_number : '-' : "-"}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Skeleton>
              </div>
            </Col>

            {!dataDetail.images ||
            !dataDetail.images.length ||
            dataDetail.images[0] === "" ? null : (
              <Col span={3}>
                <div
                  style={{
                    padding: 20,
                    backgroundColor: "white",
                    height: "100%",
                  }}
                >
                  <Skeleton
                    paragraph={{rows: 14}}
                    active
                    loading={isLoadingCallAPIDetail}
                  >
                    <Row gutter={[0, 20]}>
                      <Col span={24} style={{paddingTop: 5}}>
                        <StyledITitle style={{color: "black", fontSize: 16}}>
                          Hình ảnh
                        </StyledITitle>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            maxHeight: 525,
                            overflowY: "auto",
                            overflowX: "hidden",
                          }}
                        >
                          <Fancybox>
                            <p>
                              {dataDetail.images.map((image, index) => {
                                return (
                                  <a
                                    key={index}
                                    data-fancybox="gallery"
                                    href={dataDetail.url + image}
                                  >
                                    <img
                                      src={dataDetail.url + image}
                                      alt="ảnh hợp đồng"
                                      width="100%"
                                      style={{
                                        marginBottom: 10,
                                      }}
                                    />
                                  </a>
                                );
                              })}
                            </p>
                          </Fancybox>
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            )}
            <Col span={10}>
              <div
                style={{
                  padding: 20,
                  backgroundColor: "white",
                  height: "100%",
                }}
              >
                <Skeleton
                  paragraph={{rows: 14}}
                  active
                  loading={isLoadingCallAPIDetail}
                >
                  <Row gutter={[0, 20]}>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        Tiến độ hợp đồng
                      </StyledITitle>
                    </Col>
                    <Col span={24}>
                      <StyledTable
                        dataSource={dataDetail.tien_do_hop_dong}
                        columns={columns_td}
                        bordered
                        size="small"
                        pagination={false}
                        style={{
                          width: "100%",
                          border: "1px solid #e8e8e8",
                          borderRadius: 0,
                        }}
                        scroll={{x: 0, y: 495}}
                        locale={{
                          emptyText: <Empty description="Không có thông tin"/>,
                        }}
                      />
                    </Col>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        1.Hồ sơ giao xe
                      </StyledITitle>
                    </Col>
                    <Col span={24}>
                      <StyledTable
                        dataSource={dataDetail.contracts}
                        columns={columns}
                        bordered
                        size="small"
                        pagination={false}
                        style={{
                          width: "100%",
                          border: "1px solid #e8e8e8",
                          borderRadius: 0,
                        }}
                        scroll={{x: 0, y: 495}}
                        locale={{
                          emptyText: <Empty description="Không có thông tin"/>,
                        }}
                      />
                    </Col>
                    {dataDetail.payment_type_id === 2 && (
                      <>
                        <Col span={24} style={{paddingTop: 5}}>
                          <StyledITitle
                            style={{color: "black", fontSize: 16}}
                          >
                            2.Hồ sơ giải ngân (đối với khách hàng vay ngân hàng)
                          </StyledITitle>
                        </Col>
                        <Col span={24}>
                          <StyledTable
                            dataSource={dataDetail.ho_so_giai_ngan}
                            columns={columns_hs}
                            bordered
                            size="small"
                            pagination={false}
                            style={{
                              width: "100%",
                              border: "1px solid #e8e8e8",
                              borderRadius: 0,
                            }}
                            scroll={{x: 0, y: 495}}
                            locale={{
                              emptyText: (
                                <Empty description="Không có thông tin"/>
                              ),
                            }}
                          />
                        </Col>
                      </>
                    )}
                  </Row>
                </Skeleton>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>

      <Modal
        visible={isShowCancelContract}
        width={360}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{padding: 20}}
        centered
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24} style={{padding: 0}}>
              <div style={{textAlign: "center"}}>
                <ITitle
                  style={{
                    color: colors.main,
                    fontSize: 16,
                    fontWeight: "bold",
                  }}
                  title="Lý do hủy"
                />
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: "100%",
                  marginTop: 5,
                  textAlign: "center",
                  fontWeight: 600,
                  color: "#000",
                }}
              >
                <IInputTextArea
                  style={{minHeight: 100, maxHeight: 300}}
                  placeholder="nhập lý do từ chối..."
                  value={reasonCancel}
                  onChange={(e) => {
                    setReasonCancel(e.target.value);
                  }}
                />
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -50,
              right: -1,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "100%",
            }}
          >
            <div style={{width: 140, marginRight: 15}}>
              <IButton
                color={colors.oranges}
                title="Hủy bỏ"
                onClick={() => {
                  setIsShowCancelContract(false);
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>
            <div style={{width: 140}}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                icon={ISvg.NAME.CHECKMARK}
                loading={loadingRemove}
                onClick={() => {
                  if (reasonCancel === "") {
                    message.warning("Vui lòng nhập lý do hủy");
                    return;
                  }
                  const obj = {
                    id: Number(id),
                    note: reasonCancel,
                  };
                  postCancelContract(obj);
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
      <IModal
        visible={isApprove}
        footer={null}
        width={450}
        centered={true}
        closable={false}
        onCancel={async () => {
          setApprove(false);
          getDetailContract(id);
        }}
        style={{paddingTop: 0}}
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{textAlign: "center"}}>
                <StyledITitle style={{color: colors.main, fontSize: 17}}>
                  Duyệt hợp đồng
                </StyledITitle>
              </div>
            </Col>

            <Col span={24}>
              <div style={{textAlign: "center"}}>
                <span style={{fontWeight: 500}}>
                  Đóng hợp đồng thành công.
                </span>
              </div>
            </Col>
          </Row>
        </div>
      </IModal>
    </div>
  );
}
