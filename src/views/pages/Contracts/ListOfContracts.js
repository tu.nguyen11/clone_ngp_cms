import React, {useState, useEffect} from "react";
import {Col, Row, Tooltip} from "antd";
import {colors, images} from "../../../assets";
import {
  IDatePicker,
  ISearch,
  ISelect,
  ISvg,
  ITitle,
  ITable,
} from "../../components";
import FormatterDay from "../../../utils/FormatterDay";

import {useHistory} from "react-router";
import {APIService} from "../../../services";
import {StyledITitle} from "../../components/common/Font/font";

const NewDate = new Date();

const StartDate = new Date();

let startValue = StartDate.setMonth(StartDate.getMonth() - 1);

export default function ListOfContracts() {
  const history = useHistory();

  const [loadingSalesMan, setLoadingSalesMan] = useState(false);
  const [loadingStatus, setLoadingStatus] = useState(false);
  const [isLoadingTable, setLoadingTable] = useState(false);

  const [listSalesMan, setListSalesMan] = useState([]);
  const [dataStatus, setDataStatus] = useState([]);
  const [listContracts, setListContracts] = useState({
    list_contracts: [],
    size: 10,
    total: 0,
  });

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      width: 60,
      align: "center",
      render: (stt) => <span> {stt} </span>,
    },
    {
      title: "Mã hợp đồng",
      dataIndex: "ma_hop_dong",
      key: "ma_hop_dong",
      render: (ma_hop_dong) => {
        return !ma_hop_dong ? (
          "-"
        ) : (
          <Tooltip title={ma_hop_dong}>
            <span>{ma_hop_dong}</span>
          </Tooltip>
        );
      },
    },
    {
      title: "Trạng thái",
      dataIndex: "trang_thai",
      key: "trang_thai",
      align: "center",
      render: (trang_thai) => <span>{trang_thai || "-"}</span>,
    },
    {
      title: "Mã khách hàng",
      dataIndex: "user_code",
      key: "user_code",
      render: (user_code) => {
        return !user_code ? (
          "-"
        ) : (
          <Tooltip title={user_code}>
            <span>{user_code}</span>
          </Tooltip>
        );
      },
    },
    {
      title: "Tên khách hàng",
      dataIndex: "ten_khach_hang",
      key: "ten_khach_hang",
      render: (ten_khach_hang) => {
        return !ten_khach_hang ? (
          "-"
        ) : (
          <Tooltip title={ten_khach_hang}>
            <span>{ten_khach_hang}</span>
          </Tooltip>
        );
      },
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      align: "center",
      render: (phone) => <span> {phone || "-"} </span>,
    },
    {
      title: "Mẫu xe quan tâm",
      dataIndex: "product_name",
      key: "product_name",
      render: (product_name) => <span>{product_name || "-"}</span>,
    },
    {
      title: "Nhân viên phụ trách",
      dataIndex: "ten_nhan_vien",
      key: "ten_nhan_vien",
      render: (ten_nhan_vien) => {
        return !ten_nhan_vien ? (
          "-"
        ) : (
          <Tooltip title={ten_nhan_vien}>
            <span>{ten_nhan_vien}</span>
          </Tooltip>
        );
      },
    },
    {
      title: "Lịch giao xe",
      dataIndex: "lich_giao_xe",
      key: "lich_giao_xe",
      render: (lich_giao_xe) => <span>{lich_giao_xe || "-"}</span>,
    },
  ];

  const [filterTable, setFilterTable] = useState({
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
    search: "",
    page: 1,
    id_staff: 0,
    id_status: 0,
  });

  const getListSalesMan = async () => {
    try {
      setLoadingSalesMan(true);
      const data = await APIService._getListSalesManDropList();
      const dataNew = data.data.map((item) => {
        return {key: item.id, value: item.name};
      });

      dataNew.unshift({key: 0, value: "Tất cả"});

      setListSalesMan(dataNew);
      setLoadingSalesMan(false);
    } catch (error) {
      console.log({error});
      setLoadingSalesMan(false);
    }
  };

  const getListStatusDropListContract = async () => {
    try {
      setLoadingStatus(true);
      const data = await APIService._getListStatusDropListContract();
      const dataNew = data.status.map((item) => {
        return {key: item.id, value: item.name};
      });

      dataNew.unshift({key: 0, value: "Tất cả"});

      setDataStatus(dataNew);
      setLoadingStatus(false);
    } catch (error) {
      console.log({error});
      setLoadingStatus(false);
    }
  };

  const getListContract = async (obj) => {
    try {
      setLoadingTable(true);
      const data = await APIService._getListContract(obj);
      const dataNew = data.contracts.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        return item;
      });

      setListContracts({
        list_contracts: dataNew,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log({error});
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    getListSalesMan();
    getListStatusDropListContract();
  }, []);

  useEffect(() => {
    getListContract(filterTable);
  }, [filterTable]);

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row>
        <Col span={12}>
          <div>
            <StyledITitle style={{color: colors.main, fontSize: 22}}>
              Danh sách hợp đồng
            </StyledITitle>
          </div>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã khách hàng, tên khách hàng">
              <ISearch
                placeholder="Tìm kiếm theo mã khách hàng, tên khách hàng"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    search: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{width: 16, height: 16}}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{margin: "24px 0px"}}>
        <Row>
          <Col span={24}>
            <Row>
              <Col span={21}>
                <div style={{display: "flex", alignItems: "center"}}>
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    width={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{marginLeft: 15, marginRight: 15}}
                  />
                  <IDatePicker
                    isBackground
                    from={filterTable.start_date}
                    to={filterTable.end_date}
                    onChange={(date) => {
                      if (date.length <= 1) {
                        return;
                      }

                      setLoadingTable(true);
                      setFilterTable({
                        ...filterTable,
                        start_date: date[0]._d.setHours(0, 0, 0),
                        end_date: date[1]._d.setHours(23, 59, 59),
                        page: 1,
                      });
                    }}
                  />
                  <ITitle
                    title="Lọc theo"
                    level={4}
                    style={{marginLeft: 15, marginRight: 15}}
                  />
                  <div style={{width: "200px"}}>
                    <ISelect
                      isTooltip={true}
                      defaultValue="Nhân viên phụ trách"
                      minWidth="100px"
                      data={listSalesMan}
                      select={loadingSalesMan ? false : true}
                      loading={loadingSalesMan}
                      onChange={(key) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          id_staff: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div
                    style={{
                      width: 150,
                      marginLeft: 15,
                    }}
                  >
                    <ISelect
                      defaultValue="Trạng thái"
                      minWidth="100px"
                      data={dataStatus}
                      select={loadingStatus ? false : true}
                      loading={loadingStatus}
                      onChange={(key) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          id_status: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <ITable
        columns={columns}
        data={listContracts.list_contracts}
        style={{width: "100%"}}
        defaultCurrent={1}
        sizeItem={listContracts.size}
        indexPage={filterTable.page}
        loading={isLoadingTable}
        maxpage={listContracts.total}
        scroll={{x: listContracts.list_contracts.length === 0 ? 0 : 1500}}
        onRow={(record) => {
          return {
            onClick: () => {
              history.push(`/contract/detail/${record.id}`);
            }, // click row
            onMouseDown: (event) => {
              if (event.button === 2 || event === 4) {
                window.open(`/contract/detail/${record.id}`, "_blank");
              }
            },
          };
        }}
        onChangePage={(page) => {
          setLoadingTable(true);
          setFilterTable({...filterTable, page});
        }}
      />
    </div>
  );
}
