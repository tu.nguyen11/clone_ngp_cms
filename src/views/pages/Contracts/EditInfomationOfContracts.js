import React, {useState, useEffect} from "react";
import {Row, Col, Skeleton, Empty, Checkbox, Table, message} from "antd";
import moment from "moment";
import {ISvg, IButton} from "../../components";
import {colors} from "../../../assets";
import {StyledITitle} from "../../components/common/Font/font";
import {APIService} from "../../../services";
import {useHistory, useParams} from "react-router-dom";
import "../input.css";
import styled from "styled-components";
import {Fancybox, IDatePickerFrom} from "../../components/common";
import FormatterDay from "../../../utils/FormatterDay";
import "./contractForm.css";

const StyledTable = styled(Table)`
  padding: 0;
  .ant-table-thead {
    background: #e4efff !important;
  }
  .ant-table-header {
    background-color: #e4efff !important;
    /* overflow-y: hidden !important; */
  }
`;

export default function DetailOfContract(props) {
  const history = useHistory();
  const {id} = useParams();

  const [isLoadingCallAPIDetail, setIsLoadingAPIDetail] = useState(false);
  const [loadingSave, setLoadingSave] = useState(false);

  const [dataDetail, setDataDetail] = useState({
    contracts: [],
    tien_do_hop_dong: [],
    ho_so_giai_ngan: [],
    url: "",
  });

  const getDetailContract = async (id) => {
    try {
      setIsLoadingAPIDetail(true);
      const data = await APIService._getDetailContract(id);
      const {detail, link_image} = data;
      setDataDetail({
        ...detail, contracts: detail.giay_to,
        tien_do_hop_dong: detail.tien_do_hop_dong,
        ho_so_giai_ngan: detail.ho_so_giai_ngan,
        url: link_image
      });
      setIsLoadingAPIDetail(false);
    } catch (error) {
      console.log({error});
      setIsLoadingAPIDetail(false);
    }
  };

  const postEditContract = async (editFilePaper, editHoSoGiaiNgan) => {
    try {
      setLoadingSave(true);
      const data = await APIService._postEditContract(editFilePaper, editHoSoGiaiNgan);
      setLoadingSave(false);
      message.success('Chỉnh sửa thành công')
      history.push(`/contract/detail/${id}`);
    } catch (error) {
      console.log({error});
      setLoadingSave(false);
    }
  };

  useEffect(() => {
    getDetailContract(id);
  }, [id]);

  const renderTitle = (title) => {
    return (
      <span
        style={{
          fontWeight: 500,
          fontSize: 14,
        }}
      >
        {title}
      </span>
    );
  };
  const columns_hs = [
    {
      title: "Hồ sơ giải ngân",
      align: "left",
      key: "name",
      dataIndex: "name",
      render: (name) => <span>{name || "-"}</span>,
    },
    {
      title: renderTitle("Ngày dự kiến"),
      align: "left",
      width: 200,
      render: (text, obj, index) => (
        <IDatePickerFrom
          showTime={false}
          inputReadOnly={false}
          isBorderBottom={true}
          format={"DD-MM-YYYY"}
          paddingInput="0px"
          placeholder="nhập thời gian"
          value={
            !obj.expected_date || obj.expected_date < 0
              ? undefined
              : moment(new Date(obj.expected_date), "DD-MM-YYYY")
          }
          onChange={(date) => {
            let ho_so_giai_ngan = [...dataDetail.ho_so_giai_ngan];
            ho_so_giai_ngan[index].expected_date = date._d.getTime();
            setDataDetail({...dataDetail, ho_so_giai_ngan});
          }}
        />
      ),
    },
  ];

  const columns_td = [
    {
      title: "",
      align: "center",
      width: 40,
      render: (obj) => {
        return (
          <Checkbox
            disabled
            checked={obj.status === 1 ? true : false}
          ></Checkbox>
        );
      },
    },
    {
      title: renderTitle("Hồ sơ giấy tờ xe"),
      key: "name",
      dataIndex: "name",
      render: (name) => <span>{name || "-"}</span>,
    }
  ];
  const columns = [
    {
      title: "",
      align: "center",
      width: 40,
      render: (obj) => {
        return (
          <Checkbox
            disabled
            checked={obj.status === 1 ? true : false}
          ></Checkbox>
        );
      },
    },
    {
      title: renderTitle("Hồ sơ, giấy tờ xe"),
      key: "name",
      dataIndex: "name",
      render: (name) => <span>{name || "-"}</span>,
    },
    {
      title: renderTitle("Ngày dự kiến"),
      align: "center",
      width: 200,
      render: (text, obj, index) => (
        <IDatePickerFrom
          showTime={false}
          inputReadOnly={false}
          isBorderBottom={true}
          format={"DD-MM-YYYY"}
          paddingInput="0px"
          placeholder="nhập thời gian"
          value={
            !obj.expected_date || obj.expected_date < 0
              ? undefined
              : moment(new Date(obj.expected_date), "DD-MM-YYYY")
          }
          onChange={(date) => {
            let giay_to = [...dataDetail.giay_to];
            giay_to[index].expected_date = date._d.getTime();
            setDataDetail({...dataDetail, giay_to});
          }}
        />
      ),
    },
  ];

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row gutter={[0, 30]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{color: colors.main, fontSize: 22}}>
              Thông tin chi tiết hợp đồng
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div className="flex justify-end">
            <IButton
              color={colors.main}
              title="Lưu"
              icon={ISvg.NAME.SAVE}
              styleHeight={{width: 140, marginRight: 15}}
              loading={loadingSave}
              onClick={() => {
                let giay_to = dataDetail.giay_to;
                let hs = dataDetail.ho_so_giai_ngan
                let giayToAdd = [];
                giay_to.forEach((item) => {
                  if (item.expected_date > 0 || item.expected_date !== null) {
                    giayToAdd.push({
                      expected_date: item.expected_date,
                      id_contract: Number(id),
                      id_paper: item.file_paper_id,
                    });
                  }
                });
                let arrHS = [];
                hs.forEach(item => {
                  if (item.expected_date > 0 || item.expected_date !== null) {
                    arrHS.push({
                      expected_date: item.expected_date,
                      id_contract: Number(id),
                      file_paper_disburesment_id: item.file_paper_disbursement_id,
                    })
                  }
                })
                postEditContract(giayToAdd, arrHS);
              }}
            />
            <IButton
              color={colors.oranges}
              title="Hủy"
              icon={ISvg.NAME.CROSS}
              styleHeight={{width: 140}}
              onClick={() => {
                history.push(`/contract/detail/${id}`);
              }}
            />
          </div>
        </Col>

        <Col span={24}>
          <Row gutter={[6, 6]} type="flex">
            <Col span={8}>
              <div
                style={{
                  padding: 20,
                  backgroundColor: "white",
                  height: "100%",
                }}
              >
                <Skeleton
                  paragraph={{rows: 14}}
                  active
                  loading={isLoadingCallAPIDetail}
                >
                  <Row gutter={[0, 20]}>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        Thông tin khách hàng
                      </StyledITitle>
                    </Col>
                    <Col span={12}>
                      <p style={{fontWeight: 600}}>Tên khách hàng</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.ten_khach_hang || "-"}
                      </span>
                    </Col>
                    <Col span={12}>
                      <p style={{ fontWeight: 600 }}>Mã khách hàng</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.user_code || "-"}
                      </span>
                    </Col>
                    {dataDetail.user_type_id === 1 ? (
                      <Col span={24}>
                        <p style={{ fontWeight: 600 }}>Mã số thuế</p>
                        <span
                          style={{
                            wordWrap: "break-word",
                            wordBreak: "break-all",
                          }}
                        >
                          {dataDetail.tax_code || "-"}
                        </span>
                      </Col>
                    ) : (
                      <Col span={24}>
                        <Row gutter={[12, 12]}>
                          <Col span={8}>
                            <p style={{ fontWeight: 600 }}>CMND</p>
                            <span
                              style={{
                                wordWrap: "break-word",
                                wordBreak: "break-all",
                              }}
                            >
                              {dataDetail.cmnd || "-"}
                            </span>
                          </Col>
                          <Col span={8}>
                            <p style={{ fontWeight: 600 }}>Ngày cấp</p>
                            <span
                              style={{
                                wordWrap: "break-word",
                                wordBreak: "break-all",
                              }}
                            >
                              {!dataDetail.date_cmnd ||
                              /1970/g.test(dataDetail.date_cmnd)
                                ? "-"
                                : FormatterDay.dateFormatWithString(
                                  dataDetail.date_cmnd,
                                  "#DD#/#MM#/#YYYY#"
                                )}
                            </span>
                          </Col>
                          <Col span={8}>
                            <p style={{ fontWeight: 600 }}>Nơi cấp</p>
                            <span
                              style={{
                                wordWrap: "break-word",
                                wordBreak: "break-all",
                              }}
                            >
                              {dataDetail.issued_cmnd || "-"}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                    )}
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Địa chỉ</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.address || "-"}
                      </span>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[12, 12]}>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Điện thoại</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.phone || "-"}
                          </span>
                        </Col>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>
                            Ngành nghề kinh doanh
                          </p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.nganh_nghe || "-"}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <p style={{ fontWeight: 600 }}>Trạng thái</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.contract_status_id === 6
                          ? "Đang chạy"
                          : dataDetail.contract_status_id === 9
                            ? "Đã giao"
                            : "Đã đóng"}
                      </span>
                    </Col>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        Thông tin xe
                      </StyledITitle>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Tên sản phẩm</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.product_name || "-"}
                      </span>
                    </Col>
                    <Col span={24}>
                      <p style={{fontWeight: 600}}>Loại xe</p>
                      <span
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-all",
                        }}
                      >
                        {dataDetail.nganh_hang || "-"}
                      </span>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[12, 12]}>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Dòng xe</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.dong_xe || "-"}
                          </span>
                        </Col>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Thương hiệu</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.loai_xe || "-"}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[12, 12]}>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Thương hiệu</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.thuong_hieu || "-"}
                          </span>
                        </Col>
                        <Col span={12}>
                          <p style={{fontWeight: 600}}>Loại thùng</p>
                          <span
                            style={{
                              wordWrap: "break-word",
                              wordBreak: "break-all",
                            }}
                          >
                            {dataDetail.loai_thung || "-"}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Skeleton>
              </div>
            </Col>

            {!dataDetail.images ||
            !dataDetail.images.length ||
            dataDetail.images[0] === "" ? null : (
              <Col span={3}>
                <div
                  style={{
                    padding: 20,
                    backgroundColor: "white",
                    height: "100%",
                  }}
                >
                  <Skeleton
                    paragraph={{rows: 14}}
                    active
                    loading={isLoadingCallAPIDetail}
                  >
                    <Row gutter={[0, 20]}>
                      <Col span={24} style={{paddingTop: 5}}>
                        <StyledITitle style={{color: "black", fontSize: 16}}>
                          Hình ảnh
                        </StyledITitle>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            maxHeight: 525,
                            overflowY: "auto",
                            overflowX: "hidden",
                          }}
                        >
                          <Fancybox>
                            <p>
                              {dataDetail.images.map((image, index) => {
                                return (
                                  <a
                                    key={index}
                                    data-fancybox="gallery"
                                    href={dataDetail.url + image}
                                  >
                                    <img src={dataDetail.url + image}
                                         alt="ảnh hợp đồng"
                                         width="100%"
                                         style={{
                                           marginBottom: 10,
                                           display: index > 5 ? "none" : 'block'
                                         }}
                                    />
                                  </a>
                                );
                              })}
                            </p>
                          </Fancybox>
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            )}

            <Col span={13}>
              <div
                style={{
                  padding: 20,
                  backgroundColor: "white",
                  height: "100%",
                }}
              >
                <Skeleton
                  paragraph={{rows: 14}}
                  active
                  loading={isLoadingCallAPIDetail}
                >
                  <Row gutter={[0, 20]}>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        Tiến độ hợp đồng
                      </StyledITitle>
                    </Col>
                    <Col span={24}>
                      <StyledTable
                        dataSource={dataDetail.tien_do_hop_dong}
                        columns={columns_td}
                        bordered
                        size="small"
                        pagination={false}
                        style={{
                          width: "100%",
                          border: "1px solid #e8e8e8",
                          borderRadius: 0,
                        }}
                        scroll={{x: 0, y: 495}}
                        locale={{
                          emptyText: <Empty description="Không có thông tin"/>,
                        }}
                      />
                    </Col>
                    <Col span={24} style={{paddingTop: 5}}>
                      <StyledITitle style={{color: "black", fontSize: 16}}>
                        1.Hồ sơ giao xe
                      </StyledITitle>
                    </Col>
                    <Col span={24}>
                      <StyledTable
                        dataSource={dataDetail.contracts}
                        columns={columns}
                        bordered
                        size="small"
                        pagination={false}
                        style={{
                          width: "100%",
                          border: "1px solid #e8e8e8",
                          borderRadius: 0,
                        }}
                        scroll={{x: 0, y: 495}}
                        locale={{
                          emptyText: <Empty description="Không có thông tin"/>,
                        }}
                      />
                    </Col>
                    {dataDetail.payment_type_id === 2 && (
                      <>
                        <Col span={24} style={{paddingTop: 5}}>
                          <StyledITitle style={{color: "black", fontSize: 16}}>
                            2.Hồ sơ giải ngân ( đối với khách hàng vay ngân hàng)
                          </StyledITitle>
                        </Col>
                        <Col span={24}>
                          <StyledTable
                            dataSource={dataDetail.ho_so_giai_ngan}
                            columns={columns_hs}
                            bordered
                            size="small"
                            pagination={false}
                            style={{
                              width: "100%",
                              border: "1px solid #e8e8e8",
                              borderRadius: 0,
                            }}
                            scroll={{x: 0, y: 495}}
                            locale={{
                              emptyText: <Empty description="Không có thông tin"/>,
                            }}
                          />
                        </Col>
                      </>
                    )}
                  </Row>
                </Skeleton>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
