import React, {useEffect, useState} from "react"
import {Col, Row, Skeleton, Empty} from "antd";
import {StyledITitle} from "../../../components/common/Font/font";
import {colors} from "../../../../assets";
import {IButton, ISvg, ITableHtml} from "../../../components";
import {useParams, useHistory} from 'react-router-dom'
import {APIService} from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";

export default function DetailTradingSchedule(props) {
  const history = useHistory()
  const {id} = useParams()
  const [loading, setLoading] = useState(true)
  const [dataTable, setDataTable] = useState({
    dataTable1: [],
    dataTable2: []
  })
  const isHideButton = [9, 10]
  const isSeting = props.location.state
  const getDetail = async (id) => {
    try {
      const data = await APIService._getScheduleCar(id)
      let dataReady = data.schedule.filter(item => item.type_1_thoi_gian_giao_type_2_thoi_gian_chuan_bi_giao === 2)
      let dataTrading = data.schedule.filter(item => item.type_1_thoi_gian_giao_type_2_thoi_gian_chuan_bi_giao === 1)
      setDataTable({
        dataTable1: [...dataTrading],
        dataTable2: [...dataReady]
      })
      setLoading(false)
    } catch (e) {
      setLoading(false)
      console.log(e)
    }
  }
  useEffect(() => {
    getDetail(id)
  }, [])
  let headerTable = [
    {
      name: "Thời gian tiến hành giao xe",
      align: "left",
      width: '80%'
    },
    {
      name: "Ngày dự kiến",
      align: "left",
    },
  ];
  let headerTableReady = [
    {
      name: "Thời gian chuẩn bị giao xe",
      align: "left",
      width: '80%'
    },
    {
      name: "Ngày dự kiến",
      align: "left",
    },
  ];
  const headerTableRender = (headerTable) => {
    return (
      <tr className="scroll" style={{background: "#E4EFFF"}}>
        {headerTable.map((item, index) => (
          <th className="th-table" style={{textAlign: item.align, width: item.width}}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };
  const bodyTableReadyTime = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td colspan="7" style={{padding: 20}}>
          <Empty description="Không có dữ liệu"/>
        </td>
      </tr>
    ) : contentTable.map((item, index) => {
      return (
        <tr key={index} className="tr-table" style={{maxHeight: 500, overflow: "auto"}}>
          <td className="td-table" style={{textAlign: "left"}}>
            <span>{item.name}</span>
          </td>
          <td
            className="td-table"
            style={{
              textAlign: "left",
              overflowWrap: "break-word",
              maxWidth: 250,
            }}
          >
            <span>{item.expected_date ? FormatterDay.dateFormatWithString(
              item.expected_date,
              "#DD#/#MM#/#YYYY#"
            ) : '-'}</span>
          </td>
        </tr>
      )
    })
  }
  return (
    <div>
      <Row>
        <Col span={24}>
          <div>
            <StyledITitle style={{color: colors.main, fontSize: 22}}>
              Lịch giao xe
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div style={{display: 'flex', justifyContent: 'flex-end'}}>
            {(isHideButton.includes(isSeting) || isSeting === null) ? null : (
              <IButton
                title={'Chỉnh sửa'}
                icon={ISvg.NAME.WRITE}
                color={colors.main}
                onClick={() => {
                  history.push(`/contract/tradingSchedule/edit/${id}`)
                }}
              />
            )}
          </div>
        </Col>
        <Col span={24}>
          <div style={{width: 800, padding: 24, background: '#fff', marginTop: 40}}>
            <Skeleton
              paragraph={{rows: 14}}
              active
              loading={loading}
            >
              <h6 style={{paddingBottom: 20}}>Thời gian giao xe</h6>
              <ITableHtml
                childrenBody={bodyTableReadyTime(dataTable.dataTable1)}
                childrenHeader={headerTableRender(headerTable)}
              />
              <div style={{marginTop: 40}}>
                <ITableHtml
                  childrenBody={bodyTableReadyTime(dataTable.dataTable2)}
                  childrenHeader={headerTableRender(headerTableReady)}
                />
              </div>
            </Skeleton>
          </div>
        </Col>
      </Row>
    </div>
  )
}
