import React, {useEffect, useState} from 'react'
import {Row, Col, Empty, Skeleton, Form} from 'antd'
import {StyledITitle} from "../../../components/common/Font/font";
import {colors} from "../../../../assets";
import {IButton, ISvg, ITableHtml} from "../../../components";
import {useHistory, useParams} from "react-router-dom";
import {APIService} from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";
import moment from "moment";
import {IDatePickerFrom} from "../../../components/common";
import {message} from "antd/es";

export default function EditTradingSchedule() {

  const history = useHistory()
  const {id} = useParams()
  const [loading, setLoading] = useState(true)
  const [loadingButton, setLoadingButton] = useState(false)
  const [dataTable, setDataTable] = useState({
    dataTable1: [],
    dataTable2: []
  })
  const getDetail = async (id) => {
    try {
      const data = await APIService._getScheduleCar(id)
      let dataTrading = data.schedule.filter(item => item.type_1_thoi_gian_giao_type_2_thoi_gian_chuan_bi_giao === 1)
      let dataReady = data.schedule.filter(item => item.type_1_thoi_gian_giao_type_2_thoi_gian_chuan_bi_giao === 2)
      setDataTable({
        dataTable1: [...dataTrading],
        dataTable2: [...dataReady]
      })
      setLoading(false)
    } catch (e) {
      setLoading(false)
      console.log(e)
    }
  }
  useEffect(() => {
    getDetail(id)
  }, [])
  let headerTable = [
    {
      name: "Thời gian tiến hành giao xe",
      align: "left",
      width: '70%'
    },
    {
      name: "Ngày dự kiến",
      align: "left",
      width: '70%'
    },
  ];
  let headerTableReady = [
    {
      name: "Thời gian chuẩn bị giao xe",
      align: "left",
      width: '70%'
    },
    {
      name: "Ngày dự kiến",
      align: "left",
      width: '70%'
    },
  ];
  const headerTableRender = (headerTable) => {
    return (
      <tr className="scroll" style={{background: "#E4EFFF"}}>
        {headerTable.map((item, index) => (
          <th className="th-table" style={{textAlign: item.align, width: item.width}}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };
  const bodyTableReadyTime = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td colspan="7" style={{padding: 20}}>
          <Empty description="Không có dữ liệu"/>
        </td>
      </tr>
    ) : contentTable.map((item, index) => {
      return (
        <tr key={index} className="tr-table" style={{maxHeight: 500, overflow: "auto"}}>
          <td className="td-table" style={{textAlign: "left"}}>
            <span>{item.name}</span>
          </td>
          <td
            className="td-table"
            style={{
              textAlign: "left",
              overflowWrap: "break-word",
              maxWidth: 250,
            }}
          >{item.type_1_thoi_gian_giao_type_2_thoi_gian_chuan_bi_giao === 2 ? (
            <IDatePickerFrom
              showTime={false}
              inputReadOnly={false}
              isBorderBottom={true}
              format={"DD-MM-YYYY"}
              paddingInput="0px"
              placeholder="nhập thời gian"
              value={
                !item.expected_date || item.expected_date < 0
                  ? null
                  : moment(new Date(item.expected_date), "DD-MM-YYYY")
              }
              onChange={(date) => {
                if(date){
                  let data = [...dataTable.dataTable2];
                  data[index].expected_date = date._d.getTime();
                  setDataTable({...dataTable, data});
                }
              }}
            />
          ) : (
            <span>{FormatterDay.dateFormatWithString(
              item.expected_date,
              "#DD#/#MM#/#YYYY#"
            )}</span>
          )
          }

          </td>
        </tr>
      )
    })
  }
  const updateSchedule = async (data) => {
    try {
      const res = await APIService._updateScheduleCar(data)
      message.success('Cập nhật thành công')
      setLoadingButton(false)
      history.push(`/contract/tradingSchedule/detail/${id}`)
    } catch (e) {
      setLoadingButton(false)
      console.log(e)
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    let isEmpty = dataTable.dataTable2.filter(item => {
      return item.expected_date === 0 || item.expected_date === null
    })
    console.log(dataTable.dataTable2)
    console.log(isEmpty)
    if (isEmpty.length >= 2) {
      message.error('Vui lòng điền ít nhất 2 ngày dự kiến giao xe')
    } else {
      setLoadingButton(true)
      updateSchedule(dataTable.dataTable2)
    }
  }
  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col span={24}>
            <div>
              <StyledITitle style={{color: colors.main, fontSize: 22}}>
                Chỉnh sửa lịch giao xe
              </StyledITitle>
            </div>
          </Col>
          <Col span={24}>
            <div style={{display: 'flex', justifyContent: 'flex-end'}}>
              <IButton
                title={'Lưu'}
                loading={loadingButton}
                icon={ISvg.NAME.SAVE}
                color={colors.main}
                htmlType={'submit'}
              />
              <IButton
                loading={loadingButton}
                title={'Hủy bỏ'}
                icon={ISvg.NAME.CROSS}
                style={{marginLeft: 15}}
                color={colors.oranges}
                onClick={() => {
                  history.push(`/contract/tradingSchedule/detail/${id}`)
                }}
              />
            </div>
          </Col>
          <Col span={24}>
            <div style={{width: 800, padding: 24, background: '#fff'}}>
              <Skeleton
                paragraph={{rows: 14}}
                active
                loading={loading}
              >
                <h6 style={{paddingBottom: 20}}>Thời gian giao xe</h6>
                <ITableHtml
                  childrenBody={bodyTableReadyTime(dataTable.dataTable1)}
                  childrenHeader={headerTableRender(headerTable)}
                />
                <div style={{marginTop: 40}}>
                  <ITableHtml
                    childrenBody={bodyTableReadyTime(dataTable.dataTable2)}
                    childrenHeader={headerTableRender(headerTableReady)}
                  />
                </div>
              </Skeleton>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  )
}
