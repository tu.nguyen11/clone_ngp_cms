
let default_numbers = ' hai ba bốn năm sáu bảy tám chín';
let units = ('1 một' + default_numbers).split(' ');
let ch = 'lẻ mười' + default_numbers;
let tr = 'không một' + default_numbers;
let tram = tr.split(' ');
let u = '2 nghìn triệu tỉ'.split(' ');
let chuc = ch.split(' ');

/**
 * additional words
 * @param  {[type]} a [description]
 * @return {[type]}   [description]
 */
function tenth(a) {
    let sl1 = units[a[1]];
    let sl2 = chuc[a[0]];
    let append = '';
    if (a[0] > 0 && a[1] == 5)
        sl1 = 'lăm';
    if (a[0] > 1) {
        append = ' mươi';
        if (a[1] == 1)
            sl1 = ' mốt';
    }
    let str = sl2 + '' + append + ' ' + sl1;
    return str;
}

/**
 * convert number in blocks of 3
 * @param  {[type]} d [description]
 * @return {[type]}   [description]
 */
function block_of_three(d) {
    let _a;
    _a = d + '';
    if (d == '000') return '';
    switch (_a.length) {
        case 0:
            return '';

        case 1:
            return units[_a];

        case 2:
            return tenth(_a);

        case 3:
            let sl12;
            sl12 = '';
            if (_a.slice(1, 3) != '00')
                sl12 = tenth(_a.slice(1, 3));
            let sl3;
            sl3 = tram[_a[0]] + ' trăm';
            return sl3 + ' ' + sl12;
    }
}

function numberToText(str, currency) {
    str = parseInt(str) + '';
    //str=fixCurrency(a,1000);
    let i = 0;
    let arr = [];
    let index = str.length;
    let result = []
    if (index == 0 || str == 'NaN')
        return '';
    let string = '';

    //explode number string into blocks of 3numbers and push to queue
    while (index >= 0) {
        arr.push(str.substring(index, Math.max(index - 3, 0)));
        index -= 3;
    }

    //loop though queue and convert each block
    for (i = arr.length - 1; i >= 0; i--) {
        if (arr[i] != '' && arr[i] != '000') {
            result.push(block_of_three(arr[i]))
            if (u[i])
                result.push(u[i]);
        }
    }
    if (currency)
        result.push(currency)
    string = result.join(' ')
    //remove unwanted white space
    string = string.replace(/[0-9]/g, '').replace(/  /g, ' ').replace(/ $/, '');
    // Upper Case first letter
    return string.charAt(0).toUpperCase() + string.slice(1)
}

export default numberToText
