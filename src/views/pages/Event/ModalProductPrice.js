import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import { Transfer, Table, Button, List, Checkbox, Input, message } from "antd";
import difference from "lodash/difference";
import uniqBy from "lodash/uniqBy";
import { APIService } from "../../../services";
import ISelectFrequency from "../../components/ISelectFrequency";
import {
  ITable,
  ISvg,
  IButton,
  IList,
  ITitle,
  ISearch,
  IInput,
} from "../../components";
import { Container, Row, Col } from "reactstrap";
import { colors } from "../../../assets";
const { Search } = Input;

let arrayAdd = [];

class ModalProductPrice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      targetKeys: [],
      dataSource: [],
      totalDataSource: [],
      current: 0,
      pageSize: 10,
      key: "",
      keyword: "",
      loading: false,
      selectedRowKeys: [],
      dataRight: !this.props.dataRight ? this.props.dataRight : [],
      arrayAdd: [],
      price: "",
    };
    this.onChange = this.onChange.bind(this);
  }

  _getListProduct = async (keyword) => {
    this.setState({ loading: true });
    const data = await APIService._getModalListProduct(keyword);

    const pagination = { ...this.state.pagination };
    pagination.total = 100;
    this.setState({
      loading: false,
      dataProduct: data.products,
      pagination,
    });
  };

  componentDidMount() {
    this._getListProduct(this.state.keyword);
  }

  onChange(checkedValues) {
    let obj = checkedValues.map((item, index) => {
      const stt = index + 1;
      const id = item.id;
      const name = item.name;
      const code = item.code;
      return { stt, id, name, code };
    });
    arrayAdd = obj;
    this.setState({
      dataProduct: this.state.dataProduct,
    });
  }
  render() {
    const columnsRight = [
      {
        dataIndex: "stt",
        title: "STT",
      },
      {
        dataIndex: "code",
        title: "Mã sản phẩm",
      },
      {
        dataIndex: "name",
        title: "Tên sản phẩm",
      },
      // {
      //   // dataIndex: ,
      //   title: 'Doanh thu',
      //   render: obj => {
      //     return (
      //       <IInput
      //         onChange={event => {
      //
      //           obj.price = event.target.value;
      //           this.setState({price: obj.price});
      //         }}
      //       />
      //     );
      //   },
      // },
      {
        dataIndex: "id",
        align: "right",
        width: 50,
        render: (id) => (
          <div
            style={{
              width: 20,
              height: 20,
              borderRadius: 10,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              background: colors.gray._300,
            }}
            className="cursor"
            onClick={() => {
              var objNew = this.state.dataRight.find((item) => {
                return item.id == id;
              });
              if (objNew) {
                let index = this.state.dataRight.indexOf(objNew);

                this.state.dataRight.splice(index, 1);
                this.setState({});
              }
            }}
          >
            <ISvg
              name={ISvg.NAME.CROSS}
              width={7}
              height={7}
              fill={colors.oranges}
            />
          </div>
        ),
      },
    ];

    return (
      <Row>
        <Row className="mb-4 ml-3">
          <ITitle
            title={"Chọn sản phẩm áp dụng"}
            style={{ fontWeight: 700 }}
            level={3}
          />
        </Row>
        <Row className="p-0 m-0">
          <Col xs="auto">
            <div style={{ height: 475, overflow: "auto" }}>
              <Search
                placeholder=""
                onSearch={(value) => {
                  this._getListProduct(value);
                }}
                // style={{ width: 200 }}
              />
              <Checkbox.Group
                style={{ width: "100%" }}
                onChange={this.onChange}
              >
                <List
                  dataSource={this.state.dataProduct}
                  renderItem={(item, index) => (
                    <List.Item>
                      <Checkbox
                        // value={{ ...item, ...{ stt: index } }}
                        value={item}
                        style={{ marginRight: 16 }}
                        // defaultChecked={item.route_active}
                        // disabled={item.route_active}
                      />
                      <ITitle level={4} title={item.name} />
                    </List.Item>
                  )}
                  style={{ height: 475 }}
                  // columns={columnsLeft}
                  // bodyStyle={{ height: 400 }}
                  // rowSelection={rowSelection}
                />
              </Checkbox.Group>
            </div>
            <div
              style={{
                flex: 1,
                display: "flex",
                justifyContent: "flex-end",
                marginTop: 30,
              }}
            >
              <Button
                type="primary"
                style={{
                  flexDirection: "row",
                  display: "flex",
                  minWidth: 160,
                  paddingLeft: 30,
                  paddingRight: 30,
                  height: 42,
                  borderRadius: 0,
                  borderColor: colors.main,
                  borderWidth: 1,
                  background: colors.white,
                }}
                onClick={() => {
                  if (arrayAdd.length == 0) {
                    message.info("Chưa chọn sản phẩm");
                    return;
                  }
                  for (var i = 0; i < arrayAdd.length; i++) {
                    var currentItem = arrayAdd[i];

                    var check = this.state.dataRight.find((item, index) => {
                      return item.id == currentItem.id;
                    });
                    if (check) {
                      continue;
                    }
                    this.state.dataRight.push(currentItem);
                  }

                  // this.state.dataRight = this.state.dataRight.concat(arrayAdd);
                  this.setState({});
                }}
              >
                <span
                  style={{ color: colors.main, fontSize: 14, fontWeight: 500 }}
                >
                  Thêm
                </span>
                <div style={{ marginLeft: 30 }}>
                  <ISvg
                    name={ISvg.NAME.BUTTONRIGHT}
                    width={20}
                    height={20}
                    fill={colors.main}
                  />
                </div>
              </Button>
            </div>
          </Col>
          <Col>
            <ITable
              data={this.state.dataRight}
              scroll={{ y: 475 }}
              columns={columnsRight}
              // bodyStyle={{height: 475}}
            />
            <div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  marginTop: 20,
                }}
              >
                <IButton
                  icon={ISvg.NAME.SAVE}
                  title="Lưu"
                  onClick={() => {
                    //
                    if (this.state.dataRight.length == 0) {
                      this.props.onSave([]);
                      return;
                    }
                    // for (var i in this.state.dataRight) {
                    //   var item = this.state.dataRight[i];
                    //   if (this.state.price == '') {
                    //     message.error('Chưa nhập doanh thu');
                    //     return null;
                    //   }
                    // }
                    this.props.onSave(this.state.dataRight);
                  }}
                  color={colors.main}
                  style={{ marginRight: 20, width: 120 }}
                />
                <IButton
                  icon={ISvg.NAME.CROSS}
                  title="Hủy bỏ"
                  onClick={() => {
                    this.props.onCancel();
                  }}
                  color={colors.oranges}
                />
              </div>
            </div>
          </Col>
        </Row>
      </Row>
    );
  }
}

export default ModalProductPrice;
