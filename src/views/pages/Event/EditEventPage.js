import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  Upload,
  Icon,
  Form,
  message,
  Checkbox,
  DatePicker
} from 'antd'
import React, { Component } from 'react'
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  ISelectStatus,
  ICascader,
  ISelectAttrubie,
  IDatePicker
} from '../../components'
import { priceFormat } from '../../../utils'

import InfiniteScroll from 'react-infinite-scroller'
import { colors, images } from '../../../assets'
import { Container, Row, Col } from 'reactstrap'
import { APIService } from '../../../services'
import IUploadMultiple from '../../components/IUploadMultiple'
import { ImageType } from '../../../constant'
import moment from 'moment'
import FormatterDay from '../../../utils/FormatterDay'
import IButtonRight from '../../components/IButtonRight'
const widthScreen = window.innerWidth

const { Paragraph, Title } = Typography
function _renderTitle (title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: 'bold' }} />
    </div>
  )
}
function _renderColumns (title) {
  return (
    <div style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
      <ITitle level={4} title={title} />
    </div>
  )
}

function _widthColumn (widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn
  if (widthScreen <= 1200) {
    widthColumn = 1200 / 10
  } else widthColumn = (widthScreen - 300) / 10
  return widthColumn * type
}
class EditEventPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: {
        product: {
          id: 12,
          productCode: 'XA0050012',
          name: 'NPK BACONCO 16-16-8+13S+TE(Zn+B) ',
          status: -1,
          typeId: 1,
          typeName: 'PHÂN BÓN',
          categoryId: 3,
          categoryName: 'PHÂN BÓN BACONCO',
          attributeIdMin: 1,
          variationNameMin: 'Bao',
          quantityMin: 1,
          attributeIdMax: 3,
          variationNameMax: 'Tấn',
          quantityMax: 20,
          price_min: 550000.0,
          oldPrice_min: 550000.0,
          price_max: 1.1e7,
          oldPrice_max: 1.1e7,
          distributorOffer: 0.0,
          uses: 'Dữ liệu đang cập nhật',
          element: 'Dữ liệu đang cập nhật',
          manual: '',
          note: null,
          image: '["BACONCO16.16.813STE.png"]',
          image_url: 'http://dev-res.vinhhoang.com.vn/kido_image/product/',
          statusName: 'Ẩn',
          images: ['BACONCO16.16.813STE.png']
        },
        images: []
      },
      selectedRowKeys: [],
      arrayImagesAdd: [],
      loading: false,
      write: true,
      valueStoreProduct: 0,
      nameStoreProduct: '',
      status: 0,
      errStatus: true,
      errCategory: true,
      value: '', // Check here to configure the default column
      opick: true,
      visible: false,
      page: 1,
      total: 0,
      sizeItem: 0,
      valueSearch: '',
      status: 0,
      value: '',
      city_id: 0,
      district_id: 0,
      key: '',
      arrID: [],
      arrow: true,
      mocData: []
    }
    this.id = Number(this.props.match.params.id)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.onChangeStatus = this.onChangeStatus.bind(this)
    this.onChangeCategory = this.onChangeCategory.bind(this)
  }

  componentDidMount () {
    this._getAPIListSuppliers(
      this.state.status,
      this.state.page,
      this.state.city_id,
      this.state.district_id,
      this.state.key
    )
    window.addEventListener('resize', this.handleResize)
  }

  _getAPIListSuppliers = async () => {
    try {
      const data = await APIService._getListSuppliers(
        this.state.status,
        this.state.page,
        this.state.city_id,
        this.state.district_id,
        this.state.key
      )

      let suppliers = data.suppliers
      suppliers.map((item, index) => {
        suppliers[index].stt = (this.state.page - 1) * 10 + index + 1
        suppliers[index].sttAndId = {
          id: item.id,
          stt: index
        }
        suppliers[index].fullAddress =
          item.address + ', ' + item.districtsName + ', ' + item.cityName
      })

      this.setState({
        data: suppliers,
        mocData: suppliers,
        total: data.total,
        sizeItem: data.size,
        loadingTable: false
      })
    } catch (error) {
      message.error(error)
      this.setState({
        loadingTable: false
      })
    }
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return
      }
      fieldsValue.typeId = this.state.typeId
      fieldsValue.categoryId = this.state.categoryId
      fieldsValue.status = this.state.status
      // fieldsValue.id = 0; // 0 là thêm server rằng
      fieldsValue.image = ['hinh12.png']
      const objAdd = {
        id: 0, // 0 là thêm server rằng
        productCode: fieldsValue.productCode,
        productName: fieldsValue.productName,
        status: fieldsValue.status,
        typeId: fieldsValue.typeId,
        categoryId: fieldsValue.categoryId,
        attributeIdMin: this.state.attributeIdMin,
        quantityMin: Number(fieldsValue.quantityMin),
        attributeIdMax: this.state.attributeIdMax,
        quantityMax: Number(fieldsValue.quantityMax),
        priceMin: Number(fieldsValue.priceMin),
        priceMax: Number(fieldsValue.priceMax),
        distributorOffer: Number(fieldsValue.distributorOffer),
        uses: fieldsValue.uses,
        element: fieldsValue.element,
        manual: fieldsValue.manual,
        note: fieldsValue.note,
        image: this.state.arrayImagesAdd
      }

      this._APIpostAddProduct(objAdd)
    })
  }

  onChangeStatus = value => {
    this.setState({ status: value.key, errStatus: false }, () =>
      this.props.form.validateFields(['status'], { force: true })
    )
  }

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
    }
    if (value.length == 2) {
      this.setState({
        errCategory: false,
        typeId: value[0],
        categoryId: value[1]
      })
    }
  }
  showModal = () => {
    this.setState({
      visible: true
    })
  }

  handleOk = e => {
    console.log(e)
    this.setState({
      visible: false
    })
  }
  handleCancel = e => {
    console.log(e)
    this.setState({
      visible: false
    })
  }
  onSelectChange = selectedRowKeys => {
    let arrayID = []

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.data[index])
    })
    console.log('selectedRowKeys changed: ', selectedRowKeys)
    console.log('array changed: ', arrayID)
    this.setState({ selectedRowKeys, mocData: arrayID })
  }
  handleAdd = () => {
    this.state.mocData.map(item => {
      this.state.data.push(item)
    })
    this.setState({})
  }
  render () {
    const columnsType = [
      {
        dataIndex: 'supplierCode',
        key: 'supplierCode',
        align: 'left',
        width: 300,
        render: supplierCode => _renderColumns(supplierCode)
      }
    ]

    const columns = [
      {
        title: _renderTitle('STT'),
        dataIndex: 'stt',
        key: 'stt',
        align: 'center',
        // fixed: "left",
        width: 100,
        render: id => _renderColumns(id)
      },
      {
        title: _renderTitle('Mã sản phm'),
        dataIndex: 'supplierCode',
        key: 'supplierCode',
        align: 'left',
        width: 300,

        render: supplierCode => _renderColumns(supplierCode)
      },

      {
        title: _renderTitle('Tên sản phẩm'),
        dataIndex: 'supplierName',
        key: 'supplierName',
        align: 'left',
        width: 500,
        render: supplierName => <ITitle level={4} title={supplierName} />
      },
      {
        title: _renderTitle('Số lượng'),
        dataIndex: 'phone',
        key: 'phone',
        align: 'center',
        // width: _widthColumn(this.state.width, 1),
        width: 150,

        render: phone => _renderColumns(phone)
      },
      {
        title: '',
        dataIndex: 'sttAndId',
        key: 'sttAndId',
        align: 'right',
        // fixed: "right",
        width: 100,

        render: sttAndId => (
          <Row>
            <Col
              // onClick={() =>
              //   this.props.history.push('/detailAgency/' + sttAndId.id)
              // }
              className='cursor'
            >
              <ISvg
                name={ISvg.NAME.CROSS}
                width={11}
                height={20}
                // color={colors.oranges}
                fill={colors.oranges}
              />
            </Col>
          </Row>
        )
      }
    ]

    const { getFieldDecorator } = this.props.form
    const dateFormat = 'DD/MM/YYYY'
    const { selectedRowKeys, value } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true
    }
    return (
      <Container fluid>
        <Row className='p-0 m-0' xs='auto' sm={12} md={12}>
          <ITitle
            level={1}
            title='Chi tiết chương trình'
            style={{ color: colors.main, fontWeight: 'bold' }}
          />
        </Row>
        <Form onSubmit={this.handleSubmit}>
          <Row
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              paddingTop: 32,
              paddingBottom: 32
            }}
          >
            <IButton
              icon={ISvg.NAME.SAVE}
              title='Lưu'
              htmlType='submit'
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => this.handleSubmit}
            />
            <IButton
              icon={ISvg.NAME.CROSS}
              title='Hủy'
              color={colors.oranges}
              style={{ marginRight: 20 }}
              onClick={() => {
                this.props.history.push('/productPage')
              }}
            />
          </Row>
          <Row style={{ height: '100%' }} className='p-0' sm={12} md={12}>
            <Col
              style={{ background: colors.white }}
              className='pl-5 ml-3 mr-3'
            >
              <Row className='p-0 m-0' style={{}}>
                <Col
                  className='pl-0 pr-0 pt-0 m-0'
                  style={{ paddingBottom: 40 }}
                >
                  <Row className='p-0 mt-4 mb-4 ml-0 mr-0'>
                    <ITitle
                      level={2}
                      title='Thông tin sản phẩm'
                      style={{ color: colors.black, fontWeight: 'bold' }}
                    />
                  </Row>

                  <Row className='p-0 mt-2 ml-0 mb-0 mr-0' xs='auto'>
                    <Col className='p-0 m-0'>
                      <Form.Item>
                        {getFieldDecorator('productName', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'Chọn loại chương trinh'
                            }
                          ]
                        })(
                          <Col className='p-0 m-0'>
                            <Row className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Loại phần thưởng'}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className='p-0 m-0'>
                              <ISelectStatus placeholder='chọn....' />
                            </Row>
                          </Col>
                        )}
                      </Form.Item>
                    </Col>

                    <Col
                      className='p-0'
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator('productCode', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'Chọn điều kiện tham gia'
                            }
                          ]
                        })(
                          <div>
                            <Row className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Điều kiện tham gia'}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className='p-0 m-0'>
                              <ISelectAttrubie
                                placeholder='chọn điều kiện tham gia'
                                type='max'
                                onChange={value => {
                                  this.setState(
                                    {
                                      attributeIdMax: value,
                                      errAttributesMax: false
                                    },
                                    () =>
                                      this.props.form.validateFields(
                                        ['attributeIdMax'],
                                        { force: true }
                                      )
                                  )
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>

                  {/* <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator('productCode', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'nhập mã',
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={'Mã sản phẩm'}
                                style={{fontWeight: 700}}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput placeholder="nhập mã" />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>

                    <Col className="p-0" style={{marginLeft: 28, marginTop: 0}}>
                      <Form.Item>
                        {getFieldDecorator('status', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errStatus,
                              message: 'chọn trạng thái',
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={'Trạng thái'}
                                style={{fontWeight: 700}}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ISelectStatus
                                onChange={this.onChangeStatus}
                                placeholder="chọn trạng thái"
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row> */}

                  {/* <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 mt-0">
                      <Form.Item>
                        {getFieldDecorator('category', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: this.state.errCategory,
                              message: 'chọn nhãn hàng',
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={'Nhãn hàng'}
                                style={{fontWeight: 700}}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ICascader
                                type="category"
                                level={2}
                                style={{width: '100%'}}
                                placeholder="chọn nhóm sản phẩm"
                                onChange={this.onChangeCategory}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row> */}

                  <Row className='p-0 mt-4 mb-4 ml-0 mr-0'>
                    <ITitle
                      level={2}
                      title='Quy cách'
                      style={{ color: colors.black, fontWeight: 'bold' }}
                    />
                  </Row>
                  <Row className='p-0 mt-2 ml-0 mb-0 mr-0' xs='auto'>
                    <Col className='p-0 m-0'>
                      <Form.Item>
                        {getFieldDecorator('priceMin', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'Chọn điều mua hàng'
                            }
                          ]
                        })(
                          <div>
                            <Row className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Điều kiện mua hàng'}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className='p-0 m-0'>
                              <ISelectAttrubie
                                placeholder='chai/túi/bao 10kg...'
                                type='min'
                                onChange={value => {
                                  this.setState(
                                    {
                                      attributeIdMin: value,
                                      errAttributesMin: false
                                    },
                                    () =>
                                      this.props.form.validateFields(
                                        ['attributeIdMin'],
                                        { force: true }
                                      )
                                  )
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>

                    <Col
                      className='p-0'
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator('priceMin', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'Thêm danh sách áp dụng'
                            }
                          ]
                        })(
                          <div>
                            <Row className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Danh sách áp dụng'}
                                style={{ fontWeight: 700 }}
                              />
                              {/* <IButton
                                icon={ISvg.NAME.ARROWUP}
                                title="Tạo mới"
                                color={{background: 'rgb(190, 200, 200)'}}
                                style={{marginRight: 20}}
                                shape="circle"
                                type
                              /> */}
                              <Button
                                style={{
                                  background: 'rgb(190, 200, 200)',
                                  justifyContent: 'center',
                                  alignItems: 'center'
                                }}
                                shape='circle'
                                onClick={this.showModal}
                              >
                                <Icon type='plus' style={{ marginBottom: 3 }} />
                              </Button>
                              <Modal
                                style={{ width: 1000 }}
                                title='Chọn sản phẩm áp dụng'
                                visible={this.state.visible}
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                                // okText="Lưu"
                                // cancelText="Hủy"
                                // // bodyStyle={{ backgroundColor:'pink',width:500 }}
                                width={1000}
                                footer={[
                                  <Row>
                                    <Col xs={6}></Col>
                                    <Col
                                      className='clearfix'
                                      style={{ padding: '.5rem' }}
                                    >
                                      <IButton
                                        className='btn btn-danger float-right'
                                        icon={ISvg.NAME.CROSS}
                                        title='Hủy bỏ'
                                        color={colors.oranges}
                                        htmlType='submit'
                                        style={{ marginRight: 20 }}
                                        onClick={() => this.handleSubmit}
                                      />
                                      <IButton
                                        className='btn btn-danger float-right'
                                        icon={ISvg.NAME.SAVE}
                                        title='Lưu'
                                        color={colors.main}
                                        htmlType='submit'
                                        style={{ marginRight: 20 }}
                                        onClick={() => this.handleSubmit}
                                      />
                                    </Col>
                                    {/* <Col xs={6}></Col>
                                    <Col style={{backgroundColor: 'yellow'}}>
                                      <IButton
                                        icon={ISvg.NAME.SAVE}
                                        title="Lưu"
                                        color={colors.main}
                                        htmlType="submit"
                                        style={{marginRight: 20}}
                                        onClick={() => this.handleSubmit}
                                      />
                                    </Col>
                                    <Col
                                      className="float-lg-right"
                                      style={{backgroundColor: 'red'}}
                                    >
                                      <IButton
                                        icon={ISvg.NAME.CROSS}
                                        title="Hủy bỏ"
                                        color={colors.oranges}
                                        htmlType="submit"
                                        style={{marginRight: 20}}
                                        onClick={() => this.handleSubmit}
                                      />
                                    </Col> */}
                                  </Row>
                                ]}
                              >
                                <Row>
                                  <Col sm={4} style={{}}>
                                    <Row className='mb-3'>
                                      <ISearch
                                        placeholder='nhập từ khóa'
                                        style={{ width: 250 }}
                                        icon={
                                          <div
                                            style={{
                                              height: 42,
                                              display: 'flex',
                                              alignItems: 'center'
                                            }}
                                          >
                                            <img
                                              src={images.icSearch}
                                              style={{ width: 25, height: 25 }}
                                            />
                                          </div>
                                        }
                                      />
                                    </Row>
                                    <Row>
                                      <ITable
                                        data={this.state.data}
                                        columns={columnsType}
                                        style={{ width: '100%' }}
                                        defaultCurrent={1}
                                        bodyStyle={{ minHeight: '24vh' }}
                                        rowSelection={rowSelection}
                                        // scroll={{y: 300}}
                                        hidePage
                                        loading={this.state.loadingTable}
                                        footer={null}
                                      />
                                    </Row>
                                    <Row className='float-right' style={{}}>
                                      <IButtonRight
                                        className='ml-3 mt-3'
                                        title='Thêm'
                                        icon={ISvg.NAME.ARROWRIGHT}
                                        color={colors.main}
                                        style={{ marginRight: 20 }}
                                        ARROWRIGHT={this.state.arrow}
                                        onClick={() => this.handleAdd()}
                                      />
                                    </Row>
                                  </Col>
                                  <Col sm={8} style={{}}>
                                    <ITable
                                      data={this.state.data}
                                      columns={columns}
                                      style={{ width: '100%' }}
                                      defaultCurrent={1}
                                      bodyStyle={{ minHeight: '43vh' }}
                                      // hidePage={this.state.notPage}
                                      loading={this.state.loadingTable}
                                      footer={null}
                                    />
                                  </Col>
                                </Row>
                              </Modal>
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row className='p-0 mt-4 mb-4 ml-0 mr-0'>
                    <ITitle
                      level={4}
                      title='Giới hạn mua'
                      style={{ color: colors.black, fontWeight: 'bold' }}
                    />
                  </Row>
                  <Row className='p-0 mt-2 ml-0 mb-0 mr-0' xs='auto'>
                    <Form.Item>
                      {getFieldDecorator('priceMin', {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: true,
                            message: 'nhập giá tiền mỗi đơn vị'
                          }
                        ]
                      })(
                        <div>
                          <Row className='p-0 m-0'>
                            <Col className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Tối đa'}
                                style={{ fontWeight: 700 }}
                              />
                            </Col>
                            <Col className='p-0 m-0'>
                              <IInput
                                style={{
                                  borderWidth: 0
                                }}
                                placeholder='0đ'
                              />
                            </Col>
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Row>
                  <Row className='p-0 mt-2 ml-0 mb-0 mr-0' xs='auto'>
                    <Form.Item>
                      {getFieldDecorator('priceMin', {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: true,
                            message: 'nhập giá tiền mỗi đơn vị'
                          }
                        ]
                      })(
                        <div>
                          <Row className='p-0 m-0'>
                            <Col className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Tối thiểu'}
                                style={{ fontWeight: 700 }}
                              />
                            </Col>
                            <Col className='p-0 m-0'>
                              <IInput placeholder='0đ' />
                            </Col>
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Row>
                </Col>
                <Col className='m-0 p-0 ' xs='auto'>
                  <div
                    style={{
                      width: 4,
                      height: '100%',
                      background: colors.background
                    }}
                  ></div>
                </Col>
                <Col
                  className='pl-5 pr-0 pt-0 m-0'
                  style={{
                    paddingBottom: 40
                  }}
                  xs={4}
                >
                  <Row className='p-0 mt-4 mb-4 ml-0 mr-0'>
                    <ITitle
                      level={2}
                      title='Giá bán'
                      style={{ color: colors.black, fontWeight: 'bold' }}
                    />
                  </Row>
                  <Row className='p-0 m-0'>
                    <Col className='ml-0 pl-0'>
                      <Form.Item>
                        {getFieldDecorator('priceMin', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'Chọn phần thưởng'
                            }
                          ]
                        })(
                          <Col className='p-0 m-0'>
                            <Row className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Phần thưởng'}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className='p-0 m-0'>
                              <ISelectStatus placeholder='chọn phần thưởng' />
                            </Row>
                          </Col>
                        )}
                      </Form.Item>

                      <Form.Item>
                        {getFieldDecorator('priceMax', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'nhập giá tiền thùng/kiện'
                            }
                          ]
                        })(
                          <Col className='p-0 mt-2 ml-0 mb-0 mr-0'>
                            <Row className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Giá theo thùng/kiện'}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className='p-0 m-0'>
                              <ISelectStatus placeholder='danh sách phần thưởng' />
                            </Row>
                          </Col>
                        )}
                      </Form.Item>

                      <Form.Item>
                        {getFieldDecorator('distributorOffer', {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: 'nhập tên cửa hàng đại lý'
                            }
                          ]
                        })(
                          <Col
                            className='p-0 mt-2 ml-0 mb-0 mr-0'
                            style={{ marginTop: 20, marginBottom: 20 }}
                          >
                            <Row className='p-0 m-0'>
                              <ITitle
                                level={4}
                                title={'Chiết khấu đại lý'}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className='p-0 m-0'>
                              <IInput
                                type='number'
                                placeholder={'nhập phần trăm chiết khấu'}
                              />
                            </Row>
                          </Col>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>

            <Col
              style={{ background: colors.white, maxWidth: 575, minWidth: 300 }}
              className='pl-5 ml-3 mr-3 pr-5 shadow'
            >
              <Row className='p-0 m-0'>
                <Col
                  className='pl-0 pr-0 pt-0 m-0'
                  style={{ paddingBottom: 40 }}
                >
                  <Row className='p-0 mt-4 mb-4 ml-0 mr-0'>
                    <ITitle
                      level={2}
                      title='Thông tin chương trình'
                      style={{ color: colors.black, fontWeight: 'bold' }}
                    />
                  </Row>
                  <Form.Item>
                    {getFieldDecorator('uses', {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: 'nhập công dụng sản phẩm'
                        }
                      ]
                    })(
                      <Row className='p-0 m-0' xs='auto'>
                        <Col className='p-0 m-0'>
                          <Row className='p-0 m-0'>
                            <ITitle
                              level={4}
                              title={'Mã chương tình'}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className='p-0 m-0'>
                            <ISelectStatus placeholder='chọn mã chương trình' />
                          </Row>
                        </Col>
                      </Row>
                    )}
                  </Form.Item>
                  <Form.Item>
                    {getFieldDecorator('uses', {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: 'nhập công dụng sản phẩm'
                        }
                      ]
                    })(
                      <Row className='p-0 m-0' xs='auto'>
                        <Col className='p-0 m-0'>
                          <Row className='p-0 m-0'>
                            <ITitle
                              level={4}
                              title={'Trạng thái'}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className='p-0 m-0'>
                            <ISelectStatus
                              style={{ marginTop: 10 }}
                              placeholder='chọn trạng thái chương trình'
                            />
                          </Row>
                        </Col>
                      </Row>
                    )}
                  </Form.Item>

                  <Form.Item>
                    {getFieldDecorator('uses', {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: 'nhập công dụng sản phẩm'
                        }
                      ]
                    })(
                      <Row className='p-0 m-0' xs='auto'>
                        <Col className='p-0 m-0'>
                          <Row className='p-0 m-0'>
                            <ITitle
                              level={4}
                              title={'Tiêu đề'}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className='p-0 m-0'>
                            <IInput
                              style={{ marginTop: 10 }}
                              placeholder='nhập tên chương trình'
                            />
                          </Row>
                        </Col>
                      </Row>
                    )}
                  </Form.Item>

                  <Form.Item>
                    {getFieldDecorator('element', {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: 'nhập thành phần sản phẩm'
                        }
                      ]
                    })(
                      <Row
                        className='p-0'
                        style={{
                          marginTop: 20,
                          marginLeft: 0,
                          marginBottom: 0,
                          marginRight: 0
                        }}
                        xs='auto'
                      >
                        <Col className='p-0 m-0'>
                          <Row className='p-0 m-0'>
                            <ITitle
                              level={4}
                              title={'Nội dung'}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className='p-0 m-0'>
                            <IInput
                              loai='textArea'
                              style={{ height: 140, marginTop: 10 }}
                              placeholder='nhập nội dung và thể lệ chương trình'
                            />
                          </Row>
                        </Col>
                      </Row>
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Col>

            <Col
              style={{ background: colors.white }}
              className='pl-4 ml-3 mr-3'
              xs='auto'
            >
              <Row className='p-0 m-0' style={{ width: 250 }}>
                <Col
                  className='pl-0 pr-0 pt-0 m-0'
                  style={{ paddingBottom: 40 }}
                >
                  <Row className='p-0 mt-4 mb-3 ml-0 mr-0'>
                    <ITitle
                      level={2}
                      title='Thời hạn chương trình'
                      style={{ color: colors.black, fontWeight: 'bold' }}
                    />
                  </Row>

                  <Form.Item>
                    {/* {getFieldDecorator("avatar", {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: "chọn hình",
                          valuePropName: "fileList",
                          getValueFromEvent: this.normFile
                        }
                      ]
                    })(
                       */}
                    {getFieldDecorator('priceMin', {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: 'Chọn ngày bắt đầu'
                        }
                      ]
                    })(
                      <div>
                        <Row className='p-0 mt-1 mb-1 ml-0 mr-0'>
                          <ITitle
                            level={4}
                            title={'Bắt đầu'}
                            style={{ fontWeight: 700 }}
                          />
                        </Row>
                        <Row className='p-0 m-0'>
                          <DatePicker
                            defaultValue={moment('2015/01/01', dateFormat)}
                            format={dateFormat}
                          />
                        </Row>
                      </div>
                    )}
                  </Form.Item>
                  <Form.Item>
                    {/* {getFieldDecorator("avatar", {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: "chọn hình",
                          valuePropName: "fileList",
                          getValueFromEvent: this.normFile
                        }
                      ]
                    })( */}
                    {getFieldDecorator('priceMin', {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: 'Chọn ngày kết thúc'
                        }
                      ]
                    })(
                      <div>
                        <Row className='p-0 mt-1 mb-1 ml-0 mr-0'>
                          <ITitle
                            level={4}
                            title={'Kết thúc'}
                            style={{ fontWeight: 700 }}
                          />
                        </Row>
                        <Row className='p-0 m-0'>
                          <DatePicker
                            defaultValue={moment('2015/01/01', dateFormat)}
                            format={dateFormat}
                          />
                        </Row>
                      </div>
                    )}
                  </Form.Item>
                  <Form.Item>
                    {/* {getFieldDecorator("avatar", {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          // required: true,
                          message: "chọn hình",
                          valuePropName: "fileList",
                          getValueFromEvent: this.normFile
                        }
                      ]
                    })( */}
                    {getFieldDecorator('priceMin', {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          required: true,
                          message: 'Thêm hình ảnh'
                        }
                      ]
                    })(
                      <div>
                        <Row className='p-0 mt-1 mb-2 ml-0 mr-0'>
                          <ITitle
                            level={4}
                            title={'Hình ảnh'}
                            style={{ fontWeight: 700 }}
                          />
                        </Row>
                        <Row className='p-0 m-0'>
                          <IUpload style={{ width: 300 }} />
                        </Row>
                      </div>
                    )}
                  </Form.Item>
                  <Form.Item>
                    {getFieldDecorator('avatar', {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          // required: true,
                          message: 'chọn loại',
                          valuePropName: 'fileList',
                          getValueFromEvent: this.normFile
                        }
                      ]
                    })(
                      <div>
                        <Row className='p-0 mt-1 mb-3 ml-0 mr-0'>
                          <Col xs={9}>
                            <ITitle
                              level={4}
                              title={'Hiện nút tham gia'}
                              style={{ fontWeight: 700 }}
                            />
                          </Col>
                          <Col>
                            <Checkbox />
                          </Col>
                        </Row>
                      </div>
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      </Container>
    )
  }
}
const editEventPage = Form.create({ name: 'EditEventPage' })(EditEventPage)

export default editEventPage
