import { Col, Row, Form, message, Modal } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../assets";
import { IButton, IDatePicker, ISvg } from "../../components";
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1,
  useModalAgencyS2,
} from "../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../components/common/Font/font";
import { CLEAR_REDUX, CREATE_DATE_EVENT } from "../../store/reducers";
import moment from "moment";
import { DefineKeyEvent } from "../../../utils/DefineKey";
export default function CreateEvent(props) {
  const [isOpenApply, setIsOpenApply] = useState(false);
  const [isOpenPayThereWard, setIsOpenPayThereWard] = useState(false);
  const history = useHistory();
  const params = useParams();
  const { id } = params;
  const format = "HH:mm:ss DD-MM-YYYY";
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const [isVisibleAgency1, setVisibleAgency1] = useState(false);
  const [isVisibleAgency2, setVisibleAgency2] = useState(false);

  const { event } = dataRoot;

  const handleStartOpenChange = (open) => {
    if (!open) {
      setIsOpenApply(true);
    }
  };

  const handleEndOpenChange = (open) => {
    setIsOpenApply(open);
  };

  const handleStartOpenChangePay = (open) => {
    if (!open) {
      setIsOpenPayThereWard(true);
    }
  };

  const disabledStartDateApply = (startValue) => {
    if (!startValue || !event.to_time_apply) {
      return false;
    }
    return startValue.valueOf() > event.to_time_apply;
  };

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !event.from_time_apply) {
      return false;
    }
    return endValue.valueOf() <= event.from_time_apply;
  };

  const disabledStartDatePaythereward = (startValue) => {
    if (!startValue || !event.to_time_paythereward) {
      return false;
    }
    return startValue.valueOf() > event.to_time_paythereward;
  };

  const disabledEndDatePaythereward = (endValue) => {
    if (!endValue || !event.from_time_paythereward) {
      return false;
    }
    return endValue.valueOf() <= event.from_time_paythereward;
  };

  const handleEndOpenChangePay = (open) => {
    setIsOpenPayThereWard(open);
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo chương trình tích lũy mới
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX());
                    history.push("/listEventPage");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1100 }}>
            <Row gutter={[24, 24]}>
              <Col span={14}>
                <div
                  style={{ padding: 24, height: 500 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thời gian áp dụng & trả thưởng
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Thời gian áp dụng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput="0px"
                                disabled={
                                  event.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                placeholder="nhập thời gian bắt đầu"
                                onOpenChange={handleStartOpenChange}
                                format={format}
                                value={
                                  event.from_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(event.from_time_apply),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "from_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_DATE_EVENT(data));
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Kết thúc</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledEndDateApply}
                                isBorderBottom={true}
                                disabled={
                                  event.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                paddingInput="0px"
                                open={isOpenApply}
                                format={format}
                                onOpenChange={handleEndOpenChange}
                                value={
                                  event.to_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(event.to_time_apply),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "to_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_DATE_EVENT(data));
                                }}
                                placeholder="nhập thời gian kết thúc"
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <div>
                        <TagP style={{ fontWeight: 500 }}>
                          Thời gian trả thưởng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledStartDatePaythereward}
                                isBorderBottom={true}
                                format={format}
                                disabled={
                                  event.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                paddingInput="0px"
                                placeholder="nhập thời gian bắt đầu"
                                value={
                                  event.from_time_paythereward === 0
                                    ? undefined
                                    : moment(
                                        new Date(event.from_time_paythereward),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "from_time_paythereward",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_DATE_EVENT(data));
                                }}
                                onOpenChange={handleStartOpenChangePay}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col>
                              <TagP style={{ fontWeight: 500 }}>Đến ngày</TagP>
                            </Col>
                            <Col>
                              <IDatePickerFrom
                                showTime
                                format={format}
                                isBorderBottom={true}
                                disabled={
                                  event.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                disabledDate={disabledEndDatePaythereward}
                                paddingInput="0px"
                                placeholder="nhập thời gian kết thúc"
                                value={
                                  event.to_time_paythereward === 0
                                    ? undefined
                                    : moment(
                                        new Date(event.to_time_paythereward),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "to_time_paythereward",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_DATE_EVENT(data));
                                }}
                                open={isOpenPayThereWard}
                                onOpenChange={handleEndOpenChangePay}
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={10}>
                <div
                  style={{ padding: 24, height: 500 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 24 }}>
                        <Row>
                          <Col span={6}></Col>
                          <Col span={12}>
                            <IButton
                              title="Tạo điều kiện lọc"
                              color={
                                event.type_way ===
                                DefineKeyEvent.event_type_assigned
                                  ? "white"
                                  : colors.main
                              }
                              disabled={
                                event.type_event_status ===
                                DefineKeyEvent.event_running
                              }
                              styleHeight={{
                                background:
                                  event.type_way ===
                                  DefineKeyEvent.event_type_assigned
                                    ? colors.main
                                    : "white",
                              }}
                              onClick={() => {
                                const data = {
                                  key: "type_way",
                                  value: DefineKeyEvent.event_type_assigned,
                                };
                                dispatch(CREATE_DATE_EVENT(data));
                                // khi nhấn vô push qua màn hình filter
                                history.push(`/eventCreate/filter/${id}`);
                              }}
                            />
                          </Col>
                          <Col span={6}></Col>
                        </Row>
                      </div>
                    </Col>
                    <Col span={24}>
                      <IButton
                        title="Chọn theo Danh sách đại lý cấp 1"
                        color={
                          event.type_way === DefineKeyEvent.event_type_S1
                            ? "white"
                            : colors.main
                        }
                        disabled={
                          event.type_event_status ===
                          DefineKeyEvent.event_running
                        }
                        styleHeight={{
                          background:
                            event.type_way === DefineKeyEvent.event_type_S1
                              ? colors.main
                              : "white",
                        }}
                        onClick={() => {
                          const data = {
                            key: "type_way",
                            value: DefineKeyEvent.event_type_S1,
                          };
                          dispatch(CREATE_DATE_EVENT(data));
                          setVisibleAgency1(true);
                        }}
                      />
                    </Col>
                    <Col span={24}>
                      <IButton
                        title="Chọn theo Danh sách đại lý cấp 2"
                        color={
                          event.type_way === DefineKeyEvent.event_type_S2
                            ? "white"
                            : colors.main
                        }
                        disabled={
                          event.type_event_status ===
                          DefineKeyEvent.event_running
                        }
                        styleHeight={{
                          background:
                            event.type_way === DefineKeyEvent.event_type_S2
                              ? colors.main
                              : "white",
                        }}
                        onClick={() => {
                          const data = {
                            key: "type_way",
                            value: DefineKeyEvent.event_type_S2,
                          };
                          dispatch(CREATE_DATE_EVENT(data));
                          setVisibleAgency2(true);
                        }}
                      />
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={() => {
                    let dateNow = new Date();
                    dateNow.setHours(
                      dateNow.getHours() + 1,
                      dateNow.getMinutes() - 5
                    );
                    if (
                      event.from_time_apply === 0 ||
                      event.to_time_apply === 0 ||
                      event.from_time_paythereward === 0 ||
                      event.to_time_paythereward === 0 ||
                      event.type_way === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian áp dụng
                          </span>{" "}
                          & <span style={{ fontWeight: 600 }}>trả thương</span>{" "}
                          ,{" "}
                          <span style={{ fontWeight: 600 }}>
                            đối tượng áp dụng
                          </span>{" "}
                          chương trình
                        </span>
                      );
                      return IError(content);
                    }

                    // if (event.ISedit) {
                    //   if (
                    //     event.type_event_status !== DefineKeyEvent.event_running
                    //   ) {
                    //     if (event.from_time_apply <= dateNow.getTime()) {
                    //       const content = (
                    //         <span>
                    //           Ngày bắt đầu chương trình phải lớn hơn giờ hiện
                    //           tại 1 tiếng.
                    //         </span>
                    //       );
                    //       return IError(content);
                    //     }
                    //   }
                    // } else {
                    //   if (event.from_time_apply <= dateNow.getTime()) {
                    //     const content = (
                    //       <span>
                    //         Ngày bắt đầu chương trình phải lớn hơn giờ hiện tại
                    //         1 tiếng.
                    //       </span>
                    //     );
                    //     return IError(content);
                    //   }
                    // }

                    switch (event.type_way) {
                      case DefineKeyEvent.event_type_assigned:
                        history.push(`/eventCreate/filter/${id}`);
                        break;
                      case DefineKeyEvent.event_type_S1:
                        history.push(`/eventCreate/filter_agency/1/${id}`);
                        break;
                      case DefineKeyEvent.event_type_S2:
                        history.push(`/eventCreate/filter_agency/2/${id}`);
                        break;
                      default:
                        break;
                    }
                  }}
                  title="Tiếp tục"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isVisibleAgency1}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1(
          () => {
            setVisibleAgency1(false);
          },
          () => {
            history.push(`/eventCreate/filter_agency/1/${id}`);
          }
        )}
      </Modal>
      <Modal
        visible={isVisibleAgency2}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS2(
          () => {
            setVisibleAgency2(false);
          },
          () => {
            history.push(`/eventCreate/filter_agency/2/${id}`);
          }
        )}
      </Modal>
    </div>
  );
}
