import React, { useState, useEffect } from "react";
import { Row, Col, message, Skeleton, Empty } from "antd";
import { ISvg, IButton, ITableHtml } from "../../components";
import { colors } from "../../../assets";
import { priceFormat } from "../../../utils";
import FormatterDay from "../../../utils/FormatterDay";
import { APIService } from "../../../services";
import { StyledITitle } from "../../components/common/Font/font";
import { useParams } from "react-router-dom";

function StatisticalEventPage() {
  const params = useParams();
  const { id } = params;
  const formatStringDate = "#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#";
  const [loading, setLoading] = useState(true);
  const [dataDetail, setDataDetail] = useState({ lst_user: [] });

  const _fetchAPIGetStatisticalEvent = async (event_id) => {
    try {
      const data = await APIService._getStatisticalEvent(event_id);
      data.report.lst_user.map((item, index) => {
        item.stt = index + 1;
      });
      console.log("data: ", data);
      setDataDetail(data.report);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    _fetchAPIGetStatisticalEvent(Number(id));
  }, [Number(id)]);

  const bodyTable = (databody) => {
    return databody.length === 0 ? (
      <tr height="300px">
        <td colspan="5">
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      databody.map((item) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {item.stt}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {item.user_name}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {item.dms_code}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {dataDetail.type_promotion_id === 1
              ? priceFormat(item.increment_level + "đ")
              : item.increment_level + " sản phẩm"}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {dataDetail.type_promotion_id === 1
              ? item.discount + "%"
              : dataDetail.event_reward_type === "EVENT_REWARD_DISCOUNT"
              ? item.discount + "%"
              : priceFormat(item.discount) + "đ"}
          </td>
        </tr>
      ))
    );
  };

  const headerTable = (dataHeader) => {
    return (
      <tr className="tr-table scroll">
        {dataHeader.map((item, index) => (
          <th className="th-table" style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };
  let dataHeader = [
    {
      name: "STT",
      align: "left",
    },
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Mức tích lũy hiện tại",
      align: "left",
    },
    {
      name: "Mức chiết khấu áp dụng",
      align: "left",
    },
  ];

  async function _getAPIReportDetailEvent(event_id) {
    try {
      const data = await APIService._getDataReportDetailEvent(event_id);
      window.open(data.file_url + data.file_name);
      message.success("Xuất báo cáo thành công");
    } catch (error) {}
  }

  async function _postAPISyncDataReport(event_id) {
    try {
      const data = await APIService._postSyncDataReportEvent(event_id);
      message.success("Cập nhật báo cáo thành công");
      setLoading(true);
      _fetchAPIGetStatisticalEvent(Number(id));
    } catch (error) {
      setLoading(false);
    }
  }

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      {loading ? (
        <div>
          <Row gutter={[0, 24]}>
            <Col span={24}>
              <div>
                <StyledITitle style={{ color: colors.main }}>
                  Thống kê Chương trình tích lũy
                </StyledITitle>
              </div>
            </Col>
            <Col
              span={24}
              style={{ padding: "18px", marginTop: 24 }}
              className="box-shadow"
            >
              <Skeleton loading={loading} active paragraph={{ rows: 16 }} />
            </Col>
          </Row>
        </div>
      ) : (
        <Row gutter={[0, 24]}>
          <Col span={24}>
            <div>
              <StyledITitle style={{ color: colors.main }}>
                Thống kê Chương trình tích lũy
              </StyledITitle>
            </div>
          </Col>
          <Col span={24}>
            <div className="flex justify-end">
              <IButton
                title="Cập nhật báo cáo"
                color={colors.main}
                icon={false}
                styleHeight={{
                  width: 140,
                }}
                onClick={() => {
                  _postAPISyncDataReport(Number(id));
                }}
              />
              <IButton
                title="Xuất file"
                color={colors.main}
                icon={ISvg.NAME.DOWLOAND}
                styleHeight={{
                  width: 140,
                  marginLeft: 15,
                }}
                onClick={() => {
                  _getAPIReportDetailEvent(Number(id));
                }}
              />
            </div>
          </Col>

          {dataDetail.type_promotion_id === 1 ? (
            <Col span={24}>
              <div style={{ padding: "0px 0px 0px 24px" }}>
                <Row gutter={[12, 12]}>
                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Tên chương trình</p>
                    <span>{dataDetail.event_name}</span>
                  </Col>
                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Trạng thái</p>
                    <span>{dataDetail.status_name}</span>
                  </Col>

                  <Col span={7}>
                    <Row gutter={[0, 0]}>
                      <Col span={24}>
                        <p style={{ fontWeight: 500 }}>Thời gian áp dụng</p>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                            <span>
                              {FormatterDay.dateFormatWithString(
                                dataDetail.start_date,
                                formatStringDate
                              )}
                            </span>
                          </Col>
                          <Col span={12}>
                            <p style={{ fontWeight: 500 }}>Kết thúc</p>
                            <span>
                              {FormatterDay.dateFormatWithString(
                                dataDetail.end_date,
                                formatStringDate
                              )}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={7}>
                    <Row gutter={[0, 0]}>
                      <Col span={24}>
                        <p style={{ fontWeight: 500 }}>Thời gian trả thưởng</p>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                            <span>
                              {FormatterDay.dateFormatWithString(
                                dataDetail.reward_start_date,
                                formatStringDate
                              )}
                            </span>
                          </Col>
                          <Col span={12}>
                            <p style={{ fontWeight: 500 }}>Kết thúc</p>
                            <span>
                              {FormatterDay.dateFormatWithString(
                                dataDetail.reward_end_date,
                                formatStringDate
                              )}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>

                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Mã chương trình</p>
                    <span>{dataDetail.event_code}</span>
                  </Col>
                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Ngành hàng áp dụng</p>
                    <span>{dataDetail.product_type}</span>
                  </Col>
                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Điều kiện tích lũy</p>
                    <span>
                      {dataDetail.type_promotion_id === 1
                        ? "Doanh số sản phẩm"
                        : "Số lượng sản phẩm"}
                    </span>
                  </Col>

                  <Col span={6}>
                    <p style={{ fontWeight: 500 }}>
                      Tổng tích lũy của các đại lý tham gia
                    </p>
                    <span>
                      {dataDetail.type_promotion_id === 1
                        ? priceFormat(dataDetail.total_increment + "đ")
                        : dataDetail.total_increment + " sản phẩm"}
                    </span>
                  </Col>
                </Row>
              </div>
            </Col>
          ) : (
            <Col span={24}>
              <div style={{ padding: "0px 0px 0px 24px" }}>
                <Row gutter={[12, 12]}>
                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Tên chương trình</p>
                    <span>{dataDetail.event_name}</span>
                  </Col>
                  <Col span={3} xxl={4}>
                    <p style={{ fontWeight: 500 }}>Trạng thái</p>
                    <span>{dataDetail.status_name}</span>
                  </Col>
                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Ngành hàng áp dụng</p>
                    <span>{dataDetail.product_type}</span>
                  </Col>
                  <Col span={6}>
                    <p style={{ fontWeight: 500 }}>
                      Tổng tích lũy của các đại lý tham gia
                    </p>
                    <span>
                      {dataDetail.type_promotion_id === 1
                        ? priceFormat(dataDetail.total_increment + "đ")
                        : dataDetail.total_increment + " sản phẩm"}
                    </span>
                  </Col>
                  <Col span={7} xxl={6}>
                    <Row gutter={[0, 0]}>
                      <Col span={24}>
                        <p style={{ fontWeight: 500 }}>Thời gian áp dụng</p>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                            <span>
                              {FormatterDay.dateFormatWithString(
                                dataDetail.start_date,
                                formatStringDate
                              )}
                            </span>
                          </Col>
                          <Col span={12}>
                            <p style={{ fontWeight: 500 }}>Kết thúc</p>
                            <span>
                              {FormatterDay.dateFormatWithString(
                                dataDetail.end_date,
                                formatStringDate
                              )}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Mã chương trình</p>
                    <span>{dataDetail.event_code}</span>
                  </Col>
                  <Col span={3} xxl={4}>
                    <p style={{ fontWeight: 500 }}>Điều kiện tích lũy</p>
                    <span>
                      {dataDetail.type_promotion_id === 1
                        ? "Doanh số sản phẩm"
                        : "Số lượng sản phẩm"}
                    </span>
                  </Col>
                  <Col span={4}>
                    <p style={{ fontWeight: 500 }}>Quy cách tích lũy</p>
                    <span>
                      {dataDetail.attribute_type === 2
                        ? "Quy cách nhỏ"
                        : "Quy cách lớn"}
                    </span>
                  </Col>
                  <Col span={6}>
                    <p style={{ fontWeight: 500 }}>Thưởng chiết khấu</p>
                    <span>
                      {dataDetail.event_reward_type === "EVENT_REWARD_MONEY"
                        ? "đ/ Quy cách tích lũy"
                        : "%/ Doanh số tích lũy"}
                    </span>
                  </Col>

                  <Col span={7} xxl={6}>
                    <Row gutter={[0, 0]}>
                      <Col span={24}>
                        <p style={{ fontWeight: 500 }}>Thời gian trả thưởng</p>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                            <span>
                              {FormatterDay.dateFormatWithString(
                                dataDetail.reward_start_date,
                                formatStringDate
                              )}
                            </span>
                          </Col>
                          <Col span={12}>
                            <p style={{ fontWeight: 500 }}>Kết thúc</p>
                            <span>
                              {FormatterDay.dateFormatWithString(
                                dataDetail.reward_end_date,
                                formatStringDate
                              )}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
            </Col>
          )}
          <Col span={24}>
            <div style={{ padding: "36px 24px" }} className="box-shadow ">
              <ITableHtml
                height="480px"
                childrenHeader={headerTable(dataHeader)}
                childrenBody={bodyTable(dataDetail.lst_user)}
                isBorder={false}
              />
            </div>
          </Col>
        </Row>
      )}
    </div>
  );
}

export default StatisticalEventPage;
