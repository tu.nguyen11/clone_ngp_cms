import { Col, Row, Tag, Popconfirm, Checkbox, InputNumber } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'
import { APIService } from '../../../../services'
import { colors } from '../../../../assets'
import { IButton, ISvg } from '../../../components'
import {
  StyledITileHeading,
  StyledITitle
} from '../../../components/common/Font/font'

import moment from 'moment'
import {
  CREATE_DATE_EVENT_FILTER_IMMEDIATE,
  CREATE_DATE_EVENT_IMMEDIATE,
  CLEAR_REDUX_IMMEDIATE
} from '../../../store/reducers'
import {
  IDatePickerFrom,
  IError,
  StyledISelect,
  TagP
} from '../../../components/common'
import { DefineKeyEvent } from '../../../../utils/DefineKey'

export default function CreateEventFilterImmediate (props) {
  const [isOpenApply, setIsOpenApply] = useState(false)
  const history = useHistory()
  const params = useParams()
  const { id } = params
  const format = 'HH:mm:ss DD-MM-YYYY'

  const dispatch = useDispatch()
  const dataRoot = useSelector(state => state)
  const { eventImmediate } = dataRoot
  const [dropdownRegion, setDropdownRegion] = useState([])
  const [loading, setLoading] = useState(false)
  const [dropdownCity, setDropdownCity] = useState([])
  const [loadingCity, setLoadingCity] = useState(false)

  const [checkCondition1, setCheckCondition1] = useState(false)
  const [checkCondition2, setCheckCondition2] = useState(false)
  const [checkCondition3, setCheckCondition3] = useState(false)

  const handleStartOpenChange = open => {
    if (!open) {
      setIsOpenApply(true)
    }
  }

  const handleEndOpenChange = open => {
    setIsOpenApply(open)
  }

  const disabledStartDateApply = startValue => {
    if (!startValue || !eventImmediate.to_time_apply) {
      return false
    }
    return startValue.valueOf() > eventImmediate.to_time_apply
  }

  const disabledEndDateApply = endValue => {
    if (!endValue || !eventImmediate.from_time_apply) {
      return false
    }
    return endValue.valueOf() <= eventImmediate.from_time_apply
  }

  const _fetchAgencyGetRegion = async () => {
    try {
      const data = await APIService._getAllAgencyGetRegion()
      let dataNew = data.agency.map(item => {
        const key = item.id
        const value = item.name
        return { key, value }
      })
      dataNew.unshift({
        key: 0,
        value: 'Toàn quốc'
      })
      setDropdownRegion(() => dataNew)
      setLoading(false)
    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  const _fetchListCityByRegion = async arrayRegion => {
    try {
      const data = await APIService._getListCityByRegion(arrayRegion)
      let dataNew = data.list_cities.map(item => {
        const key = item.id
        const value = item.name
        const parent = item.parent
        return { key, value, parent }
      })

      let dropdownRegionNew = [...dropdownRegion]
      const isAll = dropdownRegionNew.map(item => item.key).indexOf(0)
      if (isAll >= 0) {
        dropdownRegionNew.splice(0, 1)
        if (
          eventImmediate.event_filter.arrayRegion.length ===
          dropdownRegionNew.length
        ) {
          dataNew.unshift({
            key: 0,
            value: 'Toàn quốc'
          })
        }
      }

      setDropdownCity(dataNew)
      setLoadingCity(false)
    } catch (error) {
      console.log(error)
      setLoadingCity(false)
    }
  }

  useEffect(() => {
    _apiSelectCityRegion()
  }, [])

  const _apiSelectCityRegion = async () => {
    await _fetchAgencyGetRegion()
    await _fetchListCityByRegion(eventImmediate.event_filter.arrayRegion)
  }

  return (
    <div style={{ width: '100%', padding: '18px 0px 0px 0px' }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo chương trình trả thưởng ngay mới
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'flex-end'
                }}
              >
                <IButton
                  title='Hủy'
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_IMMEDIATE())
                    history.push('/event/immediate/list')
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1200 }}>
            <Row gutter={[24, 24]} type='flex'>
              <Col span={6}>
                <div
                  style={{ padding: 24, height: '100%' }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Thiết lập chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Cấp độ Chương trình
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24} style={{ padding: 0 }}>
                      <Checkbox
                        style={{ marginRight: 12 }}
                        checked={
                          eventImmediate.priori ===
                          DefineKeyEvent.priori_prioritize
                        }
                        onChange={e => {
                          const data = {
                            key: 'priori',
                            value: e.target.checked === true ? 1 : 0
                          }
                          dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                        }}
                      />
                      <span>Ưu tiên</span>
                    </Col>
                    {eventImmediate.priori ===
                    DefineKeyEvent.priori_prioritize ? (
                      <Col span={24} style={{ padding: 0 }}>
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center'
                          }}
                        >
                          <span
                            style={{
                              marginLeft: 32,
                              width: 140
                            }}
                          >
                            Độ ưu tiên
                          </span>
                          <InputNumber
                            className='disabled-up'
                            value={eventImmediate.valuePriori}
                            style={{
                              height: 32,
                              width: '100%',
                              borderRadius: 0
                            }}
                            onChange={value => {
                              const data = {
                                key: 'valuePriori',
                                value: value
                              }
                              dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                            }}
                            min={0}
                          />
                        </div>
                      </Col>
                    ) : null}
                  </Row>
                </div>
              </Col>
              <Col span={10}>
                <div
                  style={{ padding: 24, height: '100%' }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Thời gian áp dụng & trả thưởng
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Thời gian áp dụng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput='0px'
                                disabled={
                                  eventImmediate.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                placeholder='nhập thời gian bắt đầu'
                                onOpenChange={handleStartOpenChange}
                                format={format}
                                value={
                                  eventImmediate.from_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediate.from_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={date => {
                                  const data = {
                                    key: 'from_time_apply',
                                    value: date._d.getTime()
                                  }
                                  dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Kết thúc</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledEndDateApply}
                                isBorderBottom={true}
                                disabled={
                                  eventImmediate.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                paddingInput='0px'
                                open={isOpenApply}
                                format={format}
                                onOpenChange={handleEndOpenChange}
                                value={
                                  eventImmediate.to_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(eventImmediate.to_time_apply),
                                        format
                                      )
                                }
                                onChange={date => {
                                  const data = {
                                    key: 'to_time_apply',
                                    value: date._d.getTime()
                                  }
                                  dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                                }}
                                placeholder='nhập thời gian kết thúc'
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>

              <Col span={8}>
                <div
                  style={{ padding: 24, minHeight: 650 }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 24]}>
                    <Col>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          marginTop: 16
                        }}
                      >
                        {checkCondition1 === true ||
                        eventImmediate.event_filter.id_agency === 0 ? (
                          <TagP
                            style={{
                              fontWeight: 500,
                              display: 'flex',
                              alignItems: 'center'
                            }}
                          >
                            <span
                              style={{
                                color: 'red',
                                fontSize: 20,
                                marginRight: 10
                              }}
                            >
                              *
                            </span>
                            <div>Điều kiện tham gia 1</div>
                          </TagP>
                        ) : (
                          <TagP
                            style={{
                              fontWeight: 500,
                              marginLeft: 12
                            }}
                          >
                            <div>Điều kiện tham gia 1</div>
                          </TagP>
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <div
                              style={{
                                padding: 12,
                                border:
                                  checkCondition1 === true
                                    ? '1px solid red'
                                    : '1px solid #d9d9d9'
                              }}
                            >
                              <Col>
                                <div>
                                  <TagP
                                    style={{
                                      borderBottom: '1px solid #d9d9d9',
                                      paddingBottom: 16
                                    }}
                                  >
                                    Cấp người dùng
                                  </TagP>
                                </div>
                              </Col>
                              <Col>
                                <StyledISelect
                                  isBorderBottom='1px solid #d9d9d9'
                                  defaultValue={
                                    eventImmediate.event_filter.id_agency === 0
                                      ? undefined
                                      : eventImmediate.event_filter.id_agency
                                  }
                                  disabled={
                                    eventImmediate.type_event_status ===
                                    DefineKeyEvent.event_running
                                  }
                                  placeholder='Chọn đại lý'
                                  onChange={key => {
                                    const data = {
                                      key: 'id_agency',
                                      value: key
                                    }

                                    dispatch(
                                      CREATE_DATE_EVENT_FILTER_IMMEDIATE(data)
                                    )
                                    setCheckCondition1(false)
                                  }}
                                  data={[
                                    {
                                      value: 'Đại lý cấp 1',
                                      key: DefineKeyEvent.agency_S1
                                    },
                                    {
                                      value: 'Đại lý cấp 2',
                                      key: DefineKeyEvent.agency_S2
                                    }
                                  ]}
                                />
                              </Col>
                            </div>
                          </Row>
                        </Col>
                      </Row>
                    </Col>

                    <Col span={24}>
                      <div>
                        {checkCondition2 === true ||
                        eventImmediate.event_filter.arrayRegion.length === 0 ? (
                          <TagP
                            style={{
                              fontWeight: 500,
                              display: 'flex',
                              alignItems: 'center'
                            }}
                          >
                            <span
                              style={{
                                color: 'red',
                                fontSize: 20,
                                marginRight: 10
                              }}
                            >
                              *
                            </span>
                            <div>Điều kiện tham gia 2</div>
                          </TagP>
                        ) : (
                          <TagP style={{ fontWeight: 500, paddingLeft: 12 }}>
                            Điều kiện tham gia 2
                          </TagP>
                        )}
                      </div>
                    </Col>

                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <div
                              style={{
                                padding: '6px 12px 12px 12px',
                                border:
                                  checkCondition2 === true
                                    ? '1px solid red'
                                    : '1px solid #d9d9d9'
                              }}
                            >
                              <Col>
                                <div
                                  style={{
                                    borderBottom: '1px solid #d9d9d9',
                                    paddingBottom: 6
                                  }}
                                >
                                  <Row>
                                    <Col span={12}>
                                      <span style={{}}>Vùng địa lý</span>
                                    </Col>
                                    <Col span={12}>
                                      <div
                                        style={{
                                          display: 'flex',
                                          justifyContent: 'flex-end'
                                        }}
                                      >
                                        <Popconfirm
                                          placement='bottom'
                                          title={
                                            <span>
                                              Xóa dữ liệu đang chọn <br />
                                              của Vùng địa lý
                                            </span>
                                          }
                                          disabled={
                                            eventImmediate.type_event_status ===
                                            DefineKeyEvent.event_running
                                          }
                                          onConfirm={() => {
                                            const data = {
                                              key: 'arrayCity',
                                              value: []
                                            }

                                            const dataCityClone = {
                                              key: 'arrayCityClone',
                                              value: []
                                            }
                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                                data
                                              )
                                            )
                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                                dataCityClone
                                              )
                                            )
                                            const dataArrRegion = {
                                              key: 'arrayRegion',
                                              value: []
                                            }
                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                                dataArrRegion
                                              )
                                            )
                                            _fetchListCityByRegion([])
                                          }}
                                          okText='Đồng ý'
                                          cancelText='Hủy'
                                        >
                                          <Tag color={colors.main}>Xóa</Tag>
                                        </Popconfirm>
                                      </div>
                                    </Col>
                                  </Row>
                                </div>
                              </Col>
                              <Col>
                                <StyledISelect
                                  disabled={
                                    eventImmediate.type_event_status ===
                                    DefineKeyEvent.event_running
                                  }
                                  mode='multiple'
                                  isBorderBottom='1px solid #d9d9d9'
                                  placeholder='Chọn vùng địa lý'
                                  data={dropdownRegion}
                                  onDropdownVisibleChange={open => {
                                    if (!open) {
                                      if (
                                        eventImmediate.event_filter.arrayRegion
                                          .length === 0
                                      ) {
                                        return
                                      }
                                      setLoadingCity(true)
                                      _fetchListCityByRegion(
                                        eventImmediate.event_filter.arrayRegion
                                      )
                                      return
                                    }
                                  }}
                                  value={
                                    eventImmediate.event_filter.arrayRegion
                                  }
                                  onChange={arrKey => {
                                    const isAll = arrKey.indexOf(0)
                                    if (isAll >= 0) {
                                      let arrayAddress = [...dropdownRegion]
                                      arrayAddress.splice(0, 1)
                                      let idArray = arrayAddress.map(
                                        item => item.key
                                      )
                                      const data = {
                                        key: 'arrayRegion',
                                        value: [...idArray]
                                      }
                                      let arrCity = eventImmediate.event_filter.arrayCityClone.filter(
                                        item => {
                                          return idArray.find(item1 => {
                                            return item1 === item.parent
                                          })
                                        }
                                      )

                                      let arrCityShow = arrCity.map(
                                        item => item.key
                                      )
                                      const dataCityRemove = {
                                        key: 'arrayCityClone',
                                        value: [...arrCity]
                                      }
                                      const dataCityShow = {
                                        key: 'arrayCity',
                                        value: [...arrCityShow]
                                      }
                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                          dataCityRemove
                                        )
                                      )
                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                          dataCityShow
                                        )
                                      )

                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(data)
                                      )
                                      setLoadingCity(true)
                                      _fetchListCityByRegion([])
                                      setCheckCondition2(false)
                                    } else {
                                      const data = {
                                        key: 'arrayRegion',
                                        value: [...arrKey]
                                      }
                                      let arrCity = eventImmediate.event_filter.arrayCityClone.filter(
                                        item => {
                                          return arrKey.find(item1 => {
                                            return item1 === item.parent
                                          })
                                        }
                                      )

                                      let arrCityShow = arrCity.map(
                                        item => item.key
                                      )
                                      const dataCityRemove = {
                                        key: 'arrayCityClone',
                                        value: [...arrCity]
                                      }
                                      const dataCityShow = {
                                        key: 'arrayCity',
                                        value: [...arrCityShow]
                                      }
                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                          dataCityRemove
                                        )
                                      )
                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                          dataCityShow
                                        )
                                      )

                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(data)
                                      )
                                      setLoadingCity(true)
                                      _fetchListCityByRegion(arrKey)
                                      setCheckCondition2(false)
                                    }
                                  }}
                                />
                              </Col>
                            </div>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <div>
                        {checkCondition3 === true ||
                        eventImmediate.event_filter.arrayCity.length === 0 ? (
                          <TagP
                            style={{
                              fontWeight: 500,
                              display: 'flex',
                              textAlign: 'center'
                            }}
                          >
                            <span
                              style={{
                                color: 'red',
                                fontSize: 20,
                                marginRight: 10
                              }}
                            >
                              *
                            </span>
                            <div>Điều kiện tham gia 3</div>
                          </TagP>
                        ) : (
                          <TagP style={{ fontWeight: 500, paddingLeft: 12 }}>
                            Điều kiện tham gia 3
                          </TagP>
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <div
                              style={{
                                padding: 12,
                                border: checkCondition3
                                  ? '1px solid red'
                                  : '1px solid #d9d9d9'
                              }}
                            >
                              <Col>
                                <div
                                  style={{
                                    borderBottom: '1px solid #d9d9d9',
                                    paddingBottom: 6
                                  }}
                                >
                                  <Row>
                                    <Col span={12}>
                                      <span style={{}}>Tỉnh / Thành phố</span>
                                    </Col>
                                    <Col span={12}>
                                      <div
                                        style={{
                                          display: 'flex',
                                          justifyContent: 'flex-end'
                                        }}
                                      >
                                        <Popconfirm
                                          placement='bottom'
                                          title={
                                            <span>
                                              Xóa dữ liệu đang chọn <br />
                                              của Tỉnh / Thành phố
                                            </span>
                                          }
                                          disabled={
                                            eventImmediate.type_event_status ===
                                            DefineKeyEvent.event_running
                                          }
                                          onConfirm={() => {
                                            const data = {
                                              key: 'arrayCity',
                                              value: []
                                            }

                                            const dataCityClone = {
                                              key: 'arrayCityClone',
                                              value: []
                                            }
                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                                data
                                              )
                                            )
                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                                dataCityClone
                                              )
                                            )
                                          }}
                                          okText='Đồng ý'
                                          cancelText='Hủy'
                                        >
                                          <Tag color={colors.main}>Xóa</Tag>
                                        </Popconfirm>
                                      </div>
                                    </Col>
                                  </Row>
                                </div>
                              </Col>

                              <Col>
                                <StyledISelect
                                  mode='multiple'
                                  isBorderBottom='1px solid #d9d9d9'
                                  placeholder='Chọn tỉnh/ thành phố'
                                  style={{ maxHeight: 100, overflow: 'auto' }}
                                  disabled={
                                    eventImmediate.event_filter.arrayRegion
                                      .length === 0 ||
                                    eventImmediate.type_event_status ===
                                      DefineKeyEvent.event_running
                                  }
                                  loading={loadingCity}
                                  onChange={arrKey => {
                                    const isAll = arrKey.indexOf(0)
                                    if (isAll >= 0) {
                                      let arrayCity = [...dropdownCity]
                                      arrayCity.splice(0, 1)
                                      let arrKeyID = [...arrayCity]
                                      let arrID = arrKeyID.map(item => item.key)
                                      const data = {
                                        key: 'arrayCity',
                                        value: [...arrID]
                                      }

                                      const dataCityClone = {
                                        key: 'arrayCityClone',
                                        value: [...arrKeyID]
                                      }
                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(data)
                                      )
                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                          dataCityClone
                                        )
                                      )

                                      setCheckCondition3(false)
                                    } else {
                                      let arrKeyID = arrKey.map(item => {
                                        return dropdownCity.find(item1 => {
                                          return item1.key === item
                                        })
                                      })
                                      const data = {
                                        key: 'arrayCity',
                                        value: [...arrKey]
                                      }

                                      const dataCityClone = {
                                        key: 'arrayCityClone',
                                        value: [...arrKeyID]
                                      }
                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(data)
                                      )
                                      dispatch(
                                        CREATE_DATE_EVENT_FILTER_IMMEDIATE(
                                          dataCityClone
                                        )
                                      )

                                      setCheckCondition3(false)
                                    }
                                  }}
                                  onDropdownVisibleChange={open => {
                                    if (open) {
                                      setLoadingCity(true)
                                      _fetchListCityByRegion(
                                        eventImmediate.event_filter.arrayRegion
                                      )
                                      return
                                    }
                                  }}
                                  value={eventImmediate.event_filter.arrayCity}
                                  data={dropdownCity}
                                />
                              </Col>
                            </div>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: 'flex', justifyContent: 'flex-end' }}
                      >
                        <IButton
                          title='Hủy'
                          color={colors.oranges}
                          icon={ISvg.NAME.CROSS}
                          styleHeight={{
                            width: 140
                          }}
                          disabled={
                            eventImmediate.type_event_status ===
                            DefineKeyEvent.event_running
                          }
                          onClick={() => {
                            // clear data
                            const data = {
                              key: 'type_way',
                              value: 0
                            }
                            const data1 = {
                              key: 'id_agency',
                              value: 0
                            }
                            const data2 = {
                              key: 'arrayRegion',
                              value: []
                            }
                            const data3 = {
                              key: 'arrayCity',
                              value: []
                            }
                            const dataCityClone = {
                              key: 'arrayCityClone',
                              value: []
                            }
                            dispatch(
                              CREATE_DATE_EVENT_FILTER_IMMEDIATE(dataCityClone)
                            )
                            dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE(data3))
                            dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE(data2))
                            dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE(data1))
                            dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                            history.goBack()
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <IButton
                  onClick={async () => {
                    let dateNow = new Date()
                    dateNow.setHours(
                      dateNow.getHours() + 1,
                      dateNow.getMinutes() - 5
                    )

                    if (eventImmediate.event_filter.id_agency === 0) {
                      setCheckCondition1(true)
                    }
                    if (eventImmediate.event_filter.arrayRegion.length === 0) {
                      setCheckCondition2(true)
                    }
                    if (eventImmediate.event_filter.arrayCity.length === 0) {
                      setCheckCondition3(true)
                    }
                    if (
                      eventImmediate.from_time_apply === 0 ||
                      eventImmediate.to_time_apply === 0 ||
                      eventImmediate.from_time_paythereward === 0 ||
                      eventImmediate.to_time_paythereward === 0 ||
                      eventImmediate.event_filter.arrayRegion.length === 0 ||
                      eventImmediate.event_filter.arrayCity.length === 0 ||
                      eventImmediate.event_filter.id_agency === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{' '}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian áp dụng
                          </span>{' '}
                          & <span style={{ fontWeight: 600 }}>trả thương</span>{' '}
                          ,{' '}
                          <span style={{ fontWeight: 600 }}>các điều kiện</span>{' '}
                          tham gia chương trình
                        </span>
                      )
                      return IError(content)
                    }
                    if (eventImmediate.priori === 1) {
                      try {
                        const data = await APIService._getCheckExistPriotyInEvent(
                          eventImmediate.valuePriori,
                          eventImmediate.event_filter.id_agency
                        )
                        if (data.exist_index === 0) {
                          return IError('Độ ưu tiên đã tồn tại')
                        }
                      } catch (error) {
                        console.log(error)
                      }
                    }

                    // if (event.ISedit) {
                    //   if (
                    //     event.type_event_status !== DefineKeyEvent.event_running
                    //   ) {
                    //     if (event.from_time_apply <= dateNow.getTime()) {
                    //       const content = (
                    //         <span>
                    //           Ngày bắt đầu chương trình phải lớn hơn giờ hiện
                    //           tại 1 tiếng.
                    //         </span>
                    //       );
                    //       return IError(content);
                    //     }
                    //   }
                    // } else {
                    //   if (event.from_time_apply <= dateNow.getTime()) {
                    //     const content = (
                    //       <span>
                    //         Ngày bắt đầu chương trình phải lớn hơn giờ hiện tại
                    //         1 tiếng.
                    //       </span>
                    //     );
                    //     return IError(content);
                    //   }
                    // }

                    history.push(`/event/immediate/create/desgin/${id}`)
                  }}
                  title='Tiếp tục'
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
    </div>
  )
}
