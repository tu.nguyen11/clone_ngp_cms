import { BackTop, Col, Row, Skeleton, Popconfirm, Modal } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'
import { colors } from '../../../../assets'
import { APIService } from '../../../../services'
import { IButton, ISvg, ITableHtml } from '../../../components'
import FormatterDay from '../../../../utils/FormatterDay'
import {
  StyledITileHeading,
  StyledITitle
} from '../../../components/common/Font/font'
import { priceFormat } from '../../../../utils'

export default function DetailEventImmediatePage () {
  const history = useHistory()
  const params = useParams()
  const [urlIMG, setUrlIMG] = useState('')
  const [dataDetail, setDataDetail] = useState({
    list_application_object: [],
    list_cate_type: [],
    list_product_condition: [],
    lst_gift_reward_response: [],
    lst_region: [],
    lst_city: []
  })
  const [loading, setLoading] = useState(true)
  const [loadingRemove, setLoadingRemove] = useState(false)
  const { id, type } = params
  const _fetchAPIDetailEventImmediate = async id => {
    try {
      let data = await APIService._getDetailEventImmediate(id)
      data.event_response.image_url = data.image_url
      data.event_response.type_event_status = Number(type)
      setDataDetail(data.event_response)
      setUrlIMG(data.image_url)
      setLoading(false)
    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  const postStopEventImmediate = async arr => {
    try {
      let data = await APIService._postStopEventImmediate(arr)
      setLoadingRemove(false)
      Modal.success({
        content: 'Dừng chương trình thành công',
        onOk: () => history.goBack()
      })
    } catch (error) {
      console.log(error)
      setLoadingRemove(false)
    }
  }

  useEffect(() => {
    _fetchAPIDetailEventImmediate(id)
  }, [])

  let dataHeader = [
    {
      name: 'Tên đại lý',
      align: 'left'
    },
    {
      name: 'Mã đại lý',
      align: 'left'
    },
    {
      name: 'Cấp đại lý',
      align: 'center'
    }
  ]

  const headerTable = dataHeader => {
    return (
      <tr className='tr-table scroll'>
        {dataHeader.map((item, index) => (
          <th className='th-table' style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    )
  }

  const bodyTable = databody => {
    return databody.map(item => (
      <tr className='tr-table'>
        <td
          className='td-table'
          style={{ textAlign: 'left', fontWeight: 'normal' }}
        >
          {item.user_agency_name}
        </td>
        <td
          className='td-table'
          style={{ textAlign: 'left', fontWeight: 'normal' }}
        >
          {item.user_agency_code}
        </td>
        <td
          className='td-table'
          style={{
            textAlign: 'center',
            fontWeight: 'normal',
            width: 80
          }}
        >
          {item.level}
        </td>
      </tr>
    ))
  }

  const _renderUIObjectGreedAssign = () => {
    return (
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 1</p>
          <div
            style={{
              border: '1px solid rgba(122, 123, 123, 0.5)',
              padding: 6,
              marginTop: 12
            }}
          >
            <p
              style={{
                borderBottom: '1px solid rgba(122, 123, 123, 0.5)',
                paddingBottom: 6
              }}
            >
              Cấp người dùng
            </p>
            <p style={{ padding: '6px 0px' }}>{dataDetail.level}</p>
          </div>
        </Col>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 2</p>
          <div
            style={{
              border: '1px solid rgba(122, 123, 123, 0.5)',
              padding: 6,
              marginTop: 12
            }}
          >
            <p
              style={{
                borderBottom: '1px solid rgba(122, 123, 123, 0.5)',
                paddingBottom: 6
              }}
            >
              Vùng địa lý
            </p>
            <p style={{ padding: '6px 0px' }}>
              {dataDetail.lst_region.map(item => {
                if (dataDetail.lst_region.length === 1) {
                  return <span>{item.name}</span>
                }
                return <span>{item.name + ', '}</span>
              })}
            </p>
          </div>
        </Col>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 3</p>
          <div
            style={{
              border: '1px solid rgba(122, 123, 123, 0.5)',
              padding: 6,
              marginTop: 12
            }}
          >
            <p
              style={{
                borderBottom: '1px solid rgba(122, 123, 123, 0.5)',
                paddingBottom: 6
              }}
            >
              Tỉnh / Thành phố
            </p>
            <p style={{ padding: '6px 0px', maxHeight: 100, overflow: 'auto' }}>
              {dataDetail.lst_city.map(item => {
                if (dataDetail.lst_city.length === 1) {
                  return <span>{item.name}</span>
                }
                return <span>{item.name + ', '}</span>
              })}
            </p>
          </div>
        </Col>
      </Row>
    )
  }

  const treeProduct = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: 'none',
                  padding: 0
                }}
              >
                <li
                  style={{
                    fontWeight: 'bold',
                    borderBottom: '0.5px dashed #7A7B7B',
                    padding: '6px 0px'
                  }}
                >
                  {item.name}
                </li>
                {treeProduct(item.children)}
              </ul>
            )
          } else {
            return (
              <ul
                style={{
                  listStyleType: 'none',
                  borderBottom: '0.5px dashed #7A7B7B'
                }}
              >
                <li style={{ padding: '6px 6px' }}>
                  <Row gutter={[12, 0]} type='flex'>
                    <Col span={20}>
                      <div
                        style={{
                          wordWrap: 'break-word',
                          wordBreak: 'break-word',
                          overflow: 'hidden',
                          height: '100%',
                          display: 'flex',
                          alignItems: 'center'
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            )
          }
        })}
      </ul>
    )
  }

  const treeProductGift = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: 'none',
                  padding: 0
                }}
              >
                <li
                  style={{
                    fontWeight: 'bold',
                    borderBottom: '0.5px dashed #7A7B7B',
                    padding: '6px 0px'
                  }}
                >
                  {item.name}
                </li>
                {treeProductGift(item.children)}
              </ul>
            )
          } else {
            return (
              <ul
                style={{
                  listStyleType: 'none',
                  borderBottom: '0.5px dashed #7A7B7B'
                }}
              >
                <li style={{ padding: '6px 0' }}>
                  <Row gutter={[12, 0]} type='flex'>
                    <Col span={20}>
                      <div
                        style={{
                          wordWrap: 'break-word',
                          wordBreak: 'break-word',
                          overflow: 'hidden',
                          height: '100%',
                          display: 'flex',
                          alignItems: 'center'
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    <Col span={4}>
                      <div
                        style={{
                          wordWrap: 'break-word',
                          wordBreak: 'break-word',
                          overflow: 'hidden',
                          height: '100%',
                          display: 'flex',
                          alignItems: 'center'
                        }}
                      >
                        <span>{item.quantity}</span>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            )
          }
        })}
      </ul>
    )
  }

  return (
    <div style={{ width: '100%', padding: '18px 0px 0px 0px' }}>
      <BackTop />
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Chi tiết Chương trình trả ngay
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'flex-end'
                }}
              >
                {/* <Link to={'/event/immediate/statistical/' + id}>
                  <IButton
                    title='Xem thống kê'
                    color={colors.main}
                    style={{ marginRight: 24 }}
                    styleHeight={{
                      width: 140
                    }}
                  />
                </Link> */}
                {/* {Number(type) === DefineKeyEvent.event_end ? null : (
                  <IButton
                    title='Chỉnh sửa'
                    color={colors.main}
                    icon={ISvg.NAME.WRITE}
                    styleHeight={{
                      width: 140
                    }}
                    onClick={() => {
                      dispatch(ASSIGNED_DATA_REDUX_DETAIL(dataDetail))
                      history.push(`/eventCreate/edit/${id}`)
                    }}
                  />
                )} */}
                {dataDetail.status === 2 ? null : (
                  <Popconfirm
                    title={
                      <span>
                        Bạn có muốn tạm dừng chương <br />
                        trình lại không?
                      </span>
                    }
                    placement='bottom'
                    onConfirm={() => {
                      const data = [id]
                      setLoadingRemove(true)
                      postStopEventImmediate(data)
                    }}
                  >
                    <IButton
                      title='Dừng chương trình'
                      color={colors.oranges}
                      icon={ISvg.NAME.CROSS}
                      styleHeight={{
                        width: 200,
                        marginLeft: 24
                      }}
                      loading={loadingRemove}
                    />
                  </Popconfirm>
                )}
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div>
            <Row gutter={[24, 0]}>
              <Col span={16}>
                <div className='box-shadow' style={{ padding: '24px 36px' }}>
                  <Skeleton loading={loading} active paragraph={{ rows: 8 }}>
                    <Row gutter={[24, 0]}>
                      <Col span={11}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Mô tả & nguyên tắc Chương trình
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <p style={{ fontWeight: 500 }}>Mã chương trình</p>
                              <span>{dataDetail.code}</span>
                            </div>
                          </Col>
                          <Col span={24}>
                            <p style={{ fontWeight: 500 }}>Tên chương trình</p>
                            <span>{dataDetail.name}</span>
                          </Col>
                          <Col span={24}>
                            <p style={{ fontWeight: 500 }}>
                              Trạng thái chương trình
                            </p>
                            <span>{dataDetail.status_name}</span>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[24, 0]}>
                              <Col span={12}>
                                <p style={{ fontWeight: 500 }}>
                                  Loại chương trình
                                </p>
                                <span>{dataDetail.type_name}</span>
                              </Col>
                              <Col span={12}>
                                <p style={{ fontWeight: 500 }}>
                                  Cấp độ ưu tiên
                                </p>
                                <span>
                                  {dataDetail.sort_order == 0
                                    ? '-'
                                    : dataDetail.sort_order}
                                </span>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[24, 0]}>
                              <Col span={12}>
                                <p style={{ fontWeight: 500 }}>
                                  Điều kiện nhận thưởng
                                </p>
                                <span>{dataDetail.type_promotion}</span>
                              </Col>
                              <Col span={12}>
                                <p style={{ fontWeight: 500 }}>
                                  Phần thưởng khuyến mãi
                                </p>
                                <span>{dataDetail.gift_promotion}</span>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={13}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Thời gian áp dụng
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <Row gutter={[24, 0]}>
                                <Col span={12}>
                                  <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                                  <span>
                                    {FormatterDay.dateFormatWithString(
                                      dataDetail.start_date,
                                      '#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#'
                                    )}
                                  </span>
                                </Col>
                                <Col span={12}>
                                  <p style={{ fontWeight: 500 }}>Kết thúc</p>
                                  <span>
                                    {FormatterDay.dateFormatWithString(
                                      dataDetail.end_date,
                                      '#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#'
                                    )}
                                  </span>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <StyledITileHeading minFont='10px' maxFont='16px'>
                                Đối tượng áp dụng
                              </StyledITileHeading>
                            </div>
                          </Col>
                          <Col span={24}>
                            {dataDetail.type_entry_condition === 1 ? (
                              _renderUIObjectGreedAssign()
                            ) : (
                              <ITableHtml
                                height='420px'
                                childrenBody={bodyTable(
                                  dataDetail.list_application_object
                                )}
                                childrenHeader={headerTable(dataHeader)}
                                style={{
                                  border: '1px solid rgba(122, 123, 123, 0.5)',
                                  padding: '0px 8px '
                                }}
                                isBorder={false}
                              />
                            )}
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
                <div
                  style={{
                    padding: '24px 36px',
                    borderTop: '1px solid rgba(122, 123, 123, 0.5)'
                  }}
                  className='box-shadow'
                >
                  <Skeleton loading={loading} active paragraph={{ rows: 8 }}>
                    <Row gutter={[24, 0]}>
                      <Col span={24}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Thể lệ & chi tiết thiết lập
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Ngành hàng áp dụng
                            </StyledITileHeading>
                            <span>{dataDetail.company_name}</span>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[30, 16]}>
                          <Col span={12}>
                            <Row gutter={[0, 16]}>
                              <Col span={24}>
                                <div style={{ marginTop: 24 }}>
                                  <StyledITileHeading
                                    minFont='10px'
                                    maxFont='16px'
                                  >
                                    Điều kiện nhận thưởng
                                  </StyledITileHeading>
                                </div>
                              </Col>
                              <Col span={24}>
                                <Row gutter={[0, 16]}>
                                  <Col span={24}>
                                    <span style={{ marginLeft: 20 }}>
                                      Điều kiện: {dataDetail.type_promotion}
                                    </span>
                                  </Col>
                                  <Col span={24}>
                                    <div
                                      style={{
                                        border:
                                          '1px solid rgba(122, 123, 123, 0.5)',
                                        height: 350
                                      }}
                                    >
                                      <Row gutter={[0, 10]}>
                                        <Col span={24}>
                                          <div
                                            style={{
                                              overflow: 'auto',
                                              padding: '10px 18px',
                                              height: 310,
                                              borderBottom:
                                                '1px solid rgba(122, 123, 123, 0.5)'
                                            }}
                                          >
                                            {treeProduct(
                                              dataDetail.list_cate_type
                                            )}
                                          </div>
                                        </Col>
                                        <Col span={24}>
                                          <div
                                            style={{
                                              display: 'flex',
                                              justifyContent: 'center',
                                              alignItems: 'center'
                                            }}
                                          >
                                            <span>Số lượng Quy cách nhỏ: </span>
                                            <div
                                              style={{
                                                margin: '0px 6px',
                                                fontWeight: 600
                                              }}
                                            >
                                              {priceFormat(
                                                dataDetail.total_quantity
                                              ) || 0}
                                            </div>
                                            <span>Sản phẩm</span>
                                          </div>
                                        </Col>
                                      </Row>
                                    </div>
                                  </Col>

                                  <Col span={24}>
                                    <div
                                      style={{
                                        padding: '12px'
                                      }}
                                    >
                                      <Row
                                        gutter={[12, 0]}
                                        style={{
                                          display: 'flex'
                                        }}
                                      >
                                        {[
                                          {
                                            key: 1,
                                            value: ' Bất kì'
                                          }
                                        ].map(item => {
                                          return (
                                            <Col span={7}>
                                              {1 === item.key ? (
                                                <div
                                                  style={{
                                                    display: 'flex',
                                                    flexDirection: 'row',
                                                    alignItems: 'center'
                                                  }}
                                                >
                                                  <div
                                                    style={{
                                                      width: 20,
                                                      height: 20,
                                                      borderRadius: 10,
                                                      border: `4px solid ${colors.main}`,
                                                      marginRight: 12
                                                    }}
                                                  />
                                                  <span>{item.value}</span>
                                                </div>
                                              ) : (
                                                <div
                                                  style={{
                                                    display: 'flex',
                                                    flexDirection: 'row',
                                                    alignItems: 'center'
                                                  }}
                                                >
                                                  <div
                                                    style={{
                                                      width: 20,
                                                      height: 20,
                                                      borderRadius: 10,
                                                      border:
                                                        '1px solid rgba(34, 35, 43, 0.5)',
                                                      marginRight: 12
                                                    }}
                                                  />
                                                  <span
                                                    style={{
                                                      color:
                                                        'rgba(34, 35, 43, 0.5)'
                                                    }}
                                                  >
                                                    {item.value}
                                                  </span>
                                                </div>
                                              )}
                                            </Col>
                                          )
                                        })}
                                      </Row>
                                    </div>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={12}>
                            <Row gutter={[0, 16]}>
                              <Col span={24}>
                                <div style={{ marginTop: 24 }}>
                                  <StyledITileHeading
                                    minFont='10px'
                                    maxFont='16px'
                                  >
                                    Phần thưởng khuyến mãi
                                  </StyledITileHeading>
                                </div>
                              </Col>
                              <Col span={24}>
                                <Row gutter={[0, 16]}>
                                  <Col span={24}>
                                    <span style={{ marginLeft: 20 }}>
                                      Quà tặng: {dataDetail.gift_promotion}
                                    </span>
                                  </Col>
                                  <Col span={24}>
                                    <div
                                      style={{
                                        overflow: 'auto',
                                        border:
                                          '1px solid rgba(122, 123, 123, 0.5)',
                                        padding: '10px 18px',
                                        height: 350
                                      }}
                                    >
                                      {treeProductGift(
                                        dataDetail.lst_gift_reward_response
                                      )}
                                    </div>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
              <Col span={8}>
                <div className='box-shadow' style={{ padding: '24px 36px' }}>
                  <Skeleton loading={loading} active paragraph={{ rows: 16 }}>
                    <Row gutter={[24, 0]}>
                      <Col span={24}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Hình ảnh
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <img
                              src={urlIMG + dataDetail.image}
                              alt='hình-event'
                              style={{
                                width: '100%',
                                height: 300,
                                objectFit: 'cover'
                              }}
                            />
                          </Col>

                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Nội dung mô tả
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <span>{dataDetail.description}</span>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  )
}
