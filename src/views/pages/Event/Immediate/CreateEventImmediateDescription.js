import { Col, Row, Form, message, Skeleton } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import { IButton, ISvg } from "../../../components";
import {
  IInputText,
  IUploadHook,
  IFormItem,
  IInputTextArea,
  TagP,
} from "../../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../components/common/Font/font";
import { CLEAR_REDUX_IMMEDIATE } from "../../../store/reducers";

function CreateEventImmediateDescription(props) {
  const history = useHistory();
  const params = useParams();
  const { id } = params;
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const {
    eventImmediate,
    treeProductIImmediate,
    modalS1,
    modalS2,
    treeProductPresentIImmediate,
  } = dataRoot;
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [loading, setLoading] = useState(true);

  const _fetchAPIPostCreateEvent = async (obj) => {
    try {
      const data = await APIService._postCreateEventNow(obj);
      dispatch(CLEAR_REDUX_IMMEDIATE());
      message.success("Tạo chương trình thành công");
      setLoadingSubmit(false);
      history.push("/event/immediate/list");
    } catch (error) {
      setLoadingSubmit(false);
      console.log(error);
    }
  };

  // const _fetchAPIPostUpdateEvent = async (obj) => {
  //   try {
  //     const data = await APIService._postUpdateEventIncrement(obj);
  //     dispatch(CLEAR_REDUX_IMMEDIATE());
  //     message.success("Chỉnh sửa chương trình thành công");
  //     setLoadingSubmit(false);
  //     history.push("/listEventPage");
  //   } catch (error) {
  //     setLoadingSubmit(false);
  //     console.log(error);
  //   }
  // };

  useEffect(() => {
    reduxForm.setFieldsValue(
      {
        content_event: eventImmediate.content_event,
        code_event: eventImmediate.code_event,
        name_event: eventImmediate.name_event,
        img: eventImmediate.img_event,
      },
      () => setLoading(false)
    );
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setLoadingSubmit(true);

        if (eventImmediate.ISedit) {
          // let dataIDProduct = [];
          // treeProduct.listConfirm.map((item) => {
          //   item.children.map((item1) => {
          //     if (typeof item1.code === "undefined") {
          //       const product_id = Number(item1.id.split("-")[1]);
          //       dataIDProduct.push({ product_id: product_id });
          //     } else {
          //       dataIDProduct.push({ product_id: item1.id });
          //     }
          //   });
          // });
          // const dataIDS1 = modalS1.listS1Show.map((item) => {
          //   return item.id;
          // });
          // const dataIDS2 = modalS2.listS2Show.map((item) => {
          //   return item.id;
          // });
          // const dataUpdate = {
          //   company_id: eventImmediate.event_degsin.key_cate,
          //   update_event_id: id,
          //   content: values.content_event,
          //   event_code: values.code_event,
          //   event_name: values.name_event,
          //   event_type: 1,
          //   image: values.img,
          //   level_user: 0,
          //   priori: 0,
          //   value_priori: 0,
          //   start_date: eventImmediate.from_time_apply,
          //   end_date: eventImmediate.to_time_apply,
          //   reward_start_date: event.from_time_paythereward,
          //   reward_end_date: event.to_time_paythereward,
          //   type_entry_condition: event.type_way,
          //   condition: [
          //     {
          //       create_date: event.from_time_apply,
          //       event_id: '0',
          //       level_user: event.event_filter.id_agency,
          //       status: 0,
          //       type: '',
          //       region_id:
          //         event.type_way === DefineKeyEvent.event_type_assigned
          //           ? event.event_filter.arrayRegion
          //           : [0],
          //       user_s1:
          //         event.type_way === DefineKeyEvent.event_type_S1
          //           ? dataIDS1
          //           : [0],
          //       user_s2:
          //         event.type_way === DefineKeyEvent.event_type_S2
          //           ? dataIDS2
          //           : [0],
          //       cities_id: event.event_filter.arrayCity
          //     }
          //   ],
          //   product: dataIDProduct,
          //   reward:
          //     event.key_filter_promotion === DefineKeyEvent.filter_sales
          //       ? event.event_degsin.reward_sales
          //       : event.event_degsin.reward_count
          // }
          // _fetchAPIPostUpdateEvent(dataUpdate)
        } else {
          let productApply = [];
          treeProductIImmediate.listConfirm.map((item) => {
            item.children.map((item1) => {
              const product_id = Number(item1.id.split("-")[1]);
              const condition_apply_total_money = 0;
              const percent_sale = 0;
              const quantity_sale = 0;
              productApply.push({
                product_id: product_id,
                condition_apply_total_money: condition_apply_total_money,
                percent_sale: percent_sale,
                quantity_sale: quantity_sale,
              });
            });
          });

          let product_reward_condition = [];

          treeProductPresentIImmediate.listConfirm.map((item) => {
            item.children.map((item1) => {
              const product_id = Number(item1.id.split("-")[1]);
              const price_attibute = 0;
              const percent_sale = 0;
              const quantity_sale = item1.quantity_product_present;
              const is_gift = item1.sale_type;

              product_reward_condition.push({
                product_id: product_id,
                price_attibute: price_attibute,
                percent_sale: percent_sale,
                quantity_sale: quantity_sale,
                is_gift: is_gift,
              });
            });
          });

          const dataIDS1 = modalS1.listS1Show.map((item) => {
            return item.id;
          });
          const dataIDS2 = modalS2.listS2Show.map((item) => {
            return item.id;
          });

          const dataAdd = {
            applied_condition_type: eventImmediate.event_degsin.key_condition,
            company_id: eventImmediate.event_degsin.key_cate,
            level_user:
              eventImmediate.type_way === 1
                ? eventImmediate.event_filter.id_agency
                : eventImmediate.type_way === 2
                ? 1
                : 2,
            condition: [
              {
                create_date: eventImmediate.from_time_apply,
                event_id: "0",
                level_user:
                  eventImmediate.type_way === 1
                    ? eventImmediate.event_filter.id_agency
                    : eventImmediate.type_way === 2
                    ? 1
                    : 2,
                status: 0,
                type: "",
                region_id:
                  eventImmediate.type_way === DefineKeyEvent.event_type_assigned
                    ? eventImmediate.event_filter.arrayRegion
                    : [0],
                user_s1:
                  eventImmediate.type_way === DefineKeyEvent.event_type_S1
                    ? dataIDS1
                    : [0],
                user_s2:
                  eventImmediate.type_way === DefineKeyEvent.event_type_S2
                    ? dataIDS2
                    : [0],
                cities_id: eventImmediate.event_filter.arrayCity,
              },
            ],
            product_appplied_condition: [
              {
                condition_product_money_min: 0,
                condition_product_quantity_min:
                  eventImmediate.condition_product_quantity_min,
                productApply: [...productApply],
                product_reward_condition: [...product_reward_condition],
              },
            ],
            content: values.content_event,
            event_code: values.code_event,
            event_name: values.name_event,
            end_date: eventImmediate.to_time_apply,
            start_date: eventImmediate.from_time_apply,
            event_condition_type: 1, // bất kì
            event_type: 0,
            image: values.img,
            prioty: eventImmediate.priori,
            reward_condition_type: eventImmediate.event_degsin.key_reward,
            type_entry_condition: eventImmediate.type_way,
            value_prioty: eventImmediate.valuePriori,
            update_event_id: 0,
          };

          _fetchAPIPostCreateEvent(dataAdd);
        }
      }
    });
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Form onSubmit={(event) => handleSubmit(event)}>
        <Row gutter={[0, 24]} type="flex" style={{ height: "100%" }}>
          <Col span={24}>
            <div>
              <StyledITitle style={{ color: colors.main }}>
                Thông tin chương trình
              </StyledITitle>
            </div>
          </Col>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Hủy"
                    color={colors.oranges}
                    icon={ISvg.NAME.CROSS}
                    styleHeight={{ width: 140 }}
                    onClick={() => {
                      dispatch(CLEAR_REDUX_IMMEDIATE());
                      history.push("/event/immediate/list");
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <div style={{ width: 815, height: "100%" }}>
              <Row gutter={[12, 24]} type="flex" style={{ height: "100%" }}>
                <Col span={15}>
                  <div
                    style={{ padding: 24, height: "100%" }}
                    className="box-shadow"
                  >
                    <Skeleton loading={loading} active paragraph={{ rows: 12 }}>
                      <Row gutter={[0, 24]}>
                        <Col>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Thông tin chương trình
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <div style={{ marginTop: 16 }}>
                                <TagP style={{ fontWeight: 500 }}>
                                  Mã chương trình
                                </TagP>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("code_event", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "nhập mã chương trình",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    disabled={
                                      eventImmediate.type_event_status ===
                                      DefineKeyEvent.event_running
                                    }
                                    placeholder="nhập mã chương trình"
                                    value={reduxForm.getFieldValue(
                                      "code_event"
                                    )}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <div>
                                <TagP style={{ fontWeight: 500 }}>
                                  Tên chương trình
                                </TagP>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("name_event", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "nhập tên chương trình",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    placeholder="nhập tên chương trình"
                                    value={reduxForm.getFieldValue(
                                      "name_event"
                                    )}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <div>
                                <TagP style={{ fontWeight: 500 }}>
                                  Nội dung
                                </TagP>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("content_event", {
                                  rules: [
                                    {
                                      required: true,
                                      message:
                                        "nhập nội dung và thể lệ chương trình",
                                    },
                                  ],
                                })(
                                  <IInputTextArea
                                    placeholder="nhập nội dung và thể lệ chương trình"
                                    style={{
                                      height: 300,
                                      marginTop: 12,
                                    }}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Skeleton>
                  </div>
                </Col>
                <Col span={9}>
                  <div
                    style={{ padding: 24, height: "100%" }}
                    className="box-shadow"
                  >
                    <Skeleton loading={loading} active paragraph={{ rows: 12 }}>
                      <Row gutter={[0, 24]}>
                        <Col>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Hình ảnh
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <IFormItem>
                            {getFieldDecorator("img", {
                              rules: [
                                {
                                  required: true,
                                  message: "chọn hình",
                                },
                              ],
                            })(
                              <IUploadHook
                                callback={(nameFile) => {
                                  reduxForm.setFieldsValue({
                                    img: nameFile,
                                  });
                                }}
                                dataProps={
                                  eventImmediate.ISedit
                                    ? [
                                        {
                                          uid: -1,
                                          name: reduxForm.getFieldValue("img"),
                                          status: "done",
                                          url:
                                            eventImmediate.URl_img +
                                            reduxForm.getFieldValue("img"),
                                        },
                                      ]
                                    : []
                                }
                                width="100%"
                                height="235px"
                              />
                            )}
                          </IFormItem>
                        </Col>
                      </Row>
                    </Skeleton>
                  </div>
                </Col>
              </Row>
              <Col span={24}>
                <div style={{ display: "flex", justifyContent: "flex-end" }}>
                  <IButton
                    title={
                      eventImmediate.ISedit
                        ? "Lưu chương trình"
                        : "Tạo chương trình"
                    }
                    color={colors.main}
                    htmlType="submit"
                    styleHeight={{ width: 140 }}
                    loading={loadingSubmit}
                  />
                </div>
              </Col>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const createEventImmediateDescription = Form.create({
  name: "CreateEventImmediateDescription",
})(CreateEventImmediateDescription);

export default createEventImmediateDescription;
