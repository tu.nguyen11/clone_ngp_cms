import React, { useState, useEffect } from "react";
import { Row, Col, Button, message, Tooltip, Modal } from "antd";
import { useHistory, useParams } from "react-router-dom";

import { useDispatch } from "react-redux";
import { APIService } from "../../../../services";
import {
  ASSIGNED_DATA_REDUX_DETAIL,
  CLEAR_REDUX_IMMEDIATE_NEW,
} from "../../../store/reducers";
import {
  IButton,
  IDatePicker,
  ISearch,
  ISelect,
  ISvg,
  ITable,
  ITitle,
} from "../../../components";
import FormatterDay from "../../../../utils/FormatterDay";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import { colors, images } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { getStatusColor } from "../../../../utils/colors";

function ListEventImmediatePage(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const params = useParams();
  const formatString = "#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#";
  const [productType, setProductType] = useState([]);

  const NewDate = new Date();
  const DateStart = new Date();
  let startValue = DateStart.setMonth(DateStart.getMonth() - 1);
  const [isLoadingEdit, setLoadingEdit] = useState(false);
  const [showPopupActive, setShowPopupActive] = useState(false);
  const [showPopupStop, setShowPopupStop] = useState(false);

  const [filter, setFilter] = useState({
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
    status: 3,
    type: 3,
    key: "",
    page: 1,
    user_type: 1,
    group_id: 0,
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const [loadingTable, setLoadingTable] = useState(true);

  const _fetchAPIGetEvent = async (filter) => {
    try {
      const data = await APIService._getListEventNow(filter);

      data.listEvent.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
          status: item.status,
        });
      });
      setDataTable({ ...data });
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  const getListProductType = async () => {
    try {
      const data = await APIService._getListProductType();
      const newData = data.groups.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      newData.unshift({
        key: 0,
        value: "Tất cả",
      });
      setProductType(newData);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getListProductType();
  }, []);

  useEffect(() => {
    _fetchAPIGetEvent(filter);
  }, [filter]);

  const [dataTable, setDataTable] = useState({
    listEvent: [],
    total: 1,
    size: 10,
  });

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys([...selectedRowKeys]);
    },
    getCheckboxProps: (record) => ({
      disabled: record.disabled,
    }),
  };

  const status = [
    {
      key: 3,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Nháp",
    },
    {
      key: 4,
      value: "Đã kích hoạt",
    },
    {
      key: 1,
      value: "Đang chạy",
    },
    {
      key: 2,
      value: "Kết thúc",
    },
  ];

  const type = [
    {
      key: -1,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Chương trình song song",
    },
    {
      key: 1,
      value: "Chương trình ưu tiên",
    },
  ];

  const renderTitle = (title) => {
    return (
      <div>
        <ITitle title={title} style={{ fontWeight: "bold" }} />
      </div>
    );
  };

  const columns = [
    {
      title: renderTitle("STT"),
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },
    {
      title: renderTitle("Mã chương trình"),
      dataIndex: "code",
      key: "code",
      render: (code) => <span>{code}</span>,
    },
    {
      title: renderTitle("Tên chương trình"),
      dataIndex: "name",
      key: "name",
      render: (name) => <span>{name}</span>,
    },
    {
      title: renderTitle("Trạng thái"),
      dataIndex: "status_name",
      key: "status_name",
      render: (status_name) => <span>{status_name}</span>,
    },
    {
      title: renderTitle("Loại chương trình"),
      dataIndex: "type_name",
      key: "type_name",
      render: (type_name) => <span>Chương trình song song</span>,
    },
    {
      title: renderTitle("Độ ưu tiên"),
      dataIndex: "priority",
      key: "priority",
      render: (priority) => <span>{priority === "" ? "-" : priority}</span>,
    },
    {
      title: (
        <span style={{ fontWeight: "bold" }}>
          Điều kiện
          <br />
          nhận thưởng
        </span>
      ),
      dataIndex: "type_name",
      key: "type_name",
      render: (type_name) => (
        <span>{type_name === null ? "-" : type_name}</span>
      ),
    },
    {
      title: (
        <span style={{ fontWeight: "bold" }}>
          Phần thưởng
          <br />
          khuyến mãi
        </span>
      ),
      dataIndex: "gift_promotion",
      key: "gift_promotion",
      render: (gift_promotion) => (
        <span>{gift_promotion === null ? "-" : gift_promotion}</span>
      ),
    },
    {
      title: renderTitle("Bắt đầu"),
      dataIndex: "start_date",
      key: "start_date",
      render: (start_date) => {
        return (
          <span>
            {FormatterDay.dateFormatWithString(start_date, formatString)}
          </span>
        );
      },
    },
    {
      title: renderTitle("Kết thúc"),
      dataIndex: "end_date",
      key: "end_date",
      render: (end_date) => {
        return (
          <span>
            {FormatterDay.dateFormatWithString(end_date, formatString)}
          </span>
        );
      },
    },

    // {
    //   title: '',
    //   align: 'center',
    //   width: 30,
    //   render: obj => {
    //     const type =
    //       obj.status_name === 'Khởi tạo'
    //         ? DefineKeyEvent.event_initialization
    //         : obj.status_name === 'Đang chạy'
    //         ? DefineKeyEvent.event_running
    //         : DefineKeyEvent.event_end
    //     return obj.status_name === 'Kết thúc' ? null : (
    //       <Button
    //         type='link'
    //         className='cursor scale'
    //         onClick={e => {
    //           e.stopPropagation()
    //           setLoadingTable(true)
    //           _fetchAPIDetailEvent(obj.id, type)
    //         }}
    //       >
    //         <ISvg
    //           name={ISvg.NAME.WRITE}
    //           width={20}
    //           fill={colors.icon.default}
    //         />
    //       </Button>
    //     )
    //   }
    // },
    // {
    //   title: "",

    //   align: "center",
    //   width: 30,
    //   render: (obj) => {
    //     const type =
    //       obj.status_name === "Khởi tạo"
    //         ? DefineKeyEvent.event_initialization
    //         : obj.status_name === "Đang chạy"
    //         ? DefineKeyEvent.event_running
    //         : DefineKeyEvent.event_end;
    //     return (
    //       <Button
    //         type="link"
    //         className="cursor scale"
    //         onClick={(e) => {
    //           e.stopPropagation();
    //           history.push(`/detail/event/immediate/${obj.id}`);
    //         }}
    //       >
    //         <ISvg
    //           name={ISvg.NAME.CHEVRONRIGHT}
    //           width={12}
    //           fill={colors.icon.default}
    //         />
    //       </Button>
    //     );
    //   },
    // },
  ];

  const postActiveOrStopEvent = async (obj) => {
    try {
      const data = await APIService._postActiveOrStopEvent(obj);
      setSelectedRowKeys([]);
      obj.type === 1 ? setShowPopupActive(false) : setShowPopupStop(false);
      message.success(
        obj.type === 1
          ? "Các CTKM bạn vừa chọn sẽ được kích hoạt vào lúc 0:00 ngày hôm sau"
          : "Dừng CTKM thành công"
      );
      _fetchAPIGetEvent(filter);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Danh sách chương trình khuyến mãi trên đơn
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo tên , mã CTKM">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo tên , mã CTKM"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilter({ ...filter, key: e.target.value, page: 1 });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={18}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Lọc danh sách"
                    level={4}
                    style={{
                      marginLeft: 15,
                      marginRight: 15,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      width={250}
                      from={Number(filter.start_date)}
                      to={Number(filter.end_date)}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          start_date: date[0]._d.setHours(0, 0, 0),
                          end_date: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>
                  <div style={{ margin: "0px 15px 0px 15px" }}>
                    <ISelect
                      defaultValue="Trạng thái"
                      data={status}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          type: value,
                          status:
                            value === 1 || value === 2 || value === 4
                              ? 1
                              : value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{ width: 150 }}>
                    <ISelect
                      defaultValue="Loại sản phẩm"
                      data={productType}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          group_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>
              <Col span={6}>
                <Row gutter={[15, 0]}>
                  <div className="flex justify-end">
                    <IButton
                      icon={ISvg.NAME.ARROWUP}
                      title={"Tạo mới"}
                      // style={{ marginRight: 12 }}
                      styleHeight={{
                        width: 120,
                      }}
                      color={colors.main}
                      onClick={() => {
                        dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                        history.push("/create/event/immediate/0");
                      }}
                    />

                    <IButton
                      title={"Kích hoạt CTKM"}
                      color={colors.main}
                      style={{ marginRight: 12, marginLeft: 12 }}
                      styleHeight={{
                        width: 130,
                      }}
                      onClick={() => {
                        if (selectedRowKeys.length > 0) {
                          const arrCheck = selectedRowKeys.filter((item) => {
                            let itemParse = JSON.parse(item);
                            return itemParse.status === 1;
                          });

                          if (arrCheck.length > 0) {
                            message.warning(
                              "Vui lòng chỉ chọn những CTKM ở trạng thái Nháp!"
                            );
                            return;
                          }
                          setShowPopupActive(true);
                        } else {
                          message.warning("Vui lòng chọn CTKM!");
                          return;
                        }
                      }}
                    />

                    <IButton
                      title={"Dừng CTKM"}
                      color={colors.oranges}
                      styleHeight={{
                        width: 120,
                      }}
                      onClick={() => {
                        if (selectedRowKeys.length > 0) {
                          const arrCheck = selectedRowKeys.filter((item) => {
                            let itemParse = JSON.parse(item);
                            return itemParse.status === 0;
                          });

                          if (arrCheck.length > 0) {
                            message.warning(
                              "Vui lòng chỉ chọn những CTKM ở trạng thái Đang chạy!"
                            );
                            return;
                          }
                          setShowPopupStop(true);
                        } else {
                          message.warning("Vui lòng chọn CTKM!");
                          return;
                        }
                      }}
                    />
                  </div>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={dataTable.listEvent}
                style={{ width: "100%" }}
                defaultCurrent={1}
                rowSelection={rowSelection}
                sizeItem={dataTable.size}
                indexPage={filter.page}
                maxpage={dataTable.total}
                rowKey="rowTable"
                scroll={{ x: 0 }}
                size="small"
                loading={loadingTable}
                onRow={(record, rowIndex) => {
                  return {
                    onClick: (event) => {
                      const rowValue =
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey;
                      const objRow = JSON.parse(rowValue);

                      history.push(`/detail/event/immediate/${objRow.id}`);
                    }, // click row
                    onMouseDown: (event) => {
                      const rowValue =
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey;
                      const objRow = JSON.parse(rowValue);

                      if (event.button == 2 || event == 4) {
                        window.open(
                          `/detail/event/immediate/${objRow.id}`,
                          "_blank"
                        );
                      }
                    },
                  };
                }}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilter({ ...filter, page: page });
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
      <Modal
        visible={showPopupActive}
        width={400}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 30 }}
        centered
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Thông báo
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: "100%",
                  marginTop: 5,
                  textAlign: "center",
                  fontWeight: 600,
                  color: "#000",
                }}
              >
                <span>Bạn có muốn kích hoạt CTKM đã chọn ?</span>
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -60,
              right: -1,
              display: "flex",
              justifyContent: "center",
              width: "100%",
            }}
          >
            <div style={{ width: 165, marginRight: 25 }}>
              <IButton
                color={colors.oranges}
                title="Hủy bỏ"
                styleHeight={{ minWidth: 0 }}
                onClick={() => {
                  setShowPopupActive(false);
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{ width: 165 }}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                styleHeight={{ minWidth: 0 }}
                icon={ISvg.NAME.CHECKMARK}
                onClick={async () => {
                  const arrID = selectedRowKeys.map((item) => {
                    let itemParse = JSON.parse(item);
                    return itemParse.id;
                  });

                  const obj = {
                    event_id_list: arrID,
                    type: 1,
                  };
                  console.log("objActive: ", obj);
                  postActiveOrStopEvent(obj);
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        visible={showPopupStop}
        width={400}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 30 }}
        centered
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Thông báo
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: "100%",
                  marginTop: 5,
                  textAlign: "center",
                  fontWeight: 600,
                  color: "#000",
                }}
              >
                <span>Các CTKM bạn vừa chọn sẽ được tạm dừng.</span>
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -60,
              right: -1,
              display: "flex",
              justifyContent: "center",
              width: "100%",
            }}
          >
            <div style={{ width: 165, marginRight: 25 }}>
              <IButton
                color={colors.oranges}
                title="Hủy bỏ"
                styleHeight={{ minWidth: 0 }}
                onClick={() => {
                  setShowPopupStop(false);
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{ width: 165 }}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                styleHeight={{ minWidth: 0 }}
                icon={ISvg.NAME.CHECKMARK}
                onClick={async () => {
                  const arrID = selectedRowKeys.map((item) => {
                    let itemParse = JSON.parse(item);
                    return itemParse.id;
                  });

                  const obj = {
                    event_id_list: arrID,
                    type: 2,
                  };
                  console.log("objStop: ", obj);
                  postActiveOrStopEvent(obj);
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default ListEventImmediatePage;
