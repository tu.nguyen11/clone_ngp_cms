import { Col, InputNumber, Modal, Row } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import { IButton, ISvg } from "../../../components";
import {
  IError,
  StyledInputNumber,
  StyledISelect,
} from "../../../components/common";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";
import useModalTreeProductImmediate from "../../../components/common/ModalAgency/useModalTreeProductImmediate";
import useModalTreeProductPresentImmediate from "../../../components/common/ModalAgency/useModalTreeProductPresentImmediate";
import {
  CLEAR_REDUX_IMMEDIATE,
  CREATE_KEY_DEGSIN_IMMEDIATE,
  UPDATE_TREE,
  UPDATE_DATA_PRODUCT,
  CREATE_DATE_EVENT_IMMEDIATE,
  REMOVE_TREE,
  ASSIGNED_KEY,
} from "../../../store/reducers";

export default function CreateEventDesginImmediate() {
  const history = useHistory();
  const { id } = useParams();
  const [dataDrowpCate, setDataDrowpCate] = useState([]);
  const [key, setKey] = useState(1);
  const [isVisibleTree, setVisibleTree] = useState(false);
  const [isVisibleTreePersent, setVisibleTreePresent] = useState(false);

  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const {
    eventImmediate,
    treeProductIImmediate = { listConfirm: [] },
    treeProductPresentIImmediate = { listConfirm: [] },
  } = dataRoot;

  const _fetchGetProductGetCateByCompany = async () => {
    try {
      const data = await APIService._getProductGetCateByCompany();
      let dataNew = data.cate.map((item) => {
        const key = item.id;
        const value = item.company_name;
        return { key, value };
      });
      setDataDrowpCate(dataNew);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    _fetchGetProductGetCateByCompany();
  }, []);

  const treeProuct = (dataProduct, isPercent) => {
    let keyRootTemp = undefined;
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            keyRootTemp = item.id;
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: 0,
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding: "6px 0px",
                  }}
                >
                  {item.name}
                </li>
                {treeProuct(item.children, isPercent)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[12, 0]} type="flex">
                    <Col span={isPercent ? 13 : 21}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    {isPercent ? (
                      <Col span={8}>
                        <InputNumber
                          style={{
                            height: 32,
                            borderRadius: 0,
                          }}
                          className="disabled-up"
                          value={
                            isPercent
                              ? eventImmediate.event_degsin.key_reward ===
                                DefineKeyEvent.event_product_present
                                ? item.quantity_product_present
                                : item.percent_sale
                              : eventImmediate.event_degsin.key_condition ===
                                DefineKeyEvent.filter_sales
                              ? item.quantity_sale
                              : item.quantity_specifications
                          }
                          formatter={(value) =>
                            isPercent
                              ? eventImmediate.event_degsin.key_reward ===
                                  DefineKeyEvent.event_product_present ||
                                eventImmediate.event_degsin.key_reward === 0
                                ? `${value}`.replace(
                                    /\B(?=(\d{3})+(?!\d))/g,
                                    ","
                                  )
                                : `${value}%`
                              : `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={(value) =>
                            isPercent
                              ? eventImmediate.event_degsin.key_reward ===
                                DefineKeyEvent.event_product_present
                                ? value.replace(/\$\s?|(,*)/g, "")
                                : value.replace("%", "")
                              : value.replace(/\$\s?|(,*)/g, "")
                          }
                          onChange={(value) => {
                            const dataSales = {
                              key: "listConfirm",
                              keyRoot: "treeProductIImmediate",
                              keyObject: "children",
                              keyValue: "quantity_sale",
                              keyLocationRoot: item.locationRoot,
                              value: value,
                              idx: index,
                            };
                            const dataCount = {
                              key: "listConfirm",
                              keyRoot: "treeProductIImmediate",
                              keyObject: "children",
                              keyValue: "quantity_specifications",
                              keyLocationRoot: item.locationRoot,
                              value: value,
                              idx: index,
                            };

                            const dataProductSales = {
                              keyRoot: "dataProduct",
                              keyObject: "children",
                              keyValue: "quantity_sale",
                              keyLocationRoot: item.locationRootReal,
                              value: value,
                              idx: item.locationChildrenReal,
                            };

                            const dataProductCount = {
                              keyRoot: "dataProduct",
                              keyObject: "children",
                              keyValue: "quantity_specifications",
                              keyLocationRoot: item.locationRootReal,
                              value: value,
                              idx: item.locationChildrenReal,
                            };

                            const data_percent = {
                              key: "listConfirm",
                              keyRoot: "treeProductPresentIImmediate",
                              keyObject: "children",
                              keyValue: "percent_sale",
                              keyLocationRoot: item.locationRoot,
                              value: value,
                              idx: index,
                            };

                            const dataProductPercent = {
                              keyRoot: "dataProductPresent",
                              keyObject: "children",
                              keyValue: "percent_sale",
                              keyLocationRoot: item.locationRootReal,
                              value: value,
                              idx: item.locationChildrenReal,
                            };

                            const data_quantity_percent = {
                              key: "listConfirm",
                              keyRoot: "treeProductPresentIImmediate",
                              keyObject: "children",
                              keyValue: "quantity_product_present",
                              keyLocationRoot: item.locationRoot,
                              value: value,
                              idx: index,
                            };

                            const data_quantity_percent_clone = {
                              key: "listTreeClone",
                              keyRoot: "treeProductPresentIImmediate",
                              keyObject: "children",
                              keyValue: "quantity_product_present",
                              keyLocationRoot: item.locationRoot,
                              value: value,
                              idx: index,
                            };

                            const dataProductPercentQuantity = {
                              keyRoot: "dataProductPresent",
                              keyObject: "children",
                              keyValue: "quantity_product_present",
                              keyLocationRoot: item.locationRootReal,
                              value: value,
                              idx: item.locationChildrenReal,
                            };

                            if (isPercent) {
                              dispatch(
                                UPDATE_TREE(
                                  eventImmediate.event_degsin.key_reward ===
                                    DefineKeyEvent.event_product_present
                                    ? data_quantity_percent
                                    : data_percent
                                )
                              );
                              dispatch(
                                UPDATE_TREE(data_quantity_percent_clone)
                              );
                              dispatch(
                                UPDATE_DATA_PRODUCT(
                                  eventImmediate.event_degsin.key_reward ===
                                    DefineKeyEvent.event_product_present
                                    ? dataProductPercentQuantity
                                    : dataProductPercent
                                )
                              );
                            } else {
                              dispatch(
                                UPDATE_TREE(
                                  eventImmediate.event_degsin.key_condition ===
                                    DefineKeyEvent.filter_sales
                                    ? dataSales
                                    : dataCount
                                )
                              );
                              dispatch(
                                UPDATE_DATA_PRODUCT(
                                  eventImmediate.event_degsin.key_condition ===
                                    DefineKeyEvent.filter_sales
                                    ? dataProductSales
                                    : dataProductCount
                                )
                              );
                            }
                          }}
                          min={1}
                        />
                      </Col>
                    ) : null}

                    <Col span={3}>
                      <div
                        style={{
                          height: 32,
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                          className="cursor"
                          onClick={() => {
                            if (isPercent) {
                              const dataRemovePresent = {
                                key: "listConfirm",
                                keyRoot: "treeProductPresentIImmediate",
                                keyObject: "children",
                                keyValue: "quantity_product_present",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };
                              const dataRemovePresentShow = {
                                key: "listTreeShow",
                                keyRoot: "treeProductPresentIImmediate",
                                keyObject: "children",
                                keyValue: "quantity_product_present",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };

                              const dataRemovePresentClone = {
                                key: "listTreeClone",
                                keyRoot: "treeProductPresentIImmediate",
                                keyObject: "children",
                                keyValue: "quantity_product_present",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };
                              const dataAssigned = {
                                keyRoot: "treeProductPresentIImmediate",
                                keyTree: "listTreeShow",
                                keyListID: "listKeyClone",
                              };

                              const dataProductPercentQuantity = {
                                keyRoot: "dataProductPresent",
                                keyObject: "children",
                                keyValue: "quantity_product_present",
                                keyLocationRoot: item.locationRootReal,
                                value: 0,
                                idx: item.locationChildrenReal,
                              };
                              dispatch(
                                UPDATE_DATA_PRODUCT(dataProductPercentQuantity)
                              );

                              dispatch(REMOVE_TREE(dataRemovePresentShow));
                              dispatch(REMOVE_TREE(dataRemovePresent));
                              dispatch(REMOVE_TREE(dataRemovePresentClone));
                              dispatch(ASSIGNED_KEY(dataAssigned));
                            } else {
                              const dataRemovePresent = {
                                key: "listConfirm",
                                keyRoot: "treeProductIImmediate",
                                keyObject: "children",
                                keyValue: "quantity_product_present",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };
                              const dataRemovePresentShow = {
                                key: "listTreeShow",
                                keyRoot: "treeProductIImmediate",
                                keyObject: "children",
                                keyValue: "quantity_product_present",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };

                              const dataRemovePresentClone = {
                                key: "listTreeClone",
                                keyRoot: "treeProductIImmediate",
                                keyObject: "children",
                                keyValue: "quantity_product_present",
                                keyLocationRoot: item.locationRoot,
                                idx: index,
                              };
                              const dataAssigned = {
                                keyRoot: "treeProductIImmediate",
                                keyTree: "listTreeShow",
                                keyListID: "listKeyClone",
                              };

                              dispatch(REMOVE_TREE(dataRemovePresentShow));
                              dispatch(REMOVE_TREE(dataRemovePresent));
                              dispatch(REMOVE_TREE(dataRemovePresentClone));
                              dispatch(ASSIGNED_KEY(dataAssigned));
                            }
                          }}
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill={colors.oranges}
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo Chương trình trả thưởng ngay mới
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_IMMEDIATE());
                    history.push("/event/immediate/list");
                  }}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div
            style={{
              width: 1000,
              padding: "36px 48px",
              background: "white",
              minHeight: 650,
            }}
            className="shadow"
          >
            <Row gutter={[24, 24]}>
              <Col span={12}>
                <Row gutter={[0, 12]}>
                  <Col span={24}>
                    <StyledITileHeading minFont="10px" maxFont="16px">
                      Ngành hàng áp dụng
                    </StyledITileHeading>
                  </Col>
                  <Col span={24}>
                    <StyledISelect
                      data={dataDrowpCate}
                      value={
                        eventImmediate.event_degsin.key_cate === 0
                          ? undefined
                          : eventImmediate.event_degsin.key_cate
                      }
                      onChange={(value) => {
                        const data = {
                          key: "key_cate",
                          keyRoot: "event_degsin",
                          value: value,
                        };
                        dispatch(CREATE_KEY_DEGSIN_IMMEDIATE(data));

                        if (eventImmediate.event_degsin.key_condition !== 0)
                          setVisibleTree(true);
                      }}
                      marginLeft="0px"
                      placeholder="Vui lòng chọn Ngành hàng áp dụng"
                      isBorderBottom="1px solid #d9d9d9"
                    />
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row gutter={[24]}>
                  <Col span={12}>
                    <Row gutter={[0, 12]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Điều kiện nhận thưởng
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <StyledISelect
                          data={[
                            // {
                            //   key: DefineKeyEvent.filter_sales,
                            //   value: 'Doanh số sản phẩm'
                            // },
                            {
                              key: DefineKeyEvent.filter_money,
                              value: "Số lượng sản phẩm",
                            },
                          ]}
                          value={
                            eventImmediate.event_degsin.key_condition === 0
                              ? undefined
                              : eventImmediate.event_degsin.key_condition
                          }
                          disabled={eventImmediate.event_degsin.key_cate === 0}
                          onChange={(value) => {
                            const data = {
                              key: "key_condition",
                              keyRoot: "event_degsin",
                              value: value,
                            };
                            dispatch(CREATE_KEY_DEGSIN_IMMEDIATE(data));
                            if (treeProductIImmediate.listConfirm.length === 0)
                              setVisibleTree(true);
                          }}
                          marginLeft="0px"
                          placeholder="Vui lòng chọn điều kiện nhận thưởng"
                          isBorderBottom="1px solid #d9d9d9"
                        />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={12}>
                    <Row gutter={[0, 12]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Phần thưởng khuyến mãi
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <StyledISelect
                          data={[
                            {
                              key: 1,
                              value: "Quà tặng",
                            },
                            // {
                            //   key: 2,
                            //   value: 'Phầm trăm (%) giảm giá'
                            // }
                          ]}
                          value={
                            eventImmediate.event_degsin.key_reward === 0
                              ? undefined
                              : eventImmediate.event_degsin.key_reward
                          }
                          // disabled={eventImmediate.event_degsin.key_cate === 0}
                          onChange={(value) => {
                            const data = {
                              key: "key_reward",
                              keyRoot: "event_degsin",
                              value: value,
                            };
                            dispatch(CREATE_KEY_DEGSIN_IMMEDIATE(data));
                            // if (
                            //   treeProductPresentIImmediate.listConfirm
                            //     .length === 0
                            // )
                            setVisibleTreePresent(true);
                          }}
                          marginLeft="0px"
                          placeholder="Vui lòng chọn phần thưởng khuyến mãi"
                          isBorderBottom="1px solid #d9d9d9"
                        />
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={12}>
                <div>
                  <div style={{ border: "1px solid #d9d9d9" }}>
                    <Row>
                      <Col span={24}>
                        <div
                          style={{
                            padding: "12px",
                            borderBottom: "1px solid #d9d9d9",
                          }}
                        >
                          <Row gutter={[12, 0]}>
                            {[
                              {
                                key: 1,
                                value: " Bất kì",
                              },
                              // {
                              //   key: 2,
                              //   value: 'Chính xác'
                              // },
                              // {
                              //   key: 3,
                              //   value: 'Tổng số'
                              // }
                            ].map((item) => {
                              return (
                                <Col span={6}>
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <div
                                      style={{
                                        width: 20,
                                        height: 20,
                                        borderRadius: 10,
                                        border:
                                          key === item.key
                                            ? `4px solid ${colors.main}`
                                            : "1px solid rgba(34, 35, 43, 0.5)",
                                        marginRight: 12,
                                        transition: "all 0.2s ease-in-out 0s",
                                      }}
                                      onClick={() => setKey(item.key)}
                                      className="cursor"
                                    />
                                    <span>{item.value}</span>
                                  </div>
                                </Col>
                              );
                            })}
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            padding: "12px",
                            borderBottom: "1px solid #d9d9d9",
                          }}
                        >
                          <Row gutter={[12, 0]} type="flex">
                            <Col span={21}>
                              <div
                                style={{
                                  height: "100%",
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <StyledITileHeading
                                  minFont="10px"
                                  maxFont="14px"
                                >
                                  Nhóm sản phẩm/ Sản phẩm
                                </StyledITileHeading>
                              </div>
                            </Col>
                            {/* <Col span={7}>
                              <div
                                style={{
                                  height: '100%',
                                  display: 'flex',
                                  alignItems: 'center',
                                  textAlign: 'center'
                                }}
                              >
                                <StyledITileHeading
                                  minFont='10px'
                                  maxFont='14px'
                                >
                                  {eventImmediate.event_degsin.key_condition ===
                                  DefineKeyEvent.filter_sales
                                    ? 'Doanh số'
                                    : 'Số lượng Quy cách nhỏ'}
                                </StyledITileHeading>
                              </div>
                            </Col>
                            */}
                            <Col span={3}>
                              <div
                                style={{
                                  border: `1px solid ${colors.main}`,
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                  padding: "4px 0px",
                                  height: "100%",
                                }}
                                onClick={() => {
                                  if (
                                    eventImmediate.event_degsin.key_cate === 0
                                  ) {
                                    return IError("Bạn chưa chọn nghành hàng.");
                                  } else {
                                    setVisibleTree(true);
                                  }
                                }}
                                className="cursor"
                              >
                                <ISvg
                                  name={ISvg.NAME.WRITE}
                                  width={16}
                                  height={16}
                                  fill={colors.main}
                                />
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            padding: "12px",
                            height: 350,
                            overflow: "auto",
                          }}
                        >
                          {treeProuct(treeProductIImmediate.listConfirm, false)}
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <Col span={24}>
                    <span>Số lượng Quy cách nhỏ</span>
                    <StyledInputNumber
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      style={{
                        margin: "0px 12px",
                        height: 32,
                        borderRadius: 0,
                        width: 170,
                      }}
                      value={eventImmediate.condition_product_quantity_min}
                      onChange={(value) => {
                        const dataAction = {
                          key: "condition_product_quantity_min",
                          value: value,
                        };
                        dispatch(CREATE_DATE_EVENT_IMMEDIATE(dataAction));
                      }}
                      min={0}
                      className="disabled-up"
                    />{" "}
                    Sản phẩm
                  </Col>
                </div>
              </Col>

              <Col span={12}>
                <div style={{ border: "1px solid #d9d9d9" }}>
                  <Row>
                    <Col span={24}>
                      <div
                        style={{
                          padding: "12px",
                          borderBottom: "1px solid #d9d9d9",
                        }}
                      >
                        <Row gutter={[12, 0]} type="flex">
                          <Col span={14}>
                            <div
                              style={{
                                height: "100%",
                                display: "flex",
                                alignItems: "center",
                              }}
                            >
                              <StyledITileHeading minFont="10px" maxFont="14px">
                                Nhóm sản phẩm/ Sản phẩm
                              </StyledITileHeading>
                            </div>
                          </Col>
                          <Col span={7}>
                            <div
                              style={{
                                height: "100%",
                                display: "flex",
                                alignItems: "center",
                                textAlign: "center",
                              }}
                            >
                              <StyledITileHeading minFont="10px" maxFont="14px">
                                {eventImmediate.event_degsin.key_reward ===
                                DefineKeyEvent.event_discount_present ? (
                                  <StyledITileHeading
                                    minFont="10px"
                                    maxFont="14px"
                                  >
                                    Tỉ lệ giảm/ đơn giá <br />
                                    QC nhỏ
                                  </StyledITileHeading>
                                ) : (
                                  <StyledITileHeading
                                    minFont="10px"
                                    maxFont="14px"
                                  >
                                    Số lượng
                                    <br />
                                    Quy cách nhỏ
                                  </StyledITileHeading>
                                )}
                              </StyledITileHeading>
                            </div>
                          </Col>
                          <Col span={3}>
                            <div
                              style={{
                                border: `1px solid ${colors.main}`,
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                padding: "4px 0px",
                                height: "100%",
                              }}
                              onClick={() => setVisibleTreePresent(true)}
                              className="cursor"
                            >
                              <ISvg
                                name={ISvg.NAME.WRITE}
                                width={16}
                                height={16}
                                fill={colors.main}
                              />
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          padding: "12px",
                          height: 390,
                          overflow: "auto",
                        }}
                      >
                        {treeProuct(
                          treeProductPresentIImmediate.listConfirm,
                          true
                        )}
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              width: 1000,
            }}
          >
            <IButton
              title="Quay lại"
              color={colors.main}
              onClick={() => history.goBack()}
              icon={ISvg.NAME.ARROWTHINLEFT}
              styleHeight={{
                width: 140,
              }}
            />
            <IButton
              title="Tiếp tục"
              color={colors.main}
              icon={ISvg.NAME.ARROWRIGHT}
              iconRight={true}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                if (treeProductIImmediate.listConfirm.length === 0) {
                  return IError(
                    "Bạn chưa chọn sản phẩm cho điều kiện nhận thưởng chương trình."
                  );
                }

                if (eventImmediate.condition_product_quantity_min === 0) {
                  return IError(
                    "Bạn chưa nhập số lượng Quy cách nhỏ của tổng sản phẩm để thỏa điều điều kiện chương trình."
                  );
                }
                if (eventImmediate.event_degsin.key_reward === 0) {
                  return IError("Bạn chưa chọn loại Phần thưởng khuyến mãi.");
                }
                if (treeProductPresentIImmediate.listConfirm.length === 0) {
                  return IError(
                    "Bạn chưa chọn sản phẩm nhận thưởng cho chương trình."
                  );
                }

                history.push(`/event/immediate/description/create/${id}`);
              }}
              style={{ marginLeft: 16 }}
            />
          </div>
        </Col>
        <Modal
          visible={isVisibleTree}
          footer={null}
          width={1200}
          centered={true}
          closable={false}
          maskClosable={false}
        >
          {useModalTreeProductImmediate(
            () => {
              setVisibleTree(false);
            },
            eventImmediate.event_degsin.key_cate,
            eventImmediate.event_degsin.key_condition ===
              DefineKeyEvent.filter_sales
              ? "Thêm nhóm sản phẩm & điều kiện doanh số"
              : "Thêm nhóm sản phẩm & điều kiện số lượng"
          )}
        </Modal>

        <Modal
          visible={isVisibleTreePersent}
          footer={null}
          width={1200}
          centered={true}
          closable={false}
          maskClosable={false}
        >
          {useModalTreeProductPresentImmediate(
            () => {
              setVisibleTreePresent(false);
            },
            0,
            eventImmediate.event_degsin.key_reward ===
              DefineKeyEvent.event_product_present
              ? "Thêm nhóm sản phẩm & điều kiện doanh số"
              : "Thêm nhóm sản phẩm & thiết lập tỷ lệ giảm giá"
          )}
        </Modal>
      </Row>
    </div>
  );
}
