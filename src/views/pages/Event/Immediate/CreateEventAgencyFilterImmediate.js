import { Col, Row, Modal, Checkbox, InputNumber } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'
import { APIService } from '../../../../services'
import { colors } from '../../../../assets'
import { IButton, ISvg, ITableHtml } from '../../../components'
import {
  StyledITileHeading,
  StyledITitle
} from '../../../components/common/Font/font'

import moment from 'moment'
import {
  CREATE_DATE_EVENT_IMMEDIATE,
  CLEAR_REDUX_IMMEDIATE,
  RESET_MODAL
} from '../../../store/reducers'
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1,
  useModalAgencyS2
} from '../../../components/common'
import { DefineKeyEvent } from '../../../../utils/DefineKey'

export default function CreateEventAgencyFilterImmediate (props) {
  const [isOpenApply, setIsOpenApply] = useState(false)
  const history = useHistory()
  const params = useParams()
  const { id } = params
  const format = 'HH:mm:ss DD-MM-YYYY'

  const dispatch = useDispatch()
  const dataRoot = useSelector(state => state)
  const { eventImmediate, modalS1, modalS2 } = dataRoot
  const [dropdownRegion, setDropdownRegion] = useState([])
  const [loading, setLoading] = useState(false)

  const [isVisibleAgency, setVisibleAgency] = useState(false)

  const handleStartOpenChange = open => {
    if (!open) {
      setIsOpenApply(true)
    }
  }

  const handleEndOpenChange = open => {
    setIsOpenApply(open)
  }

  const disabledStartDateApply = startValue => {
    if (!startValue || !eventImmediate.to_time_apply) {
      return false
    }
    return startValue.valueOf() > eventImmediate.to_time_apply
  }

  const disabledEndDateApply = endValue => {
    if (!endValue || !eventImmediate.from_time_apply) {
      return false
    }
    return endValue.valueOf() <= eventImmediate.from_time_apply
  }

  let dataHeader = [
    {
      name: 'Tên đại lý',
      align: 'left'
    },
    {
      name: 'Mã đại lý',
      align: 'left'
    },
    {
      name: 'Cấp đại lý',
      align: 'center'
    }
  ]

  const headerTable = dataHeader => {
    return (
      <tr className='tr-table scroll'>
        {dataHeader.map((item, index) => (
          <th className='th-table' style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    )
  }

  const bodyTable = databody => {
    return databody.map(item => (
      <tr className='tr-table'>
        <td
          className='td-table'
          style={{ textAlign: 'left', fontWeight: 'normal' }}
        >
          {eventImmediate.type_way === 2 ? item.shop_name : item.supplierName}
        </td>
        <td
          className='td-table'
          style={{ textAlign: 'left', fontWeight: 'normal' }}
        >
          {eventImmediate.type_way === 2 ? item.dms_code : item.supplierCode}
        </td>
        <td
          className='td-table'
          style={{ textAlign: 'center', fontWeight: 'normal' }}
        >
          {eventImmediate.type_way === 2 ? 'Cấp 1' : 'Cấp 2'}
        </td>
      </tr>
    ))
  }

  const _renderModalS1 = () => {
    return (
      <Modal
        visible={isVisibleAgency}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1(() => {
          setVisibleAgency(false)
        })}
      </Modal>
    )
  }

  const _renderModalS2 = () => {
    return (
      <Modal
        visible={isVisibleAgency}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS2(() => {
          setVisibleAgency(false)
        })}
      </Modal>
    )
  }

  return (
    <div style={{ width: '100%', padding: '18px 0px 0px 0px' }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo chương trình trả thưởng ngay mới
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'flex-end'
                }}
              >
                <IButton
                  title='Hủy'
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  disabled={
                    eventImmediate.type_event_status ===
                    DefineKeyEvent.event_running
                  }
                  styleHeight={{
                    width: 140
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_IMMEDIATE())
                    history.push('/event/immediate/list')
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1200 }}>
            <Row gutter={[24, 24]} type='flex'>
              <Col span={6}>
                <div
                  style={{ padding: 24, height: '100%' }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Thiết lập chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Cấp độ Chương trình
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24} style={{ padding: 0 }}>
                      <Checkbox
                        style={{ marginRight: 12 }}
                        checked={
                          eventImmediate.priori ===
                          DefineKeyEvent.priori_prioritize
                        }
                        onChange={e => {
                          const data = {
                            key: 'priori',
                            value: e.target.checked === true ? 1 : 0
                          }
                          dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                        }}
                      />
                      <span>Ưu tiên</span>
                    </Col>
                    {eventImmediate.priori ===
                    DefineKeyEvent.priori_prioritize ? (
                      <Col span={24} style={{ padding: 0 }}>
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center'
                          }}
                        >
                          <span
                            style={{
                              marginLeft: 32,
                              width: 140
                            }}
                          >
                            Độ ưu tiên
                          </span>
                          <InputNumber
                            className='disabled-up'
                            value={eventImmediate.valuePriori}
                            style={{
                              height: 32,
                              width: '100%',
                              borderRadius: 0
                            }}
                            onChange={value => {
                              const data = {
                                key: 'valuePriori',
                                value: value
                              }
                              dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                            }}
                            min={0}
                          />
                        </div>
                      </Col>
                    ) : null}
                  </Row>
                </div>
              </Col>
              <Col span={9}>
                <div
                  style={{ padding: 24, height: '100%' }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Thời gian áp dụng & trả thưởng
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Thời gian áp dụng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput='0px'
                                disabled={
                                  eventImmediate.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                placeholder='nhập thời gian bắt đầu'
                                onOpenChange={handleStartOpenChange}
                                format={format}
                                value={
                                  eventImmediate.from_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediate.from_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={date => {
                                  const data = {
                                    key: 'from_time_apply',
                                    value: date._d.getTime()
                                  }
                                  dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Kết thúc</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledEndDateApply}
                                isBorderBottom={true}
                                disabled={
                                  eventImmediate.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                paddingInput='0px'
                                open={isOpenApply}
                                format={format}
                                onOpenChange={handleEndOpenChange}
                                value={
                                  eventImmediate.to_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(eventImmediate.to_time_apply),
                                        format
                                      )
                                }
                                onChange={date => {
                                  const data = {
                                    key: 'to_time_apply',
                                    value: date._d.getTime()
                                  }
                                  dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                                }}
                                placeholder='nhập thời gian kết thúc'
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>

              <Col span={9}>
                <div
                  style={{ padding: 24, minHeight: 650 }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 24]}>
                    <Col>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    {eventImmediate.type_event_status ===
                    DefineKeyEvent.event_running ? null : (
                      <Col span={24}>
                        <div
                          style={{
                            display: 'flex',
                            justifyContent: 'flex-end'
                          }}
                        >
                          <IButton
                            title='Chỉnh sửa danh sách'
                            color={colors.main}
                            icon={ISvg.NAME.WRITE}
                            onClick={() => setVisibleAgency(true)}
                          />
                        </div>
                      </Col>
                    )}

                    <Col span={24}>
                      <ITableHtml
                        height='420px'
                        childrenBody={bodyTable(
                          eventImmediate.type_way === 2
                            ? modalS1.listS1Show
                            : modalS2.listS2Show
                        )}
                        childrenHeader={headerTable(dataHeader)}
                        style={{
                          border: '1px solid rgba(122, 123, 123, 0.5)',
                          padding: '0px 8px '
                        }}
                        isBorder={false}
                      />
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: 'flex', justifyContent: 'flex-end' }}
                      >
                        <IButton
                          title='Hủy'
                          color={colors.oranges}
                          icon={ISvg.NAME.CROSS}
                          styleHeight={{
                            width: 140
                          }}
                          onClick={() => {
                            const data = {
                              key: 'type_way',
                              value: 0
                            }
                            dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                            eventImmediate.type_way === 2
                              ? dispatch(
                                  RESET_MODAL({
                                    keyInitial_listShow: 'listS1Show',
                                    keyInitial_root: 'modalS1',
                                    keyInitial_listAddClone: 'listS1AddClone',
                                    keyInitial_listAdd: 'listS1Add',
                                    keyInitial_list: 'listS1'
                                  })
                                )
                              : dispatch(
                                  RESET_MODAL({
                                    keyInitial_listShow: 'listS2Show',
                                    keyInitial_root: 'modalS2',
                                    keyInitial_listAddClone: 'listS2AddClone',
                                    keyInitial_listAdd: 'listS2Add',
                                    keyInitial_list: 'listS2'
                                  })
                                )

                            history.goBack()
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <IButton
                  onClick={async () => {
                    let dateNow = new Date()
                    dateNow.setHours(
                      dateNow.getHours() + 1,
                      dateNow.getMinutes() - 5
                    )

                    if (eventImmediate.type_way === 2) {
                      if (modalS1.listS1Show.length === 0) {
                        return IError('Chưa chọn đại lý áp dụng chương trình')
                      }
                    } else {
                      if (modalS2.listS2Show.length === 0) {
                        return IError('Chưa chọn đại lý áp dụng chương trình')
                      }
                    }

                    if (eventImmediate.type_way === 2) {
                      if (modalS1.listS1Show.length === 0) {
                        return IError('Chưa chọn đại lý áp dụng chương trình')
                      }
                    } else {
                      if (modalS2.listS2Show.length === 0) {
                        return IError('Chưa chọn đại lý áp dụng chương trình')
                      }
                    }

                    if (
                      eventImmediate.from_time_apply === 0 ||
                      eventImmediate.to_time_apply === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{' '}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian áp dụng
                          </span>{' '}
                          & <span style={{ fontWeight: 600 }}>trả thương</span>{' '}
                          , <span style={{ fontWeight: 600 }}>đại lý</span> áp
                          dụng chương trình
                        </span>
                      )
                      return IError(content)
                    }

                    if (eventImmediate.priori === 1) {
                      try {
                        const data = await APIService._getCheckExistPriotyInEvent(
                          eventImmediate.valuePriori,
                          eventImmediate.type_way === 2 ? 1 : 2
                        )
                        if (data.exist_index === 0) {
                          return IError('Độ ưu tiên đã tồn tại')
                        }
                      } catch (error) {
                        console.log(error)
                      }
                    }

                    // if (event.ISedit) {
                    //   if (
                    //     event.type_event_status !== DefineKeyEvent.event_running
                    //   ) {
                    //     if (event.from_time_apply <= dateNow.getTime()) {
                    //       const content = (
                    //         <span>
                    //           Ngày bắt đầu chương trình phải lớn hơn giờ hiện
                    //           tại 1 tiếng.
                    //         </span>
                    //       );
                    //       return IError(content);
                    //     }
                    //   }
                    // } else {
                    //   if (event.from_time_apply <= dateNow.getTime()) {
                    //     const content = (
                    //       <span>
                    //         Ngày bắt đầu chương trình phải lớn hơn giờ hiện tại
                    //         1 tiếng.
                    //       </span>
                    //     );
                    //     return IError(content);
                    //   }
                    // }

                    history.push(`/event/immediate/create/desgin/${id}`)
                  }}
                  title='Tiếp tục'
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      {eventImmediate.type_way === 2 ? _renderModalS1() : _renderModalS2()}
    </div>
  )
}
