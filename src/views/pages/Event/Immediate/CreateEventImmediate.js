import { Col, Row, Modal, Checkbox, InputNumber } from 'antd'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'

import moment from 'moment'
import {
  StyledITileHeading,
  StyledITitle
} from '../../../components/common/Font/font'
import { colors } from '../../../../assets'
import { IButton, ISvg } from '../../../components'
import {
  CREATE_DATE_EVENT_IMMEDIATE,
  CLEAR_REDUX_IMMEDIATE
} from '../../../store/reducers'
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1,
  useModalAgencyS2
} from '../../../components/common'
import { APIService } from '../../../../services'

import { DefineKeyEvent } from '../../../../utils/DefineKey'
export default function CreateEventImmediate (props) {
  const [isOpenApply, setIsOpenApply] = useState(false)
  const dataRoot = useSelector(state => state)
  const { eventImmediate } = dataRoot
  const history = useHistory()
  const params = useParams()
  const { id } = params
  const format = 'HH:mm:ss DD-MM-YYYY'
  const dispatch = useDispatch()
  const [isVisibleAgency1, setVisibleAgency1] = useState(false)
  const [isVisibleAgency2, setVisibleAgency2] = useState(false)

  const handleStartOpenChange = open => {
    if (!open) {
      setIsOpenApply(true)
    }
  }

  // const fetchAPIgetCheckExistPriotyInEvent = async index => {
  //   try {
  //     const data = await APIService._getCheckExistPriotyInEvent(index)
  //     dispatch(
  //       CREATE_DATE_EVENT_IMMEDIATE({
  //         key: 'checkPriori',
  //         value: data.exist_index === 0
  //       })
  //     )
  //   } catch (error) {
  //     console.log(error)
  //   }
  // }

  const handleEndOpenChange = open => {
    setIsOpenApply(open)
  }

  const disabledStartDateApply = startValue => {
    if (!startValue || !eventImmediate.to_time_apply) {
      return false
    }
    return startValue.valueOf() > eventImmediate.to_time_apply
  }

  const disabledEndDateApply = endValue => {
    if (!endValue || !eventImmediate.from_time_apply) {
      return false
    }
    return endValue.valueOf() <= eventImmediate.from_time_apply
  }

  return (
    <div style={{ width: '100%', padding: '18px 0px 0px 0px' }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo Chương trình trả thưởng ngay mới
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'flex-end'
                }}
              >
                <IButton
                  title='Hủy'
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_IMMEDIATE())
                    history.push('/event/immediate/list')
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ width: 1100 }}>
            <Row gutter={[24, 24]}>
              <Col span={6}>
                <div
                  style={{ padding: 24, height: 500 }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Thiết lập chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Cấp độ Chương trình
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24} style={{ padding: 0 }}>
                      <Checkbox
                        style={{ marginRight: 12 }}
                        checked={
                          eventImmediate.priori ===
                          DefineKeyEvent.priori_prioritize
                        }
                        onChange={e => {
                          const data = {
                            key: 'priori',
                            value: e.target.checked === true ? 1 : 0
                          }
                          dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                        }}
                      />
                      <span>Ưu tiên</span>
                    </Col>
                    {eventImmediate.priori ===
                    DefineKeyEvent.priori_prioritize ? (
                      <Col span={24} style={{ padding: 0 }}>
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center'
                          }}
                        >
                          <span
                            style={{
                              marginLeft: 32,
                              width: 140
                            }}
                          >
                            Độ ưu tiên
                          </span>
                          <InputNumber
                            formatter={value =>
                              `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                            }
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            className='disabled-up'
                            value={eventImmediate.valuePriori}
                            style={{
                              height: 32,
                              width: '100%',
                              borderRadius: 0
                            }}
                            onChange={value => {
                              const data = {
                                key: 'valuePriori',
                                value: value
                              }
                              dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                            }}
                            min={1}
                          />
                        </div>
                      </Col>
                    ) : null}
                  </Row>
                </div>
              </Col>
              <Col span={10}>
                <div
                  style={{ padding: 24, height: 500 }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Thời gian áp dụng & trả thưởng
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Thời gian áp dụng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput='0px'
                                // disabled={
                                //   event.type_event_status ===
                                //   DefineKeyEvent.event_running
                                // }
                                placeholder='nhập thời gian bắt đầu'
                                onOpenChange={handleStartOpenChange}
                                format={format}
                                value={
                                  eventImmediate.from_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediate.from_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={date => {
                                  const data = {
                                    key: 'from_time_apply',
                                    value: date._d.getTime()
                                  }
                                  dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Kết thúc</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledEndDateApply}
                                isBorderBottom={true}
                                // disabled={
                                //   event.type_event_status ===
                                //   DefineKeyEvent.event_running
                                // }
                                paddingInput='0px'
                                open={isOpenApply}
                                format={format}
                                onOpenChange={handleEndOpenChange}
                                value={
                                  eventImmediate.to_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(eventImmediate.to_time_apply),
                                        format
                                      )
                                }
                                onChange={date => {
                                  const data = {
                                    key: 'to_time_apply',
                                    value: date._d.getTime()
                                  }
                                  dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                                }}
                                placeholder='nhập thời gian kết thúc'
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={8}>
                <div
                  style={{ padding: 24, height: 500 }}
                  className='box-shadow'
                >
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <StyledITileHeading minFont='10px' maxFont='16px'>
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 24 }}>
                        <Row>
                          <Col span={6}></Col>
                          <Col span={12}>
                            <IButton
                              title='Tạo điều kiện lọc'
                              color={
                                eventImmediate.type_way ===
                                DefineKeyEvent.event_type_assigned
                                  ? 'white'
                                  : colors.main
                              }
                              // disabled={
                              //   event.type_event_status ===
                              //   DefineKeyEvent.event_running
                              // }
                              styleHeight={{
                                background:
                                  eventImmediate.type_way ===
                                  DefineKeyEvent.event_type_assigned
                                    ? colors.main
                                    : 'white'
                              }}
                              onClick={() => {
                                const data = {
                                  key: 'type_way',
                                  value: DefineKeyEvent.event_type_assigned
                                }
                                dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                                // khi nhấn vô push qua màn hình filter
                                history.push(
                                  `/event/immediate/create/filter/${id}`
                                )
                              }}
                            />
                          </Col>
                          <Col span={6}></Col>
                        </Row>
                      </div>
                    </Col>
                    <Col span={24}>
                      <IButton
                        title='Chọn theo Danh sách đại lý cấp 1'
                        color={
                          eventImmediate.type_way ===
                          DefineKeyEvent.event_type_S1
                            ? 'white'
                            : colors.main
                        }
                        // disabled={
                        //   eventImmediate.type_event_status ===
                        //   DefineKeyEvent.event_running
                        // }
                        styleHeight={{
                          background:
                            eventImmediate.type_way ===
                            DefineKeyEvent.event_type_S1
                              ? colors.main
                              : 'white'
                        }}
                        onClick={() => {
                          const data = {
                            key: 'type_way',
                            value: DefineKeyEvent.event_type_S1
                          }
                          dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                          setVisibleAgency1(true)
                        }}
                      />
                    </Col>
                    <Col span={24}>
                      <IButton
                        title='Chọn theo Danh sách đại lý cấp 2'
                        color={
                          eventImmediate.type_way ===
                          DefineKeyEvent.event_type_S2
                            ? 'white'
                            : colors.main
                        }
                        // disabled={
                        //   event.type_event_status ===
                        //   DefineKeyEvent.event_running
                        // }
                        styleHeight={{
                          background:
                            eventImmediate.type_way ===
                            DefineKeyEvent.event_type_S2
                              ? colors.main
                              : 'white'
                        }}
                        onClick={() => {
                          const data = {
                            key: 'type_way',
                            value: DefineKeyEvent.event_type_S2
                          }
                          dispatch(CREATE_DATE_EVENT_IMMEDIATE(data))
                          setVisibleAgency2(true)
                        }}
                      />
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <IButton
                  onClick={async () => {
                    let dateNow = new Date()
                    dateNow.setHours(
                      dateNow.getHours() + 1,
                      dateNow.getMinutes() - 5
                    )
                    // if (eventImmediate.priori === 1) {
                    //   try {
                    //     const data = await APIService._getCheckExistPriotyInEvent(
                    //       eventImmediate.valuePriori
                    //     )
                    //     if (data.exist_index === 0) {
                    //       return IError('Độ ưu tiên đã tồn tại')
                    //     }
                    //   } catch (error) {
                    //     console.log(error)
                    //   }
                    // }

                    if (
                      eventImmediate.from_time_apply === 0 ||
                      eventImmediate.to_time_apply === 0 ||
                      eventImmediate.type_way === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{' '}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian áp dụng
                          </span>{' '}
                          & <span style={{ fontWeight: 600 }}>trả thương</span>{' '}
                          ,{' '}
                          <span style={{ fontWeight: 600 }}>
                            đối tượng áp dụng
                          </span>{' '}
                          chương trình
                        </span>
                      )
                      return IError(content)
                    }

                    // if (eventImmediate.ISedit) {
                    //   if (
                    //     eventImmediate.type_event_status !==
                    //     DefineKeyEvent.event_running
                    //   ) {
                    //     if (
                    //       eventImmediate.from_time_apply <= dateNow.getTime()
                    //     ) {
                    //       const content = (
                    //         <span>
                    //           Ngày bắt đầu chương trình phải lớn hơn giờ hiện
                    //           tại 1 tiếng.
                    //         </span>
                    //       )
                    //       return IError(content)
                    //     }
                    //   }
                    // } else {
                    //   if (eventImmediate.from_time_apply <= dateNow.getTime()) {
                    //     const content = (
                    //       <span>
                    //         Ngày bắt đầu chương trình phải lớn hơn giờ hiện tại
                    //         1 tiếng.
                    //       </span>
                    //     )
                    //     return IError(content)
                    //   }
                    // }

                    switch (eventImmediate.type_way) {
                      case DefineKeyEvent.event_type_assigned:
                        history.push(`/event/immediate/create/filter/${id}`)
                        break
                      case DefineKeyEvent.event_type_S1:
                        history.push(`/event/immediate/create/filter/${id}`)
                        break
                      case DefineKeyEvent.event_type_S2:
                        history.push(`/event/immediate/create/filter/${id}`)
                        break
                      default:
                        break
                    }
                  }}
                  title='Tiếp tục'
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isVisibleAgency1}
        footer={null}
        width={1100}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1(
          () => {
            setVisibleAgency1(false)
          },
          () => {
            history.push(`/event/immediate/create/filter/agency/1/${id}`)
          }
        )}
      </Modal>
      <Modal
        visible={isVisibleAgency2}
        footer={null}
        width={1100}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS2(
          () => {
            setVisibleAgency2(false)
          },
          () => {
            history.push(`/event/immediate/create/filter/agency/2/${id}`)
          }
        )}
      </Modal>
    </div>
  )
}
