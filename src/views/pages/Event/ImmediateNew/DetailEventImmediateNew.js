import {
  BackTop,
  Col,
  Row,
  Skeleton,
  Popconfirm,
  Radio,
  Icon,
  Empty,
  message,
  List,
  Checkbox,
} from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import { IButton, ISvg, ITableHtml, ISelect } from "../../../components";
import FormatterDay from "../../../../utils/FormatterDay";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";
import { priceFormat } from "../../../../utils";
import { Fragment } from "react";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import { IError, StyledInputNumber } from "../../../components/common";
import { ASSIGNED_EVENT_INFO_DETAIL } from "../../../store/reducers";

export default function DetailEventImmediateNew() {
  const history = useHistory();
  const params = useParams();
  const [urlIMG, setUrlIMG] = useState("");
  const [urlPDF, setUrlPDF] = useState("");
  const [loadingButtonStatus, setLoadingButtonStatus] = useState(false);
  const [loadingButtonPriority, setLoadingButtonPriority] = useState(false);
  const dispatch = useDispatch();

  const [dataDetail, setDataDetail] = useState({
    list_application_object: [],
    list_cate_type: [],
    list_product_condition: [],
    lst_gift_reward_response: [],
    lst_region: [],
    lst_city: [],
    lst_event_child: [],
  });

  const [prveDataDetail, setPrveDataDetail] = useState({});

  const [dataNetwork, setDataNetWork] = useState({});

  const [listEventChild, setListEventChild] = useState([]);

  const [loading, setLoading] = useState(true);
  const { id, type } = params;

  const isCheckCondition =
    (dataDetail.application_type_id === 1 && dataDetail.reward_type_id !== 1) ||
    (dataDetail.application_type_id === 2 && dataDetail.reward_type_id !== 1) ||
    (dataDetail.application_type_id === 3 && dataDetail.reward_type_id !== 1);

  const _fetchAPIDetailEventImmediate = async (id) => {
    try {
      let data = await APIService._getDetailEventImmediate(id);

      data.event_response.lst_event_child = !data.event_response.lst_event_child
        ? []
        : data.event_response.lst_event_child;
      data.event_response.lst_event_child.map((item) => {
        item.list_cate_type.map((el) => (el.children = el.event_group_product));
      });
      data.event_response.image_url = data.image_url;
      data.event_response.type_event_status = Number(type);
      setDataDetail({ ...data.event_response });
      setPrveDataDetail({ ...JSON.parse(JSON.stringify(data.event_response)) });
      setUrlIMG(data.image_url);
      setUrlPDF(data.pdf_url);
      setDataNetWork({ ...JSON.parse(JSON.stringify(data)) });
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const _fetchEventChild = async (id) => {
    try {
      const data = await APIService._listEventChildParentID(id);

      setListEventChild([...data.listEvent]);
    } catch (error) {
    } finally {
    }
  };

  useEffect(() => {
    _fetchEventChild(id);
  }, []);

  const _putUpdateStatus = async (dataUpdate) => {
    try {
      await APIService._postUpdateStatusEventChildren(dataUpdate);
      message.success("Chỉnh sửa thành công");
      await _fetchAPIDetailEventImmediate(id);
    } catch (error) {
    } finally {
      setLoadingButtonStatus(false);
    }
  };

  const _postUpdatePriorityEvent = async (dataUpdate) => {
    try {
      await APIService._postUpdatePriorityEvent(dataUpdate);
      message.success("Chỉnh sửa thành công");
      await _fetchAPIDetailEventImmediate(id);
    } catch (error) {
    } finally {
      setLoadingButtonPriority(false);
    }
  };

  useEffect(() => {
    _fetchAPIDetailEventImmediate(id);
  }, []);

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp đại lý",
      align: "center",
    },
  ];

  const headerTable = (dataHeader) => {
    return (
      <tr className=" scroll">
        {dataHeader.map((item, index) => (
          <th className="th-table" style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const _renderDVLevel = () => {
    if (dataDetail.reward_type_id === 2) {
      return "đ/Quy cách";
    }
    if (dataDetail.reward_type_id === 1) {
      return "% Doanh số";
    }
    if (dataDetail.reward_type_id === 3) {
      return "";
    }
  };

  const bodyTable = (databody) => {
    return databody.map((item) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {item.user_agency_name}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {item.user_agency_code}
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "center",
            fontWeight: "normal",
            width: 80,
          }}
        >
          {item.level}
        </td>
      </tr>
    ));
  };

  const _renderUIObjectGreedAssign = () => {
    return (
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 1</p>
          <div
            style={{
              border: "1px solid rgba(122, 123, 123, 0.5)",
              padding: 6,
              marginTop: 12,
            }}
          >
            <p
              style={{
                borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                paddingBottom: 6,
              }}
            >
              Cấp người dùng
            </p>
            <p style={{ padding: "6px 0px" }}>{dataDetail.level}</p>
          </div>
        </Col>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 2</p>
          <div
            style={{
              padding: 6,
              border: "1px solid rgba(122, 123, 123, 0.5)",
              marginTop: 12,
            }}
          >
            <p
              style={{
                borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                paddingBottom: 6,
              }}
            >
              Cấp bậc
            </p>
            <p>
              {!dataDetail.lst_membership.length
                ? "-"
                : dataDetail.lst_membership.map(
                    (item, index) =>
                      `${item.name}${
                        index === dataDetail.lst_membership.length - 1
                          ? ""
                          : ", "
                      }`
                  )}
            </p>
          </div>
        </Col>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 3</p>
          <div
            style={{
              border: "1px solid rgba(122, 123, 123, 0.5)",
              padding: 6,
              marginTop: 12,
            }}
          >
            <p
              style={{
                borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                paddingBottom: 6,
              }}
            >
              Vùng địa lý
            </p>
            <p style={{ padding: "6px 0px" }}>
              {dataDetail.lst_region.map((item) => {
                if (dataDetail.lst_region.length === 1) {
                  return <span>{item.name}</span>;
                }
                return <span>{item.name + ", "}</span>;
              })}
            </p>
          </div>
        </Col>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 3</p>
          <div
            style={{
              border: "1px solid rgba(122, 123, 123, 0.5)",
              padding: 6,
              marginTop: 12,
            }}
          >
            <p
              style={{
                borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                paddingBottom: 6,
              }}
            >
              Tỉnh / Thành phố
            </p>
            <p style={{ padding: "6px 0px", maxHeight: 100, overflow: "auto" }}>
              {dataDetail.lst_city.map((item) => {
                if (dataDetail.lst_city.length === 1) {
                  return <span>{item.name}</span>;
                }
                return <span>{item.name + ", "}</span>;
              })}
            </p>
          </div>
        </Col>
      </Row>
    );
  };

  const treeProduct = (dataProduct, idxRoot) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProduct(item.children, idxRoot)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={24}>
                      <div
                        style={{
                          overflow: "hidden",
                          height: "100%",
                          paddingLeft: 30,
                        }}
                      >
                        <div
                          style={{
                            wordWrap: "break-word",
                            wordBreak: "break-word",
                            color: colors.blackChart,
                          }}
                        >
                          {item.name}
                        </div>
                        <div style={{ fontStyle: "italic" }}>
                          {item.description}
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  const _renderCondition = (type, style, data, idx) => {
    if (type === 1) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo`}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>Doanh số nguyên đơn</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Doanh số nguyên đơn</p>
                  <span>{priceFormat(data.condition) + "đ"}</span>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
    if (type === 2) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo `}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>
                    {style === 1 ? "Doanh số sản phẩm" : "Số lượng sản phẩm"}
                  </span>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện sản phẩm</p>
                  <span>Danh sách sản phẩm</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <div
                    style={{
                      padding: "12px 12px 36px 12px",
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Row gutter={[24, 24]}>
                      <Col span={24}>
                        <React.Fragment>
                          <Row gutter={[12, 12]}>
                            <Col span={14}>
                              <span style={{ fontWeight: 600 }}>Sản phẩm</span>
                            </Col>
                            <Col span={10} style={{ textAlign: "right" }}>
                              <span style={{ fontWeight: 600, marginRight: 6 }}>
                                {style === 1 ? "Doanh số" : "Số lượng sản phẩm"}
                              </span>
                            </Col>
                          </Row>
                        </React.Fragment>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            height: 350,
                            overflow: "auto",
                            padding: "0px 12px",
                          }}
                        >
                          {data.lsProduct.map((item) => {
                            return (
                              <div
                                style={{ borderBottom: "0.5px dashed #7A7B7B" }}
                              >
                                <React.Fragment>
                                  <Row gutter={[12, 12]}>
                                    <Col span={14}>
                                      <span>{item.product_name}</span>
                                    </Col>
                                    <Col
                                      span={10}
                                      style={{ textAlign: "right" }}
                                    >
                                      <span>
                                        {style === 1
                                          ? priceFormat(item.condition) + " đ"
                                          : priceFormat(item.condition)}
                                      </span>
                                    </Col>
                                  </Row>
                                </React.Fragment>
                              </div>
                            );
                          })}
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
    if (type === 3) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo}`}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>
                    {style === 1 ? "Doanh số sản phẩm" : "Số lượng sản phẩm"}{" "}
                  </span>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện sản phẩm</p>
                  <span>Nhóm sản phẩm</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <div
                    style={{
                      padding: "12px 12px 36px 12px",
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Row gutter={[24, 24]}>
                      <Col span={24}>
                        <React.Fragment>
                          <Row gutter={[12, 12]}>
                            <Col span={14}>
                              <span style={{ fontWeight: 600 }}>Nhóm hàng</span>
                            </Col>
                            <Col span={10} style={{ textAlign: "right" }}>
                              <span style={{ fontWeight: 600, marginRight: 4 }}>
                                {style === 1 ? "Doanh số" : "Số lượng"}
                              </span>
                            </Col>
                          </Row>
                        </React.Fragment>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            height: 350,
                            overflow: "auto",
                            padding: "0px 12px",
                          }}
                        >
                          {data.lsGroup.map((item, index) => {
                            return (
                              <div style={{ marginBottom: 24 }}>
                                <p style={{ fontWeight: 600 }}>{`Nhóm hàng ${
                                  index + 1
                                }`}</p>
                                <>
                                  {item.lst_product.map((item1, index1) => {
                                    return (
                                      <div
                                        style={{
                                          borderBottom: "0.5px dashed #7A7B7B",
                                        }}
                                      >
                                        <React.Fragment>
                                          <Row gutter={[12, 12]}>
                                            <Col span={14}>
                                              <span>{item1.product_name}</span>
                                            </Col>
                                            <Col
                                              span={10}
                                              style={{ textAlign: "right" }}
                                            >
                                              <span>
                                                {style === 1
                                                  ? priceFormat(
                                                      item1.condition
                                                    ) + " đ"
                                                  : priceFormat(
                                                      item1.condition
                                                    )}
                                              </span>
                                            </Col>
                                          </Row>
                                        </React.Fragment>
                                      </div>
                                    );
                                  })}
                                </>
                              </div>
                            );
                          })}
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
  };

  const renderItemSalesOrCount = (item, data) => {
    const even = (element) =>
      element[
        dataDetail.application_type_id === 2 ? "quantity_max" : "money_max"
      ] === 0;
    const check_1_limit = data.some((item11) => even(item11));

    const isCheck_1_limit = check_1_limit && data.length === 1;
    return (
      <Col span={24}>
        <Row>
          <Col span={24}>
            <div
              style={{
                borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                borderBottom: "1px dashed rgba(122, 123, 123, 0.5)",
                borderTop: "1px solid rgba(122, 123, 123, 0.5)",
                padding: "8px 15px",
              }}
            >
              <Row>
                <Col span={8}>
                  <div
                    style={{
                      marginTop: 4,
                    }}
                  >
                    <span style={{ fontWeight: 600 }}>Mức xét thưởng</span>
                  </div>
                </Col>
                <Col
                  span={isCheck_1_limit ? 10 : 8}
                  offset={isCheck_1_limit ? 6 : 0}
                >
                  <div
                    style={{
                      marginTop: 4,
                    }}
                  >
                    <span>
                      Từ:{" "}
                      {priceFormat(
                        dataDetail.application_type_id === 1 ||
                          dataDetail.application_type_id === 3
                          ? item.money_min
                          : item.quantity_min
                      ) +
                        `${
                          dataDetail.application_type_id === 1 ||
                          dataDetail.application_type_id === 3
                            ? " đ"
                            : ""
                        }`}
                    </span>
                  </div>
                </Col>
                {isCheck_1_limit ? null : (
                  <Col span={8}>
                    <div
                      style={{
                        marginTop: 4,
                      }}
                    >
                      <span>
                        Đến dưới:{" "}
                        {priceFormat(
                          dataDetail.application_type_id === 1 ||
                            dataDetail.application_type_id === 3
                            ? item.money_max
                            : item.quantity_max
                        ) +
                          `${
                            dataDetail.application_type_id === 1 ||
                            dataDetail.application_type_id === 3
                              ? " đ"
                              : ""
                          }`}
                      </span>
                    </div>
                  </Col>
                )}
              </Row>
            </div>
          </Col>
          <Col span={24}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                borderTop: "1px solid white",
                borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                padding: "15px 15px",
              }}
            >
              {dataDetail.application_type_id === 3 ? (
                <Row style={{ width: "100%" }} gutter={[12, 0]}>
                  <Col span={24}>
                    <Row style={{ marginBottom: 15 }}>
                      <Col span={dataDetail.reward_type_id !== 1 ? 10 : 14}>
                        <div>
                          <span style={{ fontWeight: 600 }}>
                            {dataDetail.reward_type_id === 1
                              ? "Tỷ lệ chiết khấu"
                              : "Tỷ lệ thưởng"}
                          </span>
                        </div>
                      </Col>
                      {dataDetail.reward_type_id !== 1 ? (
                        <Fragment>
                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách lớn
                            </span>
                          </Col>

                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách nhỏ
                            </span>
                          </Col>
                        </Fragment>
                      ) : (
                        <Col span={10}>
                          <span style={{ fontWeight: 600 }}>
                            {`${(item.vat * 100).toFixed(2)} % Doanh số`}
                          </span>
                        </Col>
                      )}
                    </Row>
                  </Col>

                  <Col span={24}>
                    <div>
                      {item.lst_product.map((itemProduct, indexProduct) => {
                        return (
                          <Col span={24}>
                            <Row
                              type="flex"
                              style={{
                                borderBottom:
                                  "1px dashed rgba(122, 123, 123, 0.5)",
                                paddingBottom: 10,
                                paddingTop: 10,
                              }}
                            >
                              <Col span={isCheckCondition ? 10 : 14}>
                                <div
                                  style={{
                                    height: "100%",
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <span>{itemProduct.product_name}</span>
                                </div>
                              </Col>
                              {isCheckCondition ? (
                                <Fragment>
                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        dataDetail.reward_type_id === 3
                                          ? !itemProduct.quantity_max
                                            ? 0
                                            : itemProduct.quantity_max
                                          : !itemProduct.money_max
                                          ? 0
                                          : itemProduct.money_max
                                      ) + ` ${_renderDVLevel()}`}
                                    </span>
                                  </Col>

                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        dataDetail.reward_type_id === 3
                                          ? !itemProduct.quantity_min
                                            ? 0
                                            : itemProduct.quantity_min
                                          : !itemProduct.money_min
                                          ? 0
                                          : itemProduct.money_min
                                      ) + ` ${_renderDVLevel()}`}
                                    </span>
                                  </Col>
                                </Fragment>
                              ) : (
                                <Col span={10} style={{ textAlign: "center" }}>
                                  <span>
                                    {(itemProduct.discount * 100).toFixed(2)}
                                    {_renderDVLevel()}
                                  </span>
                                </Col>
                              )}
                            </Row>
                          </Col>
                        );
                      })}
                    </div>
                  </Col>
                </Row>
              ) : (
                <Row style={{ width: "100%" }} gutter={[12, 0]}>
                  <Col span={24}>
                    <Row style={{ marginBottom: 15 }}>
                      <Col span={isCheckCondition ? 10 : 14}>
                        <div>
                          <span style={{ fontWeight: 600 }}>Tỷ lệ thưởng</span>
                        </div>
                      </Col>
                      {isCheckCondition ? (
                        <Fragment>
                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách lớn
                            </span>
                          </Col>

                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách nhỏ
                            </span>
                          </Col>
                        </Fragment>
                      ) : (
                        <Col span={10} style={{ textAlign: "center" }}>
                          <span style={{ fontWeight: 600 }}>
                            Tỉ lệ chiết khấu
                          </span>
                        </Col>
                      )}
                    </Row>
                  </Col>

                  <Col span={24}>
                    <div>
                      {item.lst_product.map((itemProduct, indexProduct) => {
                        return (
                          <Col span={24}>
                            <Row
                              type="flex"
                              style={{
                                borderBottom:
                                  "1px dashed rgba(122, 123, 123, 0.5)",
                                paddingBottom: 10,
                                paddingTop: 10,
                              }}
                            >
                              <Col span={isCheckCondition ? 10 : 14}>
                                <div
                                  style={{
                                    height: "100%",
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <span>{itemProduct.product_name}</span>
                                </div>
                              </Col>
                              {isCheckCondition ? (
                                <Fragment>
                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        dataDetail.reward_type_id === 3
                                          ? !itemProduct.quantity_max
                                            ? 0
                                            : itemProduct.quantity_max
                                          : !itemProduct.money_max
                                          ? 0
                                          : itemProduct.money_max
                                      ) + ` ${_renderDVLevel()}`}
                                    </span>
                                  </Col>

                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        dataDetail.reward_type_id === 3
                                          ? !itemProduct.quantity_min
                                            ? 0
                                            : itemProduct.quantity_min
                                          : !itemProduct.money_min
                                          ? 0
                                          : itemProduct.money_min
                                      ) + ` ${_renderDVLevel()}`}
                                    </span>
                                  </Col>
                                </Fragment>
                              ) : (
                                <Col span={10} style={{ textAlign: "center" }}>
                                  <span>
                                    {(itemProduct.discount * 100).toFixed(2)}
                                    {_renderDVLevel()}
                                  </span>
                                </Col>
                              )}
                            </Row>
                          </Col>
                        );
                      })}
                    </div>
                  </Col>
                </Row>
              )}
            </div>
          </Col>
        </Row>
      </Col>
    );
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <BackTop />
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Chi tiết Chương trình khuyến mãi trên đơn
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            <IButton
              title="Thêm chương trình con"
              color={colors.main}
              styleHeight={{
                width: 200,
                marginRight: 12,
              }}
              onClick={() => {
                dispatch(ASSIGNED_EVENT_INFO_DETAIL(dataNetwork));
                history.push(`/create/event/immediate/design/${id}`);
              }}
            />
            <IButton
              title="Hủy"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => history.push("/event/immediate/list")}
            />
          </div>
        </Col>

        <Col span={24}>
          <div
            className="border-full"
            style={{
              padding: 12,
              border: "1px solid rgba(122, 123, 123, 0.5)",
            }}
          >
            <Skeleton loading={loading} active paragraph={{ rows: 16 }}>
              <Fragment>
                <Row gutter={[12, 12]} type="flex">
                  <Col span={16}>
                    <div className="boder-full box-shadow">
                      <Row gutter={[12, 16]}>
                        <Col span={24}>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Mô tả & nguyên tắc chương trình
                          </StyledITileHeading>
                        </Col>
                        <Col span={12}>
                          <p style={{ fontWeight: 600 }}>
                            Thông tin chương trình
                          </p>
                        </Col>
                        <Col span={12} style={{ textAlign: "center" }}>
                          <p style={{ fontWeight: 600 }}>
                            Thời gian áp dụng & Trả thưởng
                          </p>
                        </Col>
                        <Col span={24}>
                          <Fragment>
                            <Row gutter={[12, 12]}>
                              <Col span={12}>
                                <Fragment>
                                  <Row gutter={[12, 12]}>
                                    <Col span={24}>
                                      <div>
                                        <div style={{ display: "flex" }}>
                                          <p style={{ fontWeight: 600 }}>
                                            Tên chương trình
                                          </p>
                                          {dataDetail.is_show_event_name !==
                                          0 ? null : (
                                            <span
                                              style={{
                                                marginLeft: 8,
                                                color: "#7c7c7d",
                                              }}
                                            >
                                              (Ẩn tên CTKM)
                                            </span>
                                          )}
                                        </div>

                                        <span>
                                          {!dataDetail.name
                                            ? "-"
                                            : dataDetail.name}
                                        </span>
                                      </div>
                                    </Col>
                                    <Col span={24}>
                                      <p style={{ fontWeight: 600 }}>
                                        Mã chương trình
                                      </p>
                                      <span>
                                        {!dataDetail.code
                                          ? "-"
                                          : dataDetail.code}
                                      </span>
                                    </Col>

                                    <Col span={24}>
                                      <p style={{ fontWeight: 600 }}>
                                        Trạng thái chương trình
                                      </p>
                                      <span>
                                        {!dataDetail.status_name
                                          ? "-"
                                          : dataDetail.status_name}
                                      </span>
                                    </Col>
                                  </Row>
                                </Fragment>
                              </Col>
                              <Col span={12}>
                                <Fragment>
                                  <Row gutter={[12, 12]}>
                                    {/* <Col span={24}>
                                      <StyledITileHeading
                                        minFont="10px"
                                        maxFont="16px"
                                      >
                                        Thời gian áp dụng & trả thưởng
                                      </StyledITileHeading>
                                    </Col> */}
                                    <Col span={24}>
                                      <StyledITileHeading
                                        minFont="10px"
                                        maxFont="15px"
                                      >
                                        Thời gian áp dụng
                                      </StyledITileHeading>
                                    </Col>
                                    <Col span={24}>
                                      <div style={{ marginTop: 12 }}>
                                        <Row gutter={[24, 0]}>
                                          <Col span={12}>
                                            <p style={{ fontWeight: 600 }}>
                                              Bắt đầu
                                            </p>
                                            <span>
                                              {!dataDetail.start_date
                                                ? "-"
                                                : FormatterDay.dateFormatWithString(
                                                    dataDetail.start_date,
                                                    "#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#"
                                                  )}
                                            </span>
                                          </Col>
                                          <Col span={12}>
                                            <p style={{ fontWeight: 600 }}>
                                              Kết thúc
                                            </p>
                                            <span>
                                              {!dataDetail.end_date
                                                ? "-"
                                                : FormatterDay.dateFormatWithString(
                                                    dataDetail.end_date,
                                                    "#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#"
                                                  )}
                                            </span>
                                          </Col>
                                        </Row>
                                      </div>
                                    </Col>
                                    <Col span={24}>
                                      <div style={{ marginTop: 12 }}>
                                        <StyledITileHeading
                                          minFont="10px"
                                          maxFont="16px"
                                        >
                                          Đối tượng áp dụng
                                        </StyledITileHeading>
                                      </div>
                                    </Col>
                                    <Col span={24}>
                                      {dataDetail.type_entry_condition === 3 ? (
                                        _renderUIObjectGreedAssign()
                                      ) : (
                                        <ITableHtml
                                          height="420px"
                                          childrenBody={bodyTable(
                                            dataDetail.list_application_object
                                          )}
                                          childrenHeader={headerTable(
                                            dataHeader
                                          )}
                                          style={{
                                            border:
                                              "1px solid rgba(122, 123, 123, 0.5)",
                                            borderTop: "unset",
                                          }}
                                          isBorder={false}
                                        />
                                      )}
                                    </Col>
                                    {dataDetail.is_event_sync_past === 1 ? (
                                      <Col span={24}>
                                        <div
                                          style={{
                                            display: "flex",
                                            flexDirection: "row",
                                            alignItems: "center",
                                          }}
                                        >
                                          <img
                                            src={images.check}
                                            alt=""
                                            width="12px"
                                            height="12px"
                                          />
                                          <span
                                            style={{
                                              marginLeft: 15,
                                              fontWeight: 600,
                                            }}
                                          >
                                            Thiết lập chương trình về thời gian
                                            quá khứ
                                          </span>
                                        </div>
                                      </Col>
                                    ) : null}
                                  </Row>
                                </Fragment>
                              </Col>
                            </Row>
                          </Fragment>
                        </Col>
                      </Row>
                    </div>
                  </Col>

                  <Col span={8}>
                    <div className="boder-full box-shadow">
                      <Row gutter={[12, 16]}>
                        <Col span={24}>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Hình ảnh minh họa
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          {!dataDetail.image ? (
                            "-"
                          ) : (
                            <img
                              src={urlIMG + dataDetail.image}
                              alt="hình-event"
                              style={{
                                width: "100%",
                                height: 300,
                                objectFit: "cover",
                              }}
                            />
                          )}
                        </Col>

                        <Col span={24}>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Nội dung mô tả
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <pre>
                            {!dataDetail.description
                              ? "-"
                              : dataDetail.description}
                          </pre>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                  <Col span={24}>
                    <div className="boder-full box-shadow">
                      <Row gutter={[12, 16]}>
                        <Col span={12}>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            {`Thể lệ & Chi tiết thiết lập`}
                          </StyledITileHeading>
                        </Col>
                        <Col span={12}>
                          <div
                            style={{
                              width: "100%",
                              display: "flex",
                              justifyContent: "flex-end",
                            }}
                          >
                            <Popconfirm
                              title="Nhấn OK để lưu thiết lập."
                              onConfirm={() => {
                                const dataUpdate =
                                  dataDetail.lst_event_child.map((item) => {
                                    const event_priority_type =
                                      item.event_priority_type;
                                    const lst_excommunicate_event_ids =
                                      item.event_priority_type ===
                                      DefineKeyEvent.song_song
                                        ? []
                                        : [...item.lst_excommunicate_event_ids];
                                    const value_priority =
                                      item.event_priority_type ===
                                      DefineKeyEvent.song_song
                                        ? 0
                                        : item.sort_order;
                                    const lst_event_id = [item.event_id];
                                    return {
                                      event_priority_type,
                                      lst_excommunicate_event_ids,
                                      value_priority,
                                      lst_event_id,
                                    };
                                  });

                                const dataFilter = dataUpdate.filter(
                                  (element) =>
                                    element.event_priority_type ===
                                    DefineKeyEvent.uu_tien
                                );
                                const arrayPriority = dataFilter.map(
                                  (el) => el.value_priority
                                );

                                const arrayPriorityNewSet = [
                                  ...new Set(arrayPriority),
                                ];
                                if (
                                  arrayPriority.length >
                                  arrayPriorityNewSet.length
                                ) {
                                  return IError(
                                    "Độ ưu tiên không được trùng nhau."
                                  );
                                }
                                setLoadingButtonPriority(true);
                                _postUpdatePriorityEvent(dataUpdate);
                              }}
                              onCancel={() => {
                                setDataDetail({ ...prveDataDetail });
                                message.success("Hủy thiết lập thành công");
                              }}
                            >
                              <IButton
                                loading={loadingButtonPriority}
                                title="Lưu thiết lập ưu tiên"
                                color={colors.main}
                                icon={ISvg.NAME.SAVE}
                                styleHeight={{
                                  width: 220,
                                }}
                              />
                            </Popconfirm>
                          </div>
                        </Col>

                        <Col span={24}>
                          <StyledITileHeading minFont="10px" maxFont="15px">
                            Ngành hàng áp dụng
                          </StyledITileHeading>
                          <span>
                            {!dataDetail.company_name
                              ? "-"
                              : dataDetail.company_name}
                          </span>
                        </Col>
                        <Col span={24}>
                          <div className="boder-full">
                            <Fragment>
                              <Row gutter={[16, 16]}>
                                <Col span={24}>
                                  <StyledITileHeading
                                    minFont="10px"
                                    maxFont="16px"
                                  >
                                    Điều kiện trả thưởng
                                  </StyledITileHeading>
                                </Col>
                                <Col span={24}>
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                    }}
                                  >
                                    <div>
                                      <StyledITileHeading
                                        minFont="10px"
                                        maxFont="14px"
                                      >
                                        Điều kiện khuyến mãi
                                      </StyledITileHeading>
                                      <span>
                                        {!dataDetail.application_type_name
                                          ? "-"
                                          : dataDetail.application_type_name}
                                      </span>
                                    </div>
                                    <div style={{ margin: "0px 72px" }}>
                                      <StyledITileHeading
                                        minFont="10px"
                                        maxFont="14px"
                                      >
                                        Phần thưởng khuyến mãi
                                      </StyledITileHeading>
                                      <span>
                                        {!dataDetail.reward_type_name
                                          ? "-"
                                          : dataDetail.reward_type_name}
                                      </span>
                                    </div>
                                    {dataDetail.application_type_id === 2 ? (
                                      <div>
                                        <StyledITileHeading
                                          minFont="10px"
                                          maxFont="14px"
                                        >
                                          Quy cách khuyến mãi
                                        </StyledITileHeading>
                                        <span>
                                          {dataDetail.product_atrtibute_type ===
                                          2
                                            ? "Quy cách lớn"
                                            : "Quy cách nhỏ"}
                                        </span>
                                      </div>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                            </Fragment>
                          </div>
                        </Col>
                        <Col span={24} style={{ overflowX: "auto" }}>
                          <div>
                            <Row>
                              <Col span={24}>
                                <Row>
                                  <Col span={6}>
                                    <div
                                      style={{
                                        textAlign: "center",
                                        borderLeft: "1px solid #C4C4C4",
                                        borderTop: "1px solid #C4C4C4",
                                        borderRight: "1px solid #C4C4C4",
                                        borderBottom: "1px solid #C4C4C4",
                                        padding: "10px 0px",
                                      }}
                                    >
                                      <StyledITileHeading
                                        minFont="10px"
                                        maxFont="14px"
                                      >
                                        Danh mục Sản phẩm áp dụng
                                      </StyledITileHeading>
                                    </div>
                                  </Col>
                                  <Col span={12}>
                                    <div
                                      style={{
                                        textAlign: "center",
                                        borderTop: "1px solid #C4C4C4",
                                        borderRight: "1px solid #C4C4C4",
                                        borderBottom: "1px solid #C4C4C4",
                                        padding: "10px 0px",
                                      }}
                                    >
                                      <StyledITileHeading
                                        minFont="10px"
                                        maxFont="14px"
                                      >
                                        Hạn mức thưởng
                                      </StyledITileHeading>
                                    </div>
                                  </Col>
                                  <Col span={6}>
                                    <div
                                      style={{
                                        textAlign: "center",
                                        borderTop: "1px solid #C4C4C4",
                                        borderRight: "1px solid #C4C4C4",
                                        borderBottom: "1px solid #C4C4C4",
                                        padding: "10px 0px",
                                      }}
                                    >
                                      <StyledITileHeading
                                        minFont="10px"
                                        maxFont="14px"
                                      >
                                        Điều kiện kèm theo
                                      </StyledITileHeading>
                                    </div>
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={24}>
                                {!dataDetail.lst_event_child
                                  ? null
                                  : dataDetail.lst_event_child.map(
                                      (item, index) => {
                                        return (
                                          <>
                                            <Col span={24}>
                                              <div
                                                style={{
                                                  borderLeft:
                                                    "1px solid #C4C4C4",
                                                  borderRight:
                                                    "1px solid #C4C4C4",
                                                  borderBottom:
                                                    "1px solid #C4C4C4",
                                                  padding: "10px 15px",
                                                  display: "flex",
                                                  flexDirection: "row",
                                                  justifyContent:
                                                    "space-between",
                                                  alignItems: "center",
                                                }}
                                              >
                                                <div>
                                                  <StyledITileHeading
                                                    minFont="10px"
                                                    maxFont="16px"
                                                    style={{
                                                      display: "inline-block",
                                                    }}
                                                  >
                                                    Chương trình {index + 1}
                                                  </StyledITileHeading>
                                                  <StyledITileHeading
                                                    style={{
                                                      display: "inline-block",
                                                      marginLeft: 48,
                                                      color:
                                                        item.status === 1
                                                          ? colors.main
                                                          : colors.oranges,
                                                    }}
                                                    minFont="10px"
                                                    maxFont="16px"
                                                  >
                                                    {item.status_name}
                                                  </StyledITileHeading>
                                                </div>
                                                {/* <Popconfirm
                                                  title={`Nhấn OK để ${
                                                    item.status === 1
                                                      ? 'Dừng chương trình'
                                                      : 'Mở chương trình'
                                                  } `}
                                                  onConfirm={() => {
                                                    setLoadingButtonStatus(true)
                                                    const dataUpdate = {
                                                      id: [item.event_id],
                                                      status:
                                                        item.status === 1
                                                          ? 2
                                                          : 1
                                                    }
                                                    _putUpdateStatus(dataUpdate)
                                                  }}
                                                >
                                                  <IButton
                                                    loading={
                                                      loadingButtonStatus
                                                    }
                                                    title={
                                                      item.status === 1
                                                        ? 'Dừng chương trình'
                                                        : 'Mở chương trình'
                                                    }
                                                    color={
                                                      item.status === 1
                                                        ? colors.oranges
                                                        : colors.main
                                                    }
                                                    styleHeight={{
                                                      width: 180
                                                    }}
                                                  />
                                                </Popconfirm> */}
                                              </div>
                                            </Col>

                                            <Col span={24}>
                                              <div
                                                style={{
                                                  borderLeft:
                                                    "1px solid #C4C4C4",
                                                  borderRight:
                                                    "1px solid #C4C4C4",
                                                  borderBottom:
                                                    "1px solid #C4C4C4",
                                                  padding: "10px 15px",
                                                }}
                                              >
                                                <Row type="flex">
                                                  <Col span={6}>
                                                    <StyledITileHeading
                                                      minFont="10px"
                                                      maxFont="14px"
                                                    >
                                                      Mức độ ưu tiên
                                                    </StyledITileHeading>
                                                    <Radio.Group
                                                      style={{
                                                        marginTop: 6,
                                                      }}
                                                      value={
                                                        item.event_priority_type
                                                      }
                                                      onChange={(e) => {
                                                        item.event_priority_type =
                                                          Number(
                                                            e.target.value
                                                          );
                                                        setDataDetail({
                                                          ...dataDetail,
                                                        });
                                                      }}
                                                    >
                                                      <Radio
                                                        value={
                                                          DefineKeyEvent.song_song
                                                        }
                                                      >
                                                        Song song
                                                      </Radio>
                                                      <Radio
                                                        value={
                                                          DefineKeyEvent.uu_tien
                                                        }
                                                      >
                                                        Ưu tiên
                                                      </Radio>
                                                    </Radio.Group>
                                                  </Col>
                                                  {item.event_priority_type ===
                                                  DefineKeyEvent.song_song ? null : (
                                                    <>
                                                      <Col
                                                        span={12}
                                                        style={{
                                                          textAlign: "center",
                                                        }}
                                                      >
                                                        <StyledITileHeading
                                                          minFont="10px"
                                                          maxFont="14px"
                                                        >
                                                          Độ ưu tiên
                                                        </StyledITileHeading>
                                                        <StyledInputNumber
                                                          className="disabled-up"
                                                          formatter={(value) =>
                                                            `${value}`.replace(
                                                              /\B(?=(\d{3})+(?!\d))/g,
                                                              ","
                                                            )
                                                          }
                                                          min={0}
                                                          value={
                                                            item.sort_order
                                                          }
                                                          onChange={(value) => {
                                                            item.sort_order =
                                                              value;

                                                            setDataDetail({
                                                              ...dataDetail,
                                                            });
                                                          }}
                                                          parser={(value) =>
                                                            value.replace(
                                                              /\$\s?|(,*)/g,
                                                              ""
                                                            )
                                                          }
                                                          style={{
                                                            marginTop: 6,
                                                            borderRadius: 0,
                                                            width: 120,
                                                          }}
                                                        />
                                                      </Col>
                                                      <Col
                                                        span={6}
                                                        style={{
                                                          textAlign: "center",
                                                        }}
                                                      >
                                                        <StyledITileHeading
                                                          minFont="10px"
                                                          maxFont="14px"
                                                        >
                                                          Loại trừ
                                                        </StyledITileHeading>
                                                        <div
                                                          style={{
                                                            marginTop: 6,
                                                            maxHeight: 80,
                                                            overflow: "auto",
                                                            textAlign: "left",
                                                            border:
                                                              "1px solid #C4C4C4",
                                                            padding: 6,
                                                          }}
                                                        >
                                                          {listEventChild.map(
                                                            (item1) => {
                                                              let dataNew =
                                                                !item.lst_excommunicate_event_ids
                                                                  ? []
                                                                  : item.lst_excommunicate_event_ids;
                                                              const check =
                                                                dataNew.includes(
                                                                  item1.id
                                                                );
                                                              return (
                                                                <Checkbox
                                                                  style={{
                                                                    display:
                                                                      "block",
                                                                    marginLeft: 8,
                                                                  }}
                                                                  checked={
                                                                    check
                                                                  }
                                                                  onClick={(
                                                                    e
                                                                  ) => {
                                                                    if (
                                                                      e.target
                                                                        .checked
                                                                    ) {
                                                                      item.lst_excommunicate_event_ids.push(
                                                                        item1.id
                                                                      );
                                                                    } else {
                                                                      const index =
                                                                        item.lst_excommunicate_event_ids.indexOf(
                                                                          item1.id
                                                                        );
                                                                      item.lst_excommunicate_event_ids.splice(
                                                                        index,
                                                                        1
                                                                      );
                                                                    }
                                                                    setDataDetail(
                                                                      {
                                                                        ...dataDetail,
                                                                      }
                                                                    );
                                                                  }}
                                                                  key={item1.id}
                                                                >
                                                                  {item1.name}
                                                                </Checkbox>
                                                              );
                                                            }
                                                          )}
                                                        </div>
                                                      </Col>
                                                    </>
                                                  )}
                                                </Row>
                                              </div>
                                            </Col>

                                            <Col span={24}>
                                              <Row
                                                style={
                                                  {
                                                    // minHeight: 200,
                                                    // maxHeight: 600,
                                                    // border: "1px solid black",
                                                  }
                                                }
                                                type="flex"
                                              >
                                                <Col span={6}>
                                                  <div
                                                    style={{
                                                      borderLeft:
                                                        "1px solid #C4C4C4",
                                                      borderRight:
                                                        "1px solid #C4C4C4",
                                                      borderBottom:
                                                        "1px solid #C4C4C4",
                                                      padding: "10px 10px",
                                                      height: "100%",
                                                    }}
                                                  >
                                                    <Row>
                                                      <Col span={24}>
                                                        <div
                                                          style={{
                                                            border:
                                                              "1px solid #C4C4C4",
                                                            padding: "8px 8px",
                                                          }}
                                                        >
                                                          <Fragment>
                                                            <Row type="flex">
                                                              <Col
                                                                span={
                                                                  dataDetail.application_type_id ===
                                                                  2
                                                                    ? 18
                                                                    : 24
                                                                }
                                                              >
                                                                <div
                                                                  style={{
                                                                    height:
                                                                      "100%",
                                                                    display:
                                                                      "flex",
                                                                    alignItems:
                                                                      "center",
                                                                  }}
                                                                >
                                                                  <p>
                                                                    DANH SÁCH
                                                                    SẢN PHẨM
                                                                  </p>
                                                                </div>
                                                              </Col>
                                                              {dataDetail.application_type_id ===
                                                              2 ? (
                                                                <Col span={6}>
                                                                  <div
                                                                    style={{
                                                                      height:
                                                                        "100%",
                                                                      display:
                                                                        "flex",
                                                                      alignItems:
                                                                        "center",
                                                                      justifyContent:
                                                                        "center",
                                                                      textAlign:
                                                                        "center",
                                                                    }}
                                                                  >
                                                                    <p>
                                                                      Tỉ lệ quy
                                                                      đổi
                                                                    </p>
                                                                  </div>
                                                                </Col>
                                                              ) : null}
                                                            </Row>
                                                          </Fragment>
                                                        </div>
                                                      </Col>

                                                      <Col span={24}>
                                                        <div
                                                          style={{
                                                            borderLeft:
                                                              "1px solid #C4C4C4",
                                                            borderRight:
                                                              "1px solid #C4C4C4",
                                                            borderBottom:
                                                              "1px solid #C4C4C4",
                                                            padding: "8px 8px",
                                                            // height: "100%",
                                                            // overflow: "auto",
                                                          }}
                                                        >
                                                          {treeProduct(
                                                            item.list_cate_type
                                                          )}
                                                        </div>
                                                      </Col>
                                                    </Row>
                                                  </div>
                                                </Col>
                                                <Col span={12}>
                                                  <div
                                                    style={{
                                                      padding: "10px 10px",
                                                      borderBottom:
                                                        "1px solid #C4C4C4",
                                                      borderRight:
                                                        "1px solid #C4C4C4",
                                                      height: "100%",
                                                    }}
                                                  >
                                                    <div
                                                      style={{
                                                        borderRight:
                                                          "1px solid #C4C4C4",
                                                        borderBottom:
                                                          "1px solid #C4C4C4",
                                                        padding:
                                                          "8px 10px 10px 10px",
                                                        borderLeft:
                                                          "1px solid #C4C4C4",
                                                        borderTop:
                                                          "1px solid #C4C4C4",
                                                      }}
                                                    >
                                                      <Row>
                                                        <Col span={24}>
                                                          <div
                                                            style={{
                                                              // borderLeft: "1px solid #C4C4C4",
                                                              // borderRight: "1px solid #C4C4C4",
                                                              // borderBottom: "1px solid #C4C4C4",
                                                              // borderTop: "1px solid #C4C4C4",
                                                              padding:
                                                                "0px 8px 8px 0px",
                                                            }}
                                                          >
                                                            <p>
                                                              {!dataDetail.application_type_name
                                                                ? "-"
                                                                : dataDetail.application_type_name}
                                                            </p>
                                                          </div>
                                                        </Col>
                                                        <Col span={24}>
                                                          <div
                                                            style={
                                                              {
                                                                // borderLeft: "1px solid #C4C4C4",
                                                                // borderRight: "1px solid #C4C4C4",
                                                                // borderBottom: "1px solid #C4C4C4",
                                                                // padding: "8px 8px",
                                                                // height: 300,
                                                                // overflow: "auto",
                                                              }
                                                            }
                                                          >
                                                            {item.list_product_condition.map(
                                                              (item1) => {
                                                                return renderItemSalesOrCount(
                                                                  item1,
                                                                  item.list_product_condition
                                                                );
                                                              }
                                                            )}
                                                          </div>
                                                        </Col>
                                                      </Row>
                                                    </div>
                                                  </div>
                                                </Col>

                                                <Col span={6}>
                                                  <div
                                                    style={{
                                                      borderRight:
                                                        "1px solid #C4C4C4",
                                                      borderBottom:
                                                        "1px solid #C4C4C4",
                                                      padding: "10px 10px",
                                                      height: "100%",
                                                    }}
                                                  >
                                                    {!item.is_condition_order_join ? (
                                                      <Empty />
                                                    ) : (
                                                      item.lst_setup_condition.map(
                                                        (item1, index1) => (
                                                          <Col span={24}>
                                                            {_renderCondition(
                                                              item1.condition_style,
                                                              item1.type,
                                                              item1,
                                                              index1
                                                            )}
                                                          </Col>
                                                        )
                                                      )
                                                    )}
                                                  </div>
                                                </Col>
                                              </Row>
                                            </Col>
                                          </>
                                        );
                                      }
                                    )}
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </Fragment>
            </Skeleton>
          </div>
        </Col>
      </Row>
    </div>
  );
}
