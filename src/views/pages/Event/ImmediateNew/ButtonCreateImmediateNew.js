import { DefineKeyEvent } from "../../../../utils/DefineKey";

export const createEventImmediate = ({
  eventImmediateNew,
  treeProductIImmediate,
  modalS1,
  modalS2,
  dataArrayCondition,
  treeProductPresentIImmediateConditionAttach,
  treeProductImmediateConditionAttachGroup,
}) => {
  const dataIDS1 = modalS1.listS1Show.map((item) => {
    return item.id;
  });

  // const dataIDS2 = modalS2.listS2Show.map((item) => {
  //   return item.id;
  // });

  let dataTreeNew = [];
  treeProductIImmediate.listConfirm.forEach((item) => {
    dataTreeNew = [...dataTreeNew, ...item.children];
  });
  let product_appplied_condition = [];
  if (eventImmediateNew.gift_promotion === 1) {
    eventImmediateNew.arr_bonus_level.forEach((item) => {
      let arrProduct = item.arr_product.map((item1) => {
        const idx = dataTreeNew.map((el) => el.id).indexOf(item1.id);
        // let arrID = item1.id
        let product_id = item1.id;
        let quantity_sale = 0;
        let price_sale = 0;
        let percent_sale = item1.quantity_sale / 100;
        let quantity_max = 0;
        let quantity_min = 0;
        let is_chuan_quy_doi =
          treeProductIImmediate.keyDefaultProduct === item1.id ? 1 : 0;
        let ti_le_quy_doi = 1;
        return {
          product_id,
          quantity_sale,
          price_sale,
          percent_sale,
          quantity_max,
          quantity_min,
          is_chuan_quy_doi,
          ti_le_quy_doi,
        };
      });

      if (eventImmediateNew.condition_promotion === 1) {
        product_appplied_condition.push({
          condition_product_money_min: 0,
          condition_product_quantity_min: 0,
          productApply: [],
          money_max: item.to,
          money_min: item.from,
          quantity_max: 0,
          quantity_min: 0,
          product_reward_condition: arrProduct,
          listProductConditionIdWithProductGift: [],
        });
      } else {
        if (eventImmediateNew.condition_promotion === 3) {
          product_appplied_condition.push({
            condition_product_money_min: 0,
            condition_product_quantity_min: 0,
            productApply: [],
            money_max: item.to,
            money_min: item.from,
            quantity_max: 0,
            quantity_min: 0,
            product_reward_condition: [
              {
                product_id: 0,
                quantity_sale: 0,
                price_sale: 0,
                percent_sale: item.quantity_sale / 100,
                quantity_max: 0,
                quantity_min: 0,
              },
            ],
            listProductConditionIdWithProductGift: [],
          });
        } else {
          product_appplied_condition.push({
            condition_product_money_min: 0,
            condition_product_quantity_min: 0,
            productApply: [],
            money_max: 0,
            money_min: 0,
            quantity_max: item.to,
            quantity_min: item.from,
            product_reward_condition: arrProduct,
            listProductConditionIdWithProductGift: [],
          });
        }
      }
    });
  }

  if (eventImmediateNew.gift_promotion === 2) {
    eventImmediateNew.arr_bonus_level.forEach((item) => {
      let arrProduct = item.arr_product.map((item1) => {
        // let arrID = item1.id
        let product_id = item1.id;
        let quantity_sale = 0;
        let price_sale = 0;
        let percent_sale = 0;
        let quantity_max = item1.maxUnitValue;
        let quantity_min = item1.minUnitValue;
        let is_chuan_quy_doi =
          treeProductIImmediate.keyDefaultProduct === item1.id ? 1 : 0;
        const idx = dataTreeNew.map((el) => el.id).indexOf(item1.id);
        let ti_le_quy_doi = 1;

        return {
          product_id,
          quantity_sale,
          price_sale,
          percent_sale,
          quantity_max,
          quantity_min,
          is_chuan_quy_doi,
          ti_le_quy_doi,
        };
      });

      if (eventImmediateNew.condition_promotion === 1) {
        product_appplied_condition.push({
          condition_product_money_min: 0,
          condition_product_quantity_min: 0,
          productApply: [],
          money_max: item.to,
          money_min: item.from,
          quantity_max: 0,
          quantity_min: 0,
          product_reward_condition: arrProduct,
          listProductConditionIdWithProductGift: [],
        });
      } else {
        product_appplied_condition.push({
          condition_product_money_min: 0,
          condition_product_quantity_min: 0,
          productApply: [],
          money_max: 0,
          money_min: 0,
          quantity_max: item.to,
          quantity_min: item.from,
          product_reward_condition: arrProduct,
          listProductConditionIdWithProductGift: [],
        });
      }
    });
  }

  if (eventImmediateNew.gift_promotion === 3) {
    eventImmediateNew.arr_gift_group.forEach((item, index) => {
      let arrProduct = eventImmediateNew.arr_product_gift_group[
        index
      ].arr_product.map((item1) => {
        // let arrID = item1.id
        let product_id = item1.id;
        let quantity_sale =
          eventImmediateNew.attribute_promotion === 2
            ? item1.maxUnitValue
            : item1.minUnitValue;
        let price_sale = 0;
        let percent_sale = 0;
        let quantity_max = item1.quantity_gif_max;
        let quantity_min = item1.quantity_gif_min;
        let is_chuan_quy_doi = 0;
        // const idx = dataTreeNew.map(el => el.id).indexOf(item1.id)
        let ti_le_quy_doi = 0;

        return {
          product_id,
          quantity_sale,
          price_sale,
          percent_sale,
          quantity_max,
          quantity_min,
          is_chuan_quy_doi,
          ti_le_quy_doi,
        };
      });

      if (
        eventImmediateNew.condition_promotion === 1 ||
        eventImmediateNew.condition_promotion === 3
      ) {
        product_appplied_condition.push({
          condition_product_money_min: 0,
          condition_product_quantity_min: 0,
          productApply: [],
          money_max: item.to,
          money_min: item.from,
          quantity_max: 0,
          quantity_min: 0,
          product_reward_condition: arrProduct,
          listProductConditionIdWithProductGift: [],
        });
      } else {
        product_appplied_condition.push({
          condition_product_money_min: 0,
          condition_product_quantity_min: 0,
          productApply: [],
          money_max: 0,
          money_min: 0,
          quantity_max: item.to,
          quantity_min: item.from,
          product_reward_condition: arrProduct,
          listProductConditionIdWithProductGift: [],
        });
      }
    });
  }

  let dataConditionJoin = [];
  if (eventImmediateNew.check_condition_attach === 1) {
    dataConditionJoin = dataArrayCondition.map((item) => {
      const condition_type =
        item.condition_type === DefineKeyEvent.conditon_nguyen_don
          ? 1
          : item.condition_type;
      const condition_order_style =
        item.condition_type === DefineKeyEvent.conditon_nguyen_don //1 : Nguyen don, 2 : Từng sản phẩm, 3 : Nhom
          ? 1
          : item.condition_order_style;
      const condition = item.sum_sales_order;
      let ls_product = [];
      if (item.condition_order_style === DefineKeyEvent.tung_san_pham) {
        let arrProduct = [];

        treeProductPresentIImmediateConditionAttach.listConfirm.map(
          (itemLv1) => {
            itemLv1.children.map((itemLv2) => {
              itemLv2.children.map((itemLv3) => {
                arrProduct.push(itemLv3.id);
              });
            });
          }
        );

        arrProduct.forEach((item, index) => {
          const product_id = item;
          const condition =
            eventImmediateNew.arr_product_condition_attach[index]
              .salesOrQuantity;
          ls_product.push({
            product_id: product_id,
            condition: condition,
          });
        });
      }
      ls_product =
        item.condition_type === DefineKeyEvent.conditon_nguyen_don ||
        item.condition_order_style === DefineKeyEvent.nhom_san_pham
          ? []
          : ls_product;
      let ls_group = [];
      if (item.condition_order_style === DefineKeyEvent.nhom_san_pham) {
        treeProductImmediateConditionAttachGroup.listArrListTreeConfirm.forEach(
          (item, index) => {
            ls_group.push({
              group_id: index + 1,
              condition:
                eventImmediateNew.arr_condition_attach_group[index]
                  .salesOrQuantityGroup,
              ls_product: [],
            });
            item.treeProduct.forEach((itemLv1) => {
              itemLv1.children.forEach((itemLv2) => {
                itemLv2.children.forEach((itemLv3) => {
                  const product_id = itemLv3.id;
                  ls_group[index].ls_product.push({
                    product_id: product_id,
                    condition: 0,
                  });
                });
              });
            });
          }
        );
      }
      ls_group =
        item.condition_type === DefineKeyEvent.conditon_nguyen_don ||
        item.condition_order_style === DefineKeyEvent.tung_san_pham
          ? []
          : ls_group;
      return {
        condition_type,
        condition_order_style,
        condition,
        ls_product,
        ls_group,
      };
    });
  }

  let product = [];
  treeProductIImmediate.listConfirm.forEach((itemLv1) => {
    itemLv1.children.forEach((itemLv2) => {
      itemLv2.children.forEach((itemLv3) => {
        product.push({
          condition_apply_total_money: 0,
          description: itemLv3.description,
          percent_sale: 0,
          product_id: itemLv3.id,
          quantity_sale: 0,
        });
      });
    });
  });

  const dataAdd = {
    applied_condition_type: eventImmediateNew.condition_promotion,
    company_id: eventImmediateNew.event_degsin.key_type,
    condition: [
      {
        cities_id: eventImmediateNew.event_filter.arrayCity,
        level_user:
          eventImmediateNew.type_way === 1
            ? eventImmediateNew.event_filter.id_agency
            : 1,
        membership_id: eventImmediateNew.event_filter.membership,
        user_id: eventImmediateNew.type_way === 2 ? dataIDS1 : [0],
        // user_s2:
        //   eventImmediateNew.type_way === DefineKeyEvent.event_type_S2
        //     ? dataIDS2
        //     : [0],
      },
    ],
    // level_user:
    //   eventImmediateNew.type_way === 1
    //     ? eventImmediateNew.event_filter.id_agency
    //     : eventImmediateNew.type_way === 2
    //     ? 1
    //     : 2,

    condition_set_up: [...dataConditionJoin],

    condition_setup_type: eventImmediateNew.check_condition_attach, // điều kiện kèm theo hay ko
    content: eventImmediateNew.content_event,
    end_date: eventImmediateNew.to_time_apply,
    event_birthday_type: eventImmediateNew.event_birthday ? 1 : 0,
    event_code: eventImmediateNew.code_event,
    event_condition_type: 1, // bất kì
    event_id_dependent: eventImmediateNew.is_evet_parent === 2 ? 2 : 1,
    event_name: eventImmediateNew.name_event,
    event_parent_id:
      eventImmediateNew.is_evet_parent === 2 ? eventImmediateNew.id_parent : 0,
    event_priority_type: eventImmediateNew.event_priority_type,
    event_type: 1,
    excommunicate_event_ids:
      eventImmediateNew.event_priority_type === DefineKeyEvent.song_song ||
      (eventImmediateNew.event_priority_type === DefineKeyEvent.uu_tien &&
        eventImmediateNew.excommunicate_event_ids === 0)
        ? []
        : eventImmediateNew.excommunicate_event_ids,
    image: eventImmediateNew.img_event,
    pdf_file: eventImmediateNew.pdf_file,
    pdf_name: eventImmediateNew.pdf_name,
    prioty: 0,
    product: product,
    product_appplied_condition: product_appplied_condition,
    product_attribute_type: eventImmediateNew.attribute_promotion,
    reward_condition_type: eventImmediateNew.gift_promotion,
    start_date: eventImmediateNew.from_time_apply,
    type_entry_condition: eventImmediateNew.type_way === 1 ? 3 : 1,
    update_event_id: 0,
    is_show_event_name: eventImmediateNew.show_name_event === true ? 1 : 0,
    value_prioty: eventImmediateNew.value_prioty,
  };

  return dataAdd;
};
