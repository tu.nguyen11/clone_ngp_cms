import { Checkbox, Col, Icon, List, message, Row } from "antd";
import React, { useEffect, useState } from "react";
import { StyledITileHeading } from "../../../components/common/Font/font";
import { StyledSearchCustom } from "../../../components/common";
import { colors } from "../../../../assets";
import IButton from "../../../components/IButton";
import ISvg from "../../../components/ISvg";
import ITableHtml from "../../../components/ITableHtml";
import { APIService } from "../../../../services";
import { useDispatch, useSelector } from "react-redux";
import {
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
  ASSIGNED_EVENT_INFO,
  _fetchAPIDetailEventImmediateParent,
} from "../../../store/reducers";

export default function useModalEvent(callbackClose = () => {}) {
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { eventImmediateNew } = dataRoot;

  const [eventCheck, setEventCheck] = useState({});

  const [dataEvent, setDataEvent] = useState([]);

  const getListEventNowParent = async () => {
    try {
      const data = await APIService._getListEventNowParent();

      const dataNew = data.listEvent.map((item) => {
        if (
          item.code_event ===
          eventImmediateNew.event_dependent_confirm.code_event
        ) {
          item.active = 1;
        } else {
          item.active = 0;
        }
        return item;
      });

      const dataAction = {
        key: "arr_event_tmp",
        value: dataNew,
      };

      dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction));

      const dataAction1 = {
        key: "arr_event_confirm",
        value: dataNew,
      };

      dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction1));
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getListEventNowParent("");
  }, []);

  const headerTable = [
    {
      name: "",
      align: "center",
      width: 40,
    },
    {
      name: "STT",
      align: "center",
      width: 60,
    },
    {
      name: "Mã chương trình",
      align: "left",
      width: 200,
    },
    {
      name: "Tên chương trình",
      align: "left",
    },
    {
      name: "Trạng thái",
      align: "center",
      width: 120,
    },
  ];

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="tr-table">
        {headerTable.map((item, index) => (
          <th
            className={"th-table"}
            style={{ textAlign: item.align, width: item.width }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.map((item, index) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{
            paddingLeft: 15,
            fontWeight: 400,
            height: "100%",
            fontSize: "clamp(8px, 4vw, 14px)",
          }}
        >
          <div
            style={{
              width: 20,
              height: 20,
              border:
                eventImmediateNew.id_parent === item.id
                  ? "5px solid" + `${colors.main}`
                  : "1px solid" + `${colors.line_2}`,
              borderRadius: 10,
            }}
            onClick={() => {
              const dataAction = {
                key: "id_parent",
                value: item.id,
              };
              dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction));
            }}
          />
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "center",
            fontWeight: 400,
            fontSize: "clamp(8px, 4vw, 14px)",
          }}
        >
          {index + 1}
        </td>
        <td
          className="td-table"
          style={{
            fontWeight: 400,
            fontSize: "clamp(8px, 4vw, 14px)",
            wordBreak: "break-all",
          }}
        >
          {item.code_event}
        </td>
        <td
          className="td-table"
          style={{ fontWeight: 400, fontSize: "clamp(8px, 4vw, 14px)" }}
        >
          {item.name_event}
        </td>

        <td
          className="td-table"
          style={{
            fontWeight: 400,
            fontSize: "clamp(8px, 4vw, 14px)",
            textAlign: "center",
          }}
        >
          {item.status_name}
        </td>
      </tr>
    ));
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Chọn chương trình mẹ
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div
            style={{
              height: 510,
              border: "1px solid rgba(122, 123, 123, 0.5)",
            }}
          >
            <ITableHtml
              childrenBody={bodyTableProduct(eventImmediateNew.arr_event_tmp)}
              childrenHeader={headerTableProduct(headerTable)}
              isBorder={false}
            />
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 60,
              right: -25,
            }}
          >
            <IButton
              title="Xác nhận"
              color={colors.main}
              loading={eventImmediateNew.loading}
              icon={ISvg.NAME.CHECKMARK}
              styleHeight={{
                width: 140,
                marginRight: 24,
              }}
              onClick={async () => {
                const idx = eventImmediateNew.arr_event_tmp
                  .map((item) => item.id)
                  .indexOf(eventImmediateNew.id_parent);
                if (idx === -1) {
                  return message.error("Mời bạn chọn chương trình mẹ.");
                }

                await dispatch(
                  _fetchAPIDetailEventImmediateParent(
                    eventImmediateNew.id_parent
                  )
                );
                callbackClose();
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                let data = JSON.parse(JSON.stringify(dataEvent));
                const dataNew = data.map((item, index1) => {
                  item.active = 0;

                  return item;
                });

                setDataEvent(dataNew);
                const dataAction = {
                  key: "event_dependent_tmp",
                  value: {},
                };

                dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction));

                const dataAction1 = {
                  key: "arr_event_tmp",
                  value: eventImmediateNew.arr_event_confirm,
                };
                dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction1));

                callbackClose();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
