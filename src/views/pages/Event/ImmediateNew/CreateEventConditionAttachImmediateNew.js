import { Col, Empty, Icon, message, Modal, Popconfirm, Row, Tag } from "antd";
import React, { useState } from "react";
import { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import { IButton, ISvg } from "../../../components";
import { StyledInputNumber, StyledISelect } from "../../../components/common";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";
import useModalTreeProductImmediateAttach from "../../../components/common/ModalAgency/useModalTreeProductImmediateAttach";
import useModalTreeProductImmediateAttachGroup from "../../../components/common/ModalAgency/useModalTreeProductImmediateAttachGroup";
import {
  CLEAR_REDUX_IMMEDIATE_NEW,
  UPDATE_CONDITION_COMBO,
  UPDATE_ARR_ATTACH,
  ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE_CONDITION_ATTACH,
  UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE,
  REMOVE_CONDITION_COMBO,
  UPDATE_KEY_EVENT_COMBO_DESIGN_CONDITION_ARRAY,
  UPDATE_IDX_ARRAY_COMBO_DESIGN_CONDITION,
  ONCHANGE_CONDITION_EVENT_GROUP_COMBO_ARRAY,
  REMOVE_PRODUCT_GROUP_IMMEDIATE,
} from "../../../store/reducers";
import { createEventImmediate } from "./ButtonCreateImmediateNew";

export default function CreateEventConditionAttachImmediateNew() {
  const dataRoot = useSelector((state) => state);
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();
  const [loadingSubmit, setLoadingSubmit] = useState(false);

  const [isVisibleTree, setVisibleTree] = useState(false);
  const [isVisibleTreeGroup, setVisibleTreeGroup] = useState(false);
  const {
    eventImmediateNew,
    dataArrayCondition = [],
    treeProductImmediateConditionAttachGroup,
    treeProductPresentIImmediateConditionAttach,
    treeProductIImmediate,
    modalS1,
    modalS2,
    modalBusiness,
    modalCity,
    modalDistrict,
    modalWard,
  } = dataRoot;

  const _fetchAPIPostCreateEvent = async (obj) => {
    try {
      const data = await APIService._postCreateEventNowNew(obj);
      dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
      message.success("Tạo chương trình thành công");
      setLoadingSubmit(false);
      history.push("/event/immediate/list");
    } catch (error) {
      setLoadingSubmit(false);
      console.log(error);
    }
  };

  const treeProct = (dataProduct, idxRoot) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children, idxRoot)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={24}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 30,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    {/* <Col span={24}>
                      <div style={{ paddingLeft: 30 }}>
                        <IInputTextArea
                          style={{
                            minHeight: 35,
                            height: 35,
                            padding: 6,
                            marginTop: 8,
                            border: "none",
                          }}
                          placeholder="Mô tả chính sách bán hàng của sản phẩm"
                          value={item.description}
                          onChange={(e) => {
                            const data = {
                              key: "description",
                              tree:
                                eventGift.event_design.key_type_product === 1
                                  ? treeProductEventGiftGroup.listArrListTreeConfirm
                                  : treeProductEventGiftGroup2.listArrListTreeConfirm,
                              value: e.target.value,
                              idxRoot: idxRoot,
                              id: item.id,
                              keyTypeProduct:
                                eventGift.event_design.key_type_product,
                            };
                            dispatch(ADD_CONTENT_PRODUCT(data));
                          }}
                        />
                      </div>
                    </Col> */}
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  const treeProductConfirmGroup = () => {
    return treeProductImmediateConditionAttachGroup.listArrListTreeConfirm
      .length === 0
      ? null
      : treeProductImmediateConditionAttachGroup.listArrListTreeConfirm.map(
          (itemTree, idxTreeItem) => (
            <div
              style={{
                padding: "4px 12px",
                border: "1px solid #C4C4C4",
                marginBottom: 8,
              }}
            >
              <Row gutter={[12, 0]} type="flex">
                <Col
                  span={24}
                  style={{
                    borderBottom: "1px solid #C4C4C4",
                    paddingBottom: 6,
                  }}
                >
                  <Row>
                    <Col span={22}>
                      <span style={{ fontWeight: 700 }}>
                        NHÓM HÀNG{" "}
                        {idxTreeItem < 9
                          ? `0${idxTreeItem + 1}`
                          : idxTreeItem + 1}
                      </span>
                    </Col>
                    <Col span={2}>
                      {treeProductImmediateConditionAttachGroup
                        .listArrListTreeConfirm.length === 1 ? null : (
                        <div
                          style={{
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "flex-end",
                          }}
                        >
                          <div
                            style={{
                              background: "rgba(227, 95, 75, 0.1)",
                              width: 20,
                              height: 20,
                              borderRadius: 10,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              cursor: "pointer",
                            }}
                            onClick={() => {
                              const dataAction = {
                                treeProduct:
                                  treeProductImmediateConditionAttachGroup
                                    .listArrListTreeConfirm[idxTreeItem]
                                    .treeProduct,
                                idxTreeItem: idxTreeItem,
                              };
                              dispatch(
                                REMOVE_PRODUCT_GROUP_IMMEDIATE(dataAction)
                              );
                            }}
                            className="cursor"
                          >
                            <ISvg
                              name={ISvg.NAME.CROSS}
                              width={8}
                              height={8}
                              fill="#E35F4B"
                            />
                          </div>
                        </div>
                      )}
                    </Col>
                  </Row>
                </Col>

                <Col span={24}>
                  <ul style={{ padding: 0 }}>
                    {itemTree.treeProduct.map((item, index) => {
                      if (item.children && item.children.length) {
                        return (
                          <ul
                            style={{
                              listStyleType: "none",
                              padding: 0,
                            }}
                          >
                            <li
                              style={{
                                fontWeight: 500,
                                borderBottom: "0.5px dashed #7A7B7B",
                                padding: "6px 0px",
                                color: "black",
                              }}
                            >
                              {item.name}
                            </li>
                            {treeProct(item.children, idxTreeItem)}
                          </ul>
                        );
                      } else {
                        return (
                          <ul
                            style={{
                              listStyleType: "none",
                              borderBottom: "0.5px dashed #7A7B7B",
                            }}
                          >
                            <li style={{ padding: "6px 0px" }}>
                              <Row gutter={[12, 0]} type="flex">
                                <Col span={24}>
                                  <div
                                    style={{
                                      wordWrap: "break-word",
                                      wordBreak: "break-word",
                                      overflow: "hidden",
                                      display: "flex",
                                      alignItems: "center",
                                      height: "100%",
                                    }}
                                  >
                                    <span>{item.name}</span>
                                  </div>
                                </Col>
                              </Row>
                            </li>
                          </ul>
                        );
                      }
                    })}
                  </ul>
                </Col>

                <Col
                  span={24}
                  style={{
                    borderTop: "1px solid #C4C4C4",
                    padding: "6px 0px",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      style={{ display: "flex" }}
                      className="cursor"
                      onClick={() => {
                        const data = {
                          key: "id_count_save_item_condition_attach",
                          value: idxTreeItem,
                        };
                        dispatch(UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE(data));
                        dispatch(
                          ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE_CONDITION_ATTACH(
                            treeProductImmediateConditionAttachGroup
                              .listArrListTreeConfirm[idxTreeItem].treeProduct
                          )
                        );
                        setVisibleTreeGroup(true);
                      }}
                    >
                      <span
                        style={{
                          margin: "0px 12px",
                          color: colors.main,
                          fontWeight: 500,
                        }}
                      >
                        Thêm phiên bản
                      </span>
                      <div>
                        <ISvg
                          name={ISvg.NAME.ADDUPLOAD}
                          width={20}
                          height={20}
                          fill={colors.main}
                        />
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          )
        );
  };

  const _renderItemProudct = (dataProduct) => {
    return dataProduct.map((item, index) => {
      return (
        <p
          style={{
            listStyleType: "none",
            borderBottom: "0.5px dashed #7A7B7B",
            marginRight: 16,
          }}
        >
          <p style={{ padding: "6px 0px" }}>
            <Row gutter={[12, 0]} type="flex">
              <Col span={14}>
                <div
                  style={{
                    wordWrap: "break-word",
                    wordBreak: "break-word",
                    overflow: "hidden",
                    display: "flex",
                    alignItems: "center",
                    height: "100%",
                  }}
                >
                  <span>{item.name}</span>
                </div>
              </Col>
              <Col span={10}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <span>Từ :</span>
                  <StyledInputNumber
                    style={{ marginLeft: 24, minWidth: 130 }}
                    formatter={(value) =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                    onChange={(value) => {
                      const data = {
                        index: index,
                        value: value,
                      };

                      dispatch(UPDATE_ARR_ATTACH(data));
                    }}
                    min={0}
                    className="disabled-up"
                    parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                    value={item.salesOrQuantity}
                  />
                </div>
              </Col>
            </Row>
          </p>
        </p>
      );
    });
  };

  const _renderConditionOrder = (data, idx) => (
    <div
      style={{
        padding: "12px 12px 36px 12px",
        border: "1px solid rgba(122, 123, 123, 0.5)",
      }}
    >
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <div>
            <span style={{ fontWeight: 600, marginRight: 160 }}>
              Doanh số nguyên đơn
            </span>
            <span style={{ fontWeight: 600 }}>Doanh số</span>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <span style={{ marginRight: 80 }}>Doanh số trên đơn :</span>
            <div>
              <span>Từ :</span>
              <StyledInputNumber
                style={{ marginLeft: 24, minWidth: 160 }}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                className="disabled-up"
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                value={data.sum_sales_order}
                min={0}
                onChange={(value) => {
                  const data = {
                    idx: idx,
                    key: "sum_sales_order",
                    value: value,
                  };

                  dispatch(UPDATE_CONDITION_COMBO(data));
                }}
              />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );

  const _renderTitleCondition = (type) => {
    if (
      type === DefineKeyEvent.condition_type_sales &&
      eventImmediateNew.condition_order_style === DefineKeyEvent.tung_san_pham
    )
      return "Doanh số sản phẩm trên đơn";
    if (
      type === DefineKeyEvent.condition_type_count &&
      eventImmediateNew.condition_order_style === DefineKeyEvent.tung_san_pham
    )
      return "Số lượng sản phẩm trên đơn";
    if (
      eventImmediateNew.condition_order_style === DefineKeyEvent.nhom_san_pham
    )
      return "Nhóm hàng";
  };

  const _renderTitleCondition2 = (type) => {
    if (
      type === DefineKeyEvent.condition_type_sales &&
      eventImmediateNew.condition_order_style === DefineKeyEvent.tung_san_pham
    )
      return "Doanh số sản phẩm";
    if (
      type === DefineKeyEvent.condition_type_count &&
      eventImmediateNew.condition_order_style === DefineKeyEvent.tung_san_pham
    )
      return "Số lượng sản phẩm";
    if (
      type === DefineKeyEvent.condition_type_sales &&
      eventImmediateNew.condition_order_style === DefineKeyEvent.nhom_san_pham
    )
      return "Doanh số nhóm sản phẩm";
    if (
      type === DefineKeyEvent.condition_type_count &&
      eventImmediateNew.condition_order_style === DefineKeyEvent.nhom_san_pham
    )
      return "Số lượng nhóm sản phẩm";
  };

  const _renderCondition = (data, idx) => (
    <div
      style={{
        padding: "12px 12px 36px 12px",
        border: "1px solid rgba(122, 123, 123, 0.5)",
      }}
    >
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <React.Fragment>
            <Row gutter={[12, 12]}>
              <Col span={14}>
                <span style={{ fontWeight: 600 }}>
                  {_renderTitleCondition(data.condition_type)}
                </span>
              </Col>
              <Col span={10}>
                <span style={{ fontWeight: 600 }}>
                  {_renderTitleCondition2(data.condition_type)}
                </span>
              </Col>
            </Row>
          </React.Fragment>
        </Col>
        <Col span={24}>
          <div style={{ maxHeight: 420, overflow: "auto" }}>
            {eventImmediateNew.arr_product_condition_attach.length === 0 ? (
              <Empty />
            ) : (
              _renderItemProudct(eventImmediateNew.arr_product_condition_attach)
            )}
          </div>
        </Col>
      </Row>
    </div>
  );

  const _renderConditionGroup = (data, idx) => (
    <div
      style={{
        padding: "12px 12px 36px 12px",
        border: "1px solid rgba(122, 123, 123, 0.5)",
      }}
    >
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <React.Fragment>
            <Row gutter={[12, 12]}>
              <Col span={8}>
                <span style={{ fontWeight: 600 }}>
                  {_renderTitleCondition(data.condition_type)}
                </span>
              </Col>
              <Col span={16}>
                <span style={{ fontWeight: 600 }}>
                  {_renderTitleCondition2(data.condition_type)}
                </span>
              </Col>
            </Row>
          </React.Fragment>
        </Col>
        <Col span={24}>
          {eventImmediateNew.arr_condition_attach_group.length === 0 ? (
            <Empty />
          ) : (
            <>
              {eventImmediateNew.arr_condition_attach_group.map(
                (item, index) => {
                  return (
                    <React.Fragment>
                      <Row gutter={[12, 12]}>
                        <Col span={8}>
                          <span style={{ fontWeight: 600 }}>{`Nhóm hàng ${
                            index + 1
                          }`}</span>
                        </Col>
                        <Col span={16}>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <span>Từ :</span>
                            <StyledInputNumber
                              style={{ marginLeft: 24, minWidth: 130 }}
                              formatter={(value) =>
                                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                              }
                              onChange={(value) => {
                                const data1 = {
                                  idx: index,
                                  value: value,
                                };
                                dispatch(
                                  ONCHANGE_CONDITION_EVENT_GROUP_COMBO_ARRAY(
                                    data1
                                  )
                                );
                              }}
                              min={0}
                              className="disabled-up"
                              parser={(value) =>
                                value.replace(/\$\s?|(,*)/g, "")
                              }
                              value={item.salesOrQuantityGroup}
                            />
                          </div>
                        </Col>
                      </Row>
                    </React.Fragment>
                  );
                }
              )}
            </>
          )}
        </Col>
      </Row>
    </div>
  );

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <form>
        <Row gutter={[0, 24]}>
          <Col span={24}>
            <div>
              <StyledITitle
                style={{ color: colors.main }}
              >{`Tạo Chương trình khuyến mãi trên đơn > Thiết lập điều kiện kèm theo`}</StyledITitle>
            </div>
          </Col>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Hủy"
                    color={colors.oranges}
                    icon={ISvg.NAME.CROSS}
                    styleHeight={{
                      width: 140,
                    }}
                    onClick={() => {
                      dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                      history.push("/event/immediate/list");
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={22}>
            <Row gutter={[24, 24]} type="flex">
              <Col span={24}>
                <div
                  style={{ padding: 36, minHeight: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[24, 36]} type="flex">
                    <Col span={8}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Danh mục điều kiện kèm theo
                      </StyledITileHeading>
                    </Col>
                    <Col span={16}>
                      <StyledITileHeading
                        minFont="10px"
                        maxFont="16px"
                        style={{ padding: "0 36px" }}
                      >
                        Thiết lập điều kiện xét tích lũy kèm theo
                      </StyledITileHeading>
                    </Col>
                    {/* <Col span={24}>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginLeft: 24
                        }}
                      >
                        <span
                          style={{
                            margin: '0px 12px',
                            color: colors.main,
                            fontWeight: 500
                          }}
                        >
                          Thêm mức thưởng
                        </span>
                        <div
                          className='cursor'
                          onClick={() => {
                            dispatch(ADD_CONDITION_COMBO())
                          }}
                        >
                          <ISvg
                            name={ISvg.NAME.ADDUPLOAD}
                            width={20}
                            height={20}
                            fill={colors.main}
                          />
                        </div>
                      </div>
                    </Col> */}

                    {dataArrayCondition.map((item, index) => {
                      return (
                        <Fragment>
                          {index === 0 ? null : (
                            <Col span={24} style={{ textAlign: "right" }}>
                              <Popconfirm
                                title="Nhấn OK để xóa Điều kiện"
                                onConfirm={() => {
                                  dispatch(REMOVE_CONDITION_COMBO(index));
                                }}
                              >
                                <Tag
                                  icon={<Icon type="delete" />}
                                  color={colors.main}
                                >
                                  Xóa
                                </Tag>
                              </Popconfirm>
                            </Col>
                          )}
                          <Col span={8}>
                            <div
                              style={{
                                // overflowY: "auto",
                                // minHeight: 530,
                                // paddingRight: 32,
                                height: "100%",
                              }}
                            >
                              <React.Fragment>
                                <Row gutter={[24, 24]}>
                                  <Col span={24}>
                                    <p style={{ fontWeight: 600 }}>
                                      Điều kiện kèm theo
                                    </p>
                                    <StyledISelect
                                      marginLeft="0px"
                                      isBorderBottom="1px solid #d9d9d9"
                                      value={item.condition_type}
                                      onChange={(key) => {
                                        const data = {
                                          idx: index,
                                          key: "condition_type",
                                          value: key,
                                        };
                                        dispatch(UPDATE_CONDITION_COMBO(data));
                                      }}
                                      // disabled={
                                      // 	eventCombo.type_event_status ===
                                      // 	DefineKeyEvent.event_running
                                      // }
                                      data={[
                                        {
                                          value: "Doanh số nguyên đơn hàng",
                                          key: DefineKeyEvent.conditon_nguyen_don,
                                        },
                                        {
                                          value: "Doanh số phiên bản",
                                          key: DefineKeyEvent.condition_type_sales,
                                        },
                                        {
                                          value: "Số lượng phiên bản",
                                          key: DefineKeyEvent.condition_type_count,
                                        },
                                      ]}
                                    />
                                  </Col>
                                  {item.condition_type ===
                                  DefineKeyEvent.conditon_nguyen_don ? null : (
                                    <Col span={24}>
                                      <p style={{ fontWeight: 600 }}>
                                        Điều kiện sản phẩm
                                      </p>
                                      <StyledISelect
                                        marginLeft="0px"
                                        isBorderBottom="1px solid #d9d9d9"
                                        value={item.condition_order_style}
                                        onChange={(key) => {
                                          const data = {
                                            idx: index,
                                            key: "condition_order_style",
                                            value: key,
                                          };
                                          dispatch(
                                            UPDATE_CONDITION_COMBO(data)
                                          );
                                        }}
                                        // disabled={eventCombo.type_event_status === DefineKeyEvent.event_running}
                                        data={[
                                          {
                                            value: "Danh sách phiên bản",
                                            key: DefineKeyEvent.tung_san_pham,
                                          },
                                          {
                                            value: "Nhóm phiên bản",
                                            key: DefineKeyEvent.nhom_san_pham,
                                          },
                                        ]}
                                      />
                                    </Col>
                                  )}

                                  {eventImmediateNew.type_event_status ===
                                    DefineKeyEvent.event_running ||
                                  item.condition_type ===
                                    DefineKeyEvent.conditon_nguyen_don ? null : (
                                    <Col span={24}>
                                      <div
                                        style={{
                                          display: "flex",
                                          flexDirection: "row",
                                          alignItems: "center",
                                          justifyContent: "flex-end",
                                        }}
                                      >
                                        <div
                                          style={{
                                            display: "flex",
                                          }}
                                          className="cursor"
                                          onClick={() => {
                                            if (
                                              item.condition_order_style ===
                                              DefineKeyEvent.nhom_san_pham
                                            ) {
                                              const data = {
                                                key: "id_count_save_item_condition_attach",
                                                value:
                                                  treeProductImmediateConditionAttachGroup
                                                    .listArrListTreeConfirm
                                                    .length === 0
                                                    ? 0
                                                    : -1,
                                              };
                                              dispatch(
                                                UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE(
                                                  data
                                                )
                                              );

                                              setVisibleTreeGroup(true);
                                            } else {
                                              setVisibleTree(true);
                                            }
                                          }}
                                        >
                                          <span
                                            style={{
                                              margin: "0px 12px",
                                              color: colors.main,
                                              fontWeight: 500,
                                            }}
                                          >
                                            {item.condition_order_style ===
                                            DefineKeyEvent.tung_san_pham
                                              ? "Thêm phiên bản"
                                              : "Thêm nhóm phiên bản"}
                                          </span>
                                          <div>
                                            <ISvg
                                              name={ISvg.NAME.ADDUPLOAD}
                                              width={20}
                                              height={20}
                                              fill={colors.main}
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </Col>
                                  )}

                                  {item.condition_type ===
                                  DefineKeyEvent.conditon_nguyen_don ? null : item.condition_order_style ===
                                    DefineKeyEvent.nhom_san_pham ? (
                                    <Col span={24}>
                                      {treeProductConfirmGroup()}
                                    </Col>
                                  ) : (
                                    <Col span={24}>
                                      {treeProct(
                                        treeProductPresentIImmediateConditionAttach.listConfirm
                                      )}
                                    </Col>
                                  )}
                                </Row>
                              </React.Fragment>
                            </div>
                          </Col>
                          <Col span={16}>
                            <div style={{ padding: "0 36px", height: "100%" }}>
                              <React.Fragment>
                                {item.condition_type ===
                                DefineKeyEvent.conditon_nguyen_don
                                  ? _renderConditionOrder(item, index)
                                  : item.condition_order_style ===
                                    DefineKeyEvent.nhom_san_pham
                                  ? _renderConditionGroup(item, index)
                                  : _renderCondition(item, index)}
                              </React.Fragment>
                            </div>
                          </Col>
                        </Fragment>
                      );
                    })}
                  </Row>
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <IButton
                onClick={() => history.goBack()}
                title="Quay lại"
                color={colors.main}
                icon={ISvg.NAME.ARROWTHINLEFT}
                style={{ marginRight: 12 }}
                styleHeight={{
                  width: 140,
                }}
              />
              <IButton
                title={
                  eventImmediateNew.is_evet_parent === 2
                    ? "Tiếp tục"
                    : "Hoàn thành"
                }
                color={colors.main}
                loading={loadingSubmit}
                icon={ISvg.NAME.ARROWRIGHT}
                iconRight={true}
                styleHeight={{
                  width: 140,
                }}
                onClick={() => {
                  if (eventImmediateNew.is_evet_parent === 2) {
                    return history.push(
                      `/create/event/immediate/children/${id}`
                    );
                  }

                  setLoadingSubmit(true);
                  const dataAdd = createEventImmediate({
                    eventImmediateNew,
                    treeProductIImmediate,
                    modalS1,
                    modalS2,
                    dataArrayCondition,
                    treeProductPresentIImmediateConditionAttach,
                    treeProductImmediateConditionAttachGroup,
                  });
                  _fetchAPIPostCreateEvent(dataAdd);
                }}
              />
            </div>
          </Col>
        </Row>
        {/* <Modal
          visible={isVisibleTree}
          footer={null}
          width={1200}
          centered={true}
          closable={false}
          maskClosable={false}
        >
          {useModalTreeConditionProductArray(
            () => {
              setVisibleTree(false);
            },
            eventImmediateNew.event_degsin.key_cate,
            dataArrayCondition[idxDataArrayCondition].condition_order_style ===
              DefineKeyEvent.nhom_san_pham,
            dataArrayCondition[idxDataArrayCondition].eventCombo.event_design
              .id_count_save_item_condition_attach,
            idxDataArrayCondition
          )}
        </Modal> */}
        <Modal
          visible={isVisibleTree}
          footer={null}
          width={1200}
          centered={true}
          closable={false}
          maskClosable={false}
        >
          {useModalTreeProductImmediateAttach(() => {
            setVisibleTree(false);
          }, eventImmediateNew.event_degsin.key_type)}
        </Modal>
        <Modal
          visible={isVisibleTreeGroup}
          footer={null}
          width={1200}
          centered={true}
          closable={false}
          maskClosable={false}
        >
          {useModalTreeProductImmediateAttachGroup(
            () => {
              setVisibleTreeGroup(false);
            },
            eventImmediateNew.event_degsin.key_type,
            true,
            eventImmediateNew.id_count_save_item_condition_attach
          )}
        </Modal>
      </form>
    </div>
  );
}
