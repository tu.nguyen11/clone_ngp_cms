import { Col, Row, Form, Skeleton, Switch } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import { IButton, ISvg } from "../../../components";
import { CloseOutlined, CheckOutlined } from "@ant-design/icons";
import {
  IInputText,
  IUploadHook,
  IFormItem,
  IInputTextArea,
  TagP,
} from "../../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../components/common/Font/font";
import {
  CLEAR_REDUX_IMMEDIATE_NEW,
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
} from "../../../store/reducers";
import styled from "styled-components";

const ISwitch = styled(Switch)`
  .ant-switch-inner {
    line-height: 12px !important;
  }
`;

function CreateEventImmediateNewDescriptionNew(props) {
  const history = useHistory();
  const params = useParams();
  const { id } = params;
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const [loadingImport, setLoadingImport] = useState(false);

  const { eventImmediateNew } = dataRoot;
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    reduxForm.setFieldsValue(
      {
        content_event: eventImmediateNew.content_event,
        code_event: eventImmediateNew.code_event,
        name_event: eventImmediateNew.name_event,
        img: eventImmediateNew.img_event,
      },
      () => setLoading(false)
    );
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        history.push(`/create/event/immediate/design/${id}`);
      }
    });
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 36px 0px" }}>
      <Form onSubmit={(event) => handleSubmit(event)}>
        <Row gutter={[0, 24]} type="flex" style={{ height: "100%" }}>
          <Col span={24}>
            <div>
              <StyledITitle style={{ color: colors.main }}>
                Tạo Chương trình khuyến mãi trên đơn
              </StyledITitle>
            </div>
          </Col>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Hủy"
                    color={colors.oranges}
                    icon={ISvg.NAME.CROSS}
                    styleHeight={{ width: 140 }}
                    onClick={() => {
                      dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                      history.push("/event/immediate/list");
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <div style={{ width: 920, height: "100%" }}>
              <Row gutter={[12, 24]} type="flex" style={{ height: "100%" }}>
                <Col span={13}>
                  <div
                    style={{ padding: 24, height: "100%" }}
                    className="box-shadow"
                  >
                    <Skeleton loading={loading} active paragraph={{ rows: 12 }}>
                      <Row gutter={[0, 24]}>
                        <Col>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Thông tin chương trình
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <div style={{ marginTop: 16 }}>
                                <TagP style={{ fontWeight: 500 }}>
                                  Mã chương trình
                                  <span style={{ color: "red", marginLeft: 4 }}>
                                    *
                                  </span>
                                </TagP>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("code_event", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "nhập mã chương trình",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    disabled={
                                      eventImmediateNew.is_evet_parent === 2
                                    }
                                    placeholder="nhập mã chương trình"
                                    value={reduxForm.getFieldValue(
                                      "code_event"
                                    )}
                                    onChange={(e) => {
                                      const dataAction = {
                                        key: "code_event",
                                        value: e.target.value,
                                      };
                                      dispatch(
                                        CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                          dataAction
                                        )
                                      );
                                    }}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <TagP style={{ fontWeight: 500 }}>
                                    Tên chương trình
                                    <span
                                      style={{ color: "red", marginLeft: 4 }}
                                    >
                                      *
                                    </span>
                                  </TagP>
                                </div>
                                <div>
                                  <ISwitch
                                    checked={eventImmediateNew.show_name_event}
                                    checkedChildren={<CheckOutlined />}
                                    unCheckedChildren={<CloseOutlined />}
                                    onChange={(checked) => {
                                      const dataAction = {
                                        key: "show_name_event",
                                        value: checked,
                                      };
                                      dispatch(
                                        CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                          dataAction
                                        )
                                      );
                                    }}
                                  />
                                </div>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("name_event", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "nhập tên chương trình",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    placeholder="nhập tên chương trình"
                                    value={reduxForm.getFieldValue(
                                      "name_event"
                                    )}
                                    disabled={
                                      eventImmediateNew.is_evet_parent === 2
                                    }
                                    onChange={(e) => {
                                      const dataAction = {
                                        key: "name_event",
                                        value: e.target.value,
                                      };
                                      dispatch(
                                        CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                          dataAction
                                        )
                                      );
                                    }}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <div>
                                <TagP style={{ fontWeight: 500 }}>
                                  Nội dung
                                  {/* <span style={{ color: "red", marginLeft: 4 }}>
                                    *
                                  </span> */}
                                </TagP>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("content_event", {
                                  rules: [
                                    {
                                      required: false,
                                      message:
                                        "nhập nội dung và thể lệ chương trình",
                                    },
                                  ],
                                })(
                                  <IInputTextArea
                                    placeholder="nhập nội dung và thể lệ chương trình"
                                    style={{
                                      height: 300,
                                      marginTop: 12,
                                    }}
                                    disabled={
                                      eventImmediateNew.is_evet_parent === 2
                                    }
                                    value={eventImmediateNew.content_event}
                                    onChange={(e) => {
                                      const dataAction = {
                                        key: "content_event",
                                        value: e.target.value,
                                      };
                                      dispatch(
                                        CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                          dataAction
                                        )
                                      );
                                    }}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Skeleton>
                  </div>
                </Col>
                <Col span={11}>
                  <div
                    style={{ padding: 24, height: "100%" }}
                    className="box-shadow"
                  >
                    <Skeleton loading={loading} active paragraph={{ rows: 12 }}>
                      <Row gutter={[0, 24]}>
                        <Col>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Hình ảnh
                            {/* <span style={{ color: "red", marginLeft: 4 }}>
                              *
                            </span> */}
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <IFormItem>
                            {getFieldDecorator("img", {
                              rules: [
                                {
                                  // required: true,
                                  message: "chọn hình",
                                },
                              ],
                            })(
                              <IUploadHook
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                callback={(nameFile, image_url) => {
                                  reduxForm.setFieldsValue({
                                    img: nameFile,
                                  });
                                  const dataAction = {
                                    key: "img_event",
                                    value: nameFile,
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction)
                                  );

                                  const dataUrl_img = {
                                    key: "URl_img",
                                    value: image_url,
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataUrl_img)
                                  );
                                }}
                                removeCallback={() => {
                                  reduxForm.setFieldsValue({
                                    img: "",
                                  });
                                  const data = {
                                    key: "img_event",
                                    value: "",
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );
                                  const dataUrl_img = {
                                    key: "URl_img",
                                    value: "",
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataUrl_img)
                                  );
                                }}
                                dataProps={
                                  eventImmediateNew.ISedit
                                    ? [
                                        {
                                          uid: -1,
                                          name: reduxForm.getFieldValue("img"),
                                          status: "done",
                                          url:
                                            eventImmediateNew.URl_img +
                                            reduxForm.getFieldValue("img"),
                                        },
                                      ]
                                    : eventImmediateNew.img_event === ""
                                    ? []
                                    : [
                                        {
                                          uid: -1,
                                          name: eventImmediateNew.img_event,
                                          status: "done",
                                          url:
                                            eventImmediateNew.URl_img +
                                            eventImmediateNew.img_event,
                                        },
                                      ]
                                }
                                width="100%"
                                height="235px"
                              />
                            )}
                          </IFormItem>
                        </Col>
                        {/* <Col span={24}>
                          <IButton
                            title="Upload file pdf"
                            color={colors.main}
                            loading={loadingImport}
                            styleHeight={{
                              width: 140,
                              display: "",
                              paddingRight: "none",
                              justifyContent: "none",
                            }}
                            disabled={eventImmediateNew.is_evet_parent === 2}
                            style={{ marginBottom: 24 }}
                            onClick={() => {
                              var input = document.getElementById("my-file");
                              input.click();
                              input.onchange = async function (e) {
                                try {
                                  var file = input.files[0];
                                  if (!file) return;
                                  setLoadingImport(true);
                                  const data = await APIService._uploadImage(
                                    "PDF",
                                    file
                                  );
                                  const dataAction = {
                                    key: "pdf_file",
                                    value: data.images[0],
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction)
                                  );

                                  const dataAction1 = {
                                    key: "pdf_url",
                                    value: data.image_url,
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction1)
                                  );

                                  const dataAction2 = {
                                    key: "pdf_name",
                                    value: file.name,
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction2)
                                  );

                                  if (data === true) {
                                    setLoadingImport(false);
                                    e.target.value = null;
                                    return;
                                  }
                                  setLoadingImport(false);

                                  e.target.value = null;
                                } catch (error) {
                                  message.error("Upload PDF thất bại");
                                  setLoadingImport(false);
                                }
                              };
                            }}
                          />
                          <input
                            type="file"
                            id="my-file"
                            accept=".pdf"
                            style={{ display: "none" }}
                          />
                          {/* <a
                            download
                            href={`${eventCombo.pdf_url}${eventCombo.pdf_file}`}
                            target='_blank'
                          >
                            {eventCombo.pdf_name}
                          </a> */}
                        {/* </Col>
                        {eventImmediateNew.pdf_name === "" ? null : (
                          <Col span={24}>
                            <Row gutter={[16, 0]}>
                              <Col span={20}>
                                <StyledITileHeading
                                  minFont="10px"
                                  maxFont="14px"
                                  style={{ fontWeight: 500 }}
                                >
                                  {eventImmediateNew.pdf_name}
                                </StyledITileHeading>
                              </Col>
                              <Col
                                span={4}
                                style={{
                                  display: "flex",
                                  justifyContent: "flex-end",
                                }}
                              >
                                <div
                                  style={{
                                    width: 50,
                                  }}
                                >
                                  <IButton
                                    href={`${eventImmediateNew.pdf_url}${eventImmediateNew.pdf_file}`}
                                    icon={ISvg.NAME.DOWLOAND}
                                    color={colors.main}
                                    target="_blank"
                                    styleHeight={{
                                      minWidth: 0,
                                      justifyContent: "none",
                                      paddingLeft: "none",
                                    }}
                                  />
                                </div>
                              </Col>
                            </Row>
                          </Col>
                        )}  */}
                      </Row>
                    </Skeleton>
                  </div>
                </Col>
              </Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    onClick={() => history.goBack()}
                    title="Quay lại"
                    color={colors.main}
                    icon={ISvg.NAME.ARROWTHINLEFT}
                    style={{ marginRight: 12 }}
                    styleHeight={{
                      width: 140,
                    }}
                  />
                  <IButton
                    title="Tiếp tục"
                    color={colors.main}
                    htmlType="submit"
                    icon={ISvg.NAME.ARROWRIGHT}
                    iconRight={true}
                    styleHeight={{ width: 160 }}
                  />
                </div>
              </Col>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const createEventImmediateNewDescriptionNew = Form.create({
  name: "CreateEventImmediateNewDescriptionNew",
})(CreateEventImmediateNewDescriptionNew);

export default createEventImmediateNewDescriptionNew;
