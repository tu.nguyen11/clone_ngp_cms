import { Col, Row, Modal } from "antd";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";

import moment from "moment";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";
import { colors } from "../../../../assets";
import { IButton, ISvg } from "../../../components";
import {
  CLEAR_REDUX_IMMEDIATE_NEW,
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
  DEAFUAT_MODAL,
} from "../../../store/reducers";
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1,
  useModalAgencyS2,
} from "../../../components/common";
import { APIService } from "../../../../services";
import useModalEvent from "./useModalEvent";

import { DefineKeyEvent } from "../../../../utils/DefineKey";
export default function CreateEventImmediateNew(props) {
  const [isOpenApply, setIsOpenApply] = useState(false);
  const dataRoot = useSelector((state) => state);
  const { eventImmediateNew, eventImmediate } = dataRoot;
  const history = useHistory();
  const params = useParams();
  const { id } = params;
  const format = "HH:mm:ss DD-MM-YYYY";
  const dispatch = useDispatch();
  const [isVisibleAgency1, setVisibleAgency1] = useState(false);
  const [isVisibleAgency2, setVisibleAgency2] = useState(false);
  const [modalEvent, setModalEvent] = useState(false);

  const [arrDependent, setArrDependent] = useState([
    {
      name: "Không phụ thuộc",
      active: true,
      type: 1,
    },
    {
      name: "Thiết lập phụ thuộc",
      active: false,
      type: 2,
    },
  ]);

  const handleStartOpenChange = (open) => {
    if (!open) {
      setIsOpenApply(true);
    }
  };

  // const fetchAPIgetCheckExistPriotyInEvent = async index => {
  //   try {
  //     const data = await APIService._getCheckExistPriotyInEvent(index)
  //     dispatch(
  //       CREATE_DATE_EVENT_IMMEDIATE({
  //         key: 'checkPriori',
  //         value: data.exist_index === 0
  //       })
  //     )
  //   } catch (error) {
  //     console.log(error)
  //   }
  // }

  const _getAPIListAgencyS1 = async () => {
    try {
      const data = await APIService._getListAllAgencyS1(1, 0, 0, -1, "", 0);
      let useragency = [...data.useragency];
      useragency.map((item, index) => {
        // useragency[index].stt = (0 - 1) * 10 + index + 1;
        useragency[index].checked = false;
      });
      dispatch(
        DEAFUAT_MODAL({
          data: useragency,
          keyInitial_listShow: "listS1Show",
          keyInitial_listAddClone: "listS1AddClone",
          keyInitial_listBefore: "listS1Before",
          keyInitial_list: "listS1",
          keyInitial_listClone: "listS1Clone",
          keyInitial_root: "modalS1",
        })
      );
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    const arrDependentClone = arrDependent.map((item) => {
      if (item.type === eventImmediateNew.check_dependent) {
        item.active = true;
      } else {
        item.active = false;
      }
      return item;
    });

    setArrDependent(arrDependentClone);
  }, []);

  const handleEndOpenChange = (open) => {
    setIsOpenApply(open);
  };

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);
    if (startValue.valueOf() < dateNow.getTime()) {
      return true;
    }
    if (!startValue || !eventImmediateNew.to_time_apply) {
      return false;
    }
    return startValue.valueOf() > eventImmediateNew.to_time_apply;
  };

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !eventImmediateNew.from_time_apply) {
      return false;
    }
    return endValue.valueOf() <= eventImmediateNew.from_time_apply;
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo Chương trình khuyến mãi trên đơn
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                    history.push("/event/immediate/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ width: 1400 }}>
            <Row gutter={[24, 24]} type="flex">
              <Col span={6}>
                <div
                  style={{ padding: 24, height: 600 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading
                        minFont="10px"
                        maxFont="16px"
                        // style={{ fontWeight: "bold" }}
                      >
                        Thiết lập chương trình sinh nhật
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: "flex", cursor: "pointer" }}
                        onClick={() => {
                          const dataAction = {
                            key: "event_birthday",
                            value: !eventImmediateNew.event_birthday,
                          };

                          dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction));
                        }}
                      >
                        <div
                          style={{
                            width: 20,
                            height: 20,
                            border: eventImmediateNew.event_birthday
                              ? `5px solid ${colors.main}`
                              : `1px solid ${colors.line_2}`,
                            borderRadius: 10,
                          }}
                        />
                        <div style={{ flex: 1, marginLeft: 10 }}>
                          <span
                            style={{
                              fontSize: 14,
                              color: colors.text.black,
                            }}
                          >
                            Chương trình sinh nhật
                          </span>
                        </div>
                      </div>
                    </Col>
                    <Col span={24}>
                      <StyledITileHeading
                        minFont="10px"
                        maxFont="16px"
                        // style={{ fontWeight: "bold" }}
                      >
                        Khai báo chương trình phụ thuộc
                      </StyledITileHeading>
                    </Col>
                    <Col span={24} style={{ padding: 0 }}>
                      <div style={{ marginTop: 10 }}>
                        {arrDependent.map((item, index) => {
                          return (
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                paddingTop: index === 0 ? 0 : 15,
                              }}
                              className="cursor"
                              onClick={() => {
                                let data = [...arrDependent];
                                data.map((item, index1) => {
                                  if (index == index1) {
                                    item.active = true;
                                    if (item.type === 1) {
                                      dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                                      _getAPIListAgencyS1();
                                    }
                                  } else {
                                    item.active = false;
                                  }

                                  const dataAction = {
                                    key: "check_dependent",
                                    value: data[index].type,
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction)
                                  );

                                  const dataAction1 = {
                                    key: "event_dependent_confirm",
                                    value: {},
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction1)
                                  );

                                  const dataAction2 = {
                                    key: "event_dependent_tmp",
                                    value: {},
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction2)
                                  );

                                  let arrEvent = JSON.parse(
                                    JSON.stringify(
                                      eventImmediateNew.arr_event_tmp
                                    )
                                  );

                                  let arrEventNew = arrEvent.map((item) => {
                                    item.active = 0;
                                    return item;
                                  });

                                  const dataAction3 = {
                                    key: "arr_event_tmp",
                                    value: arrEventNew,
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction3)
                                  );

                                  const dataAction4 = {
                                    key: "arr_event_confirm",
                                    value: arrEventNew,
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction4)
                                  );

                                  setArrDependent(data);
                                });
                              }}
                            >
                              <div
                                style={{
                                  width: 20,
                                  height: 20,
                                  border: item.active
                                    ? `5px solid ${colors.main}`
                                    : `1px solid ${colors.line_2}`,
                                  borderRadius: 10,
                                }}
                              />
                              <div style={{ flex: 1, marginLeft: 10 }}>
                                <span
                                  style={{
                                    fontSize: 14,
                                    color: colors.text.black,
                                  }}
                                >
                                  {item.name}
                                </span>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </Col>
                    {eventImmediateNew.check_dependent !== 2 ? null : (
                      <>
                        <Col span={24}>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              marginTop: 5,
                            }}
                          >
                            <IButton
                              title="Chọn chương trình"
                              color={colors.main}
                              styleHeight={{
                                width: 180,
                              }}
                              onClick={() => {
                                setModalEvent(true);
                              }}
                            />
                          </div>
                        </Col>
                        <Col span={24}>
                          <span>
                            {!eventImmediateNew.event_dependent_confirm
                              ? null
                              : eventImmediateNew.event_dependent_confirm.name}
                          </span>
                        </Col>
                      </>
                    )}
                  </Row>
                </div>
              </Col>
              <Col span={10}>
                <div
                  style={{
                    padding: 24,
                    height: 600,
                    background:
                      Object.keys(eventImmediateNew.event_dependent_confirm)
                        .length > 0
                        ? "rgba(122, 123, 123, 0.1)"
                        : "",
                  }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thời gian áp dụng & trả thưởng
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 600 }}>
                          Thời gian áp dụng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>
                                Bắt đầu
                                <span style={{ color: "red", marginLeft: 4 }}>
                                  *
                                </span>
                              </TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                // disabledDate={disabledStartDateApply}
                                paddingInput="0px"
                                placeholder="nhập thời gian bắt đầu"
                                onOpenChange={handleStartOpenChange}
                                format={format}
                                value={
                                  eventImmediateNew.from_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediateNew.from_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "from_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>
                                Kết thúc
                                <span style={{ color: "red", marginLeft: 4 }}>
                                  *
                                </span>
                              </TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledEndDateApply}
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                isBorderBottom={true}
                                // disabled={
                                //   event.type_event_status ===
                                //   DefineKeyEvent.event_running
                                // }
                                paddingInput="0px"
                                open={isOpenApply}
                                format={format}
                                onOpenChange={handleEndOpenChange}
                                value={
                                  eventImmediateNew.to_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediateNew.to_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "to_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );
                                }}
                                placeholder="nhập thời gian kết thúc"
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>

              <Col span={8}>
                <div
                  style={{
                    padding: 24,
                    height: 600,
                    background:
                      Object.keys(eventImmediateNew.event_dependent_confirm)
                        .length > 0
                        ? "rgba(122, 123, 123, 0.1)"
                        : "",
                  }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 24 }}>
                        <Row>
                          <Col span={6}></Col>
                          <Col span={12}>
                            <IButton
                              title="Tạo điều kiện lọc"
                              disabled={eventImmediateNew.is_evet_parent === 2}
                              color={
                                eventImmediateNew.type_way ===
                                DefineKeyEvent.event_type_assigned
                                  ? "white"
                                  : colors.main
                              }
                              styleHeight={{
                                background:
                                  eventImmediateNew.type_way ===
                                  DefineKeyEvent.event_type_assigned
                                    ? colors.main
                                    : Object.keys(
                                        eventImmediateNew.event_dependent_confirm
                                      ).length > 0
                                    ? "rgba(122, 123, 123, 0.1)"
                                    : "white",
                              }}
                              onClick={() => {
                                const data = {
                                  key: "type_way",
                                  value: DefineKeyEvent.event_type_assigned,
                                };
                                dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(data));
                                // khi nhấn vô push qua màn hình filter
                                history.push(
                                  `/create/event/immediate/filter/0`
                                );
                              }}
                            />
                          </Col>
                          <Col span={6}></Col>
                        </Row>
                      </div>
                    </Col>
                    <Col span={24}>
                      <IButton
                        title="Chọn theo Danh sách đại lý cấp 1"
                        color={
                          eventImmediateNew.type_way ===
                          DefineKeyEvent.event_type_S1
                            ? "white"
                            : colors.main
                        }
                        disabled={eventImmediateNew.is_evet_parent === 2}
                        styleHeight={{
                          background:
                            eventImmediateNew.type_way ===
                            DefineKeyEvent.event_type_S1
                              ? colors.main
                              : Object.keys(
                                  eventImmediateNew.event_dependent_confirm
                                ).length > 0
                              ? "rgba(122, 123, 123, 0.1)"
                              : "white",
                        }}
                        onClick={() => {
                          const data = {
                            key: "type_way",
                            value: DefineKeyEvent.event_type_S1,
                          };
                          dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(data));
                          setVisibleAgency1(true);
                        }}
                      />
                    </Col>
                    {/* <Col span={24}>
                      <IButton
                        title='Chọn theo Danh sách đại lý cấp 2'
                        color={
                          eventImmediateNew.type_way ===
                          DefineKeyEvent.event_type_S2
                            ? 'white'
                            : colors.main
                        }
                        disabled={eventImmediateNew.is_evet_parent === 2}
                        styleHeight={{
                          background:
                            eventImmediateNew.type_way ===
                            DefineKeyEvent.event_type_S2
                              ? colors.main
                              : Object.keys(
                                  eventImmediateNew.event_dependent_confirm
                                ).length > 0
                              ? 'rgba(122, 123, 123, 0.1)'
                              : 'white'
                        }}
                        onClick={() => {
                          const data = {
                            key: 'type_way',
                            value: DefineKeyEvent.event_type_S2
                          }
                          dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(data))
                          setVisibleAgency2(true)
                        }}
                      />
                    </Col> */}
                  </Row>
                </div>
              </Col>
            </Row>

            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={async () => {
                    let dateNow = new Date();
                    dateNow.setHours(
                      dateNow.getHours() + 1,
                      dateNow.getMinutes() - 5
                    );
                    if (
                      eventImmediateNew.from_time_apply >
                      eventImmediateNew.to_time_apply
                    ) {
                      const content = (
                        <span>
                          Thời gian kết thúc luôn luôn lớn hơn thời gian bắt đầu
                        </span>
                      );
                      return IError(content);
                    }

                    if (
                      eventImmediateNew.from_time_apply === 0 ||
                      eventImmediateNew.to_time_apply === 0 ||
                      eventImmediateNew.type_way === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian áp dụng
                          </span>{" "}
                          & <span style={{ fontWeight: 600 }}>trả thưởng</span>{" "}
                          ,{" "}
                          <span style={{ fontWeight: 600 }}>
                            đối tượng áp dụng
                          </span>{" "}
                          chương trình
                        </span>
                      );
                      return IError(content);
                    }

                    if (eventImmediateNew.is_evet_parent === 2) {
                      return history.push(
                        `/create/event/immediate/design/${id}`
                      );
                    }

                    switch (eventImmediateNew.type_way) {
                      case DefineKeyEvent.event_type_assigned:
                        history.push(`/create/event/immediate/filter/${id}`);
                        break;
                      case DefineKeyEvent.event_type_S1:
                        history.push(
                          `/create/event/immediate/filter/agency/1/${id}`
                        );
                        break;
                      case DefineKeyEvent.event_type_S2:
                        history.push(
                          `/create/event/immediate/filter/agency/2/${id}`
                        );
                        break;
                      default:
                        history.push(
                          `/create/event/immediate/description/${id}`
                        );
                        break;
                    }
                  }}
                  title="Tiếp tục"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isVisibleAgency1}
        footer={null}
        width={1100}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1(
          () => {
            setVisibleAgency1(false);
          },
          () => {
            history.push(`/create/event/immediate/filter/agency/1/0`);
          }
        )}
      </Modal>
      <Modal
        visible={isVisibleAgency2}
        footer={null}
        width={1100}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS2(
          () => {
            setVisibleAgency2(false);
          },
          () => {
            history.push(`/create/event/immediate/filter/agency/1/0`);
          }
        )}
      </Modal>
      <Modal
        visible={modalEvent}
        footer={null}
        width={800}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalEvent(() => {
          setModalEvent(false);
        })}
      </Modal>
    </div>
  );
}
