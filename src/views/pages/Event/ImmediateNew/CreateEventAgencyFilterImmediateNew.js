import { Col, Row, Modal, Checkbox, InputNumber } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { APIService } from "../../../../services";
import { colors } from "../../../../assets";
import { IButton, ISvg, ITableHtml } from "../../../components";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";

import moment from "moment";
import {
  CREATE_DATE_EVENT_IMMEDIATE,
  CLEAR_REDUX_IMMEDIATE_NEW,
  RESET_MODAL,
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
} from "../../../store/reducers";
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1,
  useModalAgencyS2,
} from "../../../components/common";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import useModalEvent from "./useModalEvent";

export default function CreateEventAgencyFilterImmediateNew(props) {
  const [isOpenApply, setIsOpenApply] = useState(false);
  const history = useHistory();
  const params = useParams();
  const { id } = params;
  const format = "HH:mm:ss DD-MM-YYYY";

  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { modalS1, modalS2, eventImmediateNew } = dataRoot;

  const [isVisibleAgency, setVisibleAgency] = useState(false);
  const [modalEvent, setModalEvent] = useState(false);

  const [arrDependent, setArrDependent] = useState([
    {
      name: "Không phụ thuộc",
      active: true,
      type: 1,
    },
    {
      name: "Thiết lập phụ thuộc",
      active: false,
      type: 2,
    },
  ]);

  const handleStartOpenChange = (open) => {
    if (!open) {
      setIsOpenApply(true);
    }
  };

  const handleEndOpenChange = (open) => {
    setIsOpenApply(open);
  };

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);
    if (startValue.valueOf() < dateNow.getTime()) {
      return true;
    }
    if (!startValue || !eventImmediateNew.to_time_apply) {
      return false;
    }
    return startValue.valueOf() > eventImmediateNew.to_time_apply;
  };

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !eventImmediateNew.from_time_apply) {
      return false;
    }
    return endValue.valueOf() <= eventImmediateNew.from_time_apply;
  };

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp đại lý",
      align: "center",
    },
  ];

  const headerTable = (dataHeader) => {
    return (
      <tr className="scroll">
        {dataHeader.map((item, index) => (
          <th className="th-table" style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (databody) => {
    return databody.map((item) => (
      <tr>
        <td
          className="td-table"
          style={{
            textAlign: "left",
            fontWeight: "normal",
            background:
              Object.keys(eventImmediateNew.event_dependent_confirm).length > 0
                ? "rgba(122, 123, 123, 0.1)"
                : "white",
          }}
        >
          {eventImmediateNew.type_way === 2
            ? item.shop_name || item.name
            : item.supplierName || item.name}
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "left",
            fontWeight: "normal",
            background:
              Object.keys(eventImmediateNew.event_dependent_confirm).length > 0
                ? "rgba(122, 123, 123, 0.1)"
                : "white",
          }}
        >
          {eventImmediateNew.type_way === 2
            ? item.dms_code || item.code
            : item.supplierCode || item.code}
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "center",
            fontWeight: "normal",
            background:
              Object.keys(eventImmediateNew.event_dependent_confirm).length > 0
                ? "rgba(122, 123, 123, 0.1)"
                : "white",
          }}
        >
          {eventImmediateNew.type_way === 2 ? "Cấp 1" : "Cấp 2"}
        </td>
      </tr>
    ));
  };

  const _renderModalS1 = () => {
    return (
      <Modal
        visible={isVisibleAgency}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1(() => {
          setVisibleAgency(false);
        })}
      </Modal>
    );
  };

  const _renderModalS2 = () => {
    return (
      <Modal
        visible={isVisibleAgency}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS2(() => {
          setVisibleAgency(false);
        })}
      </Modal>
    );
  };

  useEffect(() => {
    const arrDependentClone = arrDependent.map((item) => {
      if (item.type === eventImmediateNew.check_dependent) {
        item.active = true;
      } else {
        item.active = false;
      }
      return item;
    });

    setArrDependent(arrDependentClone);
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo Chương trình khuyến mãi trên đơn
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                    history.push("/event/immediate/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1400 }}>
            <Row gutter={[24, 24]} type="flex">
              <Col span={6}>
                <div
                  style={{ padding: 24, height: "100%" }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading
                        minFont="10px"
                        maxFont="16px"
                        // style={{ fontWeight: "bold" }}
                      >
                        Thiết lập chương trình sinh nhật
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: "flex", cursor: "pointer" }}
                        onClick={() => {
                          const dataAction = {
                            key: "event_birthday",
                            value: !eventImmediateNew.event_birthday,
                          };

                          dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction));
                        }}
                      >
                        <div
                          style={{
                            width: 20,
                            height: 20,
                            border: eventImmediateNew.event_birthday
                              ? `5px solid ${colors.main}`
                              : `1px solid ${colors.line_2}`,
                            borderRadius: 10,
                          }}
                        />
                        <div style={{ flex: 1, marginLeft: 10 }}>
                          <span
                            style={{
                              fontSize: 14,
                              color: colors.text.black,
                            }}
                          >
                            Chương trình sinh nhật
                          </span>
                        </div>
                      </div>
                    </Col>

                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Khai báo chương trình phụ thuộc
                      </StyledITileHeading>
                    </Col>
                    <Col span={24} style={{ padding: 0 }}>
                      <div style={{ marginTop: 10 }}>
                        {arrDependent.map((item, index) => {
                          return (
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                paddingTop: index === 0 ? 0 : 15,
                              }}
                            >
                              <div
                                style={{
                                  width: 20,
                                  height: 20,
                                  border: item.active
                                    ? "5px solid" + `${colors.main}`
                                    : "1px solid" + `${colors.line_2}`,
                                  borderRadius: 10,
                                }}
                                onClick={() => {
                                  let data = [...arrDependent];
                                  data.map((item, index1) => {
                                    if (index == index1) {
                                      item.active = true;
                                      if (item.type === 1) {
                                        dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                                      }
                                    } else {
                                      item.active = false;
                                    }
                                    const dataAction = {
                                      key: "check_dependent",
                                      value: data[index].type,
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction
                                      )
                                    );

                                    const dataAction1 = {
                                      key: "event_dependent_confirm",
                                      value: {},
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction1
                                      )
                                    );

                                    const dataAction2 = {
                                      key: "event_dependent_tmp",
                                      value: {},
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction2
                                      )
                                    );

                                    let arrEvent = JSON.parse(
                                      JSON.stringify(
                                        eventImmediateNew.arr_event_tmp
                                      )
                                    );

                                    let arrEventNew = arrEvent.map((item) => {
                                      item.active = 0;
                                      return item;
                                    });

                                    const dataAction3 = {
                                      key: "arr_event_tmp",
                                      value: arrEventNew,
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction3
                                      )
                                    );

                                    const dataAction4 = {
                                      key: "arr_event_confirm",
                                      value: arrEventNew,
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction4
                                      )
                                    );

                                    setArrDependent(data);
                                  });
                                }}
                              />
                              <div style={{ flex: 1, marginLeft: 10 }}>
                                <span
                                  style={{
                                    fontSize: 14,
                                    color: colors.text.black,
                                  }}
                                >
                                  {item.name}
                                </span>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </Col>
                    {eventImmediateNew.check_dependent !== 2 ? null : (
                      <>
                        <Col span={24}>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              marginTop: 5,
                            }}
                          >
                            <IButton
                              title="Chọn chương trình"
                              color={colors.main}
                              styleHeight={{
                                width: 180,
                              }}
                              onClick={() => {
                                setModalEvent(true);
                              }}
                            />
                          </div>
                        </Col>
                        <Col span={24}>
                          <span>
                            {!eventImmediateNew.event_dependent_confirm
                              ? null
                              : eventImmediateNew.event_dependent_confirm.name}
                          </span>
                        </Col>
                      </>
                    )}
                  </Row>
                </div>
              </Col>
              <Col span={9}>
                <div
                  style={{
                    padding: 24,
                    height: "100%",
                    background:
                      Object.keys(eventImmediateNew.event_dependent_confirm)
                        .length > 0
                        ? "rgba(122, 123, 123, 0.1)"
                        : "",
                  }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thời gian áp dụng & trả thưởng
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Thời gian áp dụng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>
                                Bắt đầu
                                <span style={{ color: "red", marginLeft: 4 }}>
                                  *
                                </span>
                              </TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                // disabledDate={disabledStartDateApply}
                                paddingInput="0px"
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                placeholder="nhập thời gian bắt đầu"
                                onOpenChange={handleStartOpenChange}
                                format={format}
                                value={
                                  eventImmediateNew.from_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediateNew.from_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "from_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>
                                Kết thúc
                                <span style={{ color: "red", marginLeft: 4 }}>
                                  *
                                </span>
                              </TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledEndDateApply}
                                isBorderBottom={true}
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                paddingInput="0px"
                                open={isOpenApply}
                                format={format}
                                onOpenChange={handleEndOpenChange}
                                value={
                                  eventImmediateNew.to_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediateNew.to_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "to_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );
                                }}
                                placeholder="nhập thời gian kết thúc"
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>

              <Col span={9}>
                <div
                  style={{
                    padding: 24,
                    minHeight: 650,
                    background:
                      Object.keys(eventImmediateNew.event_dependent_confirm)
                        .length > 0
                        ? "rgba(122, 123, 123, 0.1)"
                        : "",
                  }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 24]}>
                    <Col>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Chỉnh sửa danh sách"
                          color={colors.main}
                          icon={ISvg.NAME.WRITE}
                          styleHeight={{
                            background:
                              Object.keys(
                                eventImmediateNew.event_dependent_confirm
                              ).length !== 0
                                ? "rgba(122, 123, 123, 0.1)"
                                : "white",
                          }}
                          disabled={eventImmediateNew.is_evet_parent === 2}
                          onClick={() => setVisibleAgency(true)}
                        />
                      </div>
                    </Col>

                    <Col span={24}>
                      <ITableHtml
                        height="420px"
                        childrenBody={bodyTable(
                          eventImmediateNew.type_way === 2
                            ? modalS1.listS1Show
                            : modalS2.listS2Show
                        )}
                        childrenHeader={headerTable(dataHeader)}
                        style={{
                          borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                          borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                          // padding: '0px 8px '
                        }}
                        isBorder={false}
                      />
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: "flex", justifyContent: "flex-end" }}
                      >
                        <IButton
                          title="Hủy"
                          color={colors.oranges}
                          icon={ISvg.NAME.CROSS}
                          styleHeight={{
                            width: 140,
                            background:
                              Object.keys(
                                eventImmediateNew.event_dependent_confirm
                              ).length !== 0
                                ? "rgba(122, 123, 123, 0.1)"
                                : "white",
                          }}
                          disabled={eventImmediateNew.is_evet_parent === 2}
                          onClick={() => {
                            const data = {
                              key: "type_way",
                              value: 0,
                            };
                            dispatch(CREATE_DATE_EVENT_IMMEDIATE(data));
                            eventImmediateNew.type_way === 2
                              ? dispatch(
                                  RESET_MODAL({
                                    keyInitial_listShow: "listS1Show",
                                    keyInitial_root: "modalS1",
                                    keyInitial_listAddClone: "listS1AddClone",
                                    keyInitial_listAdd: "listS1Add",
                                    keyInitial_list: "listS1",
                                  })
                                )
                              : dispatch(
                                  RESET_MODAL({
                                    keyInitial_listShow: "listS2Show",
                                    keyInitial_root: "modalS2",
                                    keyInitial_listAddClone: "listS2AddClone",
                                    keyInitial_listAdd: "listS2Add",
                                    keyInitial_list: "listS2",
                                  })
                                );

                            history.goBack();
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={async () => {
                    let dateNow = new Date();
                    dateNow.setHours(
                      dateNow.getHours() + 1,
                      dateNow.getMinutes() - 5
                    );

                    if (
                      eventImmediateNew.from_time_apply >
                      eventImmediateNew.to_time_apply
                    ) {
                      const content = (
                        <span>
                          Thời gian kết thúc luôn luôn lớn hơn thời gian bắt đầu
                        </span>
                      );
                      return IError(content);
                    }

                    if (eventImmediateNew.type_way === 2) {
                      if (modalS1.listS1Show.length === 0) {
                        return IError("Chưa chọn đại lý áp dụng chương trình");
                      }
                    } else {
                      if (modalS2.listS2Show.length === 0) {
                        return IError("Chưa chọn đại lý áp dụng chương trình");
                      }
                    }

                    if (eventImmediateNew.type_way === 2) {
                      if (modalS1.listS1Show.length === 0) {
                        return IError("Chưa chọn đại lý áp dụng chương trình");
                      }
                    } else {
                      if (modalS2.listS2Show.length === 0) {
                        return IError("Chưa chọn đại lý áp dụng chương trình");
                      }
                    }

                    if (
                      eventImmediateNew.from_time_apply === 0 ||
                      eventImmediateNew.to_time_apply === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian áp dụng
                          </span>{" "}
                          & <span style={{ fontWeight: 600 }}>trả thương</span>
                        </span>
                      );
                      return IError(content);
                    }

                    // if (
                    //   eventImmediateNew.from_time_apply <= dateNow.getTime()
                    // ) {
                    //   const content = (
                    //     <span>
                    //       Ngày bắt đầu chương trình phải lớn hơn giờ hiện tại 1
                    //       tiếng.
                    //     </span>
                    //   );
                    //   return IError(content);
                    // }

                    history.push(`/create/event/immediate/description/${id}`);
                  }}
                  title="Tiếp tục"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      {eventImmediateNew.type_way === 2 ? _renderModalS1() : _renderModalS2()}
      <Modal
        visible={modalEvent}
        footer={null}
        width={800}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalEvent(() => {
          setModalEvent(false);
        })}
      </Modal>
    </div>
  );
}
