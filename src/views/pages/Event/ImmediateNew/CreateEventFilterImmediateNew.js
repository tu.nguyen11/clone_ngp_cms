import { Col, Row, Tag, Popconfirm, Modal, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { APIService } from "../../../../services";
import { colors } from "../../../../assets";
import { IButton, ISvg } from "../../../components";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";
import useModalEvent from "./useModalEvent";

import moment from "moment";
import {
  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW,
  CLEAR_REDUX_IMMEDIATE_NEW,
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
  _fetchAPIListBuusinessRegion,
  REMOVE_COFIRM_MODAL_THUNK,
  _fetchAPIListDistrict,
  _fetchAPIListBusinessRegion,
  _fetchAPIListWard,
  CLEAR_MODAL_ALL_KD,
} from "../../../store/reducers";
import {
  IDatePickerFrom,
  IError,
  StyledISelect,
  TagP,
} from "../../../components/common";
import { DefineKeyEvent } from "../../../../utils/DefineKey";

export default function CreateEventFilterImmediateNew(props) {
  const [isOpenApply, setIsOpenApply] = useState(false);
  const history = useHistory();
  const params = useParams();
  const { id } = params;
  const format = "HH:mm:ss DD-MM-YYYY";

  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { eventImmediateNew } = dataRoot;

  const [loading, setLoading] = useState(false);
  const [dropdownRegion, setDropdownRegion] = useState([]);
  const [dropdownCity, setDropdownCity] = useState([]);
  const [listMembership, setListMembership] = useState([]);
  const [loadingCity, setLoadingCity] = useState(false);

  const [checkCondition1, setCheckCondition1] = useState(false);
  const [checkCondition2, setCheckCondition2] = useState(false);
  const [checkCondition3, setCheckCondition3] = useState(false);
  const [checkCondition4, setCheckCondition4] = useState(false);
  const [reload, setReload] = useState(true);

  const [modalEvent, setModalEvent] = useState(false);

  const [arrDependent, setArrDependent] = useState([
    {
      name: "Không phụ thuộc",
      active: true,
      type: 1,
    },
    {
      name: "Thiết lập phụ thuộc",
      active: false,
      type: 2,
    },
  ]);

  const handleStartOpenChange = (open) => {
    if (!open) {
      setIsOpenApply(true);
    }
  };

  const _getAPIListMembership = async () => {
    try {
      const data = await APIService._getListMemberships();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setListMembership(dataNew);
    } catch (error) {}
  };

  const _fetchAgencyGetRegion = async () => {
    try {
      const data = await APIService._getAllAgencyGetRegion();
      let dataNew = data.agency.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      dataNew.unshift({
        key: 0,
        value: "Toàn quốc",
      });
      setDropdownRegion(() => dataNew);
      setLoading(false);
      setReload(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const _fetchListCityByRegion = async (
    arrayRegion,
    all = false,
    remove = false
  ) => {
    try {
      const data = await APIService._getListCityByRegion(arrayRegion);

      let dataNew = data.list_cities.map((item) => {
        const key = item.id;
        const value = item.name;
        const parent = item.parent;
        return { key, value, parent };
      });

      if (!reload) {
        if (!remove) {
          if (all) {
            let arrID = data.list_cities.map((item) => item.id);
            const dataAction1 = {
              key: "arrayCity",
              value: arrID,
            };

            const dataAction2 = {
              key: "arrayCityClone",
              value: data.list_cities,
            };

            dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(dataAction1));
            dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(dataAction2));
          } else {
            if (eventImmediateNew.event_filter.arrayCity.length === 0) {
              let arrID = data.list_cities.map((item) => item.id);
              const dataAction1 = {
                key: "arrayCity",
                value: arrID,
              };

              const dataAction2 = {
                key: "arrayCityClone",
                value: data.list_cities,
              };

              dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(dataAction1));
              dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(dataAction2));
            } else {
              let dataCityNew = [];
              let dataIdNew = [];
              data.list_cities.forEach((item) => {
                if (item.parent === arrayRegion[arrayRegion.length - 1]) {
                  dataCityNew.push(item);
                  dataIdNew.push(item.id);
                }
              });

              let dataConcat = [
                ...eventImmediateNew.event_filter.arrayCityClone,
                ...dataCityNew,
              ];
              let dataIdConcat = [
                ...eventImmediateNew.event_filter.arrayCity,
                ...dataIdNew,
              ];

              const dataAction1 = {
                key: "arrayCity",
                value: dataIdConcat,
              };

              const dataAction2 = {
                key: "arrayCityClone",
                value: dataConcat,
              };

              dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(dataAction1));
              dispatch(CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(dataAction2));
            }
          }
        }
      }

      setDropdownCity(dataNew);
      setLoadingCity(false);
      setReload(false);
    } catch (error) {
      console.log(error);
      setLoadingCity(false);
    }
  };

  useEffect(() => {
    _apiSelectCityRegion();
  }, []);

  const _apiSelectCityRegion = async () => {
    await _getAPIListMembership();
    await _fetchAgencyGetRegion();
    await _fetchListCityByRegion(eventImmediateNew.event_filter.arrayRegion);
  };

  const handleEndOpenChange = (open) => {
    setIsOpenApply(open);
  };

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);
    if (startValue.valueOf() < dateNow.getTime()) {
      return true;
    }

    if (!startValue || !eventImmediateNew.to_time_apply) {
      return false;
    }
    return startValue.valueOf() > eventImmediateNew.to_time_apply;
  };

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !eventImmediateNew.from_time_apply) {
      return false;
    }
    return endValue.valueOf() <= eventImmediateNew.from_time_apply;
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo Chương trình khuyến mãi trên đơn
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                    history.push("/event/immediate/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1400 }}>
            <Row gutter={[24, 24]} type="flex">
              <Col span={6}>
                <div
                  style={{ padding: 24, height: "100%" }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading
                        minFont="10px"
                        maxFont="16px"
                        // style={{ fontWeight: "bold" }}
                      >
                        Thiết lập chương trình sinh nhật
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: "flex", cursor: "pointer" }}
                        onClick={() => {
                          const dataAction = {
                            key: "event_birthday",
                            value: !eventImmediateNew.event_birthday,
                          };

                          dispatch(CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction));
                        }}
                      >
                        <div
                          style={{
                            width: 20,
                            height: 20,
                            border: eventImmediateNew.event_birthday
                              ? `5px solid ${colors.main}`
                              : `1px solid ${colors.line_2}`,
                            borderRadius: 10,
                          }}
                        />
                        <div style={{ flex: 1, marginLeft: 10 }}>
                          <span
                            style={{
                              fontSize: 14,
                              color: colors.text.black,
                            }}
                          >
                            Chương trình sinh nhật
                          </span>
                        </div>
                      </div>
                    </Col>

                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Khai báo Chương trình phụ thuộc
                      </StyledITileHeading>
                    </Col>
                    <Col span={24} style={{ padding: 0 }}>
                      <div style={{ marginTop: 10 }}>
                        {arrDependent.map((item, index) => {
                          return (
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                paddingTop: index === 0 ? 0 : 15,
                              }}
                            >
                              <div
                                style={{
                                  width: 20,
                                  height: 20,
                                  border: item.active
                                    ? "5px solid" + `${colors.main}`
                                    : "1px solid" + `${colors.line_2}`,
                                  borderRadius: 10,
                                }}
                                onClick={() => {
                                  let data = [...arrDependent];
                                  data.map((item, index1) => {
                                    if (index == index1) {
                                      item.active = true;
                                      if (item.type === 1) {
                                        dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                                      }
                                    } else {
                                      item.active = false;
                                    }
                                    const dataAction = {
                                      key: "check_dependent",
                                      value: data[index].type,
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction
                                      )
                                    );

                                    const dataAction1 = {
                                      key: "event_dependent_confirm",
                                      value: {},
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction1
                                      )
                                    );

                                    const dataAction2 = {
                                      key: "event_dependent_tmp",
                                      value: {},
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction2
                                      )
                                    );

                                    let arrEvent = JSON.parse(
                                      JSON.stringify(
                                        eventImmediateNew.arr_event_tmp
                                      )
                                    );

                                    let arrEventNew = arrEvent.map((item) => {
                                      item.active = 0;
                                      return item;
                                    });

                                    const dataAction3 = {
                                      key: "arr_event_tmp",
                                      value: arrEventNew,
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction3
                                      )
                                    );

                                    const dataAction4 = {
                                      key: "arr_event_confirm",
                                      value: arrEventNew,
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                        dataAction4
                                      )
                                    );

                                    setArrDependent(data);
                                  });
                                }}
                              />
                              <div style={{ flex: 1, marginLeft: 10 }}>
                                <span
                                  style={{
                                    fontSize: 14,
                                    color: colors.text.black,
                                  }}
                                >
                                  {item.name}
                                </span>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </Col>
                    {eventImmediateNew.check_dependent !== 2 ? null : (
                      <>
                        <Col span={24}>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              marginTop: 5,
                            }}
                          >
                            <IButton
                              title="Chọn chương trình"
                              color={colors.main}
                              styleHeight={{
                                width: 180,
                              }}
                              onClick={() => {
                                setModalEvent(true);
                              }}
                            />
                          </div>
                        </Col>
                        <Col span={24}>
                          <span>
                            {!eventImmediateNew.event_dependent_confirm
                              ? null
                              : eventImmediateNew.event_dependent_confirm.name}
                          </span>
                        </Col>
                      </>
                    )}
                  </Row>
                </div>
              </Col>
              <Col span={9}>
                <div
                  style={{
                    padding: 24,
                    height: "100%",
                    background:
                      Object.keys(eventImmediateNew.event_dependent_confirm)
                        .length > 0
                        ? "rgba(122, 123, 123, 0.1)"
                        : "",
                  }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thời gian áp dụng & trả thưởng
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Thời gian áp dụng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>
                                Bắt đầu{" "}
                                <span style={{ color: "red", marginLeft: 4 }}>
                                  *
                                </span>
                              </TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput="0px"
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                placeholder="nhập thời gian bắt đầu"
                                onOpenChange={handleStartOpenChange}
                                format={format}
                                value={
                                  eventImmediateNew.from_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediateNew.from_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "from_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>
                                Kết thúc
                                <span style={{ color: "red", marginLeft: 4 }}>
                                  *
                                </span>
                              </TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledEndDateApply}
                                isBorderBottom={true}
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                paddingInput="0px"
                                open={isOpenApply}
                                format={format}
                                onOpenChange={handleEndOpenChange}
                                value={
                                  eventImmediateNew.to_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(
                                          eventImmediateNew.to_time_apply
                                        ),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "to_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );
                                }}
                                placeholder="nhập thời gian kết thúc"
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>

              <Col span={9}>
                <div
                  style={{
                    padding: 24,
                    minHeight: 650,
                    background:
                      Object.keys(eventImmediateNew.event_dependent_confirm)
                        .length > 0
                        ? "rgba(122, 123, 123, 0.1)"
                        : "",
                  }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 24]}>
                    <Col>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div>
                        {/* DK tham gia 1 */}
                        <Col span={24}>
                          <div
                            style={{
                              marginTop: 16,
                            }}
                          >
                            {/* {checkCondition1 === true ||
                        eventImmediateNew.event_filter.id_agency === 0 ? (
                          <TagP
                            style={{
                              fontWeight: 500,
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <span
                              style={{
                                color: "red",
                                fontSize: 20,
                                marginRight: 10,
                              }}
                            >
                              *
                            </span>
                            <div>Điều kiện tham gia 1</div>
                          </TagP>
                        ) : ( */}
                            <TagP
                              style={{
                                fontWeight: 500,
                                marginLeft: 12,
                              }}
                            >
                              <div>Điều kiện tham gia 1</div>
                            </TagP>
                            {/* )} */}
                          </div>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[16, 16]}>
                            <Col span={24}>
                              <Row gutter={[0, 12]}>
                                <div
                                  style={{
                                    padding: 12,
                                    border:
                                      checkCondition1 === true
                                        ? "1px solid red"
                                        : "1px solid #d9d9d9",
                                  }}
                                >
                                  <Col>
                                    <div>
                                      <TagP
                                        style={{
                                          borderBottom: "1px solid #d9d9d9",
                                          paddingBottom: 16,
                                        }}
                                      >
                                        Cấp người dùng
                                      </TagP>
                                    </div>
                                  </Col>
                                  <Col>
                                    <StyledISelect
                                      isBorderBottom="1px solid #d9d9d9"
                                      defaultValue={
                                        eventImmediateNew.event_filter
                                          .id_agency === 0
                                          ? 1
                                          : eventImmediateNew.event_filter
                                              .id_agency
                                      }
                                      disabled={true}
                                      placeholder="Chọn đại lý"
                                      onChange={(key) => {
                                        const data = {
                                          key: "id_agency",
                                          value: key,
                                        };

                                        dispatch(
                                          CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                            data
                                          )
                                        );
                                        setCheckCondition1(false);
                                      }}
                                      data={[
                                        {
                                          value: "Đại lý cấp 1",
                                          key: DefineKeyEvent.agency_S1,
                                        },
                                        {
                                          value: "Đại lý cấp 2",
                                          key: DefineKeyEvent.agency_S2,
                                        },
                                      ]}
                                    />
                                  </Col>
                                </div>
                              </Row>
                            </Col>
                          </Row>
                        </Col>
                        {/* DK tham gia 2 */}
                        <Col span={24}>
                          <div>
                            {checkCondition2 === true ||
                            eventImmediateNew.event_filter.membership.length ===
                              0 ? (
                              <TagP
                                style={{
                                  fontWeight: 500,
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <span
                                  style={{
                                    color: "red",
                                    fontSize: 20,
                                    marginRight: 10,
                                  }}
                                >
                                  *
                                </span>
                                <div>Điều kiện tham gia 2</div>
                              </TagP>
                            ) : (
                              <TagP
                                style={{
                                  fontWeight: 500,
                                  marginLeft: 12,
                                }}
                              >
                                <div>Điều kiện tham gia 2</div>
                              </TagP>
                            )}
                          </div>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[16, 16]}>
                            <Col span={24}>
                              <Row gutter={[0, 12]}>
                                <div
                                  style={{
                                    padding: 12,
                                    border:
                                      checkCondition2 === true
                                        ? "1px solid red"
                                        : "1px solid #d9d9d9",
                                  }}
                                >
                                  <Col>
                                    <div
                                      style={{
                                        borderBottom: "1px solid #d9d9d9",
                                        paddingBottom: 6,
                                      }}
                                    >
                                      <Row>
                                        <Col span={12}>
                                          <span style={{}}>Cấp bậc</span>
                                        </Col>
                                        <Col span={12}>
                                          <div
                                            style={{
                                              display: "flex",
                                              justifyContent: "flex-end",
                                            }}
                                          >
                                            <Popconfirm
                                              placement="bottom"
                                              title={
                                                <span>
                                                  Xóa dữ liệu đang chọn <br />
                                                  của Cấp bậc
                                                </span>
                                              }
                                              onConfirm={() => {
                                                const data = {
                                                  key: "membership",
                                                  value: [],
                                                };

                                                dispatch(
                                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                    data
                                                  )
                                                );
                                              }}
                                              okText="Đồng ý"
                                              cancelText="Hủy"
                                            >
                                              <Tag color={colors.main}>Xóa</Tag>
                                            </Popconfirm>
                                          </div>
                                        </Col>
                                      </Row>
                                    </div>
                                  </Col>
                                  <Col>
                                    <StyledISelect
                                      mode="multiple"
                                      isBorderBottom="1px solid #d9d9d9"
                                      value={
                                        eventImmediateNew.event_filter
                                          .membership.length === 0
                                          ? undefined
                                          : eventImmediateNew.event_filter
                                              .membership
                                      }
                                      placeholder="Chọn cấp bậc"
                                      onChange={(arrKey) => {
                                        const isAll = arrKey.indexOf(0);
                                        if (isAll >= 0) {
                                          let arrayMembership = [
                                            ...listMembership,
                                          ];
                                          arrayMembership.splice(0, 1);
                                          let idArray = arrayMembership.map(
                                            (item) => item.key
                                          );
                                          const data = {
                                            key: "membership",
                                            value: [...idArray],
                                          };
                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              data
                                            )
                                          );
                                        } else {
                                          const data = {
                                            key: "membership",
                                            value: [...arrKey],
                                          };
                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              data
                                            )
                                          );
                                        }

                                        setCheckCondition2(false);
                                      }}
                                      data={listMembership}
                                    />
                                  </Col>
                                </div>
                              </Row>
                            </Col>
                          </Row>
                        </Col>
                        {/* DK tham gia 3 */}
                        <Col span={24}>
                          <div>
                            {checkCondition3 === true ||
                            eventImmediateNew.event_filter.arrayRegion
                              .length === 0 ? (
                              <TagP
                                style={{
                                  fontWeight: 500,
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <span
                                  style={{
                                    color: "red",
                                    fontSize: 20,
                                    marginRight: 10,
                                  }}
                                >
                                  *
                                </span>
                                <div>Điều kiện tham gia 3</div>
                              </TagP>
                            ) : (
                              <TagP
                                style={{ fontWeight: 500, paddingLeft: 12 }}
                              >
                                Điều kiện tham gia 3
                              </TagP>
                            )}
                          </div>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[16, 16]}>
                            <Col span={24}>
                              <Row gutter={[0, 12]}>
                                <div
                                  style={{
                                    padding: "6px 12px 12px 12px",
                                    border:
                                      checkCondition3 === true
                                        ? "1px solid red"
                                        : "1px solid #d9d9d9",
                                  }}
                                >
                                  <Col>
                                    <div
                                      style={{
                                        borderBottom: "1px solid #d9d9d9",
                                        paddingBottom: 6,
                                      }}
                                    >
                                      <Row>
                                        <Col span={12}>
                                          <span style={{}}>Vùng địa lý</span>
                                        </Col>
                                        <Col span={12}>
                                          <div
                                            style={{
                                              display: "flex",
                                              justifyContent: "flex-end",
                                            }}
                                          >
                                            <Popconfirm
                                              placement="bottom"
                                              title={
                                                <span>
                                                  Xóa dữ liệu đang chọn <br />
                                                  của Vùng địa lý
                                                </span>
                                              }
                                              onConfirm={() => {
                                                const data = {
                                                  key: "arrayCity",
                                                  value: [],
                                                };

                                                const dataCityClone = {
                                                  key: "arrayCityClone",
                                                  value: [],
                                                };
                                                dispatch(
                                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                    data
                                                  )
                                                );
                                                dispatch(
                                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                    dataCityClone
                                                  )
                                                );
                                                const dataArrRegion = {
                                                  key: "arrayRegion",
                                                  value: [],
                                                };
                                                dispatch(
                                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                    dataArrRegion
                                                  )
                                                );
                                                // _fetchListCityByRegion([]);
                                              }}
                                              okText="Đồng ý"
                                              cancelText="Hủy"
                                            >
                                              <Tag color={colors.main}>Xóa</Tag>
                                            </Popconfirm>
                                          </div>
                                        </Col>
                                      </Row>
                                    </div>
                                  </Col>
                                  <Col>
                                    <StyledISelect
                                      disabled={
                                        eventImmediateNew.type_event_status ===
                                        DefineKeyEvent.event_running
                                      }
                                      mode="multiple"
                                      isBorderBottom="1px solid #d9d9d9"
                                      placeholder="Chọn vùng địa lý"
                                      data={dropdownRegion}
                                      value={
                                        eventImmediateNew.event_filter
                                          .arrayRegion
                                      }
                                      onChange={(arrKey) => {
                                        if (
                                          arrKey.length <
                                          eventImmediateNew.event_filter
                                            .arrayRegion.length
                                        ) {
                                          let arrCityAfterRemove = [];
                                          let arrIDCityAfterRemove = [];
                                          arrKey.forEach((key) => {
                                            eventImmediateNew.event_filter.arrayCityClone.forEach(
                                              (item) => {
                                                if (item.parent === key) {
                                                  arrCityAfterRemove.push(item);
                                                  arrIDCityAfterRemove.push(
                                                    item.key || item.id
                                                  );
                                                }
                                              }
                                            );
                                          });

                                          const data = {
                                            key: "arrayRegion",
                                            value: [...arrKey],
                                          };

                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              data
                                            )
                                          );

                                          const dataCity = {
                                            key: "arrayCityClone",
                                            value: arrCityAfterRemove,
                                          };
                                          const dataCityShow = {
                                            key: "arrayCity",
                                            value: arrIDCityAfterRemove,
                                          };
                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              dataCity
                                            )
                                          );
                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              dataCityShow
                                            )
                                          );

                                          setLoadingCity(true);
                                          _fetchListCityByRegion(
                                            arrKey,
                                            false,
                                            true
                                          );
                                          setCheckCondition3(false);
                                        } else {
                                          if (arrKey.length === 0) {
                                            const dataCityRemove = {
                                              key: "arrayCityClone",
                                              value: [],
                                            };
                                            const dataCityShow = {
                                              key: "arrayCity",
                                              value: [],
                                            };
                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                dataCityRemove
                                              )
                                            );
                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                dataCityShow
                                              )
                                            );
                                          }
                                          const isAll = arrKey.indexOf(0);
                                          if (isAll >= 0) {
                                            let arrayAddress = [
                                              ...dropdownRegion,
                                            ];
                                            arrayAddress.splice(0, 1);
                                            let idArray = arrayAddress.map(
                                              (item) => item.key
                                            );
                                            const data = {
                                              key: "arrayRegion",
                                              value: [...idArray],
                                            };

                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                data
                                              )
                                            );
                                            setLoadingCity(true);
                                            _fetchListCityByRegion(
                                              idArray,
                                              true
                                            );
                                            setCheckCondition3(false);
                                            setCheckCondition4(false);
                                          } else {
                                            const data = {
                                              key: "arrayRegion",
                                              value: [...arrKey],
                                            };

                                            dispatch(
                                              CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                data
                                              )
                                            );
                                            setLoadingCity(true);
                                            _fetchListCityByRegion(arrKey);
                                            setCheckCondition3(false);
                                            setCheckCondition4(false);
                                          }
                                        }
                                      }}
                                    />
                                  </Col>
                                </div>
                              </Row>
                            </Col>
                          </Row>
                        </Col>
                        {/* DK tham gia 4 */}
                        <Col span={24}>
                          <div>
                            {checkCondition3 === true ||
                            eventImmediateNew.event_filter.arrayCity.length ===
                              0 ? (
                              <TagP
                                style={{
                                  fontWeight: 500,
                                  display: "flex",
                                  textAlign: "center",
                                }}
                              >
                                <span
                                  style={{
                                    color: "red",
                                    fontSize: 20,
                                    marginRight: 10,
                                  }}
                                >
                                  *
                                </span>
                                <div>Điều kiện tham gia 4</div>
                              </TagP>
                            ) : (
                              <TagP
                                style={{ fontWeight: 500, paddingLeft: 12 }}
                              >
                                Điều kiện tham gia 4
                              </TagP>
                            )}
                          </div>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[16, 16]}>
                            <Col span={24}>
                              <Row gutter={[0, 12]}>
                                <div
                                  style={{
                                    padding: 12,
                                    border: checkCondition4
                                      ? "1px solid red"
                                      : "1px solid #d9d9d9",
                                  }}
                                >
                                  <Col>
                                    <div
                                      style={{
                                        borderBottom: "1px solid #d9d9d9",
                                        paddingBottom: 6,
                                      }}
                                    >
                                      <Row>
                                        <Col span={12}>
                                          <span style={{}}>Tỉnh/Thành phố</span>
                                        </Col>
                                        <Col span={12}>
                                          <div
                                            style={{
                                              display: "flex",
                                              justifyContent: "flex-end",
                                            }}
                                          >
                                            <Popconfirm
                                              placement="bottom"
                                              title={
                                                <span>
                                                  Xóa dữ liệu đang chọn <br />
                                                  của Tỉnh / Thành phố
                                                </span>
                                              }
                                              disabled={
                                                eventImmediateNew.type_event_status ===
                                                DefineKeyEvent.event_running
                                              }
                                              onConfirm={() => {
                                                const data = {
                                                  key: "arrayCity",
                                                  value: [],
                                                };

                                                const dataCityClone = {
                                                  key: "arrayCityClone",
                                                  value: [],
                                                };

                                                const dataRegion = {
                                                  key: "arrayRegion",
                                                  value: [],
                                                };

                                                dispatch(
                                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                    data
                                                  )
                                                );
                                                dispatch(
                                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                    dataCityClone
                                                  )
                                                );
                                                dispatch(
                                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                    dataRegion
                                                  )
                                                );
                                              }}
                                              okText="Đồng ý"
                                              cancelText="Hủy"
                                            >
                                              <Tag color={colors.main}>Xóa</Tag>
                                            </Popconfirm>
                                          </div>
                                        </Col>
                                      </Row>
                                    </div>
                                  </Col>

                                  <Col>
                                    <StyledISelect
                                      mode="multiple"
                                      isBorderBottom="1px solid #d9d9d9"
                                      placeholder="Chọn tỉnh/ thành phố"
                                      style={{
                                        maxHeight: 200,
                                        overflow: "auto",
                                      }}
                                      disabled={
                                        // eventImmediateNew.event_filter.arrayRegion
                                        //   .length === 0 ||
                                        eventImmediateNew.type_event_status ===
                                        DefineKeyEvent.event_running
                                      }
                                      loading={loadingCity}
                                      onChange={(arrKey) => {
                                        if (arrKey.length === 0) {
                                          setDropdownCity([]);
                                        }
                                        if (
                                          arrKey.length <
                                          eventImmediateNew.event_filter
                                            .arrayCity.length
                                        ) {
                                          let arrCity = arrKey.map((item) => {
                                            return dropdownCity.find(
                                              (item1) => {
                                                return item1.key === item;
                                              }
                                            );
                                          });
                                          eventImmediateNew.event_filter.arrayRegion.forEach(
                                            (item, index) => {
                                              let arrCityOfRegion =
                                                arrCity.filter((key) => {
                                                  return key.parent === item;
                                                });

                                              if (
                                                arrCityOfRegion.length === 0
                                              ) {
                                                let arrRegionNew = [
                                                  ...eventImmediateNew
                                                    .event_filter.arrayRegion,
                                                ];
                                                arrRegionNew.splice(index, 1);
                                                const data = {
                                                  key: "arrayRegion",
                                                  value: [...arrRegionNew],
                                                };
                                                dispatch(
                                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                                    data
                                                  )
                                                );

                                                setLoadingCity(true);
                                                _fetchListCityByRegion(
                                                  arrRegionNew,
                                                  false,
                                                  true
                                                );
                                              }
                                            }
                                          );
                                        }
                                        const isAll = arrKey.indexOf(0);
                                        if (isAll >= 0) {
                                          let arrayCity = [...dropdownCity];
                                          arrayCity.splice(0, 1);
                                          let arrKeyID = [...arrayCity];
                                          let arrID = arrKeyID.map(
                                            (item) => item.key
                                          );
                                          const data = {
                                            key: "arrayCity",
                                            value: [...arrID],
                                          };

                                          const dataCityClone = {
                                            key: "arrayCityClone",
                                            value: [...arrKeyID],
                                          };
                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              data
                                            )
                                          );
                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              dataCityClone
                                            )
                                          );

                                          setCheckCondition4(false);
                                        } else {
                                          let arrKeyID = arrKey.map((item) => {
                                            return dropdownCity.find(
                                              (item1) => {
                                                return item1.key === item;
                                              }
                                            );
                                          });
                                          const data = {
                                            key: "arrayCity",
                                            value: [...arrKey],
                                          };

                                          const dataCityClone = {
                                            key: "arrayCityClone",
                                            value: [...arrKeyID],
                                          };
                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              data
                                            )
                                          );
                                          dispatch(
                                            CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                              dataCityClone
                                            )
                                          );

                                          setCheckCondition4(false);
                                        }
                                      }}
                                      // onDropdownVisibleChange={(open) => {
                                      //   if (open) {
                                      //     setLoadingCity(true);
                                      //     _fetchListCityByRegion(
                                      //       eventImmediateNew.event_filter.arrayRegion
                                      //     );
                                      //     return;
                                      //   }
                                      // }}
                                      value={
                                        eventImmediateNew.event_filter.arrayCity
                                      }
                                      data={dropdownCity}
                                    />
                                  </Col>
                                </div>
                              </Row>
                            </Col>
                          </Row>
                        </Col>

                        <Col span={24}>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "flex-end",
                            }}
                          >
                            <IButton
                              title="Hủy"
                              disabled={eventImmediateNew.is_evet_parent === 2}
                              color={colors.oranges}
                              icon={ISvg.NAME.CROSS}
                              styleHeight={{
                                width: 140,
                                background:
                                  Object.keys(
                                    eventImmediateNew.event_dependent_confirm
                                  ).length !== 0
                                    ? "rgba(122, 123, 123, 0.1)"
                                    : "white",
                              }}
                              onClick={() => {
                                // clear data

                                const data1 = {
                                  key: "id_agency",
                                  value: 0,
                                };
                                const data2 = {
                                  key: "arrayRegion",
                                  value: [],
                                };
                                const data3 = {
                                  key: "arrayCity",
                                  value: [],
                                };
                                const dataCityClone = {
                                  key: "arrayCityClone",
                                  value: [],
                                };
                                dispatch(
                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
                                    dataCityClone
                                  )
                                );
                                dispatch(
                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(data3)
                                );
                                dispatch(
                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(data2)
                                );
                                dispatch(
                                  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(data1)
                                );
                                history.goBack();
                              }}
                            />
                          </div>
                        </Col>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={async () => {
                    let dateNow = new Date();
                    dateNow.setHours(
                      dateNow.getHours() + 1,
                      dateNow.getMinutes() - 5
                    );

                    // if (eventImmediateNew.event_filter.id_agency === 0) {
                    //   setCheckCondition1(true);
                    // }
                    if (
                      eventImmediateNew.event_filter.arrayRegion.length === 0
                    ) {
                      setCheckCondition2(true);
                    }
                    if (eventImmediateNew.event_filter.arrayCity.length === 0) {
                      setCheckCondition3(true);
                    }

                    if (
                      eventImmediateNew.event_filter.membership.length === 0
                    ) {
                      setCheckCondition4(true);
                    }
                    if (
                      eventImmediateNew.from_time_apply === 0 ||
                      eventImmediateNew.to_time_apply === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian áp dụng
                          </span>{" "}
                          & <span style={{ fontWeight: 600 }}>trả thưởng</span>{" "}
                        </span>
                      );
                      return IError(content);
                    }

                    if (
                      eventImmediateNew.event_filter.membership.length === 0 ||
                      eventImmediateNew.event_filter.arrayCity.length === 0 ||
                      eventImmediateNew.event_filter.arrayRegion.length === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Nhóm đối tượng áp dụng chương trình
                          </span>
                        </span>
                      );
                      return IError(content);
                    }

                    if (eventImmediateNew.priori === 1) {
                      try {
                        const data =
                          await APIService._getCheckExistPriotyInEvent(
                            eventImmediateNew.valuePriori,
                            eventImmediateNew.event_filter.id_agency
                          );
                        if (data.exist_index === 0) {
                          return IError("Độ ưu tiên đã tồn tại");
                        }
                      } catch (error) {
                        console.log(error);
                      }
                    }

                    history.push(`/create/event/immediate/description/${id}`);
                  }}
                  title="Tiếp tục"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      <Modal
        visible={modalEvent}
        footer={null}
        width={800}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalEvent(() => {
          setModalEvent(false);
        })}
      </Modal>
    </div>
  );
}
