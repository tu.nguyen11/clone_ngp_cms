import { Col, Modal, Row, InputNumber, Checkbox, message } from "antd";
import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import { IButton, IInput, ISvg } from "../../../components";
import {
  IError,
  StyledInputNumber,
  StyledISelect,
  TagP,
  IInputTextArea,
} from "../../../components/common";
import useModalTreeProductImmediate from "../../../components/common/ModalAgency/useModalTreeProductImmediate";
import useModalTreeProductImmediateNewGiftGroup from "../../../components/common/ModalAgency/useModalTreeProductImmediateNewGiftGroup";

import {
  StyledITitle,
  StyledITileHeading,
} from "../../../components/common/Font/font";
import {
  CREATE_KEY_DEGSIN_IMMEDIATE_NEW,
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
  CLEAR_REDUX_IMMEDIATE_NEW,
  ADD_BONUS_LEVEL_EVENT_IMMEDIATE_NEW,
  ADD_GIFT_LEVEL_EVENT_IMMEDIATE_NEW,
  REMOVE_BONUS_LEVEL,
  REMOVE_GIFT_LEVEL,
  UPDATE_REWARD_PERFORMANCE,
  UPDATE_REWARD_PERFORMANCE_PRODUCT,
  UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE,
  ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE,
  UPDATE_GIFT_GROUP_PRODUCT,
  ADD_CONTENT_PRODUCT_NEW,
} from "../../../store/reducers";
import { Fragment } from "react";
import { createEventImmediate } from "./ButtonCreateImmediateNew";

export default function CreateEventDesignImmediateNew(props) {
  const [isVisibleTree, setVisibleTree] = useState(false);
  const [isVisibleTreeGift, setVisibleTreeGift] = useState(false);
  const params = useParams();
  const { id } = params;
  const history = useHistory();
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const {
    treeProductIImmediate,
    eventImmediateNew,
    treeProductImmediateGiftGroup,
    modalS1,
    modalS2,
    dataArrayCondition,
    modalBusiness,
    modalCity,
    modalDistrict,
    modalWard,
  } = dataRoot;
  const [dataDrowpCate, setDataDrowpCate] = useState([]);
  const [typeProduct, setTypeProduct] = useState([]);
  const [indexArrProductGiftGroup, setIndexArrProductGiftGroup] = useState(-1);

  const [loadingSubmit, setLoadingSubmit] = useState(false);

  const isCheckCondition =
    (eventImmediateNew.condition_promotion === 1 &&
      eventImmediateNew.gift_promotion !== 1) ||
    (eventImmediateNew.condition_promotion === 2 &&
      eventImmediateNew.gift_promotion !== 1) ||
    (eventImmediateNew.condition_promotion === 3 &&
      eventImmediateNew.gift_promotion !== 1);

  const _renderTitleCondition = (type) => {
    if (type === 1) return "Doanh số sản phẩm";
    if (type === 2) return "Số lượng sản phẩm";
    if (type === 3) return "Doanh số đơn hàng";
  };

  const backgroundColor =
    eventImmediateNew.is_evet_parent === 2 ? "#f0f0f0" : "white";

  const _fetchAPIPostCreateEvent = async (obj) => {
    try {
      const data = await APIService._postCreateEventNowNew(obj);
      dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
      message.success("Tạo chương trình thành công");
      setLoadingSubmit(false);
      history.push("/event/immediate/list");
    } catch (error) {
      setLoadingSubmit(false);
      console.log(error);
    }
  };

  const renderSalesOrder = (item, index) => {
    return (
      <Col span={24}>
        <Fragment>
          <Row gutter={[12, 0]}>
            <Col span={24}>
              <Fragment>
                <div
                  style={{
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                    padding: "15px 15px",
                  }}
                >
                  <Row gutter={[12, 12]} type="flex">
                    <Col span={5}>
                      <div
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span style={{ fontWeight: 600 }}>Mức xét thưởng</span>
                      </div>
                    </Col>
                    <Col span={8}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          height: "100%",
                        }}
                      >
                        <span>Từ:</span>
                        <StyledInputNumber
                          className="disabled-up"
                          formatter={(value) =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          min={
                            index === 0
                              ? 0
                              : eventImmediateNew.arr_bonus_level[index - 1]
                                  .to === 0
                              ? eventImmediateNew.arr_bonus_level[index - 1]
                                  .from
                              : eventImmediateNew.arr_bonus_level[index - 1].to
                          }
                          value={item.from}
                          onChange={(value) => {
                            const data = {
                              keyRoot: "arr_bonus_level",
                              key: "from",
                              idx: index,
                              value: value,
                            };
                            dispatch(UPDATE_REWARD_PERFORMANCE(data));
                          }}
                          parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                          style={{
                            margin: "0px 12px",
                            borderRadius: 0,
                            width: 200,
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={10}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          height: "100%",
                        }}
                      >
                        <span>Đến dưới:</span>
                        <StyledInputNumber
                          className="disabled-up"
                          formatter={(value) =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          // min={item.from}
                          value={item.to}
                          onChange={(value) => {
                            const data = {
                              keyRoot: "arr_bonus_level",
                              key: "to",
                              idx: index,
                              value: value,
                            };
                            dispatch(UPDATE_REWARD_PERFORMANCE(data));
                          }}
                          parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                          style={{
                            margin: "0px 12px",
                            borderRadius: 0,
                            border: item.valid_quantity_max
                              ? "1px solid red"
                              : "",
                            width: 200,
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={1}>
                      {index === 0 ? null : (
                        <div
                          style={{
                            display: "flex",
                            // justifyContent: 'flex-end',
                            alignItems: "center",
                            height: "100%",
                            width: "100%",
                            // background: 'red'
                          }}
                        >
                          <div
                            style={{
                              borderRadius: 10,
                              background: "rgba(227, 95, 75, 0.1)",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              marginTop: 3,
                              width: 20,
                              height: 20,
                            }}
                            className="cursor"
                            onClick={() => {
                              const data = {
                                key: "arr_bonus_level",
                                idx: index,
                              };
                              dispatch(REMOVE_BONUS_LEVEL(data));
                            }}
                          >
                            <ISvg
                              name={ISvg.NAME.CROSS}
                              width={8}
                              height={8}
                              fill={colors.oranges}
                            />
                          </div>
                        </div>
                      )}
                    </Col>
                  </Row>
                </div>
              </Fragment>
            </Col>

            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  borderTop: "1px solid white",
                  borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                  borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                  borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                  padding: "15px 15px",
                  with: "100%",
                }}
              >
                <Fragment>
                  <Row gutter={[12, 0]} style={{ width: "100%" }}>
                    <Col span={24}>
                      <Fragment>
                        <Row style={{ marginBottom: 15 }}>
                          <Col span={5}>
                            <div>
                              <span style={{ fontWeight: 600 }}>
                                Tỷ lệ chiết khẩu
                              </span>
                            </div>
                          </Col>
                          <Col span={19}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                height: "100%",
                              }}
                            >
                              <span style={{ opacity: 0, marginLeft: 4 }}>
                                Từ:
                              </span>

                              <StyledInputNumber
                                className="disabled-up"
                                formatter={(value) =>
                                  `${value}`.replace(
                                    /\B(?=(\d{3})+(?!\d))/g,
                                    ","
                                  )
                                }
                                min={0}
                                value={item.quantity_sale}
                                onChange={(value) => {
                                  const data = {
                                    keyRoot: "arr_bonus_level",
                                    key: "quantity_sale",
                                    idx: index,
                                    value: value,
                                  };
                                  dispatch(UPDATE_REWARD_PERFORMANCE(data));
                                }}
                                parser={(value) =>
                                  value.replace(/\$\s?|(,*)/g, "")
                                }
                                style={{
                                  margin: "0px 12px",
                                  borderRadius: 0,
                                  width: 200,
                                }}
                              />
                              <span>% Doanh số</span>
                            </div>
                          </Col>
                        </Row>
                      </Fragment>
                    </Col>
                  </Row>
                </Fragment>
              </div>
            </Col>
          </Row>
        </Fragment>
      </Col>
    );
  };

  const renderItemSalesOrCount = (item, index) => {
    return (
      <Col span={24}>
        <Fragment>
          <Row gutter={[12, 0]}>
            <Col span={24}>
              <Fragment>
                <div
                  style={{
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                    padding: "15px 15px",
                  }}
                >
                  <Row gutter={[12, 12]} type="flex">
                    <Col span={5}>
                      <div
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span style={{ fontWeight: 600 }}>Mức xét thưởng</span>
                      </div>
                    </Col>
                    <Col span={8}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          height: "100%",
                        }}
                      >
                        <span>Từ:</span>
                        <StyledInputNumber
                          className="disabled-up"
                          formatter={(value) =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          min={
                            index === 0
                              ? 0
                              : eventImmediateNew.arr_bonus_level[index - 1]
                                  .to === 0
                              ? eventImmediateNew.arr_bonus_level[index - 1]
                                  .from
                              : eventImmediateNew.arr_bonus_level[index - 1].to
                          }
                          value={item.from}
                          onChange={(value) => {
                            const data = {
                              keyRoot: "arr_bonus_level",
                              key: "from",
                              idx: index,
                              value: value,
                            };
                            dispatch(UPDATE_REWARD_PERFORMANCE(data));
                          }}
                          parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                          style={{
                            margin: "0px 12px",
                            borderRadius: 0,
                            width: 200,
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={10}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          height: "100%",
                        }}
                      >
                        <span>Đến dưới:</span>
                        <StyledInputNumber
                          className="disabled-up"
                          formatter={(value) =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          // min={item.from}
                          value={item.to}
                          onChange={(value) => {
                            const data = {
                              keyRoot: "arr_bonus_level",
                              key: "to",
                              idx: index,
                              value: value,
                            };
                            dispatch(UPDATE_REWARD_PERFORMANCE(data));
                          }}
                          parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                          style={{
                            margin: "0px 12px",
                            borderRadius: 0,
                            border: item.valid_quantity_max
                              ? "1px solid red"
                              : "",
                            width: 200,
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={1}>
                      {index === 0 ? null : (
                        <div
                          style={{
                            display: "flex",
                            // justifyContent: 'flex-end',
                            alignItems: "center",
                            height: "100%",
                            width: "100%",
                            // background: 'red'
                          }}
                        >
                          <div
                            style={{
                              borderRadius: 10,
                              background: "rgba(227, 95, 75, 0.1)",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              marginTop: 3,
                              width: 20,
                              height: 20,
                            }}
                            className="cursor"
                            onClick={() => {
                              const data = {
                                key: "arr_bonus_level",
                                idx: index,
                              };
                              dispatch(REMOVE_BONUS_LEVEL(data));
                            }}
                          >
                            <ISvg
                              name={ISvg.NAME.CROSS}
                              width={8}
                              height={8}
                              fill={colors.oranges}
                            />
                          </div>
                        </div>
                      )}
                    </Col>
                  </Row>
                </div>
              </Fragment>
            </Col>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  borderTop: "1px solid white",
                  borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                  borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                  borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                  padding: "15px 15px",
                  with: "100%",
                }}
              >
                <Fragment>
                  <Row gutter={[12, 0]} style={{ width: "100%" }}>
                    <Col span={24}>
                      <Fragment>
                        <Row style={{ marginBottom: 15 }}>
                          <Col span={isCheckCondition ? 8 : 14}>
                            <div>
                              <span style={{ fontWeight: 600 }}>
                                Tỷ lệ thưởng
                              </span>
                            </div>
                          </Col>

                          {isCheckCondition ? (
                            <Fragment>
                              <Col span={8}>
                                <div style={{ paddingLeft: 30 }}>
                                  <span style={{ fontWeight: 600 }}>
                                    Quy cách lớn
                                  </span>
                                </div>
                              </Col>

                              <Col span={8}>
                                <div style={{ paddingLeft: 30 }}>
                                  <span style={{ fontWeight: 600 }}>
                                    Quy cách nhỏ
                                  </span>
                                </div>
                              </Col>
                            </Fragment>
                          ) : (
                            <Col span={9} style={{ textAlign: "center" }}>
                              <span style={{ fontWeight: 600 }}>
                                Tỉ lệ chiết khấu
                              </span>
                            </Col>
                          )}
                        </Row>
                      </Fragment>
                    </Col>
                    <Col span={24}>
                      <div>
                        {item.arr_product.map((itemProduct, indexProduct) => {
                          return (
                            <Col span={24}>
                              <Fragment>
                                <Row
                                  type="flex"
                                  style={{
                                    borderBottom:
                                      "1px dashed rgba(122, 123, 123, 0.5)",
                                    paddingBottom: 10,
                                    paddingTop: 10,
                                  }}
                                >
                                  <Col span={isCheckCondition ? 8 : 14}>
                                    <div
                                      style={{
                                        height: "100%",
                                        display: "flex",
                                        alignItems: "center",
                                      }}
                                    >
                                      <span>{itemProduct.name}</span>
                                    </div>
                                  </Col>
                                  {isCheckCondition ? (
                                    <Fragment>
                                      <Col span={8}>
                                        <div
                                          style={{
                                            paddingLeft: 30,
                                            height: "100%",
                                            display: "flex",
                                            alignItems: "center",
                                          }}
                                        >
                                          <StyledInputNumber
                                            className="disabled-up"
                                            formatter={(value) =>
                                              `${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              )
                                            }
                                            min={0}
                                            value={itemProduct.maxUnitValue}
                                            onChange={(value) => {
                                              if (
                                                itemProduct.minUnitValue > 0
                                              ) {
                                                const data = {
                                                  key: "minUnitValue",
                                                  idxArrProduct: index,
                                                  idx: indexProduct,
                                                  value: 0,
                                                };

                                                dispatch(
                                                  UPDATE_REWARD_PERFORMANCE_PRODUCT(
                                                    data
                                                  )
                                                );

                                                return IError(
                                                  "Quy cách nhỏ sẽ trở về 0"
                                                );
                                              }

                                              const data = {
                                                key: "maxUnitValue",
                                                idxArrProduct: index,
                                                idx: indexProduct,
                                                value: value,
                                              };

                                              dispatch(
                                                UPDATE_REWARD_PERFORMANCE_PRODUCT(
                                                  data
                                                )
                                              );
                                            }}
                                            parser={(value) =>
                                              value.replace(/\$\s?|(,*)/g, "")
                                            }
                                            style={{
                                              marginRight: 10,
                                              borderRadius: 0,
                                              width: 110,
                                            }}
                                          />
                                          <span>
                                            {eventImmediateNew.gift_promotion ===
                                            1
                                              ? "% Doanh số"
                                              : "đ/ Quy cách"}
                                          </span>
                                        </div>
                                      </Col>
                                      <Col span={8}>
                                        <div
                                          style={{
                                            paddingLeft: 30,
                                            height: "100%",
                                            display: "flex",
                                            alignItems: "center",
                                          }}
                                        >
                                          <StyledInputNumber
                                            className="disabled-up"
                                            formatter={(value) =>
                                              `${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              )
                                            }
                                            min={0}
                                            value={itemProduct.minUnitValue}
                                            onChange={(value) => {
                                              if (
                                                itemProduct.maxUnitValue > 0
                                              ) {
                                                const data = {
                                                  key: "maxUnitValue",
                                                  idxArrProduct: index,
                                                  idx: indexProduct,
                                                  value: 0,
                                                };

                                                dispatch(
                                                  UPDATE_REWARD_PERFORMANCE_PRODUCT(
                                                    data
                                                  )
                                                );

                                                return IError(
                                                  "Quy cách lớn sẽ trở về 0"
                                                );
                                              }

                                              const data = {
                                                key: "minUnitValue",
                                                idxArrProduct: index,
                                                idx: indexProduct,
                                                value: value,
                                              };

                                              dispatch(
                                                UPDATE_REWARD_PERFORMANCE_PRODUCT(
                                                  data
                                                )
                                              );
                                            }}
                                            parser={(value) =>
                                              value.replace(/\$\s?|(,*)/g, "")
                                            }
                                            style={{
                                              marginRight: 10,
                                              borderRadius: 0,
                                              width: 110,
                                            }}
                                          />
                                          <span>
                                            {eventImmediateNew.gift_promotion ===
                                            1
                                              ? "% Doanh số"
                                              : "đ/ Quy cách"}
                                          </span>
                                        </div>
                                      </Col>
                                    </Fragment>
                                  ) : (
                                    <Col span={9}>
                                      <div
                                        style={{
                                          paddingLeft: 30,
                                          height: "100%",
                                          display: "flex",
                                          alignItems: "center",
                                          justifyContent: "center",
                                        }}
                                      >
                                        <StyledInputNumber
                                          className="disabled-up"
                                          formatter={(value) =>
                                            `${value}`.replace(
                                              /\B(?=(\d{3})+(?!\d))/g,
                                              ","
                                            )
                                          }
                                          min={0}
                                          value={itemProduct.quantity_sale}
                                          onChange={(value) => {
                                            const data = {
                                              key: "quantity_sale",
                                              idxArrProduct: index,
                                              idx: indexProduct,
                                              value: value,
                                            };

                                            dispatch(
                                              UPDATE_REWARD_PERFORMANCE_PRODUCT(
                                                data
                                              )
                                            );
                                          }}
                                          parser={(value) =>
                                            value.replace(/\$\s?|(,*)/g, "")
                                          }
                                          style={{
                                            marginRight: 10,
                                            borderRadius: 0,
                                            width: 110,
                                          }}
                                        />
                                        <span>
                                          {eventImmediateNew.gift_promotion ===
                                          1
                                            ? "% Doanh số"
                                            : "đ/ Quy cách"}
                                        </span>
                                      </div>
                                    </Col>
                                  )}

                                  {/* <Col span={1}></Col> */}
                                </Row>
                              </Fragment>
                            </Col>
                          );
                        })}
                      </div>
                    </Col>
                  </Row>
                </Fragment>
              </div>
            </Col>
          </Row>
        </Fragment>
      </Col>
    );
  };

  const renderItemGift = (item, index) => {
    return (
      <Col span={24}>
        <Fragment>
          <Row type="flex" gutter={[12, 0]}>
            <Col span={24}>
              <div
                style={{
                  border: "1px solid rgba(122, 123, 123, 0.5)",
                  padding: "15px 15px",
                }}
              >
                <Row gutter={[12, 12]} type="flex">
                  <Col span={7}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        height: "100%",
                      }}
                    >
                      <span style={{ fontWeight: 600 }}>Mức xét thưởng</span>
                    </div>
                  </Col>
                  <Col span={8}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        height: "100%",
                      }}
                    >
                      <span>Từ:</span>
                      <StyledInputNumber
                        className="disabled-up"
                        formatter={(value) =>
                          `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        }
                        min={
                          index === 0
                            ? 0
                            : eventImmediateNew.arr_gift_group[index - 1].to ===
                              0
                            ? eventImmediateNew.arr_gift_group[index - 1].from
                            : eventImmediateNew.arr_gift_group[index - 1].to
                        }
                        value={item.from}
                        onChange={(value) => {
                          const data = {
                            keyRoot: "arr_gift_group",
                            key: "from",
                            idx: index,
                            value: value,
                          };
                          dispatch(UPDATE_REWARD_PERFORMANCE(data));

                          // const data1 = {
                          //   keyRoot: "arr_gift_group",
                          //   key: "to",
                          //   idx: index,
                          //   value: value,
                          // };
                          // dispatch(UPDATE_REWARD_PERFORMANCE(data1));
                        }}
                        parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                        style={{
                          margin: "0px 12px",
                          borderRadius: 0,
                          width: 200,
                        }}
                      />
                    </div>
                  </Col>
                  <Col span={8}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        height: "100%",
                      }}
                    >
                      <span>Đến dưới:</span>
                      <StyledInputNumber
                        className="disabled-up"
                        formatter={(value) =>
                          `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        }
                        // min={item.from}
                        value={item.to}
                        onChange={(value) => {
                          const data = {
                            keyRoot: "arr_gift_group",
                            key: "to",
                            idx: index,
                            value: value,
                          };
                          dispatch(UPDATE_REWARD_PERFORMANCE(data));
                        }}
                        parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                        style={{
                          margin: "0px 12px",
                          borderRadius: 0,
                          // border: item.to ? "1px solid red" : "",
                          width: 200,
                        }}
                      />
                    </div>
                  </Col>
                  <Col span={1}>
                    {index === 0 ? null : (
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          height: "100%",
                          width: "100%",
                        }}
                      >
                        <div
                          style={{
                            borderRadius: 10,
                            background: "rgba(227, 95, 75, 0.1)",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            marginTop: 3,
                            width: 20,
                            height: 20,
                          }}
                          className="cursor"
                          onClick={() => {
                            const data = {
                              key: "arr_gift_group",
                              idx: index,
                            };
                            dispatch(REMOVE_GIFT_LEVEL(data));
                          }}
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill={colors.oranges}
                          />
                        </div>
                      </div>
                    )}
                  </Col>
                </Row>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                  borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                  padding: "15px 15px",
                }}
              >
                <Row style={{ width: "100%" }}>
                  {eventImmediateNew.arr_product_gift_group.length === 0 ||
                  eventImmediateNew.arr_product_gift_group[index] ===
                    undefined ? null : (
                    <>
                      <Col span={24}>
                        <Row style={{ marginBottom: 15 }}>
                          <Col span={8}>
                            <div>
                              <span style={{ fontWeight: 600 }}>
                                Tỷ lệ thưởng
                              </span>
                            </div>
                          </Col>
                          <Col span={8}>
                            <div style={{ textAlign: "center" }}>
                              <span style={{ fontWeight: 600 }}>
                                Quy cách lớn
                              </span>
                            </div>
                          </Col>
                          <Col span={8}>
                            <div style={{ textAlign: "center" }}>
                              <span style={{ fontWeight: 600 }}>
                                Quy cách nhỏ
                              </span>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <div
                          style={
                            {
                              // maxHeight: 150,
                              // overflow: "auto",
                            }
                          }
                        >
                          {eventImmediateNew.arr_product_gift_group[
                            index
                          ].arr_product.map((itemProduct, indexProduct) => {
                            return (
                              <Col span={24}>
                                <Row
                                  type="flex"
                                  style={{
                                    borderBottom:
                                      "1px dashed rgba(122, 123, 123, 0.5)",
                                    paddingBottom: 10,
                                    paddingTop: 10,
                                  }}
                                >
                                  <Col span={8}>
                                    <div
                                      style={{
                                        height: "100%",
                                        display: "flex",
                                        alignItems: "center",
                                      }}
                                    >
                                      <span>{itemProduct.name}</span>
                                    </div>
                                  </Col>
                                  <Col span={8}>
                                    <div
                                      style={{
                                        paddingLeft: 30,
                                        height: "100%",
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "center",
                                      }}
                                    >
                                      <StyledInputNumber
                                        className="disabled-up"
                                        formatter={(value) =>
                                          `${value}`.replace(
                                            /\B(?=(\d{3})+(?!\d))/g,
                                            ","
                                          )
                                        }
                                        min={0}
                                        value={itemProduct.quantity_gif_max}
                                        onChange={(value) => {
                                          if (
                                            itemProduct.quantity_gif_min > 0
                                          ) {
                                            const data = {
                                              key: "quantity_gif_min",
                                              idxArrProduct: index,
                                              idx: indexProduct,
                                              value: 0,
                                            };

                                            dispatch(
                                              UPDATE_GIFT_GROUP_PRODUCT(data)
                                            );

                                            return IError(
                                              "Quy cách nhỏ sẽ trở về 0"
                                            );
                                          }

                                          const data = {
                                            key: "quantity_gif_max",
                                            idxArrProduct: index,
                                            idx: indexProduct,
                                            value: value,
                                          };

                                          dispatch(
                                            UPDATE_GIFT_GROUP_PRODUCT(data)
                                          );
                                        }}
                                        parser={(value) =>
                                          value.replace(/\$\s?|(,*)/g, "")
                                        }
                                        style={{
                                          marginRight: 10,
                                          borderRadius: 0,
                                          width: 200,
                                        }}
                                      />
                                    </div>
                                  </Col>

                                  <Col span={8}>
                                    <div
                                      style={{
                                        paddingLeft: 30,
                                        height: "100%",
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "center",
                                      }}
                                    >
                                      <StyledInputNumber
                                        className="disabled-up"
                                        formatter={(value) =>
                                          `${value}`.replace(
                                            /\B(?=(\d{3})+(?!\d))/g,
                                            ","
                                          )
                                        }
                                        min={0}
                                        value={itemProduct.quantity_gif_min}
                                        onChange={(value) => {
                                          if (
                                            itemProduct.quantity_gif_max > 0
                                          ) {
                                            const data = {
                                              key: "quantity_gif_max",
                                              idxArrProduct: index,
                                              idx: indexProduct,
                                              value: 0,
                                            };

                                            dispatch(
                                              UPDATE_GIFT_GROUP_PRODUCT(data)
                                            );

                                            return IError(
                                              "Quy cách lớn sẽ trở về 0"
                                            );
                                          }

                                          const data = {
                                            key: "quantity_gif_min",
                                            idxArrProduct: index,
                                            idx: indexProduct,
                                            value: value,
                                          };

                                          dispatch(
                                            UPDATE_GIFT_GROUP_PRODUCT(data)
                                          );
                                        }}
                                        parser={(value) =>
                                          value.replace(/\$\s?|(,*)/g, "")
                                        }
                                        style={{
                                          marginRight: 10,
                                          borderRadius: 0,
                                          width: 200,
                                        }}
                                      />
                                    </div>
                                  </Col>
                                </Row>
                              </Col>
                            );
                          })}
                        </div>
                      </Col>
                    </>
                  )}
                </Row>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                  borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                  borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                  borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                  paddingBottom: 12,
                }}
              >
                <div
                  style={{
                    display: "flex",
                  }}
                  className="cursor"
                  onClick={() => {
                    if (
                      treeProductImmediateGiftGroup.listArrListTreeConfirm
                        .length > 0
                    ) {
                      if (
                        treeProductImmediateGiftGroup.listArrListTreeConfirm[
                          index
                        ] !== undefined
                      ) {
                        const data = {
                          key: "id_count_save_item",
                          value: index,
                        };
                        dispatch(UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE(data));

                        dispatch(
                          ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE(
                            treeProductImmediateGiftGroup
                              .listArrListTreeConfirm[index].treeProduct
                          )
                        );
                      } else {
                        setVisibleTreeGift(true);
                      }
                    }
                    setIndexArrProductGiftGroup(index);
                    setVisibleTreeGift(true);
                  }}
                >
                  <span
                    style={{
                      margin: "0px 12px",
                      color: colors.main,
                      fontWeight: 500,
                    }}
                  >
                    Thêm quà tặng
                  </span>
                  <div>
                    <ISvg
                      name={ISvg.NAME.ADDUPLOAD}
                      width={20}
                      height={20}
                      fill={colors.main}
                    />
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Fragment>
      </Col>
    );
  };

  const treeProct = (dataProduct, idxRoot) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children, idxRoot)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={24}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 30,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div style={{ paddingLeft: 30 }}>
                        <IInputTextArea
                          style={{
                            minHeight: 35,
                            height: 35,
                            padding: 6,
                            marginTop: 8,
                            border: "none",
                          }}
                          placeholder="Mô tả CTKM trên đơn của sản phẩm"
                          value={item.description}
                          onChange={(e) => {
                            const data = {
                              key: "description",
                              tree: treeProductIImmediate.listConfirm,
                              keyRoot: "treeProductIImmediate",
                              value: e.target.value,
                              idxRoot: idxRoot,
                              id: item.id,
                              // keyTypeProduct:
                              //   eventGift.event_design.key_type_product,
                            };
                            dispatch(ADD_CONTENT_PRODUCT_NEW(data));
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  // const _fetchGetProductGetCateByCompany = async () => {
  //   try {
  //     const data = await APIService._getProductGetCateByCompany();
  //     let dataNew = data.cate.map((item) => {
  //       const key = item.id;
  //       const value = item.company_name;
  //       return { key, value };
  //     });
  //     setDataDrowpCate(dataNew);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  const getListProductType = async () => {
    try {
      const data = await APIService._getListProductType();
      const newData = data.groups.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      setTypeProduct(newData);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getListProductType();
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              {`Tạo Chương trình khuyến mãi trên đơn > Thiết lập cơ cấu chương trình`}
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                    history.push("/event/immediate/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <Row gutter={[0, 20]}>
            <Col span={24}>
              <Row gutter={[24, 24]} type="flex">
                <Col span={8}>
                  <div className="boder-full box-shadow">
                    <Fragment>
                      <StyledITileHeading
                        minFont="10px"
                        maxFont="16px"
                        style={{ textAlign: "center" }}
                      >
                        Thiết lập điều kiện xét kèm
                      </StyledITileHeading>
                    </Fragment>
                    <div style={{ margin: "12px 0px" }}>
                      <Checkbox
                        checked={
                          eventImmediateNew.check_condition_attach === 0
                            ? false
                            : true
                        }
                        onChange={(e) => {
                          if (e.target.checked === true) {
                            const dataAction = {
                              key: "check_condition_attach",
                              value: 1,
                            };

                            dispatch(
                              CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction)
                            );
                          } else {
                            const dataAction = {
                              key: "check_condition_attach",
                              value: 0,
                            };

                            dispatch(
                              CREATE_DATE_EVENT_IMMEDIATE_NEW(dataAction)
                            );
                          }
                        }}
                      >
                        Thiết lập bộ điều kiện xét kèm theo
                      </Checkbox>
                    </div>
                  </div>
                </Col>

                <Col span={16} xl={16} xxl={14}>
                  <div
                    className="boder-full box-shadow"
                    style={{ background: backgroundColor }}
                  >
                    <Row gutter={[24, 24]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Điều kiện trả thưởng
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <div style={{ margin: "0px 0px 0px 24px" }}>
                          <Row gutter={[24, 0]}>
                            <Col span={8}>
                              <StyledITileHeading minFont="10px" maxFont="14px">
                                Điều kiện khuyến mãi
                              </StyledITileHeading>
                              <StyledISelect
                                style={{ marginTop: 10 }}
                                marginLeft="0px"
                                isBorderBottom="1px solid #d9d9d9"
                                value={eventImmediateNew.condition_promotion}
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                data={[
                                  {
                                    value: "Doanh số sản phẩm",
                                    key: 1,
                                  },
                                  {
                                    value: "Số lượng sản phẩm",
                                    key: 2,
                                  },
                                  {
                                    value: "Doanh số đơn hàng",
                                    key: 3,
                                  },
                                ]}
                                onChange={(key) => {
                                  const data = {
                                    key: "condition_promotion",
                                    value: key,
                                  };
                                  const data1 = {
                                    key: "gift_promotion",
                                    value: 1,
                                  };

                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );

                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data1)
                                  );
                                }}
                              />
                            </Col>
                            <Col span={8}>
                              <StyledITileHeading minFont="10px" maxFont="14px">
                                Phần thưởng khuyến mãi
                              </StyledITileHeading>
                              <StyledISelect
                                style={{ marginTop: 10 }}
                                marginLeft="0px"
                                isBorderBottom="1px solid #d9d9d9"
                                value={eventImmediateNew.gift_promotion}
                                disabled={
                                  eventImmediateNew.is_evet_parent === 2
                                }
                                data={
                                  eventImmediateNew.condition_promotion === 3
                                    ? [
                                        {
                                          value: "% Doanh số",
                                          key: 1,
                                        },
                                        {
                                          value: "Quà tặng",
                                          key: 3,
                                        },
                                      ]
                                    : [
                                        {
                                          value: "% Doanh số",
                                          key: 1,
                                        },
                                        {
                                          value: "đ/ Quy cách",
                                          key: 2,
                                        },
                                        {
                                          value: "Quà tặng",
                                          key: 3,
                                        },
                                      ]
                                }
                                onChange={async (key) => {
                                  const data = {
                                    key: "gift_promotion",
                                    value: key,
                                  };

                                  dispatch(
                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                  );
                                }}
                              />
                            </Col>
                            {eventImmediateNew.condition_promotion === 2 ? (
                              <Col span={8}>
                                <StyledITileHeading
                                  minFont="10px"
                                  maxFont="14px"
                                >
                                  Quy cách khuyến mãi
                                </StyledITileHeading>
                                <StyledISelect
                                  style={{ marginTop: 10 }}
                                  marginLeft="0px"
                                  isBorderBottom="1px solid #d9d9d9"
                                  value={eventImmediateNew.attribute_promotion}
                                  disabled={
                                    eventImmediateNew.is_evet_parent === 2
                                  }
                                  data={[
                                    {
                                      value: "Quy cách nhỏ",
                                      key: 1,
                                    },
                                    {
                                      value: "Quy cách lớn",
                                      key: 2,
                                    },
                                  ]}
                                  onChange={(key) => {
                                    const data = {
                                      key: "attribute_promotion",
                                      value: key,
                                    };
                                    dispatch(
                                      CREATE_DATE_EVENT_IMMEDIATE_NEW(data)
                                    );
                                  }}
                                />
                              </Col>
                            ) : null}
                          </Row>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>

                <Col span={8}>
                  <div className="boder-full box-shadow">
                    <Row gutter={[0, 25]} type="flex">
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Thiết lập danh mục phiên bản
                        </StyledITileHeading>
                      </Col>
                      <Col span={24}>
                        <div
                          className="boder-full"
                          style={{ background: backgroundColor }}
                        >
                          {/* <Fragment>
                            <StyledITileHeading minFont="10px" maxFont="16px">
                              Nghành hàng Cấp 1
                            </StyledITileHeading>
                          </Fragment> */}
                          <div style={{ margin: "12px 0px" }}>
                            <p style={{ fontWeight: 600 }}>Loại sản phẩm</p>
                            <StyledISelect
                              data={typeProduct}
                              value={eventImmediateNew.event_degsin.key_type}
                              disabled={eventImmediateNew.is_evet_parent === 2}
                              onChange={(value) => {
                                const data = {
                                  key: "key_type",
                                  keyRoot: "event_degsin",
                                  value: value,
                                };
                                dispatch(CREATE_KEY_DEGSIN_IMMEDIATE_NEW(data));
                              }}
                              marginLeft="0px"
                              placeholder="Vui lòng chọn Loại phiên bản áp dụng"
                              isBorderBottom="1px solid #d9d9d9"
                            />
                          </div>

                          {/* <div style={{ margin: "12px 0px" }}>
                            <p style={{ fontWeight: 600 }}>
                              Ngành hàng áp dụng
                            </p>
                            <StyledISelect
                              data={dataDrowpCate}
                              value={eventImmediateNew.event_degsin.key_cate}
                              disabled={eventImmediateNew.is_evet_parent === 2}
                              onChange={(value) => {
                                const data = {
                                  key: "key_cate",
                                  keyRoot: "event_degsin",
                                  value: value,
                                };
                                dispatch(CREATE_KEY_DEGSIN_IMMEDIATE_NEW(data));

                                // if (
                                //   eventImmediateNew.event_degsin.key_cate !== 0
                                // )
                                // setVisibleTree(true)
                              }}
                              marginLeft="0px"
                              placeholder="Vui lòng chọn Ngành hàng áp dụng"
                              isBorderBottom="1px solid #d9d9d9"
                            />
                          </div> */}
                        </div>
                      </Col>

                      {eventImmediateNew.condition_promotion === 3 ? null : (
                        <Col span={24}>
                          <div className="boder-full">
                            <Fragment>
                              <StyledITileHeading minFont="10px" maxFont="16px">
                                Danh mục phiên bản áp dụng
                              </StyledITileHeading>
                            </Fragment>

                            {treeProductIImmediate.listConfirm.length !==
                            0 ? null : (
                              <Col span={24}>
                                <div
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "flex-end",
                                  }}
                                >
                                  <div
                                    style={{
                                      display: "flex",
                                    }}
                                    className="cursor"
                                    onClick={() => {
                                      if (
                                        eventImmediateNew.event_degsin
                                          .key_cate === 0
                                      ) {
                                        const content = (
                                          <span>
                                            Vui lòng chọn loại phiên bản áp dụng
                                          </span>
                                        );
                                        return IError(content);
                                      }
                                      setVisibleTree(true);
                                    }}
                                  >
                                    <span
                                      style={{
                                        margin: "0px 12px",
                                        color: colors.main,
                                        fontWeight: 500,
                                      }}
                                    >
                                      Thêm phiên bản
                                    </span>
                                    <div>
                                      <ISvg
                                        name={ISvg.NAME.ADDUPLOAD}
                                        width={20}
                                        height={20}
                                        fill={colors.main}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </Col>
                            )}

                            {treeProductIImmediate.listConfirm.length ===
                            0 ? null : (
                              <>
                                <Col span={24}>
                                  <div
                                    style={{
                                      border: "1px solid #C4C4C4",
                                      padding: 5,
                                      height: 400,
                                      overflowY: "auto",
                                      overflowX: "hidden",
                                      marginTop: 12,
                                    }}
                                  >
                                    <Fragment>
                                      <Row>
                                        <Col span={24}>
                                          {treeProct(
                                            treeProductIImmediate.listConfirm
                                          )}
                                        </Col>
                                      </Row>
                                    </Fragment>
                                  </div>
                                </Col>
                                <Col span={24} style={{ paddingBottom: 10 }}>
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                      justifyContent: "center",
                                    }}
                                  >
                                    <div
                                      style={{
                                        display: "flex",
                                      }}
                                      className="cursor"
                                      onClick={() => setVisibleTree(true)}
                                    >
                                      <span
                                        style={{
                                          margin: "0px 12px",
                                          color: colors.main,
                                          fontWeight: 500,
                                        }}
                                      >
                                        Thêm phiên bản
                                      </span>
                                      <div>
                                        <ISvg
                                          name={ISvg.NAME.ADDUPLOAD}
                                          width={20}
                                          height={20}
                                          fill={colors.main}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </Col>
                              </>
                            )}
                          </div>
                        </Col>
                      )}
                    </Row>
                  </div>
                </Col>

                <Col span={16} xl={16} xxl={14}>
                  <div className="boder-full box-shadow">
                    <Row gutter={[24, 24]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Hạng mức thưởng
                        </StyledITileHeading>
                        <div style={{ marginTop: 10 }}>
                          <span>
                            Điều kiện:{" "}
                            {_renderTitleCondition(
                              eventImmediateNew.condition_promotion
                            )}
                          </span>
                        </div>
                      </Col>

                      {eventImmediateNew.gift_promotion !== 3 ? (
                        <Col span={24}>
                          <Row gutter={[24, 12]}>
                            <Col span={14}>
                              <div
                                style={{
                                  display: "flex",
                                  flexDirection: "row",
                                  alignItems: "center",
                                }}
                              >
                                <div
                                  style={{
                                    display: "flex",
                                  }}
                                  className="cursor"
                                  onClick={() => {
                                    if (
                                      treeProductIImmediate.listConfirm
                                        .length === 0 &&
                                      eventImmediateNew.condition_promotion !==
                                        3
                                    ) {
                                      const content = (
                                        <span>
                                          Vui lòng chọn sản phẩm áp dụng trước
                                        </span>
                                      );
                                      return IError(content);
                                    }

                                    const dataSell = {
                                      from: eventImmediateNew.arr_bonus_level[
                                        eventImmediateNew.arr_bonus_level
                                          .length - 1
                                      ].to,
                                      to:
                                        eventImmediateNew.arr_bonus_level[
                                          eventImmediateNew.arr_bonus_level
                                            .length - 1
                                        ].to !== 0
                                          ? eventImmediateNew.arr_bonus_level[
                                              eventImmediateNew.arr_bonus_level
                                                .length - 1
                                            ].to
                                          : eventImmediateNew.arr_bonus_level[
                                              eventImmediateNew.arr_bonus_level
                                                .length - 1
                                            ].to,
                                      quantity_sale:
                                        eventImmediateNew.arr_bonus_level[
                                          eventImmediateNew.arr_bonus_level
                                            .length - 1
                                        ].quantity_sale,
                                      arr_product:
                                        eventImmediateNew.arr_product_confirm,
                                    };

                                    dispatch(
                                      ADD_BONUS_LEVEL_EVENT_IMMEDIATE_NEW(
                                        dataSell
                                      )
                                    );
                                  }}
                                >
                                  <span
                                    style={{
                                      marginRight: 10,
                                      color: colors.main,
                                      fontWeight: 500,
                                    }}
                                  >
                                    Thêm mức thưởng
                                  </span>
                                  <div>
                                    <ISvg
                                      name={ISvg.NAME.ADDUPLOAD}
                                      width={20}
                                      height={20}
                                      fill={colors.main}
                                    />
                                  </div>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      ) : (
                        <Col span={24}>
                          <Row gutter={[24, 12]}>
                            <Col span={14}>
                              <div
                                style={{
                                  display: "flex",
                                  flexDirection: "row",
                                  alignItems: "center",
                                }}
                              >
                                <div
                                  style={{
                                    display: "flex",
                                  }}
                                  className="cursor"
                                  onClick={() => {
                                    if (
                                      eventImmediateNew.condition_promotion !==
                                      3
                                    ) {
                                      if (
                                        treeProductIImmediate.listConfirm
                                          .length === 0
                                      ) {
                                        const content = (
                                          <span>
                                            Vui lòng chọn sản phẩm áp dụng trước
                                          </span>
                                        );
                                        return IError(content);
                                      }
                                    }

                                    const dataAction = {
                                      key: "id_count_save_item",
                                      value:
                                        treeProductImmediateGiftGroup
                                          .listArrListTreeConfirm.length === 0
                                          ? 0
                                          : -1,
                                    };
                                    dispatch(
                                      UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE(
                                        dataAction
                                      )
                                    );

                                    const dataGift = {
                                      from: eventImmediateNew.arr_gift_group[
                                        eventImmediateNew.arr_gift_group
                                          .length - 1
                                      ].to,
                                      to:
                                        eventImmediateNew.arr_gift_group[
                                          eventImmediateNew.arr_gift_group
                                            .length - 1
                                        ].to !== 0
                                          ? eventImmediateNew.arr_gift_group[
                                              eventImmediateNew.arr_gift_group
                                                .length - 1
                                            ].to
                                          : eventImmediateNew.arr_gift_group[
                                              eventImmediateNew.arr_gift_group
                                                .length - 1
                                            ].to,
                                    };

                                    dispatch(
                                      ADD_GIFT_LEVEL_EVENT_IMMEDIATE_NEW(
                                        dataGift
                                      )
                                    );
                                  }}
                                >
                                  <span
                                    style={{
                                      marginRight: 10,
                                      color: colors.main,
                                      fontWeight: 500,
                                    }}
                                  >
                                    Thêm mức thưởng
                                  </span>
                                  <div>
                                    <ISvg
                                      name={ISvg.NAME.ADDUPLOAD}
                                      width={20}
                                      height={20}
                                      fill={colors.main}
                                    />
                                  </div>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      )}

                      {treeProductIImmediate.listConfirm.length === 0 ? (
                        eventImmediateNew.condition_promotion === 3 &&
                        eventImmediateNew.gift_promotion === 1 ? (
                          eventImmediateNew.arr_bonus_level.map(
                            (item, index) => {
                              return renderSalesOrder(item, index);
                            }
                          )
                        ) : eventImmediateNew.gift_promotion === 3 ? (
                          eventImmediateNew.arr_gift_group.map(
                            (item, index) => {
                              return renderItemGift(item, index);
                            }
                          )
                        ) : (
                          eventImmediateNew.arr_bonus_level.map(
                            (item, index) => {
                              return renderItemSalesOrCount(item, index);
                            }
                          )
                        )
                      ) : (
                        <div
                          style={{
                            width: "100%",
                            height: 500,
                          }}
                        >
                          {eventImmediateNew.gift_promotion === 3
                            ? eventImmediateNew.arr_gift_group.map(
                                (item, index) => {
                                  return renderItemGift(item, index);
                                }
                              )
                            : eventImmediateNew.condition_promotion === 3
                            ? eventImmediateNew.arr_bonus_level.map(
                                (item, index) => {
                                  return renderSalesOrder(item, index);
                                }
                              )
                            : eventImmediateNew.arr_bonus_level.map(
                                (item, index) => {
                                  return renderItemSalesOrCount(item, index);
                                }
                              )}
                        </div>
                      )}
                    </Row>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={() => history.goBack()}
                  title="Quay lại"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWTHINLEFT}
                  style={{ marginRight: 12 }}
                  styleHeight={{
                    width: 140,
                  }}
                />
                <IButton
                  title={
                    eventImmediateNew.check_condition_attach === 1 ||
                    eventImmediateNew.is_evet_parent === 2
                      ? "Tiếp tục"
                      : "Hoàn thành"
                  }
                  onClick={() => {
                    if (eventImmediateNew.condition_promotion !== 3) {
                      if (treeProductIImmediate.listConfirm.length === 0) {
                        const content = (
                          <span>
                            Vui lòng chọn{" "}
                            <span style={{ fontWeight: 600 }}>phiên bản.</span>
                          </span>
                        );
                        return IError(content);
                      }
                    }

                    let valid = false;

                    if (eventImmediateNew.gift_promotion !== 3) {
                      const even = (element) => element.to === 0;
                      const check_1_limit =
                        eventImmediateNew.arr_bonus_level.some((item) =>
                          even(item)
                        );
                      if (
                        check_1_limit &&
                        eventImmediateNew.arr_bonus_level.length > 1
                      ) {
                        const content = (
                          <span>
                            Điều kiện{" "}
                            <span style={{ fontWeight: 600 }}>CHỈ ĐỊNH</span>{" "}
                            không gồm nhiều hạng mức
                          </span>
                        );
                        return IError(content);
                      }
                      if (eventImmediateNew.arr_bonus_level.length > 1) {
                        let condition_limit = [];
                        eventImmediateNew.arr_bonus_level.forEach((item) => {
                          condition_limit = [
                            ...condition_limit,
                            ...[item.from, item.to],
                          ];
                        });
                        let check_conditon_limit = condition_limit.every(
                          function (x, i) {
                            return i === 0 || x >= condition_limit[i - 1];
                          }
                        );
                        if (!check_conditon_limit) {
                          const content = (
                            <span>
                              Thiết lập điều kiện sai quy tắc xét thưởng.
                            </span>
                          );
                          return IError(content);
                        }
                      }
                    } else {
                      const even = (element) => element.to === 0;
                      const check_1_limit =
                        eventImmediateNew.arr_gift_group.some((item) =>
                          even(item)
                        );
                      if (
                        check_1_limit &&
                        eventImmediateNew.arr_gift_group.length > 1
                      ) {
                        const content = (
                          <span>
                            Điều kiện{" "}
                            <span style={{ fontWeight: 600 }}>CHỈ ĐỊNH</span>{" "}
                            không gồm nhiều hạng mức
                          </span>
                        );
                        return IError(content);
                      }
                      if (eventImmediateNew.arr_gift_group.length > 1) {
                        let condition_limit = [];
                        eventImmediateNew.arr_gift_group.forEach((item) => {
                          condition_limit = [
                            ...condition_limit,
                            ...[item.from, item.to],
                          ];
                        });
                        let check_conditon_limit = condition_limit.every(
                          function (x, i) {
                            return i === 0 || x >= condition_limit[i - 1];
                          }
                        );
                        if (!check_conditon_limit) {
                          const content = (
                            <span>
                              Thiết lập điều kiện sai quy tắc xét thưởng.
                            </span>
                          );
                          return IError(content);
                        }
                      }
                    }

                    if (valid === true) {
                      const content = (
                        <span>
                          Vui lòng điền đầy đủ thông tin và chính xác giá trị
                          các{" "}
                          <span style={{ fontWeight: 600 }}>
                            hạng mức thưởng.
                          </span>
                        </span>
                      );
                      return IError(content);
                    }
                    if (eventImmediateNew.check_condition_attach === 1) {
                      history.push(
                        `/create/event/immediate/condition/attach/${id}`
                      );
                    } else {
                      if (eventImmediateNew.is_evet_parent === 2) {
                        return history.push(
                          `/create/event/immediate/children/${id}`
                        );
                      }
                      setLoadingSubmit(true);

                      const dataAdd = createEventImmediate({
                        eventImmediateNew,
                        treeProductIImmediate,
                        modalS1,
                        modalS2,
                        dataArrayCondition,
                        modalBusiness,
                        modalCity,
                        modalDistrict,
                        modalWard,
                      });
                      _fetchAPIPostCreateEvent(dataAdd);
                    }
                  }}
                  loading={loadingSubmit}
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        visible={isVisibleTree}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalTreeProductImmediate(
          () => {
            setVisibleTree(false);
          },
          eventImmediateNew.event_degsin.key_type,
          isVisibleTree
        )}
      </Modal>
      <Modal
        visible={isVisibleTreeGift}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalTreeProductImmediateNewGiftGroup(
          () => {
            setVisibleTreeGift(false);
          },
          0,
          true,
          eventImmediateNew.id_count_save_item,
          indexArrProductGiftGroup,
          eventImmediateNew.event_degsin.key_type,
          isVisibleTreeGift
        )}
      </Modal>
    </div>
  );
}
