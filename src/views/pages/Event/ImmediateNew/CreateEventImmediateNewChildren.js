import React, { Fragment, useEffect } from "react";
import {
  BackTop,
  Col,
  Row,
  Skeleton,
  Popconfirm,
  Radio,
  Icon,
  Empty,
  message,
  List,
  Checkbox,
} from "antd";
import { colors } from "../../../../assets";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";

import { IButton, ISvg, ITableHtml, ISelect } from "../../../components";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import FormatterDay from "../../../../utils/FormatterDay";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import { priceFormat } from "../../../../utils";
import {
  CLEAR_REDUX_IMMEDIATE_NEW,
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
} from "../../../store/reducers";
import { IError, StyledInputNumber } from "../../../components/common";
import { createEventImmediate } from "./ButtonCreateImmediateNew";
import { useState } from "react";
import { APIService } from "../../../../services";

export default function CreateEventImmediateNewChildren() {
  const dispatch = useDispatch();
  const history = useHistory();
  const dataRoot = useSelector((state) => state);
  const params = useParams();
  const { id } = params;
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [listEventChild, setListEventChild] = useState([]);

  const {
    eventImmediateNew = {
      dataDetail: {},
    },
    treeProductIImmediate,
    modalS1,
    modalS2,
    dataArrayCondition,
    modalBusiness,
    modalCity,
    modalDistrict,
    modalWard,
    treeProductPresentIImmediateConditionAttach,
    treeProductImmediateConditionAttachGroup,
  } = dataRoot;

  const _fetchAPIPostCreateEvent = async (obj) => {
    try {
      const data = await APIService._postCreateEventNowNew(obj);
      message.success("Tạo chương trình thành công");
      setLoadingSubmit(false);
      history.push("/event/immediate/list");
      dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
    } catch (error) {
      setLoadingSubmit(false);
      console.log(error);
    }
  };

  const _fetchEventChild = async (id) => {
    try {
      const data = await APIService._listEventChildParentID(id);

      setListEventChild([...data.listEvent]);
    } catch (error) {
    } finally {
    }
  };

  useEffect(() => {
    _fetchEventChild(id);
  }, []);

  const _renderCondition = (type, style, data, idx) => {
    if (type === 1) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo`}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>Doanh số nguyên đơn</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Doanh số nguyên đơn</p>
                  <span>{priceFormat(data.condition) + "đ"}</span>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
    if (type === 2) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo `}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>
                    {style === 1 ? "Doanh số sản phẩm" : "Số lượng sản phẩm"}
                  </span>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện sản phẩm</p>
                  <span>Danh sách sản phẩm</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <div
                    style={{
                      padding: "12px 12px 36px 12px",
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Row gutter={[24, 24]}>
                      <Col span={24}>
                        <React.Fragment>
                          <Row gutter={[12, 12]}>
                            <Col span={14}>
                              <span style={{ fontWeight: 600 }}>Sản phẩm</span>
                            </Col>
                            <Col span={10} style={{ textAlign: "right" }}>
                              <span style={{ fontWeight: 600, marginRight: 6 }}>
                                {style === 1 ? "Doanh số" : "Số lượng sản phẩm"}
                              </span>
                            </Col>
                          </Row>
                        </React.Fragment>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            height: 350,
                            overflow: "auto",
                            padding: "0px 12px",
                          }}
                        >
                          {data.lsProduct.map((item) => {
                            return (
                              <div
                                style={{ borderBottom: "0.5px dashed #7A7B7B" }}
                              >
                                <React.Fragment>
                                  <Row gutter={[12, 12]}>
                                    <Col span={14}>
                                      <span>{item.product_name}</span>
                                    </Col>
                                    <Col
                                      span={10}
                                      style={{ textAlign: "right" }}
                                    >
                                      <span>
                                        {style === 1
                                          ? priceFormat(item.condition) + " đ"
                                          : priceFormat(item.condition)}
                                      </span>
                                    </Col>
                                  </Row>
                                </React.Fragment>
                              </div>
                            );
                          })}
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
    if (type === 3) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo}`}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>
                    {style === 1 ? "Doanh số sản phẩm" : "Số lượng sản phẩm"}{" "}
                  </span>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện sản phẩm</p>
                  <span>Nhóm sản phẩm</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <div
                    style={{
                      padding: "12px 12px 36px 12px",
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Row gutter={[24, 24]}>
                      <Col span={24}>
                        <React.Fragment>
                          <Row gutter={[12, 12]}>
                            <Col span={14}>
                              <span style={{ fontWeight: 600 }}>Nhóm hàng</span>
                            </Col>
                            <Col span={10} style={{ textAlign: "right" }}>
                              <span style={{ fontWeight: 600, marginRight: 4 }}>
                                {style === 1 ? "Doanh số" : "Số lượng"}
                              </span>
                            </Col>
                          </Row>
                        </React.Fragment>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            height: 350,
                            overflow: "auto",
                            padding: "0px 12px",
                          }}
                        >
                          {data.lsGroup.map((item, index) => {
                            return (
                              <div style={{ marginBottom: 24 }}>
                                <p style={{ fontWeight: 600 }}>{`Nhóm hàng ${
                                  index + 1
                                }`}</p>
                                <>
                                  {item.lst_product.map((item1, index1) => {
                                    return (
                                      <div
                                        style={{
                                          borderBottom: "0.5px dashed #7A7B7B",
                                        }}
                                      >
                                        <React.Fragment>
                                          <Row gutter={[12, 12]}>
                                            <Col span={14}>
                                              <span>{item1.product_name}</span>
                                            </Col>
                                            <Col
                                              span={10}
                                              style={{ textAlign: "right" }}
                                            >
                                              <span>
                                                {style === 1
                                                  ? priceFormat(
                                                      item1.condition
                                                    ) + " đ"
                                                  : priceFormat(
                                                      item1.condition
                                                    )}
                                              </span>
                                            </Col>
                                          </Row>
                                        </React.Fragment>
                                      </div>
                                    );
                                  })}
                                </>
                              </div>
                            );
                          })}
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
  };

  const _renderConditionPreview = (type, style, data, idxxx) => {
    if (type === 1) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo ${idxxx + 1}`}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>Doanh số nguyên đơn</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Doanh số nguyên đơn</p>
                  <span>{priceFormat(data.sum_sales_order) + "đ"}</span>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
    if (type === 2) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo ${idxxx + 1}`}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>
                    {style === 1 ? "Doanh số sản phẩm" : "Số lượng sản phẩm"}{" "}
                  </span>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện sản phẩm</p>
                  <span>Danh sách sản phẩm</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <div
                    style={{
                      padding: "12px 12px 36px 12px",
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Row gutter={[24, 24]}>
                      <Col span={24}>
                        <React.Fragment>
                          <Row gutter={[12, 12]}>
                            <Col span={14}>
                              <span style={{ fontWeight: 600 }}>Sản phẩm</span>
                            </Col>
                            <Col span={10} style={{ textAlign: "right" }}>
                              <span style={{ fontWeight: 600, marginRight: 6 }}>
                                {style === 1 ? "Doanh số" : "Số lượng sản phẩm"}
                              </span>
                            </Col>
                          </Row>
                        </React.Fragment>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            height: 350,
                            overflow: "auto",
                            padding: "0px 12px",
                          }}
                        >
                          {eventImmediateNew.arr_product_condition_attach.map(
                            (item) => (
                              <div
                                style={{
                                  borderBottom: "0.5px dashed #7A7B7B",
                                }}
                              >
                                <React.Fragment>
                                  <Row gutter={[12, 12]}>
                                    <Col span={14}>
                                      <span>{item.name}</span>
                                    </Col>
                                    <Col
                                      span={10}
                                      style={{ textAlign: "right" }}
                                    >
                                      <span>
                                        {style === 1
                                          ? priceFormat(item.salesOrQuantity) +
                                            " đ"
                                          : priceFormat(item.salesOrQuantity)}
                                      </span>
                                    </Col>
                                  </Row>
                                </React.Fragment>
                              </div>
                            )
                          )}
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
    if (type === 3) {
      return (
        <>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    {`Danh mục điều kiện kèm theo ${idxxx + 1}`}
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện kèm theo</p>
                  <span>
                    {style === 1 ? "Doanh số sản phẩm" : "Số lượng sản phẩm"}{" "}
                  </span>
                </Col>
                <Col span={24}>
                  <p style={{ fontWeight: 600 }}>Điều kiện sản phẩm</p>
                  <span>Nhóm sản phẩm</span>
                </Col>
              </Row>
            </>
          </Col>
          <Col span={24}>
            <>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <StyledITileHeading minFont="10px" maxFont="16px">
                    Thiết lập điều kiện xét tích lũy kèm theo
                  </StyledITileHeading>
                </Col>
                <Col span={24}>
                  <div
                    style={{
                      padding: "12px 12px 36px 12px",
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Row gutter={[24, 24]}>
                      <Col span={24}>
                        <React.Fragment>
                          <Row gutter={[12, 12]}>
                            <Col span={14}>
                              <span style={{ fontWeight: 600 }}>Nhóm hàng</span>
                            </Col>
                            <Col span={10} style={{ textAlign: "right" }}>
                              <span style={{ fontWeight: 600, marginRight: 4 }}>
                                {style === 1 ? "Doanh số" : "Số lượng"}
                              </span>
                            </Col>
                          </Row>
                        </React.Fragment>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            height: 350,
                            overflow: "auto",
                            padding: "0px 12px",
                          }}
                        >
                          {eventImmediateNew.arr_condition_attach_group.map(
                            (item, index) => {
                              return (
                                <React.Fragment>
                                  <Row gutter={[12, 12]}>
                                    <Col span={8}>
                                      <span
                                        style={{ fontWeight: 600 }}
                                      >{`Nhóm hàng ${index + 1}`}</span>
                                    </Col>
                                    <Col span={16}>
                                      <div
                                        style={{
                                          display: "flex",
                                          flexDirection: "row",
                                          alignItems: "center",
                                          justifyContent: "space-between",
                                        }}
                                      >
                                        <span>Từ :</span>
                                        <span style={{ marginLeft: 24 }}>
                                          {style === 1
                                            ? priceFormat(
                                                item.salesOrQuantityGroup
                                              ) + " đ"
                                            : priceFormat(
                                                item.salesOrQuantityGroup
                                              )}
                                        </span>
                                      </div>
                                    </Col>
                                  </Row>
                                </React.Fragment>
                              );
                            }
                          )}
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </>
          </Col>
        </>
      );
    }
  };

  const _renderKeyCondition = (type, type_style) => {
    if (type === 0) return 1;
    if (type === 1 && type_style === DefineKeyEvent.tung_san_pham) return 2;
    if (type === 1 && type_style === DefineKeyEvent.nhom_san_pham) return 3;
    if (type === 2 && type_style === DefineKeyEvent.tung_san_pham) return 2;
    if (type === 2 && type_style === DefineKeyEvent.nhom_san_pham) return 3;
  };

  const dataDetail = eventImmediateNew.dataDetail.event_response;
  const isCheckCondition =
    (dataDetail.application_type_id === 1 && dataDetail.reward_type_id !== 1) ||
    (dataDetail.application_type_id === 2 && dataDetail.reward_type_id !== 1) ||
    (dataDetail.application_type_id === 3 && dataDetail.reward_type_id !== 1);

  const _renderDVLevel = (key = dataDetail.reward_type_id) => {
    if (key === 2) {
      return "đ/Quy cách";
    }
    if (key === 1) {
      return "% Doanh số";
    }
    if (key === 3) {
      return "";
    }
  };

  const renderItemSalesOrCount = (item, data) => {
    const even = (element) =>
      element[
        dataDetail.application_type_id === 2 ? "quantity_max" : "money_max"
      ] === 0;
    const check_1_limit = data.some((item11) => even(item11));
    const isCheck_1_limit = check_1_limit && data.length === 1;
    return (
      <Col span={24}>
        <Row>
          <Col span={24}>
            <div
              style={{
                borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                borderBottom: "1px dashed rgba(122, 123, 123, 0.5)",
                borderTop: "1px solid rgba(122, 123, 123, 0.5)",
                padding: "8px 15px",
              }}
            >
              <Row>
                <Col span={8}>
                  <div
                    style={{
                      marginTop: 4,
                    }}
                  >
                    <span style={{ fontWeight: 600 }}>Mức xét thưởng</span>
                  </div>
                </Col>
                <Col
                  span={isCheck_1_limit ? 10 : 8}
                  offset={isCheck_1_limit ? 6 : 0}
                >
                  <div
                    style={{
                      marginTop: 4,
                    }}
                  >
                    <span>
                      Từ:{" "}
                      {priceFormat(
                        dataDetail.application_type_id === 1 ||
                          dataDetail.application_type_id === 3
                          ? item.money_min
                          : item.quantity_min
                      ) +
                        `${
                          dataDetail.application_type_id === 1 ||
                          dataDetail.application_type_id === 3
                            ? " đ"
                            : ""
                        }`}
                    </span>
                  </div>
                </Col>
                {isCheck_1_limit ? null : (
                  <Col span={8}>
                    <div
                      style={{
                        marginTop: 4,
                      }}
                    >
                      <span>
                        Đến dưới:{" "}
                        {priceFormat(
                          dataDetail.application_type_id === 1 ||
                            dataDetail.application_type_id === 3
                            ? item.money_max
                            : item.quantity_max
                        ) +
                          `${
                            dataDetail.application_type_id === 1 ||
                            dataDetail.application_type_id === 3
                              ? " đ"
                              : ""
                          }`}
                      </span>
                    </div>
                  </Col>
                )}
              </Row>
            </div>
          </Col>
          <Col span={24}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                borderTop: "1px solid white",
                borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                padding: "15px 15px",
              }}
            >
              {dataDetail.application_type_id === 3 ? (
                <Row style={{ width: "100%" }} gutter={[12, 0]}>
                  <Col span={24}>
                    <Row style={{ marginBottom: 15 }}>
                      <Col span={dataDetail.reward_type_id !== 2 ? 10 : 14}>
                        <div>
                          <span style={{ fontWeight: 600 }}>
                            {dataDetail.reward_type_id === 1
                              ? "Tỷ lệ chiết khấu"
                              : "Tỷ lệ thưởng"}
                          </span>
                        </div>
                      </Col>
                      {dataDetail.reward_type_id !== 1 ? (
                        <Fragment>
                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách lớn
                            </span>
                          </Col>

                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách nhỏ
                            </span>
                          </Col>
                        </Fragment>
                      ) : (
                        <Col span={10}>
                          <span style={{ fontWeight: 600 }}>
                            {`${(item.vat * 100).toFixed(2)} % Doanh số`}
                          </span>
                        </Col>
                      )}
                    </Row>
                  </Col>

                  <Col span={24}>
                    <div>
                      {item.lst_product.map((itemProduct, indexProduct) => {
                        return (
                          <Col span={24}>
                            <Row
                              type="flex"
                              style={{
                                borderBottom:
                                  "1px dashed rgba(122, 123, 123, 0.5)",
                                paddingBottom: 10,
                                paddingTop: 10,
                              }}
                            >
                              <Col span={isCheckCondition ? 10 : 14}>
                                <div
                                  style={{
                                    height: "100%",
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <span>{itemProduct.product_name}</span>
                                </div>
                              </Col>
                              {isCheckCondition ? (
                                <Fragment>
                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        dataDetail.reward_type_id === 3
                                          ? !itemProduct.quantity_max
                                            ? 0
                                            : itemProduct.quantity_max
                                          : !itemProduct.money_max
                                          ? 0
                                          : itemProduct.money_max
                                      ) + ` ${_renderDVLevel()}`}
                                    </span>
                                  </Col>

                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        dataDetail.reward_type_id === 3
                                          ? !itemProduct.quantity_min
                                            ? 0
                                            : itemProduct.quantity_min
                                          : !itemProduct.money_min
                                          ? 0
                                          : itemProduct.money_min
                                      ) + ` ${_renderDVLevel()}`}
                                    </span>
                                  </Col>
                                </Fragment>
                              ) : (
                                <Col span={10} style={{ textAlign: "center" }}>
                                  <span>
                                    {(itemProduct.discount * 100).toFixed(2)}
                                    {_renderDVLevel()}
                                  </span>
                                </Col>
                              )}
                            </Row>
                          </Col>
                        );
                      })}
                    </div>
                  </Col>
                </Row>
              ) : (
                <Row style={{ width: "100%" }} gutter={[12, 0]}>
                  <Col span={24}>
                    <Row style={{ marginBottom: 15 }}>
                      <Col span={isCheckCondition ? 10 : 14}>
                        <div>
                          <span style={{ fontWeight: 600 }}>Tỷ lệ thưởng</span>
                        </div>
                      </Col>
                      {isCheckCondition ? (
                        <Fragment>
                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách lớn
                            </span>
                          </Col>

                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách nhỏ
                            </span>
                          </Col>
                        </Fragment>
                      ) : (
                        <Col span={10} style={{ textAlign: "center" }}>
                          <span style={{ fontWeight: 600 }}>
                            Tỉ lệ chiết khấu
                          </span>
                        </Col>
                      )}
                    </Row>
                  </Col>

                  <Col span={24}>
                    <div>
                      {item.lst_product.map((itemProduct, indexProduct) => {
                        return (
                          <Col span={24}>
                            <Row
                              type="flex"
                              style={{
                                borderBottom:
                                  "1px dashed rgba(122, 123, 123, 0.5)",
                                paddingBottom: 10,
                                paddingTop: 10,
                              }}
                            >
                              <Col span={isCheckCondition ? 10 : 14}>
                                <div
                                  style={{
                                    height: "100%",
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <span>{itemProduct.product_name}</span>
                                </div>
                              </Col>
                              {isCheckCondition ? (
                                <Fragment>
                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        dataDetail.reward_type_id === 3
                                          ? !itemProduct.quantity_max
                                            ? 0
                                            : itemProduct.quantity_max
                                          : !itemProduct.money_max
                                          ? 0
                                          : itemProduct.money_max
                                      ) + ` ${_renderDVLevel()}`}
                                    </span>
                                  </Col>

                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        dataDetail.reward_type_id === 3
                                          ? !itemProduct.quantity_min
                                            ? 0
                                            : itemProduct.quantity_min
                                          : !itemProduct.money_min
                                          ? 0
                                          : itemProduct.money_min
                                      ) + ` ${_renderDVLevel()}`}
                                    </span>
                                  </Col>
                                </Fragment>
                              ) : (
                                <Col span={10} style={{ textAlign: "center" }}>
                                  <span>
                                    {(itemProduct.discount * 100).toFixed(2)}
                                    {_renderDVLevel()}
                                  </span>
                                </Col>
                              )}
                            </Row>
                          </Col>
                        );
                      })}
                    </div>
                  </Col>
                </Row>
              )}
            </div>
          </Col>
        </Row>
      </Col>
    );
  };

  const renderItemSalesOrCountPreview = (item, data, idx) => {
    const even = (element) => element.to === 0;
    const check_1_limit = data.some((item11) => even(item11));
    const isCheck_1_limit = check_1_limit && data.length === 1;

    return (
      <Col span={24}>
        <Row>
          <Col span={24}>
            <div
              style={{
                borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                borderBottom: "1px dashed rgba(122, 123, 123, 0.5)",
                borderTop: "1px solid rgba(122, 123, 123, 0.5)",
                padding: "8px 15px",
              }}
            >
              <Row>
                <Col span={8}>
                  <div
                    style={{
                      marginTop: 4,
                    }}
                  >
                    <span style={{ fontWeight: 600 }}>Mức xét thưởng</span>
                  </div>
                </Col>
                <Col
                  span={isCheck_1_limit ? 10 : 8}
                  offset={isCheck_1_limit ? 6 : 0}
                >
                  <div
                    style={{
                      marginTop: 4,
                    }}
                  >
                    <span>
                      Từ:{" "}
                      {priceFormat(item.from) +
                        `${
                          eventImmediateNew.condition_promotion === 1 ||
                          eventImmediateNew.condition_promotion === 3
                            ? " đ"
                            : ""
                        }`}
                    </span>
                  </div>
                </Col>
                {isCheck_1_limit ? null : (
                  <Col span={8}>
                    <div
                      style={{
                        marginTop: 4,
                      }}
                    >
                      <span>
                        Đến dưới:{" "}
                        {priceFormat(item.to) +
                          `${
                            eventImmediateNew.condition_promotion === 1 ||
                            eventImmediateNew.condition_promotion === 3
                              ? " đ"
                              : ""
                          }`}
                      </span>
                    </div>
                  </Col>
                )}
              </Row>
            </div>
          </Col>

          <Col span={24}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                borderTop: "1px solid white",
                borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                padding: "15px 15px",
              }}
            >
              {eventImmediateNew.condition_promotion === 3 ? (
                <Row style={{ width: "100%" }} gutter={[12, 0]}>
                  <Col span={24}>
                    <Row style={{ marginBottom: 15 }}>
                      <Col
                        span={eventImmediateNew.gift_promotion !== 1 ? 10 : 14}
                      >
                        <div>
                          <span style={{ fontWeight: 600 }}>
                            {eventImmediateNew.gift_promotion === 1
                              ? "Tỷ lệ chiết khấu"
                              : "Tỷ lệ thưởng"}
                          </span>
                        </div>
                      </Col>
                      {eventImmediateNew.gift_promotion !== 1 ? (
                        <Fragment>
                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách lớn
                            </span>
                          </Col>

                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách nhỏ
                            </span>
                          </Col>
                        </Fragment>
                      ) : (
                        <Col span={10}>
                          <span style={{ fontWeight: 600 }}>
                            {`${item.quantity_sale.toFixed(2)} % Doanh số`}
                          </span>
                        </Col>
                      )}
                    </Row>
                  </Col>

                  <Col span={24}>
                    <div>
                      {eventImmediateNew.arr_product_gift_group.length === 0
                        ? null
                        : eventImmediateNew.arr_product_gift_group[
                            idx
                          ].arr_product.map((itemProduct, indexProduct) => {
                            return (
                              <Col span={24}>
                                <Row
                                  type="flex"
                                  style={{
                                    borderBottom:
                                      "1px dashed rgba(122, 123, 123, 0.5)",
                                    paddingBottom: 10,
                                    paddingTop: 10,
                                  }}
                                >
                                  <Col span={isCheckCondition ? 10 : 14}>
                                    <div
                                      style={{
                                        height: "100%",
                                        display: "flex",
                                        alignItems: "center",
                                      }}
                                    >
                                      <span>{itemProduct.name}</span>
                                    </div>
                                  </Col>
                                  {isCheckCondition ? (
                                    <Fragment>
                                      <Col
                                        span={7}
                                        style={{ textAlign: "right" }}
                                      >
                                        <span>
                                          {priceFormat(
                                            itemProduct.quantity_gif_max
                                          ) +
                                            ` ${_renderDVLevel(
                                              eventImmediateNew.gift_promotion
                                            )}`}
                                        </span>
                                      </Col>

                                      <Col
                                        span={7}
                                        style={{ textAlign: "right" }}
                                      >
                                        <span>
                                          {priceFormat(
                                            itemProduct.quantity_gif_min
                                          ) +
                                            ` ${_renderDVLevel(
                                              eventImmediateNew.gift_promotion
                                            )}`}
                                        </span>
                                      </Col>
                                    </Fragment>
                                  ) : (
                                    <Col
                                      span={10}
                                      style={{ textAlign: "center" }}
                                    >
                                      <span>
                                        {(
                                          itemProduct.quantity_sale * 100
                                        ).toFixed(2)}
                                        {_renderDVLevel()}
                                      </span>
                                    </Col>
                                  )}
                                </Row>
                              </Col>
                            );
                          })}
                    </div>
                  </Col>
                </Row>
              ) : (
                <Row style={{ width: "100%" }} gutter={[12, 0]}>
                  <Col span={24}>
                    <Row style={{ marginBottom: 15 }}>
                      <Col span={isCheckCondition ? 10 : 14}>
                        <div>
                          <span style={{ fontWeight: 600 }}>Tỷ lệ thưởng</span>
                        </div>
                      </Col>
                      {isCheckCondition ? (
                        <Fragment>
                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách lớn
                            </span>
                          </Col>

                          <Col span={7} style={{ textAlign: "right" }}>
                            <span style={{ fontWeight: 600 }}>
                              Quy cách nhỏ
                            </span>
                          </Col>
                        </Fragment>
                      ) : (
                        <Col span={10} style={{ textAlign: "center" }}>
                          <span style={{ fontWeight: 600 }}>
                            Tỉ lệ chiết khấu
                          </span>
                        </Col>
                      )}
                    </Row>
                  </Col>

                  <Col span={24}>
                    <div>
                      {eventImmediateNew[
                        eventImmediateNew.gift_promotion === 3
                          ? "arr_product_gift_group"
                          : "arr_bonus_level"
                      ][idx].arr_product.map((itemProduct, indexProduct) => {
                        return (
                          <Col span={24}>
                            <Row
                              type="flex"
                              style={{
                                borderBottom:
                                  "1px dashed rgba(122, 123, 123, 0.5)",
                                paddingBottom: 10,
                                paddingTop: 10,
                              }}
                            >
                              <Col span={isCheckCondition ? 10 : 14}>
                                <div
                                  style={{
                                    height: "100%",
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <span>{itemProduct.name}</span>
                                </div>
                              </Col>
                              {isCheckCondition ? (
                                <Fragment>
                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        itemProduct[
                                          eventImmediateNew.gift_promotion === 3
                                            ? "quantity_gif_max"
                                            : "maxUnitValue"
                                        ]
                                      ) +
                                        ` ${_renderDVLevel(
                                          eventImmediateNew.gift_promotion
                                        )}`}
                                    </span>
                                  </Col>

                                  <Col span={7} style={{ textAlign: "right" }}>
                                    <span>
                                      {priceFormat(
                                        itemProduct[
                                          eventImmediateNew.gift_promotion === 3
                                            ? "quantity_gif_min"
                                            : "minUnitValue"
                                        ]
                                      ) +
                                        ` ${_renderDVLevel(
                                          eventImmediateNew.gift_promotion
                                        )}`}
                                    </span>
                                  </Col>
                                </Fragment>
                              ) : (
                                <Col span={10} style={{ textAlign: "center" }}>
                                  <span>
                                    {itemProduct.quantity_sale.toFixed(2)}
                                    {_renderDVLevel(
                                      eventImmediateNew.gift_promotion
                                    )}
                                  </span>
                                </Col>
                              )}
                            </Row>
                          </Col>
                        );
                      })}
                    </div>
                  </Col>
                </Row>
              )}
            </div>
          </Col>
        </Row>
      </Col>
    );
  };

  const treeProduct = (dataProduct, is_check) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: 0,
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding: "6px 0px",
                  }}
                >
                  {item.name}
                </li>
                {treeProduct(item.children)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                }}
              >
                <li style={{ padding: "6px 6px" }}>
                  <Row gutter={[12, 0]} type="flex">
                    {/* {dataDetail.application_type_id === 2 ? (
                      <Col span={4}>
                        <div
                          style={{
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              width: 16,
                              height: 16,
                              borderRadius: 8,
                              border: !is_check
                                ? item.is_key_default === 1
                                  ? "4px solid #13A751"
                                  : "1px solid rgba(34, 35, 43, 0.5)"
                                : Number(
                                    treeProductIImmediate.keyDefaultProduct
                                  ) === item.id
                                ? "4px solid #13A751"
                                : "1px solid rgba(34, 35, 43, 0.5)",
                            }}
                          />
                        </div>
                      </Col>
                    ) : null} */}
                    <Col span={dataDetail.application_type_id === 2 ? 24 : 24}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    {/* {dataDetail.application_type_id === 2 ? (
                      <Col span={6}>
                        <div
                          style={{
                            wordWrap: "break-word",
                            wordBreak: "break-word",
                            overflow: "hidden",
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            textAlign: "center",
                          }}
                        >
                          <span>{item.ti_le_quy_doi}</span>
                        </div>
                      </Col>
                    ) : null} */}
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  const treeProduct1 = (dataProduct) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: 0,
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding: "6px 0px",
                  }}
                >
                  {item.name}
                </li>
                {treeProduct1(item.children)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                }}
              >
                <li style={{ padding: "6px 6px" }}>
                  <Row gutter={[12, 0]} type="flex">
                    {/* {dataDetail.application_type_id === 2 ? (
                      <Col span={4}>
                        <div
                          style={{
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              width: 16,
                              height: 16,
                              borderRadius: 8,
                              border:
                                treeProductIImmediate.keyDefaultProduct ===
                                item.id
                                  ? "4px solid #13A751"
                                  : "1px solid rgba(34, 35, 43, 0.5)",
                            }}
                          />
                        </div>
                      </Col>
                    ) : null} */}
                    <Col span={dataDetail.application_type_id === 2 ? 24 : 24}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    {/* {dataDetail.application_type_id === 2 ? (
                      <Col span={6}>
                        <div
                          style={{
                            wordWrap: "break-word",
                            wordBreak: "break-word",
                            overflow: "hidden",
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            textAlign: "center",
                          }}
                        >
                          <span>{item.convert_ratio}</span>
                        </div>
                      </Col>
                    ) : null} */}
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <BackTop />
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              {`Tạo Chương trình khuyến mãi trên đơn > Thiết lập cơ cấu chương trình > Thiết lập độ ưu tiên`}
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-end",
            }}
          >
            <IButton
              title="Hủy"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                dispatch(CLEAR_REDUX_IMMEDIATE_NEW());
                history.push("/event/immediate/list");
              }}
            />
          </div>
        </Col>

        <Col span={24}>
          <div>
            <Fragment>
              <Row gutter={[12, 12]} type="flex">
                <Col span={24}>
                  <div className="boder-full box-shadow">
                    <Row gutter={[12, 16]}>
                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          {`Thể lệ & Chi tiết thiết lập`}
                        </StyledITileHeading>
                      </Col>

                      <Col span={24}>
                        <StyledITileHeading minFont="10px" maxFont="15px">
                          Ngành hàng áp dụng
                        </StyledITileHeading>
                        <span>
                          {!dataDetail.company_name
                            ? "-"
                            : dataDetail.company_name}
                        </span>
                      </Col>
                      <Col span={24}>
                        <div
                          className="boder-full"
                          style={{ background: "#f0f0f0" }}
                        >
                          <Fragment>
                            <Row gutter={[16, 16]}>
                              <Col span={24}>
                                <StyledITileHeading
                                  minFont="10px"
                                  maxFont="16px"
                                >
                                  Điều kiện trả thưởng
                                </StyledITileHeading>
                              </Col>
                              <Col span={12}>
                                <Row>
                                  <Col span={6}>
                                    <StyledITileHeading
                                      minFont="10px"
                                      maxFont="14px"
                                    >
                                      Điều kiện khuyến mãi
                                    </StyledITileHeading>
                                    <span>
                                      {!dataDetail.application_type_name
                                        ? "-"
                                        : dataDetail.application_type_name}
                                    </span>
                                  </Col>
                                  <Col span={6}>
                                    <StyledITileHeading
                                      minFont="10px"
                                      maxFont="14px"
                                    >
                                      Phần thưởng khuyến mãi
                                    </StyledITileHeading>
                                    <span>
                                      {!dataDetail.reward_type_name
                                        ? "-"
                                        : dataDetail.reward_type_name}
                                    </span>
                                  </Col>
                                  {dataDetail.application_type_id === 2 ? (
                                    <Col span={6}>
                                      <p style={{ fontWeight: 600 }}>
                                        Quy cách khuyến mãi
                                      </p>
                                      <span>
                                        {dataDetail.product_atrtibute_type === 2
                                          ? "Quy cách lớn"
                                          : "Quy cách nhỏ"}
                                      </span>
                                    </Col>
                                  ) : null}
                                </Row>
                              </Col>
                            </Row>
                          </Fragment>
                        </div>
                      </Col>
                      <Col span={24} style={{ overflowX: "auto" }}>
                        <div>
                          <Row>
                            <Col span={24}>
                              <Row>
                                <Col span={6}>
                                  <div
                                    style={{
                                      textAlign: "center",
                                      borderLeft: "1px solid #C4C4C4",
                                      borderTop: "1px solid #C4C4C4",
                                      borderRight: "1px solid #C4C4C4",
                                      borderBottom: "1px solid #C4C4C4",
                                      padding: "10px 0px",
                                    }}
                                  >
                                    <StyledITileHeading
                                      minFont="10px"
                                      maxFont="14px"
                                    >
                                      Danh mục Sản phẩm áp dụng
                                    </StyledITileHeading>
                                  </div>
                                </Col>
                                <Col span={12}>
                                  <div
                                    style={{
                                      textAlign: "center",
                                      borderTop: "1px solid #C4C4C4",
                                      borderRight: "1px solid #C4C4C4",
                                      borderBottom: "1px solid #C4C4C4",
                                      padding: "10px 0px",
                                    }}
                                  >
                                    <StyledITileHeading
                                      minFont="10px"
                                      maxFont="14px"
                                    >
                                      Hạn mức thưởng
                                    </StyledITileHeading>
                                  </div>
                                </Col>
                                <Col span={6}>
                                  <div
                                    style={{
                                      textAlign: "center",
                                      borderTop: "1px solid #C4C4C4",
                                      borderRight: "1px solid #C4C4C4",
                                      borderBottom: "1px solid #C4C4C4",
                                      padding: "10px 0px",
                                    }}
                                  >
                                    <StyledITileHeading
                                      minFont="10px"
                                      maxFont="14px"
                                    >
                                      Điều kiện kèm theo
                                    </StyledITileHeading>
                                  </div>
                                </Col>
                              </Row>
                            </Col>
                            <Col span={24} style={{ background: "#f0f0f0" }}>
                              {!dataDetail.lst_event_child
                                ? null
                                : dataDetail.lst_event_child.map(
                                    (item, index) => {
                                      return (
                                        <>
                                          <Col span={24}>
                                            <div
                                              style={{
                                                borderLeft: "1px solid #C4C4C4",
                                                borderRight:
                                                  "1px solid #C4C4C4",
                                                borderBottom:
                                                  "1px solid #C4C4C4",
                                                padding: "10px 15px",
                                                display: "flex",
                                                flexDirection: "row",
                                                justifyContent: "space-between",
                                                alignItems: "center",
                                              }}
                                            >
                                              <div>
                                                <StyledITileHeading
                                                  minFont="10px"
                                                  maxFont="16px"
                                                  style={{
                                                    display: "inline-block",
                                                  }}
                                                >
                                                  Chương trình {index + 1}
                                                </StyledITileHeading>
                                                <StyledITileHeading
                                                  style={{
                                                    display: "inline-block",
                                                    marginLeft: 48,
                                                    color:
                                                      item.status === 1
                                                        ? colors.main
                                                        : colors.oranges,
                                                  }}
                                                  minFont="10px"
                                                  maxFont="16px"
                                                >
                                                  {item.status_name}
                                                </StyledITileHeading>
                                              </div>
                                            </div>
                                          </Col>

                                          <Col span={24}>
                                            <div
                                              style={{
                                                borderLeft: "1px solid #C4C4C4",
                                                borderRight:
                                                  "1px solid #C4C4C4",
                                                borderBottom:
                                                  "1px solid #C4C4C4",
                                                padding: "10px 15px",
                                              }}
                                            >
                                              <Row type="flex">
                                                <Col span={6}>
                                                  <StyledITileHeading
                                                    minFont="10px"
                                                    maxFont="14px"
                                                  >
                                                    Mức độ ưu tiên
                                                  </StyledITileHeading>
                                                  <Radio.Group
                                                    style={{
                                                      marginTop: 6,
                                                    }}
                                                    value={
                                                      item.event_priority_type
                                                    }
                                                  >
                                                    <Radio
                                                      value={
                                                        item.event_priority_type
                                                      }
                                                    >
                                                      {item.event_priority_type ===
                                                      DefineKeyEvent.song_song
                                                        ? "Song song"
                                                        : "Ưu tiên"}
                                                    </Radio>
                                                  </Radio.Group>
                                                </Col>
                                                {item.event_priority_type ===
                                                DefineKeyEvent.song_song ? null : (
                                                  <>
                                                    <Col
                                                      span={12}
                                                      style={{
                                                        textAlign: "center",
                                                      }}
                                                    >
                                                      <StyledITileHeading
                                                        minFont="10px"
                                                        maxFont="14px"
                                                      >
                                                        Độ ưu tiên
                                                      </StyledITileHeading>
                                                      <p
                                                        style={{
                                                          paddingTop: 6,
                                                        }}
                                                      >
                                                        {item.sort_order}
                                                      </p>
                                                      {/* <StyledInputNumber
                                                        className="disabled-up"
                                                        formatter={(value) =>
                                                          `${value}`.replace(
                                                            /\B(?=(\d{3})+(?!\d))/g,
                                                            ","
                                                          )
                                                        }
                                                        min={0}
                                                        value={item.sort_order}
                                                        parser={(value) =>
                                                          value.replace(
                                                            /\$\s?|(,*)/g,
                                                            ""
                                                          )
                                                        }
                                                        style={{
                                                          marginTop: 6,
                                                          borderRadius: 0,
                                                          width: 120,
                                                        }}
                                                      /> */}
                                                    </Col>
                                                    <Col
                                                      span={6}
                                                      style={{
                                                        textAlign: "center",
                                                      }}
                                                    >
                                                      <StyledITileHeading
                                                        minFont="10px"
                                                        maxFont="14px"
                                                      >
                                                        Loại trừ
                                                      </StyledITileHeading>
                                                      <div
                                                        style={{
                                                          marginTop: 6,
                                                          maxHeight: 80,
                                                          overflow: "auto",
                                                          textAlign: "left",
                                                          border:
                                                            "1px solid #C4C4C4",
                                                          padding: 6,
                                                        }}
                                                      >
                                                        {listEventChild.map(
                                                          (item1) => {
                                                            let dataNew =
                                                              !item.lst_excommunicate_event_ids
                                                                ? []
                                                                : item.lst_excommunicate_event_ids;
                                                            const check =
                                                              dataNew.includes(
                                                                item1.id
                                                              );
                                                            return (
                                                              <Checkbox
                                                                style={{
                                                                  display:
                                                                    "block",
                                                                  marginLeft: 8,
                                                                }}
                                                                checked={check}
                                                                key={item1.id}
                                                              >
                                                                {item1.name}
                                                              </Checkbox>
                                                            );
                                                          }
                                                        )}
                                                      </div>
                                                    </Col>
                                                  </>
                                                )}
                                              </Row>
                                            </div>
                                          </Col>

                                          <Col span={24}>
                                            <Row
                                              style={
                                                {
                                                  // minHeight: 200,
                                                  // maxHeight: 600,
                                                  // border: "1px solid black",
                                                }
                                              }
                                              type="flex"
                                            >
                                              <Col span={6}>
                                                <div
                                                  style={{
                                                    borderLeft:
                                                      "1px solid #C4C4C4",
                                                    borderRight:
                                                      "1px solid #C4C4C4",
                                                    borderBottom:
                                                      "1px solid #C4C4C4",
                                                    padding: "10px 10px",
                                                    height: "100%",
                                                  }}
                                                >
                                                  <Row>
                                                    <Col span={24}>
                                                      <div
                                                        style={{
                                                          border:
                                                            "1px solid #C4C4C4",
                                                          padding: "8px 8px",
                                                        }}
                                                      >
                                                        <Fragment>
                                                          <Row type="flex">
                                                            <Col
                                                              span={
                                                                dataDetail.application_type_id ===
                                                                2
                                                                  ? 24
                                                                  : 24
                                                              }
                                                            >
                                                              <div
                                                                style={{
                                                                  height:
                                                                    "100%",
                                                                  display:
                                                                    "flex",
                                                                  alignItems:
                                                                    "center",
                                                                }}
                                                              >
                                                                <p>
                                                                  DANH SÁCH SẢN
                                                                  PHẨM
                                                                </p>
                                                              </div>
                                                            </Col>
                                                            {/* {dataDetail.application_type_id ===
                                                            2 ? (
                                                              <Col span={6}>
                                                                <div
                                                                  style={{
                                                                    height:
                                                                      "100%",
                                                                    display:
                                                                      "flex",
                                                                    alignItems:
                                                                      "center",
                                                                    justifyContent:
                                                                      "center",
                                                                    textAlign:
                                                                      "center",
                                                                  }}
                                                                >
                                                                  <p>
                                                                    Tỉ lệ quy
                                                                    đổi
                                                                  </p>
                                                                </div>
                                                              </Col>
                                                            ) : null} */}
                                                          </Row>
                                                        </Fragment>
                                                      </div>
                                                    </Col>
                                                    <Col span={24}>
                                                      <div
                                                        style={{
                                                          borderLeft:
                                                            "1px solid #C4C4C4",
                                                          borderRight:
                                                            "1px solid #C4C4C4",
                                                          borderBottom:
                                                            "1px solid #C4C4C4",
                                                          padding: "8px 8px",
                                                          // height: "100%",
                                                          // overflow: "auto",
                                                        }}
                                                      >
                                                        {treeProduct(
                                                          item.list_cate_type
                                                        )}
                                                      </div>
                                                    </Col>
                                                  </Row>
                                                </div>
                                              </Col>
                                              <Col span={12}>
                                                <div
                                                  style={{
                                                    padding: "10px 10px",
                                                    borderBottom:
                                                      "1px solid #C4C4C4",
                                                    borderRight:
                                                      "1px solid #C4C4C4",
                                                    height: "100%",
                                                  }}
                                                >
                                                  <div
                                                    style={{
                                                      borderRight:
                                                        "1px solid #C4C4C4",
                                                      borderBottom:
                                                        "1px solid #C4C4C4",
                                                      padding:
                                                        "8px 10px 10px 10px",
                                                      borderLeft:
                                                        "1px solid #C4C4C4",
                                                      borderTop:
                                                        "1px solid #C4C4C4",
                                                    }}
                                                  >
                                                    <Row>
                                                      <Col span={24}>
                                                        <div
                                                          style={{
                                                            // borderLeft: "1px solid #C4C4C4",
                                                            // borderRight: "1px solid #C4C4C4",
                                                            // borderBottom: "1px solid #C4C4C4",
                                                            // borderTop: "1px solid #C4C4C4",
                                                            padding:
                                                              "0px 8px 8px 0px",
                                                          }}
                                                        >
                                                          <p>
                                                            {!dataDetail.application_type_name
                                                              ? "-"
                                                              : dataDetail.application_type_name}
                                                          </p>
                                                        </div>
                                                      </Col>
                                                      <Col span={24}>
                                                        <div
                                                          style={
                                                            {
                                                              // borderLeft: "1px solid #C4C4C4",
                                                              // borderRight: "1px solid #C4C4C4",
                                                              // borderBottom: "1px solid #C4C4C4",
                                                              // padding: "8px 8px",
                                                              // height: 300,
                                                              // overflow: "auto",
                                                            }
                                                          }
                                                        >
                                                          {item.list_product_condition.map(
                                                            (item1) => {
                                                              return renderItemSalesOrCount(
                                                                item1,
                                                                item.list_product_condition
                                                              );
                                                            }
                                                          )}
                                                        </div>
                                                      </Col>
                                                    </Row>
                                                  </div>
                                                </div>
                                              </Col>

                                              <Col span={6}>
                                                <div
                                                  style={{
                                                    borderRight:
                                                      "1px solid #C4C4C4",
                                                    borderBottom:
                                                      "1px solid #C4C4C4",
                                                    padding: "10px 10px",
                                                    height: "100%",
                                                  }}
                                                >
                                                  {!item.is_condition_order_join ? (
                                                    <Empty />
                                                  ) : (
                                                    item.lst_setup_condition.map(
                                                      (item1, index1) => (
                                                        <Col span={24}>
                                                          {_renderCondition(
                                                            item1.condition_style,
                                                            item1.type,
                                                            item1,
                                                            index1
                                                          )}
                                                        </Col>
                                                      )
                                                    )
                                                  )}
                                                </div>
                                              </Col>
                                            </Row>
                                          </Col>
                                        </>
                                      );
                                    }
                                  )}
                            </Col>
                            <Col span={24}>
                              <div
                                style={{
                                  borderLeft: "1px solid #C4C4C4",
                                  borderRight: "1px solid #C4C4C4",
                                  borderBottom: "1px solid #C4C4C4",
                                  padding: "10px 15px",
                                  display: "flex",
                                  flexDirection: "row",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <StyledITileHeading
                                    minFont="10px"
                                    maxFont="16px"
                                    style={{
                                      display: "inline-block",
                                    }}
                                  >
                                    Chương trình{" "}
                                    {dataDetail.lst_event_child.length + 1}
                                  </StyledITileHeading>
                                </div>
                              </div>
                            </Col>

                            <Col span={24}>
                              <div
                                style={{
                                  border: "1px solid #C4C4C4",
                                  borderTop: "unset",
                                  padding: "10px 15px",
                                }}
                              >
                                <Row type="flex">
                                  <Col span={6}>
                                    <StyledITileHeading
                                      minFont="10px"
                                      maxFont="14px"
                                    >
                                      Mức độ ưu tiên
                                    </StyledITileHeading>
                                    <Radio.Group
                                      style={{
                                        marginTop: 6,
                                      }}
                                      value={
                                        eventImmediateNew.event_priority_type
                                      }
                                      onChange={(e) => {
                                        const dataAction = {
                                          key: "event_priority_type",
                                          value: e.target.value,
                                        };
                                        dispatch(
                                          CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                            dataAction
                                          )
                                        );
                                      }}
                                    >
                                      <Radio value={DefineKeyEvent.song_song}>
                                        Song song
                                      </Radio>
                                      <Radio value={DefineKeyEvent.uu_tien}>
                                        Ưu tiên
                                      </Radio>
                                    </Radio.Group>
                                  </Col>
                                  {eventImmediateNew.event_priority_type ===
                                  DefineKeyEvent.song_song ? null : (
                                    <>
                                      <Col
                                        span={12}
                                        style={{
                                          textAlign: "center",
                                        }}
                                      >
                                        <StyledITileHeading
                                          minFont="10px"
                                          maxFont="14px"
                                        >
                                          Độ ưu tiên
                                        </StyledITileHeading>
                                        <StyledInputNumber
                                          className="disabled-up"
                                          formatter={(value) =>
                                            `${value}`.replace(
                                              /\B(?=(\d{3})+(?!\d))/g,
                                              ","
                                            )
                                          }
                                          onChange={(value) => {
                                            const dataAction = {
                                              key: "value_prioty",
                                              value: value,
                                            };
                                            dispatch(
                                              CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                                dataAction
                                              )
                                            );
                                          }}
                                          min={0}
                                          value={eventImmediateNew.value_prioty}
                                          parser={(value) =>
                                            value.replace(/\$\s?|(,*)/g, "")
                                          }
                                          style={{
                                            marginTop: 6,
                                            borderRadius: 0,
                                            width: 120,
                                          }}
                                        />
                                      </Col>
                                      <Col
                                        span={6}
                                        style={{
                                          textAlign: "center",
                                        }}
                                      >
                                        <StyledITileHeading
                                          minFont="10px"
                                          maxFont="14px"
                                        >
                                          Loại trừ
                                        </StyledITileHeading>
                                        <div
                                          style={{
                                            marginTop: 6,
                                            maxHeight: 80,
                                            overflow: "auto",
                                            textAlign: "left",
                                            border: "1px solid #C4C4C4",
                                            padding: 6,
                                          }}
                                        >
                                          {listEventChild.map((item1) => {
                                            let dataNew =
                                              !eventImmediateNew.excommunicate_event_ids
                                                ? []
                                                : eventImmediateNew.excommunicate_event_ids;
                                            const check = dataNew.includes(
                                              item1.id
                                            );
                                            return (
                                              <Checkbox
                                                style={{
                                                  display: "block",
                                                  marginLeft: 8,
                                                }}
                                                checked={check}
                                                key={item1.id}
                                                onClick={(e) => {
                                                  let dataNew = JSON.parse(
                                                    JSON.stringify(
                                                      eventImmediateNew.excommunicate_event_ids
                                                    )
                                                  );
                                                  if (e.target.checked) {
                                                    dataNew.push(item1.id);
                                                  } else {
                                                    const index =
                                                      dataNew.indexOf(item1.id);
                                                    dataNew.splice(index, 1);
                                                  }
                                                  const dataAction = {
                                                    key: "excommunicate_event_ids",
                                                    value: [...dataNew],
                                                  };
                                                  dispatch(
                                                    CREATE_DATE_EVENT_IMMEDIATE_NEW(
                                                      dataAction
                                                    )
                                                  );
                                                }}
                                              >
                                                {item1.name}
                                              </Checkbox>
                                            );
                                          })}
                                        </div>
                                      </Col>
                                    </>
                                  )}
                                </Row>
                              </div>
                            </Col>
                            <Col span={24}>
                              <Row type="flex">
                                <Col span={6}>
                                  <div
                                    style={{
                                      borderLeft: "1px solid #C4C4C4",
                                      borderRight: "1px solid #C4C4C4",
                                      borderBottom: "1px solid #C4C4C4",
                                      padding: "10px 10px",
                                      height: "100%",
                                    }}
                                  >
                                    <Row>
                                      <Col span={24}>
                                        <div
                                          style={{
                                            borderLeft: "1px solid #C4C4C4",
                                            borderRight: "1px solid #C4C4C4",
                                            borderBottom: "1px solid #C4C4C4",
                                            borderTop: "1px solid #C4C4C4",
                                            padding: "8px 8px",
                                          }}
                                        >
                                          <p>DANH SÁCH SẢN PHẨM</p>
                                        </div>
                                      </Col>
                                      <Col span={24}>
                                        <div
                                          style={{
                                            borderLeft: "1px solid #C4C4C4",
                                            borderRight: "1px solid #C4C4C4",
                                            borderBottom: "1px solid #C4C4C4",
                                            padding: "8px 8px",
                                          }}
                                        >
                                          {treeProduct1(
                                            eventImmediateNew.condition_promotion ===
                                              3
                                              ? []
                                              : treeProductIImmediate.listConfirm
                                          )}
                                        </div>
                                      </Col>
                                    </Row>
                                  </div>
                                </Col>
                                <Col span={12}>
                                  <div
                                    style={{
                                      padding: "10px 10px",
                                      borderBottom: "1px solid #C4C4C4",
                                      borderRight: "1px solid #C4C4C4",
                                      height: "100%",
                                    }}
                                  >
                                    <div
                                      style={{
                                        borderRight: "1px solid #C4C4C4",
                                        borderBottom: "1px solid #C4C4C4",
                                        padding: "8px 10px 10px 10px",
                                        borderLeft: "1px solid #C4C4C4",
                                        borderTop: "1px solid #C4C4C4",
                                      }}
                                    >
                                      <Row>
                                        <Col span={24}>
                                          <div
                                            style={{
                                              padding: "0px 8px 8px 0px",
                                            }}
                                          >
                                            <p>
                                              {!dataDetail.application_type_name
                                                ? "-"
                                                : dataDetail.application_type_name}
                                            </p>
                                          </div>
                                        </Col>
                                        <Col span={24}>
                                          <div>
                                            {eventImmediateNew.gift_promotion ===
                                            3
                                              ? eventImmediateNew.arr_gift_group.map(
                                                  (item, index) => {
                                                    return renderItemSalesOrCountPreview(
                                                      item,
                                                      eventImmediateNew.arr_gift_group,
                                                      index
                                                    );
                                                  }
                                                )
                                              : eventImmediateNew.arr_bonus_level.map(
                                                  (item, index) => {
                                                    return renderItemSalesOrCountPreview(
                                                      item,
                                                      eventImmediateNew.arr_bonus_level,
                                                      index
                                                    );
                                                  }
                                                )}
                                          </div>
                                        </Col>
                                      </Row>
                                    </div>
                                  </div>
                                </Col>

                                <Col span={6}>
                                  <div
                                    style={{
                                      borderRight: "1px solid #C4C4C4",
                                      borderBottom: "1px solid #C4C4C4",
                                      padding: "10px 10px",
                                      height: "100%",
                                    }}
                                  >
                                    {eventImmediateNew.check_condition_attach ===
                                    0 ? (
                                      <Empty />
                                    ) : (
                                      dataArrayCondition.map(
                                        (item1, index1) => (
                                          <Col span={24}>
                                            {_renderConditionPreview(
                                              _renderKeyCondition(
                                                item1.condition_type,
                                                item1.condition_order_style
                                              ),
                                              item1.condition_type,
                                              item1,
                                              index1
                                            )}
                                          </Col>
                                        )
                                      )
                                    )}
                                  </div>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
                <Col span={24}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                    }}
                  >
                    <IButton
                      onClick={() => history.goBack()}
                      title="Quay lại"
                      color={colors.main}
                      icon={ISvg.NAME.ARROWTHINLEFT}
                      style={{ marginRight: 12 }}
                      styleHeight={{
                        width: 140,
                      }}
                    />
                    <IButton
                      title={"Hoàn thành"}
                      onClick={() => {
                        if (eventImmediateNew.condition_promotion !== 3) {
                          if (treeProductIImmediate.listConfirm.length === 0) {
                            const content = (
                              <span>
                                Vui lòng chọn{" "}
                                <span style={{ fontWeight: 600 }}>
                                  sản phẩm.
                                </span>
                              </span>
                            );
                            return IError(content);
                          }
                        }

                        let valid = false;

                        if (eventImmediateNew.gift_promotion !== 3) {
                          const even = (element) => element.to === 0;
                          const check_1_limit =
                            eventImmediateNew.arr_bonus_level.some((item) =>
                              even(item)
                            );
                          if (
                            check_1_limit &&
                            eventImmediateNew.arr_bonus_level.length > 1
                          ) {
                            const content = (
                              <span>
                                Điều kiện{" "}
                                <span style={{ fontWeight: 600 }}>
                                  CHỈ ĐỊNH
                                </span>{" "}
                                không gồm nhiều hạng mức
                              </span>
                            );
                            return IError(content);
                          }
                          if (eventImmediateNew.arr_bonus_level.length > 1) {
                            let condition_limit = [];
                            eventImmediateNew.arr_bonus_level.forEach(
                              (item) => {
                                condition_limit = [
                                  ...condition_limit,
                                  ...[item.from, item.to],
                                ];
                              }
                            );
                            let check_conditon_limit = condition_limit.every(
                              function (x, i) {
                                return i === 0 || x >= condition_limit[i - 1];
                              }
                            );
                            if (!check_conditon_limit) {
                              const content = (
                                <span>
                                  Thiết lập điều kiện sai quy tắc xét thưởng.
                                </span>
                              );
                              return IError(content);
                            }
                          }
                        }

                        if (valid === true) {
                          const content = (
                            <span>
                              Vui lòng điền đầy đủ thông tin và chính xác giá
                              trị các{" "}
                              <span style={{ fontWeight: 600 }}>
                                hạng mức thưởng.
                              </span>
                            </span>
                          );
                          return IError(content);
                        }
                        setLoadingSubmit(true);
                        const dataAdd = createEventImmediate({
                          eventImmediateNew,
                          treeProductIImmediate,
                          modalS1,
                          modalS2,
                          dataArrayCondition,
                          modalBusiness,
                          modalCity,
                          modalDistrict,
                          modalWard,
                          treeProductPresentIImmediateConditionAttach,
                          treeProductImmediateConditionAttachGroup,
                        });
                        _fetchAPIPostCreateEvent(dataAdd);
                      }}
                      loading={loadingSubmit}
                      color={colors.main}
                      icon={ISvg.NAME.ARROWRIGHT}
                      iconRight={true}
                      styleHeight={{
                        width: 140,
                      }}
                    />
                  </div>
                </Col>
              </Row>
            </Fragment>
          </div>
        </Col>
      </Row>
    </div>
  );
}
