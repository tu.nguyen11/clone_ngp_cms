import { Col, Row, Form, Modal } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import { IButton, ISvg, ITableHtml } from "../../../components";
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1,
  useModalAgencyS2,
} from "../../../components/common";
import moment from "moment";

import {
  StyledITitle,
  StyledITileHeading,
} from "../../../components/common/Font/font";
import {
  CREATE_DATE_EVENT,
  CLEAR_REDUX,
  RESET_MODAL,
} from "../../../store/reducers";
import { DefineKeyEvent } from "../../../../utils/DefineKey";

export default function CreateEventFilter(props) {
  const history = useHistory();
  const params = useParams();
  const { id } = params;
  const format = "HH:mm:ss DD-MM-YYYY";

  const [isVisibleAgency, setVisibleAgency] = useState(false);

  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { event, modalS1, modalS2 } = dataRoot;

  const [isOpenApply, setIsOpenApply] = useState(false);
  const [isOpenPayThereWard, setIsOpenPayThereWard] = useState(false);

  const handleStartOpenChange = (open) => {
    if (!open) {
      setIsOpenApply(true);
    }
  };

  const handleEndOpenChange = (open) => {
    setIsOpenApply(open);
  };

  const handleStartOpenChangePay = (open) => {
    if (!open) {
      setIsOpenPayThereWard(true);
    }
  };

  const handleEndOpenChangePay = (open) => {
    setIsOpenPayThereWard(open);
  };

  const disabledStartDateApply = (startValue) => {
    if (!startValue || !event.to_time_apply) {
      return false;
    }
    return startValue.valueOf() > event.to_time_apply;
  };

  const disabledEndDateApply = (endValue) => {
    if (!endValue || !event.from_time_apply) {
      return false;
    }
    return endValue.valueOf() <= event.from_time_apply;
  };

  const disabledStartDatePaythereward = (startValue) => {
    if (!startValue || !event.to_time_paythereward) {
      return false;
    }
    return startValue.valueOf() > event.to_time_paythereward;
  };

  const disabledEndDatePaythereward = (endValue) => {
    if (!endValue || !event.from_time_paythereward) {
      return false;
    }
    return endValue.valueOf() <= event.from_time_paythereward;
  };

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp đại lý",
      align: "center",
    },
  ];

  let databody = [];
  for (let i = 0; i < 10; i++) {
    databody.push({
      name: "Cửa hàng phân bón màu xanh",
      code: "ABC123456",
      level: "Cấp 1",
    });
  }

  const headerTable = (dataHeader) => {
    return (
      <tr className="tr-table scroll">
        {dataHeader.map((item, index) => (
          <th className="th-table" style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (databody) => {
    return databody.map((item) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {event.type_way === 2 ? item.shop_name : item.supplierName}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {event.type_way === 2 ? item.dms_code : item.supplierCode}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "center", fontWeight: "normal" }}
        >
          {event.type_way === 2 ? "Cấp 1" : "Cấp 2"}
        </td>
      </tr>
    ));
  };

  const _renderModalS1 = () => {
    return (
      <Modal
        visible={isVisibleAgency}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1(() => {
          setVisibleAgency(false);
        })}
      </Modal>
    );
  };

  const _renderModalS2 = () => {
    return (
      <Modal
        visible={isVisibleAgency}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS2(() => {
          setVisibleAgency(false);
        })}
      </Modal>
    );
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo chương trình tích lũy mới
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX());
                    history.push("/listEventPage");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1100 }}>
            <Row gutter={[24, 24]}>
              <Col span={13}>
                <div
                  style={{ padding: 24, height: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thời gian áp dụng & trả thưởng
                      </StyledITileHeading>
                    </Col>
                    <Col>
                      <div style={{ marginTop: 16 }}>
                        <TagP style={{ fontWeight: 500 }}>
                          Thời gian áp dụng
                        </TagP>
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput="0px"
                                disabled={
                                  event.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                placeholder="nhập thời gian bắt đầu"
                                onOpenChange={handleStartOpenChange}
                                format={format}
                                value={
                                  event.from_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(event.from_time_apply),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "from_time_apply",
                                    value: date._d.getTime(),
                                  };

                                  dispatch(CREATE_DATE_EVENT(data));
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col>
                              <TagP style={{ fontWeight: 500 }}>Kết thúc</TagP>
                            </Col>
                            <Col>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledEndDateApply}
                                isBorderBottom={true}
                                disabled={
                                  event.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                paddingInput="0px"
                                open={isOpenApply}
                                format={format}
                                onOpenChange={handleEndOpenChange}
                                value={
                                  event.to_time_apply === 0
                                    ? undefined
                                    : moment(
                                        new Date(event.to_time_apply),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "to_time_apply",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_DATE_EVENT(data));
                                }}
                                placeholder="nhập thời gian kết thúc"
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <TagP style={{ fontWeight: 500 }}>
                        Thời gian trả thưởng
                      </TagP>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col>
                              <IDatePickerFrom
                                showTime
                                disabledDate={disabledStartDatePaythereward}
                                isBorderBottom={true}
                                format={format}
                                disabled={
                                  event.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                paddingInput="0px"
                                placeholder="nhập thời gian bắt đầu"
                                value={
                                  event.from_time_paythereward === 0
                                    ? undefined
                                    : moment(
                                        new Date(event.from_time_paythereward),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "from_time_paythereward",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_DATE_EVENT(data));
                                }}
                                onOpenChange={handleStartOpenChangePay}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col>
                              <TagP style={{ fontWeight: 500 }}>Đến ngày</TagP>
                            </Col>
                            <Col>
                              <IDatePickerFrom
                                showTime
                                format={format}
                                isBorderBottom={true}
                                disabled={
                                  event.type_event_status ===
                                  DefineKeyEvent.event_running
                                }
                                disabledDate={disabledEndDatePaythereward}
                                paddingInput="0px"
                                placeholder="nhập thời gian kết thúc"
                                value={
                                  event.to_time_paythereward === 0
                                    ? undefined
                                    : moment(
                                        new Date(event.to_time_paythereward),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "to_time_paythereward",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_DATE_EVENT(data));
                                }}
                                open={isOpenPayThereWard}
                                onOpenChange={handleEndOpenChangePay}
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={11}>
                <div
                  style={{ padding: 24, height: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 24]}>
                    <Col>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    {event.type_event_status ===
                    DefineKeyEvent.event_running ? null : (
                      <Col span={24}>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "flex-end",
                          }}
                        >
                          <IButton
                            title="Chỉnh sửa danh sách"
                            color={colors.main}
                            icon={ISvg.NAME.WRITE}
                            onClick={() => setVisibleAgency(true)}
                          />
                        </div>
                      </Col>
                    )}

                    <Col span={24}>
                      <ITableHtml
                        height="420px"
                        childrenBody={bodyTable(
                          event.type_way === 2
                            ? modalS1.listS1Show
                            : modalS2.listS2Show
                        )}
                        childrenHeader={headerTable(dataHeader)}
                        style={{
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          padding: "0px 8px ",
                        }}
                        isBorder={false}
                      />
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: "flex", justifyContent: "flex-end" }}
                      >
                        <IButton
                          title="Hủy"
                          color={colors.oranges}
                          icon={ISvg.NAME.CROSS}
                          styleHeight={{
                            width: 140,
                          }}
                          disabled={
                            event.type_event_status ===
                            DefineKeyEvent.event_running
                          }
                          onClick={() => {
                            const data = {
                              key: "type_way",
                              value: 0,
                            };
                            dispatch(CREATE_DATE_EVENT(data));
                            event.type_way == 2
                              ? dispatch(
                                  RESET_MODAL({
                                    keyInitial_listShow: "listS1Show",
                                    keyInitial_root: "modalS1",
                                    keyInitial_listAddClone: "listS1AddClone",
                                    keyInitial_listAdd: "listS1Add",
                                    keyInitial_list: "listS1",
                                  })
                                )
                              : dispatch(
                                  RESET_MODAL({
                                    keyInitial_listShow: "listS2Show",
                                    keyInitial_root: "modalS2",
                                    keyInitial_listAddClone: "listS2AddClone",
                                    keyInitial_listAdd: "listS2Add",
                                    keyInitial_list: "listS2",
                                  })
                                );

                            history.goBack();
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={() => {
                    let dateNow = new Date();
                    dateNow.setHours(
                      dateNow.getHours() + 1,
                      dateNow.getMinutes() - 5
                    );

                    if (event.type_way === 2) {
                      if (modalS1.listS1Show.length === 0) {
                        return IError("Chưa chọn đại lý áp dụng chương trình");
                      }
                    } else {
                      if (modalS2.listS2Show.length === 0) {
                        return IError("Chưa chọn đại lý áp dụng chương trình");
                      }
                    }

                    if (
                      event.from_time_apply === 0 ||
                      event.to_time_apply === 0 ||
                      event.from_time_paythereward === 0 ||
                      event.to_time_paythereward === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian áp dụng
                          </span>{" "}
                          & <span style={{ fontWeight: 600 }}>trả thương</span>{" "}
                          , <span style={{ fontWeight: 600 }}>đại lý</span> áp
                          dụng chương trình
                        </span>
                      );
                      return IError(content);
                    }

                    // if (event.ISedit) {
                    //   if (
                    //     event.type_event_status !== DefineKeyEvent.event_running
                    //   ) {
                    //     if (event.from_time_apply <= dateNow.getTime()) {
                    //       const content = (
                    //         <span>
                    //           Ngày bắt đầu chương trình phải lớn hơn giờ hiện
                    //           tại 1 tiếng.
                    //         </span>
                    //       );
                    //       return IError(content);
                    //     }
                    //   }
                    // } else {
                    //   if (event.from_time_apply <= dateNow.getTime()) {
                    //     const content = (
                    //       <span>
                    //         Ngày bắt đầu chương trình phải lớn hơn giờ hiện tại
                    //         1 tiếng.
                    //       </span>
                    //     );
                    //     return IError(content);
                    //   }
                    // }
                    history.push(`/eventCreate/filter/desgin/${id}`);
                  }}
                  title="Tiếp tục"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      {event.type_way === 2 ? _renderModalS1() : _renderModalS2()}
    </div>
  );
}
