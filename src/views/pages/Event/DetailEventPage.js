import { BackTop, Col, Row, Skeleton } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory, useParams } from 'react-router-dom'
import { colors } from '../../../assets'
import { APIService } from '../../../services'
import FormatterDay from '../../../utils/FormatterDay'
import { IButton, ISvg, ITableHtml } from '../../components'
import {
  StyledITileHeading,
  StyledITitle
} from '../../components/common/Font/font'
// import { _fetchAPIDetailEvent } from "../../store/reducers";
import { priceFormat } from '../../../utils'
import {
  ASSIGNED_DATA_REDUX_DETAIL,
  UPDATE_TREE,
  UPDATE_TREE_PRIORITY
} from '../../store/reducers'
import { DefineKeyEvent } from '../../../utils/DefineKey'

export default function DetailEventPage () {
  const dispatch = useDispatch()
  const dataRoot = useSelector(state => state)
  const { treeProduct } = dataRoot
  const history = useHistory()
  const params = useParams()
  const [urlIMG, setUrlIMG] = useState('')
  const [dataDetail, setDataDetail] = useState({
    list_application_object: [],
    list_cate_type: [],
    list_product_condition: [],
    lst_region: [],
    lst_city: []
  })
  const [eventRewardType, setEventRewardType] = useState('')
  const [loading, setLoading] = useState(true)
  const { id, type } = params
  const _fetchAPIDetailEvent = async id => {
    try {
      let data = await APIService._getDetailEvent(id)
      data.event_response.image_url = data.image_url
      data.event_response.type_event_status = Number(type)
      setEventRewardType(
        data.event_response.list_product_condition[0].event_reward_type || ''
      )
      setDataDetail(data.event_response)
      setUrlIMG(data.image_url)
      setLoading(false)
    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  useEffect(() => {
    _fetchAPIDetailEvent(id)
  }, [])

  let dataHeader = [
    {
      name: 'Tên đại lý',
      align: 'left'
    },
    {
      name: 'Mã đại lý',
      align: 'left'
    },
    {
      name: 'Cấp đại lý',
      align: 'center'
    }
  ]

  const headerTable = dataHeader => {
    return (
      <tr className='tr-table scroll'>
        {dataHeader.map((item, index) => (
          <th className='th-table' style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    )
  }

  const bodyTable = databody => {
    return databody.map(item => (
      <tr className='tr-table'>
        <td
          className='td-table'
          style={{ textAlign: 'left', fontWeight: 'normal' }}
        >
          {item.user_agency_name}
        </td>
        <td
          className='td-table'
          style={{ textAlign: 'left', fontWeight: 'normal' }}
        >
          {item.user_agency_code}
        </td>
        <td
          className='td-table'
          style={{
            textAlign: 'center',
            fontWeight: 'normal',
            width: 80
          }}
        >
          {item.level}
        </td>
      </tr>
    ))
  }

  const _renderItemCount = (item, index) => {
    return (
      <tr>
        <td>Từ: </td>
        <td style={{ fontWeight: 500 }}>{priceFormat(item.quantity_min)}</td>
        <td style={{ width: 50, textAlign: 'center' }}>-</td>
        <td>Đến dưới</td>
        <td style={{ fontWeight: 500 }}>{priceFormat(item.quantity_max)}</td>
        <td>Mức chiết khấu</td>
        <td style={{ fontWeight: 500 }}>
          {item.event_reward_type === 'EVENT_REWARD_DISCOUNT'
            ? (item.vat * 100).toFixed(2)
            : item.vat}
        </td>
        <td>
          {item.event_reward_type === 'EVENT_REWARD_DISCOUNT' ? (
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <span style={{ fontWeight: 500, marginRight: 5 }}>%/</span>
              <div style={{ width: 60, lineHeight: 0.9 }}>
                <span
                  style={{
                    fontSize: 'clamp(4px, 4vw, 10px)'
                  }}
                >
                  Doanh số tích lũy
                </span>
              </div>
            </div>
          ) : (
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <span style={{ fontWeight: 500 }}>đ/</span>
              <div style={{ width: 60, lineHeight: 0.9 }}>
                <span
                  style={{
                    fontSize: 'clamp(4px, 4vw, 10px)'
                  }}
                >
                  Quy cách tích lũy
                </span>
              </div>
            </div>
          )}
        </td>
      </tr>
    )
  }

  const _renderItemSales = (item, index) => {
    return (
      <tr>
        <td>Từ: </td>
        <td style={{ fontWeight: 500 }}>{priceFormat(item.money_min)}đ </td>
        <td style={{ width: 50, textAlign: 'center' }}>-</td>
        <td>Đến</td>
        <td style={{ fontWeight: 500 }}>{priceFormat(item.money_max)}đ</td>
        <td> Tỉ lệ chiết khấu</td>
        <td style={{ fontWeight: 500 }}>{(item.vat * 100).toFixed(2)}%</td>
      </tr>
    )
  }

  const treeProct = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: 'none',
                  padding: 0
                }}
              >
                <li
                  style={{
                    fontWeight: 'bold',
                    borderBottom: '0.5px dashed #7A7B7B',
                    padding: '6px 0px'
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children)}
              </ul>
            )
          } else {
            return (
              <ul
                style={{
                  listStyleType: 'none',
                  borderBottom: '0.5px dashed #7A7B7B',
                  padding: 0
                }}
              >
                <li style={{ padding: '6px 0px' }}>
                  <Row gutter={[12, 0]} type='flex'>
                    <Col span={2} style={{ paddingTop: 3 }}>
                      <div
                        style={{
                          height: '100%',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center'
                        }}
                      >
                        <div
                          style={{
                            width: 16,
                            height: 16,
                            borderRadius: 8,
                            border:
                              dataDetail.type_promotion_id === 2 &&
                              item.id === dataDetail.key_default_product
                                ? `4px solid ${colors.main}`
                                : null,
                            paddingRight: 8
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={dataDetail.type_promotion_id === 2 ? 19 : 22}>
                      <div
                        style={{
                          wordWrap: 'break-word',
                          wordBreak: 'break-word',
                          overflow: 'hidden'
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    {dataDetail.type_promotion_id === 2 ? (
                      <Col span={3}>
                        <div
                          style={{
                            textAlign: 'center',
                            height: '100%',
                            display: 'flex',
                            alignItems: 'center'
                          }}
                        >
                          {item.convert_ratio}
                        </div>
                      </Col>
                    ) : null}
                  </Row>
                </li>
              </ul>
            )
          }
        })}
      </ul>
    )
  }

  const _renderUIObjectGreedAssign = () => {
    return (
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 1</p>
          <div
            style={{
              border: '1px solid rgba(122, 123, 123, 0.5)',
              padding: 6,
              marginTop: 12
            }}
          >
            <p
              style={{
                borderBottom: '1px solid rgba(122, 123, 123, 0.5)',
                paddingBottom: 6
              }}
            >
              Cấp người dùng
            </p>
            <p style={{ padding: '6px 0px' }}>{dataDetail.level}</p>
          </div>
        </Col>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 2</p>
          <div
            style={{
              border: '1px solid rgba(122, 123, 123, 0.5)',
              padding: 6,
              marginTop: 12
            }}
          >
            <p
              style={{
                borderBottom: '1px solid rgba(122, 123, 123, 0.5)',
                paddingBottom: 6
              }}
            >
              Vùng địa lý
            </p>
            <p style={{ padding: '6px 0px' }}>
              {dataDetail.lst_region.map(item => {
                if (dataDetail.lst_region.length === 1) {
                  return <span>{item.name}</span>
                }
                return <span>{item.name + ', '}</span>
              })}
            </p>
          </div>
        </Col>
        <Col span={24}>
          <p style={{ fontWeight: 500 }}>Điều kiện tham gia 3</p>
          <div
            style={{
              border: '1px solid rgba(122, 123, 123, 0.5)',
              padding: 6,
              marginTop: 12
            }}
          >
            <p
              style={{
                borderBottom: '1px solid rgba(122, 123, 123, 0.5)',
                paddingBottom: 6
              }}
            >
              Tỉnh / Thành phố
            </p>
            <p style={{ padding: '6px 0px', maxHeight: 100, overflow: 'auto' }}>
              {dataDetail.lst_city.map(item => {
                if (dataDetail.lst_city.length === 1) {
                  return <span>{item.name}</span>
                }
                return <span>{item.name + ', '}</span>
              })}
            </p>
          </div>
        </Col>
      </Row>
    )
  }

  return (
    <div style={{ width: '100%', padding: '18px 0px 0px 0px' }}>
      <BackTop />
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Chi tiết Chương trình tích lũy
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'flex-end'
                }}
              >
                <Link to={'/statisticalEventPage/' + id}>
                  <IButton
                    title='Xem thống kê'
                    color={colors.main}
                    style={{ marginRight: 24 }}
                    styleHeight={{
                      width: 140
                    }}
                  />
                </Link>
                {Number(type) === DefineKeyEvent.event_end ? null : (
                  <IButton
                    title='Chỉnh sửa'
                    color={colors.main}
                    icon={ISvg.NAME.WRITE}
                    styleHeight={{
                      width: 140
                    }}
                    onClick={() => {
                      dispatch(ASSIGNED_DATA_REDUX_DETAIL(dataDetail))
                      history.push(`/eventCreate/edit/${id}`)
                    }}
                  />
                )}
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div>
            <Row gutter={[24, 0]}>
              <Col span={16}>
                <div className='box-shadow' style={{ padding: '24px 36px' }}>
                  <Skeleton loading={loading} active paragraph={{ rows: 8 }}>
                    <Row gutter={[24, 0]}>
                      <Col span={11}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Mô tả & nguyên tắc Chương trình
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <p style={{ fontWeight: 500 }}>Mã chương trình</p>
                              <span>{dataDetail.code}</span>
                            </div>
                          </Col>
                          <Col span={24}>
                            <p style={{ fontWeight: 500 }}>Tên chương trình</p>
                            <span>{dataDetail.name}</span>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[24, 0]}>
                              <Col span={12}>
                                <p style={{ fontWeight: 500 }}>
                                  Trạng thái chương trình
                                </p>
                                <span>{dataDetail.status_name}</span>
                              </Col>
                              <Col span={12}>
                                <p style={{ fontWeight: 500 }}>
                                  Hình thức tích lũy
                                </p>
                                <span>
                                  {dataDetail.type_promotion_id === 1
                                    ? 'Doanh số sản phẩm'
                                    : 'Số lượng sản phẩm'}
                                </span>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={13}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Thời gian áp dụng
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <Row gutter={[24, 0]}>
                                <Col span={12}>
                                  <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                                  <span>
                                    {FormatterDay.dateFormatWithString(
                                      dataDetail.start_date,
                                      '#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#'
                                    )}
                                  </span>
                                </Col>
                                <Col span={12}>
                                  <p style={{ fontWeight: 500 }}>Kết thúc</p>
                                  <span>
                                    {FormatterDay.dateFormatWithString(
                                      dataDetail.end_date,
                                      '#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#'
                                    )}
                                  </span>{' '}
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <StyledITileHeading minFont='10px' maxFont='16px'>
                                Thời gian trả thưởng
                              </StyledITileHeading>
                            </div>
                          </Col>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <Row gutter={[24, 0]}>
                                <Col span={12}>
                                  <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                                  <span>
                                    {FormatterDay.dateFormatWithString(
                                      dataDetail.start_reward,
                                      '#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#'
                                    )}
                                  </span>
                                </Col>
                                <Col span={12}>
                                  <p style={{ fontWeight: 500 }}>Kết thúc</p>
                                  <span>
                                    {FormatterDay.dateFormatWithString(
                                      dataDetail.end_reward,
                                      '#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#'
                                    )}
                                  </span>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col span={24}>
                            <div style={{ marginTop: 12 }}>
                              <StyledITileHeading minFont='10px' maxFont='16px'>
                                Đối tượng áp dụng
                              </StyledITileHeading>
                            </div>
                          </Col>
                          <Col span={24}>
                            {dataDetail.type_entry_condition === 1 ? (
                              _renderUIObjectGreedAssign()
                            ) : (
                              <ITableHtml
                                height='420px'
                                childrenBody={bodyTable(
                                  dataDetail.list_application_object
                                )}
                                childrenHeader={headerTable(dataHeader)}
                                style={{
                                  border: '1px solid rgba(122, 123, 123, 0.5)',
                                  padding: '0px 8px '
                                }}
                                isBorder={false}
                              />
                            )}
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
                <div
                  style={{
                    padding: '24px 36px',
                    borderTop: '1px solid rgba(122, 123, 123, 0.5)'
                  }}
                  className='box-shadow'
                >
                  <Skeleton loading={loading} active paragraph={{ rows: 8 }}>
                    <Row gutter={[24, 0]}>
                      <Col span={24}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Thể lệ & chi tiết thiết lập
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <p>
                              <StyledITileHeading minFont='10px' maxFont='16px'>
                                Ngành hàng áp dụng
                              </StyledITileHeading>
                            </p>
                            <span>{dataDetail.company_name}</span>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={10}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <div style={{ marginTop: 24 }}>
                              <StyledITileHeading minFont='10px' maxFont='16px'>
                                Danh mục tích lũy
                              </StyledITileHeading>
                            </div>
                          </Col>
                          <Col span={24}>
                            <div
                              style={{
                                height: 455,
                                overflow: 'auto',
                                border: '1px solid rgba(122, 123, 123, 0.5)',
                                paddingLeft: 10,
                                paddingRight: 10
                              }}
                            >
                              <Row>
                                {dataDetail.type_promotion_id === 2 ? (
                                  <Col
                                    span={24}
                                    style={{
                                      textAlign: 'right',
                                      borderBottom: '0.5px solid #bcbcbc',
                                      padding: '6px 0'
                                    }}
                                  >
                                    <span
                                      style={{
                                        fontWeight: 600
                                      }}
                                    >
                                      Tỉ lệ quy đổi
                                    </span>
                                  </Col>
                                ) : null}

                                <Col span={24}>
                                  {treeProct(dataDetail.list_cate_type)}
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={14}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <div style={{ marginTop: 24 }}>
                              <StyledITileHeading minFont='10px' maxFont='16px'>
                                Điều kiện tích lũy & khuyến mãi
                              </StyledITileHeading>
                            </div>
                          </Col>
                          {dataDetail.type_promotion_id === 1 ? (
                            <Col span={24}>
                              <span>Doanh số sản phẩm</span>
                            </Col>
                          ) : (
                            <Col span={24}>
                              <Row>
                                <Col span={24}>
                                  <span>Số lượng sản phẩm</span>
                                </Col>
                                <Col span={24}>
                                  <Row>
                                    <Col span={12}>
                                      <StyledITileHeading
                                        minFont='10px'
                                        maxFont='16px'
                                      >
                                        Quy cách tích lũy
                                      </StyledITileHeading>
                                      <span>
                                        {dataDetail.attribute_type === 2
                                          ? 'Quy cách nhỏ'
                                          : 'Quy cách lớn'}
                                      </span>
                                    </Col>
                                    <Col span={12}>
                                      <StyledITileHeading
                                        minFont='10px'
                                        maxFont='16px'
                                      >
                                        Thưởng chiết khấu
                                      </StyledITileHeading>
                                      <span>
                                        {eventRewardType ===
                                        'EVENT_REWARD_MONEY'
                                          ? 'đ/ Quy cách tích lũy'
                                          : '%/ Doanh số tích lũy'}
                                      </span>
                                    </Col>
                                  </Row>
                                </Col>
                              </Row>
                            </Col>
                          )}
                          <Col span={24}>
                            <div
                              style={{
                                overflow: 'auto',
                                border: '1px solid rgba(122, 123, 123, 0.5)',
                                padding: '10px 18px'
                              }}
                            >
                              <p style={{ fontWeight: 500 }}>
                                {dataDetail.type_promotion_id === 1
                                  ? 'Doanh số sản phẩm'
                                  : 'Số lượng sản phẩm'}
                              </p>
                              <table style={{ marginTop: 18 }}>
                                {dataDetail.list_product_condition.map(item => {
                                  return dataDetail.type_promotion_id === 2
                                    ? _renderItemCount(item)
                                    : _renderItemSales(item)
                                })}
                              </table>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
              <Col span={8}>
                <div className='box-shadow' style={{ padding: '24px 36px' }}>
                  <Skeleton loading={loading} active paragraph={{ rows: 16 }}>
                    <Row gutter={[24, 0]}>
                      <Col span={24}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Hình ảnh
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <img
                              src={urlIMG + dataDetail.image}
                              alt='hình-event'
                              style={{
                                width: '100%',
                                height: 300,
                                objectFit: 'cover'
                              }}
                            />
                          </Col>

                          <Col span={24}>
                            <StyledITileHeading minFont='10px' maxFont='16px'>
                              Nội dung mô tả
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <span>{dataDetail.description}</span>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  )
}
