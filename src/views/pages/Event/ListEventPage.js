import React, { useState, useEffect, useRef } from "react";
import { Row, Col, Button, message, Tooltip } from "antd";
import { Link, useHistory } from "react-router-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IDatePicker,
} from "../../components";
import FormatterDay from "../../../utils/FormatterDay";
import { images, colors } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { APIService } from "../../../services";
import { useDispatch } from "react-redux";
import { ASSIGNED_DATA_REDUX_DETAIL, CLEAR_REDUX } from "../../store/reducers";
import { DefineKeyEvent } from "../../../utils/DefineKey";

function ListEventPage(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const formatString = "#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#";

  const NewDate = new Date();
  const DateStart = new Date();
  let startValue = DateStart.setMonth(DateStart.getMonth() - 1);
  const [isLoadingEdit, setLoadingEdit] = useState(false);
  const [filter, setFilter] = useState({
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
    status: -1,
    type: -1,
    key: "",
    page: 1,
  });

  const _fetchAPIDetailEvent = async (id, type) => {
    try {
      let data = await APIService._getDetailEvent(id);
      data.event_response.image_url = data.image_url;
      data.event_response.type_event_status = type;

      dispatch(ASSIGNED_DATA_REDUX_DETAIL(data.event_response));
      setLoadingTable(false);
      history.push(`/eventCreate/edit/${id}`);
    } catch (error) {
      setLoadingTable(false);
      console.log(error);
    }
  };

  const [loadingTable, setLoadingTable] = useState(true);

  const _fetchAPIGetEvent = async (filter) => {
    try {
      const data = await APIService._getListEvent(filter);
      data.listEvent.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
      });
      setData(data);
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIGetEvent(filter);
  }, [filter]);

  const [data, setData] = useState({
    listEvent: [],
    total: 1,
    size: 10,
  });

  const status = [
    {
      key: -1,
      value: "Tất cả",
    },
    {
      key: 2,
      value: "Đang chạy",
    },
    {
      key: 1,
      value: "Khởi tạo",
    },
    {
      key: 3,
      value: "Kết thúc",
    },
  ];

  const type = [
    {
      key: -1,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Chương trình trả ngay",
    },
    {
      key: 1,
      value: "Chương trình tích lũy",
    },
  ];

  const renderTitle = (title) => {
    return (
      <div>
        <ITitle title={title} style={{ fontWeight: "bold" }} />
      </div>
    );
  };

  const columns = [
    {
      title: renderTitle("STT"),
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },
    {
      title: renderTitle("Mã chương trình"),
      dataIndex: "code",
      key: "code",
      wordWrap: "break-word",
      wordBreak: "break-word",
    },
    {
      title: renderTitle("Tên chương trình"),
      dataIndex: "name",
      key: "name",
      wordWrap: "break-word",
      wordBreak: "break-word",
    },
    {
      title: renderTitle("Trạng thái"),
      dataIndex: "status_name",
      key: "status_name",
    },
    {
      title: renderTitle("Bắt đầu"),
      dataIndex: "start_date",
      key: "start_date",
      render: (start_date) => {
        return FormatterDay.dateFormatWithString(start_date, formatString);
      },
    },
    {
      title: renderTitle("Kết thúc"),
      dataIndex: "end_date",
      key: "end_date",
      render: (end_date) => {
        return FormatterDay.dateFormatWithString(end_date, formatString);
      },
    },
    {
      title: renderTitle("Bắt đầu trả thưởng"),
      dataIndex: "start_reward",
      key: "start_reward",
      render: (start_reward) => {
        return FormatterDay.dateFormatWithString(start_reward, formatString);
      },
    },
    {
      title: renderTitle("Kết thúc trả thưởng"),
      dataIndex: "end_reward",
      key: "end_reward",
      render: (end_reward) => {
        return FormatterDay.dateFormatWithString(end_reward, formatString);
      },
    },
    {
      title: "",
      align: "center",
      width: 30,
      render: (obj) => {
        const type =
          obj.status_name === "Khởi tạo"
            ? DefineKeyEvent.event_initialization
            : obj.status_name === "Đang chạy"
            ? DefineKeyEvent.event_running
            : DefineKeyEvent.event_end;
        return obj.status_name === "Kết thúc" ? null : (
          <Button
            type="link"
            className="cursor scale"
            onClick={(e) => {
              e.stopPropagation();
              setLoadingTable(true);
              _fetchAPIDetailEvent(obj.id, type);
            }}
          >
            <ISvg
              name={ISvg.NAME.WRITE}
              width={20}
              fill={colors.icon.default}
            />
          </Button>
        );
      },
    },
    {
      title: "",

      align: "center",
      width: 30,
      render: (obj) => {
        const type =
          obj.status_name === "Khởi tạo"
            ? DefineKeyEvent.event_initialization
            : obj.status_name === "Đang chạy"
            ? DefineKeyEvent.event_running
            : DefineKeyEvent.event_end;
        return (
          <Button
            type="link"
            className="cursor scale"
            onClick={(e) => {
              e.stopPropagation();
              history.push("/detailEventPage/" + type + "/" + obj.id);
            }}
          >
            <ISvg
              name={ISvg.NAME.CHEVRONRIGHT}
              width={12}
              fill={colors.icon.default}
            />
          </Button>
        );
      },
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Danh sách Chương trình tích lũy
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo tên , mã chương trình">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo tên , mã chương trình"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilter({ ...filter, key: e.target.value, page: 1 });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={18}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Lọc danh sách"
                    level={4}
                    style={{
                      marginLeft: 25,
                      marginRight: 25,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filter.start_date}
                      to={filter.end_date}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          start_date: date[0]._d.setHours(0, 0, 0),
                          end_date: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>
                  <div style={{ margin: "0px 15px 0px 15px" }}>
                    <ISelect
                      defaultValue="Trạng thái"
                      data={status}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          status: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{ width: 200 }}>
                    <ISelect
                      defaultValue="Loại chương trình"
                      data={type}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          type: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>
              <Col span={6}>
                <Row gutter={[15, 0]}>
                  <div className="flex justify-end">
                    <IButton
                      icon={ISvg.NAME.ARROWUP}
                      title={"Tạo mới"}
                      // style={{ marginRight: 12 }}
                      styleHeight={{
                        width: 120,
                      }}
                      color={colors.main}
                      onClick={() => {
                        dispatch(CLEAR_REDUX());
                        history.push("/eventCreate/create/0");
                      }}
                    />

                    {/* <IButton
                      icon={ISvg.NAME.DOWLOAND}
                      title={'Xuất file'}
                      color={colors.main}
                      styleHeight={{
                        width: 120
                      }}
                      onClick={() => {
                        message.warning('Chức năng đang cập nhật')
                      }}
                    /> */}
                  </div>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={data.listEvent}
                style={{ width: "100%" }}
                defaultCurrent={1}
                sizeItem={data.size}
                indexPage={filter.page}
                maxpage={data.total}
                size="small"
                scroll={{ x: 0 }}
                loading={loadingTable}
                onRow={(record, rowIndex) => {
                  return {
                    onClick: (event) => {
                      const indexRow = Number(
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey
                      );
                      const type =
                        data.listEvent[indexRow].status_name === "Khởi tạo"
                          ? DefineKeyEvent.event_initialization
                          : data.listEvent[indexRow].status_name === "Đang chạy"
                          ? DefineKeyEvent.event_running
                          : DefineKeyEvent.event_end;

                      const id = data.listEvent[indexRow].id;
                      history.push("/detailEventPage/" + type + "/" + id);
                    }, // click row
                  };
                }}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilter({ ...filter, page: page });
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default ListEventPage;
