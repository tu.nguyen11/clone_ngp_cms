import { Col, Modal, Row, InputNumber, Button } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import { APIService } from "../../../../services";
import { DefineKeyEvent } from "../../../../utils/DefineKey";
import { IButton, ISvg } from "../../../components";
import {
  IError,
  StyledInputNumber,
  StyledISelect,
  TagP,
  useModalTreeProduct,
} from "../../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../components/common/Font/font";
import {
  ADD_DESGIN_EVENT,
  CLEAR_REDUX,
  CREATE_DATE_EVENT,
  REMOVE_DESGIN_EVENT,
  UPDATE_DESGIN_EVENT,
  UPDATE_DESGIN_EVENT_DISCOUNT,
  UPDATE_KEY_CATE,
  CHECK_VALIDATE,
  UPDATE_TREE_KEYDEFAULT,
  UPDATE_TREE_PRIORITY,
  CREATE_DATE_EVENT_FILTER,
  UPDATE_KEY_REWARD,
  UPDATE_DATA_PRODUCT,
} from "../../../store/reducers";

export default function CreateEventDesgin(props) {
  const [isVisibleTree, setVisibleTree] = useState(false);
  const params = useParams();
  const { id } = params;
  const history = useHistory();
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { event, treeProduct } = dataRoot;
  const [dataDrowpCate, setDataDrowpCate] = useState([]);
  const dataRenderItem =
    event.key_filter_promotion === DefineKeyEvent.filter_sales
      ? event.event_degsin.reward_sales
      : event.event_degsin.reward_count;

  const renderItemSales = (item, index) => {
    return (
      <Col span={24}>
        <Row gutter={[24, 12]}>
          <Col span={13}>
            <span>Từ :</span>
            <StyledInputNumber
              className="disabled-up"
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              disabled={
                event.type_event_status === DefineKeyEvent.event_running
              }
              value={item.money_min}
              min={
                index === 0
                  ? 0
                  : event.event_degsin.reward_sales[index - 1].money_max
              }
              onChange={(value) => {
                const data = {
                  key: "reward_sales",
                  keyData: "money_min",
                  idx: index,
                  valueData: value,
                };
                if (value) {
                  const data1 = {
                    key: "reward_sales",
                    validate: "valid_quantity_min",
                    idx: index,
                    check: false,
                  };
                  dispatch(CHECK_VALIDATE(data1));
                }
                dispatch(UPDATE_DESGIN_EVENT(data));
              }}
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              style={{
                margin: "0px 12px",
                borderRadius: 0,
              }}
            />
            <span>đ</span>
            <span style={{ margin: "0px 12px" }}>-</span>
            <span>Đến</span>
            <StyledInputNumber
              className="disabled-up"
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              min={item.money_min}
              onChange={(value) => {
                //check item thứ 2 có hay không
                dataRenderItem.map((item1, index1) => {
                  if (index1 == index + 1) {
                    const data2 = {
                      key: "reward_sales",
                      keyData: "money_min",
                      idx: index1,
                      valueData: value,
                    };
                    dispatch(UPDATE_DESGIN_EVENT(data2));
                    const data3 = {
                      key: "reward_sales",
                      validate: "valid_quantity_min",
                      idx: index1,
                      check: false,
                    };
                    dispatch(CHECK_VALIDATE(data3));
                  }
                });

                if (value) {
                  const data1 = {
                    key: "reward_sales",
                    validate: "valid_quantity_max",
                    idx: index,
                    check: false,
                  };
                  dispatch(CHECK_VALIDATE(data1));
                }
                const data = {
                  key: "reward_sales",
                  keyData: "money_max",
                  idx: index,
                  valueData: value,
                };

                dispatch(UPDATE_DESGIN_EVENT(data));
              }}
              disabled={
                event.type_event_status === DefineKeyEvent.event_running
              }
              value={item.money_max}
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              style={{
                margin: "0px 12px",
                borderRadius: 0,
                border:
                  !item.money_max && item.valid_quantity_max
                    ? "1px solid red"
                    : "",
              }}
            />
            <span>đ</span>
          </Col>
          <Col span={9}>
            <span>Tỉ lệ chiết khấu :</span>
            <StyledInputNumber
              className="disabled-up"
              disabled={
                event.type_event_status === DefineKeyEvent.event_running
              }
              width="75px"
              min={0}
              defaultValue={item.discountShow}
              onChange={(value) => {
                const data = {
                  key: "reward_sales",
                  keyData: "discount",
                  keyDataShow: "discountShow",
                  idx: index,
                  valueData: value,
                  type: "sales",
                };
                if (value) {
                  const data1 = {
                    key: "reward_sales",
                    validate: "valid_discount",
                    idx: index,
                    check: false,
                  };
                  dispatch(CHECK_VALIDATE(data1));
                }
                dispatch(UPDATE_DESGIN_EVENT_DISCOUNT(data));
              }}
              style={{
                margin: "0px 12px",
                borderRadius: 0,
                border: item.valid_discount ? "1px solid red" : "",
              }}
            />
            <span>%</span>
          </Col>
          {index === 0 ||
          event.type_event_status === DefineKeyEvent.event_running ? null : (
            <Col span={2}>
              <div
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  background: "rgba(227, 95, 75, 0.1)",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: 5,
                }}
                className="cursor"
                onClick={() => {
                  const data = {
                    key: "reward_sales",
                    idx: index,
                  };
                  dispatch(REMOVE_DESGIN_EVENT(data));
                }}
              >
                <ISvg
                  name={ISvg.NAME.CROSS}
                  width={8}
                  height={8}
                  fill={colors.oranges}
                />
              </div>
            </Col>
          )}
        </Row>
      </Col>
    );
  };

  const treeProctCount = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: 0,
                }}
              >
                <li
                  style={{
                    fontWeight: 500,
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding: "6px 0px",
                    color: "black",
                  }}
                >
                  {item.name}
                </li>
                {treeProctCount(item.children, index)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[12, 0]} type="flex">
                    <Col span={3}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          justifyContent: "center",
                          height: "100%",
                        }}
                      >
                        {event.type_event_status ===
                        DefineKeyEvent.event_running ? (
                          <div
                            style={{
                              width: 16,
                              height: 16,
                              borderRadius: 8,
                              border:
                                treeProduct.keyDefaultProduct === item.id
                                  ? `4px solid ${colors.main}`
                                  : "unset",
                              marginRight: 12,
                              transition: "all 0.2s ease-in-out 0s",
                            }}
                            className="cursor"
                          />
                        ) : (
                          <div
                            style={{
                              width: 16,
                              height: 16,
                              borderRadius: 8,
                              border:
                                treeProduct.keyDefaultProduct === item.id
                                  ? `4px solid ${colors.main}`
                                  : "1px solid rgba(34, 35, 43, 0.5)",
                              marginRight: 12,
                              transition: "all 0.2s ease-in-out 0s",
                            }}
                            onClick={() => {
                              if (item.priority > 1) {
                                Modal.error({
                                  title: "Thông báo",
                                  content: "Tỷ lệ quy đổi phải bằng 1.",
                                });
                                return;
                              }
                              const data = {
                                keyRoot: "treeProduct",
                                key: "keyDefaultProduct",
                                valuePayload: item.id,
                              };
                              dispatch(UPDATE_TREE_KEYDEFAULT(data));
                            }}
                            className="cursor"
                          />
                        )}

                        <span>{item.value}</span>
                      </div>
                    </Col>
                    <Col span={15}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          display: "flex",
                          alignItems: "center",
                          height: "100%",
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    <Col span={6}>
                      <InputNumber
                        style={{
                          height: 32,
                          borderRadius: 0,
                          width: "100%",
                        }}
                        max={99}
                        disabled={
                          treeProduct.keyDefaultProduct === item.id ||
                          event.type_event_status ===
                            DefineKeyEvent.event_running
                        }
                        className="disabled-up"
                        value={item.priority}
                        onChange={(value) => {
                          const data = {
                            keyRoot: "treeProduct",
                            key: "listConfirm",
                            idxChildren: index,
                            idxRoot: idx,
                            keyPriority: "priority",
                            valuePayload: value,
                          };
                          const dataProductPriority = {
                            keyRoot: "dataProduct",
                            keyObject: "children",
                            keyValue: "priority",
                            keyLocationRoot: item.locationRootReal,
                            value: value,
                            idx: item.locationChildrenReal,
                          };

                          dispatch(UPDATE_TREE_PRIORITY(data));
                          dispatch(UPDATE_DATA_PRODUCT(dataProductPriority));
                        }}
                        min={1}
                      />
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  const treeProctSales = (dataProduct, idx) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: 0,
                }}
              >
                <li
                  style={{
                    fontWeight: 500,
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding: "6px 0px",
                    color: "black",
                  }}
                >
                  {item.name}
                </li>
                {treeProctSales(item.children, index)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[12, 0]} type="flex">
                    <Col span={24}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          display: "flex",
                          alignItems: "center",
                          height: "100%",
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  const renderItemCount = (item, index) => {
    return (
      <Col span={24}>
        <Row gutter={[12, 12]}>
          <Col span={14}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <span>Từ :</span>
              <StyledInputNumber
                className="disabled-up"
                disabled={
                  event.type_event_status === DefineKeyEvent.event_running
                }
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                min={
                  index === 0
                    ? 0
                    : event.event_degsin.reward_count[index - 1].quantity_max
                }
                value={item.quantity_min}
                onChange={(value) => {
                  const data = {
                    key: "reward_count",
                    keyData: "quantity_min",
                    idx: index,
                    valueData: value,
                  };
                  if (value) {
                    const data1 = {
                      key: "reward_count",
                      validate: "valid_quantity_min",
                      idx: index,
                      check: false,
                    };
                    dispatch(CHECK_VALIDATE(data1));
                  }
                  dispatch(UPDATE_DESGIN_EVENT(data));
                }}
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                style={{
                  margin: "0px 12px",
                  borderRadius: 0,
                }}
              />
              <span style={{ margin: "0px 12px" }}>-</span>
              <span>Đến dưới:</span>
              <StyledInputNumber
                className="disabled-up"
                disabled={
                  event.type_event_status === DefineKeyEvent.event_running
                }
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                min={item.quantity_min}
                value={item.quantity_max}
                onChange={(value) => {
                  // bắt điều kiện của item1 và item2
                  dataRenderItem.map((item1, index1) => {
                    if (index1 == index + 1) {
                      const data2 = {
                        key: "reward_count",
                        keyData: "quantity_min",
                        idx: index1,
                        valueData: value,
                      };
                      dispatch(UPDATE_DESGIN_EVENT(data2));
                      const data3 = {
                        key: "reward_count",
                        validate: "valid_quantity_min",
                        idx: index1,
                        check: false,
                      };
                      dispatch(CHECK_VALIDATE(data3));
                    }
                  });
                  if (value) {
                    const data1 = {
                      key: "reward_count",
                      validate: "valid_quantity_max",
                      idx: index,
                      check: false,
                    };
                    dispatch(CHECK_VALIDATE(data1));
                  }
                  const data = {
                    key: "reward_count",
                    keyData: "quantity_max",
                    idx: index,
                    valueData: value,
                  };

                  dispatch(UPDATE_DESGIN_EVENT(data));
                }}
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                style={{
                  margin: "0px 12px",
                  borderRadius: 0,
                  border: item.valid_quantity_max ? "1px solid red" : "",
                }}
              />
            </div>
          </Col>
          <Col span={9}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <span>Mức chiết khấu :</span>
              <StyledInputNumber
                className="disabled-up"
                disabled={
                  event.type_event_status === DefineKeyEvent.event_running
                }
                width="75px"
                min={0}
                onChange={(value) => {
                  const data = {
                    key: "reward_count",
                    keyData: "discount",
                    keyDataShow: "discountShow",
                    idx: index,
                    valueData: value,
                    type: "count",
                    type_count: "sales",
                  };
                  const data_count = {
                    key: "reward_count",
                    keyData: "discount",
                    keyDataShow: "discountShow",
                    idx: index,
                    valueData: value,
                    type: "count",
                    type_count: "count",
                  };

                  if (value) {
                    const data1 = {
                      key: "reward_count",
                      validate: "valid_discount",
                      idx: index,
                      check: false,
                    };
                    dispatch(CHECK_VALIDATE(data1));
                  }
                  dispatch(
                    UPDATE_DESGIN_EVENT_DISCOUNT(
                      event.key_discount === 1 ? data : data_count
                    )
                  );
                }}
                defaultValue={item.discountShow}
                style={{
                  margin: "0px 6px 0px 12px",
                  borderRadius: 0,
                  border: item.valid_discount ? "1px solid red" : "",
                }}
              />
              {event.key_discount === 2 ? (
                <>
                  <span>đ/</span>
                  <div style={{ width: 42, lineHeight: 0.9 }}>
                    <span
                      style={{
                        fontSize: "clamp(4px, 4vw, 8px)",
                      }}
                    >
                      Quy cách tích lũy
                    </span>
                  </div>
                </>
              ) : (
                <>
                  <span>%/</span>
                  <div style={{ width: 42, lineHeight: 0.9 }}>
                    <span
                      style={{
                        fontSize: "clamp(4px, 4vw, 8px)",
                      }}
                    >
                      Doanh số tích lũy
                    </span>
                  </div>
                </>
              )}
            </div>
          </Col>
          {index === 0 ||
          event.type_event_status === DefineKeyEvent.event_running ? null : (
            <Col span={1}>
              <div
                style={{
                  borderRadius: 10,
                  background: "rgba(227, 95, 75, 0.1)",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: 5,
                  width: 20,
                  height: 20,
                }}
                className="cursor"
                onClick={() => {
                  const data = {
                    key: "reward_count",
                    idx: index,
                  };
                  dispatch(REMOVE_DESGIN_EVENT(data));
                }}
              >
                <ISvg
                  name={ISvg.NAME.CROSS}
                  width={8}
                  height={8}
                  fill={colors.oranges}
                />
              </div>
            </Col>
          )}
        </Row>
      </Col>
    );
  };

  const _fetchGetProductGetCateByCompany = async () => {
    try {
      const data = await APIService._getProductGetCateByCompany();
      let dataNew = data.cate.map((item) => {
        const key = item.id;
        const value = item.company_name;
        return { key, value };
      });
      setDataDrowpCate(dataNew);
    } catch (error) {
      console.log(error);
    }
  };
  const checkValidate = (data, type) => {
    let valid = true;

    if (event.key_filter_promotion === DefineKeyEvent.filter_sales) {
      data.map((item, index) => {
        if (!item.money_max) {
          const data = {
            key: type,
            validate: "valid_quantity_max",
            idx: index,
            check: true,
          };
          dispatch(CHECK_VALIDATE(data));
          valid = false;
        }
        if (!item.discountShow) {
          const data = {
            key: type,
            validate: "valid_discount",
            idx: index,
            check: true,
          };
          dispatch(CHECK_VALIDATE(data));
          valid = false;
        }
      });
    } else {
      data.map((item, index) => {
        if (!item.quantity_max) {
          const data = {
            key: type,
            validate: "valid_quantity_max",
            idx: index,
            check: true,
          };
          dispatch(CHECK_VALIDATE(data));
          valid = false;
        }
        if (!item.discountShow) {
          const data = {
            key: type,
            validate: "valid_discount",
            idx: index,
            check: true,
          };
          dispatch(CHECK_VALIDATE(data));
          valid = false;
        }
      });
    }
    return valid;
  };
  useEffect(() => {
    _fetchGetProductGetCateByCompany();
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Tạo chương trình tích lũy mới
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX());
                    history.push("/listEventPage");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1200 }}>
            <Row gutter={[24, 24]}>
              <Col span={8}>
                <div
                  style={{ padding: 24, height: 650, overflow: "auto" }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nghành hàng áp dụng
                      </StyledITileHeading>
                    </Col>
                    <Col span={24} style={{ paddingTop: 2 }}>
                      <StyledISelect
                        marginLeft="0px"
                        isBorderBottom="1px solid #d9d9d9"
                        value={event.event_degsin.key_cate}
                        disabled={
                          event.type_event_status ===
                          DefineKeyEvent.event_running
                        }
                        data={dataDrowpCate}
                        onChange={(key) => {
                          dispatch(UPDATE_KEY_CATE(key));
                        }}
                      />
                    </Col>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Danh mục tích lũy
                      </StyledITileHeading>
                    </Col>
                    {event.type_event_status ===
                    DefineKeyEvent.event_running ? null : (
                      <Col span={24} style={{ paddingBottom: 0 }}>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "flex-end",
                          }}
                        >
                          <span style={{ opacity: 0 }}>Từ :</span>
                          <span
                            style={{
                              margin: "0px 12px",
                              color: colors.main,
                              fontWeight: 500,
                            }}
                          >
                            Thêm sản phẩm
                          </span>
                          <div
                            className="cursor"
                            onClick={() => setVisibleTree(true)}
                          >
                            <ISvg
                              name={ISvg.NAME.ADDUPLOAD}
                              width={20}
                              height={20}
                              fill={colors.main}
                            />
                          </div>
                        </div>
                      </Col>
                    )}

                    {treeProduct.listConfirm.length === 0 ? null : (
                      <Col span={24}>
                        <Row>
                          <Col
                            span={24}
                            style={{
                              textAlign: "right",
                              borderBottom: "0.5px dashed #7A7B7B",
                              paddingBottom: 6,
                            }}
                          >
                            <span
                              style={{
                                fontWeight: 600,
                              }}
                            >
                              Tỉ lệ quy đổi
                            </span>
                          </Col>
                          <Col span={24}>
                            {event.key_filter_promotion ===
                            DefineKeyEvent.filter_sales
                              ? treeProctSales(treeProduct.listConfirm)
                              : treeProctCount(treeProduct.listConfirm)}
                          </Col>
                        </Row>
                      </Col>
                    )}
                  </Row>
                </div>
              </Col>
              <Col span={16}>
                <div
                  style={{ padding: 24, height: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[24, 24]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Điều kiện tích lũy & khuyến mãi
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[24, 0]}>
                        <Col span={12}>
                          <StyledISelect
                            marginLeft="0px"
                            isBorderBottom="1px solid #d9d9d9"
                            value={event.key_filter_promotion}
                            disabled={
                              event.type_event_status ===
                              DefineKeyEvent.event_running
                            }
                            data={[
                              {
                                value: "Doanh số sản phẩm",
                                key: DefineKeyEvent.filter_sales,
                              },
                              {
                                value: "Số lượng sản phẩm",
                                key: DefineKeyEvent.filter_money,
                              },
                            ]}
                            onChange={(key) => {
                              const data = {
                                key: "key_filter_promotion",
                                value: key,
                              };
                              dispatch(CREATE_DATE_EVENT(data));
                              const data_key_condition_select = {
                                key: "key_condition_select",
                                value: false,
                              };
                              dispatch(
                                CREATE_DATE_EVENT_FILTER(
                                  data_key_condition_select
                                )
                              );
                            }}
                          />
                        </Col>
                      </Row>
                    </Col>
                    {event.key_filter_promotion ===
                    DefineKeyEvent.filter_money ? (
                      <Col span={12}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Quy cách tích lũy
                        </StyledITileHeading>
                        <StyledISelect
                          marginLeft="0px"
                          isBorderBottom="1px solid #d9d9d9"
                          value={event.key_specifications}
                          disabled={
                            event.type_event_status ===
                            DefineKeyEvent.event_running
                          }
                          data={[
                            {
                              value: "Quy cách nhỏ",
                              key: 2,
                            },
                            {
                              value: "Quy cách lớn",
                              key: 1,
                            },
                          ]}
                          onChange={(key) => {
                            const data = {
                              key: "key_specifications",
                              value: key,
                            };
                            dispatch(CREATE_DATE_EVENT(data));
                          }}
                        />
                      </Col>
                    ) : null}

                    {event.key_filter_promotion ===
                    DefineKeyEvent.filter_money ? (
                      <Col span={12}>
                        <StyledITileHeading minFont="10px" maxFont="16px">
                          Thưởng chiết khấu
                        </StyledITileHeading>
                        <StyledISelect
                          marginLeft="0px"
                          isBorderBottom="1px solid #d9d9d9"
                          value={event.key_discount}
                          disabled={
                            event.type_event_status ===
                            DefineKeyEvent.event_running
                          }
                          data={[
                            {
                              value: "%/ Doanh số tích lũy",
                              key: 1,
                            },
                            {
                              value: "đ/ Quy cách tích lũy",
                              key: 2,
                            },
                          ]}
                          onChange={(key) => {
                            const data = {
                              key: "key_discount",
                              value: key,
                            };
                            dispatch(CREATE_DATE_EVENT(data));
                            dispatch(UPDATE_KEY_REWARD(key));
                          }}
                        />
                      </Col>
                    ) : null}
                    <Col span={24}>
                      <div
                        style={{
                          border: "1px solid #d9d9d9",
                          padding: "24px 12px",
                        }}
                      >
                        <Row gutter={[12, 12]}>
                          <Col span={24}>
                            <div style={{ marginBottom: 24 }}>
                              <TagP style={{ fontWeight: 500 }}>
                                {event.key_filter_promotion === 1
                                  ? "Doanh số sản phẩm"
                                  : "Số lượng sản phẩm"}
                              </TagP>
                            </div>
                          </Col>
                          {dataRenderItem.map((item, index) => {
                            return event.key_filter_promotion ===
                              DefineKeyEvent.filter_sales
                              ? renderItemSales(item, index)
                              : renderItemCount(item, index);
                          })}

                          <Col span={24}>
                            <Row gutter={[24, 12]}>
                              {event.type_event_status ===
                              DefineKeyEvent.event_running ? null : (
                                <Col span={14}>
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <span style={{ opacity: 0 }}>Từ :</span>
                                    <span
                                      style={{
                                        margin: "0px 12px",
                                        color: colors.main,
                                        fontWeight: 500,
                                      }}
                                    >
                                      Thêm hạng mức
                                    </span>
                                    <div
                                      className="cursor"
                                      onClick={() => {
                                        const dataSell = {
                                          key: "reward_sales",
                                          data: {
                                            money_min:
                                              event.event_degsin.reward_sales[
                                                event.event_degsin.reward_sales
                                                  .length - 1
                                              ].money_max,
                                            money_max:
                                              event.event_degsin.reward_sales[
                                                event.event_degsin.reward_sales
                                                  .length - 1
                                              ].money_max,
                                            quantity_max: 0,
                                            quantity_min: 0,
                                            event_reward_type: 0,
                                            discountShow:
                                              event.event_degsin.reward_sales[
                                                event.event_degsin.reward_sales
                                                  .length - 1
                                              ].discountShow,
                                            discount:
                                              event.event_degsin.reward_sales[
                                                event.event_degsin.reward_sales
                                                  .length - 1
                                              ].discount,
                                          },
                                        };

                                        const dataCount = {
                                          key: "reward_count",
                                          data: {
                                            money_min: 0,
                                            money_max: 0,
                                            quantity_max:
                                              event.event_degsin.reward_count[
                                                event.event_degsin.reward_count
                                                  .length - 1
                                              ].quantity_max,
                                            quantity_min:
                                              event.event_degsin.reward_count[
                                                event.event_degsin.reward_count
                                                  .length - 1
                                              ].quantity_max,
                                            event_reward_type:
                                              event.key_discount,
                                            discountShow:
                                              event.event_degsin.reward_count[
                                                event.event_degsin.reward_count
                                                  .length - 1
                                              ].discountShow,
                                            discount:
                                              event.event_degsin.reward_count[
                                                event.event_degsin.reward_count
                                                  .length - 1
                                              ].discount,
                                          },
                                        };

                                        dispatch(
                                          ADD_DESGIN_EVENT(
                                            event.key_filter_promotion ===
                                              DefineKeyEvent.filter_sales
                                              ? dataSell
                                              : dataCount
                                          )
                                        );
                                      }}
                                    >
                                      <ISvg
                                        name={ISvg.NAME.ADDUPLOAD}
                                        width={20}
                                        height={20}
                                        fill={colors.main}
                                      />
                                    </div>
                                  </div>
                                </Col>
                              )}
                            </Row>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={() => history.goBack()}
                  title="Quay lại"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWTHINLEFT}
                  style={{ marginRight: 12 }}
                  styleHeight={{
                    width: 140,
                  }}
                />
                <IButton
                  title="Tiếp tục"
                  onClick={() => {
                    if (treeProduct.listConfirm.length === 0) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>sản phẩm.</span>
                        </span>
                      );
                      return IError(content);
                    }
                    if (
                      !checkValidate(
                        event.key_filter_promotion ===
                          DefineKeyEvent.filter_sales
                          ? event.event_degsin.reward_sales
                          : event.event_degsin.reward_count,
                        event.key_filter_promotion ===
                          DefineKeyEvent.filter_sales
                          ? "reward_sales"
                          : "reward_count"
                      )
                    ) {
                      return IError(
                        "Vui lòng nhập thông tin Điều kiện tích lũy & khuyến mãi"
                      );
                    }
                    history.push(`/eventCreate/description/create/${id}`);
                  }}
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isVisibleTree}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalTreeProduct(() => {
          setVisibleTree(false);
        }, event.event_degsin.key_cate)}
      </Modal>
    </div>
  );
}
