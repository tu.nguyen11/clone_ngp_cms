import { Col, Row, Skeleton, message } from "antd";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../assets";
import { priceFormat } from "../../../../utils";
import { IButton, ISvg, ITableHtml, ITitle } from "../../../components";
import { TagP } from "../../../components/common";
import {
  StyledITileHeading,
  StyledITitle,
} from "../../../components/common/Font/font";
import "../../../../assets/img/logo.png";
import FormatterDay from "../../../../utils/FormatterDay";
import { APIService } from "../../../../services";
import { ASSIGNED_DATA_REDUX_DETAIL } from "../../../store/reducers";
import { useDispatch } from "react-redux";

export default function DetailSalesPolicyEventGiftPage() {
  const params = useParams();
  const dispatch = useDispatch();
  const { id } = params;
  const [isLoadingAPI, setLoadingAPI] = useState(true);
  const history = useHistory();
  const [urlIMG, setUrlIMG] = useState("");

  const [dataEventGift, setDataEventGift] = useState({
    list_cate_type: [],
    list_condition_product: [],
    list_user_join: [],
    lst_city: [],
    lst_membership: [],
    lst_region: [],
  });

  const statusClickHistory = () => {
    history.push(`/event/eventGift/historyStatisticalEventGiftPage/${id}`);
  };

  const statusViewSatistic = () => {
    history.push(`/event/eventGift/statisticalEventGiftPage/${id}`);
  };

  const _fetchDetailSalesPolicyEvent = async (id) => {
    try {
      const dataEvent = await APIService._getDetailSalesPolicyEvent(id);
      setDataEventGift(dataEvent.event_response);
      setUrlIMG(dataEvent.image_url);
      setLoadingAPI(false);
    } catch {
      setLoadingAPI(false);
    }
  };

  const postActiveOrStopEvent = async (obj) => {
    try {
      const data = await APIService._postActiveOrStopEvent(obj);
      message.success(
        obj.type === 1
          ? "CSBH bạn vừa chọn sẽ được kích hoạt vào lúc 0:00 ngày hôm sau"
          : "Dừng chương trình thành công"
      );
      _fetchDetailSalesPolicyEvent(id);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    _fetchDetailSalesPolicyEvent(id);
  }, []);

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp đại lý",
      align: "center",
    },
  ];

  let headerTable = (dataHeader) => {
    return (
      <tr className="scroll">
        {dataHeader.map((item, index) => (
          <th className="th-table" style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (dataBody) => {
    return dataBody.map((item) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {item.user_agency_name}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {item.user_agency_code}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "center", fontWeight: "normal", width: 80 }}
        >
          {item.level}
        </td>
      </tr>
    ));
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Chi tiết Chính sách bán hàng
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <IButton
              title={"Kích hoạt CSBH"}
              color={colors.main}
              styleHeight={{ width: 140 }}
              onClick={() => {
                const obj = {
                  event_id_list: [Number(id)],
                  type: 1,
                };
                postActiveOrStopEvent(obj);
              }}
              style={{
                display: dataEventGift.status === 0 ? "block" : "none",
                marginRight: 5,
              }}
            />

            <IButton
              icon={ISvg.NAME.WRITE}
              title={"Chỉnh sửa"}
              color={colors.main}
              styleHeight={{ width: 140 }}
              style={{
                display:
                  dataEventGift.status === 0 || dataEventGift.status === 1
                    ? "block"
                    : "none",
                marginLeft: 5,
              }}
              onClick={() => {
                const data = { ...dataEventGift, image_url: urlIMG };
                dispatch(ASSIGNED_DATA_REDUX_DETAIL(data));
                history.push("/event/gift/edit/" + id);
              }}
            />

            <IButton
              title={"Lịch sử cập nhật"}
              color={colors.main}
              styleHeight={{ width: 140 }}
              onClick={statusClickHistory}
              style={{
                display:
                  dataEventGift.status === 1 || dataEventGift.status === 2
                    ? "block"
                    : "none",
                marginLeft: 5,
              }}
            />

            <IButton
              title={"Xem thống kê"}
              color={colors.main}
              styleHeight={{ width: 140 }}
              onClick={statusViewSatistic}
              style={{
                display:
                  dataEventGift.status === 1 || dataEventGift.status === 2
                    ? "block"
                    : "none",
                marginLeft: 5,
              }}
            />

            <IButton
              title={"Dừng CSBH"}
              color={colors.red}
              styleHeight={{ width: 140 }}
              style={{
                display: dataEventGift.status === 1 ? "block" : "none",
                marginLeft: 5,
              }}
              onClick={() => {
                const obj = {
                  event_id_list: [Number(id)],
                  type: 2,
                };
                postActiveOrStopEvent(obj);
              }}
            />
          </div>
        </Col>
        <Col span={24}>
          <div>
            <Row gutter={[24, 0]} type="flex">
              <Col span={17}>
                <Row>
                  <Col span={24}>
                    <div
                      style={{
                        padding: "24px 30px",
                        background: colors.white,
                        minHeight: "500px",
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                      }}
                    >
                      <Skeleton
                        active
                        loading={isLoadingAPI}
                        paragraph={{ rows: 16 }}
                      >
                        <Row gutter={[10, 10]}>
                          <Col span={12}>
                            <Row gutter={[0, 15]}>
                              <Col span={24}>
                                <StyledITileHeading
                                  minFont="10px"
                                  maxFont="16px"
                                >
                                  Mô tả & nguyên tắc CSBH
                                </StyledITileHeading>
                              </Col>
                              <Col span={24}>
                                <Row gutter={[20, 0]}>
                                  <Col span={14}>
                                    <p style={{ fontWeight: 500 }}>Mã CSBH</p>
                                    <span style={{ wordBreak: "break-all" }}>
                                      {!dataEventGift.code
                                        ? "-"
                                        : dataEventGift.code}
                                    </span>
                                  </Col>
                                  <Col span={10}>
                                    <p style={{ fontWeight: 500 }}>
                                      Trạng thái
                                    </p>
                                    <span>
                                      {!dataEventGift.status_name
                                        ? "-"
                                        : dataEventGift.status_name}
                                    </span>
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={24}>
                                <p style={{ fontWeight: 500 }}>
                                  Tên CSBH
                                  {dataEventGift.is_show_name !== 0 ? null : (
                                    <span
                                      style={{
                                        marginLeft: 8,
                                        color: "#7c7c7d",
                                      }}
                                    >
                                      (Ẩn tên CSBH)
                                    </span>
                                  )}
                                </p>
                                <span style={{ wordBreak: "break-all" }}>
                                  {!dataEventGift.name
                                    ? "-"
                                    : dataEventGift.name}
                                </span>
                              </Col>
                              <Col span={24}>
                                <div style={{ marginTop: 20 }}>
                                  <StyledITileHeading
                                    minFont="10px"
                                    maxFont="16px"
                                  >
                                    Thời gian đề xuất áp dụng CSBH
                                  </StyledITileHeading>
                                </div>
                              </Col>
                              <Col span={24}>
                                <p style={{ fontWeight: 500, padding: 0 }}>
                                  Bắt đầu
                                </p>
                                <span>
                                  {!dataEventGift.suggest_date
                                    ? "-"
                                    : FormatterDay.dateFormatWithString(
                                        dataEventGift.suggest_date,
                                        "#DD#/#MM#/#YYYY# - #hhh#:#mm#"
                                      )}
                                </span>
                              </Col>
                              <Col span={24}>
                                <div style={{ marginTop: 20 }}>
                                  <StyledITileHeading
                                    minFont="10px"
                                    maxFont="16px"
                                  >
                                    Thời gian áp dụng CSBH
                                  </StyledITileHeading>
                                </div>
                              </Col>
                              <Col span={24}>
                                <Row gutter={[20, 0]}>
                                  <Col span={14}>
                                    <p style={{ fontWeight: 500 }}>Bắt đầu</p>
                                    <span>
                                      {" "}
                                      {!dataEventGift.start_date ||
                                      dataEventGift.start_date <= 0
                                        ? "-"
                                        : FormatterDay.dateFormatWithString(
                                            dataEventGift.start_date,
                                            "#DD#/#MM#/#YYYY# - #hhh#:#mm#"
                                          )}{" "}
                                    </span>
                                  </Col>
                                  <Col span={10}>
                                    <p style={{ fontWeight: 500 }}>Kết thúc</p>
                                    <span>
                                      {!dataEventGift.end_date ||
                                      dataEventGift.end_date <= 0
                                        ? "-"
                                        : FormatterDay.dateFormatWithString(
                                            dataEventGift.end_date,
                                            "#DD#/#MM#/#YYYY# - #hhh#:#mm#"
                                          )}{" "}
                                    </span>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={12}>
                            <Row gutter={[0, 15]}>
                              <Col span={24}>
                                <StyledITileHeading
                                  minFont="10px"
                                  maxFont="16px"
                                >
                                  Đối tượng áp dụng
                                </StyledITileHeading>
                              </Col>
                              {dataEventGift.type_join === 3 ? (
                                <>
                                  <Col span={24}>
                                    <ITitle
                                      title="Điều kiện tham gia 1"
                                      style={{
                                        marginBottom: 5,
                                        paddingLeft: 10,
                                        fontWeight: 500,
                                      }}
                                    />
                                    <div
                                      style={{
                                        padding: 6,
                                        border:
                                          "1px solid rgba(122, 123, 123, 0.5)",
                                      }}
                                    >
                                      <div
                                        style={{
                                          padding: "10px 0px",
                                          borderBottom:
                                            "1px solid rgba(122, 123, 123, 0.5)",
                                        }}
                                      >
                                        <TagP
                                          style={{
                                            paddingBottom: 6,
                                          }}
                                        >
                                          Cấp người dùng
                                        </TagP>
                                      </div>
                                      <div
                                        style={{
                                          padding: "6px 6px 0px 10px",
                                        }}
                                      >
                                        <span>
                                          {!dataEventGift.level
                                            ? "-"
                                            : dataEventGift.level}
                                        </span>
                                      </div>
                                    </div>
                                  </Col>
                                  <Col span={24}>
                                    <ITitle
                                      title="Điều kiện tham gia 2"
                                      style={{
                                        marginTop: 10,
                                        marginBottom: 5,
                                        paddingLeft: 10,
                                        fontWeight: 500,
                                      }}
                                    />
                                    <div
                                      style={{
                                        padding: 6,
                                        border:
                                          "1px solid rgba(122, 123, 123, 0.5)",
                                      }}
                                    >
                                      <div
                                        style={{
                                          padding: "10px 0px",
                                          borderBottom:
                                            "1px solid rgba(122, 123, 123, 0.5)",
                                        }}
                                      >
                                        <TagP
                                          style={{
                                            paddingBottom: 6,
                                          }}
                                        >
                                          Cấp bậc
                                        </TagP>
                                      </div>
                                      <div
                                        style={{
                                          padding: "6px 6px 0px 10px",
                                        }}
                                      >
                                        <span>
                                          {!dataEventGift.lst_membership
                                            ? "-"
                                            : dataEventGift.lst_membership.map(
                                                (item, index) =>
                                                  `${item.name}${
                                                    index ===
                                                    dataEventGift.lst_membership
                                                      .length -
                                                      1
                                                      ? ""
                                                      : ", "
                                                  }`
                                              )}
                                        </span>
                                      </div>
                                    </div>
                                  </Col>
                                  <Col span={24}>
                                    <ITitle
                                      title="Điều kiện tham gia 3"
                                      style={{
                                        marginTop: 10,
                                        marginBottom: 5,
                                        paddingLeft: 10,
                                        fontWeight: 500,
                                      }}
                                    />
                                    <div
                                      style={{
                                        padding: 6,
                                        border:
                                          "1px solid rgba(122, 123, 123, 0.5)",
                                      }}
                                    >
                                      <div
                                        style={{
                                          padding: "10px 0px",
                                          borderBottom:
                                            "1px solid rgba(122, 123, 123, 0.5)",
                                        }}
                                      >
                                        <TagP
                                          style={{
                                            paddingBottom: 6,
                                          }}
                                        >
                                          Vùng địa lý
                                        </TagP>
                                      </div>
                                      <div
                                        style={{
                                          padding: "6px 6px 0px 10px",
                                          overflow: "auto",
                                          maxHeight: 200,
                                        }}
                                      >
                                        <span>
                                          {!dataEventGift.lst_region
                                            ? "-"
                                            : dataEventGift.lst_region.map(
                                                (item, index) =>
                                                  `${item.name}${
                                                    index ===
                                                    dataEventGift.lst_region
                                                      .length -
                                                      1
                                                      ? ""
                                                      : ", "
                                                  }`
                                              )}
                                        </span>
                                      </div>
                                    </div>
                                  </Col>
                                  <Col span={24}>
                                    <ITitle
                                      title="Điều kiện tham gia 4"
                                      style={{
                                        marginTop: 10,
                                        marginBottom: 5,
                                        paddingLeft: 10,
                                        fontWeight: 500,
                                      }}
                                    />
                                    <div
                                      style={{
                                        padding: 6,
                                        border:
                                          "1px solid rgba(122, 123, 123, 0.5)",
                                      }}
                                    >
                                      <div
                                        style={{
                                          padding: "10px 0px",
                                          borderBottom:
                                            "1px solid rgba(122, 123, 123, 0.5)",
                                        }}
                                      >
                                        <TagP
                                          style={{
                                            paddingBottom: 6,
                                          }}
                                        >
                                          Tỉnh/ Thành phố
                                        </TagP>
                                      </div>
                                      <div
                                        style={{
                                          padding: "6px 6px 0px 10px",
                                          overflow: "auto",
                                          maxHeight: 200,
                                        }}
                                      >
                                        <span>
                                          {!dataEventGift.lst_city
                                            ? "-"
                                            : dataEventGift.lst_city.map(
                                                (item, index) =>
                                                  `${item.name}${
                                                    index ===
                                                    dataEventGift.lst_city
                                                      .length -
                                                      1
                                                      ? ""
                                                      : ", "
                                                  }`
                                              )}
                                        </span>
                                      </div>
                                    </div>
                                  </Col>
                                </>
                              ) : (
                                <div style={{ marginTop: 25 }}>
                                  <ITableHtml
                                    height="420px"
                                    childrenBody={bodyTable(
                                      dataEventGift.list_user_join
                                    )}
                                    childrenHeader={headerTable(dataHeader)}
                                    style={{
                                      borderLeft:
                                        "1px solid rgba(122, 123, 123, 0.5)",
                                      borderRight:
                                        "1px solid rgba(122, 123, 123, 0.5)",
                                      borderBottom:
                                        "1px solid rgba(122, 123, 123, 0.5)",
                                    }}
                                    isBorder={false}
                                  />
                                </div>
                              )}
                            </Row>
                          </Col>
                        </Row>
                      </Skeleton>
                    </div>
                  </Col>

                  <Col span={24}>
                    <div
                      style={{
                        padding: "24px 30px",
                        background: colors.white,
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      }}
                    >
                      <Skeleton
                        active
                        loading={isLoadingAPI}
                        paragraph={{ rows: 16 }}
                      >
                        <Row gutter={[10, 20]}>
                          <Col span={12}>
                            <StyledITileHeading minFont="10px" maxFont="16px">
                              Loại sản phẩm
                            </StyledITileHeading>
                            <div
                              style={{
                                marginTop: 5,
                              }}
                            >
                              <span
                                style={{
                                  marginBottom: 10,
                                  marginTop: 5,
                                }}
                              >
                                {dataEventGift.group_id === 1
                                  ? "Máy móc"
                                  : dataEventGift.group_id === 2
                                  ? "Phụ tùng"
                                  : "-"}
                              </span>
                            </div>
                          </Col>
                          <Col span={6}>
                            <StyledITileHeading minFont="10px" maxFont="16px">
                              Điều kiện nhận thưởng
                            </StyledITileHeading>
                            <div
                              style={{
                                marginTop: 5,
                              }}
                            >
                              <span>
                                {dataEventGift.type_condition_apply === 2
                                  ? "Doanh số sản phẩm"
                                  : dataEventGift.type_condition_apply === 1
                                  ? "Số lượng sản phẩm"
                                  : "-"}
                              </span>
                            </div>
                          </Col>
                          <Col span={6}>
                            <StyledITileHeading minFont="10px" maxFont="16px">
                              Hình thức khuyến mãi
                            </StyledITileHeading>
                            <div
                              style={{
                                marginTop: 5,
                              }}
                            >
                              <span>
                                {dataEventGift.type_condition_apply === 2
                                  ? "Chiết khấu"
                                  : dataEventGift.type_condition_apply === 1
                                  ? "Số lượng"
                                  : "-"}
                              </span>
                            </div>
                          </Col>
                        </Row>
                        <Row>
                          {dataEventGift.list_cate_type.map((item, index) => (
                            <div>
                              <Col span={24}>
                                <div style={{ marginTop: 10 }}>
                                  <Row>
                                    {item.group < 10 ? (
                                      <span style={{ fontWeight: "bold" }}>
                                        {"NHÓM HÀNG 0"}
                                        {item.group !== 0 ? item.group : "-"}
                                      </span>
                                    ) : (
                                      <span style={{ fontWeight: "bold" }}>
                                        {"NHÓM HÀNG "}
                                        {item.group !== 0 ? item.group : "-"}
                                      </span>
                                    )}
                                  </Row>
                                </div>
                              </Col>
                              <Col span={24}>
                                <div
                                  style={{
                                    border:
                                      "1px solid rgba(122, 123, 123, 0.5)",
                                    height: "100%",
                                  }}
                                >
                                  <Row type="flex">
                                    <Col span={12}>
                                      <div
                                        style={{
                                          borderRight:
                                            "1px solid rgba(122, 123, 123, 0.5)",
                                          height: "100%",
                                        }}
                                      >
                                        <div
                                          style={{
                                            borderBottom:
                                              "1px dashed rgba(122, 123, 123, 0.5)",
                                            padding: "20px 0px",
                                            marginLeft: 16,
                                            marginRight: 16,
                                          }}
                                        >
                                          <TagP
                                            style={{
                                              padding: 0,
                                              fontWeight: "bold",
                                            }}
                                          >
                                            Danh mục tích lũy
                                          </TagP>
                                        </div>
                                        <div
                                          style={{
                                            overflow: "auto",
                                            maxHeight: 400,
                                          }}
                                        >
                                          {item.event_group_product.map(
                                            (itemCate, indexCate) => (
                                              <div>
                                                <Col>
                                                  <div
                                                    style={{
                                                      borderBottom:
                                                        "1px dashed rgba(122, 123, 123, 0.5)",
                                                      padding: "5px 10px",
                                                      fontWeight: "bold",
                                                      color: colors.blackChart,
                                                      marginLeft: 16,
                                                      marginRight: 16,
                                                    }}
                                                  >
                                                    <span
                                                      style={{
                                                        fontWeight: "bold",
                                                      }}
                                                    >
                                                      {!itemCate.name
                                                        ? "-"
                                                        : itemCate.name.toUpperCase()}
                                                    </span>
                                                    {/* Dung cu pin */}
                                                  </div>
                                                </Col>
                                                {itemCate.children.map(
                                                  (itemComp, indexComp) => (
                                                    <div>
                                                      <Col>
                                                        <div
                                                          style={{
                                                            borderBottom:
                                                              "1px dashed rgba(122, 123, 123, 0.5)",
                                                            marginLeft: 16,
                                                            marginRight: 16,
                                                            padding: "5px 10px",
                                                          }}
                                                        >
                                                          <div
                                                            style={{
                                                              marginLeft: 10,
                                                              fontWeight:
                                                                "bold",
                                                              fontSize: 14,
                                                              color:
                                                                colors.blackChart,
                                                            }}
                                                          >
                                                            {!itemComp.name
                                                              ? "-"
                                                              : itemComp.name}
                                                            {/* Dau bom cao ap */}
                                                          </div>
                                                        </div>
                                                      </Col>
                                                      <div>
                                                        {itemComp.children.map(
                                                          (
                                                            itemPro,
                                                            indexPro
                                                          ) => (
                                                            <div
                                                              style={{
                                                                borderBottom:
                                                                  indexPro ===
                                                                  itemComp
                                                                    .children
                                                                    .length -
                                                                    1
                                                                    ? "none"
                                                                    : "1px dashed rgba(122, 123, 123, 0.5)",
                                                                padding:
                                                                  "5px 5px",
                                                                marginLeft: 16,
                                                                marginRight: 16,
                                                              }}
                                                            >
                                                              <div
                                                                style={{
                                                                  marginLeft: 30,
                                                                  fontSize: 15,
                                                                  color:
                                                                    colors.blackChart,
                                                                }}
                                                              >
                                                                {!itemPro.name
                                                                  ? "-"
                                                                  : itemPro.name}
                                                                {/* Dau bom huyndai */}
                                                              </div>
                                                              {!itemPro.description ? null : (
                                                                <div
                                                                  style={{
                                                                    marginLeft: 30,
                                                                    fontSize: 14,
                                                                    // color:
                                                                    //   colors.blackChart,
                                                                    fontStyle:
                                                                      "italic",
                                                                  }}
                                                                >
                                                                  {
                                                                    itemPro.description
                                                                  }
                                                                </div>
                                                              )}
                                                            </div>
                                                          )
                                                        )}
                                                      </div>
                                                    </div>
                                                  )
                                                )}
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    </Col>

                                    <Col span={12}>
                                      <div
                                        style={{
                                          padding: "20px 0px",
                                          marginLeft: 16,
                                          marginRight: 16,
                                        }}
                                      >
                                        <TagP
                                          style={{
                                            padding: 0,
                                            fontWeight: "bold",
                                          }}
                                        >
                                          Điều kiện tích lũy & khuyến mãi
                                        </TagP>
                                      </div>
                                      <div
                                        style={{
                                          maxHeight: 409,
                                          overflow: "auto",
                                        }}
                                      >
                                        {dataEventGift.list_condition_product.map(
                                          (itemEvent, indexEvent) =>
                                            itemEvent.group_id ===
                                            item.group ? (
                                              dataEventGift.type_condition_apply ===
                                              1 ? (
                                                <Row gutter={[12, 0]}>
                                                  <div
                                                    style={{
                                                      paddingLeft: 15,
                                                      margin:
                                                        "0px 25px 0px 0px",
                                                      minWidth: 600,
                                                    }}
                                                  >
                                                    <Col span={24}>
                                                      <div
                                                        style={{ fontSize: 14 }}
                                                      >
                                                        <Row gutter={[0, 15]}>
                                                          <Col span={6}>
                                                            {itemEvent.han_muc <
                                                            10 ? (
                                                              <span
                                                                style={{
                                                                  display:
                                                                    "flex",
                                                                }}
                                                              >
                                                                {"Mức 0"}
                                                                {
                                                                  itemEvent.han_muc
                                                                }
                                                              </span>
                                                            ) : (
                                                              <span
                                                                style={{
                                                                  display:
                                                                    "flex",
                                                                }}
                                                              >
                                                                {"Mức "}
                                                                {
                                                                  itemEvent.han_muc
                                                                }
                                                              </span>
                                                            )}
                                                          </Col>
                                                          <Col
                                                            span={
                                                              dataEventGift.group_id ===
                                                              2
                                                                ? 10
                                                                : 6
                                                            }
                                                          >
                                                            <span>
                                                              {"Đến: "}
                                                              {priceFormat(
                                                                itemEvent.condition_min
                                                              )}
                                                            </span>
                                                          </Col>
                                                          <Col
                                                            span={
                                                              dataEventGift.group_id ===
                                                              2
                                                                ? 8
                                                                : 6
                                                            }
                                                          >
                                                            <span>
                                                              {"Tặng: "}
                                                              {
                                                                itemEvent.discount
                                                              }
                                                            </span>
                                                          </Col>
                                                          {dataEventGift.group_id ===
                                                          2 ? null : (
                                                            <Col span={6}>
                                                              <span>
                                                                {
                                                                  " Quy đổi tỷ lệ: "
                                                                }
                                                                {
                                                                  itemEvent.quy_doi_ty_le
                                                                }
                                                                {" % "}
                                                              </span>
                                                            </Col>
                                                          )}
                                                        </Row>
                                                      </div>
                                                    </Col>
                                                  </div>
                                                </Row>
                                              ) : (
                                                <Row gutter={[12, 0]}>
                                                  <div
                                                    style={{
                                                      paddingLeft: 15,
                                                      margin:
                                                        "0px 25px 0px 0px",
                                                      minWidth: 400,
                                                    }}
                                                  >
                                                    <Col span={24}>
                                                      <div
                                                        style={{ fontSize: 14 }}
                                                      >
                                                        <Row gutter={[0, 15]}>
                                                          <Col span={4}>
                                                            {itemEvent.han_muc <
                                                            10 ? (
                                                              <span
                                                                style={{
                                                                  display:
                                                                    "flex",
                                                                }}
                                                              >
                                                                {"Mức 0"}
                                                                {
                                                                  itemEvent.han_muc
                                                                }
                                                              </span>
                                                            ) : (
                                                              <span
                                                                style={{
                                                                  display:
                                                                    "flex",
                                                                }}
                                                              >
                                                                {"Mức "}
                                                                {
                                                                  itemEvent.han_muc
                                                                }
                                                              </span>
                                                            )}
                                                          </Col>
                                                          <Col span={10}>
                                                            <span
                                                              style={{
                                                                display: "flex",
                                                                justifyContent:
                                                                  "center",
                                                              }}
                                                            >
                                                              {"Đến: "}
                                                              {priceFormat(
                                                                itemEvent.condition_min
                                                              )}
                                                            </span>
                                                          </Col>
                                                          <Col span={10}>
                                                            <span
                                                              style={{
                                                                display: "flex",
                                                                justifyContent:
                                                                  "center",
                                                              }}
                                                            >
                                                              {"Chiết khấu: "}
                                                              {
                                                                itemEvent.discount
                                                              }
                                                              {" % "}
                                                            </span>
                                                          </Col>
                                                        </Row>
                                                      </div>
                                                    </Col>
                                                  </div>
                                                </Row>
                                              )
                                            ) : (
                                              ""
                                            )
                                        )}
                                      </div>
                                    </Col>
                                  </Row>
                                </div>
                              </Col>
                            </div>
                          ))}
                        </Row>
                      </Skeleton>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={7}>
                <div
                  style={{
                    padding: "24px 30px",
                    background: colors.white,
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                    minHeight: "500px",
                  }}
                >
                  <Skeleton
                    active
                    loading={isLoadingAPI}
                    paragraph={{ rows: 16 }}
                  >
                    <Row>
                      {!dataEventGift.image ||
                      dataEventGift.image === "" ? null : (
                        <>
                          <Col span={24}>
                            <StyledITileHeading minFont="10px" maxFont="18px">
                              Hình ảnh
                            </StyledITileHeading>
                          </Col>
                          <Col span={24}>
                            <img
                              src={urlIMG + dataEventGift.image}
                              alt="hình-event"
                              style={{
                                marginTop: 10,
                                width: "100%",
                                height: "100%",
                                objectFit: "cover",
                                justifyContent: "center",
                              }}
                            />
                          </Col>
                        </>
                      )}

                      <Col span={24}>
                        <div
                          style={{
                            marginTop:
                              !dataEventGift.image || dataEventGift.image === ""
                                ? 0
                                : 20,
                          }}
                        >
                          <StyledITileHeading minFont="10px" maxFont="18px">
                            Nội dung mô tả
                          </StyledITileHeading>
                        </div>
                      </Col>
                      <Col span={24}>
                        <div
                          style={{
                            overflow: "auto",
                            maxHeight: 700,
                            maxWidth: "100%",
                            wordBreak: "break-word",
                            color: colors.blackChart,
                            textAlign: "left",
                            marginTop: 10,
                          }}
                        >
                          {!dataEventGift.description
                            ? "-"
                            : dataEventGift.description}
                        </div>
                      </Col>
                    </Row>
                  </Skeleton>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}
