import { Col, Row, Form, message, Skeleton, Switch } from "antd";
import { CloseOutlined, CheckOutlined } from "@ant-design/icons";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../../assets";
import { APIService } from "../../../../../services";
import { IButton, ISvg } from "../../../../components";
import {
  IInputText,
  IUploadHook,
  IFormItem,
  IInputTextArea,
  TagP,
} from "../../../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../../components/common/Font/font";
import {
  CLEAR_REDUX_EVENT_GIFT,
  CREATE_INFO_EVENT_GIFT,
} from "../../../../store/reducers";
import styled from "styled-components";

const ISwitch = styled(Switch)`
  .ant-switch-inner {
    line-height: 12px !important;
  }
`;

function CreateEventGiftDescription(props) {
  const history = useHistory();
  const params = useParams();
  const { id, type } = params;
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const {
    eventGift,
    treeProductEventGiftGroup,
    treeProductEventGiftGroup2,
    modalS1,
    modalS2,
  } = dataRoot;
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [loading, setLoading] = useState(true);

  const postCreateEventGift = async (obj) => {
    try {
      const data = await APIService._postCreateEventGift(obj);
      dispatch(CLEAR_REDUX_EVENT_GIFT());
      message.success(
        type === "create"
          ? "Tạo chương trình thành công"
          : "Chỉnh sửa chương trình thành công"
      );
      setLoadingSubmit(false);
      history.push("/event/gift/list");
    } catch (error) {
      setLoadingSubmit(false);
      console.log(error);
    }
  };

  useEffect(() => {
    reduxForm.setFieldsValue(
      {
        content_event: eventGift.content_event,
        code_event: eventGift.code_event,
        name_event: eventGift.name_event,
        img: eventGift.img_event,
      },
      () => setLoading(false)
    );
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setLoadingSubmit(true);

        const dataIDS1 = modalS1.listS1Show.map((item) => {
          return item.id;
        });

        let arrProduct = [];
        let reward = [];

        if (eventGift.event_design.key_type_product === 1) {
          treeProductEventGiftGroup.listArrListTreeConfirm.forEach(
            (itemGroup, indexGroup) => {
              itemGroup.treeProduct.forEach((itemTree, indexTree) => {
                itemTree.children.forEach((itemLv2) => {
                  itemLv2.children.forEach((itemLv3) => {
                    arrProduct.push({
                      product_id: itemLv3.id,
                      group_product: indexGroup + 1,
                      description: itemLv3.description,
                    });
                  });
                });
              });
            }
          );
        } else {
          treeProductEventGiftGroup2.listArrListTreeConfirm.forEach(
            (itemGroup, indexGroup) => {
              itemGroup.treeProduct.forEach((itemTree, indexTree) => {
                itemTree.children.forEach((itemLv2) => {
                  itemLv2.children.forEach((itemLv3) => {
                    arrProduct.push({
                      product_id: itemLv3.id,
                      group_product: indexGroup + 1,
                      description: itemLv3.description,
                    });
                  });
                });
              });
            }
          );
        }

        if (eventGift.conditions_apply === 2) {
          eventGift.event_design.reward.forEach((itemLevel, indexLevel) => {
            itemLevel.forEach((itemGroup, indexGroup) => {
              reward.push({
                condition_max:
                  indexLevel === eventGift.event_design.reward.length - 1
                    ? 0
                    : eventGift.event_design.reward[indexLevel + 1][indexGroup]
                        .number_to - 1,
                condition_min: itemGroup.number_to,
                discount: itemGroup.discount,
                type: eventGift.conditions_apply === 2 ? 1 : 2,
                group_product: indexGroup + 1,
                location: indexLevel + 1,
              });
            });
          });
        } else {
          eventGift.event_design.rewardQuantity.forEach(
            (itemLevel, indexLevel) => {
              itemLevel.forEach((itemGroup, indexGroup) => {
                reward.push({
                  condition_max:
                    indexLevel ===
                    eventGift.event_design.rewardQuantity.length - 1
                      ? 0
                      : eventGift.event_design.rewardQuantity[indexLevel + 1][
                          indexGroup
                        ].number_to - 1,
                  condition_min: itemGroup.number_to,
                  discount: itemGroup.discount,
                  type: eventGift.conditions_apply === 2 ? 1 : 2,
                  group_product: indexGroup + 1,
                  location: indexLevel + 1,
                });
              });
            }
          );
        }

        console.log("reward: ", reward);

        const dataAdd = {
          condition_join: [
            {
              cities_id: eventGift.event_filter.arrayCity,
              level_user:
                eventGift.type_way === 1 ? eventGift.event_filter.id_agency : 1,
              membership_id: eventGift.event_filter.membership,
              user_id: eventGift.type_way === 1 ? [0] : dataIDS1,
            },
          ],

          description: values.content_event,
          end_date: 0,
          event_code: values.code_event,
          event_name: values.name_event,
          is_show_event_name: eventGift.show_name_event === true ? 1 : 0,
          event_parent_id: 0,
          group_id: eventGift.event_design.key_type_product,
          image: values.img,
          product: arrProduct,
          reward: reward,
          start_date: 0,
          status: 0,
          suggest_date: eventGift.recommended_time,
          type_condition_apply: eventGift.conditions_apply,
          type_increment: 0,
          type_join:
            eventGift.type_way === 1 ? 3 : eventGift.type_way === 2 ? 1 : 2,
          type_priority: 0,
          type_reward:
            eventGift.event_design.key_type_product === 2
              ? 3
              : eventGift.conditions_apply === 2
              ? 1
              : 2,
          update_event_id: 0,
          user_level:
            eventGift.type_way === 1 ? eventGift.event_filter.id_agency : 1,
        };

        console.log("dataAdd: ", dataAdd);

        const dataUpdate = {
          condition_join: [
            {
              cities_id: eventGift.event_filter.arrayCity,

              membership_id: eventGift.event_filter.membership,
              user_id: eventGift.type_way === 1 ? [0] : dataIDS1,
            },
          ],
          user_level:
            eventGift.type_way === 1 ? eventGift.event_filter.id_agency : 1,
          description: values.content_event,
          end_date: 0,
          event_code: values.code_event,
          event_name: values.name_event,
          is_show_event_name: eventGift.show_name_event === true ? 1 : 0,
          event_parent_id: Number(id),
          group_id: eventGift.event_design.key_type_product,
          image: values.img,
          product: arrProduct,
          reward: reward,
          start_date: 0,
          status: eventGift.type_event_status,
          suggest_date: eventGift.recommended_time,
          type_condition_apply: eventGift.conditions_apply,
          type_increment: 0,
          type_join:
            eventGift.type_way === 1 ? 3 : eventGift.type_way === 2 ? 1 : 2,
          type_priority: 0,
          type_reward: eventGift.conditions_apply === 2 ? 1 : 2,
          update_event_id: Number(id),
        };
        console.log("dataUpdate: ", dataUpdate);
        postCreateEventGift(type === "create" ? dataAdd : dataUpdate);
      }
    });
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 25px 0px" }}>
      <Form onSubmit={(event) => handleSubmit(event)}>
        <Row gutter={[0, 24]} type="flex" style={{ height: "100%" }}>
          <Col span={24}>
            <div>
              <StyledITitle style={{ color: colors.main }}>
                {type === "create"
                  ? "Tạo Chính sách bán hàng tặng hàng"
                  : "Chỉnh sửa Chính sách bán hàng tặng quà"}
              </StyledITitle>
            </div>
          </Col>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Hủy"
                    color={colors.oranges}
                    icon={ISvg.NAME.CROSS}
                    styleHeight={{ width: 140 }}
                    onClick={() => {
                      dispatch(CLEAR_REDUX_EVENT_GIFT());
                      history.push("/event/gift/list");
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <div style={{ width: 815, height: "100%" }}>
              <Row gutter={[12, 24]} type="flex" style={{ height: "100%" }}>
                <Col span={15}>
                  <div
                    style={{ padding: 24, height: "100%" }}
                    className="box-shadow"
                  >
                    <Skeleton loading={loading} active paragraph={{ rows: 12 }}>
                      <Row gutter={[0, 24]}>
                        <Col>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Thông tin chương trình
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <div style={{ marginTop: 16 }}>
                                <TagP style={{ fontWeight: 500 }}>
                                  Mã chương trình
                                  <span style={{ color: "red", marginLeft: 4 }}>
                                    *
                                  </span>
                                </TagP>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("code_event", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "nhập mã chương trình",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    placeholder="nhập mã chương trình"
                                    disabled={type === "edit" ? true : false}
                                    onChange={(e) => {
                                      const data = {
                                        key: "code_event",
                                        value: e.target.value,
                                      };

                                      dispatch(CREATE_INFO_EVENT_GIFT(data));
                                    }}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 6]}>
                            <Col span={24}>
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                }}
                              >
                                <div
                                  style={{
                                    display: "flex",
                                  }}
                                >
                                  <p style={{ fontWeight: 500 }}>
                                    Tên chương trình
                                  </p>
                                  <span style={{ color: "red", marginLeft: 4 }}>
                                    *
                                  </span>
                                </div>
                                <div>
                                  <ISwitch
                                    checked={eventGift.show_name_event}
                                    checkedChildren={<CheckOutlined />}
                                    unCheckedChildren={<CloseOutlined />}
                                    onChange={(checked) => {
                                      console.log(checked);
                                      const dataAction = {
                                        key: "show_name_event",
                                        value: checked,
                                      };
                                      dispatch(
                                        CREATE_INFO_EVENT_GIFT(dataAction)
                                      );
                                    }}
                                  />
                                </div>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("name_event", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "nhập tên chương trình",
                                    },
                                  ],
                                })(
                                  <IInputText
                                    placeholder="nhập tên chương trình"
                                    value={reduxForm.getFieldValue(
                                      "name_event"
                                    )}
                                    onChange={(e) => {
                                      const data = {
                                        key: "name_event",
                                        value: e.target.value,
                                      };
                                      dispatch(CREATE_INFO_EVENT_GIFT(data));
                                    }}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <div>
                                <TagP style={{ fontWeight: 500 }}>
                                  Nội dung
                                </TagP>
                              </div>
                            </Col>
                            <Col span={24}>
                              <IFormItem>
                                {getFieldDecorator("content_event", {
                                  rules: [
                                    {
                                      required: false,
                                      message:
                                        "nhập nội dung và thể lệ chương trình",
                                    },
                                  ],
                                })(
                                  <IInputTextArea
                                    placeholder="nhập nội dung và thể lệ chương trình"
                                    style={{
                                      height: 300,
                                      marginTop: 12,
                                    }}
                                    onChange={(e) => {
                                      const data = {
                                        key: "content_event",
                                        value: e.target.value,
                                      };
                                      dispatch(CREATE_INFO_EVENT_GIFT(data));
                                    }}
                                  />
                                )}
                              </IFormItem>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Skeleton>
                  </div>
                </Col>
                <Col span={9}>
                  <div
                    style={{ padding: 24, height: "100%" }}
                    className="box-shadow"
                  >
                    <Skeleton loading={loading} active paragraph={{ rows: 12 }}>
                      <Row gutter={[0, 24]}>
                        <Col>
                          <StyledITileHeading minFont="10px" maxFont="16px">
                            Hình ảnh
                          </StyledITileHeading>
                        </Col>
                        <Col span={24}>
                          <IFormItem>
                            {getFieldDecorator("img", {
                              rules: [
                                {
                                  required: false,
                                  message: "chọn hình",
                                },
                              ],
                            })(
                              <IUploadHook
                                callback={(nameFile, image_url) => {
                                  reduxForm.setFieldsValue({
                                    img: nameFile,
                                  });

                                  const data = {
                                    key: "img_event",
                                    value: nameFile,
                                  };
                                  dispatch(CREATE_INFO_EVENT_GIFT(data));
                                  const dataUrl_img = {
                                    key: "URl_img",
                                    value: image_url,
                                  };
                                  dispatch(CREATE_INFO_EVENT_GIFT(dataUrl_img));
                                }}
                                removeCallback={() => {
                                  reduxForm.setFieldsValue({
                                    img: "",
                                  });
                                  const data = {
                                    key: "img_event",
                                    value: "",
                                  };
                                  dispatch(CREATE_INFO_EVENT_GIFT(data));
                                  const dataUrl_img = {
                                    key: "URl_img",
                                    value: "",
                                  };
                                  dispatch(CREATE_INFO_EVENT_GIFT(dataUrl_img));
                                }}
                                dataProps={
                                  eventGift.ISedit
                                    ? [
                                        {
                                          uid: -1,
                                          name: reduxForm.getFieldValue("img"),
                                          status: "done",
                                          url:
                                            eventGift.URl_img +
                                            reduxForm.getFieldValue("img"),
                                        },
                                      ]
                                    : eventGift.img_event === ""
                                    ? []
                                    : [
                                        {
                                          uid: -1,
                                          name: eventGift.img_event,
                                          status: "done",
                                          url:
                                            eventGift.URl_img +
                                            eventGift.img_event,
                                        },
                                      ]
                                }
                                width="100%"
                                height="235px"
                              />
                            )}
                          </IFormItem>
                        </Col>
                      </Row>
                    </Skeleton>
                  </div>
                </Col>
              </Row>
              <Col span={24}>
                <div style={{ display: "flex", justifyContent: "flex-end" }}>
                  <IButton
                    onClick={() => history.goBack()}
                    title="Quay lại"
                    color={colors.main}
                    icon={ISvg.NAME.ARROWTHINLEFT}
                    style={{ marginRight: 12 }}
                    styleHeight={{
                      width: 140,
                    }}
                  />
                  <IButton
                    title={
                      type === "edit" ? "Lưu chương trình" : "Tạo chương trình"
                    }
                    color={colors.main}
                    htmlType="submit"
                    styleHeight={{ width: 180 }}
                    loading={loadingSubmit}
                  />
                </div>
              </Col>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const createEventGiftDescription = Form.create({
  name: "CreateEventGiftDescription",
})(CreateEventGiftDescription);

export default createEventGiftDescription;
