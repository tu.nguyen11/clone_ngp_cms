import { Col, Modal, Row, message } from "antd";
import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../../assets";
import { APIService } from "../../../../../services";
import { DefineKeyEvent } from "../../../../../utils/DefineKey";
import { IButton, ISvg } from "../../../../components";
import {
  IError,
  StyledInputNumber,
  StyledISelect,
  IInputText,
  IInputTextArea,
} from "../../../../components/common";
import useModalTreeProductEventGiftGroup from "../../../../components/common/ModalAgency/useModalTreeProductEventGiftGroup";
import useModalTreeProductEventGiftGroup2 from "../../../../components/common/ModalAgency/useModalTreeProductEventGiftGroup2";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../../components/common/Font/font";
import {
  CLEAR_REDUX_EVENT_GIFT,
  CREATE_INFO_EVENT_GIFT,
  UPDATE_DESIGN_EVENT_GIFT,
  UPDATE_KEY_CATE_EVENT_GIFT,
  CHECK_VALIDATE_EVENT_GIFT,
  UPDATE_ID_COUNT_SAVE_ITEM,
  ASSIGNED_TREE_CLICK_GROUP,
  ADD_DESIGN_EVENT_LEVEL_GROUP,
  REMOVE_PRODUCT_GROUP,
  REMOVE_LEVEL,
  CHANGE_CONDITION_APPLY,
  ADD_CONTENT_PRODUCT,
} from "../../../../store/reducers";

export default function CreateEventGiftDesignCondition(props) {
  const typingTimeoutRef = useRef(null);
  const [isVisibleTree, setVisibleTree] = useState(false);
  const [isVisibleTree2, setVisibleTree2] = useState(false);
  const params = useParams();
  const { id, type } = params;
  const history = useHistory();
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { eventGift, treeProductEventGiftGroup, treeProductEventGiftGroup2 } =
    dataRoot;
  const [typeProduct, setTypeProduct] = useState([]);
  const [contentProduct, setContentProduct] = useState("");

  const renderItemGroup = (arr, index) => {
    return (
      <Col span={24}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            border: "1px solid rgba(122, 123, 123, 0.5)",
            padding: "15px 15px",
            width: "100%",
            marginBottom: 15,
          }}
        >
          <Row style={{ width: "100%" }}>
            {eventGift.conditions_apply === 2 ? (
              <Col span={24}>
                <Row style={{ marginBottom: 15 }} gutter={[40, 0]}>
                  <Col span={6}>
                    <div>
                      <span style={{ fontWeight: 600 }}>
                        Mức {index < 9 ? `0${index + 1}` : index + 1}
                      </span>
                    </div>
                  </Col>
                  <Col span={10}>
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <span style={{ fontWeight: 600 }}>
                        Doanh số nhóm sản phẩm
                      </span>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <span style={{ fontWeight: 600 }}>Chiết khấu</span>
                    </div>
                  </Col>
                  <Col span={2}>
                    {index === 0 ? null : (
                      <div
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "flex-end",
                          opacity: index === 0 ? 0 : "",
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            dispatch(REMOVE_LEVEL(index));
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill="#E35F4B"
                          />
                        </div>
                      </div>
                    )}
                  </Col>
                </Row>
              </Col>
            ) : (
              <Col span={24}>
                <Row style={{ marginBottom: 15 }} gutter={[40, 0]}>
                  <Col span={6}>
                    <div>
                      <span style={{ fontWeight: 600 }}>Mức 0{index + 1}</span>
                    </div>
                  </Col>
                  <Col
                    span={
                      eventGift.event_design.key_type_product === 2 ? 10 : 8
                    }
                  >
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <span style={{ fontWeight: 600 }}>SL nhóm sản phẩm</span>
                    </div>
                  </Col>
                  <Col
                    span={eventGift.event_design.key_type_product === 2 ? 6 : 4}
                  >
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <span style={{ fontWeight: 600 }}>Tặng</span>
                    </div>
                  </Col>
                  {eventGift.event_design.key_type_product === 2 ? null : (
                    <Col span={4}>
                      <div
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        <span style={{ fontWeight: 600 }}>Quy đổi tỷ lệ</span>
                      </div>
                    </Col>
                  )}
                  <Col span={2}>
                    {index === 0 ? null : (
                      <div
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "flex-end",
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            dispatch(REMOVE_LEVEL(index));
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill="#E35F4B"
                          />
                        </div>
                      </div>
                    )}
                  </Col>
                </Row>
              </Col>
            )}
            <Col span={24}>
              {arr.map((itemProduct, indexProduct) => {
                return eventGift.conditions_apply === 2 ? (
                  <Col span={24}>
                    <Row
                      type="flex"
                      style={{
                        borderBottom: "1px dashed rgba(122, 123, 123, 0.5)",
                        paddingBottom: 10,
                        paddingTop: 10,
                      }}
                      gutter={[40, 0]}
                    >
                      <Col span={6}>
                        <div
                          style={{
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <span>{`Nhóm sản phẩm 0${indexProduct + 1}`}</span>
                        </div>
                      </Col>
                      <Col span={10}>
                        <Row gutter={[12, 0]}>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              width: "100%",
                            }}
                          >
                            <Col span={3} xxl={2}>
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "flex-start",
                                  alignItems: "center",
                                  height: 30,
                                }}
                              >
                                <span>Đến</span>
                              </div>
                            </Col>
                            <Col span={21} xxl={22}>
                              <StyledInputNumber
                                className="disabled-up"
                                formatter={(value) =>
                                  `${value}`.replace(
                                    /\B(?=(\d{3})+(?!\d))/g,
                                    ","
                                  )
                                }
                                value={itemProduct.number_to}
                                min={0}
                                onChange={(value) => {
                                  if (typingTimeoutRef.current) {
                                    clearTimeout(typingTimeoutRef.current);
                                  }

                                  typingTimeoutRef.current = setTimeout(() => {
                                    const reg = /^-?\d*[.,]?\d*$/.test(value);
                                    if (reg) {
                                      const data = {
                                        key: "reward",
                                        keyData: "number_to",
                                        idx: index,
                                        idxProduct: indexProduct,
                                        valueData: Math.floor(Number(value)),
                                      };
                                      if (value) {
                                        const data1 = {
                                          key: "reward",
                                          validate: "valid_number_to",
                                          idxRoot: index,
                                          idx: indexProduct,
                                          check: false,
                                        };
                                        dispatch(
                                          CHECK_VALIDATE_EVENT_GIFT(data1)
                                        );
                                      }
                                      dispatch(UPDATE_DESIGN_EVENT_GIFT(data));
                                    } else {
                                      const data = {
                                        key: "reward",
                                        keyData: "number_to",
                                        idx: index,
                                        idxProduct: indexProduct,
                                        valueData: 0,
                                      };
                                      dispatch(UPDATE_DESIGN_EVENT_GIFT(data));
                                    }
                                  }, 300);
                                }}
                                parser={(value) =>
                                  value.replace(/\$\s?|(,*)/g, "")
                                }
                                style={{
                                  borderRadius: 0,
                                  width: "100%",
                                  border: itemProduct.valid_number_to
                                    ? "1px solid red"
                                    : "",
                                }}
                              />
                            </Col>
                          </div>
                        </Row>
                      </Col>
                      <Col span={6}>
                        <div
                          style={{
                            width: "100%",
                            display: "flex",
                            justifyContent: "center",
                          }}
                        >
                          <StyledInputNumber
                            className="disabled-up"
                            width="75px"
                            min={0}
                            value={itemProduct.discount}
                            onChange={(value) => {
                              if (typingTimeoutRef.current) {
                                clearTimeout(typingTimeoutRef.current);
                              }

                              typingTimeoutRef.current = setTimeout(() => {
                                let valueNew = +(
                                  Math.round(value * 100) / 100
                                ).toFixed(2);
                                const data = {
                                  key: "reward",
                                  keyData: "discount",
                                  idx: index,
                                  idxProduct: indexProduct,
                                  valueData: valueNew,
                                };
                                if (value) {
                                  const data1 = {
                                    key: "reward",
                                    validate: "valid_discount",
                                    idxRoot: index,
                                    idx: indexProduct,
                                    check: false,
                                  };
                                  dispatch(CHECK_VALIDATE_EVENT_GIFT(data1));
                                }
                                dispatch(UPDATE_DESIGN_EVENT_GIFT(data));
                              }, 300);
                            }}
                            style={{
                              borderRadius: 0,
                              border: itemProduct.valid_discount
                                ? "1px solid red"
                                : "",
                              width: "100%",
                            }}
                          />
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              marginLeft: 10,
                            }}
                          >
                            <span>%</span>
                          </div>
                        </div>
                      </Col>
                      <Col span={2}></Col>
                    </Row>
                  </Col>
                ) : (
                  <Col span={24}>
                    <Row
                      type="flex"
                      style={{
                        borderBottom: "1px dashed rgba(122, 123, 123, 0.5)",
                        paddingBottom: 10,
                        paddingTop: 10,
                      }}
                      gutter={[40, 0]}
                    >
                      <Col span={6}>
                        <div
                          style={{
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <span>{`Nhóm sản phẩm 0${indexProduct + 1}`}</span>
                        </div>
                      </Col>
                      <Col
                        span={
                          eventGift.event_design.key_type_product === 2 ? 10 : 8
                        }
                      >
                        <Row gutter={[12, 0]}>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              width: "100%",
                            }}
                          >
                            <Col span={4} xxl={2}>
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "flex-start",
                                  alignItems: "center",
                                  height: 30,
                                }}
                              >
                                <span>Đến</span>
                              </div>
                            </Col>
                            <Col span={20} xxl={22}>
                              <StyledInputNumber
                                className="disabled-up"
                                formatter={(value) =>
                                  `${value}`.replace(
                                    /\B(?=(\d{3})+(?!\d))/g,
                                    ","
                                  )
                                }
                                value={itemProduct.number_to}
                                min={0}
                                onChange={(value) => {
                                  if (typingTimeoutRef.current) {
                                    clearTimeout(typingTimeoutRef.current);
                                  }

                                  typingTimeoutRef.current = setTimeout(() => {
                                    const reg = /^-?\d*[.,]?\d*$/.test(value);
                                    if (reg && value !== "") {
                                      let valueNew = Math.floor(Number(value));
                                      const data = {
                                        key: "rewardQuantity",
                                        keyData: "number_to",
                                        idx: index,
                                        idxProduct: indexProduct,
                                        valueData: valueNew,
                                      };
                                      if (value) {
                                        const data1 = {
                                          key: "rewardQuantity",
                                          validate: "valid_number_to",
                                          idxRoot: index,
                                          idx: indexProduct,
                                          check: false,
                                        };
                                        dispatch(
                                          CHECK_VALIDATE_EVENT_GIFT(data1)
                                        );
                                      }
                                      dispatch(UPDATE_DESIGN_EVENT_GIFT(data));

                                      let conversionRateNew;
                                      if (
                                        valueNew !== null &&
                                        valueNew !== undefined &&
                                        valueNew !== 0
                                      ) {
                                        let conversionRate =
                                          (itemProduct.discount / valueNew) *
                                          100;

                                        if (conversionRate % 1 !== 0) {
                                          let conversionRateString =
                                            conversionRate + "";
                                          let arrConversionRate =
                                            conversionRateString.split(".");

                                          conversionRateNew = [
                                            arrConversionRate[0],
                                            arrConversionRate[1].slice(0, 2),
                                          ].join(".");
                                        } else {
                                          conversionRateNew = conversionRate;
                                        }

                                        const dataAction = {
                                          key: "rewardQuantity",
                                          keyData: "conversion_rate",
                                          idx: index,
                                          idxProduct: indexProduct,
                                          valueData: conversionRateNew,
                                        };

                                        dispatch(
                                          UPDATE_DESIGN_EVENT_GIFT(dataAction)
                                        );
                                      }
                                    } else {
                                      const data = {
                                        key: "rewardQuantity",
                                        keyData: "number_to",
                                        idx: index,
                                        idxProduct: indexProduct,
                                        valueData: 0,
                                      };
                                      dispatch(UPDATE_DESIGN_EVENT_GIFT(data));

                                      const dataAction = {
                                        key: "rewardQuantity",
                                        keyData: "conversion_rate",
                                        idx: index,
                                        idxProduct: indexProduct,
                                        valueData: Math.floor(
                                          (0 / itemProduct.discount) * 100
                                        ),
                                      };

                                      dispatch(
                                        UPDATE_DESIGN_EVENT_GIFT(dataAction)
                                      );
                                    }
                                  }, 300);
                                }}
                                parser={(value) =>
                                  value.replace(/\$\s?|(,*)/g, "")
                                }
                                style={{
                                  borderRadius: 0,
                                  width: "100%",
                                  border: itemProduct.valid_number_to
                                    ? "1px solid red"
                                    : "",
                                }}
                              />
                            </Col>
                          </div>
                        </Row>
                      </Col>
                      <Col
                        span={
                          eventGift.event_design.key_type_product === 2 ? 6 : 4
                        }
                      >
                        <div
                          style={{
                            width: "100%",
                            display: "flex",
                            justifyContent: "center",
                          }}
                        >
                          <StyledInputNumber
                            className="disabled-up"
                            width="75px"
                            min={0}
                            value={itemProduct.discount}
                            onChange={(value) => {
                              if (typingTimeoutRef.current) {
                                clearTimeout(typingTimeoutRef.current);
                              }

                              typingTimeoutRef.current = setTimeout(() => {
                                const reg = /^-?\d*[.,]?\d*$/.test(value);

                                if (reg && value !== "") {
                                  const data = {
                                    key: "rewardQuantity",
                                    keyData: "discount",
                                    idx: index,
                                    idxProduct: indexProduct,
                                    valueData: value,
                                  };
                                  if (value) {
                                    const data1 = {
                                      key: "rewardQuantity",
                                      validate: "valid_discount",
                                      idxRoot: index,
                                      idx: indexProduct,
                                      check: false,
                                    };
                                    dispatch(CHECK_VALIDATE_EVENT_GIFT(data1));
                                  }
                                  dispatch(UPDATE_DESIGN_EVENT_GIFT(data));
                                  let valueNew = Math.floor(Number(value));
                                  let conversionRateNew;
                                  if (
                                    valueNew !== null &&
                                    valueNew !== undefined &&
                                    valueNew !== 0
                                  ) {
                                    let conversionRate =
                                      (valueNew / itemProduct.number_to) * 100;
                                    if (conversionRate % 1 !== 0) {
                                      let conversionRateString =
                                        conversionRate + "";
                                      let arrConversionRate =
                                        conversionRateString.split(".");

                                      conversionRateNew = [
                                        arrConversionRate[0],
                                        arrConversionRate[1].slice(0, 2),
                                      ].join(".");
                                    } else {
                                      conversionRateNew = conversionRate;
                                    }

                                    const dataAction = {
                                      key: "rewardQuantity",
                                      keyData: "conversion_rate",
                                      idx: index,
                                      idxProduct: indexProduct,
                                      valueData: conversionRateNew,
                                    };

                                    dispatch(
                                      UPDATE_DESIGN_EVENT_GIFT(dataAction)
                                    );
                                  }
                                } else {
                                  const data = {
                                    key: "rewardQuantity",
                                    keyData: "discount",
                                    idx: index,
                                    idxProduct: indexProduct,
                                    valueData: 0,
                                  };
                                  dispatch(UPDATE_DESIGN_EVENT_GIFT(data));

                                  const dataAction = {
                                    key: "rewardQuantity",
                                    keyData: "conversion_rate",
                                    idx: index,
                                    idxProduct: indexProduct,
                                    valueData: Math.floor(
                                      (0 / itemProduct.number_to) * 100
                                    ),
                                  };

                                  dispatch(
                                    UPDATE_DESIGN_EVENT_GIFT(dataAction)
                                  );
                                }
                              }, 300);
                            }}
                            style={{
                              borderRadius: 0,
                              border: itemProduct.valid_discount
                                ? "1px solid red"
                                : "",
                              width: "100%",
                            }}
                          />
                        </div>
                      </Col>
                      {eventGift.event_design.key_type_product === 2 ? null : (
                        <Col span={4}>
                          <div
                            style={{
                              width: "100%",
                              height: "100%",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <span>{Number(itemProduct.conversion_rate)}</span>

                            <div
                              style={{
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                                marginLeft: 10,
                              }}
                            >
                              <span>%</span>
                            </div>
                          </div>
                        </Col>
                      )}
                      <Col span={2}></Col>
                    </Row>
                  </Col>
                );
              })}
            </Col>
          </Row>
        </div>
      </Col>
    );
  };

  const treeProct = (dataProduct, idxRoot) => {
    return (
      <ul style={{ padding: 0 }}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children, idxRoot)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{ padding: "6px 0px" }}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={24}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 30,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div style={{ paddingLeft: 30 }}>
                        {/* <IInputText
                          placeholder="Mô tả chính sách bán hàng của sản phẩm"
                          // value={contentProduct}
                          value={item.content}
                          onChange={(e) => {
                            // let value = e.target.value;
                            // console.log("value: ", value);
                            // setContentProduct(value);
                            // if (typingTimeoutRef.current) {
                            //   clearTimeout(typingTimeoutRef.current);
                            // }

                            // typingTimeoutRef.current = setTimeout(() => {
                            const data = {
                              key: "content",
                              tree:
                                treeProductEventGiftGroup.listArrListTreeConfirm,
                              value: e.target.value,
                              idxRoot: idxRoot,
                              id: item.id,
                              keyTypeProduct:
                                eventGift.event_design.key_type_product,
                            };
                            dispatch(ADD_CONTENT_PRODUCT(data));
                            // }, 300);
                          }}
                        /> */}
                        <IInputTextArea
                          style={{
                            minHeight: 35,
                            height: 35,
                            padding: 6,
                            marginTop: 8,
                            border: "none",
                          }}
                          placeholder="Mô tả chính sách bán hàng của sản phẩm"
                          // value={contentProduct}
                          value={item.description}
                          onChange={(e) => {
                            // let value = e.target.value;
                            // console.log("value: ", value);
                            // setContentProduct(value);
                            // if (typingTimeoutRef.current) {
                            //   clearTimeout(typingTimeoutRef.current);
                            // }

                            // typingTimeoutRef.current = setTimeout(() => {

                            const data = {
                              key: "description",
                              tree:
                                eventGift.event_design.key_type_product === 1
                                  ? treeProductEventGiftGroup.listArrListTreeConfirm
                                  : treeProductEventGiftGroup2.listArrListTreeConfirm,
                              value: e.target.value,
                              idxRoot: idxRoot,
                              id: item.id,
                              keyTypeProduct:
                                eventGift.event_design.key_type_product,
                            };
                            dispatch(ADD_CONTENT_PRODUCT(data));
                            // }, 300);
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  const treeProductConfirmGroup = (dataProduct, idx) => {
    return eventGift.event_design.key_type_product === 1
      ? treeProductEventGiftGroup.listArrListTreeConfirm.map(
          (itemTree, idxTreeItem) => (
            <div
              style={{
                padding: "4px 12px",
                border: "1px solid #C4C4C4",
                marginBottom: 8,
              }}
            >
              <Row gutter={[12, 0]} type="flex">
                <Col
                  span={24}
                  style={{
                    borderBottom: "1px solid #C4C4C4",
                    paddingBottom: 6,
                  }}
                >
                  <Row>
                    <Col span={22}>
                      <span style={{ fontWeight: 700 }}>
                        NHÓM HÀNG{" "}
                        {idxTreeItem < 9
                          ? `0${idxTreeItem + 1}`
                          : idxTreeItem + 1}
                      </span>
                    </Col>
                    <Col span={2}>
                      {treeProductEventGiftGroup.listArrListTreeConfirm
                        .length === 1 ? null : eventGift.type_event_status ===
                        1 ? null : (
                        <div
                          style={{
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "flex-end",
                          }}
                        >
                          <div
                            style={{
                              background: "rgba(227, 95, 75, 0.1)",
                              width: 20,
                              height: 20,
                              borderRadius: 10,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              cursor: "pointer",
                            }}
                            onClick={() => {
                              const dataAction = {
                                treeProduct:
                                  treeProductEventGiftGroup
                                    .listArrListTreeConfirm[idxTreeItem]
                                    .treeProduct,
                                idxTreeItem: idxTreeItem,
                              };
                              dispatch(REMOVE_PRODUCT_GROUP(dataAction));
                            }}
                            className="cursor"
                          >
                            <ISvg
                              name={ISvg.NAME.CROSS}
                              width={8}
                              height={8}
                              fill="#E35F4B"
                            />
                          </div>
                        </div>
                      )}
                    </Col>
                  </Row>
                </Col>

                <Col span={24}>
                  <ul style={{ padding: 0 }}>
                    {itemTree.treeProduct.map((item, index) => {
                      if (item.children && item.children.length) {
                        return (
                          <ul
                            style={{
                              listStyleType: "none",
                              padding: 0,
                            }}
                          >
                            <li
                              style={{
                                fontWeight: 500,
                                borderBottom: "0.5px dashed #7A7B7B",
                                padding: "6px 0px",
                                color: "black",
                              }}
                            >
                              {item.name}
                            </li>
                            {treeProct(item.children, idxTreeItem)}
                          </ul>
                        );
                      } else {
                        return (
                          <ul
                            style={{
                              listStyleType: "none",
                              borderBottom: "0.5px dashed #7A7B7B",
                            }}
                          >
                            <li style={{ padding: "6px 0px" }}>
                              <Row gutter={[12, 0]} type="flex">
                                <Col span={24}>
                                  <div
                                    style={{
                                      wordWrap: "break-word",
                                      wordBreak: "break-word",
                                      overflow: "hidden",
                                      display: "flex",
                                      alignItems: "center",
                                      height: "100%",
                                    }}
                                  >
                                    <span>{item.name}</span>
                                  </div>
                                </Col>
                              </Row>
                            </li>
                          </ul>
                        );
                      }
                    })}
                  </ul>
                </Col>
                {eventGift.type_event_status === 1 ? null : (
                  <Col
                    span={24}
                    style={{
                      borderTop: "1px solid #C4C4C4",
                      padding: "6px 0px",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <div
                        style={{ display: "flex" }}
                        className="cursor"
                        onClick={() => {
                          const data = {
                            key: "id_count_save_item",
                            value: idxTreeItem,
                          };
                          dispatch(UPDATE_ID_COUNT_SAVE_ITEM(data));
                          dispatch(
                            ASSIGNED_TREE_CLICK_GROUP(
                              treeProductEventGiftGroup.listArrListTreeConfirm[
                                idxTreeItem
                              ].treeProduct
                            )
                          );
                          setVisibleTree(true);
                        }}
                      >
                        <span
                          style={{
                            margin: "0px 12px",
                            color: colors.main,
                            fontWeight: 500,
                          }}
                        >
                          Thêm phiên bản
                        </span>
                        <div>
                          <ISvg
                            name={ISvg.NAME.ADDUPLOAD}
                            width={20}
                            height={20}
                            fill={colors.main}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                )}
              </Row>
            </div>
          )
        )
      : treeProductEventGiftGroup2.listArrListTreeConfirm.map(
          (itemTree, idxTreeItem) => (
            <div
              style={{
                padding: "4px 12px",
                border: "1px solid #C4C4C4",
                marginBottom: 8,
              }}
            >
              <Row gutter={[12, 0]} type="flex">
                <Col
                  span={24}
                  style={{
                    borderBottom: "1px solid #C4C4C4",
                    paddingBottom: 6,
                  }}
                >
                  <Row>
                    <Col span={22}>
                      <span style={{ fontWeight: 700 }}>
                        NHÓM HÀNG {idxTreeItem + 1}
                      </span>
                    </Col>
                    {idxTreeItem === 0 ? null : eventGift.type_event_status ===
                      1 ? null : (
                      <Col span={2}>
                        <div
                          style={{
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "flex-end",
                          }}
                        >
                          <div
                            style={{
                              background: "rgba(227, 95, 75, 0.1)",
                              width: 20,
                              height: 20,
                              borderRadius: 10,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              cursor: "pointer",
                            }}
                            onClick={() => {
                              dispatch(REMOVE_PRODUCT_GROUP(idxTreeItem));
                            }}
                          >
                            <ISvg
                              name={ISvg.NAME.CROSS}
                              width={8}
                              height={8}
                              fill="#E35F4B"
                            />
                          </div>
                        </div>
                      </Col>
                    )}
                  </Row>
                </Col>

                <Col span={24}>
                  <ul style={{ padding: 0 }}>
                    {itemTree.treeProduct.map((item, index) => {
                      if (item.children && item.children.length) {
                        return (
                          <ul
                            style={{
                              listStyleType: "none",
                              padding: 0,
                            }}
                          >
                            <li
                              style={{
                                fontWeight: 500,
                                borderBottom: "0.5px dashed #7A7B7B",
                                padding: "6px 0px",
                                color: "black",
                              }}
                            >
                              {item.name}
                            </li>
                            {treeProct(item.children, idxTreeItem)}
                          </ul>
                        );
                      } else {
                        return (
                          <ul
                            style={{
                              listStyleType: "none",
                              borderBottom: "0.5px dashed #7A7B7B",
                            }}
                          >
                            <li style={{ padding: "6px 0px" }}>
                              <Row gutter={[12, 0]} type="flex">
                                <Col span={24}>
                                  <div
                                    style={{
                                      wordWrap: "break-word",
                                      wordBreak: "break-word",
                                      overflow: "hidden",
                                      display: "flex",
                                      alignItems: "center",
                                      height: "100%",
                                    }}
                                  >
                                    <span>{item.name}</span>
                                  </div>
                                </Col>
                              </Row>
                            </li>
                          </ul>
                        );
                      }
                    })}
                  </ul>
                </Col>
                {eventGift.type_event_status === 1 ? null : (
                  <Col
                    span={24}
                    style={{
                      borderTop: "1px solid #C4C4C4",
                      padding: "6px 0px",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <div
                        style={{ display: "flex" }}
                        className="cursor"
                        onClick={() => {
                          const data = {
                            key: "id_count_save_item",
                            value: idxTreeItem,
                          };
                          dispatch(UPDATE_ID_COUNT_SAVE_ITEM(data));
                          dispatch(
                            ASSIGNED_TREE_CLICK_GROUP(
                              treeProductEventGiftGroup2.listArrListTreeConfirm[
                                idxTreeItem
                              ].treeProduct
                            )
                          );
                          setVisibleTree2(true);
                        }}
                      >
                        <span
                          style={{
                            margin: "0px 12px",
                            color: colors.main,
                            fontWeight: 500,
                          }}
                        >
                          Thêm phiên bản
                        </span>
                        <div>
                          <ISvg
                            name={ISvg.NAME.ADDUPLOAD}
                            width={20}
                            height={20}
                            fill={colors.main}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                )}
              </Row>
            </div>
          )
        );
  };

  const getListProductType = async () => {
    try {
      const data = await APIService._getListProductType();
      const newData = data.groups.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      setTypeProduct(newData);
    } catch (err) {
      console.log(err);
    }
  };

  const checkValidate = (data, type) => {
    let valid = false;

    if (type === "reward") {
      data.forEach((item, index) => {
        item.forEach((itemChild, indexChild) => {
          if (index === 0) {
            if (!Number(itemChild.number_to)) {
              const data = {
                key: type,
                validate: "valid_number_to",
                idxRoot: index,
                idx: indexChild,
                check: true,
              };
              dispatch(CHECK_VALIDATE_EVENT_GIFT(data));
              valid = true;
            }

            if (!Number(itemChild.discount)) {
              const data = {
                key: type,
                validate: "valid_discount",
                idxRoot: index,
                idx: indexChild,
                check: true,
              };
              dispatch(CHECK_VALIDATE_EVENT_GIFT(data));
              valid = true;
            }
          } else {
            if (
              !Number(itemChild.number_to) ||
              Number(itemChild.number_to) <=
                Number(data[index - 1][indexChild].number_to)
            ) {
              const data = {
                key: type,
                validate: "valid_number_to",
                idxRoot: index,
                idx: indexChild,
                check: true,
              };
              dispatch(CHECK_VALIDATE_EVENT_GIFT(data));
              valid = true;
            }

            if (
              !Number(itemChild.discount) ||
              Number(itemChild.discount) <=
                Number(data[index - 1][indexChild].discount)
            ) {
              const data = {
                key: type,
                validate: "valid_discount",
                idxRoot: index,
                idx: indexChild,
                check: true,
              };
              dispatch(CHECK_VALIDATE_EVENT_GIFT(data));
              valid = true;
            }
          }
        });
      });
    } else {
      data.forEach((item, index) => {
        item.forEach((itemChild, indexChild) => {
          if (index === 0) {
            if (!Number(itemChild.number_to)) {
              const data = {
                key: type,
                validate: "valid_number_to",
                idxRoot: index,
                idx: indexChild,
                check: true,
              };
              dispatch(CHECK_VALIDATE_EVENT_GIFT(data));
              valid = true;
            }

            if (!Number(itemChild.discount)) {
              const data = {
                key: type,
                validate: "valid_discount",
                idxRoot: index,
                idx: indexChild,
                check: true,
              };
              dispatch(CHECK_VALIDATE_EVENT_GIFT(data));
              valid = true;
            }
          } else {
            if (
              !Number(itemChild.number_to) ||
              Number(itemChild.number_to) <=
                Number(data[index - 1][indexChild].number_to)
            ) {
              const data = {
                key: type,
                validate: "valid_number_to",
                idxRoot: index,
                idx: indexChild,
                check: true,
              };
              dispatch(CHECK_VALIDATE_EVENT_GIFT(data));
              valid = true;
            }

            if (
              !Number(itemChild.discount) ||
              Number(itemChild.discount) <=
                Number(data[index - 1][indexChild].discount)
            ) {
              const data = {
                key: type,
                validate: "valid_discount",
                idxRoot: index,
                idx: indexChild,
                check: true,
              };
              dispatch(CHECK_VALIDATE_EVENT_GIFT(data));
              valid = true;
            }
          }
        });
      });
    }

    return valid;
  };

  useEffect(() => {
    getListProductType();
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              {type === "create"
                ? "Tạo Chính sách bán hàng tặng hàng"
                : "Chỉnh sửa Chính sách bán hàng tặng quà"}
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_EVENT_GIFT());
                    history.push("/event/gift/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%" }}>
            <Row gutter={[24, 24]}>
              <Col span={8}>
                <div
                  style={{ padding: 24, height: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 20]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Loại sản phẩm
                      </StyledITileHeading>
                    </Col>
                    <Col span={24} style={{ paddingTop: 2 }}>
                      <StyledISelect
                        marginLeft="0px"
                        minHeight="24px"
                        isBorderBottom="1px solid #d9d9d9"
                        value={eventGift.event_design.key_type_product}
                        disabled={eventGift.type_event_status === 1}
                        data={typeProduct}
                        onChange={(key) => {
                          if (key === 2) {
                            const data = {
                              key: "conditions_apply",
                              value: 1,
                            };
                            dispatch(CREATE_INFO_EVENT_GIFT(data));
                            dispatch(CHANGE_CONDITION_APPLY(data));
                          }
                          dispatch(UPDATE_KEY_CATE_EVENT_GIFT(key));
                        }}
                      />
                    </Col>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Danh mục sản phẩm áp dụng
                      </StyledITileHeading>
                    </Col>
                    {eventGift.type_event_status === 1 ? null : (
                      <Col span={24}>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <div
                            style={{ display: "flex" }}
                            className="cursor"
                            onClick={() => {
                              if (
                                eventGift.event_design.key_type_product === 1
                              ) {
                                const data = {
                                  key: "id_count_save_item",
                                  value:
                                    treeProductEventGiftGroup
                                      .listArrListTreeConfirm.length === 0
                                      ? 0
                                      : -1,
                                };
                                dispatch(UPDATE_ID_COUNT_SAVE_ITEM(data));
                                setVisibleTree(true);
                              } else {
                                const data = {
                                  key: "id_count_save_item",
                                  value:
                                    treeProductEventGiftGroup2
                                      .listArrListTreeConfirm.length === 0
                                      ? 0
                                      : -1,
                                };
                                dispatch(UPDATE_ID_COUNT_SAVE_ITEM(data));
                                setVisibleTree2(true);
                              }
                            }}
                          >
                            <span
                              style={{
                                margin: "0px 12px",
                                color: colors.main,
                                fontWeight: 500,
                              }}
                            >
                              Thêm nhóm hàng
                            </span>
                            <div>
                              <ISvg
                                name={ISvg.NAME.ADDUPLOAD}
                                width={20}
                                height={20}
                                fill={colors.main}
                              />
                            </div>
                          </div>
                        </div>
                      </Col>
                    )}
                    {treeProductEventGiftGroup.listArrListTreeConfirm.length ===
                    0 ? (
                      <Col span={24}>
                        <div
                          style={{
                            height: 440,
                            overflow: "auto",
                          }}
                        >
                          {treeProductConfirmGroup()}
                        </div>
                      </Col>
                    ) : (
                      <Col span={24}>
                        <div
                          style={{
                            height: 440,
                            overflow: "auto",
                          }}
                        >
                          {treeProductConfirmGroup()}
                        </div>
                      </Col>
                    )}
                  </Row>
                </div>
              </Col>
              <Col span={16}>
                <div
                  style={{ padding: "24px 12px 0px 24px", height: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[24, 20]}>
                    <Col span={12}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Điều kiện áp dụng
                      </StyledITileHeading>

                      <StyledISelect
                        marginLeft="0px"
                        style={{ marginTop: 12 }}
                        minHeight="24px"
                        isBorderBottom="1px solid #d9d9d9"
                        value={eventGift.conditions_apply}
                        disabled={eventGift.event_design.key_type_product === 2}
                        data={[
                          {
                            value: "Doanh số sản phẩm",
                            key: DefineKeyEvent.filter_sales,
                          },
                          {
                            value: "Số lượng sản phẩm",
                            key: DefineKeyEvent.filter_money,
                          },
                        ]}
                        onChange={(key) => {
                          const data = {
                            key: "conditions_apply",
                            value: key,
                          };
                          dispatch(CREATE_INFO_EVENT_GIFT(data));
                          dispatch(CHANGE_CONDITION_APPLY(data));
                        }}
                      />
                    </Col>
                    <Col span={12}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Hình thức khuyến mãi
                      </StyledITileHeading>
                      <div style={{ marginTop: 20, color: "#000" }}>
                        <span>
                          {eventGift.conditions_apply === 2
                            ? "Chiết khấu"
                            : "Số lượng"}
                        </span>
                      </div>
                    </Col>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Điều kiện tích lũy & khuyến mãi
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[24, 12]}>
                        {eventGift.type_event_status ===
                        DefineKeyEvent.event_running ? null : (
                          <Col span={14}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                              }}
                            >
                              <div
                                style={{ display: "flex" }}
                                className="cursor"
                                onClick={() => {
                                  if (
                                    treeProductEventGiftGroup
                                      .listArrListTreeConfirm.length === 0 &&
                                    treeProductEventGiftGroup2
                                      .listArrListTreeConfirm.length === 0
                                  ) {
                                    message.error(
                                      "Vui lòng chọn sản phẩm áp dụng trước!"
                                    );
                                    return;
                                  }
                                  const dataAction = {
                                    conditions_apply:
                                      eventGift.conditions_apply,
                                  };
                                  dispatch(
                                    ADD_DESIGN_EVENT_LEVEL_GROUP(dataAction)
                                  );
                                }}
                              >
                                <span
                                  style={{
                                    margin: "0px 8px",
                                    color: colors.main,
                                    fontWeight: 500,
                                  }}
                                >
                                  Thêm mức thưởng
                                </span>
                                <div>
                                  <ISvg
                                    name={ISvg.NAME.ADDUPLOAD}
                                    width={20}
                                    height={20}
                                    fill={colors.main}
                                  />
                                </div>
                              </div>
                            </div>
                          </Col>
                        )}
                      </Row>
                    </Col>

                    <Col span={24}>
                      <Row
                        style={{
                          maxHeight: 430,
                          overflowY: "auto",
                          overflowX: "hidden",
                        }}
                      >
                        {eventGift.conditions_apply === 2
                          ? eventGift.event_design.reward.map((item, index) => {
                              return renderItemGroup(item, index);
                            })
                          : eventGift.event_design.rewardQuantity.map(
                              (item, index) => {
                                return renderItemGroup(item, index);
                              }
                            )}
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={() => history.goBack()}
                  title="Quay lại"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWTHINLEFT}
                  style={{ marginRight: 12 }}
                  styleHeight={{
                    width: 140,
                  }}
                />
                <IButton
                  title="Tiếp tục"
                  onClick={() => {
                    if (
                      treeProductEventGiftGroup.listArrListTreeConfirm
                        .length === 0 &&
                      treeProductEventGiftGroup2.listArrListTreeConfirm
                        .length === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>sản phẩm.</span>
                        </span>
                      );
                      return IError(content);
                    }

                    if (eventGift.conditions_apply === 2) {
                      if (
                        checkValidate(eventGift.event_design.reward, "reward")
                      ) {
                        return IError(
                          "Vui lòng nhập chính xác thông tin Điều kiện tích lũy & khuyến mãi"
                        );
                      }
                    } else {
                      if (
                        checkValidate(
                          eventGift.event_design.rewardQuantity,
                          "rewardQuantity"
                        )
                      ) {
                        return IError(
                          "Vui lòng nhập chính xác thông tin Điều kiện tích lũy & khuyến mãi"
                        );
                      }
                    }

                    history.push(`/event/gift/description/${type}/${id}`);
                  }}
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isVisibleTree}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalTreeProductEventGiftGroup(
          () => {
            setVisibleTree(false);
          },
          eventGift.event_design.key_type_product,
          true,
          eventGift.id_count_save_item
        )}
      </Modal>
      <Modal
        visible={isVisibleTree2}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalTreeProductEventGiftGroup2(
          () => {
            setVisibleTree2(false);
          },
          eventGift.event_design.key_type_product,
          eventGift.id_count_save_item
        )}
      </Modal>
    </div>
  );
}
