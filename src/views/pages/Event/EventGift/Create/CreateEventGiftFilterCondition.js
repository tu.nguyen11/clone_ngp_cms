import { Col, Row, message, Tag, Popconfirm } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../../assets";
import { IButton, ISvg } from "../../../../components";
import {
  IDatePickerFrom,
  IError,
  StyledISelect,
  TagP,
} from "../../../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../../components/common/Font/font";
import {
  CREATE_EVENT_GIFT_FILTER,
  CREATE_INFO_EVENT_GIFT,
  CLEAR_REDUX_EVENT_GIFT,
} from "../../../../store/reducers";
import { DefineKeyEvent } from "../../../../../utils/DefineKey";
import { APIService } from "../../../../../services";
import moment from "moment";

export default function CreateEventGiftFilterCondition(props) {
  const history = useHistory();
  const params = useParams();
  const { id, type } = params;
  const format = "DD-MM-YYYY HH:mm";

  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const [dropdownRegion, setDropdownRegion] = useState([]);
  const [loading, setLoading] = useState(false);
  const [dropdownCity, setDropdownCity] = useState([]);
  const [listMembership, setListMembership] = useState([]);
  const [loadingCity, setLoadingCity] = useState(false);
  const { eventGift } = dataRoot;

  const [checkCondition1, setCheckCondition1] = useState(false);
  const [checkCondition2, setCheckCondition2] = useState(false);
  const [checkCondition3, setCheckCondition3] = useState(false);
  const [checkCondition4, setCheckCondition4] = useState(false);
  const [reload, setReload] = useState(true);

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);

    return startValue.valueOf() < dateNow.getTime();
  };

  const _getAPIListMembership = async () => {
    try {
      const data = await APIService._getListMemberships();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setListMembership(dataNew);
    } catch (error) {}
  };

  const _fetchAgencyGetRegion = async () => {
    try {
      const data = await APIService._getAllAgencyGetRegion();
      let dataNew = data.agency.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      dataNew.unshift({
        key: 0,
        value: "Toàn quốc",
      });
      setDropdownRegion(() => dataNew);
      setLoading(false);
      setReload(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const _fetchListCityByRegion = async (
    arrayRegion,
    all = false,
    remove = false
  ) => {
    try {
      const data = await APIService._getListCityByRegion(arrayRegion);

      let dataNew = data.list_cities.map((item) => {
        const key = item.id;
        const value = item.name;
        const parent = item.parent;
        return { key, value, parent };
      });

      if (!reload) {
        if (!remove) {
          if (all) {
            let arrID = data.list_cities.map((item) => item.id);
            const dataAction1 = {
              key: "arrayCity",
              value: arrID,
            };

            const dataAction2 = {
              key: "arrayCityClone",
              value: data.list_cities,
            };

            dispatch(CREATE_EVENT_GIFT_FILTER(dataAction1));
            dispatch(CREATE_EVENT_GIFT_FILTER(dataAction2));
          } else {
            if (eventGift.event_filter.arrayCity.length === 0) {
              let arrID = data.list_cities.map((item) => item.id);
              const dataAction1 = {
                key: "arrayCity",
                value: arrID,
              };

              const dataAction2 = {
                key: "arrayCityClone",
                value: data.list_cities,
              };

              dispatch(CREATE_EVENT_GIFT_FILTER(dataAction1));
              dispatch(CREATE_EVENT_GIFT_FILTER(dataAction2));
            } else {
              let dataCityNew = [];
              let dataIdNew = [];
              data.list_cities.forEach((item) => {
                if (item.parent === arrayRegion[arrayRegion.length - 1]) {
                  dataCityNew.push(item);
                  dataIdNew.push(item.id);
                }
              });

              let dataConcat = [
                ...eventGift.event_filter.arrayCityClone,
                ...dataCityNew,
              ];
              let dataIdConcat = [
                ...eventGift.event_filter.arrayCity,
                ...dataIdNew,
              ];

              const dataAction1 = {
                key: "arrayCity",
                value: dataIdConcat,
              };

              const dataAction2 = {
                key: "arrayCityClone",
                value: dataConcat,
              };

              dispatch(CREATE_EVENT_GIFT_FILTER(dataAction1));
              dispatch(CREATE_EVENT_GIFT_FILTER(dataAction2));
            }
          }
        }
      }

      setDropdownCity(dataNew);
      setLoadingCity(false);
      setReload(false);
    } catch (error) {
      console.log(error);
      setLoadingCity(false);
    }
  };

  useEffect(() => {
    _apiSelectCityRegion();
  }, []);

  const _apiSelectCityRegion = async () => {
    await _getAPIListMembership();
    await _fetchAgencyGetRegion();
    await _fetchListCityByRegion(eventGift.event_filter.arrayRegion);
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              {type === "create"
                ? "Tạo Chính sách bán hàng tặng hàng"
                : "Chỉnh sửa Chính sách bán hàng tặng quà"}
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_EVENT_GIFT());
                    history.push("/event/gift/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1200 }}>
            <Row gutter={[24, 24]} type="flex">
              <Col span={11}>
                <div
                  style={{ padding: 24, height: "100%" }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thời gian đề xuất áp dụng CSBH
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput="0px"
                                placeholder="nhập thời gian bắt đầu"
                                format={format}
                                value={
                                  eventGift.recommended_time === 0
                                    ? undefined
                                    : moment(
                                        new Date(eventGift.recommended_time),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "recommended_time",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_INFO_EVENT_GIFT(data));
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={12}>
                <div
                  style={{ padding: 24, minHeight: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 24]}>
                    <Col>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    {/* DK tham gia 1 */}
                    <Col span={24}>
                      <div
                        style={{
                          marginTop: 16,
                        }}
                      >
                        {/* {checkCondition1 === true ||
                        eventGift.event_filter.id_agency === 0 ? (
                          <TagP
                            style={{
                              fontWeight: 500,
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <span
                              style={{
                                color: "red",
                                fontSize: 20,
                                marginRight: 10,
                              }}
                            >
                              *
                            </span>
                            <div>Điều kiện tham gia 1</div>
                          </TagP>
                        ) : ( */}
                        <TagP
                          style={{
                            fontWeight: 500,
                            marginLeft: 12,
                          }}
                        >
                          <div>Điều kiện tham gia 1</div>
                        </TagP>
                        {/* )} */}
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <div
                              style={{
                                padding: 12,
                                border:
                                  checkCondition1 === true
                                    ? "1px solid red"
                                    : "1px solid #d9d9d9",
                              }}
                            >
                              <Col>
                                <div>
                                  <TagP
                                    style={{
                                      borderBottom: "1px solid #d9d9d9",
                                      paddingBottom: 16,
                                    }}
                                  >
                                    Cấp người dùng
                                  </TagP>
                                </div>
                              </Col>
                              <Col>
                                <StyledISelect
                                  isBorderBottom="1px solid #d9d9d9"
                                  defaultValue={
                                    eventGift.event_filter.id_agency === 0
                                      ? 1
                                      : eventGift.event_filter.id_agency
                                  }
                                  disabled={true}
                                  placeholder="Chọn đại lý"
                                  onChange={(key) => {
                                    const data = {
                                      key: "id_agency",
                                      value: key,
                                    };

                                    dispatch(CREATE_EVENT_GIFT_FILTER(data));
                                    setCheckCondition1(false);
                                  }}
                                  data={[
                                    {
                                      value: "Đại lý cấp 1",
                                      key: DefineKeyEvent.agency_S1,
                                    },
                                    {
                                      value: "Đại lý cấp 2",
                                      key: DefineKeyEvent.agency_S2,
                                    },
                                  ]}
                                />
                              </Col>
                            </div>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    {/* DK tham gia 2 */}
                    <Col span={24}>
                      <div>
                        {checkCondition2 === true ||
                        eventGift.event_filter.membership.length === 0 ? (
                          <TagP
                            style={{
                              fontWeight: 500,
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <span
                              style={{
                                color: "red",
                                fontSize: 20,
                                marginRight: 10,
                              }}
                            >
                              *
                            </span>
                            <div>Điều kiện tham gia 2</div>
                          </TagP>
                        ) : (
                          <TagP
                            style={{
                              fontWeight: 500,
                              marginLeft: 12,
                            }}
                          >
                            <div>Điều kiện tham gia 2</div>
                          </TagP>
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <div
                              style={{
                                padding: 12,
                                border:
                                  checkCondition2 === true
                                    ? "1px solid red"
                                    : "1px solid #d9d9d9",
                              }}
                            >
                              <Col>
                                <div
                                  style={{
                                    borderBottom: "1px solid #d9d9d9",
                                    paddingBottom: 6,
                                  }}
                                >
                                  <Row>
                                    <Col span={12}>
                                      <span style={{}}>Cấp bậc</span>
                                    </Col>
                                    <Col span={12}>
                                      <div
                                        style={{
                                          display: "flex",
                                          justifyContent: "flex-end",
                                        }}
                                      >
                                        <Popconfirm
                                          placement="bottom"
                                          title={
                                            <span>
                                              Xóa dữ liệu đang chọn <br />
                                              của Cấp bậc
                                            </span>
                                          }
                                          onConfirm={() => {
                                            const data = {
                                              key: "membership",
                                              value: [],
                                            };

                                            dispatch(
                                              CREATE_EVENT_GIFT_FILTER(data)
                                            );
                                          }}
                                          okText="Đồng ý"
                                          cancelText="Hủy"
                                        >
                                          <Tag color={colors.main}>Xóa</Tag>
                                        </Popconfirm>
                                      </div>
                                    </Col>
                                  </Row>
                                </div>
                              </Col>
                              <Col>
                                <StyledISelect
                                  mode="multiple"
                                  isBorderBottom="1px solid #d9d9d9"
                                  value={
                                    eventGift.event_filter.membership.length ===
                                    0
                                      ? undefined
                                      : eventGift.event_filter.membership
                                  }
                                  placeholder="Chọn cấp bậc"
                                  onChange={(arrKey) => {
                                    const isAll = arrKey.indexOf(0);
                                    if (isAll >= 0) {
                                      let arrayMembership = [...listMembership];
                                      arrayMembership.splice(0, 1);
                                      let idArray = arrayMembership.map(
                                        (item) => item.key
                                      );
                                      const data = {
                                        key: "membership",
                                        value: [...idArray],
                                      };
                                      dispatch(CREATE_EVENT_GIFT_FILTER(data));
                                    } else {
                                      const data = {
                                        key: "membership",
                                        value: [...arrKey],
                                      };
                                      dispatch(CREATE_EVENT_GIFT_FILTER(data));
                                    }

                                    setCheckCondition2(false);
                                  }}
                                  data={listMembership}
                                />
                              </Col>
                            </div>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    {/* DK tham gia 3 */}
                    <Col span={24}>
                      <div>
                        {checkCondition3 === true ||
                        eventGift.event_filter.arrayRegion.length === 0 ? (
                          <TagP
                            style={{
                              fontWeight: 500,
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <span
                              style={{
                                color: "red",
                                fontSize: 20,
                                marginRight: 10,
                              }}
                            >
                              *
                            </span>
                            <div>Điều kiện tham gia 3</div>
                          </TagP>
                        ) : (
                          <TagP style={{ fontWeight: 500, paddingLeft: 12 }}>
                            Điều kiện tham gia 3
                          </TagP>
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <div
                              style={{
                                padding: "6px 12px 12px 12px",
                                border:
                                  checkCondition3 === true
                                    ? "1px solid red"
                                    : "1px solid #d9d9d9",
                              }}
                            >
                              <Col>
                                <div
                                  style={{
                                    borderBottom: "1px solid #d9d9d9",
                                    paddingBottom: 6,
                                  }}
                                >
                                  <Row>
                                    <Col span={12}>
                                      <span style={{}}>Vùng địa lý</span>
                                    </Col>
                                    <Col span={12}>
                                      <div
                                        style={{
                                          display: "flex",
                                          justifyContent: "flex-end",
                                        }}
                                      >
                                        <Popconfirm
                                          placement="bottom"
                                          title={
                                            <span>
                                              Xóa dữ liệu đang chọn <br />
                                              của Vùng địa lý
                                            </span>
                                          }
                                          disabled={
                                            eventGift.type_event_status ===
                                            DefineKeyEvent.event_running
                                          }
                                          onConfirm={() => {
                                            const data = {
                                              key: "arrayCity",
                                              value: [],
                                            };

                                            const dataCityClone = {
                                              key: "arrayCityClone",
                                              value: [],
                                            };
                                            dispatch(
                                              CREATE_EVENT_GIFT_FILTER(data)
                                            );
                                            dispatch(
                                              CREATE_EVENT_GIFT_FILTER(
                                                dataCityClone
                                              )
                                            );
                                            const dataArrRegion = {
                                              key: "arrayRegion",
                                              value: [],
                                            };
                                            dispatch(
                                              CREATE_EVENT_GIFT_FILTER(
                                                dataArrRegion
                                              )
                                            );
                                            _fetchListCityByRegion([]);
                                          }}
                                          okText="Đồng ý"
                                          cancelText="Hủy"
                                        >
                                          <Tag color={colors.main}>Xóa</Tag>
                                        </Popconfirm>
                                      </div>
                                    </Col>
                                  </Row>
                                </div>
                              </Col>
                              <Col>
                                <StyledISelect
                                  disabled={
                                    eventGift.type_event_status ===
                                    DefineKeyEvent.event_running
                                  }
                                  mode="multiple"
                                  isBorderBottom="1px solid #d9d9d9"
                                  placeholder="Chọn vùng địa lý"
                                  data={dropdownRegion}
                                  value={eventGift.event_filter.arrayRegion}
                                  onChange={(arrKey) => {
                                    if (
                                      arrKey.length <
                                      eventGift.event_filter.arrayRegion.length
                                    ) {
                                      let arrCityAfterRemove = [];
                                      let arrIDCityAfterRemove = [];
                                      arrKey.forEach((key) => {
                                        eventGift.event_filter.arrayCityClone.forEach(
                                          (item) => {
                                            if (item.parent === key) {
                                              arrCityAfterRemove.push(item);
                                              arrIDCityAfterRemove.push(
                                                item.key || item.id
                                              );
                                            }
                                          }
                                        );
                                      });

                                      const data = {
                                        key: "arrayRegion",
                                        value: [...arrKey],
                                      };

                                      dispatch(CREATE_EVENT_GIFT_FILTER(data));

                                      const dataCity = {
                                        key: "arrayCityClone",
                                        value: arrCityAfterRemove,
                                      };
                                      const dataCityShow = {
                                        key: "arrayCity",
                                        value: arrIDCityAfterRemove,
                                      };
                                      dispatch(
                                        CREATE_EVENT_GIFT_FILTER(dataCity)
                                      );
                                      dispatch(
                                        CREATE_EVENT_GIFT_FILTER(dataCityShow)
                                      );

                                      setLoadingCity(true);
                                      _fetchListCityByRegion(
                                        arrKey,
                                        false,
                                        true
                                      );
                                      setCheckCondition3(false);
                                    } else {
                                      if (arrKey.length === 0) {
                                        const dataCityRemove = {
                                          key: "arrayCityClone",
                                          value: [],
                                        };
                                        const dataCityShow = {
                                          key: "arrayCity",
                                          value: [],
                                        };
                                        dispatch(
                                          CREATE_EVENT_GIFT_FILTER(
                                            dataCityRemove
                                          )
                                        );
                                        dispatch(
                                          CREATE_EVENT_GIFT_FILTER(dataCityShow)
                                        );
                                      }
                                      const isAll = arrKey.indexOf(0);
                                      if (isAll >= 0) {
                                        let arrayAddress = [...dropdownRegion];
                                        arrayAddress.splice(0, 1);
                                        let idArray = arrayAddress.map(
                                          (item) => item.key
                                        );
                                        const data = {
                                          key: "arrayRegion",
                                          value: [...idArray],
                                        };

                                        dispatch(
                                          CREATE_EVENT_GIFT_FILTER(data)
                                        );
                                        setLoadingCity(true);
                                        _fetchListCityByRegion(idArray, true);
                                        setCheckCondition3(false);
                                      } else {
                                        const data = {
                                          key: "arrayRegion",
                                          value: [...arrKey],
                                        };

                                        dispatch(
                                          CREATE_EVENT_GIFT_FILTER(data)
                                        );
                                        setLoadingCity(true);
                                        _fetchListCityByRegion(arrKey);
                                        setCheckCondition3(false);
                                      }
                                    }
                                  }}
                                />
                              </Col>
                            </div>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    {/* DK tham gia 4 */}
                    <Col span={24}>
                      <div>
                        {checkCondition3 === true ||
                        eventGift.event_filter.arrayCity.length === 0 ? (
                          <TagP
                            style={{
                              fontWeight: 500,
                              display: "flex",
                              textAlign: "center",
                            }}
                          >
                            <span
                              style={{
                                color: "red",
                                fontSize: 20,
                                marginRight: 10,
                              }}
                            >
                              *
                            </span>
                            <div>Điều kiện tham gia 4</div>
                          </TagP>
                        ) : (
                          <TagP style={{ fontWeight: 500, paddingLeft: 12 }}>
                            Điều kiện tham gia 4
                          </TagP>
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <Row gutter={[16, 16]}>
                        <Col span={24}>
                          <Row gutter={[0, 12]}>
                            <div
                              style={{
                                padding: 12,
                                border: checkCondition4
                                  ? "1px solid red"
                                  : "1px solid #d9d9d9",
                              }}
                            >
                              <Col>
                                <div
                                  style={{
                                    borderBottom: "1px solid #d9d9d9",
                                    paddingBottom: 6,
                                  }}
                                >
                                  <Row>
                                    <Col span={12}>
                                      <span style={{}}>Tỉnh/Thành phố</span>
                                    </Col>
                                    <Col span={12}>
                                      <div
                                        style={{
                                          display: "flex",
                                          justifyContent: "flex-end",
                                        }}
                                      >
                                        <Popconfirm
                                          placement="bottom"
                                          title={
                                            <span>
                                              Xóa dữ liệu đang chọn <br />
                                              của Tỉnh / Thành phố
                                            </span>
                                          }
                                          disabled={
                                            eventGift.type_event_status ===
                                            DefineKeyEvent.event_running
                                          }
                                          onConfirm={() => {
                                            const data = {
                                              key: "arrayCity",
                                              value: [],
                                            };

                                            const dataCityClone = {
                                              key: "arrayCityClone",
                                              value: [],
                                            };

                                            const dataRegion = {
                                              key: "arrayRegion",
                                              value: [],
                                            };

                                            dispatch(
                                              CREATE_EVENT_GIFT_FILTER(data)
                                            );
                                            dispatch(
                                              CREATE_EVENT_GIFT_FILTER(
                                                dataCityClone
                                              )
                                            );
                                            dispatch(
                                              CREATE_EVENT_GIFT_FILTER(
                                                dataRegion
                                              )
                                            );
                                          }}
                                          okText="Đồng ý"
                                          cancelText="Hủy"
                                        >
                                          <Tag color={colors.main}>Xóa</Tag>
                                        </Popconfirm>
                                      </div>
                                    </Col>
                                  </Row>
                                </div>
                              </Col>

                              <Col>
                                <StyledISelect
                                  mode="multiple"
                                  isBorderBottom="1px solid #d9d9d9"
                                  placeholder="Chọn tỉnh/ thành phố"
                                  style={{ maxHeight: 200, overflow: "auto" }}
                                  disabled={
                                    // eventGift.event_filter.arrayRegion
                                    //   .length === 0 ||
                                    eventGift.type_event_status ===
                                    DefineKeyEvent.event_running
                                  }
                                  loading={loadingCity}
                                  onChange={(arrKey) => {
                                    if (arrKey.length === 0) {
                                      setDropdownCity([]);
                                    }
                                    if (
                                      arrKey.length <
                                      eventGift.event_filter.arrayCity.length
                                    ) {
                                      let arrCity = arrKey.map((item) => {
                                        return dropdownCity.find((item1) => {
                                          return item1.key === item;
                                        });
                                      });
                                      eventGift.event_filter.arrayRegion.forEach(
                                        (item, index) => {
                                          let arrCityOfRegion = arrCity.filter(
                                            (key) => {
                                              return key.parent === item;
                                            }
                                          );

                                          if (arrCityOfRegion.length === 0) {
                                            let arrRegionNew = [
                                              ...eventGift.event_filter
                                                .arrayRegion,
                                            ];
                                            arrRegionNew.splice(index, 1);
                                            const data = {
                                              key: "arrayRegion",
                                              value: [...arrRegionNew],
                                            };
                                            dispatch(
                                              CREATE_EVENT_GIFT_FILTER(data)
                                            );

                                            setLoadingCity(true);
                                            _fetchListCityByRegion(
                                              arrRegionNew,
                                              false,
                                              true
                                            );
                                          }
                                        }
                                      );
                                    }
                                    const isAll = arrKey.indexOf(0);
                                    if (isAll >= 0) {
                                      let arrayCity = [...dropdownCity];
                                      arrayCity.splice(0, 1);
                                      let arrKeyID = [...arrayCity];
                                      let arrID = arrKeyID.map(
                                        (item) => item.key
                                      );
                                      const data = {
                                        key: "arrayCity",
                                        value: [...arrID],
                                      };

                                      const dataCityClone = {
                                        key: "arrayCityClone",
                                        value: [...arrKeyID],
                                      };
                                      dispatch(CREATE_EVENT_GIFT_FILTER(data));
                                      dispatch(
                                        CREATE_EVENT_GIFT_FILTER(dataCityClone)
                                      );

                                      setCheckCondition4(false);
                                    } else {
                                      let arrKeyID = arrKey.map((item) => {
                                        return dropdownCity.find((item1) => {
                                          return item1.key === item;
                                        });
                                      });
                                      const data = {
                                        key: "arrayCity",
                                        value: [...arrKey],
                                      };

                                      const dataCityClone = {
                                        key: "arrayCityClone",
                                        value: [...arrKeyID],
                                      };
                                      dispatch(CREATE_EVENT_GIFT_FILTER(data));
                                      dispatch(
                                        CREATE_EVENT_GIFT_FILTER(dataCityClone)
                                      );

                                      setCheckCondition4(false);
                                    }
                                  }}
                                  // onDropdownVisibleChange={(open) => {
                                  //   if (open) {
                                  //     setLoadingCity(true);
                                  //     _fetchListCityByRegion(
                                  //       eventGift.event_filter.arrayRegion
                                  //     );
                                  //     return;
                                  //   }
                                  // }}
                                  value={eventGift.event_filter.arrayCity}
                                  data={dropdownCity}
                                />
                              </Col>
                            </div>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: "flex", justifyContent: "flex-end" }}
                      >
                        <IButton
                          title="Hủy"
                          color={colors.oranges}
                          icon={ISvg.NAME.CROSS}
                          styleHeight={{
                            width: 140,
                          }}
                          onClick={() => {
                            // clear data
                            // const data = {
                            //   key: "type_way",
                            //   value: 0,
                            // };
                            // const data1 = {
                            //   key: "id_agency",
                            //   value: 0,
                            // };
                            const data2 = {
                              key: "arrayRegion",
                              value: [],
                            };
                            const data3 = {
                              key: "arrayCity",
                              value: [],
                            };
                            const dataCityClone = {
                              key: "arrayCityClone",
                              value: [],
                            };
                            const data4 = {
                              key: "membership",
                              value: [],
                            };
                            dispatch(CREATE_EVENT_GIFT_FILTER(dataCityClone));
                            dispatch(CREATE_EVENT_GIFT_FILTER(data3));
                            dispatch(CREATE_EVENT_GIFT_FILTER(data2));
                            // dispatch(CREATE_EVENT_GIFT_FILTER(data1));
                            dispatch(CREATE_EVENT_GIFT_FILTER(data4));
                            // dispatch(CREATE_INFO_EVENT_GIFT(data));
                            history.goBack();
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={23}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={() => {
                    if (eventGift.event_filter.arrayRegion.length === 0) {
                      setCheckCondition3(true);
                    }
                    if (eventGift.event_filter.arrayCity.length === 0) {
                      setCheckCondition4(true);
                    }
                    if (eventGift.event_filter.membership.length === 0) {
                      setCheckCondition2(true);
                    }
                    if (eventGift.recommended_time === 0) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian đề xuất áp dụng
                          </span>{" "}
                          CSBH
                        </span>
                      );
                      return IError(content);
                    }

                    if (
                      eventGift.event_filter.arrayRegion.length === 0 ||
                      eventGift.event_filter.arrayCity.length === 0 ||
                      eventGift.event_filter.id_agency === 0 ||
                      eventGift.event_filter.membership.length === 0
                    ) {
                      const content = (
                        <span>
                          Vui lòng thiết lập{" "}
                          <span style={{ fontWeight: 600 }}>
                            Nhóm đối tượng áp dụng chương trình
                          </span>
                        </span>
                      );
                      return IError(content);
                    }
                    history.push(`/event/gift/design/condition/${type}/${id}`);
                  }}
                  title="Tiếp tục"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
    </div>
  );
}
