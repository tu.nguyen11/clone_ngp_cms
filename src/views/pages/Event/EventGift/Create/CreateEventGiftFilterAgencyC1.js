import { Col, Row, Form, Modal } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../../assets";
import { IButton, ISvg, ITableHtml } from "../../../../components";
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1,
} from "../../../../components/common";
import moment from "moment";

import {
  StyledITitle,
  StyledITileHeading,
} from "../../../../components/common/Font/font";
import {
  CREATE_INFO_EVENT_GIFT,
  CLEAR_REDUX_EVENT_GIFT,
  RESET_MODAL,
} from "../../../../store/reducers";
import { DefineKeyEvent } from "../../../../../utils/DefineKey";

export default function CreateEventGiftFilterAgencyC1(props) {
  const history = useHistory();
  const params = useParams();
  const { id, type } = params;
  const format = "DD-MM-YYYY HH:mm";

  const [isVisibleAgency, setVisibleAgency] = useState(false);

  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { eventGift, modalS1 } = dataRoot;

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);

    return startValue.valueOf() < dateNow.getTime();
  };

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp đại lý",
      align: "center",
    },
  ];

  const headerTable = (dataHeader) => {
    return (
      <tr className="scroll">
        {dataHeader.map((item, index) => (
          <th className="th-table" style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (databody) => {
    return databody.map((item, index) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {eventGift.type_way === 2 ? item.shop_name : item.supplierName}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {eventGift.type_way === 2 ? item.dms_code : item.supplierCode}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "center", fontWeight: "normal" }}
        >
          Cấp 1
        </td>
      </tr>
    ));
  };

  const _renderModalS1 = () => {
    return (
      <Modal
        visible={isVisibleAgency}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1(() => {
          setVisibleAgency(false);
        })}
      </Modal>
    );
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              {type === "create"
                ? "Tạo Chính sách bán hàng tặng hàng"
                : "Chỉnh sửa Chính sách bán hàng tặng quà"}
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_EVENT_GIFT());
                    history.push("/event/gift/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1200 }}>
            <Row gutter={[24, 24]}>
              <Col span={11}>
                <div
                  style={{ padding: 24, height: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thời gian đề xuất áp dụng CSBH
                      </StyledITileHeading>
                    </Col>

                    <Col span={12}>
                      <Row gutter={[0, 12]}>
                        <Col>
                          <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                        </Col>
                        <Col>
                          <IDatePickerFrom
                            showTime
                            isBorderBottom={true}
                            disabledDate={disabledStartDateApply}
                            paddingInput="0px"
                            placeholder="nhập thời gian bắt đầu"
                            format={format}
                            value={
                              eventGift.recommended_time === 0
                                ? undefined
                                : moment(
                                    new Date(eventGift.recommended_time),
                                    format
                                  )
                            }
                            onChange={(date) => {
                              const data = {
                                key: "recommended_time",
                                value: date._d.getTime(),
                              };
                              dispatch(CREATE_INFO_EVENT_GIFT(data));
                            }}
                          />
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={12}>
                <div
                  style={{ padding: 24, height: 650 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 24]}>
                    <Col>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    {eventGift.type_event_status ===
                    DefineKeyEvent.event_running ? null : (
                      <Col span={24}>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "flex-end",
                          }}
                        >
                          <IButton
                            title="Chỉnh sửa danh sách"
                            color={colors.main}
                            icon={ISvg.NAME.WRITE}
                            onClick={() => setVisibleAgency(true)}
                          />
                        </div>
                      </Col>
                    )}

                    <Col span={24}>
                      <ITableHtml
                        height="420px"
                        childrenBody={bodyTable(modalS1.listS1Show)}
                        childrenHeader={headerTable(dataHeader)}
                        style={{
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          borderTop: "none",
                        }}
                        isBorder={false}
                      />
                    </Col>
                    <Col span={24}>
                      <div
                        style={{ display: "flex", justifyContent: "flex-end" }}
                      >
                        <IButton
                          title="Hủy"
                          color={colors.oranges}
                          icon={ISvg.NAME.CROSS}
                          styleHeight={{
                            width: 140,
                          }}
                          disabled={
                            eventGift.type_event_status ===
                            DefineKeyEvent.event_running
                          }
                          onClick={() => {
                            const data = {
                              key: "type_way",
                              value: 0,
                            };
                            dispatch(CREATE_INFO_EVENT_GIFT(data));
                            eventGift.type_way == 2
                              ? dispatch(
                                  RESET_MODAL({
                                    keyInitial_listShow: "listS1Show",
                                    keyInitial_root: "modalS1",
                                    keyInitial_listAddClone: "listS1AddClone",
                                    keyInitial_listAdd: "listS1Add",
                                    keyInitial_list: "listS1",
                                  })
                                )
                              : dispatch(
                                  RESET_MODAL({
                                    keyInitial_listShow: "listS2Show",
                                    keyInitial_root: "modalS2",
                                    keyInitial_listAddClone: "listS2AddClone",
                                    keyInitial_listAdd: "listS2Add",
                                    keyInitial_list: "listS2",
                                  })
                                );

                            history.goBack();
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={23}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={() => {
                    if (eventGift.type_way === 2) {
                      if (modalS1.listS1Show.length === 0) {
                        const content = (
                          <span>
                            Vui lòng chọn{" "}
                            <span style={{ fontWeight: 600 }}>
                              Nhóm đối tượng áp dụng chương trình
                            </span>
                          </span>
                        );
                        return IError(content);
                      }
                    }

                    if (eventGift.recommended_time === 0) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian đề xuất áp dụng
                          </span>{" "}
                          CSBH
                        </span>
                      );
                      return IError(content);
                    }
                    history.push(`/event/gift/design/condition/${type}/${id}`);
                  }}
                  title="Tiếp tục"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      {_renderModalS1()}
    </div>
  );
}
