import { Col, Row, Modal } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../../../assets";
import { IButton, ISvg } from "../../../../components";
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1,
} from "../../../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../../components/common/Font/font";
import {
  CLEAR_REDUX_EVENT_GIFT,
  CREATE_INFO_EVENT_GIFT,
} from "../../../../store/reducers";
import moment from "moment";
import { DefineKeyEvent } from "../../../../../utils/DefineKey";
export default function CreateEventGift(props) {
  const history = useHistory();
  const params = useParams();
  const { id, type } = params;
  const format = "DD-MM-YYYY HH:mm";
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const [isVisibleAgency1, setVisibleAgency1] = useState(false);

  const { eventGift, modalS1 } = dataRoot;

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);

    return startValue.valueOf() < dateNow.getTime();
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              {type === "create"
                ? "Tạo Chính sách bán hàng tặng hàng"
                : "Chỉnh sửa Chính sách bán hàng tặng quà"}
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_EVENT_GIFT());
                    history.push("/event/gift/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1100 }}>
            <Row gutter={[24, 24]}>
              <Col span={12}>
                <div
                  style={{ padding: 24, height: 500 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 36]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Thời gian đề xuất áp dụng CSBH
                      </StyledITileHeading>
                    </Col>

                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 12]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>Bắt đầu</TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput="0px"
                                placeholder="nhập thời gian bắt đầu"
                                format={format}
                                value={
                                  eventGift.recommended_time === 0
                                    ? undefined
                                    : moment(
                                        new Date(eventGift.recommended_time),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  const data = {
                                    key: "recommended_time",
                                    value: date._d.getTime(),
                                  };
                                  dispatch(CREATE_INFO_EVENT_GIFT(data));
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={12}>
                <div
                  style={{ padding: 24, height: 500 }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Nhóm đối tượng áp dụng chương trình
                      </StyledITileHeading>
                    </Col>
                    <Col span={24}>
                      <div style={{ marginTop: 24 }}>
                        <Row>
                          <Col span={6}></Col>
                          <Col span={12}>
                            <IButton
                              title="Tạo điều kiện lọc"
                              color={
                                eventGift.type_way ===
                                DefineKeyEvent.event_type_assigned
                                  ? "white"
                                  : colors.main
                              }
                              styleHeight={{
                                background:
                                  eventGift.type_way ===
                                  DefineKeyEvent.event_type_assigned
                                    ? colors.main
                                    : "white",
                              }}
                              onClick={() => {
                                const data = {
                                  key: "type_way",
                                  value: DefineKeyEvent.event_type_assigned,
                                };
                                dispatch(CREATE_INFO_EVENT_GIFT(data));
                                history.push(
                                  `/event/gift/filter/${type}/${id}`
                                );
                              }}
                            />
                          </Col>
                          <Col span={6}></Col>
                        </Row>
                      </div>
                    </Col>
                    <Col span={24}>
                      <IButton
                        title="Chọn theo Danh sách đại lý cấp 1"
                        color={
                          eventGift.type_way === DefineKeyEvent.event_type_S1
                            ? "white"
                            : colors.main
                        }
                        styleHeight={{
                          background:
                            eventGift.type_way === DefineKeyEvent.event_type_S1
                              ? colors.main
                              : "white",
                        }}
                        onClick={() => {
                          const data = {
                            key: "type_way",
                            value: DefineKeyEvent.event_type_S1,
                          };
                          dispatch(CREATE_INFO_EVENT_GIFT(data));
                          setVisibleAgency1(true);
                        }}
                      />
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Col span={24}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  onClick={() => {
                    if (eventGift.recommended_time === 0) {
                      const content = (
                        <span>
                          Vui lòng chọn{" "}
                          <span style={{ fontWeight: 600 }}>
                            Thời gian đề xuất áp dụng
                          </span>{" "}
                          CSBH
                        </span>
                      );
                      return IError(content);
                    }
                    if (eventGift.type_way === 1) {
                      if (
                        eventGift.event_filter.arrayRegion.length === 0 ||
                        eventGift.event_filter.arrayCity.length === 0 ||
                        eventGift.event_filter.id_agency === 0 ||
                        eventGift.event_filter.membership === 0
                      ) {
                        const content = (
                          <span>
                            Vui lòng thiết lập{" "}
                            <span style={{ fontWeight: 600 }}>
                              Nhóm đối tượng áp dụng chương trình
                            </span>
                          </span>
                        );
                        return IError(content);
                      }
                    } else {
                      if (modalS1.listS1Show.length === 0) {
                        const content = (
                          <span>
                            Vui lòng thiết lập{" "}
                            <span style={{ fontWeight: 600 }}>
                              Nhóm đối tượng áp dụng chương trình
                            </span>
                          </span>
                        );
                        return IError(content);
                      }
                    }

                    switch (eventGift.type_way) {
                      case DefineKeyEvent.event_type_assigned:
                        history.push(`/event/gift/filter/${type}/${id}`);
                        break;
                      case DefineKeyEvent.event_type_S1:
                        history.push(`/event/gift/agencyS1/${type}/${id}`);
                        break;
                      default:
                        break;
                    }
                  }}
                  title="Tiếp tục"
                  color={colors.main}
                  icon={ISvg.NAME.ARROWRIGHT}
                  iconRight={true}
                  styleHeight={{
                    width: 140,
                  }}
                />
              </div>
            </Col>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isVisibleAgency1}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1(
          () => {
            setVisibleAgency1(false);
          },
          () => {
            history.push(`/event/gift/agencyS1/${type}/${id}`);
          }
        )}
      </Modal>
    </div>
  );
}
