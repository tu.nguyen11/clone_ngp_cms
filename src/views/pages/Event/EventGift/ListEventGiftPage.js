import React, { useState, useEffect } from 'react'
import { Row, Col, Modal, message, Tooltip } from 'antd'
import { useHistory } from 'react-router-dom'
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IDatePicker
} from '../../../components'
import FormatterDay from '../../../../utils/FormatterDay'
import { images, colors } from '../../../../assets'
import { StyledITitle } from '../../../components/common/Font/font'
import { APIService } from '../../../../services'
import { useDispatch } from 'react-redux'
import {
  CLEAR_REDUX_EVENT_GIFT,
  ASSIGNED_DATA_REDUX_DETAIL
} from '../../../store/reducers'
import { DefineKeyEvent } from '../../../../utils/DefineKey'

function ListEventGiftPage (props) {
  const history = useHistory()
  const dispatch = useDispatch()
  const formatString = '#DD#/#MM#/#YYYY# #hhh#:#mm#'

  const NewDate = new Date()
  const DateStart = new Date()
  let startValue = DateStart.setMonth(DateStart.getMonth() - 1)
  const [filter, setFilter] = useState({
    key: '',
    page: 1,
    status: 3,
    type: 3,
    start_date: startValue,
    group_id: 0,
    end_date: NewDate.setHours(23, 59, 59, 999)
  })

  const [loadingTable, setLoadingTable] = useState(true)
  const [productType, setProductType] = useState([])

  const [selectedRowKeys, setSelectedRowKeys] = useState([])
  const [showPopupActive, setShowPopupActive] = useState(false)
  const [showPopupStop, setShowPopupStop] = useState(false)

  const rowSelection = {
    selectedRowKeys,
    onChange: selectedRowKeys => {
      debugger
      setSelectedRowKeys(selectedRowKeys)
    },
    getCheckboxProps: record => ({
      disabled: record.disabled
    })
  }

  const getListProductType = async () => {
    try {
      const data = await APIService._getListProductType()
      const newData = data.groups.map(item => {
        const key = item.id
        const value = item.name
        return { key, value }
      })
      newData.unshift({
        key: 0,
        value: 'Tất cả'
      })
      setProductType(newData)
    } catch (err) {
      console.log(err)
    }
  }

  const _fetchAPIGetEvent = async filter => {
    try {
      const data = await APIService._getListEventGift(filter)
      data.listEvent.map((item, index) => {
        item.stt = (filter.page - 1) * 15 + index + 1
        item.disabled = item.status === 2 ? true : false
        item.rowTable = JSON.stringify({
          id: item.id,
          status: item.status
        })
        return item
      })
      setData(data)
      setLoadingTable(false)
    } catch (error) {
      setLoadingTable(false)
    }
  }

  const postActiveOrStopEvent = async obj => {
    try {
      const data = await APIService._postActiveOrStopEvent(obj)
      setSelectedRowKeys([])
      obj.type === 1 ? setShowPopupActive(false) : setShowPopupStop(false)
      message.success(
        obj.type === 1
          ? 'Các CSBH bạn vừa chọn sẽ được kích hoạt vào lúc 0:00 ngày hôm sau'
          : 'Dừng chương trình thành công'
      )
      _fetchAPIGetEvent(filter)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getListProductType()
  }, [])

  useEffect(() => {
    _fetchAPIGetEvent(filter)
  }, [filter])

  const [data, setData] = useState({
    listEvent: [],
    total: 1,
    size: 15
  })

  const status = [
    {
      key: 3,
      value: 'Tất cả'
    },
    {
      key: 0,
      value: 'Nháp'
    },
    {
      key: 4,
      value: 'Đã kích hoạt'
    },
    {
      key: 1,
      value: 'Đang chạy'
    },
    {
      key: 2,
      value: 'Kết thúc'
    }
  ]

  const renderTitle = title => {
    return (
      <div>
        <ITitle title={title} style={{ fontWeight: 'bold' }} />
      </div>
    )
  }

  const columns = [
    {
      title: renderTitle('STT'),
      dataIndex: 'stt',
      key: 'stt',
      align: 'center',
      width: 60,
      render: stt => <span>{stt}</span>
    },
    {
      title: renderTitle('Tên chính sách'),
      dataIndex: 'event_name',
      key: 'event_name',
      wordWrap: 'break-word',
      wordBreak: 'break-word',
      render: event_name => <span>{!event_name ? '-' : event_name}</span>
    },
    {
      title: renderTitle('Mã chính sách'),
      dataIndex: 'event_code',
      key: 'event_code',
      wordWrap: 'break-word',
      wordBreak: 'break-word',
      render: event_code => <span>{!event_code ? '-' : event_code}</span>
    },
    {
      title: renderTitle('Loại sản phẩm'),
      dataIndex: 'group_name',
      key: 'group_name',
      wordWrap: 'break-word',
      wordBreak: 'break-word',
      render: group_name => <span>{!group_name ? '-' : group_name}</span>
    },
    {
      title: renderTitle('Ngày bắt đầu'),
      render: obj => (
        <span>
          {!obj.start_date || obj.start_date <= 0 || obj.status === 0
            ? '-'
            : FormatterDay.dateFormatWithString(obj.start_date, formatString)}
        </span>
      )
    },

    {
      title: renderTitle('Điều kiện áp dụng'),
      dataIndex: 'type_condition_apply_name',
      key: 'type_condition_apply_name',
      render: type_condition_apply_name => (
        <span>
          {!type_condition_apply_name ? '-' : type_condition_apply_name}
        </span>
      )
    },
    {
      title: renderTitle('Hình thức khuyến mãi'),
      dataIndex: 'type_reward_name',
      key: 'type_reward_name',
      render: type_reward_name => (
        <span>{!type_reward_name ? '-' : type_reward_name}</span>
      )
    },
    {
      title: renderTitle('Trạng thái'),
      dataIndex: 'status_name',
      key: 'status_name',
      render: status_name => <span>{!status_name ? '-' : status_name}</span>
    },
    {
      title: renderTitle('Ghi chú'),
      dataIndex: 'note',
      key: 'note',
      render: note => <span>{note}</span>
    }
  ]

  return (
    <div style={{ width: '100%', padding: '18px 0px 0px 0px' }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Danh sách chính sách bán hàng tặng hàng
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className='flex justify-end'>
            <Tooltip title='Tìm kiếm theo tên , mã chương trình'>
              <ISearch
                className='cursor'
                placeholder='Tìm kiếm theo tên , mã chương trình'
                onPressEnter={e => {
                  setLoadingTable(true)
                  setFilter({ ...filter, key: e.target.value, page: 1 })
                }}
                icon={
                  <div
                    style={{
                      display: 'flex',
                      width: 42,
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=''
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: '26px 0px' }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={18}>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center'
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title='Từ ngày'
                    level={4}
                    style={{
                      marginLeft: 15,
                      marginRight: 15
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filter.start_date}
                      to={filter.end_date}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return
                        }

                        setLoadingTable(true)
                        setFilter({
                          ...filter,
                          start_date: date[0]._d.setHours(0, 0, 0),
                          end_date: date[1]._d.setHours(23, 59, 59),
                          page: 1
                        })
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: 'solid'
                      }}
                    />
                  </div>
                  <div style={{ margin: '0px 15px 0px 15px' }}>
                    <ISelect
                      defaultValue='Trạng thái'
                      data={status}
                      select={true}
                      onChange={value => {
                        setLoadingTable(true)
                        setFilter({
                          ...filter,
                          type: value,
                          status:
                            value === 1 || value === 2 || value === 4
                              ? 1
                              : value,
                          page: 1
                        })
                      }}
                    />
                  </div>
                  <div style={{ width: 150 }}>
                    <ISelect
                      defaultValue='Loại sản phẩm'
                      data={productType}
                      select={true}
                      onChange={value => {
                        setLoadingTable(true)
                        setFilter({
                          ...filter,
                          group_id: value,
                          page: 1
                        })
                      }}
                    />
                  </div>
                </div>
              </Col>
              <Col span={6}>
                <Row gutter={[15, 0]}>
                  <div className='flex justify-end'>
                    <IButton
                      icon={ISvg.NAME.ARROWUP}
                      title={'Tạo mới'}
                      styleHeight={{
                        width: 120
                      }}
                      color={colors.main}
                      onClick={() => {
                        dispatch(CLEAR_REDUX_EVENT_GIFT())
                        history.push('/event/gift/create/0')
                      }}
                    />
                    <IButton
                      title={'Kích hoạt CSBH'}
                      color={colors.main}
                      style={{ marginRight: 12, marginLeft: 12 }}
                      styleHeight={{
                        width: 130
                      }}
                      onClick={() => {
                        if (selectedRowKeys.length > 0) {
                          const arrCheck = selectedRowKeys.filter(item => {
                            let itemParse = JSON.parse(item)
                            return itemParse.status === 1
                          })

                          if (arrCheck.length > 0) {
                            message.warning(
                              'Vui lòng chỉ chọn những CSBH ở trạng thái Nháp!'
                            )
                            return
                          }
                          setShowPopupActive(true)
                        } else {
                          message.warning('Vui lòng chọn CSBH!')
                          return
                        }
                      }}
                    />

                    <IButton
                      title={'Dừng CSBH'}
                      color={colors.oranges}
                      styleHeight={{
                        width: 120
                      }}
                      onClick={() => {
                        if (selectedRowKeys.length > 0) {
                          const arrCheck = selectedRowKeys.filter(item => {
                            let itemParse = JSON.parse(item)
                            return itemParse.status === 0
                          })

                          if (arrCheck.length > 0) {
                            message.warning(
                              'Vui lòng chỉ chọn những CSBH ở trạng thái Đang chạy!'
                            )
                            return
                          }
                          setShowPopupStop(true)
                        } else {
                          message.warning('Vui lòng chọn CSBH!')
                          return
                        }
                      }}
                    />
                  </div>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={data.listEvent}
                style={{ width: '100%' }}
                defaultCurrent={1}
                sizeItem={data.size}
                indexPage={filter.page}
                maxpage={data.total}
                rowKey='rowTable'
                size='small'
                scroll={{ x: 0 }}
                loading={loadingTable}
                rowSelection={rowSelection}
                onRow={(record, rowIndex) => {
                  return {
                    onClick: event => {
                      const rowValue =
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey
                      const objRow = JSON.parse(rowValue)
                      history.push(
                        `/event/eventGift/detailSalesPolicyEventGiftPage/${objRow.id}`
                      )
                    },
                    onMouseDown: event => {
                      const rowValue =
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey
                      const objRow = JSON.parse(rowValue)
                      if (event.button == 2 || event == 4) {
                        window.open(
                          `/event/eventGift/detailSalesPolicyEventGiftPage/${objRow.id}`,
                          '_blank'
                        )
                      }
                    }
                  }
                }}
                onChangePage={page => {
                  setLoadingTable(true)
                  setFilter({ ...filter, page: page })
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
      <Modal
        visible={showPopupActive}
        width={400}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 30 }}
        centered
      >
        <div
          style={{
            height: '100%'
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: 'center' }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Thông báo
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: '100%',
                  marginTop: 5,
                  textAlign: 'center',
                  fontWeight: 600,
                  color: '#000'
                }}
              >
                <span>Bạn có muốn kích hoạt chính sách bán hàng đã chọn ?</span>
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: 'absolute',
              bottom: -60,
              right: -1,
              display: 'flex',
              justifyContent: 'center',
              width: '100%'
            }}
          >
            <div style={{ width: 165, marginRight: 25 }}>
              <IButton
                color={colors.oranges}
                title='Hủy bỏ'
                styleHeight={{ minWidth: 0 }}
                onClick={() => {
                  setShowPopupActive(false)
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{ width: 165 }}>
              <IButton
                color={colors.main}
                title='Xác nhận'
                styleHeight={{ minWidth: 0 }}
                icon={ISvg.NAME.CHECKMARK}
                onClick={async () => {
                  const arrID = selectedRowKeys.map(item => {
                    let itemParse = JSON.parse(item)
                    return itemParse.id
                  })

                  const obj = {
                    event_id_list: arrID,
                    type: 1
                  }
                  console.log('objActive: ', obj)
                  postActiveOrStopEvent(obj)
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        visible={showPopupStop}
        width={400}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 30 }}
        centered
      >
        <div
          style={{
            height: '100%'
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: 'center' }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Thông báo
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: '100%',
                  marginTop: 5,
                  textAlign: 'center',
                  fontWeight: 600,
                  color: '#000'
                }}
              >
                <span>Các CSBH bạn vừa chọn sẽ được tạm dừng.</span>
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: 'absolute',
              bottom: -60,
              right: -1,
              display: 'flex',
              justifyContent: 'center',
              width: '100%'
            }}
          >
            <div style={{ width: 165, marginRight: 25 }}>
              <IButton
                color={colors.oranges}
                title='Hủy bỏ'
                styleHeight={{ minWidth: 0 }}
                onClick={() => {
                  setShowPopupStop(false)
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{ width: 165 }}>
              <IButton
                color={colors.main}
                title='Xác nhận'
                styleHeight={{ minWidth: 0 }}
                icon={ISvg.NAME.CHECKMARK}
                onClick={async () => {
                  const arrID = selectedRowKeys.map(item => {
                    let itemParse = JSON.parse(item)
                    return itemParse.id
                  })

                  const obj = {
                    event_id_list: arrID,
                    type: 2
                  }
                  console.log('objStop: ', obj)
                  postActiveOrStopEvent(obj)
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
    </div>
  )
}

export default ListEventGiftPage
