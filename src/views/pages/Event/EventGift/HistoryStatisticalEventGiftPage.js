import React, { useEffect, useState } from "react";
import { StyledITitle } from "../../../components/common/Font/font";
import { Row, Col, Tooltip, Modal } from "antd";
import { colors } from "../../../../assets";
import { ITable, ITitle, ITableHtml } from "../../../components";
import FormatterDay from "../../../../utils/FormatterDay";
import { CloseOutlined } from "@ant-design/icons";
import { TagP } from "../../../components/common";
import { priceFormat } from "../../../../utils";
import { APIService } from "../../../../services";
import { useParams } from "react-router-dom";

export default function HistoryStatisticalEventGiftPage() {
  const [visible, setVisible] = useState(false);

  const [loading, setLoading] = useState(true);

  const [dataHistoryStatistical, setDataHistoryStatistical] = useState({
    list_event: [],
    size: 15,
    total: 0,
  });

  const params = useParams();
  const { id } = params;

  const [dataLevelEvent, setDataLevelEvent] = useState({
    user_level: "",
    membership_name: [],
    geographical_area: [],
    city_name: [],
    list_limit: [],
    type_join: 0,
    type_condition_apply: 0,
    type_reward: 0,
    list_user: [],
    list_location: [],
  });

  const [filter, setFilter] = useState({
    page: 1,
    id: id,
  });

  const clickOK = () => {
    setVisible(false);
  };

  const _fetchListHistoryStatisticalEventGift = async (filterTable) => {
    try {
      const dataTemp = await APIService._getListHistoryStatisticalEventGift(
        filterTable.page,
        filterTable.id
      );
      dataTemp.list_event.map((item, index) => {
        item.stt = (filter.page - 1) * 15 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
        });
      });
      setDataHistoryStatistical(dataTemp);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const _fetchListHistoryStatisticalEventGiftAndLevel = async (id) => {
    try {
      const dataLevelTemp = await APIService._getListHistoryStatisticalEventGiftAndLevel(
        id
      );
      const dataNew = dataLevelTemp.detail_event;
      console.log(dataNew);
      setDataLevelEvent(dataNew);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    _fetchListHistoryStatisticalEventGift(filter);
  }, [filter]);

  const columns = [
    {
      title: "Thời gian cập nhật",
      dataIndex: "update_time",
      key: "update_time",
      render: (update_time) =>
        !update_time ? (
          "-"
        ) : (
          <span>
            {FormatterDay.dateFormatWithString(
              update_time,
              "#YYYY#-#MM#-#DD# #hhh#:#mm#"
            )}
          </span>
        ),
    },
    {
      title: "Thời gian áp dụng",
      dataIndex: "time_application",
      key: "time_application",
      render: (time_application) =>
        !time_application ? (
          "-"
        ) : (
          <span>
            {FormatterDay.dateFormatWithString(
              time_application,
              "#YYYY#-#MM#-#DD# #hhh#:#mm#"
            )}
          </span>
        ),
    },
    {
      title: "Người cập nhật",
      dataIndex: "admin_name",
      key: "admin_name",
      render: (admin_name) =>
        !admin_name ? (
          "-"
        ) : (
          <Tooltip title={admin_name}>
            <span>{admin_name}</span>
          </Tooltip>
        ),
    },
    {
      title: "Điều kiện áp dụng",
      dataIndex: "type_condition_apply_id",
      key: "type_condition_apply_id",
      render: (type_condition_apply_id) =>
        !type_condition_apply_id ? (
          "-"
        ) : (
          <Tooltip title={type_condition_apply_id}>
            <span>
              {type_condition_apply_id === 1
                ? "Số lượng sản phẩm"
                : "Doanh số sản phẩm"}
            </span>
          </Tooltip>
        ),
    },
    {
      title: "Hình thức khuyến mãi",
      dataIndex: "type_reward_id",
      key: "type_reward_id",
      render: (type_reward_id) =>
        !type_reward_id ? (
          "-"
        ) : (
          <span>{type_reward_id === 1 ? "Chiết khấu" : "Số lượng"}</span>
        ),
    },
  ];

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp đại lý",
      align: "center",
    },
  ];

  let headerTable = (dataHeader) => {
    return (
      <tr className="tr-table scroll">
        {dataHeader.map((item, index) => (
          <th className="th-table" style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (dataBody) => {
    return dataBody.map((item) => (
      <tr className="tr-table">
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {item.shop_name}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "left", fontWeight: "normal" }}
        >
          {item.dms_code}
        </td>
        <td
          className="td-table"
          style={{ textAlign: "center", fontWeight: "normal", width: 80 }}
        >
          {"Cấp " + dataLevelEvent.user_level}
        </td>
      </tr>
    ));
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <StyledITitle style={{ color: colors.main, fontSize: "22px" }}>
            Lịch sử cập nhật CSBH tặng hàng
          </StyledITitle>
        </Col>
      </Row>
      <ITable
        columns={columns}
        data={dataHistoryStatistical.list_event}
        defaultCurrent={1}
        sizeItem={dataHistoryStatistical.size}
        indexPage={filter.page}
        loading={loading}
        maxPage={dataHistoryStatistical.total}
        rowKey="rowTable"
        scroll={{ x: 0 }}
        style={{ width: "100%", marginTop: 35, paddingLeft: 15 }}
        backgroundWhite={true}
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => {
              const rowTable = event.currentTarget.attributes[1].value;
              const obj = JSON.parse(rowTable);
              const id = obj.id;
              _fetchListHistoryStatisticalEventGiftAndLevel(id);
              setVisible(true);
            },
          };
        }}
      />
      <Modal
        visible={visible}
        footer={null}
        closable={false}
        width={1200}
        style={{ marginTop: 50 }}
      >
        <div>
          <Row gutter={[24, 16]}>
            <Col span={11}>
              <Row>
                <StyledITitle style={{ color: colors.main, fontSize: "16px" }}>
                  Đối tượng áp dụng
                </StyledITitle>
              </Row>
              {dataLevelEvent.type_join === 3 ? (
                <>
                  <ITitle
                    title="Điều kiện tham gia 1"
                    style={{
                      fontSize: "16px",
                      marginTop: 10,
                      paddingLeft: 15,
                      fontWeight: 600,
                    }}
                  />
                  <div
                    style={{
                      padding: 6,
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Col>
                      <div style={{ marginTop: 5 }}>
                        <TagP
                          style={{
                            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                            paddingBottom: 16,
                          }}
                        >
                          Cấp người dùng
                        </TagP>
                      </div>
                    </Col>
                    <Col>
                      <span style={{ paddingLeft: 10 }}>
                        {"Đại lý cấp " + dataLevelEvent.user_level}
                      </span>
                    </Col>
                  </div>
                  <ITitle
                    title="Điều kiện tham gia 2"
                    style={{
                      fontSize: "16px",
                      marginTop: 10,
                      paddingLeft: 15,
                      fontWeight: 600,
                    }}
                  />
                  <div
                    style={{
                      padding: 6,
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Col>
                      <div>
                        <TagP
                          style={{
                            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                            paddingBottom: 16,
                          }}
                        >
                          Cấp bậc
                        </TagP>
                      </div>
                    </Col>
                    <Col>
                      <span style={{ paddingLeft: 10 }}>
                        {!dataLevelEvent.membership_name
                          ? "-"
                          : dataLevelEvent.membership_name.map(
                              (item, index) =>
                                `${item.name}${
                                  index ===
                                  dataLevelEvent.membership_name.length - 1
                                    ? ""
                                    : ", "
                                }`
                            )}
                      </span>
                    </Col>
                  </div>
                  <ITitle
                    title="Điều kiện tham gia 3"
                    style={{
                      fontSize: "16px",
                      marginTop: 10,
                      paddingLeft: 15,
                      fontWeight: 600,
                    }}
                  />
                  <div
                    style={{
                      padding: 6,
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Col>
                      <div>
                        <TagP
                          style={{
                            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                            paddingBottom: 16,
                          }}
                        >
                          Vùng địa lý
                        </TagP>
                      </div>
                    </Col>
                    <Col>
                      <div
                        style={{
                          overflow: "auto",
                          maxHeight: "100px",
                          color: colors.blackChart,
                          paddingLeft: 10,
                        }}
                      >
                        {!dataLevelEvent.geographical_area
                          ? "-"
                          : dataLevelEvent.geographical_area.map(
                              (item, index) =>
                                `${item.name}${
                                  index ===
                                  dataLevelEvent.geographical_area.length - 1
                                    ? ""
                                    : ", "
                                }`
                            )}
                      </div>
                    </Col>
                  </div>
                  <ITitle
                    title="Điều kiện tham gia 4"
                    style={{
                      fontSize: "16px",
                      marginTop: 10,
                      paddingLeft: 15,
                      fontWeight: 600,
                    }}
                  />
                  <div
                    style={{
                      padding: 6,
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                  >
                    <Col>
                      <div>
                        <TagP
                          style={{
                            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                            paddingBottom: 16,
                          }}
                        >
                          Tỉnh/ Thành phố
                        </TagP>
                      </div>
                    </Col>
                    <Col>
                      <div
                        style={{
                          overflow: "auto",
                          maxHeight: "100px",
                          color: colors.blackChart,
                          paddingLeft: 10,
                        }}
                      >
                        {!dataLevelEvent.city_name
                          ? "-"
                          : dataLevelEvent.city_name.map(
                              (item, index) =>
                                `${item.name}${
                                  index === dataLevelEvent.city_name.length - 1
                                    ? ""
                                    : ", "
                                }`
                            )}
                      </div>
                    </Col>
                  </div>
                </>
              ) : (
                <div style={{ marginTop: 25 }}>
                  <ITableHtml
                    height="420px"
                    childrenBody={bodyTable(dataLevelEvent.list_user)}
                    childrenHeader={headerTable(dataHeader)}
                    style={{
                      borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                      borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                      borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                    }}
                    isBorder={false}
                  />
                </div>
              )}
            </Col>
            <Col span={13}>
              <Row>
                <Col span={18}>
                  <StyledITitle
                    style={{
                      color: colors.main,
                      fontSize: "16px",
                      marginTop: 10,
                    }}
                  >
                    Hạn mức chính sách
                  </StyledITitle>
                </Col>
                <Col span={6}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                      cursor: "pointer",
                    }}
                    onClick={clickOK}
                  >
                    <CloseOutlined
                      style={{
                        fontWeight: "bold",
                        fontSize: "20px",
                        color: colors.gray_300,
                      }}
                    />
                  </div>
                </Col>
              </Row>
              {dataLevelEvent.list_location.map((item, index) => (
                <Row>
                  {dataLevelEvent.type_condition_apply === 1 ? (
                    <div
                      style={{
                        maxHeight: 400,
                        overflow: "auto",
                        border: "1px solid rgba(122, 123, 123, 0.5)",
                        marginTop: dataLevelEvent.type_join === 3 ? 34 : 25,
                      }}
                    >
                      <div
                        style={{
                          fontSize: 14,
                          paddingBottom: 10,
                        }}
                      >
                        <Row>
                          <div style={{ margin: "6px 18px" }}>
                            <Col span={6}>
                              {item.location < 10 ? (
                                <span style={{ fontWeight: 600, fontSize: 16 }}>
                                  {"Mức 0"}
                                  {item.location}
                                </span>
                              ) : (
                                <span style={{ fontWeight: 600, fontSize: 16 }}>
                                  {"Mức "}
                                  {item.location}
                                </span>
                              )}
                            </Col>
                            <Col span={2}></Col>
                            <Col span={dataLevelEvent.type === 2 ? 10 : 8}>
                              <div style={{ textAlign: "center" }}>
                                <span style={{ fontWeight: 600, fontSize: 16 }}>
                                  SL nhóm sản phẩm
                                </span>
                              </div>
                            </Col>
                            <Col span={dataLevelEvent.type === 2 ? 6 : 4}>
                              <span
                                style={{
                                  fontWeight: 600,
                                  fontSize: 16,
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                Tặng
                              </span>
                            </Col>
                            {dataLevelEvent.type === 2 ? null : (
                              <Col span={4}>
                                <span
                                  style={{
                                    fontWeight: 600,
                                    fontSize: 16,
                                    display: "flex",
                                    justifyContent: "center",
                                  }}
                                >
                                  Quy đổi tỷ lệ
                                </span>
                              </Col>
                            )}
                          </div>
                        </Row>
                        {item.list_group.map((itemPro, indexPro) => (
                          <div
                            style={{
                              borderBottom:
                                indexPro === item.list_group.length - 1
                                  ? "none"
                                  : "1px dashed rgba(122, 123, 123, 0.5)",
                              margin: "0px 18px",
                            }}
                          >
                            <Row gutter={[0, 24]}>
                              <Col span={6}>
                                {itemPro.group_id < 10 ? (
                                  <span
                                    style={{
                                      display: "flex",
                                    }}
                                  >
                                    {"Nhóm sản phẩm 0"}
                                    {itemPro.group_id}
                                  </span>
                                ) : (
                                  <span
                                    style={{
                                      display: "flex",
                                    }}
                                  >
                                    {"Nhóm sản phẩm "}
                                    {itemPro.group_id}
                                  </span>
                                )}
                              </Col>
                              <Col span={2}>
                                <div style={{ textAlign: "right" }}>
                                  <span>Đến</span>
                                </div>
                              </Col>
                              <Col span={dataLevelEvent.type === 2 ? 10 : 8}>
                                <div
                                  style={{
                                    textAlign: "center",
                                  }}
                                >
                                  <span>{itemPro.list_limit.condition}</span>
                                </div>
                              </Col>
                              <Col span={dataLevelEvent.type === 2 ? 6 : 4}>
                                <div
                                  style={{
                                    textAlign: "center",
                                  }}
                                >
                                  <span>{itemPro.list_limit.gift}</span>
                                </div>
                              </Col>
                              {dataLevelEvent.type === 2 ? null : (
                                <Col span={4}>
                                  <div
                                    style={{
                                      textAlign: "center",
                                    }}
                                  >
                                    <span>
                                      {itemPro.list_limit.discount}
                                      {" % "}
                                    </span>
                                  </div>
                                </Col>
                              )}
                            </Row>
                          </div>
                        ))}
                      </div>
                    </div>
                  ) : (
                    <div
                      style={{
                        maxHeight: 400,
                        overflow: "auto",
                        border: "1px solid rgba(122, 123, 123, 0.5)",
                        marginTop: dataLevelEvent.type_join === 3 ? 34 : 25,
                      }}
                    >
                      <div
                        style={{
                          fontSize: 14,
                          paddingBottom: 10,
                          width: 600,
                        }}
                      >
                        <Row>
                          <div style={{ margin: "6px 14px" }}>
                            <Col span={6}>
                              {item.location < 10 ? (
                                <span
                                  style={{
                                    display: "flex",
                                    fontWeight: 600,
                                    fontSize: 16,
                                  }}
                                >
                                  {"Mức 0"}
                                  {item.location}
                                </span>
                              ) : (
                                <span
                                  style={{
                                    display: "flex",
                                    fontWeight: 600,
                                    fontSize: 16,
                                  }}
                                >
                                  {"Mức "}
                                  {item.location}
                                </span>
                              )}
                            </Col>
                            <Col span={2}></Col>
                            <Col span={8}>
                              <div style={{ textAlign: "center" }}>
                                <span style={{ fontWeight: 600, fontSize: 16 }}>
                                  Doanh số nhóm sản phẩm
                                </span>
                              </div>
                            </Col>
                            <Col span={8}>
                              <span
                                style={{
                                  fontWeight: 600,
                                  fontSize: 16,
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                Chiết khấu
                              </span>
                            </Col>
                          </div>
                        </Row>
                        {item.list_group.map((itemGroup, indexGroup) => (
                          <div
                            style={{
                              borderBottom:
                                indexGroup === item.list_group.length - 1
                                  ? "none"
                                  : "1px dashed rgba(122, 123, 123, 0.5)",
                              margin: "0px 14px",
                            }}
                          >
                            <Row gutter={[0, 24]}>
                              <Col span={6}>
                                {itemGroup.group_id < 10 ? (
                                  <span
                                    style={{
                                      display: "flex",
                                    }}
                                  >
                                    {"Nhóm sản phẩm 0"}
                                    {itemGroup.group_id}
                                  </span>
                                ) : (
                                  <span
                                    style={{
                                      display: "flex",
                                    }}
                                  >
                                    {"Nhóm sản phẩm "}
                                    {itemGroup.group_id}
                                  </span>
                                )}
                              </Col>
                              <Col span={2}>
                                <div style={{ textAlign: "center" }}>
                                  <span>Đến</span>
                                </div>
                              </Col>
                              <Col span={8}>
                                <div
                                  style={{
                                    width: "100%",
                                    textAlign: "center",
                                  }}
                                >
                                  <span>
                                    {priceFormat(
                                      itemGroup.list_limit.condition
                                    ) + "đ"}
                                  </span>
                                </div>
                              </Col>
                              <Col span={8}>
                                <span
                                  style={{
                                    display: "flex",
                                    justifyContent: "center",
                                    // paddingLeft: 30,
                                  }}
                                >
                                  {itemGroup.list_limit.gift}
                                  {" %"}
                                </span>
                              </Col>
                            </Row>
                          </div>
                        ))}
                      </div>
                    </div>
                  )}
                </Row>
              ))}

              {/* <div
                style={{
                  maxHeight: 400,
                  overflow: "auto",
                  border: "1px solid rgba(122, 123, 123, 0.5)",
                  marginTop: dataLevelEvent.type_join === 3 ? 34 : 25,
                }}
              >
                {dataLevelEvent.list_location.map((item, index) =>
                  dataLevelEvent.type_condition_apply === 1 ? (
                    <div
                      style={{ paddingLeft: 0, margin: "10px 25px 0px 25px" }}
                    >
                      <div
                        style={{
                          fontSize: 14,
                          paddingBottom: "10px",
                          borderBottom:
                            dataLevelEvent.list_location.length - 1 !== index
                              ? "1px solid rgba(122, 123, 123, 0.5)"
                              : "none",
                        }}
                      >
                        <Row>
                          <Col span = {6}>
                            <span style = {{ fontWeight: "bold", }}>
                              {'Mức '}
                              {item.location}
                            </span>
                          </Col>
                          <Col span = {6}>
                            <span style = {{ fontWeight: "bold" }}>
                              SL nhóm sản phẩm
                            </span>
                          </Col>
                          <Col span = {6}>
                            <span style = {{ fontWeight: "bold" }}>
                              Tặng
                            </span>
                          </Col>
                          <Col span = {6}>
                            <span style = {{ fontWeight: "bold" }}>
                              Quy đổi tỷ lệ
                            </span>
                          </Col>
                        </Row>
                        {item.list_group.map((itemPro, indexPro) =>
                        (
                          <Row gutter={[0, 0]}>
                          <Col span={4}>
                            <span
                              style={{
                                display: "flex",
                                fontWeight: "bold",
                                marginLeft: 10,
                              }}
                            >
                              {'Nhóm sản phẩm '}
                              {itemPro.group_id}
                            </span>
                          </Col>
                          <Col span={2}>
                            <span>Đến</span>

                          </Col>
                          <Col span={6}>
                            <span>{item.condition}</span>
                          </Col>
                          <Col span={6}>
                            <span>{item.gift}</span>
                          </Col>
                          <Col span={6}>
                            <span>{item.gift}</span>
                          </Col>
                        </Row>

                        ))}

                      </div>
                    </div>
                  ) : (
                    <div
                      style={{ paddingLeft: 0, margin: "10px 25px 0px 25px" }}
                    >
                      <div
                        style={{
                          fontSize: 14,
                          paddingBottom: "10px",
                          borderBottom:
                            dataLevelEvent.list_location.length - 1 !== index
                              ? "1px solid rgba(122, 123, 123, 0.5)"
                              : "none",
                        }}
                      >
                        <Row>
                          <Col span={8}>
                            <span
                              style={{ display: "flex", fontWeight: "bold" }}
                            >
                              {"Mức "}
                              {item.location}
                            </span>
                          </Col>
                          <Col span={8}>
                            <span style={{ fontWeight: "bold" }}>
                              Doanh số nhóm sản phẩm
                            </span>
                          </Col>
                          <Col span={8}>
                            <span style={{ fontWeight: "bold" }}>
                              Chiết khấu
                            </span>
                          </Col>
                        </Row>
                        {item.list_group.map((itemGroup, indexGroup) => (
                          <Row gutter={[0, 0]}>
                            <Col span={6}>
                              <span
                                style={{
                                  display: "flex",
                                  fontWeight: "bold",
                                  marginLeft: 10,
                                }}
                              >
                                {"Nhóm sản phẩm "}
                                {itemGroup.group_id}
                              </span>
                            </Col>
                            <Col span={2}>
                              <span>Đến</span>
                            </Col>
                            <Col span={8}>
                              <span>
                                {priceFormat(itemGroup.list_limit.condition) +
                                  "đ"}
                              </span>
                            </Col>
                            <Col span={6}>
                              <span>{itemGroup.list_limit.gift}</span>
                            </Col>
                            <Col span={2}>
                              <span>%</span>
                            </Col>
                          </Row>
                        ))}
                      </div>
                    </div>
                  )
                )}
              </div> */}
            </Col>
          </Row>
        </div>
      </Modal>
    </div>
  );
}
