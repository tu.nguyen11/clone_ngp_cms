import React, { useEffect } from "react";
import { Row, Col, Tooltip } from "antd";
import { IButton, ITable } from "../../../components";
import { StyledITitle } from "../../../components/common/Font/font";
import { colors } from "../../../../assets";
import FormatterDay from "../../../../utils/FormatterDay";
import { priceFormat } from "../../../../utils";
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { APIService } from "../../../../services";

export default function StatisticalEventGiftPage() {
  const [loading, setLoading] = useState(true);

  const params = useParams();
  const { id } = params;

  const [data, setData] = useState({
    list_event: [],
    size: 15,
    total: 0,
  });

  const [filter, setFilter] = useState({
    page: 1,
    id: id,
  });

  const history = useHistory();

  const _fetchListStatisticalEventGift = async (filterTable) => {
    try {
      const dataListStatistical = await APIService._getListStatisticalEventGiftPage(
        filterTable.page,
        filterTable.id
      );
      dataListStatistical.list_event.map((item, index) => {
        item.stt = (filter.page - 1) * 15 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
        });
      });
      console.log(dataListStatistical);
      setData(dataListStatistical);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    _fetchListStatisticalEventGift(filter);
  }, [filter]);

  const reloadThePage = () => {
    window.location.reload();
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      width: 100,
      align: "center",
      render: (stt) => <span> {stt} </span>,
    },
    {
      title: "Tên đại lý",
      dataIndex: "shop_name",
      width: 300,
      key: "shop_name",
      render: (shop_name) =>
        !shop_name ? (
          "-"
        ) : (
          <Tooltip title={shop_name}>
            <span> {shop_name} </span>
          </Tooltip>
        ),
    },
    {
      title: "Ngày đặt hàng",
      dataIndex: "order_date",
      key: "order_date",
      render: (order_date) =>
        !order_date || order_date <= 0 ? (
          "-"
        ) : (
          <span>
            {FormatterDay.dateFormatWithString(
              order_date,
              "#DD#/#MM#/#YYYY# #hhh#:#mm#"
            )}
          </span>
        ),
    },
    {
      title: "Mã đơn hàng",
      dataIndex: "order_code",
      key: "order_code",
      render: (order_code) =>
        !order_code ? (
          "-"
        ) : (
          <Tooltip title={order_code}>
            <span> {order_code} </span>
          </Tooltip>
        ),
    },
    {
      title: "Tổng tiền",
      dataIndex: "total_money",
      key: "total_money",
      render: (total_money) =>
        !total_money ? "0" : <span> {priceFormat(total_money)} </span>,
    },
    {
      title: "Giá trị khuyến mãi",
      dataIndex: "promotion_value",
      key: "promotion_value",
      align: "center",
      render: (promotion_value) =>
        !promotion_value ? "0" : <span> {priceFormat(promotion_value)} </span>,
    },
  ];
  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[16, 8]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main, fontSize: "22px" }}>
              Thống kê CSBH tặng hàng
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <IButton
              title="Cập nhật"
              color={colors.main}
              style={{ width: 170 }}
              onClick={reloadThePage}
            />
          </div>
        </Col>
      </Row>
      <ITable
        columns={columns}
        scroll={{ x: 0 }}
        data={data.list_event}
        style={{ width: "100%", marginTop: "35px" }}
        defaultCurrent={1}
        sizeItem={data.size}
        indexPage={filter.page}
        loading={loading}
        maxPage={data.total}
        backgroundWhite={true}
        rowKey="rowTable"
        onRow={(record, index) => {
          return {
            onClick: (event) => {
              const rowTable = event.currentTarget.attributes[1].value;
              const obj = JSON.parse(rowTable);
              const id = obj.id;
              history.push(`/orderPageS1/detail/${id}`);
            },
          };
        }}
      />
    </div>
  );
}
