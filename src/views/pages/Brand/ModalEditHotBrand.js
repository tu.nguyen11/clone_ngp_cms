import React, { useState, useEffect, useRef, useCallback } from "react";
import { Row, Col, List, Checkbox, Input, message, notification } from "antd";
import { colors } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { ITitle, ISvg, IButton, ISelect } from "../../components";
import { APIService } from "../../../services";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import styled from "styled-components";

const { Search } = Input;

const StyledSearchCustomNew = styled(Search)`
  height: 42px;

  width: 100%;
  border: 0px !important;

  .ant-input:focus {
    box-shadow: none !important;
  }

  .ant-input-affix-wrapper .ant-input {
    padding-left: 0px !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
  .ant-input-search: not(.ant-input-search-enter-button) {
    padding-right: 6px !important;
  }
  .ant-input-suffix {
    font-size: 20px;
  }

  .ant-input {
    padding-left: 0 !important;
  }
`;

const reorder = (list, startIndex, endIndex) => {
  let result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const openNotification = () => {
  notification.error({
    message: "Thông báo",
    description: "Vượt quá số lượng cho phép. Vui lòng xóa bớt thương hiệu",
    onClick: () => {
      console.log("Notification Clicked!");
    },
  });
};

const ModalEditHotBrand = (props) => {
  const [dataTable, setDataTable] = useState({
    rows: [],
    loadingList: true,
  });
  const [loading, setLoading] = useState(true);

  const [isLoadingSave, setIsLoadingSave] = useState(false);
  const [dataList, setDataList] = useState([]);
  const [dataIDCheck, setDataIDCheck] = useState([]);
  const [filter, setFilter] = useState({
    key: "",
    page: 1,
  });

  const [filterLoadMore, setFilterLoadMore] = useState({
    key: "",
  });

  const columns = [
    { title: "STT", key: "stt", width: 60, textAlign: "center" },
    {
      title: "Thương hiệu",
      key: "name",
      width: 440,
      textAlign: "left",
    },

    { title: "", key: "id", width: 60, textAlign: "center" },
  ];

  const typeShow = [
    {
      key: 1,
      value: "Lớn",
    },
    {
      key: 2,
      value: "Nhỏ",
    },
  ];

  const onDragEnd = (result) => {
    console.log(result);
    if (!result.destination) {
      return;
    }

    let items = reorder(
      dataTable.rows,
      result.source.index,
      result.destination.index
    );
    items.map((item, index) => {
      item.stt = index + 1;
    });
    let dataIDDrag = items.map((item) => item.id);
    dataList.map((item, index) => {
      if (dataIDDrag.indexOf(Number(item.id)) != -1) {
        item.number = dataIDDrag.indexOf(Number(item.id)) + 1;
        return;
      }
    });
    dataTable.rows = items;
    setDataTable({ ...dataTable, rows: dataTable.rows });
    setDataList(dataList);
    setDataIDCheck(dataIDDrag);
  };

  const _fetchAPIListHotBrand = async (filter) => {
    try {
      const data = await APIService._getAPIListHotBrand(filter);
      data.product.map((item, index) => {
        item.stt = index + 1;
        item.code = item.id + "";
        return item;
      });

      let dataID = data.product.map((item) => {
        return item.id;
      });

      setDataTable({
        ...dataTable,
        rows: data.product,
      });

      // setHasMore(data.product.length > 0);
      setDataIDCheck(dataID);
      setLoading(false);
      await _fetchAPIListSelectBrand(filter, dataID);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchAPIListSelectBrand = async (filter, dataID) => {
    try {
      const data = await APIService._getAPIListBrand(filter.key);
      if (dataID.length !== 0) {
        let arrDataID = [...dataID];
        setDataIDCheck(arrDataID);
        data.data.map((item, index) => {
          if (arrDataID.indexOf(Number(item.id)) != -1) {
            item.is_check = 1;
            item.number = arrDataID.indexOf(Number(item.id)) + 1;
            return;
          }
          return (item.is_check = 0);
        });
      } else {
        data.data.map((item, index) => {
          if (dataIDCheck.indexOf(Number(item.id)) != -1) {
            item.is_check = 1;
            item.number = dataIDCheck.indexOf(Number(item.id)) + 1;
            return;
          }
          return (item.is_check = 0);
        });
      }
      data.data.map((item) => {
        item.code = item.id + "";
        return item;
      });
      setDataList([...data.data]);
    } catch (err) {
      console.log(err);
    }
  };

  const _postUpdateListHotBrand = async (arr) => {
    try {
      const data = await APIService._postUpdateListBrandHot(arr);
      message.success("Cập nhật thành công");
      setIsLoadingSave(false);
      props.handleCancel();
      props.callback1();
    } catch (err) {
      setIsLoadingSave(false);
    }
  };

  const handleRemove = (id) => {
    let dataIDCheckClone = [...dataIDCheck];
    let index = dataTable.rows
      .map((item, index) => {
        return item.id;
      })
      .indexOf(id);
    dataTable.rows.splice(index, 1);
    dataList.map((item, index) => {
      if (item.id == id) {
        item.is_check = 0;
        item.number = 0;
      }
    });

    let indexIdCheck = dataIDCheckClone.indexOf(id);
    dataIDCheckClone.splice(indexIdCheck, 1);

    dataList.map((item, index) => {
      if (dataIDCheckClone.indexOf(Number(item.id)) != -1) {
        item.number = dataIDCheckClone.indexOf(Number(item.id)) + 1;
        return;
      }
    });
    dataTable.rows.map((item, index) => {
      item.stt = index + 1;
    });
    setDataTable({ ...dataTable, rows: dataTable.rows });
    setDataList(dataList);
    setDataIDCheck(dataIDCheckClone);
  };

  useEffect(() => {
    _fetchAPIListHotBrand(filter);
  }, [filter, props.openModalProps]);

  useEffect(() => {
    _fetchAPIListSelectBrand(filterLoadMore, []);
  }, [filterLoadMore]);

  const tableHeaders = (
    <Row
      style={{
        background: "rgb(230, 247, 254)",
        borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
      }}
    >
      <Col span={3}>
        <div
          style={{
            width: 60,
            textAlign: "center",
            padding: "6px 12px",
            fontWeight: "bold",
          }}
        >
          <span
            style={{
              fontWeight: "bold",
            }}
          >
            STT
          </span>
        </div>
      </Col>
      <Col span={19}>
        <div
          style={{
            width: 440,
            textAlign: "left",
            padding: "6px 12px",
            fontWeight: "bold",
          }}
        >
          <span
            style={{
              fontWeight: "bold",
            }}
          >
            Thương hiệu
          </span>
        </div>
      </Col>
      <Col span={2}></Col>
    </Row>
  );

  return (
    <Row gutter={[0, 0]}>
      <Col span={24}>
        <Row gutter={[15, 0]}>
          <Col span={8}>
            <Row gutter={[0, 5]}>
              <Col span={24}>
                <StyledITitle style={{ fontSize: 16 }}>
                  Chọn thương hiệu
                </StyledITitle>
              </Col>
              <Col span={24}>
                <StyledSearchCustomNew
                  placeholder="Tìm thương hiệu"
                  onPressEnter={(e) => {
                    setFilterLoadMore({
                      ...filterLoadMore,
                      key: e.target.value,
                    });
                  }}
                />
              </Col>
              <Col span={24}>
                <Row>
                  <div
                    style={{
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                      backgroundColor: "#E4EFFF",

                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Col span={2}></Col>
                    <Col span={22}>
                      <div
                        style={{
                          fontWeight: "bold",
                          padding: "6px 12px",
                          color: colors.blackChart,
                        }}
                      >
                        Thương hiệu
                      </div>
                    </Col>
                  </div>
                </Row>

                <div
                  style={{
                    height: 327,
                    overflowY: "auto",
                    overflowX: "hidden",
                    borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                    borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                    paddingLeft: 12,
                    paddingRight: 12,
                  }}
                >
                  <List
                    dataSource={dataList}
                    bordered={false}
                    renderItem={(item, index) => (
                      <Row>
                        <List.Item>
                          <Col span={24}>
                            <Checkbox
                              value={{ ...item, ...{ stt: index } }}
                              checked={item.is_check === 1 ? true : false}
                              style={{ width: "100%" }}
                              defaultChecked={
                                item.is_check === 1 ? true : false
                              }
                              onChange={(e) => {
                                if (e.target.checked === true) {
                                  dataList[index].is_check = 1;
                                  setDataList(dataList.slice(0));
                                } else {
                                  dataList[index].is_check = 0;
                                  setDataList(dataList.slice(0));
                                }
                              }}
                            >
                              <span>{item.name}</span>
                            </Checkbox>
                          </Col>
                        </List.Item>
                      </Row>
                    )}
                    // columns={columns}
                    // bodyStyle={{ height: 400 }}
                    // rowSelection={rowSelection}
                  />
                </div>
              </Col>
              <Col span={24}>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Thêm"
                    color={colors.main}
                    icon={ISvg.NAME.BUTTONRIGHT}
                    styleHeight={{
                      width: 140,
                    }}
                    isRight={true}
                    onClick={() => {
                      let dataCheck = dataList;
                      let dataIDCheckClone = dataIDCheck;
                      let dataNew = [];

                      if (dataTable.rows.length === 50) {
                        openNotification();
                        return;
                      } else {
                        dataCheck.map((item, index) => {
                          if (item.is_check === 1) {
                            let dataFilter = dataTable.rows.find((item1) => {
                              return item1.id === item.id;
                            });
                            if (!dataFilter) {
                              dataNew.push(item);
                            }
                          } else {
                            let indexFilter = dataTable.rows.findIndex(
                              (item1) => {
                                return item1.id === item.id;
                              }
                            );
                            let indexIdFilter = dataIDCheckClone.indexOf(
                              item.id
                            );
                            if (indexFilter > -1) {
                              dataTable.rows.splice(indexFilter, 1);
                              dataIDCheckClone.splice(indexIdFilter, 1);
                            }
                          }
                        });
                        dataNew.map((item, index) => {
                          item.type = 1;
                          return item;
                        });
                        dataNew.map((item) => {
                          dataTable.rows.unshift(item);
                          dataIDCheckClone.unshift(item.id);
                        });
                        dataTable.rows.map((item, index) => {
                          item.stt = index + 1;
                        });
                        dataList.map((item, index) => {
                          if (dataIDCheckClone.indexOf(Number(item.id)) != -1) {
                            item.number =
                              dataIDCheckClone.indexOf(Number(item.id)) + 1;
                            return;
                          }
                        });
                        setDataTable({ ...dataTable, rows: dataTable.rows });
                        setDataIDCheck(dataIDCheckClone);
                        setDataList(dataList);
                      }
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={16}>
            <Row gutter={[0, 10]}>
              <Col span={24}>
                <StyledITitle style={{ fontSize: 16 }}>
                  Danh sách hiển thị
                </StyledITitle>
              </Col>
              <Col span={24}>
                <div
                  style={{
                    height: 450,
                    // overflow: "auto",
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable droppableId="droppable">
                      {(droppableProvided, droppableSnapshot) => (
                        <div ref={droppableProvided.innerRef}>
                          <table width="100%">
                            {tableHeaders}

                            <div
                              style={{
                                height: 414,
                                overflow: "auto",
                              }}
                            >
                              {dataTable.rows.map((row, index) => {
                                return (
                                  <Draggable
                                    key={row.id}
                                    draggableId={row.code}
                                    index={index}
                                  >
                                    {(draggableProvided, draggableSnapshot) => (
                                      <div
                                        className="table-edit"
                                        ref={draggableProvided.innerRef}
                                        {...draggableProvided.draggableProps}
                                        {...draggableProvided.dragHandleProps}
                                      >
                                        <tr
                                          style={{
                                            borderBottom:
                                              "1px solid rgba(122, 123, 123, 0.5)",
                                          }}
                                        >
                                          {/* {columns.map(function (column) {
                                          return column.key == "id" ? ( */}

                                          {/* ) : ( */}
                                          <td
                                            style={{
                                              padding: "6px 12px",
                                              width: 60,
                                              textAlign: "center",
                                            }}
                                          >
                                            {row.stt}
                                          </td>
                                          <td
                                            style={{
                                              padding: "6px 22px",
                                              width: 440,
                                              // textAlign:
                                              //   column.key !== "name"
                                              //     ? "center"
                                              //     : "left",
                                              // verticalAlign:
                                              //   column.key !== "name"
                                              //     ? "middle"
                                              //     : "none ",
                                            }}
                                          >
                                            {row.name}
                                          </td>
                                          <td
                                            style={{
                                              padding: "6px 12px",
                                              width: 60,
                                              display: "flex",
                                              justifyContent: "center",
                                            }}
                                          >
                                            <div
                                              style={{
                                                width: 20,
                                                height: 20,
                                                borderRadius: 10,
                                                display: "flex",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                background:
                                                  "rgba(227, 95, 75, 0.1)",
                                                cursor: "pointer",
                                              }}
                                              onClick={() => {
                                                handleRemove(row.id);
                                              }}
                                            >
                                              <ISvg
                                                name={ISvg.NAME.CROSS}
                                                width={7}
                                                height={7}
                                                fill={colors.oranges}
                                              />
                                            </div>
                                          </td>
                                          {/* ); */}
                                          {/* })} */}
                                        </tr>
                                      </div>
                                    )}
                                  </Draggable>
                                );
                              })}
                            </div>

                            {droppableProvided.placeholder}
                          </table>
                        </div>
                      )}
                    </Droppable>
                  </DragDropContext>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
      <Col span={24}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "absolute",
            top: 35,
            right: -25,
          }}
        >
          <IButton
            title="Lưu"
            color={colors.main}
            icon={ISvg.NAME.SAVE}
            loading={isLoadingSave}
            styleHeight={{
              width: 140,
              marginRight: 15,
            }}
            onClick={() => {
              setIsLoadingSave(true);
              let arrId = dataTable.rows.map((item, index) => {
                return {
                  id: item.id,
                  type: 0,
                };
              });
              _postUpdateListHotBrand(arrId);
            }}
          />
          <IButton
            title="Hủy bỏ"
            color={colors.oranges}
            icon={ISvg.NAME.CROSS}
            styleHeight={{
              width: 140,
            }}
            onClick={() => {
              props.handleCancel();
            }}
          />
        </div>
      </Col>
    </Row>
  );
};

export default ModalEditHotBrand;
