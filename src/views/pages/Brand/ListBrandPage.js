import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message, Modal, Form } from "antd";
import { ITitle, ISvg, ITable, IButton, IImage } from "../../components";
import { colors, images } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { APIService } from "../../../services";
import { IFormItem, IInputText } from "../../components/common";
import IUploadHook from "./../../components/common/Upload/IUploadHook";
import { ImageType } from "../../../constant";
import ModalEditHotBrand from "./ModalEditHotBrand";
import { conditionallyUpdateScrollbar } from "reactstrap/lib/utils";
import { useHistory } from "react-router-dom";

function ListBrandPage(props) {
  const [filter, setFilter] = useState({
    key: "",
    page: 1,
  });

  const [dataTable, setDataTable] = useState({
    listBrand: [],
    total: 1,
    size: 10,
    image_url: "",
  });

  const history = useHistory();

  const [loadingTable, setLoadingTable] = useState(true);
  const [isLoadingButton, setIsLoadingButton] = useState(false);
  const [visibleModalPrioritize, setVisibleModalPrioritize] = useState(false);
  const [visibleModalCreate, setVisibleModalCreate] = useState(false);

  const [dataRow, setDataRow] = useState({});

  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;

  const _fetchAPIListHotBrand = async (filter) => {
    try {
      const data = await APIService._getAPIListHotBrand(filter);
      data.product.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
          name: item.name,
          image: item.image,
          url: data.image_url,
        });
        return item;
      });

      // let dataID = data.product.map((item) => {
      //   return item.id;
      // });

      setDataTable({
        listBrand: data.product,
        size: data.size,
        total: data.total,
        image_url: data.image_url,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const _APIpostUpdateHotBrand = async (obj) => {
    try {
      const data = await APIService._postUpdateHotBrand(obj);
      setDataRow({});
      setIsLoadingButton(false);
      setVisibleModalCreate(false);
      message.success("Chỉnh sửa sản phẩm thành công!");
      await _fetchAPIListHotBrand(filter);
    } catch (error) {
      console.log(error);
      setIsLoadingButton(false);
    }
  };

  useEffect(() => {
    _fetchAPIListHotBrand(filter);
  }, [filter]);

  const handleSubmit = (e) => {
    setIsLoadingButton(true);
    e.preventDefault();
    reduxForm.validateFields((err, values) => {
      if (err) {
        setIsLoadingButton(false);
        return;
      }

      const obj = {
        id: dataRow.id,
        name: values.brand_name,
        image: values.image,
      };
      console.log("obj: ", obj);
      _APIpostUpdateHotBrand(obj);
    });
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Thương hiệu",
      dataIndex: "name",
      key: "name",
      render: (name) =>
        !name ? (
          "-"
        ) : (
          <Tooltip title={name}>
            <span>{name}</span>
          </Tooltip>
        ),
    },
    // {
    //   title: "Kích thước hiển thị",
    //   dataIndex: "type_show",
    //   key: "type_show",
    //   align: "center",
    //   render: (type_show) => <span>{type_show}</span>,
    // },
    {
      title: "Hình ảnh",
      dataIndex: "image",
      align: "center",
      key: "image",
      render: (image) => {
        return (
          <IImage
            src={dataTable.image_url + image}
            style={{ width: 40, height: 40 }}
          />
        );
      },
    },
    {
      title: "Thứ tự",
      dataIndex: "hightlight",
      key: "hightlight",
      align: "center",
      render: (hightlight) => <span>{!hightlight ? "-" : hightlight}</span>,
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Danh sách thương hiệu nổi bật
              </StyledITitle>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <div className="flex justify-end">
                  <IButton
                    title={"Sắp xếp hiển thị"}
                    color={colors.main}
                    styleHeight={{
                      width: 120,
                    }}
                    onClick={() => {
                      setVisibleModalPrioritize(true);
                    }}
                  />
                </div>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listBrand}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    rowKey="rowTable"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    scroll={{ x: 0 }}
                    onRow={() => {
                      return {
                        onClick: (event) => {
                          const dataRow = JSON.parse(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          reduxForm.setFieldsValue({
                            brand_name: dataRow.name,
                            image: dataRow.image,
                          });
                          setDataRow({ ...dataRow });

                          setVisibleModalCreate(true);
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={visibleModalCreate}
        width={600}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0 }}
        centered
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Form onSubmit={handleSubmit}>
            <Row gutter={[0, 10]}>
              <Col span={24}>
                <div style={{ textAlign: "left" }}>
                  <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                    Chỉnh sửa thương hiệu
                  </StyledITitle>
                </div>
              </Col>
              <Col span={24}>
                <Row gutter={[20, 0]} style={{ marginTop: 15 }}>
                  <Col span={12}>
                    <IFormItem>
                      {getFieldDecorator("brand_name", {
                        rules: [
                          {
                            required: true,
                            message: "Vui lòng nhập tên thương hiệu",
                          },
                        ],
                      })(
                        <div>
                          <Row>
                            <ITitle
                              level={4}
                              title={"Tên thương hiệu"}
                              style={{
                                fontWeight: 600,
                              }}
                            />
                            <IInputText
                              style={{ color: "#000" }}
                              placeholder="Nhập tên thương hiệu"
                              value={reduxForm.getFieldValue("brand_name")}
                            />
                          </Row>
                        </div>
                      )}
                    </IFormItem>
                  </Col>
                  <Col span={12}>
                    <ITitle
                      level={4}
                      title="Hình ảnh"
                      style={{
                        fontWeight: 600,
                      }}
                    />
                    <IFormItem>
                      {getFieldDecorator("image", {
                        rules: [
                          {
                            required: true,
                            message: "Vui lòng chọn ảnh",
                          },
                        ],
                      })(
                        <div style={{ marginTop: 10 }}>
                          <IUploadHook
                            type={ImageType.BRAND}
                            callback={(nameFile) => {
                              reduxForm.setFieldsValue({
                                image: nameFile,
                              });
                            }}
                            removeCallback={() => {
                              reduxForm.setFieldsValue({
                                image: "",
                              });
                            }}
                            dataProps={
                              reduxForm.getFieldValue("image") === ""
                                ? []
                                : [
                                    {
                                      uid: -1,
                                      name: reduxForm.getFieldValue("image"),
                                      status: "done",
                                      url:
                                        dataTable.image_url +
                                        reduxForm.getFieldValue("image"),
                                    },
                                  ]
                            }
                            width="100%"
                            height="100px"
                          />
                        </div>
                      )}
                    </IFormItem>
                  </Col>
                </Row>
              </Col>
            </Row>
            <div
              style={{
                position: "absolute",
                bottom: -60,
                right: -1,
                display: "flex",
                justifyContent: "flex-end",
                width: "100%",
              }}
            >
              <div style={{ width: 140 }}>
                <IButton
                  color={colors.main}
                  title="Lưu"
                  styleHeight={{ minWidth: 0 }}
                  icon={ISvg.NAME.SAVE}
                  htmlType="submit"
                  loading={isLoadingButton}
                />
              </div>
              <div style={{ width: 140, marginLeft: 15 }}>
                <IButton
                  color={colors.oranges}
                  title="Hủy bỏ"
                  styleHeight={{ minWidth: 0 }}
                  onClick={async () => {
                    reduxForm.setFieldsValue({
                      brand_name: "",
                      image: "",
                    });
                    setDataRow({});
                    setVisibleModalCreate(false);
                    // await _fetchAPIListHotBrand(filter);
                  }}
                  icon={ISvg.NAME.CROSS}
                />
              </div>
            </div>
          </Form>
        </div>
      </Modal>

      <Modal
        visible={visibleModalPrioritize}
        // visible={true}
        footer={null}
        width={900}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        <ModalEditHotBrand
          openModalProps={visibleModalPrioritize}
          handleCancel={() => {
            setVisibleModalPrioritize(false);
          }}
          callback1={() => {
            _fetchAPIListHotBrand(filter);
          }}
        />
      </Modal>
    </div>
  );
}

const listBrandPage = Form.create({ name: "ListBrandPage" })(ListBrandPage);

export default listBrandPage;
