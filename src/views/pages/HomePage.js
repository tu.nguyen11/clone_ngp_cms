import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import {
  PageHeader,
  Button,
  Badge,
  Affix,
  Input,
  Select,
  Table,
  Divider,
  Tag,
  Pagination,
  Collapse,
  Carousel,
  Timeline,
  Icon,
  message
} from "antd";
import { ReactComponent as SVGOrder } from "../../assets/svg/ic_order.svg";
import { Editor } from "@tinymce/tinymce-react";
import { images, colors } from "../../assets";
import { ISvg } from "../components";
const { Panel } = Collapse;
const { Option } = Select;
const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;
const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    render: text => <a>{text}</a>
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address"
  },
  {
    title: "Tags",
    key: "tags",
    dataIndex: "tags",
    render: tags => (
      <span>
        {tags.map(tag => {
          let color = tag.length > 5 ? "geekblue" : "green";
          if (tag === "loser") {
            color = "volcano";
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </span>
    )
  },
  {
    title: "Action",
    key: "action",
    render: (text, record) => (
      <span>
        <a>Invite {record.name}</a>
        <Divider type="vertical" />
        <a>Delete</a>
      </span>
    )
  }
];

const data = [
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"]
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["loser"]
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["cool", "teacher"]
  },
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"]
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["loser"]
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["cool", "teacher"]
  },
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"]
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["loser"]
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["cool", "teacher"]
  },
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"]
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["loser"]
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["cool", "teacher"]
  }
];
class HomePage extends Component {

  render() {
    const Order = images.icOrder;
    return (
      <Container fluid>
        <Col className="p-0">
          <Row
            style={{
              background: "red"
            }}
          >
            {/* <Affix
              offsetTop={0}
              style={{
                width: "100%"
                //border: "1px solid rgb(235, 237, 240)",
              }}
            >
              <PageHeader
                style={{ background: "white", width: "100%" }}
                title="Page Header"
                //   breadcrumb={{ routes }}
                subTitle={
                  <Badge count={109} style={{ backgroundColor: "#52c41a" }} />
                }
                extra={[
                  <Button key="3">Operation</Button>,
                  <Button key="2">Operation</Button>,
                  <Button key="1" type="primary">
                    Primary
                  </Button>
                ]}
              />
            </Affix> */}
          </Row>
          <Col className="p-0">
            <Row className="p-4">
              <Col className="p-0" sm={12} md="auto">
                {/* <ISvg
                  name={ISvg.NAME.ORDER}
                  width={200}
                  height={200}
                  fill={"yellow"}
                /> */}
                <Select
                  size="large"
                  defaultValue=".com"
                  style={{ minWidth: 240, width: "100%" }}
                >
                  <Option value=".com">.com</Option>
                  <Option value=".jp">.jp</Option>
                  <Option value=".cn">.cn</Option>
                  <Option value=".org">.org</Option>
                </Select>
              </Col>
              <Col className="p-0" sm={12} md="auto">
                <Select
                  size="large"
                  defaultValue=".com"
                  style={{ minWidth: 240, width: "100%" }}
                >
                  <Option value=".com">.com</Option>
                  <Option value=".jp">.jp</Option>
                  <Option value=".cn">.cn</Option>
                  <Option value=".org">.org</Option>
                </Select>
              </Col>
              <Col className="p-0" sm={12} md="auto">
                <Select
                  size="large"
                  defaultValue=".com"
                  style={{ minWidth: 240, width: "100%" }}
                >
                  <Option value=".com">.com</Option>
                  <Option value=".jp">.jp</Option>
                  <Option value=".cn">.cn</Option>
                  <Option value=".org">.org</Option>
                </Select>
              </Col>
            </Row>
            <Row className="m-2 p-0" style={{ background: "white" }}>
              <Col className="p-0">
                <Carousel autoplay>
                  <div>
                    <h3>1</h3>
                  </div>
                  <div>
                    <h3>2</h3>
                  </div>
                  <div>
                    <h3>3</h3>
                  </div>
                  <div>
                    <h3>4</h3>
                  </div>
                </Carousel>
                <Collapse accordion>
                  <Panel header="This is panel header 1" key="1">
                    <p>{text}</p>
                  </Panel>
                  <Panel header="This is panel header 2" key="2">
                    <p>{text}</p>
                  </Panel>
                  <Panel header="This is panel header 3" key="3">
                    <p>{text}</p>
                  </Panel>
                </Collapse>
              </Col>
            </Row>

            <Row className="m-2 mt-4 p-0" style={{ background: "white" }}>
              <Col className="p-4">
                <Timeline mode="alternate">
                  <Timeline.Item>
                    Create a services site 2015-09-01
                  </Timeline.Item>
                  <Timeline.Item color="green">
                    Solve initial network problems 2015-09-01
                  </Timeline.Item>
                  <Timeline.Item
                    dot={
                      <Icon
                        type="clock-circle-o"
                        style={{ fontSize: "16px" }}
                      />
                    }
                  >
                    Sed ut perspiciatis unde omnis iste natus error sit
                    voluptatem accusantium doloremque laudantium, totam rem
                    aperiam, eaque ipsa quae ab illo inventore veritatis et
                    quasi architecto beatae vitae dicta sunt explicabo.
                  </Timeline.Item>
                  <Timeline.Item color="red">
                    Network problems being solved 2015-09-01
                  </Timeline.Item>
                  <Timeline.Item>
                    Create a services site 2015-09-01
                  </Timeline.Item>
                  <Timeline.Item
                    dot={
                      <Icon
                        type="clock-circle-o"
                        style={{ fontSize: "16px" }}
                      />
                    }
                  >
                    Technical testing 2015-09-01
                  </Timeline.Item>
                </Timeline>
              </Col>
            </Row>

            <Row className="m-2 mt-4 p-0" style={{ background: "white" }}>
              <Col className="p-0">
                <Table columns={columns} dataSource={data} pagination={false} />
                <Row className="m-4 d-flex align-items-center justify-content-center">
                  <Pagination
                    onChange={() => {
                      message.success(
                        "This is a prompt message for success, and it will disappear in 10 seconds",
                        1000
                      );
                    }}
                    defaultCurrent={6}
                    total={500}
                  />
                </Row>
              </Col>
            </Row>
            <Row className="m-2 mt-4 p-0" style={{ background: "white" }}>
              <Editor
                initialValue="<p>This is the initial content of the editor</p>"
                init={{
                  height: 500,
                  menubar: false,
                  paste_data_images: true,
                  image_advtab: true,
                  plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste code help wordcount"
                  ],
                  toolbar:
                    "undo redo | formatselect | bold italic backcolor | image | \
                      alignleft aligncenter alignright alignjustify | \
                      bullist numlist outdent indent | removeformat | help",
                  file_picker_types: "file image media",
                  file_picker_callback: function(callback, value, meta) {
                    // Provide file and text for the link dialog
                    if (meta.filetype == "file") {
                      callback("mypage.html", { text: "My text" });
                    }

                    // Provide image and alt text for the image dialog
                    if (meta.filetype == "image") {
                      callback("myimage.jpg", { alt: "My alt text" });
                    }

                    // Provide alternative source and posted for the media dialog
                    if (meta.filetype == "media") {
                      callback("movie.mp4", {
                        source2: "alt.ogg",
                        poster: "image.jpg"
                      });
                    }
                  }
                }}
                onChange={e => {
                  console.log("Content was updated:", e.target.getContent());
                }}
              />
            </Row>
          </Col>
        </Col>
      </Container>
    );
  }
}

export default HomePage;
