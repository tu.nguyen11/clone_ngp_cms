import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  message,
  DatePicker,
  Form,
  Input,
} from "antd";
import moment from "moment";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  ISelectCity,
  ISelectCounty,
  ISelectStatus,
} from "../../components";

import { colors } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import { ImageType } from "../../../constant";
import ISelectCitiesEmpty from "../../components/ISelectCitiesEmpty";

const { RangePicker } = DatePicker;

class CreateStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      write: true,
      errType: true,
      errStatus: true,
      errCity: true,
      dateStart: "",
      data: {},
      imageEdit: [],
      url: "",
      dataAdd: {},
      Cate: [],
      listType: [],
      name: "",
      code: "",
      status: 0,
      type: 0,
      email: "",
      address: "",
      regions: [],
    };
    // this.check = this.check.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onOkDatePickerStart = this.onOkDatePickerStart.bind(this);
    this.onOkDatePickerEnd = this.onOkDatePickerEnd.bind(this);
    this.agencyID = Number(this.props.match.params.id);
  }

  // API Start
  componentDidMount() {
    this._getListStatus();
    this._getTypeWarehouse();
  }

  _postAPIAddWarehouse = async (obj) => {
    const data = await APIService._postAddWarehouse(obj);
    this.props.history.push("/listStorePage");
  };

  _getTypeWarehouse = async () => {
    const data = await APIService._getTypeWarehouse();
    var typeWH = [];
    data.warehouse_types.map((item, index) => {
      const key = item.id;
      const value = item.name;
      typeWH.push({ key, value });
    });
    this.setState({
      listType: typeWH,
    });
  };

  _getListStatus = async () => {
    try {
      const data = await APIService._getListStatus();

      var StatusPare = [];
      StatusPare.push({
        key: 0,
        value: "Tất cả",
      });
      data.order_status.map((item, index) => {
        const value = item.name;
        const key = item.id;
        StatusPare.push({ value, key });
      });
      this.setState({
        Cate: StatusPare,
      });
    } catch (error) {}
  };

  onOkDatePickerStart = (date, dateString) => {
    this.setState(
      {
        dateStart: Date.parse(dateString),
        errStart: false,
      },
      () => {
        this.props.form.validateFields(["dateBegin"], { force: true });
      }
    );
  };

  onOkDatePickerEnd = (date, dateString) => {
    this.setState(
      {
        dateEnd: Date.parse(dateString),
        errEnd: false,
      },
      () => {
        this.props.form.validateFields(["dateEnd"], { force: true });
      }
    );
  };
  _getAPIListCounty = async (city_id) => {
    const data = await APIService._getListCounty(city_id);
    const dataNew = data.district.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });

    this.state.data.supplier.districtID = dataNew[0].key;
    this.setState({
      data: this.state.data,
      listCounty: dataNew,
    });
  };

  onChangeStatus = (value) => {
    this.state.status = value;
    this.setState({ objEdit: this.state.objEdit, errStatus: false }, () =>
      this.props.form.validateFields(["status"], { force: true })
    );
  };
  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      const data = {
        name: this.state.name,
        code: this.state.code,
        warehouse_type: this.state.type,
        phone: this.state.phone,
        email: this.state.email,
        address: this.state.address,
        status: this.state.status,
        representative_name: this.state.representative_name,
        listCityId: this.state.listCityId,
        listDistrictId: [1],
      };

      this.setState(
        {
          dataAdd: data,
        },
        () => {
          this._postAPIAddWarehouse(this.state.dataAdd);
        }
      );
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Container fluid>
        <Row className="mt-3" xs="auto">
          <ITitle
            level={1}
            title="Tạo mới kho"
            style={{
              color: colors.main,
              fontWeight: "bold",
            }}
          />
        </Row>
        <Form onSubmit={this.handleSubmit}>
          <Row
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
            xs="auto"
            sm={8}
            md={12}
          >
            <Form.Item>
              <IButton
                icon={ISvg.NAME.SAVE}
                title="Lưu"
                color={colors.main}
                htmlType="submit"
                style={{ marginRight: 20 }}
                onClick={() => this.handleSubmit}
              />
            </Form.Item>
            <Form.Item>
              <IButton
                icon={ISvg.NAME.CROSS}
                title="Hủy"
                color={colors.oranges}
                style={{}}
                onClick={() => {
                  this.props.history.goBack();
                }}
              />
            </Form.Item>
          </Row>
          <div
            style={{
              maxWidth: 815,
              minWidth: 615,
              flex: 1,
              background: colors.white,
            }}
            className="shadow"
          >
            <Row>
              <Col className="mr-2" style={{ background: colors.white }}>
                <Row className="p-0 mt-4 mb-4 ml-2 mr-0">
                  <Col className="m-0 p-0">
                    <ITitle
                      level={2}
                      title="Thông tin kho"
                      style={{ color: colors.black, fontWeight: "bold" }}
                    />
                  </Col>
                </Row>
                <Form.Item>
                  {getFieldDecorator("name", {
                    rules: [
                      {
                        required: true,
                        message: "nhập tên cửa hàng đại lý",
                      },
                    ],
                  })(
                    <div>
                      <Row className="p-0 ml-2" xs="auto">
                        <Col className="p-0 m-0">
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Tên kho"}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <Col className="p-0 m-0">
                              <IInput
                                style={{ marginTop: 10 }}
                                level={4}
                                onChange={(event) => {
                                  this.state.name = event.target.value;

                                  this.setState({});
                                }}
                                placeholder=""
                              />
                            </Col>
                          </Row>
                        </Col>
                        <div style={{ width: 24 }} />
                        <Col></Col>
                      </Row>
                    </div>
                  )}
                </Form.Item>

                <Row className="p-0 m-0 mt-2 ml-2 mb-0 mr-0" xs="auto">
                  <Col className="p-0 m-0" style={{ color: colors.black }}>
                    <Form.Item>
                      {getFieldDecorator("code", {
                        rules: [
                          {
                            required: true,
                            message: "nhập mã kho",
                          },
                        ],
                      })(
                        <div>
                          <Row className="p-0 m-0">
                            <ITitle
                              level={4}
                              title={"Mã kho"}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <IInput
                              style={{ marginTop: 10 }}
                              level={4}
                              onChange={(event) => {
                                this.state.code = event.target.value;
                                this.setState({});
                              }}
                              placeholder=""
                            />
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Col>
                  <div style={{ width: 24 }} />
                  <Col className="p-0 m-0">
                    <Form.Item>
                      {getFieldDecorator("type", {
                        rules: [
                          {
                            required: this.state.errType,
                            message: "chọn loại kho",
                          },
                        ],
                      })(
                        <div>
                          <Row className="p-0 m-0">
                            <ITitle
                              level={4}
                              title={"Loại kho"}
                              style={{ fontWeight: 700, marginBottom: 10 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ISelect
                              isBackground={false}
                              select={true}
                              onChange={(value, key) => {
                                this.state.type = value;

                                this.setState(
                                  {
                                    errType: false,
                                  },
                                  () =>
                                    this.props.form.validateFields(["type"], {
                                      force: true,
                                    })
                                );
                              }}
                              data={this.state.listType}
                            />
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Col>
                </Row>

                <Row className="p-0 mt-2 ml-2 mb-0 mr-0" xs="auto">
                  <Col className="p-0 m-0" style={{ color: colors.black }}>
                    <Form.Item>
                      {getFieldDecorator("phone", {
                        rules: [
                          {
                            required: true,

                            message: "nhập số điện thoại",
                          },
                        ],
                      })(
                        <div>
                          {" "}
                          <Row className="p-0 m-0">
                            <ITitle
                              level={4}
                              title={"Điện thoại"}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <IInput
                              style={{ marginTop: 10 }}
                              level={4}
                              onChange={(event) => {
                                this.state.phone = event.target.value;
                                this.setState({});
                              }}
                              placeholder=""
                            />
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Col>

                  <Col
                    className="p-0 "
                    style={{ marginLeft: 28, marginTop: 0 }}
                  >
                    <Form.Item>
                      {getFieldDecorator("email", {
                        rules: [
                          {
                            type: "email",
                            message: "không đúng định dạng (abc@gmail.com)",
                          },
                          {
                            required: true,
                            message: "nhập email",
                          },
                        ],
                      })(
                        <div>
                          {" "}
                          <Row className="p-0 m-0">
                            <ITitle
                              level={4}
                              title={"Email"}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <IInput
                              style={{ marginTop: 10 }}
                              level={4}
                              onChange={(event) => {
                                this.state.email = event.target.value;
                                this.setState({});
                              }}
                              placeholder=""
                            />
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Col>
                </Row>

                <Row className="p-0 mt-2 ml-2 mb-0 mr-0" xs="auto">
                  <Col className="p-0 m-0">
                    <Form.Item>
                      {getFieldDecorator("address", {
                        rules: [
                          {
                            required: true,

                            message: "nhập địa chỉ",
                          },
                        ],
                      })(
                        <div>
                          <Row
                            className="p-0 m-0"
                            style={{ backgroungColor: "red" }}
                          >
                            <ITitle
                              level={4}
                              title={"Địa chỉ"}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row
                            className="p-0 m-0"
                            style={{ backgroungColor: "red" }}
                          >
                            <Col className="p-0 m-0">
                              <IInput
                                style={{
                                  marginTop: 10,
                                  width: "100%",
                                  borderTopWidth: 0,
                                }}
                                level={4}
                                onChange={(event) => {
                                  this.state.address = event.target.value;
                                  this.setState({});
                                }}
                                placeholder=""
                              />
                            </Col>
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row className="p-0 mt-2 ml-2 mb-0 mr-0" xs="auto">
                  <Col className="p-0 m-0">
                    <Form.Item>
                      {getFieldDecorator("representative_name", {
                        rules: [
                          {
                            required: true,

                            message: "nhập tên đại diện",
                          },
                        ],
                      })(
                        <div>
                          <Row className="p-0 m-0">
                            <ITitle
                              level={4}
                              title={"Đại diện"}
                              style={{ fontWeight: 700 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <IInput
                              // style={{ marginTop: 10 }} ????????
                              level={4}
                              onChange={(event) => {
                                this.state.representative_name =
                                  event.target.value;
                                this.setState({});
                              }}
                              placeholder=""
                            />
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Col>
                  <div style={{ width: 24 }} />

                  <Col className="p-0 m-0" style={{}}>
                    <Form.Item>
                      {getFieldDecorator("status", {
                        rules: [
                          {
                            required: this.state.errStatus,
                            message: "chọn trạng thái",
                          },
                        ],
                      })(
                        <div>
                          <ITitle
                            level={4}
                            title={"Trạng thái"}
                            style={{ fontWeight: 700 }}
                          />

                          <ISelectStatus
                            type="STORE"
                            onChange={this.onChangeStatus}
                            placeholder=""
                            // style={{ borderBottom: "1px solid #ccc" }}
                            isBackground={false}
                          />
                        </div>
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row className="p-0 mt-3 mb-4 ml-2 mr-0">
                  <ITitle
                    level={2}
                    title="Khu vực"
                    style={{ color: colors.black, fontWeight: "bold" }}
                  />
                </Row>
                <Row className="p-0 mt-2 ml-2 mb-0 mr-0" xs="auto">
                  <Col className="p-0 m-0">
                    <Form.Item>
                      {getFieldDecorator("listCityId", {
                        rules: [
                          {
                            required: this.state.errCity,

                            message: "chọn tỉnh/thành phố",
                          },
                        ],
                      })(
                        <div>
                          <Row className="p-0 m-0">
                            <ITitle
                              level={4}
                              title={"Tỉnh/Thành Phố"}
                              style={{ fontWeight: 700, marginBottom: 20 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ISelectCitiesEmpty
                              mode="multiple"
                              supplier_id={0}
                              onChange={(arr) => {
                                this.setState(
                                  {
                                    listCityId: arr,
                                    errCity: false,
                                  },
                                  () => {
                                    this.props.form.validateFields(
                                      ["listCityId"],
                                      { force: true }
                                    );
                                  }
                                );
                              }}
                              style={{
                                borderBottom: "1px solid #ccc",
                              }}
                              isBackground={false}
                              placeholder=""
                            />
                          </Row>
                        </div>
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Form>
      </Container>
    );
  }
}

const createStore = Form.create({ name: "CreateStore" })(CreateStore);

export default createStore;
