import { Table, Typography, Avatar, Button, message, DatePicker } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
  IInput,
  IDatePicker
} from "../../components";
import { colors } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { APIService } from "../../../services";
import { thisExpression } from "@babel/types";
import QueueAnim from "rc-queue-anim";
import FormatterDay from "../../../utils/FormatterDay";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={3} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  if (!title) {
    return null;
  }
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={3} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 13;
  } else widthColumn = (widthScreen - 300) / 11;
  return widthColumn * type;
}

export default class DetailCouponPage extends Component {
  constructor(props) {
    super(props);
    this.type = this.props.match.params.type;
    this.state = {
      selectedRowKeys: [],
      width: 0,
      loading: false,
      bool: false,
      status: 0,
      dataTable: [],
      data: {},
      keySearch: "",
      category_id: 0,
      type: 0,
      // loadingTable: true,
      page: 1,
      value: "", // Check here to configure the default column
      arrayRemove: [],
      Cate: [
        { value: "12", key: 1 },
        { value: "13", key: 2 }
      ],
      tableHeight: 0,
      shippers: [],
      dataInfor: []
    };
    this.tableContainer = React.createRef();
    this.stock_id = Number(this.props.match.params.id);

    this.columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",
        render: (id, row, index) => {
          if (index == this.state.dataTable.length - 1) {
            var obj = {
              children: (
                <ITitle
                  level={4}
                  title={"Tổng cộng"}
                  style={{
                    color: colors.main,
                    fontWeight: "bold",
                    fontSize: 16
                  }}
                />
              ),
              props: { colSpan: 5 }
            };
            return obj;
          }
          return _renderColumns(id);
        }
      },
      {
        title: _renderTitle("Mã sản phẩm"),
        dataIndex: "product_code",
        key: "product_code",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (product_code, row, index) => {
          if (index == this.state.dataTable.length - 1) {
            var obj = {
              children: null,
              props: { colSpan: 0 }
            };
            return obj;
          }
          return _renderColumns(product_code);
        }
      },
      {
        title: _renderTitle("Mã NCC"),
        dataIndex: "producer_code",
        key: "producer_code",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (producer_code, row, index) => {
          if (index == this.state.dataTable.length - 1) {
            var obj = {
              children: null,
              props: { colSpan: 0 }
            };
            return obj;
          }
          return _renderColumns(producer_code);
        }
      },
      {
        title: _renderTitle("Tên sản phẩm"),
        dataIndex: "product_name",
        key: "product_name",
        align: "left",
        width: _widthColumn(this.state.width, 2),

        render: (product_name, row, index) => {
          if (index == this.state.dataTable.length - 1) {
            var obj = {
              children: null,
              props: { colSpan: 0 }
            };
            return obj;
          }
          return _renderColumns(product_name);
        }
      },
      {
        title: _renderTitle("Đơn vị"),
        dataIndex: "product_unit_name",
        key: "product_unit_name",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (product_unit_name, row, index) => {
          if (index == this.state.dataTable.length - 1) {
            var obj = {
              children: null,
              props: { colSpan: 0 }
            };
            return obj;
          }
          return <ITitle level={4} title={product_unit_name} />;
        }
      },

      {
        title: _renderTitle("Thực nhận"),
        align: "center",
        children: [
          {
            title: _renderTitle("Tổng kiện"),
            dataIndex: "total_package",
            key: "total_package",

            align: "center",
            width: 100,
            render: (total_package, row, index) =>
              this._renderTotal(total_package, index)
          },
          {
            title: _renderTitle("Tổng SL"),
            dataIndex: "total_quantity",
            key: "total_quantity",
            align: "center",
            width: 100,
            render: (total_quantity, row, index) =>
              this._renderTotal(total_quantity, index)
          },
          {
            title: _renderTitle("Giá trị"),
            dataIndex: "total_money",
            key: "total_money",
            align: "center",
            width: 100,
            render: (total_money, row, index) =>
              this._renderTotal(priceFormat(total_money + "đ"), index)
          }
        ]
      },
      {
        title: _renderTitle("Ghi chú"),
        dataIndex: "note",
        key: "note",
        align: "center",
        width: 209,
        // fixed: "left",
        render: note => _renderColumns(note)
      }
    ];
  }
  _renderTotal(title, index) {
    {
      if (index == this.state.dataTable.length - 1) {
        var obj = {
          children: (
            <ITitle
              level={4}
              title={title}
              style={{
                color: colors.main,
                fontWeight: "bold",
                fontSize: 16
              }}
            />
          )
        };
        return obj;
      }
      return _renderColumns(title);
    }
  }
  componentDidMount() {
    this.type == "INPUT"
      ? this._getDetailStockInput()
      : this._getDetailStockOnput();
  }

  _getDetailStockInput = async () => {
    try {
      const data = await APIService._getDetailStockInput(this.stock_id);
      const dataImport = await APIService._getDetailStockInput(this.stock_id);

      this.printdataImport = dataImport.warehouse_import;

      let product = data.warehouse_import.products;
      product.map((item, index) => {
        product[index].stt = index + 1;
        product[index].idAndStt = {
          id: item.product_id,
          index: index
        };
      });
      product.push({
        // product_id: "Tổng tiền",
        // voucherPackage: data.warehouse_import.sumVoucherPackage,
        // voucherQuantity: data.warehouse_import.sumVoucherQuantity,
        total_package: data.warehouse_import.sum_package,
        total_quantity: data.warehouse_import.sum_quantity,
        total_money: data.warehouse_import.sum_value
      });

      this.setState({
        dataTable: product,
        data: data,
        loadingTable: false
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false
      });
    }
  };
  _getDetailStockOnput = async () => {
    try {
      const data = await APIService._getDetailStockOnput(this.stock_id);
      const dataExport = await APIService._getDetailStockOnput(this.stock_id);

      this.printdataExport = dataExport.detail_warehouse_export;

      let product = data.detail_warehouse_export.products;
      product.map((item, index) => {
        product[index].stt = index + 1;
        product[index].idAndStt = {
          id: item.product_id,
          index: index + 1
        };
      });
      product.push({
        // product_id: "Tổng tiền",
        // voucherPackage: data.warehouse_import.sumVoucherPackage,
        // voucherQuantity: data.warehouse_import.sumVoucherQuantity,
        total_package: data.detail_warehouse_export.sum_package,
        total_quantity: data.detail_warehouse_export.sum_quantity,
        total_money: data.detail_warehouse_export.sum_value
      });
      this.setState({
        dataTable: product,
        data: data,
        loadingTable: false
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false
      });
    }
  };
  _renderInfor = () => {
    var item;
    this.type == "INPUT"
      ? (item = this.state.data.warehouse_import)
      : (item = this.state.data.detail_warehouse_export);
    if (!item) {
      return null;
    }
    return (
      <Row
        style={{
          height: 100
        }}
      >
        <Col xs={8} style={{}} className="m-0 p-0">
          <Row style={{ marginBottom: 20 }}>
            <Col>
              <ITitle
                level={3}
                title={"Tên kho hàng"}
                style={{ fontWeight: 500 }}
              />
              <ITitle
                style={{ marginTop: 10 }}
                level={3}
                title={item.warehouse_name}
              />
            </Col>
            <Col>
              <ITitle
                level={3}
                title={"Mã phiếu"}
                style={{ fontWeight: 500 }}
              />
              <ITitle style={{ marginTop: 10 }} level={3} title={item.code} />
            </Col>
            <Col>
              <ITitle
                level={3}
                title={"Người lập phiếu"}
                style={{ fontWeight: 500 }}
              />
              <ITitle
                style={{ marginTop: 10 }}
                level={3}
                title={item.creator_name}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <ITitle
                level={3}
                title={
                  this.type == "INPUT"
                    ? "Thời gian nhập kho"
                    : "Thời gian xuất kho"
                }
                style={{ fontWeight: 500 }}
              />
              <ITitle
                style={{ marginTop: 10 }}
                level={3}
                title={FormatterDay.dateFormatWithString(
                  item.time,
                  "#DD#/#MM#/#YYYY# #hhhh#:#mm#:#ss#"
                )}
              />
            </Col>
            <Col>
              <ITitle
                level={3}
                title={"Loại hình"}
                style={{ fontWeight: 500 }}
              />
              <ITitle
                style={{ marginTop: 10 }}
                level={3}
                title={[
                  { key: 1, value: "Nhập kho" },
                  { key: 2, value: "Xuât kho" },
                  { key: 3, value: "Xuất chuyển kho" }
                ].map((Item, index) => {
                  if (Item.key == item.type) {
                    return Item.value;
                  }
                })}
              />
            </Col>
            <Col>
              <ITitle
                level={3}
                title={
                  this.type == "INPUT" ? "Người nhập kho" : "Người xuất kho"
                }
                style={{ fontWeight: 500 }}
              />
              <ITitle
                style={{ marginTop: 10 }}
                level={3}
                title={item.warehouse_manage_name}
              />
            </Col>
          </Row>
        </Col>
        <Col xs={4} style={{}} className="m-0 p-0">
          <Row style={{ marginBottom: 20 }}>
            <Col>
              <ITitle level={3} title={"Ghi chú"} style={{ fontWeight: 500 }} />
              <ITitle style={{ marginTop: 10 }} level={3} title={item.note} />
            </Col>
          </Row>
          <Row>
            {this.type == "INPUT" ? (
              <Col></Col>
            ) : (
              <Col>
                <ITitle
                  level={3}
                  title={"Kho tiếp nhận"}
                  style={{ fontWeight: 500 }}
                />

                <ITitle
                  style={{ marginTop: 10 }}
                  level={3}
                  title={item.warehouse_transfer_name}
                />
              </Col>
            )}
          </Row>
        </Col>
      </Row>
    );
  };
  _postExportFileInput = async () => {
    const data = await APIService._postExportFileInput(this.stock_id);
    let url = data.url;
    window.open(url);
  };
  render() {
    return (
      <Container fluid>
        <Row className="mt-3">
          <ITitle
            level={1}
            title="Chi tiết phiếu"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end"
            }}
          />
        </Row>
        <Row className="center" style={{ marginTop: 36 }}>
          <Col
            style={{
              display: "flex",
              justifyContent: "flex-end"
            }}
          >
            <Row>
              <Col xs="auto">
                <Row>
                  <IButton
                    icon={ISvg.NAME.PRINT}
                    title="In phiếu"
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    // onClick={() => {
                    //   this.props.history.push('/printInvoice' + this.stock_id);
                    //
                    // }}
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/printInvoice/" + this.type,
                        // search: "?query=abc",
                        state: {
                          data:
                            this.type == "INPUT"
                              ? this.printdataImport
                              : this.printdataExport
                        }
                      });
                    }}
                  />
                  <IButton
                    icon={ISvg.NAME.DOWLOAND}
                    title="Xuất file"
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() => {
                      this._postExportFileInput();
                    }}
                  />

                  <IButton
                    icon={ISvg.NAME.COPY}
                    title="Sao chép"
                    color={colors.main}
                    style={{}}
                    onClick={() => {
                      this.props.history.push({
                        pathname: "/CreateCouponInputPage/" + this.type,
                        // search: "?query=abc",
                        state: {
                          dataInfor:
                            this.type == "INPUT"
                              ? this.state.data.warehouse_import
                              : this.state.data.detail_warehouse_export
                        }
                      });
                    }}
                  />
                  {/* 
                  <IButton
                    icon={ISvg.NAME.DELETE}
                    title="Xóa"
                    color={colors.oranges}
                    style={{ marginRight: 20 }}
                    onClick={() => {
                      if (this.state.arrayRemove.length == 0) {
                        message.warning("Bạn chưa chọn mã sản phẩm để xóa.");
                        return;
                      }
                      const objRemove = {
                        id: this.state.arrayRemove,
                        status: -1 // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                      };
                      this._APIpostRemoveProduct(objRemove)
                    }}
                  /> */}
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row style={{ marginBottom: 21 }}>
          <ITitle
            level={3}
            style={{ fontWeight: "bold" }}
            title={this.type == "INPUT" ? "PHIẾU NHẬP KHO" : "PHIẾU XUẤT KHO"}
          />
        </Row>
        {this._renderInfor()}
        <Row
          style={{
            // paddingLeft: 140,
            // paddingRight: 210,
            marginTop: 20,
            height: 600
          }}
        >
          <div
            className="m-0 p-0 mt-4 "
            ref={this.tableContainer}
            style={{ flex: 1 }}
          >
            <ITable
              data={this.state.dataTable}
              columns={this.columns}
              loading={this.state.loadingTable}
              // bodyStyle={{
              //   height: this.state.tableHeight
              // }}
              style={{ width: "100%", background: colors.background }}
              scroll={{ y: this.state.tableHeight - 50 }}
              defaultCurrent={1}
              hidePage
              bordered
            />
          </div>
        </Row>
      </Container>
    );
  }
}
