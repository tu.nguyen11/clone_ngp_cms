import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  message,
  DatePicker,
  Form,
  Input,
} from "antd";
import moment from "moment";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  ISelectCity,
  ISelectCounty,
  ISelectStatus,
  ILoading,
} from "../../components";
import ISelectCitiesEmpty from "../../components/ISelectCitiesEmpty";
import { colors } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import FormatterDay from "../../../utils/FormatterDay";
import { ImageType } from "../../../constant";

const { RangePicker } = DatePicker;

class EditStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      write: true,
      dateEnd: "",
      errEnd: true,
      dateStart: "",
      data: {},
      imageEdit: [],
      url: "",
      dataEdit: {},
      Cate: [],
      listType: [],
      listCityId: [],
      loading: true,
    };
    // this.check = this.check.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onOkDatePickerStart = this.onOkDatePickerStart.bind(this);
    this.onOkDatePickerEnd = this.onOkDatePickerEnd.bind(this);
    this.agencyID = Number(this.props.match.params.id);
    this.onChangeCity = this.onChangeCity.bind(this);
    this.onChangeStatus = this.onChangeStatus.bind(this);
  }

  // API Start
  componentDidMount() {
    this._getAPIDetailWarehouse(this.agencyID);
    this._getTypeWarehouse();
  }
  _getAPIDetailWarehouse = async (warehouseID) => {
    try {
      const data = await APIService._getDetailWH(warehouseID);

      let arrIDCity = [];
      data.warehouse.regions.map((item, index) => {
        const key = item.id;
        // const value = item.name;
        arrIDCity.push(key);
      });

      this.setState({
        dataEdit: data.warehouse,
        listCityId: arrIDCity,
        loading: false,
      });
    } catch (error) {}
  };

  _postAPIEditWarehouse = async (obj) => {
    const data = await APIService._postEditWarehouse(obj);
    message.success("sửa thành công");
    this.props.history.push("/listStorePage");
  };

  _getTypeWarehouse = async () => {
    const data = await APIService._getTypeWarehouse();
    var typeWH = [];

    data.warehouse_types.map((item, index) => {
      const key = item.id;
      const value = item.name;
      typeWH.push({ key, value });
    });
    this.setState({
      listType: typeWH,
    });
  };

  _getListStatus = async () => {
    try {
      const data = await APIService._getListStatus();
      var StatusPare = [];
      StatusPare.push({
        key: 0,
        value: "Tất cả",
      });
      data.order_status.map((item, index) => {
        const value = item.name;
        const key = item.id;
        StatusPare.push({ value, key });
      });
      this.setState({
        Cate: StatusPare,
      });
    } catch (error) {}
  };

  onOkDatePickerStart = (date, dateString) => {
    this.setState(
      {
        dateStart: Date.parse(dateString),
        errStart: false,
      },
      () => {
        this.props.form.validateFields(["dateBegin"], { force: true });
      }
    );
  };

  onOkDatePickerEnd = (date, dateString) => {
    this.setState(
      {
        dateEnd: Date.parse(dateString),
        errEnd: false,
      },
      () => {
        this.props.form.validateFields(["dateEnd"], { force: true });
      }
    );
  };
  _getAPIListCounty = async (city_id) => {
    const data = await APIService._getListCounty(city_id);
    const dataNew = data.district.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    this.state.data.supplier.districtID = dataNew[0].key;
    this.setState({
      data: this.state.data,
      listCounty: dataNew,
    });
  };

  onChangeCity = (key, value) => {
    this.state.data.supplier.cityID = key;
    this.setState(
      {
        // data: this.state.data,
        disabledCounty: true,
        errCity: false,
      },
      () => {
        this._getAPIListCounty(this.state.data.supplier.cityID);
        this.props.form.validateFields(["cityID"], { force: true });
      }
    );
  };

  onChangeCounty = (key, value) => {
    // this.state.data.supplier.cityID = key;
    this.setState(
      {
        // data: this.state.data,
        errCounty: false,
      },
      () => {
        this._getAPIListCounty(this.state.data.supplier.cityID);
        this.props.form.validateFields(["districtID"], { force: true });
      }
    );
  };
  onChangeStatus = (value) => {
    this.state.dataEdit.status = value;
    this.setState({ objEdit: this.state.objEdit, errStatus: false }, () =>
      this.props.form.validateFields(["status"], { force: true })
    );
  };
  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      const data = {
        id: this.agencyID,
        warehouse_type: this.state.dataEdit.supplier_type_id,
        phone: this.state.dataEdit.phone,
        email: this.state.dataEdit.email,
        address: this.state.dataEdit.address,
        representative_name: this.state.dataEdit.representative_name,
        status: this.state.dataEdit.status,
        listCityId: this.state.listCityId,
        listDistrictId: [1],
      };

      this.setState(
        // {
        //   dataEdit: data
        // },
        () => {
          this._postAPIEditWarehouse(data);
        }
      );
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Container fluid>
        <Row className="mt-4" xs="auto">
          <ITitle
            level={1}
            title="Chỉnh sửa kho"
            style={{
              color: colors.main,
              fontWeight: "bold",
            }}
          />
        </Row>
        {!this.state.loading ? (
          <Form onSubmit={this.handleSubmit}>
            <Row
              style={{
                display: "flex",
                justifyContent: "flex-end",
                paddingTop: 32,
                paddingBottom: 32,
              }}
              xs="auto"
              sm={8}
              md={12}
            >
              <Form.Item>
                <IButton
                  icon={ISvg.NAME.SAVE}
                  title="Lưu"
                  color={colors.main}
                  htmlType="submit"
                  style={{ marginRight: 20 }}
                  onClick={() => this.handleSubmit}
                />
              </Form.Item>
              <Form.Item>
                <IButton
                  icon={ISvg.NAME.CROSS}
                  title="Hủy"
                  color={colors.oranges}
                  style={{}}
                  onClick={() => {
                    this.props.history.push("/listStorePage");
                  }}
                />
              </Form.Item>
            </Row>
            <div style={{ maxWidth: 815, minWidth: 615, flex: 1 }}>
              <Row>
                <Col style={{ background: colors.white }}>
                  <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                    <Col>
                      <ITitle
                        level={2}
                        title="Thông tin kho"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Col>
                  </Row>

                  <Row className="p-0 ml-3" xs="auto">
                    <Col className="p-0 m-0">
                      <Row className="p-0 m-0 ">
                        <ITitle
                          level={4}
                          title={"Tên kho"}
                          style={{ fontWeight: 700, color: "#A8ACAD" }}
                        />
                      </Row>
                      <Row className="p-0 m-0">
                        <ITitle
                          style={{ marginTop: 10, color: "#A8ACAD" }}
                          level={4}
                          title={this.state.dataEdit.name}
                        />
                      </Row>
                    </Col>
                  </Row>

                  <Row className="p-0 mt-2 ml-3 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0" style={{ color: colors.black }}>
                      <Row className="p-0 m-0">
                        <ITitle
                          level={4}
                          title={"Mã Kho"}
                          style={{ fontWeight: 700, color: "#A8ACAD" }}
                        />
                      </Row>
                      <Row className="p-0 m-0">
                        <ITitle
                          style={{ marginTop: 10, color: "#A8ACAD" }}
                          level={4}
                          title={this.state.dataEdit.code}
                        />
                      </Row>
                    </Col>

                    <Col
                      className="p-0 "
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator("supplierName", {
                          rules: [
                            {
                              message: "nhập tên cửa hàng đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Loại kho"}
                                style={{ fontWeight: 700, marginBottom: 10 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ISelect
                                value={this.state.dataEdit.supplier_type_id}
                                style={{ width: "100%" }}
                                select={true}
                                onChange={(value, key) => {
                                  this.state.dataEdit.supplier_type_id = value;
                                  this.setState({
                                    // dataEdit: this.state.dataEdit
                                  });
                                }}
                                data={this.state.listType}
                                isBackground={false}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="p-0 mt-2 ml-3 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0" style={{ color: colors.black }}>
                      <Form.Item>
                        {getFieldDecorator("phone", {
                          rules: [
                            {
                              message: "nhập tên cửa hàng đại lý",
                            },
                          ],
                        })(
                          <div>
                            {" "}
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Điện thoại"}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                style={{ marginTop: 10 }}
                                level={4}
                                value={this.state.dataEdit.phone}
                                onChange={(event) => {
                                  this.state.dataEdit.phone =
                                    event.target.value;
                                  this.setState({});
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>

                    <Col
                      className="p-0 "
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator("supplierName", {
                          rules: [
                            {
                              message: "nhập tên cửa hàng đại lý",
                            },
                          ],
                        })(
                          <div>
                            {" "}
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Email"}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                style={{ marginTop: 10 }}
                                level={4}
                                value={this.state.dataEdit.email}
                                onChange={(event) => {
                                  this.state.dataEdit.email =
                                    event.target.value;
                                  this.setState({});
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row className="p-0 mt-2 ml-3 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("supplierName", {
                          rules: [
                            {
                              message: "nhập tên cửa hàng đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row
                              className="p-0 m-0"
                              style={{ backgroungColor: "red" }}
                            >
                              <ITitle
                                level={4}
                                title={"Địa chỉ"}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row
                              className="p-0 m-0"
                              style={{ backgroungColor: "red" }}
                            >
                              <IInput
                                style={{
                                  marginTop: 10,
                                  width: "100%",
                                  borderTopWidth: 0,
                                }}
                                level={4}
                                value={this.state.dataEdit.address}
                                onChange={(event) => {
                                  this.state.dataEdit.address =
                                    event.target.value;
                                  this.setState({});
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="p-0 mt-2 ml-3 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("supplierName", {
                          rules: [
                            {
                              message: "nhập tên cửa hàng đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Đại diện"}
                                style={{ fontWeight: 700 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                style={{ marginTop: 10 }}
                                level={4}
                                value={this.state.dataEdit.representative_name}
                                onChange={(event) => {
                                  this.state.dataEdit.representative_name =
                                    event.target.value;
                                  this.setState({});
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>

                    <Col
                      className="p-0  ml-3 mb-0 mr-0"
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator("supplierName", {
                          rules: [
                            {
                              message: "nhập tên cửa hàng đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Trạng thái"}
                                style={{ fontWeight: 700, marginBottom: 10 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ISelectStatus
                                type="STORE"
                                onChange={this.onChangeStatus}
                                placeholder="Chọn trạng thái"
                                value={this.state.dataEdit.status}
                                isBackground={false}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="p-0 mt-3 mb-4 ml-3 mr-0">
                    <ITitle
                      level={2}
                      title="Khu vực"
                      style={{ color: colors.black, fontWeight: "bold" }}
                    />
                  </Row>
                  <Row className="p-0 mt-2 ml-3 mb-0 mr-0" xs="auto">
                    <Col xs={6} className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("supplierName", {
                          rules: [
                            {
                              message: "nhập tên cửa hàng đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0">
                              <ITitle
                                level={4}
                                title={"Tỉnh/Thành Phố"}
                                style={{ fontWeight: 700, marginBottom: 26 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <ISelectCitiesEmpty
                                supplier_id={this.agencyID}
                                mode="multiple"
                                value={this.state.listCityId}
                                onChange={(arr) => {
                                  this.setState({
                                    listCityId: arr,
                                  });
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </Form>
        ) : (
          <ILoading />
        )}
      </Container>
    );
  }
}

const editStore = Form.create({ name: "EditStore" })(EditStore);

export default editStore;
