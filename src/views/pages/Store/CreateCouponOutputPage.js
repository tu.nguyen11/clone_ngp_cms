import { Table, Typography, Avatar, Button, message, DatePicker } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
  IInput,
  IDatePicker
} from "../../components";
import { colors } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { APIService } from "../../../services";
import { thisExpression } from "@babel/types";
import QueueAnim from "rc-queue-anim";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={3} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={3} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 13;
  } else widthColumn = (widthScreen - 300) / 11;
  return widthColumn * type;
}

export default class CreateCouponOnputPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      width: 0,
      loading: false,
      bool: false,
      status: 0,
      dataTable: [],
      data: {},
      keySearch: "",
      category_id: 0,
      type: 0,
      // loadingTable: true,
      page: 1,
      value: "", // Check here to configure the default column
      arrayRemove: [],
      Cate: [
        { value: "12", key: 1 },
        { value: "13", key: 2 }
      ]
    };
  }
  componentDidMount() {
    this._getDetailStockInput(1);
  }

  _getDetailStockInput = async (page, status, keySearch, category_id, type) => {
    try {
      const data = await APIService._getDetailStockInput(
        page,
        status,
        keySearch,
        category_id,
        type
      );
      let product = data.warehouse_import.listProduct;
      product.map((item, index) => {
        product[index].stt = index + 1;
        product[index].idAndStt = {
          id: item.id,
          index: index
        };
      });
      product.push({
        productCode: "Tổng tiền",
        voucherPackage: data.warehouse_import.sumVoucherPackage,
        voucherQuantity: data.warehouse_import.sumVoucherQuantity,
        realityPackage: data.warehouse_import.sumRealityPackage,
        realityQuantity: data.warehouse_import.sumRealityQuantity,
        realityPrice: data.warehouse_import.sumRealityPrice
      });
      this.setState({
        dataTable: product,
        data: data,
        loadingTable: false
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false
      });
    }
  };
  _renderInfor = () => {
    var item = this.state.data.warehouse_import;
    return (
      <Col
        style={{
          paddingLeft: 140,
          paddingRight: 210
        }}
      >
        <Row>
          <ITitle
            level={3}
            title={"Phiếu xuất kho"}
            style={{ fontWeight: "bold" }}
          />
        </Row>
        <Row className="mt-3 mb-3" xs={4}>
          <Col>
            <Row>
              <ITitle
                level={3}
                title={"Tên kho hàng"}
                style={{ fontWeight: 500 }}
              />
            </Row>
            <Row>
              <ICascader
                type="category"
                level={2}
                style={{ width: "100%" }}
                placeholder="Chọn nhóm sản phẩm"
              />
            </Row>
          </Col>
          <Col style={{ marginLeft: 40, marginRight: 35 }}>
            <Row>
              <ITitle
                level={3}
                title={"Mã phiếu"}
                style={{ fontWeight: 500 }}
              />
            </Row>
            <Row>
              <IInput placeholder="xuất mã phiếu xuất" />
            </Row>
          </Col>
          <Col style={{ marginRight: 40 }}>
            <Row>
              <ITitle
                level={3}
                title={"Người lập phiếu"}
                style={{ fontWeight: 500 }}
              />
            </Row>
            <Row>
              <ICascader
                type="category"
                level={2}
                style={{ width: "100%" }}
                placeholder="Chọn nhóm sản phẩm"
              />
            </Row>
          </Col>
          <Col xs="6">
            <Row>
              <ITitle level={3} title={"Ghi chú"} style={{ fontWeight: 500 }} />
            </Row>
            <Row>
              <IInput placeholder="Nhập kho điều chuyển từ kho số 4 Thủ Đức" />
            </Row>
          </Col>
        </Row>
        <Row xs={4}>
          <Col>
            <Row>
              <ITitle
                level={3}
                title={"Thời gian xuất kho"}
                style={{ fontWeight: 500 }}
              />
            </Row>
            <Row>
              <DatePicker
                showTime
                suffixIcon={() => null}
                placeholder="xuất ngày kho"
                size="large"
                // style={{ width: "100%" }}
                // onChange={this.onOkDatePickerEnd}
              />
            </Row>
          </Col>
          <Col
            style={{
              marginLeft: 40,
              marginRight: 35
            }}
          >
            <Row>
              <ITitle
                level={3}
                title={"Loại hình"}
                style={{ fontWeight: 500 }}
              />
            </Row>
            <Row>
              <ICascader
                type="category"
                level={2}
                style={{ width: "100%" }}
                placeholder="Chọn nhóm sản phẩm"
              />
            </Row>
          </Col>
          <Col style={{ marginRight: 40 }}>
            <Row>
              <ITitle
                level={3}
                title={"Người tiếp nhận"}
                style={{ fontWeight: 500 }}
              />
            </Row>
            <Row>
              <IInput placeholder="Nhập kho điều chuyển từ kho số 4 Thủ Đức" />
            </Row>
          </Col>
          <Col xs="6">
            <Row style={{ height: "100%" }}>
              <Col xs={4}>
                <Row>
                  <ITitle
                    level={3}
                    title={"Kho tiếp nhận"}
                    style={{ fontWeight: 500 }}
                  />
                </Row>
                <Row>
                  <ICascader
                    type="category"
                    level={2}
                    style={{ width: "100%" }}
                    placeholder="Chọn nhóm sản phẩm"
                  />
                </Row>
              </Col>
              <Col xs={3}>
                <Row>
                  <ITitle level={3} title={""} style={{ fontWeight: 500 }} />
                </Row>
                <Row>
                  <ITitle style={{ marginTop: 10 }} level={3} title={""} />
                </Row>
              </Col>
              <Col xs={2}></Col>
            </Row>
          </Col>
        </Row>
      </Col>
    );
  };
  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",
        render: (id, a, index) => {
          if (index == this.state.dataTable.length - 1) {
            return null;
          }
          return _renderColumns(id);
        }
      },
      {
        title: _renderTitle("Mã sản phẩm"),
        dataIndex: "productCode",
        key: "productCode",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: productCode => _renderColumns(productCode)
      },
      {
        title: _renderTitle("Mã NCC"),
        dataIndex: "agencyCode",
        key: "agencyCode",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: agencyCode => _renderColumns(agencyCode)
      },
      {
        title: _renderTitle("Tên sản phẩm"),
        dataIndex: "productName",
        key: "productName",
        align: "left",
        width: _widthColumn(this.state.width, 2),

        render: productName => _renderColumns(productName)
      },
      {
        title: _renderTitle("Đơn vị"),
        dataIndex: "variationName",
        key: "variationName",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: variationName => <ITitle level={4} title={variationName} />
      },
      {
        title: _renderTitle("Chứng từ"),
        align: "center",
        children: [
          {
            title: _renderTitle("Tổng kiện"),
            dataIndex: "voucherPackage",
            key: "voucherPackage  ",
            align: "center",
            width: 100,
            render: voucherPackage => _renderColumns(voucherPackage)
          },
          {
            title: _renderTitle("Tổng SL"),
            dataIndex: "voucherQuantity",
            key: "voucherQuantity",
            align: "center",
            width: 100,
            render: voucherQuantity => _renderColumns(voucherQuantity)
          }
        ]
      },
      {
        title: _renderTitle("Thực nhận"),
        align: "center",
        children: [
          {
            title: _renderTitle("Tổng kiện"),
            dataIndex: "realityPackage",
            key: "realityPackage",

            align: "center",
            width: 100,
            render: realityPackage => _renderColumns(realityPackage)
          },
          {
            title: _renderTitle("Tổng SL"),
            dataIndex: "realityQuantity",
            key: "realityQuantity",
            align: "center",
            width: 100,
            render: realityQuantity => _renderColumns(realityQuantity)
          },
          {
            title: _renderTitle("Giá trị"),
            dataIndex: "realityPrice",
            key: "realityPrice",
            align: "center",
            width: 100,
            render: realityPrice => _renderColumns(realityPrice)
          }
        ]
      },
      {
        title: _renderTitle("Ghi chú"),
        dataIndex: "note",
        key: "note",
        align: "center",
        width: 209,
        // fixed: "left",
        render: note => _renderColumns(note)
      }
    ];
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Tạo phiếu xuất"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end"
            }}
          />
        </Row>
        <Row className="center" style={{ marginTop: 36 }}>
          <Col
            style={{
              display: "flex",
              justifyContent: "flex-end"
            }}
          >
            <Row>
              <Col xs="auto" className="m-0 p-0">
                <Row>
                  <IButton
                    icon={ISvg.NAME.SAVE}
                    title="Lưu"
                    htmlType="submit"
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() => this.handleSubmit}
                  />
                  <IButton
                    icon={ISvg.NAME.CROSS}
                    title="Hủy"
                    color={colors.oranges}
                    style={{ marginRight: 20 }}
                    onClick={() => {
                      // this.props.history.push("/List");
                    }}
                  />
                  {/* 
                  <IButton
                    icon={ISvg.NAME.DELETE}
                    title="Xóa"
                    color={colors.oranges}
                    style={{ marginRight: 20 }}
                    onClick={() => {
                      if (this.state.arrayRemove.length == 0) {
                        message.warning("Bạn chưa chọn mã sản phẩm để xóa.");
                        return;
                      }
                      const objRemove = {
                        id: this.state.arrayRemove,
                        status: -1 // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                      };
                      this._APIpostRemoveProduct(objRemove);
                    }}
                  /> */}
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>

        {this._renderInfor()}
        <Row style={{ paddingLeft: 140, paddingRight: 210, marginTop: 20 }}>
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%", background: colors.white }}
            defaultCurrent={2}
            scroll={{ x: 1200, y: 600 }}
            hidePage={true}
          />
        </Row>
      </Container>
    );
  }
}
