import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  ISelectWareHouse,
  ISelectStatus,
  IInput,
  ICascader,
} from "../../components";
import { colors, images } from "../../../assets";
import { message, Button, Upload, Icon, DatePicker, Form } from "antd";
import { ExcelToJson } from "../../../utils/ExcelUtil";
import { APIService } from "../../../services";
import moment from "moment";
let dateFormat = "dd/MM/yyyy";
function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}
function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 13;
  } else widthColumn = (widthScreen - 300) / 11;
  return widthColumn * type;
}

class CreateCouponPage extends Component {
  constructor(props) {
    super(props);
    this.type = this.props.match.params.type;
    this.data = this.props.location.state;
    this.state = {
      tableHeight: "",
      shippers: [],
      type_warehouse: 0,
      keyStatus: 2, // lấy tất cả
      dataStock: [],
      nameStock: "",
      CateInput: [
        {
          key: 1,
          value: "Nhập kho",
        },
      ],
      CateOutput: [
        {
          key: 2,
          value: "Xuất kho",
        },
        {
          key: 3,
          value: "Xuất chuyển kho",
        },
      ],
      activeStock: false,
      dataInfor: {
        id: 0,
        import_date: 0,
        warehouse_id: 0,
        note: "",
        code: "",
        founder: "",
        reciever: "",
        type_id: this.type == "INPUT" ? 1 : 2, // 1 xuất kho , 2 là xuất chuyển kho
        warehouse_reciever: 0,
        listproductImport: [],
        listproductExport: [],
      },
      warehouse_name: this.data ? this.data.dataInfor.warehouse_name : "",
    };

    this.tableContainer = React.createRef();
  }
  _renderTotal(title, index) {
    if (this.state.shippers.length == 1) {
      var obj = {
        children: null,
        props: { colSpan: 0 },
      };
      return obj;
    }
    if (index == this.state.shippers.length - 1) {
      var obj = {
        children: (
          <ITitle
            level={4}
            title={title}
            style={{
              color: colors.main,
              fontWeight: "bold",
              fontSize: 16,
            }}
          />
        ),
      };
      return obj;
    }
    return this._renderColumns(title);
  }
  _renderColumns(title, index) {
    if (
      index == this.state.shippers.length - 1 ||
      this.state.shippers.length == 1
    ) {
      var obj = {
        children: null,
        props: { colSpan: 0 },
      };
      return obj;
    }
    return (
      <div
        style={{
          wordWrap: "break-word",
          wordBreak: "break-word",
        }}
      >
        <ITitle level={4} title={title} />
      </div>
    );
  }
  componentDidMount() {
    this._getListStock();

    // khởi tạo để có giá trị hiển thị input type=file
    this.state.shippers.push({
      stt: 1,
      code: "VH323131",
      name: "Trần Dần",
      phone: "01234567890",
      email: "TranTiger@gmail.com",
    });

    this.setState({
      shippers: this.state.shippers,
    });

    setTimeout(() => {
      try {
        this.setState({
          tableHeight:
            this.tableContainer.current.parentElement.clientHeight - 200,
        }); // 360
      } catch (error) {}
    }, 1);
  }

  _getListStock = async () => {
    try {
      const data = await APIService._getListStock();
      const dataSelect = data.list_type.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      this.setState({ dataStock: dataSelect });
    } catch (err) {
      console.log(err);
    }
  };
  onOkDatePickerStart = (date, dateString) => {
    this.state.dataInfor.import_date = Date.parse(dateString);
    this.setState({});
    // message.warning(this.state.import_date);
  };
  onChangeSelect = (key, obj) => {
    this.state.dataInfor.warehouse_id = key;
    this.setState({});
  };
  onChangeSelectWarehouseReciever = (key, obj) => {
    this.state.dataInfor.warehouse_reciever = key;
    this.setState({});
  };

  _importFile() {
    return (
      <input
        type="file"
        style={{
          opacity: 0,
          position: "absolute",
          width: 170,
          height: 42,
          background: "red",
        }}
        onChange={async (e) => {
          var arrKey = [
            "stt",
            "product_code",
            "agency_code",
            "product_name",
            "variation_name",
            "total_package",
            "quantity",
            "note",
            "total_money",
          ];

          const data = await ExcelToJson(e.target.files[0], 4, arrKey);

          var sumPackage = 0;
          var sumQuanlity = 0;
          data.map((item, index) => {
            sumPackage = sumPackage + item.total_package;
            sumQuanlity = sumQuanlity + item.quantity;
          });
          // data.push({
          //   // product_id: "Tổng tiền",
          //   // voucherPackage: data.warehouse_import.sumVoucherPackage,
          //   // voucherQuantity: data.warehouse_import.sumVoucherQuantity,
          //   total_package: sumPackage,
          //   quantity: sumQuanlity
          // });
          this.type == "INPUT"
            ? (this.state.dataInfor.listproductImport = data)
            : (this.state.dataInfor.listproductExport = data);
          let dataNewExecl = [...data];
          dataNewExecl.push({
            total_package: sumPackage,
            quantity: sumQuanlity,
          });
          this.setState({
            dataInfor: this.state.dataInfor,
            shippers: [...dataNewExecl],
          });
        }}
      ></input>
    );
  }
  handleSubmit = () => {
    // var check =
    //   this.state.dataInfor.code == "" ||
    //   this.state.dataInfor.founder == "" ||
    //   this.state.dataInfor.import_date == 0 ||
    //   this.state.dataInfor.note == "" ||
    //   this.state.dataInfor.reciever == "" ||
    //   this.state.dataInfor.warehouse_reciever == "" ||
    //   this.type == "INPUT"
    //     ? this.state.dataInfor.listproductImport.length == 0
    //     : this.state.dataInfor.listproductExport.length == 0;
    // if (check) {
    //   return message.warning("ádasdas");
    // }
    // message.warning("đủ");
    const didImportExcel =
      this.type == "INPUT"
        ? this.state.dataInfor.listproductImport.length !== 0
        : this.state.dataInfor.listproductExport.length !== 0;
    const warehouse_reciever =
      this.type == "INPUT" ? null : this.state.dataInfor.warehouse_reciever;
    if (
      this.state.dataInfor.code == "" ||
      this.state.dataInfor.code == "" ||
      this.state.dataInfor.founder == "" ||
      this.state.dataInfor.import_date == 0 ||
      // this.state.dataInfor.note == "" ||
      this.state.dataInfor.reciever == "" ||
      warehouse_reciever ||
      !didImportExcel
    ) {
      return message.warning("Vui lòng nhập đầy đủ thông tin");
    }
    this.type == "INPUT" ? this._postImportAddNew() : this._postExportAddNew();
  };
  _postImportAddNew = async () => {
    const data = {
      id: 0,
      import_date: this.state.dataInfor.import_date,
      warehouse_id: this.state.dataInfor.warehouse_id,
      note: this.state.dataInfor.note,
      code: this.state.dataInfor.code,
      founder: this.state.dataInfor.founder,
      reciever: this.state.dataInfor.reciever,
      type_id: this.state.dataInfor.type_id,
      listproductImport: this.state.dataInfor.listproductImport,
    };

    try {
      await APIService._postImportAddNew(data);
      message.success("Tạo thành công");
      this.props.history.push("/listStoreInputPage");
    } catch (error) {}
  };
  _postExportAddNew = async () => {
    const data = {
      id: 0,
      import_date: this.state.dataInfor.import_date,
      warehouse_id: this.state.dataInfor.warehouse_id,
      note: this.state.dataInfor.note,
      code: this.state.dataInfor.code,
      founder: this.state.dataInfor.founder,
      reciever: this.state.dataInfor.reciever,
      type_id: this.state.dataInfor.type_id,
      warehouse_reciever: 0,
      listproductExport: this.state.dataInfor.listproductExport,
    };
    try {
      await APIService._postExportAddNew(data);
      message.success("Tạo thành công");
      this.props.history.push("/listStoreOutputPage");
    } catch (error) {}
  };
  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",

        width: 80,
        render: (id, row, index) => {
          if (this.state.shippers.length === 1) {
            var obj = {
              children: (
                <div
                  style={{
                    height: 300,
                    alignItems: "center",
                    justifyContent: "center",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  {this._importFile()}
                  <ISvg
                    name={ISvg.NAME.ADDFILE}
                    width={60}
                    height={60}
                    fill={"white"}
                  />
                  <ITitle
                    level={3}
                    title="Nhấp chọn hoặc kéo thả file để tải lên danh sách các sản phẩm nhập kho"
                    style={{ color: colors.text.gray }}
                  />
                </div>
              ),
              props: { colSpan: 13 },
            };

            return obj;
          }
          if (index == this.state.shippers.length - 1) {
            var obj = {
              children: (
                <ITitle
                  level={4}
                  title={"Tổng cộng"}
                  style={{
                    color: colors.main,
                    fontWeight: "bold",
                    fontSize: 16,
                  }}
                />
              ),
              props: { colSpan: 4 },
            };
            return obj;
          }
          var obj = {
            children: this._renderColumns(id),
            props: {},
          };
          return obj;
        },
      },
      {
        title: _renderTitle("Mã sản phẩm"),
        dataIndex: "product_code",
        key: "product_code",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (product_code, row, index) =>
          this._renderColumns(product_code, index),
      },
      {
        title: _renderTitle("Mã NCC"),
        dataIndex: "agency_code",
        key: "agency_code",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (agencyCode, row, index) =>
          this._renderColumns(agencyCode, index),
      },
      {
        title: _renderTitle("Tên sản phẩm"),
        dataIndex: "product_name",
        key: "product_name",
        align: "left",
        width: _widthColumn(this.state.width, 2),

        render: (product_name, index) =>
          this._renderColumns(product_name, index),
      },
      {
        title: _renderTitle("Đơn vị"),
        dataIndex: "variation_name",
        key: "variation_name",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (variation_name, row, index) =>
          this._renderColumns(variation_name, index),
      },
      {
        title: _renderTitle("Thực nhận"),
        align: "center",
        children: [
          {
            title: _renderTitle("Tổng kiện"),
            dataIndex: "total_package",
            key: "total_package",
            align: "center",
            width: 100,
            render: (total_package, row, index) =>
              this._renderTotal(total_package, index),
          },
          {
            title: _renderTitle("Tổng SL"),
            dataIndex: "quantity",
            key: "quantity",
            align: "center",
            width: 100,
            render: (quantity, row, index) =>
              this._renderTotal(quantity, index),
          },
          // {
          //   title: _renderTitle("Giá trị"),
          //   dataIndex: "total_money",
          //   key: "total_money",
          //   align: "center",
          //   width: 100,
          //   render: (total_money, row, index) =>
          //     this._renderTotal(total_money, index)
          // }
        ],
      },
      {
        title: _renderTitle("Ghi chú"),
        dataIndex: "note",
        key: "note",
        align: "center",
        width: 209,
        // fixed: "left",
        render: (note) => this._renderColumns(note),
      },
    ];
    return (
      <Container className="p-0 m-0" fluid style={{ flex: 1 }}>
        <Row className="p-0 m-0 mt-3">
          <ITitle
            level={1}
            title={this.type == "INPUT" ? "Tạo phiếu nhập" : "Tạo phiếu xuất"}
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
            }}
          />
        </Row>
        <Row className="m-0 p-0 mt-4">
          <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}></Row>
          <Row style={{ marginRight: 20 }}>
            <IButton
              icon={ISvg.NAME.UPLOAD}
              title="Nhập file"
              color={colors.main}
            />
            {this._importFile()}
          </Row>
          <Row className="m-0 p-0">
            <IButton
              icon={ISvg.NAME.SAVE}
              title="Lưu"
              htmlType="submit"
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={this.handleSubmit}
            />
            <IButton
              icon={ISvg.NAME.CROSS}
              title="Hủy"
              color={colors.oranges}
              style={{}}
              onClick={() => {
                window.location.reload(true);
              }}
            />
          </Row>
        </Row>
        {this._renderInfor()}
        {this._importFile()}

        <Row className="m-0 p-0" style={{ height: 600 }}>
          <div
            className="m-0 p-0 mt-4 "
            ref={this.tableContainer}
            style={{ flex: 1 }}
          >
            <ITable
              data={this.state.shippers}
              columns={columns}
              //loading={this.state.loadingTable}
              // bodyStyle={{
              //   height: this.state.tableHeight
              // }}
              style={{ width: "100%", background: colors.background }}
              scroll={{ y: this.state.tableHeight - 50 }}
              defaultCurrent={1}
              hidePage
            />
          </div>
        </Row>
      </Container>
    );
  }
  _renderInfor = () => {
    return (
      <Col className="m-0 p-0 mt-4">
        <Row className="m-0 p-0">
          <Col className="ml-0 p-0">
            <ITitle
              level={3}
              title={"Tên kho hàng"}
              style={{ fontWeight: 500 }}
            />
            <Form.Item>
              <ISelect
                // defaultValue={this.state.warehouse_name}
                // type="write"
                isBackground={false}
                style={{
                  borderTopWidth: 0,
                  borderLeftWidth: 0,
                  borderRightWidth: 0,
                  borderStyle: "solid",
                  borderBottomWidth: 1,
                  borderBottomColor: colors.line,
                  width: "100%",
                }}
                select={true}
                placeholder="Chọn kho hàng"
                data={this.state.dataStock}
                onChange={this.onChangeSelect}
              />
            </Form.Item>
          </Col>
          <Col>
            <ITitle level={3} title={"Mã phiếu"} style={{ fontWeight: 500 }} />

            <IInput
              placeholder={
                this.type == "INPUT"
                  ? "Nhập mã phiếu nhập"
                  : "Nhập mã phiếu xuất"
              }
              onChange={(event) => {
                this.state.dataInfor.code = event.target.value;
                this.setState({});
              }}
              value={this.state.dataInfor.code}
            />
          </Col>
          <Col>
            <ITitle
              level={3}
              title={"Người lập phiếu"}
              style={{ fontWeight: 500 }}
            />
            <IInput
              level={2}
              style={{ width: "100%" }}
              placeholder="Nhập người lập phiếu"
              onChange={(event) => {
                this.state.dataInfor.founder = event.target.value;
                this.setState({
                  dataInfor: this.state.dataInfor,
                });
              }}
              value={this.state.dataInfor.founder}
            />
          </Col>
          <Col className="mr-0 p-0">
            <ITitle level={3} title={"Ghi chú"} style={{ fontWeight: 500 }} />
            <IInput
              placeholder="Nhập ghi chú cho phiếu"
              onChange={(event) => {
                this.state.dataInfor.note = event.target.value;
                this.setState({});
              }}
              value={this.state.dataInfor.note}
            />
          </Col>
        </Row>
        <Row className="m-0 p-0">
          <Col className="ml-0 p-0">
            <ITitle
              level={3}
              title={
                this.type == "INPUT"
                  ? "Thời gian nhập kho"
                  : "Thời gian xuất kho"
              }
              style={{ fontWeight: 500 }}
            />
            <div className="clear-pad">
              <DatePicker
                showTime
                suffixIcon={() => null}
                placeholder="Nhập thời gian"
                size="large"
                style={{
                  width: "100%",
                  borderBottom: "1px solid #ccc",
                  padding: 0,
                }}
                onChange={this.onOkDatePickerStart}
              />
            </div>
          </Col>
          <Col>
            <ITitle level={3} title={"Loại hình"} style={{ fontWeight: 500 }} />
            <ISelect
              isBackground={false}
              defaultValue={
                this.type == "INPUT"
                  ? this.state.CateInput[0].value
                  : this.state.CateOutput[0].value
              }
              type="write"
              select={true}
              placeholder="Chọn loại hình"
              data={
                this.type == "INPUT"
                  ? this.state.CateInput
                  : this.state.CateOutput
              }
              onChange={(value, key) => {
                if (value == 2) {
                  this.state.dataInfor.type_id = value;
                  this.setState({ activeStock: false });
                } else {
                  this.state.dataInfor.type_id = value;
                  this.setState({ activeStock: true });
                }
              }}
            />
          </Col>
          <Col>
            <ITitle
              level={3}
              title={this.type == "INPUT" ? "Người nhập kho" : "Người xuất kho"}
              style={{ fontWeight: 500 }}
            />
            <IInput
              placeholder={
                this.type == "INPUT"
                  ? "Nhập tên người nhập kho"
                  : "Nhập tên người xuất kho"
              }
              onChange={(event) => {
                this.state.dataInfor.reciever = event.target.value;
                this.setState({});
              }}
              value={this.state.dataInfor.reciever}
            />
          </Col>
          <Col className="mr-0 p-0"></Col>
        </Row>
      </Col>
    );
    return (
      <Col style={{}}>
        <Row>
          <ITitle
            level={3}
            title={this.type == "INPUT" ? "PHIẾU NHẬP KHO" : "PHIẾU XUẤT KHO"}
            style={{ fontWeight: "bold" }}
          />
        </Row>
        <Row className="mt-3 mb-3" xs={8}>
          <Col>
            <ITitle
              level={3}
              title={"Tên kho hàng"}
              style={{ fontWeight: 500 }}
            />
            <Form.Item>
              <ISelect
                defaultValue={this.state.warehouse_name}
                // type="write"
                select={true}
                placeholder="Chọn kho hàng"
                data={this.state.dataStock}
                onChange={this.onChangeSelect}
              />
            </Form.Item>
          </Col>
          <Col>
            <ITitle level={3} title={"Mã phiếu"} style={{ fontWeight: 500 }} />

            <IInput
              placeholder={
                this.type == "INPUT"
                  ? "Nhập mã phiếu nhập"
                  : "Nhập mã phiếu xuất"
              }
              onChange={(event) => {
                this.state.dataInfor.code = event.target.value;
                this.setState({});
              }}
              value={this.state.dataInfor.code}
            />
          </Col>
          <Col>
            <Row>
              <ITitle
                level={3}
                title={"Người lập phiếu"}
                style={{ fontWeight: 500 }}
              />
              <IInput
                level={2}
                style={{ width: "100%" }}
                placeholder="Nhập người lập phiếu"
                onChange={(event) => {
                  this.state.dataInfor.founder = event.target.value;
                  this.setState({});
                }}
                value={this.state.dataInfor.founder}
              />
            </Row>
          </Col>
          <Col xs={4}>
            <ITitle level={3} title={"Ghi chú"} style={{ fontWeight: 500 }} />
            <IInput
              placeholder="Nhập ghi chú cho phiếu"
              onChange={(event) => {
                this.state.dataInfor.note = event.target.value;
                this.setState({});
              }}
              value={this.state.dataInfor.note}
            />
          </Col>
        </Row>
        <Row xs={8} className="m-0 p-0">
          <Col>
            <ITitle
              level={3}
              title={
                this.type == "INPUT"
                  ? "Thời gian nhập kho"
                  : "Thời gian xuất kho"
              }
              style={{ fontWeight: 500 }}
            />
            <DatePicker
              // value={moment(
              //   new Date(this.state.dataInfor.import_date),
              //   dateFormat
              // )}
              showTime
              suffixIcon={() => null}
              placeholder="Nhập thời gian"
              size="large"
              style={{ width: "100%" }}
              onChange={this.onOkDatePickerStart}
              // value={item.dataInfor.time}
            />
          </Col>
          <Col style={{}} className="m-0 p-0">
            <ITitle level={3} title={"Loại hình"} style={{ fontWeight: 500 }} />

            <ISelect
              defaultValue={
                this.type == "INPUT"
                  ? this.state.CateInput[0].value
                  : this.state.CateOutput[0].value
              }
              type="write"
              select={true}
              placeholder="Chọn loại hình"
              data={
                this.type == "INPUT"
                  ? this.state.CateInput
                  : this.state.CateOutput
              }
              onChange={(value, key) => {
                if (value == 2) {
                  this.state.dataInfor.type_id = value;
                  this.setState({ activeStock: false });
                } else {
                  this.state.dataInfor.type_id = value;
                  this.setState({ activeStock: true });
                }
              }}
            />
          </Col>
          <Col>
            <ITitle
              level={3}
              title={this.type == "INPUT" ? "Người nhập kho" : "Người xuất kho"}
              style={{ fontWeight: 500 }}
            />
            <IInput
              placeholder={
                this.type == "INPUT"
                  ? "Nhập tên người nhập kho"
                  : "Nhập tên người xuất kho"
              }
              onChange={(event) => {
                this.state.dataInfor.reciever = event.target.value;
                this.setState({});
              }}
              value={this.state.dataInfor.reciever}
            />
          </Col>
          <Col xs={4}>
            <Row style={{}}>
              {this._renderwarehouse()}
              <Col xs={3}>
                <Row>
                  <ITitle level={3} title={""} style={{ fontWeight: 500 }} />
                </Row>
                <Row>
                  <ITitle style={{ marginTop: 10 }} level={3} title={""} />
                </Row>
              </Col>
              <Col xs={2}></Col>
            </Row>
          </Col>
        </Row>
      </Col>
    );
  };
  _renderwarehouse() {
    if (this.type == "INPUT") {
      return null;
    }
    return this.state.activeStock ? (
      <Col xs={4}>
        <Row>
          <ITitle
            level={3}
            title={"Kho tiếp nhận"}
            style={{ fontWeight: 500 }}
          />
        </Row>
        <Row>
          <ISelect
            type="write"
            select={true}
            placeholder="Chọn kho tiếp nhận"
            data={this.state.dataStock}
            onChange={this.onChangeSelectWarehouseReciever}
          />
        </Row>
      </Col>
    ) : (
      <Col xs={4}></Col>
    );
  }
}
export default CreateCouponPage;
