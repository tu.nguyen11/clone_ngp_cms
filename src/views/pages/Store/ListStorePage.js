import { Table, Typography, Avatar, Button, message } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
} from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { APIService } from "../../../services";
import { thisExpression } from "@babel/types";
import QueueAnim from "rc-queue-anim";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 8;
  } else widthColumn = (widthScreen - 160) / 8;
  return widthColumn * type;
}

export default class ListStorePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      width: 0,
      loading: false,
      bool: false,
      status: 0,
      dataTable: [],
      data: {},
      keySearch: "",
      category_id: 0,
      type: 0,
      region: 0,
      keyword: "",
      loadingTable: true,
      page: 1,
      value: "", // Check here to configure the default column
      arrayRemove: [],
      id: "",
      name: "",
      typeWarehouse: [],
      warehouse_types: [],
      listType: [],
    };
    this.handleResize = this.handleResize.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
  }
  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }
  componentDidMount() {
    this._getListWarehouse(
      this.state.type,
      this.state.region,
      this.state.keyword,
      this.state.page // caterory cha
    );
    this._getTypeWarehouse();
    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  _getListWarehouse = async (type, region, keyword, page) => {
    try {
      const data = await APIService._getListWarehouse(
        type,
        region,
        keyword,
        page
      );

      let warehouses = data.warehouses;
      warehouses.map((item, index) => {
        warehouses[index].stt = (this.state.page - 1) * 10 + index + 1;
        warehouses[index].idAndStt = {
          id: item.id,
          index: index,
        };
      });
      this.setState({
        dataTable: warehouses,
        data: data,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  _getTypeWarehouse = async () => {
    const data = await APIService._getTypeWarehouse();
    var typeWH = [];
    typeWH.push({
      key: 0,
      value: "Tất cả",
    });
    data.warehouse_types.map((item, index) => {
      const key = item.id;
      const value = item.name;
      typeWH.push({ key, value });
    });
    this.setState({
      listType: typeWH,
    });
  };

  onSelectChange = (selectedRowKeys) => {
    // const Id = this.state.dataTable[selectedRowKeys[0]].id;
    let arrID = [];
    selectedRowKeys.map((item, index) => {
      const Id = this.state.dataTable[item].id;
      arrID.push(Id);
    });
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys, arrayRemove: arrID });
  };

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
      this.setState(
        {
          type: value[0],
          category_id: 0,
        },
        () =>
          this._getListProduct(
            this.state.page,
            this.state.status,
            this.state.keySearch,
            this.state.category_id,
            this.state.type // caterory cha
          )
      );
    }
    if (value.length == 2) {
      this.setState(
        {
          type: value[0],
          category_id: value[1],
        },
        () =>
          this._getListProduct(
            this.state.page,
            this.state.status,
            this.state.keySearch,
            this.state.category_id,
            this.state.type // caterory cha
          )
      );
    }
  };

  _APIpostRemoveWarehouse = async (objRemove) => {
    try {
      const data = await APIService._postRemoveWarehouse(objRemove);
      message.success("Xóa thành công");
      this.setState(
        {
          selectedRowKeys: [],
        },
        () =>
          this._getListWarehouse(
            this.state.type,
            this.state.region,
            this.state.keyword,
            this.state.page // caterory cha
          )
      );
    } catch (err) {
      console.log(err);
    }
  };
  _onFilter = async (value, selectedOptions) => {
    try {
      const data = await APIService._getListWarehouse(
        0,
        this.state.page,
        selectedOptions[0].value,
        selectedOptions[1].value,
        this.state.key
      );
      let suppliers = data.suppliers;
      suppliers.map((item, index) => {
        suppliers[index].stt = (this.state.page - 1) * 10 + index + 1;
        suppliers[index].sttAndId = {
          id: item.id,
          stt: index,
        };
        suppliers[index].fullAddress =
          item.address + "," + item.districtsName + "," + item.cityName;
      });
      this.setState({
        data: suppliers,
        total: data.total,
        sizeItem: data.size,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  render() {
    // if (this.state.data.warehouses.warehouse_type
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Trạng Thái"),
        dataIndex: "status",
        key: "status",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (status) =>
          _renderColumns(status == 1 ? "Đang hoạt động" : "Ngưng hoạt động"),
      },
      {
        title: _renderTitle("Mã kho"),
        dataIndex: "warehouse_code",
        key: "warehouse_code",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (warehouse_code) => _renderColumns(warehouse_code),
      },
      {
        title: _renderTitle("Tên kho"),
        dataIndex: "warehouse_name",
        key: "warehouse_name",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (warehouse_name) => <ITitle level={4} title={warehouse_name} />,
      },
      {
        title: _renderTitle("Địa chỉ"),
        dataIndex: "warehouse_address",
        key: "warehouse_address",
        align: "left",
        width: _widthColumn(this.state.width, 2),

        render: (warehouse_address) => _renderColumns(warehouse_address),
      },
      {
        title: _renderTitle("Số điện thoại"),
        dataIndex: "warehouse_phone",
        key: "warehouse_phone",
        // align: "center",
        width: _widthColumn(this.state.width, 1),

        render: (warehouse_phone) => _renderColumns(warehouse_phone),
      },
      {
        title: _renderTitle("Người đại diện"),
        dataIndex: "representative",
        key: "representative",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (representative) => _renderColumns(representative),
      },
      {
        title: _renderTitle("Loại kho"),
        dataIndex: "warehouse_type",
        key: "warehouse_type",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (warehouse_type) =>
          _renderColumns(
            this.state.listType.map((item, index) => {
              if (item.key == warehouse_type) return item.value;
            })
          ),
      },
      // {
      //   title: _renderTitle('Trạng Thái'),
      //   dataIndex: 'status',
      //   key: 'status',
      //   align: 'center',
      //   width: _widthColumn(this.state.width, 1),

      //   render: status => _renderColumns(status == 1 ? 'Hiện' : 'Ẩn'),
      // },
      {
        title: "",
        dataIndex: "idAndStt",
        key: "idAndStt",
        align: "center",
        width: 50,
        fixed: widthScreen <= 1400 ? "right" : "",

        render: (idAndStt) => (
          <div
            onClick={() =>
              this.props.history.push("/storeDetail/" + idAndStt.id)
            }
            className="cursor"
          >
            <ISvg
              name={ISvg.NAME.CHEVRONRIGHT}
              width={11}
              height={20}
              fill={colors.icon.default}
            />
          </div>
        ),
      },
    ];

    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Kho hàng"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <ISearch
            onChange={(e) => {
              this.state.keyword = e.target.value;
              this.setState({
                keyword: this.state.keyword,
              });
            }}
            placeholder=""
            onPressEnter={() =>
              this._getListWarehouse(
                this.state.type,
                this.state.region,
                this.state.keyword,
                this.state.page // caterory cha
              )
            }
            icon={
              <div
                style={{
                  display: "flex",
                  width: 42,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                className="cursor"
                onClick={() =>
                  this.setState(
                    {
                      loadingTable: true,
                    },
                    () =>
                      this._getListWarehouse(
                        this.state.type,
                        this.state.region,
                        this.state.keyword,
                        this.state.page // caterory cha
                      )
                  )
                }
                className="cursor"
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>

        <Row className="center" style={{ marginTop: 36 }}>
          <Col
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
            className="m-0 p-0"
          >
            <Row>
              <Col className="mt-0 center ml-4 mr-4" xs="auto">
                <Row className="center mr-5">
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    width={20}
                    height={20}
                    fill={colors.icon.default}
                  />
                  <div style={{ marginLeft: 10 }}>
                    <ITitle title="Lọc danh sách" level={4} />
                  </div>
                </Row>
                <Row className="mr-4 p-0">
                  {this.state.listType.length == 0 ? null : (
                    <ISelect
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                      defaultValue={this.state.listType[0].key}
                      select={true}
                      onChange={(value, key) => {
                        this.setState(
                          {
                            loadingTable: true,
                            type: value,
                          },
                          () =>
                            this._getListWarehouse(
                              this.state.type,
                              this.state.region,
                              this.state.keyword,
                              this.state.page // caterory cha
                            )
                        );
                      }}
                      data={this.state.listType}
                    />
                  )}
                </Row>
                <Row className="m-0 p-0" style={{ marginRight: 20 }}>
                  <ICascader
                    style={{
                      background: colors.white,
                      borderWidth: 1,
                      // paddingLeft: 12,
                      borderColor: colors.line,
                      borderStyle: "solid",
                    }}
                    type="city"
                    // level={1}
                    onChange={(value, selectedOptions) => {
                      this.setState(
                        {
                          region: value[0],
                          loadingTable: true,
                        },
                        () =>
                          this._getListWarehouse(
                            this.state.type,
                            this.state.region,
                            this.state.keyword,
                            this.state.type // caterory cha
                          )
                      );
                    }}
                    placeholder="Chọn khu vực"
                  />
                </Row>
              </Col>

              <Col xs="auto">
                <Row className="m-0 p-0">
                  <IButton
                    icon={ISvg.NAME.ARROWUP}
                    title="Tạo mới"
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() => this.props.history.push("/storeCreate")}
                    // onClick={() => {
                    //   message.warning('Chức năng dsdsa cập nhật');
                    // }}
                  />
                  <IButton
                    icon={ISvg.NAME.DOWLOAND}
                    title="Xuất file"
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() => {
                      message.warning("Chức năng đang cập nhật");
                    }}
                  />

                  <IButton
                    icon={ISvg.NAME.DELETE}
                    title="Xóa"
                    color={colors.oranges}
                    style={{}}
                    rowSelection={rowSelection}
                    onClick={() => {
                      if (this.state.arrayRemove.length == 0) {
                        message.warning("Bạn chưa chọn kho để xóa.");
                        return;
                      }
                      const objRemove = {
                        id_warehouse: this.state.arrayRemove,
                      };
                      this._APIpostRemoveWarehouse(objRemove);
                    }}
                    //   if (this.state.arrayRemove.length == 0) {
                    //     message.warning('Bạn chưa chọn mã sản phẩm để xóa.');
                    //     return;
                    //   }
                    //   const objRemove = {
                    //     id: this.state.arrayRemove,
                    //     status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                    //   };
                    //   this._APIpostRemoveProduct(objRemove);
                    // }}
                  />
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mt-4">
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%" }}
            rowSelection={rowSelection}
            defaultCurrent={2}
            maxpage={6}
            sizeItem={this.state.data.size}
            indexPage={this.state.page}
            maxpage={this.state.data.total}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                },
                () =>
                  this._getListWarehouse(
                    this.state.type,
                    this.state.region,
                    this.state.keyword,
                    this.state.page // caterory cha
                  )
              );
            }}
            // scroll={{ x: this.state.width }}
          />
        </Row>
      </Container>
    );
  }
}
