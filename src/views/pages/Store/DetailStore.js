import { Table, Typography, Avatar, Button, message } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  ICascader,
  IDatePicker,
  ILoading,
} from "../../components";
import { colors } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { write } from "fs";
import { APIService } from "../../../services";
import { thisExpression } from "@babel/types";
import FormatterDay from "../../../utils/FormatterDay";
const widthScreen = window.innerWidth;

const { Paragraph, Title } = Typography;
function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1200) {
    widthColumn = 1200 / 10;
  } else widthColumn = (widthScreen - 300) / 10;
  return widthColumn * type;
}

export default class DetailStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      index: [],
      width: 0,
      loadingTable: true,
      bool: false,
      data: {},
      nameCity: [],
      idCity: "",
      page: 1,
      total: 0,
      sizeItem: 0,
      valueSearch: "",
      loading: true,
      status: 0,
      value: "",
      city_id: 0,
      district_id: 0,
      key: "",
      arrID: [],
      notPage: false,
      from_date: 0,
      to_date: 0,

      typeStatus: [
        { key: 0, value: "Tất cả loại hình" },
        { key: 1, value: "Nhập kho" },
        { key: 2, value: "Xuất kho" },
        { key: 3, value: "Xuất chuyển kho" },
      ],
    };
    this.agencyID = Number(this.props.match.params.id);
  }

  componentDidMount() {
    this._getAPIDetailWarehouse(this.agencyID);
    this._getListHistory(
      this.state.from_date,
      this.state.to_date,
      this.state.page,
      this.agencyID
    );
    this.getDate();
  }

  _getAPIDetailWarehouse = async (warehouseID) => {
    try {
      const data = await APIService._getDetailWH(warehouseID);
      let nameCity = [];
      data.warehouse.regions.map((item, index) => {
        nameCity.push(item.name);
      });

      this.setState({
        data: data.warehouse,
        nameCity: nameCity,
        loading: false,
      });
    } catch (error) {}
  };

  _getListHistory = async (from_date, to_date, page, warehouse_id) => {
    try {
      const data = await APIService._getListHistoryWarehouse(
        from_date,
        to_date,
        page,
        warehouse_id
      );
      this.setState({
        dataTable: data.warehouse_historys,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };
  getDate = () => {
    let from = new Date();
    from.setDate(1);
    from.setHours(0, 0, 0, 0);

    let to = new Date();
    to.setHours(0, 0, 0, 0);
    this.setState({
      from_date: from.getTime(),
      to_date: to.getTime(),
    });
  };
  render() {
    const columns = [
      {
        title: _renderTitle("Ngày thao tác"),
        dataIndex: "create_date",
        key: "create_date",
        align: "left",
        // width: _widthColumn(this.state.width, 3),
        width: 170,
        render: (create_date) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              create_date,
              "#hhhh#:#mm#:#ss# #DD#-#MM#-#YYYY#"
            )
          ),
      },
      {
        title: _renderTitle("Người thao tác"),
        dataIndex: "creater",
        key: "creater",
        align: "left",
        // width: _widthColumn(this.state.width, 3),
        width: 200,
        render: (creater) => <ITitle level={4} title={creater} />,
      },
      {
        title: _renderTitle("Hoạt động"),
        dataIndex: "type",
        key: "type",
        align: "left",
        // width: _widthColumn(this.state.width, 3),
        width: 120,
        render: (type) =>
          _renderColumns(
            this.state.typeStatus.map((item, index) => {
              if (item.key == type) return item.value;
            })
          ),
      },
      {
        title: _renderTitle("Giá trị tồn trước"),
        dataIndex: "total_money_prev",
        key: "total_money_prev",
        align: "right",
        // width: _widthColumn(this.state.width, 3),
        width: 120,
        render: (total_money_prev) =>
          _renderColumns(priceFormat(total_money_prev) + "đ"),
      },
      {
        title: _renderTitle("Giá trị tồn sau"),
        dataIndex: "total_money_current",
        key: "total_money_current",
        align: "right",
        // width: _widthColumn(this.state.width, 3),
        width: 120,

        render: (total_money_current) =>
          _renderColumns(priceFormat(total_money_current) + "đ"),
      },
      {
        title: _renderTitle("Mã phiếu"),
        dataIndex: "code",
        key: "code",
        align: "left",
        // width: _widthColumn(this.state.width, 2),
        width: 150,
        render: (code) => _renderColumns(code),
      },

      // {
      //   title: "",
      //   dataIndex: "sttAndId",
      //   key: "sttAndId",
      //   align: "right",
      //   // fixed: "right",
      //   width: 100,
      //   // fixed: widthScreen <= 1375 ? 'right' : '',

      //   render: sttAndId => (
      //     <Row>
      //       <Col
      //         // onClick={() =>
      //         //   this.props.history.push("/agencyEdit/" + sttAndId.id)
      //         // }
      //         className="cell"
      //       >
      //         <ISvg
      //           name={ISvg.NAME.DOWLOAND}
      //           width={20}
      //           height={20}
      //           fill={colors.icon.default}
      //         />
      //       </Col>
      //     </Row>
      //   )
      // }
    ];

    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };

    return (
      <Container fluid>
        <Row style={{}}>
          <ITitle
            level={1}
            title="Chi tiết kho"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <Col
            className="float-right "
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Row>
              <IButton
                className="text-right"
                icon={ISvg.NAME.WRITE}
                title="Chỉnh sửa"
                color={colors.main}
                onClick={() => {
                  this.props.history.push("/storeEdit/" + this.agencyID);
                }} // idAndStt.index vị trí của mảng
                className="cell"
              />
            </Row>
          </Col>
        </Row>
        {!this.state.loading ? (
          <Row className="mt-5">
            <Col
              xs={4}
              // className="mr-2"
              style={{
                background: colors.white,
                borderRight: "25px solid #F3F7F5",
              }}
            >
              <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                <Col>
                  <ITitle
                    level={2}
                    title="Thông tin kho"
                    style={{ color: colors.black, fontWeight: "bold" }}
                  />
                </Col>
                {/* <Col>
       <IButton
         icon={ISvg.NAME.SEE}
         title="Xem lịch sử"
         color={colors.main}
         style={{}}
       />
     </Col> */}
              </Row>
              <Row className="p-0 ml-3" xs="auto">
                <Col className="p-0 m-0">
                  <Row className="p-0 m-0 ">
                    <ITitle
                      level={4}
                      title={"Tên kho"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row className="p-0 m-0">
                    <ITitle
                      style={{ marginTop: 10 }}
                      level={4}
                      title={this.state.data.name}
                    />
                  </Row>
                </Col>
              </Row>

              <Row className="p-0 mt-3 ml-3 mb-0 mr-0" xs="auto">
                <Col className="p-0 m-0" style={{ color: colors.black }}>
                  <Row className="p-0 m-0">
                    <ITitle
                      level={4}
                      title={"Mã Kho"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row className="p-0 m-0">
                    <ITitle
                      style={{ marginTop: 10 }}
                      level={4}
                      title={this.state.data.code}
                    />
                  </Row>
                </Col>

                <Col className="p-0 " style={{ marginLeft: 28, marginTop: 0 }}>
                  <Row className="p-0 m-0">
                    <ITitle
                      level={4}
                      title={"Loại kho"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row className="p-0 m-0">
                    <ITitle
                      style={{ marginTop: 10 }}
                      level={4}
                      title={this.state.data.type}
                    />
                  </Row>
                </Col>
              </Row>

              <Row className="p-0 mt-3 ml-3 mb-0 mr-0" xs="auto">
                <Col className="p-0 m-0" style={{ color: colors.black }}>
                  <Row className="p-0 m-0">
                    <ITitle
                      level={4}
                      title={"Điện thoại"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row className="p-0 m-0">
                    <ITitle
                      style={{ marginTop: 10 }}
                      level={4}
                      title={this.state.data.phone}
                    />
                  </Row>
                </Col>

                <Col className="p-0 " style={{ marginLeft: 28, marginTop: 0 }}>
                  <Row className="p-0 m-0">
                    <ITitle
                      level={4}
                      title={"Email"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row className="p-0 m-0">
                    <ITitle
                      style={{ marginTop: 10 }}
                      level={4}
                      title={this.state.data.email}
                    />
                  </Row>
                </Col>
              </Row>

              {/* <Row className="p-0 mt-3 ml-3 mb-0 mr-0" xs="auto">
            <Col className="p-0 m-0" style={{color: colors.black}}>
              <Row className="p-0 m-0">
                <ITitle
                  level={4}
                  title={'Điện thoại'}
                  style={{fontWeight: 700}}
                />
              </Row>
              <Row className="p-0 m-0">
                <ITitle style={{marginTop: 10}} level={4} title={'ABC123'} />
              </Row>
            </Col>

            <Col
              className="p-0 "
              style={{marginTop: 0, backgroundColor: 'red'}}
            >
              <Row className="p-0 m-0">
                <ITitle level={4} title={'Email'} style={{fontWeight: 700}} />
              </Row>
              <Row className="p-0 m-0">
                <ITitle
                  style={{marginTop: 10}}
                  level={4}
                  title={'kho.a.phunh@gmail.com'}
                />
              </Row>
            </Col>
          </Row> */}

              <Row className="p-0 mt-3 ml-3 mb-0 mr-0" xs="auto">
                <Col className="p-0 m-0">
                  <Row className="p-0 m-0">
                    <ITitle
                      level={4}
                      title={"Địa chỉ"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row xs={6} className="p-0 m-0">
                    <ITitle
                      style={{ marginTop: 10 }}
                      level={4}
                      title={this.state.data.address}
                    />
                  </Row>
                </Col>
                <Col></Col>
              </Row>
              <Row className="p-0 mt-3 ml-3 mb-0 mr-0" xs="auto">
                <Col className="p-0 m-0">
                  <Row className="p-0 m-0">
                    <ITitle
                      level={4}
                      title={"Đại diện"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row className="p-0 m-0">
                    <ITitle
                      style={{ marginTop: 10 }}
                      level={4}
                      title={this.state.data.representative_name}
                    />
                  </Row>
                </Col>

                <Col className="p-0" style={{ marginLeft: 28, marginTop: 0 }}>
                  <Row className="p-0 m-0">
                    <ITitle
                      level={4}
                      title={"Trạng thái"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row className="p-0 m-0">
                    <ITitle
                      style={{ marginTop: 10 }}
                      level={4}
                      title={
                        this.state.data.status == 1
                          ? "Đang hoạt động"
                          : "Ngưng hoạt động"
                      }
                    />
                  </Row>
                </Col>
              </Row>
              <Row className="p-0 mt-5 mb-4 ml-3 mr-0">
                <ITitle
                  level={2}
                  title="Khu vực"
                  style={{ color: colors.black, fontWeight: "bold" }}
                />
              </Row>
              <Row className="p-0 mt-3 ml-3 mb-0 mr-0" xs="auto">
                <Col className="p-0 m-0">
                  <Row className="p-0 m-0">
                    <ITitle
                      level={4}
                      title={"Tỉnh/Thành Phố"}
                      style={{ fontWeight: 700 }}
                    />
                  </Row>
                  <Row className="p-0 m-0">
                    {this.state.nameCity.map((item, index) => {
                      return (
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={item + ","}
                        />
                      );
                    })}
                  </Row>
                </Col>
              </Row>
              {/* <Row className="p-0 mt-3 ml-3 mb-0 mr-0" xs="auto">
            <Col className="p-0 m-0">
              <Row className="p-0 m-0">
                <ITitle
                  level={4}
                  title={'Quận/Huyện'}
                  style={{fontWeight: 700}}
                />
              </Row>
              <Row className="p-0 m-0">
                <ITitle style={{marginTop: 10}} level={4} title={'Quận 1'} />
              </Row>
            </Col>
          </Row> */}
            </Col>
            {/* <div> â</div> */}
            <Col style={{ background: colors.white }} xs={8}>
              <Row className="p-0 mt-3 ml-0 mb-0 mr-0" xs="auto">
                <Col
                  className="p-0 m-0 mt-2"
                  style={
                    {
                      // alignItems:'flex-end'
                    }
                  }
                >
                  <ITitle
                    level={4}
                    title={"Lịch sử xuất/ nhập kho"}
                    style={{ fontWeight: 700, marginTop: 4 }}
                  />
                  {/* <ITitle style={{fontSize: 14}} title={'Từ ngày'} style={{}} /> */}
                </Col>

                <Col
                  className="p-0 ml-2"
                  style={{ display: "flex", justifyContent: "flex-end" }}
                >
                  <Row
                    style={{}}
                    className="mt-2"
                    // style={{backgroundColor: 'yellow'}}
                  >
                    <div style={{ marginTop: 10 }}>
                      <ITitle
                        style={{ fontSize: 14 }}
                        title={"Từ ngày"}
                        // style={{marginTop: 10}}
                      />
                    </div>
                    <Col>
                      <IDatePicker
                        to={this.state.to_date}
                        from={this.state.from_date}
                        style={{ width: 250 }}
                        onChange={(a, b) => {
                          if (a.length <= 1) {
                            console.log("aa");
                            return;
                          }
                          this.state.from_date = a[0]._d.setHours(0, 0, 0);
                          this.state.to_date = a[1]._d.setHours(23, 59, 59);

                          this.setState({
                            from_date: this.state.from_date,
                            to_date: this.state.to_date,
                          });
                          this._getListHistory(
                            this.state.from_date,
                            this.state.to_date,
                            this.state.page,
                            this.agencyID
                          );
                        }}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className="p-0 mt-3 ml-0 mb-0 mr-0" xs="auto">
                <ITable
                  data={this.state.dataTable}
                  columns={columns}
                  style={{ width: "100%" }}
                  defaultCurrent={1}
                  // hidePage={this.state.notPage}
                  loading={this.state.loadingTable}
                  footer={null}
                />
              </Row>
            </Col>
          </Row>
        ) : (
          <ILoading />
        )}
      </Container>
    );
  }
}
