import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import ReactDOM from "react-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IImage,
  ISelectWareHouse,
  ISelectStatus
} from "../../components";
import { colors, images } from "../../../assets";
import { message, Button, Upload, Icon } from "antd";
import { ExcelToJson } from "../../../utils/ExcelUtil";
function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}
class CreateCouponPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHeight: 0,
      shippers: [],
      type_warehouse: 0,
      keyStatus: 2 // lấy tất cả
    };
    this.columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",

        width: 80,
        render: (id, row, index) => {
          if (this.state.shippers.length === 1) {
            var obj = {
              children: <div style={{ height: 300, background: "red" }}></div>,
              props: { colSpan: 10 }
            };
            // if (index === 0) {
            //   obj.props.colSpan = 2;
            // }
            return obj;
          }

          var obj = {
            children: _renderColumns(id),
            props: {}
          };
          // if (index === 0) {
          //   obj.props.colSpan = 2;
          // }
          return obj;
        }
      },
      {
        title: _renderTitle("Mã Shipper"),
        dataIndex: "code",
        key: "code",
        width: 160,
        render: (id, row, index) => {
          if (this.state.shippers.length === 1) {
            var obj = {
              children: null,
              props: { colSpan: 0 }
            };
            return obj;
          }
          var obj = {
            children: _renderColumns(id),
            props: {}
          };
          // if (index === 0) {
          //   obj.props.colSpan = 0;
          // }
          return obj;
        }
      },
      {
        title: _renderTitle("Tên Shipper"),
        dataIndex: "name",
        key: "name",

        render: id => {
          if (this.state.shippers.length === 1) {
            var obj = {
              children: null,
              props: { colSpan: 0 }
            };
            return obj;
          }
          return _renderColumns(id);
        }
      },
      {
        title: _renderTitle("Số điện thoại"),
        dataIndex: "phone",
        key: "phone",
        width: 200,
        render: id => {
          if (this.state.shippers.length === 1) {
            var obj = {
              children: null,
              props: { colSpan: 0 }
            };
            return obj;
          }
          return _renderColumns(id);
        }
      },
      {
        title: _renderTitle("Email"),
        dataIndex: "email",
        key: "email",

        render: id => {
          if (this.state.shippers.length === 1) {
            var obj = {
              children: null,
              props: { colSpan: 0 }
            };
            return obj;
          }
          return _renderColumns(id);
        }
      }
    ];
    this.tableContainer = React.createRef();
  }
  render() {
    return (
      <Container className="p-0 m-0" fluid style={{ flex: 1 }}>
        <Col className="d-flex flex-column" style={{ height: "100%" }}>
          <Row className="p-0 m-0">
            <ITitle
              level={1}
              title="aaaa"
              style={{
                color: colors.main,
                fontWeight: "bold",
                flex: 1,
                display: "flex"
              }}
            />
            <ISearch
              onChange={this.onChangeSearch}
              style={{ width: 400 }}
              placeholder=""
              onPressEnter={() => {
                message.info("api");
              }}
              icon={
                <div
                  style={{
                    height: 42,
                    display: "flex",
                    alignItems: "center"
                  }}
                >
                  <img
                    src={images.icSearch}
                    style={{ width: 25, height: 25 }}
                  />
                </div>
              }
            />
          </Row>
          <Row className="m-0 p-0 mt-4">
            <Row className="flex-1 m-0 p-0" style={{ flex: 1 }}>
              <Row className="m-0 p-0 center">
                <ISvg
                  name={ISvg.NAME.EXPERIMENT}
                  width={20}
                  height={20}
                  fill={colors.icon.default}
                />
                <div style={{ marginLeft: 25 }}>
                  <ITitle title="Kho hàng" level={4} />
                </div>
              </Row>
              <Row className="m-0 p-0 ">
                <ISelectWareHouse
                  className="ml-4"
                  onSearch={() => {}}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                  value={this.state.type_warehouse}
                  onChange={key => {
                    this.setState(
                      {
                        type_warehouse: key,
                        loadingTable: true
                      }
                      // () =>
                      //   this._getAPIListInventory(
                      //     this.state.type_warehouse,
                      //     this.state.type_product,
                      //     this.state.category_id,
                      //     this.state.page,
                      //     this.state.fromDate,
                      //     this.state.toDate,
                      //     this.state.keyword
                      //   )
                    );
                  }}
                  showSearch
                />
              </Row>
              <Row className="m-0 p-0 ">
                <ISelectStatus
                  className="ml-4"
                  all={true}
                  value={this.state.keyStatus}
                  onChange={key => {
                    this.setState({
                      keyStatus: key
                    });
                  }}
                />
              </Row>
            </Row>
            <Row className="m-0 p-0">
              <IButton
                icon={ISvg.NAME.ADD}
                title={"Tạo mới"}
                color={colors.main}
                onClick={() => {
                  this.props.history.push("/createShipperPage");
                  // const obj = {
                  //   start_date: this.state.start_date,
                  //   end_date: this.state.end_date,
                  //   status: this.key,
                  //   key: this.state.value
                  // };
                  // this._postAPIExportExecl(obj);
                }}
              />
              <IButton
                icon={ISvg.NAME.DOWLOAND}
                title={"Xuất file"}
                color={colors.main}
                style={{ marginRight: 20, marginLeft: 20 }}
                onClick={() => {
                  // const obj = {
                  //   start_date: this.state.start_date,
                  //   end_date: this.state.end_date,
                  //   status: this.key,
                  //   key: this.state.value
                  // };
                  // this._postAPIExportExecl(obj);
                }}
              />
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa đơn"
                color={colors.oranges}
                onClick={() => {
                  // this._updateOrder(OrderStatus.DELETE);
                }}
              />
            </Row>
          </Row>
          <input
            type="file"
            onChange={async e => {
              const data = await ExcelToJson(e.target.files[0]);
              this.setState({
                shippers: data
              });
            }}
          ></input>
          <div
            className="m-0 p-0 mt-4 "
            style={{ flex: 1 }}
            ref={this.tableContainer}
          >
            <ITable
              data={this.state.shippers}
              columns={this.columns}
              //loading={this.state.loadingTable}
              bodyStyle={
                {
                  // height: this.state.tableHeight
                }
              }
              style={{ width: "100%", background: colors.background }}
              scroll={{ y: this.state.tableHeight - 50 }}
              defaultCurrent={1}
              hidePage
            />
          </div>
        </Col>
      </Container>
    );
  }

  componentDidMount() {
    for (var i = 0; i < 1; i++) {
      this.state.shippers.push({
        stt: i,
        code: "VH323131",
        name: "Trần Dần",
        phone: "01234567890",
        email: "TranTiger@gmail.com"
      });
    }
    setTimeout(() => {
      try {
        this.setState({
          tableHeight:
            this.tableContainer.current.parentElement.clientHeight - 200
        }); // 360
      } catch (error) {}
    }, 1);
  }
}

export default CreateCouponPage;
