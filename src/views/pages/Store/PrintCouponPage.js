import React, { Component } from "react";
import { Container, Col, Row } from "reactstrap";
import { images } from "../../../assets";
import { priceFormat } from "../../../utils";
import writtenNumber from "written-number";

class PrintCouponPage extends Component {
  constructor(props) {
    super(props);
    this.data = this.props.location.state.data;
    // this.stock_id = Number(this.props.match.params.id);
    this.type = this.props.match.params.type;
  }
  render() {
    const order_info = {},
      price = {},
      product = {};
    const { products } = this.data;

    let priceString =
      writtenNumber(this.data.sum_value, { lang: "vi" }) + " đồng";
    return (
      <Container
        id="body-print"
        style={{
          paddingTop: 20,
          paddingBottom: 20,
          paddingLeft: 0,
          paddingRight: 0,
          minWidth: 1280,
        }}
      >
        {/* HEADER */}
        <Row className="d-flex justify-content-center align-items-center p-0 m-0">
          <Col sm="auto" style={{}}>
            <img src={images.logo} style={{ width: 78, height: 70 }} />
          </Col>
          <Col style={{}}>
            <p
              style={{
                fontSize: 14,
                fontWeight: "600",
                color: "black",
              }}
            >
              CÔNG TY CỔ PHẦN TẬP ĐOÀN LỘC TRỜI
            </p>
            <p
              style={{
                fontSize: 14,
                fontWeight: "600",

                color: "black",
              }}
            >
              41 Đinh Tiên Hoàng, Phường Diên Hồng, TP.Pleiku, Gia Lai
            </p>
            <p
              style={{
                fontSize: 14,
                fontWeight: "600",

                color: "black",
              }}
            >
              Tel: 0269.222.1868
            </p>
          </Col>
        </Row>

        {/* TITLE */}
        <Row
          style={{ padding: 26 }}
          className="d-flex justify-content-center align-items-center p-0  m-0 mt-4"
        >
          <p
            style={{
              fontSize: 26,
              fontWeight: "600",
              color: "black",
            }}
          >
            {this.type == "INPUT" ? "PHIẾU NHẬP KHO" : "PHIẾU XUẤT KHO"}
          </p>
        </Row>

        <Row className="p-0 m-0 mt-2">
          <Col></Col>
          <Col>
            <div className="d-flex center flex-column">
              <p
                style={{
                  fontSize: 15,
                  fontWeight: "600",
                  fontStyle: "italic",
                  color: "black",
                }}
              >
                Ngày ..... tháng ..... năm .....
              </p>
              <p
                className="mt-2"
                style={{
                  fontSize: 15,
                  fontWeight: "600",
                  color: "black",
                }}
              >
                Số ...............
              </p>
            </div>
          </Col>
          <Col>
            <div className="d-flex center flex-column">
              <p
                style={{
                  fontSize: 15,
                  fontWeight: "600",
                  color: "black",
                }}
              >
                Nợ: ..............
              </p>
              <p
                className="mt-2"
                style={{
                  fontSize: 15,
                  fontWeight: "600",
                  color: "black",
                }}
              >
                Có: ..............
              </p>
            </div>
          </Col>
        </Row>

        <Row className="p-0 m-0 mt-4">
          <Col>
            <div style={{ width: "100%", display: "flex" }}>
              <p
                style={{
                  fontSize: 15,
                  fontWeight: "500",
                  color: "black",
                }}
              >
                Họ và tên người giao:
              </p>

              <p
                className="ml-2 mb-1"
                style={{
                  fontSize: 15,
                  fontWeight: "500",
                  color: "black",
                  flex: 1,
                  borderBottom: "2px dotted black",
                }}
              ></p>
            </div>
            {this.type == "INPUT" ? (
              <div className="d-flex mt-3">
                <div
                  className="mr-2"
                  style={{ width: "100%", display: "flex", flex: 1 }}
                >
                  <p
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                    }}
                  >
                    Theo ngày:
                  </p>

                  <p
                    className="ml-2 mb-1"
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                      flex: 1,
                      borderBottom: "2px dotted black",
                    }}
                  ></p>
                </div>
                <div style={{ width: "100%", display: "flex", flex: 1 }}>
                  <p
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                    }}
                  >
                    Của:
                  </p>

                  <p
                    className="ml-2 mb-1"
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                      flex: 1,
                      borderBottom: "2px dotted black",
                    }}
                  ></p>
                </div>
              </div>
            ) : null}

            {this.type == "INPUT" ? (
              <div className="d-flex mt-3">
                <div
                  className="mr-2"
                  style={{ width: "100%", display: "flex", flex: 1 }}
                >
                  <p
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                    }}
                  >
                    Nhập lại kho:
                  </p>

                  <p
                    className="ml-2 mb-1"
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                      flex: 1,
                      borderBottom: "2px dotted black",
                    }}
                  ></p>
                </div>
                <div style={{ width: "100%", display: "flex", flex: 1 }}>
                  <p
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                    }}
                  >
                    Địa điểm:
                  </p>

                  <p
                    className="ml-2 mb-1"
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                      flex: 1,
                      borderBottom: "2px dotted black",
                    }}
                  ></p>
                </div>
              </div>
            ) : null}
            {this.type == "OUTPUT" ? (
              <div>
                <div className="d-flex mt-3">
                  <p
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                    }}
                  >
                    Địa chỉ (bộ phận):
                  </p>

                  <p
                    className="ml-2 mb-1"
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                      flex: 1,
                      borderBottom: "2px dotted black",
                    }}
                  ></p>
                </div>

                <div className="d-flex mt-3">
                  <p
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                    }}
                  >
                    Lý do xuất kho:
                  </p>

                  <p
                    className="ml-2 mb-1"
                    style={{
                      fontSize: 15,
                      fontWeight: "500",
                      color: "black",
                      flex: 1,
                      borderBottom: "2px dotted black",
                    }}
                  ></p>
                </div>

                <div className="d-flex mt-3">
                  <div
                    className="mr-2"
                    style={{ width: "100%", display: "flex", flex: 1 }}
                  >
                    <p
                      style={{
                        fontSize: 15,
                        fontWeight: "500",
                        color: "black",
                      }}
                    >
                      Xuất tại kho (ngăn lô):
                    </p>

                    <p
                      className="ml-2 mb-1"
                      style={{
                        fontSize: 15,
                        fontWeight: "500",
                        color: "black",
                        flex: 1,
                        borderBottom: "2px dotted black",
                      }}
                    ></p>
                  </div>
                  <div style={{ width: "100%", display: "flex", flex: 1 }}>
                    <p
                      style={{
                        fontSize: 15,
                        fontWeight: "500",
                        color: "black",
                      }}
                    >
                      Địa điểm:
                    </p>

                    <p
                      className="ml-2 mb-1"
                      style={{
                        fontSize: 15,
                        fontWeight: "500",
                        color: "black",
                        flex: 1,
                        borderBottom: "2px dotted black",
                      }}
                    ></p>
                  </div>
                </div>
              </div>
            ) : null}
          </Col>
        </Row>

        <Col className="" style={{ marginTop: 64 }}>
          <table border={1} borderColor="black" style={{ width: "100%" }}>
            <tr>
              <th width={50} rowSpan={2}>
                {this._renderCellHeader("STT")}
              </th>
              <th rowSpan={2} width={260}>
                {this._renderCellHeader(
                  "Tên, nhãn hiệu, quy cách, phẩm chất vật tư, dụng cụ sản phẩm, hàng hóa",
                  "left"
                )}
              </th>
              <th rowSpan={2}>{this._renderCellHeader("Mã sản phẩm")}</th>
              <th rowSpan={2}>{this._renderCellHeader("Đơn vị tính")}</th>
              <th colSpan={2}>{this._renderCellHeader("Số lượng")}</th>
              <th rowSpan={2} width={140}>
                {this._renderCellHeader("Đơn giá")}
              </th>
              <th rowSpan={2} width={140}>
                {this._renderCellHeader("Thành tiền")}
              </th>
            </tr>
            <tr>
              <th>{this._renderCellHeader("Chứng từ")}</th>
              <th>{this._renderCellHeader("Thực nhập")}</th>
            </tr>
            {products.map((item, index) => {
              return this._renderItemProduct(item, index + 1);
            })}

            <tr>
              <th colSpan={3}>{this._renderCellHeader("Cộng")}</th>
              <th></th>
              <th>{this._renderCellHeader("")}</th>
              <th>{this._renderCellHeader("")}</th>
              <th width={140}>{this._renderCellHeader("", "right")}</th>
              <th width={140}>
                {this._renderCell(
                  priceFormat(this.data.sum_value) + "đ",
                  "right"
                )}
              </th>
            </tr>

            <tr style={{}}>
              <th colSpan={3}>
                {this._renderCellHeader("Tổng số tiền (Viết bằng chữ):")}
              </th>
              <th colSpan={5}>
                {this._renderCell(
                  priceString.charAt(0).toUpperCase() + priceString.substring(1)
                )}
              </th>
            </tr>
            <tr style={{}}>
              <th colSpan={3}>
                {this._renderCellHeader("Số chứng từ gốc kèm theo:")}
              </th>
              <th colSpan={5}>
                {/* {this._renderCell(
                  'Bốn mươi lăm triệu bốn trăm ba mươi hai nghìn một trăm đồng ',
                  'left'
                )} */}
              </th>
            </tr>
          </table>

          <Row className="mt-2 p-0">
            <Col className="p-4">
              <div className="center">
                <p
                  style={{
                    fontSize: 14,
                    color: "black",
                    fontWeight: "bold",
                  }}
                >
                  Người lập phiếu
                </p>
              </div>
              <div className="center">
                <p
                  style={{
                    fontSize: 12,
                    color: "black",
                    fontStyle: "italic",
                  }}
                >
                  (Ký, họ tên)
                </p>
              </div>

              <div style={{ height: 120 }}></div>
            </Col>

            <Col className="p-4">
              <div className="center">
                <p
                  style={{
                    fontSize: 14,
                    color: "black",
                    fontWeight: "bold",
                  }}
                >
                  Người giao hàng
                </p>
              </div>
              <div className="center">
                <p
                  style={{
                    fontSize: 12,
                    color: "black",
                    fontStyle: "italic",
                  }}
                >
                  (Ký, họ tên)
                </p>
              </div>

              <div style={{ height: 120 }}></div>
            </Col>
            <Col className="p-4">
              <div className="center">
                <p
                  style={{
                    fontSize: 14,
                    color: "black",
                    fontWeight: "bold",
                  }}
                >
                  Thủ kho
                </p>
              </div>
              <div className="center">
                <p
                  style={{
                    fontSize: 12,
                    color: "black",
                    fontStyle: "italic",
                  }}
                >
                  (Ký, họ tên)
                </p>
              </div>

              <div style={{ height: 120 }}></div>
            </Col>

            <Col className="p-4">
              <div className="center">
                <p
                  style={{
                    fontSize: 12,
                    color: "black",
                    fontStyle: "italic",
                  }}
                >
                  Ngày ..... tháng ..... năm ..........
                </p>
              </div>
              <div className="center">
                <p
                  style={{
                    fontSize: 14,
                    color: "black",
                    fontWeight: "bold",
                    textAlign: "center",
                  }}
                >
                  Kế toán trưởng
                </p>
              </div>
              <div className="center">
                <p
                  style={{
                    fontSize: 14,
                    color: "black",
                    fontWeight: "bold",
                    textAlign: "center",
                  }}
                >
                  (Hoặc bộ phận có nhu cầu nhập)
                </p>
              </div>
              <div className="center">
                <p
                  style={{
                    fontSize: 12,
                    color: "black",
                    fontStyle: "italic",
                  }}
                >
                  (Ký, họ tên)
                </p>
              </div>

              <div style={{ height: 120 }}></div>
            </Col>
          </Row>
        </Col>
      </Container>
    );
  }
  componentDidMount() {
    window.print();
  }

  _renderCell = (content, type) => {
    return (
      <p
        style={{
          fontSize: 14,
          padding: 8,
          color: "black",
          textAlign: type ? type : "center",
          fontWeight: "260",
        }}
      >
        {content}
      </p>
    );
  };
  _renderCellHeader = (content, type) => {
    return (
      <p
        style={{
          fontSize: 14,
          padding: 8,
          color: "black",
          textAlign: type ? type : "center",
          fontWeight: "bold",
        }}
      >
        {content}
      </p>
    );
  };

  _renderItemProduct = (item, index) => {
    return (
      <tr>
        <th width={50}>{this._renderCell(item.product_id)}</th>
        <th width={260}>{this._renderCell(item.product_name, "left")}</th>
        <th>{this._renderCell(item.product_code)}</th>
        <th>{this._renderCell(item.product_unit_name)}</th>
        <th>{this._renderCell(item.total_quantity)}</th>
        <th>{this._renderCell("")}</th>
        <th width={140}>{this._renderCell("", "right")}</th>
        <th width={140}>
          {this._renderCell(priceFormat(item.total_money) + "đ", "right")}
        </th>
      </tr>
    );
  };
}

export default PrintCouponPage;
