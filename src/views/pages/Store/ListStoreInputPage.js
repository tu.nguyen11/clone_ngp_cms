import { Table, Typography, Avatar, Button, message } from 'antd'
import React, { Component } from 'react'
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
  IDatePicker,
  ISelectWareHouse,
  ISelectTypeWareHouse,
  ISelectAllWareHouse
} from '../../components'
import { colors, images } from '../../../assets'
import { Container, Row, Col } from 'reactstrap'
import { priceFormat } from '../../../utils'
import { APIService } from '../../../services'
import { thisExpression } from '@babel/types'
import QueueAnim from 'rc-queue-anim'
import moment from 'moment'

import FormatterDay from '../../../utils/FormatterDay'
const widthScreen = window.innerWidth

function _renderTitle (title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: 'bold' }} />
    </div>
  )
}

function _renderColumns (title) {
  return (
    <div style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
      <ITitle level={4} title={title} />
    </div>
  )
}

function _widthColumn (widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 9
  } else widthColumn = (widthScreen - 300) / 9
  return widthColumn * type
}

const NewDate = new Date()
const data = []
for (let i = 1; i <= 15; i++) {
  data.push({
    stt: i,
    id: i,
    import_date: 1572946138000,
    code: 'ABC123456',
    typeName: 'Nhập kho',
    creatorName: 'Admin',
    warehouseName: 'T Ritika Singh',
    importerName: 'Admin',
    price: 27000000,
    note: 'Admin'
  })
}

export default class ListStoreInputPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      keyTypeWareHouse: 0,
      width: 0,
      loading: false,
      bool: false,
      status: 0,
      dataTable: [],
      data: {},
      keySearch: '',
      category_id: 0,
      type: 0,
      loadingTable: true,
      fromDate: NewDate.setHours(0, 0, 0),
      toDate: NewDate.setHours(23, 59, 59),
      type_warehouse: 0,
      page: 1,
      value: '', // Check here to configure the default column
      arrayRemove: []
    }
    this.handleResize = this.handleResize.bind(this)
    this.onChangeCategory = this.onChangeCategory.bind(this)
  }
  handleResize () {
    console.log(window.innerWidth)
    this.setState({
      width: window.innerWidth
    })
  }
  componentDidMount () {
    this._APIgetListWarehouseImport(
      this.state.type_warehouse,
      this.state.fromDate,
      this.state.toDate,
      this.state.status,
      this.state.keySearch,
      this.state.page,
      this.state.keyTypeWareHouse
    )
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount () {
    this.handleResize()
    window.removeEventListener('resize', this.handleResize)
  }

  _APIgetListWarehouseImport = async (
    warehouse_id,
    start_date,
    end_date,
    status,
    key,
    page,
    type
  ) => {
    try {
      const data = await APIService._getListWarehouseImport(
        warehouse_id,
        start_date,
        end_date,
        status,
        key,
        page,
        type
      )
      let warehouse_import = data.warehouse_import
      warehouse_import.map((item, index) => {
        warehouse_import[index].stt = (this.state.page - 1) * 10 + index + 1
        warehouse_import[index].idAndStt = {
          id: item.id
        }
      })
      this.setState({
        dataTable: warehouse_import,
        data: data,
        loadingTable: false
      })
    } catch (err) {
      console.log(err)
      this.setState({
        loadingTable: false
      })
    }
  }

  onSelectChange = selectedRowKeys => {
    let arrayID = []

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[index].id)
    })
    console.log('selectedRowKeys changed: ', selectedRowKeys)
    console.log('array changed: ', arrayID)
    this.setState({ selectedRowKeys, arrayRemove: arrayID })
  }

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
      this.setState(
        {
          type: value[0],
          category_id: 0
        },
        () =>
          this._APIgetListWarehouseImport(
            this.state.type_warehouse,
            this.state.fromDate,
            this.state.toDate,
            this.state.status,
            this.state.keySearch,
            this.state.page,
            this.state.keyTypeWareHouse
          )
      )
    }
    if (value.length == 2) {
      this.setState(
        {
          type: value[0],
          category_id: value[1]
        },
        () =>
          this._APIgetListWarehouseImport(
            this.state.type_warehouse,
            this.state.fromDate,
            this.state.toDate,
            this.state.status,
            this.state.keySearch,
            this.state.page,
            this.state.keyTypeWareHouse
          )
      )
    }
  }

  _APIpostRemoveProduct = objRemove => {
    try {
      const data = APIService._postRemoveProduct(objRemove)
      message.success('Xóa thành công')
      this.setState(
        {
          selectedRowKeys: []
        },
        () =>
          this._APIgetListWarehouseImport(
            this.state.type_warehouse,
            this.state.fromDate,
            this.state.toDate,
            this.state.status,
            this.state.keySearch,
            this.state.page,
            this.state.keyTypeWareHouse
          )
      )
    } catch (err) {
      console.log(err)
    }
  }

  render () {
    const columns = [
      {
        title: _renderTitle('STT'),
        dataIndex: 'stt',
        key: 'stt',
        align: 'center',
        width: 60,
        // fixed: "left",

        render: id => _renderColumns(id)
      },
      {
        title: _renderTitle('Thời gian nhập'),
        dataIndex: 'import_date',
        key: 'import_date',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 150,

        render: import_date =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              import_date,
              '#DD#/#MM#/#YYYY# #hhhh#:#mm#:#ss#'
            )
          )
      },
      {
        title: _renderTitle('Mã phiếu'),
        dataIndex: 'code',
        key: 'code',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 130,

        render: code => _renderColumns(code)
      },
      {
        title: _renderTitle('Loại hình'),
        dataIndex: 'typeName',
        key: 'typeName',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 170,

        render: typeName => <ITitle level={4} title={typeName} />
      },
      {
        title: _renderTitle('Người lập phiếu'),
        dataIndex: 'creatorName',
        key: 'creatorName',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),

        width: 210,
        render: creatorName => _renderColumns(creatorName)
      },
      {
        title: _renderTitle('Tên kho'),
        dataIndex: 'warehouseName',
        key: 'warehouseName',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 300,

        render: warehouseName => _renderColumns(warehouseName)
      },
      {
        title: _renderTitle('Người nhập kho'),
        dataIndex: 'importerName',
        key: 'importerName',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 220,

        render: importerName => _renderColumns(importerName)
      },
      {
        title: _renderTitle('Giá trị'),
        dataIndex: 'price',
        key: 'price',
        align: 'right',
        // width: _widthColumn(this.state.width, 1),
        width: 120,

        render: price => _renderColumns(priceFormat(price) + 'đ')
      },
      {
        title: _renderTitle('Ghi chú'),
        dataIndex: 'note',
        key: 'note',
        // align: "center",
        // width: _widthColumn(this.state.width, 1),
        width: 230,

        render: note => {
          return (
            <div
              style={{
                wordWrap: 'break-word',
                wordBreak: 'break-word',
                overflow: 'hidden'
              }}
              className='ellipsis-text'
            >
              <ITitle level={4} title={note} />
            </div>
          )
        }
      },
      {
        title: '',
        dataIndex: 'id',
        key: 'id',
        align: 'right',
        width: 50,
        fixed: widthScreen <= 1400 ? 'right' : '',

        render: id => (
          <Row>
            <Col
              onClick={
                () =>
                  this.props.history.push(
                    '/DetailCouponPage/' + id + '/' + 'INPUT'
                    //  search: "?query=abc",
                    // state: { stock_id: id, type: this.state.type }
                  )
                // this.props.history.push({ pathname: "/DetailCouponPage/" })
              }
              className='cursor'
            >
              <ISvg
                name={ISvg.NAME.CHEVRONRIGHT}
                width={11}
                height={20}
                fill={colors.icon.default}
              />
            </Col>
          </Row>
        )
      }
    ]

    const { selectedRowKeys, value } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true
    }
    return (
      <Container fluid>
        <Row className='p-0'>
          <ITitle
            level={1}
            title='Nhập kho'
            style={{
              color: colors.main,
              fontWeight: 'bold',
              flex: 1,
              display: 'flex',
              alignItems: 'flex-end'
            }}
          />
          <ISearch
            onChange={e => {
              this.state.keySearch = e.target.value
              this.setState({
                keySearch: this.state.keySearch
              })
            }}
            onPressEnter={() =>
              this._APIgetListWarehouseImport(
                this.state.type_warehouse,
                this.state.fromDate,
                this.state.toDate,
                this.state.status,
                this.state.keySearch,
                this.state.page,
                this.state.keyTypeWareHouse
              )
            }
            icon={
              <div
                style={{
                  display: 'flex',
                  width: 42,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
                className='cursor'
                onClick={() =>
                  this._APIgetListWarehouseImport(
                    this.state.type_warehouse,
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.status,
                    this.state.keySearch,
                    this.state.page,
                    this.state.keyTypeWareHouse
                  )
                }
                className='cursor'
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>

        <Row className='center' style={{ marginTop: 36 }}>
          <Col
            style={{
              display: 'flex'
              //   justifyContent: "flex-end"
            }}
          >
            <Row style={{ flex: 1 }}>
              <Col className='mt-0 ml-0 center mr-4' xs='auto'>
                <Row className='center mr-5'>
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    width={20}
                    height={20}
                    fill={colors.icon.default}
                  />
                  <div style={{ marginLeft: 10 }}>
                    <ITitle title='Từ ngày' level={4} />
                  </div>
                </Row>
                <Row className='mr-5 p-0'>
                  <IDatePicker
                    style={{ maxWidth: 320, minWidth: 230 }}
                    from={this.state.fromDate}
                    to={this.state.toDate}
                    onChange={(dates, dateStrings) => {
                      if (dateStrings[0] == '' || dateStrings[1] == '') return
                      const from = dates[0]._d.setHours(0, 0, 0)
                      const to = dates[1]._d.setHours(23, 59, 59)
                      this.setState(
                        {
                          fromDate: from,
                          toDate: to
                        },
                        () =>
                          this._APIgetListWarehouseImport(
                            this.state.type_warehouse,
                            this.state.fromDate,
                            this.state.toDate,
                            this.state.status,
                            this.state.keySearch,
                            this.state.page,
                            this.state.keyTypeWareHouse
                          )
                      )
                    }}
                  />
                </Row>
                <Row className='mr-4 p-0'>
                  <ISelectAllWareHouse
                    isBackground={true}
                    style={{
                      background: colors.white,
                      borderWidth: 1,
                      // paddingLeft: 12,
                      borderColor: colors.line,
                      borderStyle: 'solid'
                    }}
                    onSearch={() => {}}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                    value={this.state.type_warehouse}
                    onChange={key => {
                      this.setState(
                        {
                          type_warehouse: key,
                          loadingTable: true
                        },
                        () =>
                          this._APIgetListWarehouseImport(
                            this.state.type_warehouse,
                            this.state.fromDate,
                            this.state.toDate,
                            this.state.status,
                            this.state.keySearch,
                            this.state.page,
                            this.state.keyTypeWareHouse
                          )
                      )
                    }}
                    showSearch
                  />
                </Row>
                <Row className='m-0 p-0' style={{ marginRight: 20 }}>
                  <ISelectTypeWareHouse
                    isBackground={true}
                    style={{
                      // width: "100%",
                      background: colors.white,
                      borderWidth: 1,
                      borderColor: colors.line,
                      borderStyle: 'solid'
                    }}
                    value={this.state.keyTypeWareHouse}
                    onChange={key => {
                      this.setState(
                        {
                          keyTypeWareHouse: key,
                          loadingTable: true
                        },
                        () =>
                          this._APIgetListWarehouseImport(
                            this.state.type_warehouse,
                            this.state.fromDate,
                            this.state.toDate,
                            this.state.status,
                            this.state.keySearch,
                            this.state.page,
                            this.state.keyTypeWareHouse
                          )
                      )
                    }}
                  />
                </Row>
              </Col>

              <Col
                xs='auto'
                style={{
                  flex: 1,
                  display: 'flex',
                  justifyContent: 'flex-end'
                }}
              >
                <Row>
                  <IButton
                    icon={ISvg.NAME.ARROWUP}
                    title='Tạo mới'
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() =>
                      this.props.history.push('/CreateCouponInputPage/INPUT')
                    }
                  />
                  <IButton
                    icon={ISvg.NAME.DOWLOAND}
                    title='Xuất file'
                    color={colors.main}
                    onClick={() => {
                      message.warning('Chức năng đang cập nhật')
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className='mt-5'>
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: '100%' }}
            rowSelection={rowSelection}
            defaultCurrent={2}
            maxpage={6}
            sizeItem={this.state.data.size}
            indexPage={this.state.page}
            maxpage={this.state.data.total}
            onChangePage={page => {
              this.setState(
                {
                  page: page
                },
                () =>
                  this._APIgetListWarehouseImport(
                    this.state.type_warehouse,
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.status,
                    this.state.keySearch,
                    this.state.page,
                    this.state.keyTypeWareHouse
                  )
              )
            }}
            // scroll={{ x: this.state.width }}
          />
        </Row>
      </Container>
    )
  }
}
