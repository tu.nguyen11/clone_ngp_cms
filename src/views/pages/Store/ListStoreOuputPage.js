import { Table, Typography, Avatar, Button, message } from 'antd'
import React, { Component } from 'react'
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
  IDatePicker,
  ISelectWareHouse,
  ISelectTypeWareHouse,
  ISelectAllWareHouse
} from '../../components'
import { colors, images } from '../../../assets'
import { Container, Row, Col } from 'reactstrap'
import { priceFormat } from '../../../utils'
import { APIService } from '../../../services'
import { thisExpression } from '@babel/types'
import QueueAnim from 'rc-queue-anim'
import FormatterDay from '../../../utils/FormatterDay'
const widthScreen = window.innerWidth

function _renderTitle (title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: 'bold' }} />
    </div>
  )
}

function _renderColumns (title) {
  return (
    <div style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
      <ITitle level={4} title={title} />
    </div>
  )
}

function _widthColumn (widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 9
  } else widthColumn = (widthScreen - 300) / 9
  return widthColumn * type
}

const NewDate = new Date()

export default class ListStoreOuputPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      width: 0,
      loading: false,
      bool: false,
      status: 0,
      dataTable: [],
      data: {},
      keySearch: '',
      category_id: 0,
      type: 0,
      loadingTable: true,
      fromDate: NewDate.setHours(0, 0, 0),
      toDate: NewDate.setHours(23, 59, 59),
      type_warehouse: 0,
      keyTypeWareHouse: 0,
      page: 1,
      value: '', // Check here to configure the default column
      arrayRemove: [],

      warehouse_id: 0
    }
    this.handleResize = this.handleResize.bind(this)
    this.onChangeCategory = this.onChangeCategory.bind(this)
  }
  handleResize () {
    console.log(window.innerWidth)
    this.setState({
      width: window.innerWidth
    })
  }
  componentDidMount () {
    this._APIgetListWarehouseExport(
      this.state.fromDate,
      this.state.toDate,
      this.state.warehouse_id,
      this.state.type,
      this.state.keySearch,
      this.state.page
    )
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount () {
    this.handleResize()
    window.removeEventListener('resize', this.handleResize)
  }

  _APIgetListWarehouseExport = async (
    from_date,
    to_date,
    warehouse_id,
    type,
    keyword,
    page
  ) => {
    try {
      const data = await APIService._getListWarehouseExport(
        from_date,
        to_date,
        warehouse_id,
        type,
        keyword,
        page
      )

      let warehouse_exports = data.warehouse_exports
      warehouse_exports.map((item, index) => {
        warehouse_exports[index].stt = (this.state.page - 1) * 10 + index + 1
        warehouse_exports[index].idAndStt = {
          id: item.id,
          index: index
        }
      })
      this.setState({
        dataTable: warehouse_exports,
        data: data,
        loadingTable: false
      })
    } catch (err) {
      console.log(err)
      this.setState({
        loadingTable: false
      })
    }
  }

  onSelectChange = selectedRowKeys => {
    let arrayID = []

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[item].id)
    })
    console.log('selectedRowKeys changed: ', selectedRowKeys)
    this.setState({ selectedRowKeys, arrayRemove: arrayID })
  }

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
      this.setState(
        {
          type: value[0],
          category_id: 0
        },
        () =>
          this._APIgetListWarehouseExport(
            this.state.fromDate,
            this.state.toDate,
            this.state.warehouse_id,
            this.state.type,
            this.state.keySearch,
            this.state.page
          )
      )
    }
    if (value.length == 2) {
      this.setState(
        {
          type: value[0],
          category_id: value[1]
        },
        () =>
          this._APIgetListWarehouseExport(
            this.state.fromDate,
            this.state.toDate,
            this.state.warehouse_id,
            this.state.type,
            this.state.keySearch,
            this.state.page
          )
      )
    }
  }

  render () {
    const columns = [
      {
        title: _renderTitle('STT'),
        dataIndex: 'stt',
        key: 'stt',
        align: 'center',
        width: 60,
        // fixed: "left",

        render: id => _renderColumns(id)
      },
      {
        title: _renderTitle('Thời gian nhập'),
        dataIndex: 'export_time',
        key: 'export_time',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 150,

        render: export_time =>
          _renderColumns(
            FormatterDay.dateFormatWithString(
              export_time,
              '#DD#/#MM#/#YYYY# #hhhh#:#mm#:#ss#'
            )
          )
      },
      {
        title: _renderTitle('Mã phiếu'),
        dataIndex: 'code',
        key: 'code',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 130,

        render: code => _renderColumns(code)
      },
      {
        title: _renderTitle('Loại hình'),
        dataIndex: 'type',
        key: 'type',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 170,

        render: type => (
          <ITitle
            level={4}
            title={[
              { key: 1, value: 'Nhập kho' },
              { key: 2, value: 'Xuât kho' },
              { key: 3, value: 'Xuất chuyển kho' }
            ].map((item, index) => {
              if (item.key == type) return item.value
            })}
          />
        )
      },
      {
        title: _renderTitle('Người lập phiếu'),
        dataIndex: 'founder',
        key: 'founder',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 210,

        render: founder => _renderColumns(founder)
      },
      {
        title: _renderTitle('Tên kho'),
        dataIndex: 'warehouse_name',
        key: 'warehouse_name',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 300,

        render: warehouse_name => _renderColumns(warehouse_name)
      },
      {
        title: _renderTitle('Người xuất kho'),
        dataIndex: 'exporter',
        key: 'exporter',
        align: 'left',
        // width: _widthColumn(this.state.width, 1),
        width: 220,

        render: exporter => _renderColumns(exporter)
      },
      {
        title: _renderTitle('Giá trị'),
        dataIndex: 'total_money',
        key: 'total_money',
        align: 'right',
        // width: _widthColumn(this.state.width, 1),
        width: 120,

        render: total_money => _renderColumns(priceFormat(total_money) + 'đ')
      },
      {
        title: _renderTitle('Ghi chú'),
        dataIndex: 'note',
        key: 'note',
        align: 'center',
        // width: _widthColumn(this.state.width, 1),
        width: 230,

        render: note => {
          return (
            <div
              style={{
                wordWrap: 'break-word',
                wordBreak: 'break-word',
                overflow: 'hidden'
              }}
              className='ellipsis-text'
            >
              <ITitle level={4} title={note} />
            </div>
          )
        }
      },
      {
        title: '',
        dataIndex: 'id',
        key: 'id',
        align: 'right',
        width: 50,
        fixed: widthScreen <= 1400 ? 'right' : '',

        render: id => (
          <Row>
            <Col
              onClick={
                () =>
                  this.props.history.push(
                    '/DetailCouponPage/' + id + '/' + 'OUTPUT'
                    //  search: "?query=abc",
                    // state: { stock_id: id, type: this.state.type }
                  )
                // this.props.history.push({ pathname: "/DetailCouponPage/" })
              }
              className='cursor'
            >
              <ISvg
                name={ISvg.NAME.CHEVRONRIGHT}
                width={11}
                height={20}
                fill={colors.icon.default}
              />
            </Col>
          </Row>
        )
      }
    ]

    const { selectedRowKeys, value } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true
    }
    return (
      <Container fluid>
        <Row className='p-0'>
          <ITitle
            level={1}
            title='Xuất kho'
            style={{
              color: colors.main,
              fontWeight: 'bold',
              flex: 1,
              display: 'flex',
              alignItems: 'flex-end'
            }}
          />
          <ISearch
            onChange={e => {
              this.state.keySearch = e.target.value
              this.setState({
                keySearch: this.state.keySearch
              })
            }}
            placeholder=''
            onPressEnter={() =>
              this._getListProduct(
                this.state.page,
                this.state.status,
                this.state.keySearch,
                this.state.category_id,
                this.state.type
              )
            }
            icon={
              <div
                style={{
                  display: 'flex',
                  width: 42,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
                className='cursor'
                onClick={() =>
                  this._getListProduct(
                    this.state.page,
                    this.state.status,
                    this.state.keySearch,
                    this.state.category_id,
                    this.state.type
                  )
                }
                className='cursor'
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>

        <Row className='center' style={{ marginTop: 36 }}>
          <Col
            style={{
              display: 'flex'
              //   justifyContent: "flex-end"
            }}
          >
            <Row style={{ flex: 1 }}>
              <Col className='mt-0 center  mr-4' xs='auto'>
                <Row className='center mr-5'>
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    width={20}
                    height={20}
                    fill={colors.icon.default}
                  />
                  <div style={{ marginLeft: 10 }}>
                    <ITitle title='Từ ngày' level={4} />
                  </div>
                </Row>
                <Row className='mr-5 p-0'>
                  <IDatePicker
                    style={{ maxWidth: 320, minWidth: 230 }}
                    from={this.state.fromDate}
                    to={this.state.toDate}
                    onChange={(dates, dateStrings) => {
                      if (dateStrings[0] == '' || dateStrings[1] == '') return

                      const from = dates[0]._d.setHours(0, 0, 0)
                      const to = dates[1]._d.setHours(23, 59, 59)
                      this.setState(
                        {
                          fromDate: from,
                          toDate: to,
                          loadingTable: true
                        },
                        () =>
                          this._APIgetListWarehouseExport(
                            this.state.fromDate,
                            this.state.toDate,
                            this.state.warehouse_id,
                            this.state.type,
                            this.state.keySearch,
                            this.state.page
                          )
                      )
                    }}
                  />
                </Row>
                <Row className='mr-4 p-0'>
                  <ISelectAllWareHouse
                    onSearch={() => {}}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                    value={this.state.warehouse_id}
                    onChange={key => {
                      this.setState(
                        {
                          warehouse_id: key,
                          loadingTable: true
                        },
                        () =>
                          this._APIgetListWarehouseExport(
                            this.state.fromDate,
                            this.state.toDate,
                            this.state.warehouse_id,
                            this.state.type,
                            this.state.keySearch,
                            this.state.page
                          )
                      )
                    }}
                    showSearch
                    style={{
                      background: colors.white,
                      borderWidth: 1,
                      borderColor: colors.line,
                      borderStyle: 'solid'
                    }}
                  />
                </Row>
                <Row className='m-0 p-0' style={{ marginRight: 20 }}>
                  <ISelectTypeWareHouse
                    style={{
                      width: '100%',
                      background: colors.white,
                      borderWidth: 1,
                      borderColor: colors.line,
                      borderStyle: 'solid'
                    }}
                    value={this.state.type}
                    onChange={key => {
                      this.setState(
                        {
                          type: key,
                          loadingTable: true
                        },
                        () =>
                          this._APIgetListWarehouseExport(
                            this.state.fromDate,
                            this.state.toDate,
                            this.state.warehouse_id,
                            this.state.type,
                            this.state.keySearch,
                            this.state.page
                          )
                      )
                    }}
                  />
                </Row>
              </Col>

              <Col
                xs='auto'
                // className="m-0 p-0"
                style={{
                  flex: 1,
                  display: 'flex',
                  justifyContent: 'flex-end'
                }}
              >
                <Row>
                  <IButton
                    icon={ISvg.NAME.ARROWUP}
                    title='Tạo mới'
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() =>
                      this.props.history.push('/CreateCouponInputPage/OUTPUT')
                    }
                  />
                  <IButton
                    icon={ISvg.NAME.DOWLOAND}
                    title='Xuất file'
                    color={colors.main}
                    style={{}}
                    onClick={() => {
                      message.warning('Chức năng đang cập nhật')
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className='mt-5'>
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: '100%' }}
            rowSelection={rowSelection}
            defaultCurrent={2}
            maxpage={6}
            sizeItem={this.state.data.size}
            indexPage={this.state.page}
            maxpage={this.state.data.total}
            onChangePage={page => {
              this.setState(
                {
                  page: page
                },
                () =>
                  this._APIgetListWarehouseExport(
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.warehouse_id,
                    this.state.type,
                    this.state.keySearch,
                    this.state.page
                  )
              )
            }}
            // scroll={{ x: this.state.width }}
          />
        </Row>
      </Container>
    )
  }
}
