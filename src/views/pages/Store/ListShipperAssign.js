import { Table, Typography, Avatar, Button, message, Modal, Empty } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
  IDatePicker,
  ISelectAllWareHouse,
  ISelectAllTypeShipper,
  ISelectShipperAssagin,
} from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { APIService } from "../../../services";
import { thisExpression } from "@babel/types";
import FormatterDay from "../../../utils/FormatterDay";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 11;
  } else widthColumn = (widthScreen - 300) / 11;
  return widthColumn * type;
}
const NewDate = new Date();

export default class ListShipperAssign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      width: 0,
      shipper_id: 0,
      loading: false,
      bool: false,
      tableHeight: 0,
      visibleModale: false,
      status: 0,
      dataTable: [],
      data: {},
      keySearch: "",
      category_id: 0,
      type: 0,
      loadingTable: true,
      page: 1,
      value: "", // Check here to configure the default column
      arrayRemove: [],
      warehouse_id: 0,
      shipper_type: 0,
      fromDate: NewDate.setHours(0, 0, 0),
      keySearchAssagin: "",

      toDate: NewDate.setHours(23, 59, 59),
    };
    this.handleResize = this.handleResize.bind(this);
    this.tableContainer = React.createRef();

    this.columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Ngày tạo"),
        dataIndex: "create_time",
        key: "create_time",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (create_time) =>
          _renderColumns(
            FormatterDay.dateFormatWithString(create_time, "#DD#/#MM#/#YYYY#")
          ),
      },
      {
        title: _renderTitle("Mã đơn"),
        dataIndex: "order_code",
        key: "order_code",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (order_code) => _renderColumns(order_code),
      },
      {
        title: _renderTitle("Kho xuất"),
        dataIndex: "export_warehouse",
        key: "export_warehouse",
        align: "left",
        width: _widthColumn(this.state.width, 1),

        render: (export_warehouse) => (
          <ITitle level={4} title={export_warehouse} />
        ),
      },
      {
        title: _renderTitle("Tên người nhận"),
        dataIndex: "receiver_name",
        key: "receiver_name",
        align: "left",
        // width: _widthColumn(this.state.width, 1),
        width: 150,

        render: (receiver_name) => _renderColumns(receiver_name),
      },
      {
        title: _renderTitle("Địa chỉ"),
        dataIndex: "address",
        key: "address",
        align: "left",
        // width: _widthColumn(this.state.width, 2),
        width: 300,

        render: (address) => _renderColumns(address),
      },
      {
        title: _renderTitle("Số điện thoại"),
        dataIndex: "phone",
        key: "phone",
        align: "left",
        // width: _widthColumn(this.state.width, 1),
        width: 120,

        render: (phone) => _renderColumns(phone),
      },
      {
        title: _renderTitle("Tổng tiền"),
        dataIndex: "total_amount",
        key: "total_amount",
        align: "center",
        // width: _widthColumn(this.state.width, 1),
        width: 150,

        render: (total_amount) =>
          _renderColumns(priceFormat(total_amount + "đ")),
      },
      {
        title: _renderTitle("Người tạo đơn"),
        dataIndex: "user_oder_creator",
        key: "user_oder_creator",
        align: "center",
        // width: _widthColumn(this.state.width, 1),
        width: 200,

        render: (user_oder_creator) => _renderColumns(user_oder_creator),
      },
      {
        title: _renderTitle("Shipper"),
        dataIndex: "shipper_name",
        key: "shipper_name",
        align: "center",
        width: _widthColumn(this.state.width, 1),

        render: (shipper_name) => _renderColumns(shipper_name),
      },
    ];
  }
  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }
  componentDidMount() {
    this._APIgetListShipperAssgin(
      this.state.warehouse_id,
      this.state.shipper_type,
      this.state.fromDate,
      this.state.toDate,
      this.state.page,
      this.state.keySearch
    );

    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  _APIpostShipperAssign = async (obj) => {
    try {
      const data = await APIService._postShipperAssign(obj);
      this.setState(
        {
          selectedRowKeys: [],
        },
        () => {
          this._APIgetListShipperAssgin(
            this.state.warehouse_id,
            this.state.shipper_type,
            this.state.fromDate,
            this.state.toDate,
            this.state.page,
            this.state.keySearch
          );
          message.success("Gán đơn thành công");
        }
      );
    } catch (err) {
      console.log(err);
    }
  };

  _APIgetListShipperAssgin = async (
    warehouse_id,
    shipper_type,
    fromDate,
    toDate,
    page,
    keyword
  ) => {
    try {
      const data = await APIService._getListShipperAssgin(
        warehouse_id,
        shipper_type,
        fromDate,
        toDate,
        page,
        keyword
      );

      let list_shipper_assign = data.list_shipper_assign;
      list_shipper_assign.map((item, index) => {
        list_shipper_assign[index].stt = (this.state.page - 1) * 10 + index + 1;
        list_shipper_assign[index].idAndStt = {
          id: item.order_id,
          index: index,
        };
      });
      this.setState({
        dataTable: list_shipper_assign,
        data: data,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];
    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[item].order_id);
    });
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys, arrayRemove: arrayID });
  };

  _APIpostRemoveShipper = async (objRemove) => {
    try {
      const data = await APIService._postRemoveShipperAssign(objRemove);
      message.success("Xóa thành công");
      this.setState(
        {
          selectedRowKeys: [],
        },
        () =>
          this._APIgetListShipperAssgin(
            this.state.warehouse_id,
            this.state.shipper_type,
            this.state.fromDate,
            this.state.toDate,
            this.state.page,
            this.state.keySearch
          )
      );
    } catch (err) {
      console.log(err);
    }
  };

  openModalVisible(visibleModale) {
    this.setState({ visibleModale });
  }

  setModalVisible(visibleModale, type) {
    // type 1: xác nhận , 2 : hủy bỏ
    if (type === 1) {
      const obj = {
        listOrder_id: this.state.arrayRemove,
        shipper_id: this.state.shipper_id,
      };

      this._APIpostShipperAssign(obj);
    }
    this.setState({ visibleModale });
  }

  render() {
    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <div style={{ width: "100%" }}>
        <Modal
          title={
            <ITitle
              level={3}
              title="Gán đơn hàng cho Shipper"
              style={{ fontWeight: "bold" }}
            />
          }
          visible={this.state.visibleModale}
          style={{ borderRadius: 0, background: "#8c8c8c" }}
          centered
          footer={null}
          style={{ padding: 0, margin: 0 }}
          width={400}
          // onOk={() => this.setModalVisible(false, 1)}
          onCancel={() => this.setModalVisible(false, 2)}
        >
          <ISelectShipperAssagin
            placeholder="Chọn shipper"
            showSearch
            warehouse_id={this.state.warehouse_id}
            onChange={(key) => {
              this.setState({
                shipper_id: key,
              });
            }}
            style={{ marginTop: 8 }}
          />
          <div style={{}}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                marginTop: 20,
              }}
            >
              <IButton
                icon={ISvg.NAME.CHECKMARK}
                title="Xác nhận"
                onClick={() => {
                  this.setModalVisible(false, 1);
                }}
                color={colors.main}
                style={{ marginRight: 20, width: 120 }}
              />
              <IButton
                icon={ISvg.NAME.CROSS}
                title="Hủy bỏ"
                onClick={() => {
                  this.setModalVisible(false, 2);
                }}
                color={colors.oranges}
              />
            </div>
          </div>
        </Modal>
        <Container className="p-0 m-0" fluid style={{ flex: 1 }}>
          <Col className="d-flex flex-column" style={{ height: "100%" }}>
            <Row className="p-0">
              <ITitle
                level={1}
                title="Gán shipper"
                style={{
                  color: colors.main,
                  fontWeight: "bold",
                  flex: 1,
                  display: "flex",
                  alignItems: "flex-end",
                }}
              />
              <ISearch
                onChange={(e) => {
                  this.state.keySearch = e.target.value;
                  this.setState({
                    keySearch: this.state.keySearch,
                  });
                }}
                placeholder=""
                onPressEnter={() =>
                  this._APIgetListShipperAssgin(
                    this.state.warehouse_id,
                    this.state.shipper_type,
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.page,
                    this.state.keySearch
                  )
                }
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    className="cursor"
                    onClick={() =>
                      this._APIgetListShipperAssgin(
                        this.state.warehouse_id,
                        this.state.shipper_type,
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.page,
                        this.state.keySearch
                      )
                    }
                    className="cursor"
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 20, height: 20 }}
                    />
                  </div>
                }
              />
            </Row>

            <Row className="center" style={{ marginTop: 36 }}>
              <Col
                style={{
                  display: "flex",
                  //   justifyContent: "flex-end"
                }}
              >
                <Row style={{ flex: 1 }}>
                  <Col className="mt-0 center  mr-4" xs="auto">
                    <Row className="center mr-5">
                      <ISvg
                        name={ISvg.NAME.EXPERIMENT}
                        width={20}
                        height={20}
                        fill={colors.icon.default}
                      />
                      <div style={{ marginLeft: 10 }}>
                        <ITitle title="Từ ngày" level={4} />
                      </div>
                    </Row>
                    <Row className="mr-5 p-0">
                      <IDatePicker
                        style={{ maxWidth: 320, minWidth: 230 }}
                        from={this.state.fromDate}
                        to={this.state.toDate}
                        onChange={(dates, dateStrings) => {
                          if (dateStrings[0] == "" || dateStrings[1] == "")
                            return;

                          const from = dates[0]._d.setHours(0, 0, 0);
                          const to = dates[1]._d.setHours(23, 59, 59);
                          this.setState(
                            {
                              fromDate: from,
                              toDate: to,
                            },
                            () =>
                              this._APIgetListShipperAssgin(
                                this.state.warehouse_id,
                                this.state.shipper_type,
                                this.state.fromDate,
                                this.state.toDate,
                                this.state.page,
                                this.state.keySearch
                              )
                          );
                        }}
                      />
                    </Row>
                    <Row className="center mr-5">
                      <ITitle title="Kho hàng" level={4} />
                    </Row>
                    <Row className="mr-4 p-0">
                      <ISelectAllWareHouse
                        all={true}
                        onSearch={() => {}}
                        callback={(abc) => {}}
                        // warehouse_id={this.state.warehouse_id}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                        // value={this.state.warehouse_id}
                        defaultValue={"Chọn kho"}
                        onChange={(key) => {
                          this.setState(
                            {
                              warehouse_id: key,
                              loadingTable: true,
                            },
                            () =>
                              this._APIgetListShipperAssgin(
                                this.state.warehouse_id,
                                this.state.shipper_type,
                                this.state.fromDate,
                                this.state.toDate,
                                this.state.page,
                                this.state.keySearch
                              )
                          );
                        }}
                        showSearch
                        style={{
                          background: colors.white,
                          borderWidth: 1,
                          borderColor: colors.line,
                          borderStyle: "solid",
                        }}
                      />
                    </Row>
                    <Row className="m-0 p-0" style={{ marginRight: 20 }}>
                      <ISelectAllTypeShipper
                        value={this.state.shipper_type}
                        onChange={(key) => {
                          this.setState(
                            {
                              loadingTable: true,
                              shipper_type: key,
                            },
                            () =>
                              this._APIgetListShipperAssgin(
                                this.state.warehouse_id,
                                this.state.shipper_type,
                                this.state.fromDate,
                                this.state.toDate,
                                this.state.page,
                                this.state.keySearch
                              )
                          );
                        }}
                        style={{
                          background: colors.white,
                          borderWidth: 1,
                          borderColor: colors.line,
                          borderStyle: "solid",
                        }}
                      />
                    </Row>
                  </Col>

                  <Col
                    xs="auto"
                    // className="m-0 p-0"
                    style={{
                      flex: 1,
                      display: "flex",
                      justifyContent: "flex-end",
                    }}
                  >
                    <Row>
                      <IButton
                        icon={""}
                        title="Gán đơn"
                        color={colors.main}
                        style={{ marginRight: 20 }}
                        onClick={() => {
                          if (this.state.arrayRemove.length == 0) {
                            message.warning("Bạn chưa chọn item để gán.");
                            return;
                          }
                          this.setState({
                            visibleModale: true,
                          });
                        }}
                      />
                      <IButton
                        icon={ISvg.NAME.DELETE}
                        title="Xóa đơn"
                        color={colors.oranges}
                        style={{}}
                        onClick={() => {
                          if (this.state.arrayRemove.length == 0) {
                            message.warning(
                              "Bạn chưa chọn mã phân loại để xóa."
                            );
                            return;
                          }
                          const objRemove = {
                            listOrder_id: this.state.arrayRemove,
                          };
                          this._APIpostRemoveShipper(objRemove);
                        }}
                      />
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="mt-5">
              <ITable
                data={this.state.dataTable}
                columns={this.columns}
                loading={this.state.loadingTable}
                style={{ width: "100%" }}
                rowSelection={rowSelection}
                locale={{
                  emptyText: (
                    <Empty
                      image={Empty.PRESENTED_IMAGE_SIMPLE}
                      description={<span>Vui lòng chọn kho</span>}
                    ></Empty>
                  ),
                }}
                defaultCurrent={2}
                sizeItem={this.state.data.size}
                indexPage={this.state.page}
                maxpage={this.state.data.total}
                onChangePage={(page) => {
                  this.setState(
                    {
                      page: page,
                    },
                    () =>
                      this._APIgetListShipperAssgin(
                        this.state.warehouse_id,
                        this.state.shipper_type,
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.page,
                        this.state.keySearch
                      )
                  );
                }}
                // scroll={{ x: this.state.width }}
              />
            </Row>
          </Col>
        </Container>
      </div>
    );
  }
}
