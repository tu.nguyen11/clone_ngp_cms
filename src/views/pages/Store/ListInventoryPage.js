import { Table, Typography, Avatar, Button, message } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
  IDatePicker,
  ISelectWareHouse,
  ISelectAllWareHouse,
} from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { APIService } from "../../../services";
import QueueAnim from "rc-queue-anim";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 10;
  } else widthColumn = (widthScreen - 300) / 10;
  return widthColumn * type;
}

const NewDate = new Date();

export default class ListInventoryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      width: 0,
      loading: false,
      bool: false,
      status: 0,
      dataTable: [],
      data: {},
      keyword: "",
      category_id: 0,
      type_warehouse: 0,
      type_product: 0,

      fromDate: NewDate.setHours(0, 0, 0),
      toDate: NewDate.setHours(23, 59, 59),
      type: 0,
      loadingTable: false,
      page: 1,
      value: "", // Check here to configure the default column
      arrayRemove: [],
    };
    this.handleResize = this.handleResize.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
  }
  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }
  componentDidMount() {
    this._getAPIListInventory(
      this.state.type_warehouse,
      this.state.type_product,
      this.state.category_id,
      this.state.page,
      this.state.fromDate,
      this.state.toDate,
      this.state.keyword
    );
    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  _getAPIListInventory = async (
    type_warehouse,
    type_product,
    category_id,
    page,
    fromDate,
    toDate,
    keyword
  ) => {
    try {
      const data = await APIService._getListInventory(
        type_warehouse,
        type_product,
        category_id,
        page,
        fromDate,
        toDate,
        keyword
      );

      let inventory = data.inventory;
      inventory.map((item, index) => {
        inventory[index].stt = (this.state.page - 1) * 10 + index + 1;
        inventory[index].amount_input = item.inventory_management.amount_input;
        inventory[index].output_inventory =
          item.inventory_management.output_inventory;
        inventory[index].sell_inventerory =
          item.inventory_management.sell_inventerory;
        inventory[index].amount_inventory =
          item.inventory_management.amount_inventory;
        inventory[index].actual_inventory =
          item.inventory_management.actual_inventory;
        inventory[index].difference = item.inventory_management.difference;
        inventory[index].time = item.inventory_management.time;
        inventory[index].user = item.inventory_management.user;
      });
      this.setState({
        dataTable: inventory,
        data: data,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[item].id);
    });
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys, arrayRemove: arrayID });
  };

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
      this.setState(
        {
          type_product: value[0],
          category_id: 0,
        },
        () =>
          this._getAPIListInventory(
            this.state.type_warehouse,
            this.state.type_product,
            this.state.category_id,
            this.state.page,
            this.state.fromDate,
            this.state.toDate,
            this.state.keyword
          )
      );
    }
    if (value.length == 2) {
      this.setState(
        {
          type_product: value[0],
          category_id: value[1],
        },
        () =>
          this._getAPIListInventory(
            this.state.type_warehouse,
            this.state.type_product,
            this.state.category_id,
            this.state.page,
            this.state.fromDate,
            this.state.toDate,
            this.state.keyword
          )
      );
    }
  };

  _APIpostRemoveProduct = (objRemove) => {
    try {
      const data = APIService._postRemoveProduct(objRemove);
      message.success("Xóa thành công");
      this.setState(
        {
          selectedRowKeys: [],
        },
        () =>
          this._getListProduct(
            this.state.page,
            this.state.status,
            this.state.keyword,
            this.state.category_id,
            this.state.type // caterory cha
          )
      );
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Mã sản phẩm"),
        dataIndex: "product_code",
        key: "product_code",
        align: "left",
        // width: _widthColumn(this.state.width, 1),
        width: 150,

        render: (product_code) => _renderColumns(product_code),
      },
      {
        title: _renderTitle("Tên sản phẩm"),
        dataIndex: "product_name",
        key: "product_name",
        align: "left",
        // width: _widthColumn(this.state.width, 1),
        width: 300,

        render: (product_name) => _renderColumns(product_name),
      },
      {
        title: _renderTitle("Đơn vị"),
        dataIndex: "min_unit",
        key: "min_unit",
        align: "center",
        width: _widthColumn(this.state.width, 1),

        render: (min_unit) => <ITitle level={4} title={min_unit} />,
      },
      {
        title: _renderTitle("Nhập trong kỳ"),
        children: [
          {
            title: "Số lượng",
            dataIndex: "amount_input",
            key: "amount_input",
            width: 125,
            align: "center",
            render: (amount_input) => _renderColumns(amount_input),
          },
        ],
        dataIndex: "price",
        key: "price",
        align: "center",
        width: _widthColumn(this.state.width, 2),

        // render: price => _renderColumns(priceFormat(price) + "đ")
      },
      {
        title: _renderTitle("Xuất trong kỳ"),
        dataIndex: "price_max_unit",
        key: "price_max_unit",
        children: [
          {
            title: "Xuất kho",
            dataIndex: "output_inventory",
            key: "output_inventory",
            width: 100,
            align: "center",
            render: (output_inventory) => _renderColumns(output_inventory),
          },
          {
            title: "Bán hàng",
            dataIndex: "sell_inventerory",
            key: "sell_inventerory",
            width: 100,
            align: "center",
            render: (sell_inventerory) => _renderColumns(sell_inventerory),
          },
        ],
        align: "center",
        width: _widthColumn(this.state.width, 1),
      },
      {
        title: _renderTitle("Tồn cuối kỳ"),
        dataIndex: "image",
        key: "image",
        children: [
          {
            title: "Số lượng",
            dataIndex: "amount_inventory",
            key: "amount_inventory",
            width: 125,
            align: "center",
            render: (amount_inventory) => _renderColumns(amount_inventory),
          },
        ],
        align: "center",
        width: _widthColumn(this.state.width, 1),

        render: (image) => (
          <IImage src={image} style={{ width: 40, height: 40 }} />
        ),
      },
      {
        title: _renderTitle("Kiếm kê tồn thực tế"),
        dataIndex: "remain_amount",
        key: "remain_amount",
        children: [
          {
            title: "Số lượng",
            dataIndex: "actual_inventory",
            key: "actual_inventory",
            width: 100,
            align: "center",
            render: (actual_inventory) => _renderColumns(actual_inventory),
          },
          {
            title: "Chênh lệch",
            dataIndex: "difference",
            key: "difference",
            width: 100,
            align: "center",
            render: (difference) => _renderColumns(difference),
          },
        ],
        align: "center",
        width: _widthColumn(this.state.width, 1),

        render: (remain_amount) => _renderColumns(remain_amount),
      },
      {
        title: _renderTitle("Thời gian kiểm kê"),
        dataIndex: "time",
        key: "time",
        align: "center",
        // width: _widthColumn(this.state.width, 1),

        width: 200,
        render: (time) => _renderColumns(time),
      },
      {
        title: _renderTitle("Người kiểm kê"),
        dataIndex: "user",
        key: "user",
        align: "center",
        // width: _widthColumn(this.state.width, 1),
        width: 150,

        render: (user) => _renderColumns(user),
      },
    ];

    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Tồn kho"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <ISearch
            onChange={(e) => {
              this.state.keyword = e.target.value;
              this.setState({
                keyword: this.state.keyword,
              });
            }}
            placeholder=""
            onPressEnter={() =>
              this.setState(
                {
                  loadingTable: true,
                },
                () =>
                  this._getAPIListInventory(
                    this.state.type_warehouse,
                    this.state.type_product,
                    this.state.category_id,
                    this.state.page,
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.keyword
                  )
              )
            }
            icon={
              <div
                style={{
                  display: "flex",
                  width: 42,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                className="cursor"
                onClick={() =>
                  this.setState(
                    {
                      loadingTable: true,
                    },
                    () =>
                      this._getAPIListInventory(
                        this.state.type_warehouse,
                        this.state.type_product,
                        this.state.category_id,
                        this.state.page,
                        this.state.fromDate,
                        this.state.toDate,
                        this.state.keyword
                      )
                  )
                }
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>

        <Row className="center" style={{ marginTop: 36 }}>
          <Col>
            <Row style={{ flex: 1 }}>
              <Col xl="auto" className="m-0 p-0 center" ca>
                <Row className="center" className="m-0 p-0 mr-2">
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    width={20}
                    height={20}
                    fill={colors.icon.default}
                  />
                  <div style={{ marginLeft: 10 }}>
                    <ITitle title="Từ ngày" level={4} />
                  </div>
                </Row>
                <Row className="m-0 p-0">
                  <IDatePicker
                    style={{ maxWidth: 320, minWidth: 230 }}
                    from={this.state.fromDate}
                    to={this.state.toDate}
                    onChange={(value, selectedOptions) => {
                      if (value.length <= 1) {
                        console.log("aa");
                        return;
                      }
                      this.state.fromDate = value[0]._d.setHours(0, 0, 0);
                      this.state.toDate = value[1]._d.setHours(23, 59, 59);
                      this.setState(
                        {
                          start_date: this.state.start_date,
                          end_date: this.state.end_date,
                        },
                        () =>
                          this._getAPIListInventory(
                            this.state.type_warehouse,
                            this.state.type_product,
                            this.state.category_id,
                            this.state.page,
                            this.state.fromDate,
                            this.state.toDate,
                            this.state.keyword
                          )
                      );
                    }}
                  />
                </Row>
                <Row className="center m-0 mr-2 ml-4">
                  {/* <ITitle title="Kho hàng" level={4} /> */}
                </Row>
                <Row className="m-0 p-0">
                  <ISelectAllWareHouse
                    onSearch={() => {}}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                    value={this.state.type_warehouse}
                    onChange={(key) => {
                      this.setState(
                        {
                          type_warehouse: key,
                          loadingTable: true,
                        },
                        () =>
                          this._getAPIListInventory(
                            this.state.type_warehouse,
                            this.state.type_product,
                            this.state.category_id,
                            this.state.page,
                            this.state.fromDate,
                            this.state.toDate,
                            this.state.keyword
                          )
                      );
                    }}
                    showSearch
                    style={{
                      background: colors.white,
                      borderWidth: 1,
                      borderColor: colors.line,
                      borderStyle: "solid",
                    }}
                  />
                </Row>
                <Row className="center m-0 mr-2 ml-4">
                  {/* <ITitle title="Nhóm sản phẩm" level={4} /> */}
                </Row>
                <Row className="m-0 p-0">
                  <ICascader
                    type="category"
                    level={2}
                    placeholder="Chọn nhóm sản phẩm"
                    slectAll={true}
                    onChange={this.onChangeCategory}
                    style={{
                      background: colors.white,
                      borderWidth: 1,
                      borderColor: colors.line,
                      borderStyle: "solid",
                    }}
                  />
                </Row>
              </Col>

              <Col
                // className="m-0 p-0"
                style={{
                  flex: 1,
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                <Row>
                  <IButton
                    icon={ISvg.NAME.STATS}
                    title="Kiểm kho"
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() => message.info("Chức năng đang cập nhập")}
                  />
                  <IButton
                    icon={ISvg.NAME.DOWLOAND}
                    title="Xuất file"
                    color={colors.main}
                    style={{}}
                    onClick={() => {
                      message.warning("Chức năng đang cập nhật");
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mt-5">
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%" }}
            rowSelection={rowSelection}
            defaultCurrent={2}
            maxpage={6}
            sizeItem={this.state.data.size}
            indexPage={this.state.page}
            maxpage={this.state.data.total}
            bordered={true}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                  loadingTable: true,
                },
                () =>
                  this._getAPIListInventory(
                    this.state.type_warehouse,
                    this.state.type_product,
                    this.state.category_id,
                    this.state.page,
                    this.state.fromDate,
                    this.state.toDate,
                    this.state.keyword
                  )
              );
            }}
            // scroll={{ x: this.state.width }}
          />
        </Row>
      </Container>
    );
  }
}
