import React, {useState, useEffect} from "react";
import {Row, Col, message, Tooltip, Drawer} from "antd";
import {
  ISearch,
  ISvg,
  ITable,
  IButton,
  ITitle,
  ISelect,
  IDatePicker,
} from "../../../components";
import {colors, images} from "../../../../assets";
import {APIService} from "../../../../services";
import {useHistory} from "react-router-dom";
import FormatterDay from "../../../../utils/FormatterDay";
import {priceFormat} from "../../../../utils";
import RenderFrames from "./drawerFrames";

const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);
export default function ReportReleaseStock(props) {
  const history = useHistory();

  const [data, setData] = useState({
    report_delivery_bill: [],
    size: 10,
    total: 0,
  });

  const [dataRepo, setDataRepo] = useState([]);
  const [dataProductType, setDataProductType] = useState([]);
  const [dataProductSubType, setDataProductSubType] = useState([]);
  const [visible, setVisible] = useState(false);

  const [isLoading, setLoading] = useState(true);
  const [loadingSubCate, setLoadingSubCate] = useState(false)

  const [filterTable, setFilterTable] = useState({
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
    warehouse_id: "",
    key: "",
    product_type_parent_id: 0,
    type_id: 0,
    page: 1,
  });
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };
  const [dataProps, setDataProps] = useState({
    frame_number: "",
    seri_number: "",
    version_code: "",
    product_name: ""
  })
  const _getDataDropListWareHouse = async () => {
    try {
      const data = await APIService._getReponsitoryCenterTower();
      let arrNew = data.dataRepo.map((item, index) => {
        const key = item.id_repo;
        const value = item.name_repo;
        return {key, value};
      });

      setFilterTable({
        ...filterTable,
        warehouse_id: data.dataRepo[0].id_repo,
      });

      setDataRepo(arrNew);
    } catch (error) {
    }
  };

  const getListBrandCategory = async () => {
    try {
      const data = await APIService._getListBrandCategory();
      let arrNew = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {key, value};
      });

      arrNew.unshift({
        key: 0,
        value: "Tất cả ngành hàng",
      });
      setDataProductType(arrNew);
    } catch (error) {
      console.log(error)
    }
  };

  const _fetchListSubCategpry = async (id) => {
    if (typeof id === 'undefined') id = 0
    try {
      const data = await APIService._getListSubCategpry(id);
      let sub_category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      sub_category.unshift({
        key: 0,
        value: "Tất cả dòng xe",
      });
      setLoadingSubCate(false)
      setDataProductSubType(sub_category);
    } catch (err) {
      setLoadingSubCate(false)
      console.log(err);
    }
  };

  const _getAPIList = async (filterTable) => {
    try {
      const data = await APIService._getListReportReleasesStockBuild112(filterTable);
      data.report_delivery_bill.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.product_id,
        });
      });
      setLoading(false);
      setData(data);
    } catch (error) {
      setLoading(false);
    }
  };

  const _getAPI = async () => {
    await _getDataDropListWareHouse();
    // await getListBrandCategory();
    await _fetchListSubCategpry();

  };

  useEffect(() => {
    _getAPI();
    getListBrandCategory()
  }, []);

  useEffect(() => {
    if (filterTable.warehouse_id !== "") {
      _getAPIList(filterTable);
    }
  }, [filterTable]);

  const columns = [
    {
      title: "Danh mục phiên bản",
      dataIndex: "product_name",
      align: "left",
      key: "product_name",
      width: 250,
      render: (product_name) => (
        <Tooltip title={product_name}>
          <span><u>{!product_name ? "-" : product_name}</u></span>
        </Tooltip>
      ),
    },
    {
      title: "Mã phiên bản",
      dataIndex: "product_code",
      key: "product_code",
      align: "center",
      width: 150,
      render: (product_code) => (
        <span>{!product_code ? "-" : product_code}</span>
      ),
    },
    {
      title: "Đơn vị tính",
      dataIndex: "attribute_name",
      key: "attribute_name",
      align: "center",
      render: (attribute_name) => (
        <span>{!attribute_name ? "-" : attribute_name}</span>
      ),
    },
    {
      title: "Thời gian",
      dataIndex: "finish_date",
      key: "finish_date",
      align: "center",
      render: (finish_date) => (
        <span>
          {!finish_date || finish_date <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
              finish_date,
              "#DD#-#MM#-#YYYY# #hhhh#:#mm#:#ss#"
            )}
        </span>
      ),
    },
    {
      title: "Kho",
      dataIndex: "warehouse",
      key: "warehouse",
      align: "center",
      render: (warehouse) => (
        <Tooltip title={warehouse}>
          <span>{!warehouse ? "-" : warehouse}</span>
        </Tooltip>
      ),
    },
    {
      title: "Showroom",
      dataIndex: "showroom",
      key: "showroom",
      align: "center",
      render: (showroom) => (
        <Tooltip title={showroom}>
          <span>{!showroom ? "-" : showroom}</span>
        </Tooltip>
      ),
    },
    {
      title: "Khách hàng",
      dataIndex: "khach_hang",
      key: "khach_hang",
      align: "center",
      render: (khach_hang) => (
        <Tooltip title={khach_hang}>
          <span>{!khach_hang ? "-" : khach_hang}</span>
        </Tooltip>
      ),
    },
    {
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
      align: "center",
      render: (quantity) => (
        <span>{!quantity ? "-" : priceFormat(quantity)}</span>
      ),
    },
    {
      title: "Mã hợp đồng",
      dataIndex: "ma_hop_dong",
      key: "ma_hop_dong",
      align: "center",
      render: (ma_hop_dong) => (
        <Tooltip title={ma_hop_dong}>
          <span>{!ma_hop_dong ? "-" : ma_hop_dong}</span>
        </Tooltip>
      ),
    },

    {
      title: "Nhân viên bán hàng",
      dataIndex: "staff_name",
      key: "staff_name",
      align: "center",
      render: (staff_name) => (
        <Tooltip title={staff_name}>
          <span>{!staff_name ? "-" : staff_name}</span>
        </Tooltip>
      ),
    },
  ];

  return (
    <div style={{width: "100%"}}>
      <Row>
        <Col span={12}>
          <div>
            <ITitle
              level={1}
              title="Lịch sử xuất kho"
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </div>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã phiên bản">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo mã phiên bản"
                onPressEnter={(e) => {
                  setLoading(true);
                  setFilterTable({
                    ...filterTable,
                    key: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{width: 16, height: 16}}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{margin: "32px 0px"}}>
        <Row>
          <Col span={20}>
            <Row>
              <Col>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{
                      marginLeft: 15,
                      marginRight: 15,
                      width: 70,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filterTable.start_date}
                      to={filterTable.end_date}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          start_date: date[0]._d.setHours(0, 0, 0),
                          end_date: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>
                  <div style={{width: 300, paddingLeft: 30}}>
                    <ISelect
                      value={filterTable.warehouse_id}
                      data={dataRepo}
                      select={true}
                      onChange={(key) => {
                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          warehouse_id: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{width: 300, paddingLeft: 30}}>
                    <ISelect
                      defaultValue="Tất cả ngành hàng"
                      data={dataProductType}
                      select={true}
                      isTooltip={true}
                      // value={filter.type}
                      onChange={(value) => {
                        setLoading(true)
                        setFilterTable({
                          ...filterTable,
                          product_type_parent_id: value,
                          type_id: 0,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{width: 300, paddingLeft: 30}}>
                    <ISelect
                      defaultValue="Tất cả dòng xe"
                      data={dataProductSubType}
                      select={true}
                      loading={loadingSubCate}
                      style={{width: 200}}
                      value={dataProductSubType.length !== 0 ? filterTable.type_id : null}
                      onChange={(key) => {
                        setLoading(true)
                        setFilterTable({
                          ...filterTable,
                          type_id: key,
                          page: 1,
                        });
                      }}
                      onDropdownVisibleChange={(open) => {
                        if (open) {
                          setLoadingSubCate(true)
                          _fetchListSubCategpry(filterTable.product_type_parent_id);
                          return;
                        }

                      }}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>

      {/* <div style={{ margin: "28px 0px", display: "flex" }}>
        <span style={{ fontWeight: 600, fontSize: 15, width: 180 }}>
          Tổng số lượng xuất kho:
        </span>
        <div
          style={{
            borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
            minWidth: 100,
            textAlign: "center",
          }}
        >
          <span style={{ padding: "0px 20px" }}>
            {String(9).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
          </span>
        </div>
        <span style={{ marginLeft: 10 }}>cái</span>
      </div> */}

      <ITable
        data={data.report_delivery_bill}
        columns={columns}
        rowKey={"rowTable"}
        sizeItem={data.size}
        loading={isLoading}
        indexPage={filterTable.page}
        maxpage={data.total}
        bordered={true}
        backgroundWhite={true}
        scroll={{x: data.report_delivery_bill.length === 0 ? 0 : 1600}}
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => {
              showDrawer()
              setDataProps({
                frame_number: record.frame_number,
                seri_number: record.seri_number,
                version_code: record.product_code,
                product_name: record.product_name
              })
              // const dataJSON = event.currentTarget.attributes[0].value;
              // const obj = JSON.parse(dataJSON);
              // const pathname = "/stock/releases/receipt/detail/" + obj.id;
              // history.push(pathname);
            }, // click row
          };
        }}
        onChangePage={(page) => {
          setLoading(true);
          setFilterTable({...filterTable, page: page});
        }}
      />
      <Drawer title="Thông tin xe" placement="right" width={500} onClose={onClose} visible={visible}>
        <RenderFrames type={"REPORT_OUTPUT"} isAsync={true} data={dataProps}/>
      </Drawer>
    </div>
  );
}
