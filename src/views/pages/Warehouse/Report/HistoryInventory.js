import React, {useState, useEffect, useRef} from "react";
import {Row, Col, message, Tooltip, Drawer} from "antd";
import {
  ISearch,
  ITable,
  IButton,
  ITitle,
  ISelect,
  ISvg,
  IDatePicker,
} from "../../../components";
import {colors, images} from "../../../../assets";
import {APIService} from "../../../../services";
import styled from "styled-components";
import {useParams} from "react-router-dom";
import axios from "axios";
import FormatterDay from "../../../../utils/FormatterDay";
import {priceFormat} from "../../../../utils";
import RenderFrames from "./drawerFrames";

const ISelectSearch = styled(ISelect)`
  input {
    padding: 0px !important;
  }
`;

const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);
let startValueNew = new Date(startValue).setDate(
  new Date(startValue).getDate() - 1
);
let endDate = NewDate.setDate(NewDate.getDate() - 1);

export default function HistoryInventory(props) {
  const typingTimeoutRef = useRef(null);
  const {id} = useParams();
  const [data, setData] = useState({
    report_inventory: [],
    size: 10,
    total: 0,
  });

  const [dataProduct, setDataProduct] = useState([]);
  const [dataProductType, setDataProductType] = useState([]);
  const [dataProductSubType, setDataProductSubType] = useState([]);
  const [search, setSearch] = useState("");
  const [isLoading, setLoading] = useState(true);
  const [loadingExport, setLoadingExport] = useState(false);
  const [loadingProduct, setLoadingProduct] = useState(false);
  const [all, setAll] = useState(-1);
  const [productId, setProductId] = useState(-1);

  const [filterTable, setFilterTable] = useState({
    start_date: new Date(startValueNew).setHours(0, 0, 0, 0),
    end_date: new Date(endDate).setHours(23, 59, 59, 999),
    warehouse_id: id,
    // product_code: "0",
    product_type_parent_id: 0,
    type_id: 0,
    key: "",
    page: 1,
  });
  const disabledEndDate = (date) => {
    let dateNow = new Date();
    dateNow.setHours(0, 0, 0, 0);
    if (!date) {
      return false;
    }
    return date && date.valueOf() >= dateNow.getTime();
  };

  const getListBrandCategory = async () => {
    try {
      const data = await APIService._getListBrandCategory();
      let arrNew = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {key, value};
      });
      arrNew.unshift({
        key: 0,
        value: "Tất cả ngành hàng",
      });
      setLoading(false);
      setDataProductType(arrNew);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  const _fetchListSubCategpry = async (id) => {
    try {
      if (typeof id == 'undefined') id = 0;
      const data = await APIService._getListSubCategpry(id);
      let sub_category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      sub_category.unshift({
        key: 0,
        value: "Tất cả dòng xe",
      });
      setLoading(false);
      setLoadingProduct(false)
      setDataProductSubType(sub_category);
    } catch (err) {
      setLoading(false);
      console.log(err);
    }
  };

  const _fetchListProduct = async (search, product_type_parent) => {
    try {
      let product_type_parent_new =
        typeof product_type_parent === "undefined" ? 0 : product_type_parent;
      const data = await APIService._getListProductBanner(
        search,
        product_type_parent_new
      );
      const dataNew = data.product.map((item) => {
        const key = item.code;
        const value = item.name;
        return {key, value};
      });
      if (search === "") {
        dataNew.unshift({
          key: "0",
          value: "Tất cả dòng xe",
        });
      }
      setDataProduct(dataNew);
      setSearch("");
      setLoadingProduct(false);
    } catch (error) {
      console.log(error);
      setLoadingProduct(false);
    }
  };

  const _getAPIList = async (filterTable) => {
    try {
      const data = await APIService._getListHistoryInventory(filterTable);

      data.report_inventory.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
      });
      setLoading(false);

      setData(data);
    } catch (error) {
      setLoading(false);
    }
  };
  const [filter, setFilter] = useState({
    receipt_code: "",
    version_code: ""
  })
  const getAPIReportWareHouseLog = async () => {
    axios({
      url: `${localStorage.getItem(
        "local_domain_server_NGP_ADMIN"
      )}adminservice/export_excel/warehouse/report_export_log_warehouse?user_id=1&start_date=${
        filterTable.start_date
      }&end_date=${filterTable.end_date}
      &warehouse_id=${filterTable.warehouse_id}
      &product_type_parent_id=${filterTable.product_type_parent_id}
      &type_id=${filterTable.type_id}&key=${filterTable.key}&page=0`,
      method: "GET",
      headers: {
        Authorization: "Bearer ",
        token: localStorage.getItem("token"),
      },
      responseType: "blob", // important
    })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        const formatDayStart = FormatterDay.dateFormatWithString(
          filterTable.start_date,
          "#DD##MM##YYYY#"
        );
        const formatDayEnd = FormatterDay.dateFormatWithString(
          filterTable.end_date,
          "#DD##MM##YYYY#"
        );

        link.setAttribute(
          "download",
          `Bao_cao_ton_kho_${formatDayStart}_den_${formatDayEnd}.csv`
        ); //or any other extension
        document.body.appendChild(link);
        link.click();
        setLoadingExport(false);
      })
      .catch(function (error) {
        setLoadingExport(false);
      });
  };

  // useEffect(() => {
  //   _fetchListProduct(search, filterTable.type_id);
  // }, [search]);

  useEffect(() => {
    _fetchListSubCategpry(filterTable.product_type_parent_id, search);
    getListBrandCategory(search);
  }, []);

  useEffect(() => {
    if (filterTable.warehouse_id !== "") {
      _getAPIList(filterTable);
    }
  }, [filterTable]);

  const columns = [
    {
      title: "Danh mục phiên bản",
      dataIndex: "product_name",
      key: "product_name",
      align: "left",
      width: 300,
      render: (product_name) => (
        <Tooltip title={product_name}>
          <span>{!product_name ? "-" : product_name}</span>
        </Tooltip>
      ),
    },
    {
      title: "Mã phiên bản",
      dataIndex: "product_code",
      key: "product_code",
      align: "center",
      width: 120,
      render: (product_code) => (
        <Tooltip title={product_code}>
          <span>{!product_code ? "-" : product_code}</span>
        </Tooltip>
      ),
    },
    {
      title: "Đơn vị tính",
      dataIndex: "attribute_name",
      key: "attribute_name",
      align: "center",
      width: 140,
      render: (attribute_name) => (
        <span>{!attribute_name ? "-" : attribute_name}</span>
      ),
    },
    {
      title: "Tồn đầu kỳ",
      dataIndex: "total_inventory_yesterday",
      key: "total_inventory_yesterday",
      align: "center",
      width: 140,
      render: (total_inventory_yesterday) => (
        <span>
          {!total_inventory_yesterday
            ? "0"
            : priceFormat(total_inventory_yesterday)}
        </span>
      ),
    },
    {
      title: "Nhập trong kỳ",
      dataIndex: "nhap_trong_ky",
      key: "nhap_trong_ky",
      align: "center",
      width: 140,
      render: (nhap_trong_ky) => (
        <span>{!nhap_trong_ky ? "0" : priceFormat(nhap_trong_ky)}</span>
      ),
    },
    {
      title: "Xuất trong kỳ",
      dataIndex: "xuat_trong_ky",
      key: "xuat_trong_ky",
      align: "center",
      width: 140,
      render: (xuat_trong_ky) => (
        <span>{!xuat_trong_ky ? "0" : priceFormat(xuat_trong_ky)}</span>
      ),
    },
    {
      title: "Tồn trong kỳ",
      dataIndex: "ton_trong_ky",
      key: "ton_trong_ky",
      align: "center",
      width: 140,
      render: (ton_trong_ky) => (
        <span>{!ton_trong_ky ? "0" : priceFormat(ton_trong_ky)}</span>
      ),
    },
    {
      title: "Tồn thực tế",
      dataIndex: "ton_thuc_te",
      key: "ton_thuc_te",
      align: "center",
      width: 140,
      render: (ton_thuc_te) => (
        <span>{!ton_thuc_te ? "0" : priceFormat(ton_thuc_te)}</span>
      ),
    },
  ];

  return (
    <div style={{width: "100%"}}>
      <Row>
        <Col span={12}>
          <div>
            <ITitle
              level={1}
              title="Lịch sử tồn kho"
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </div>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã phiên bản, tên phiên bản">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo mã phiên bản, tên phiên bản"
                onPressEnter={(e) => {
                  setLoading(true);
                  setFilterTable({
                    ...filterTable,
                    key: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{width: 16, height: 16}}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{margin: "32px 0px"}}>
        <Row>
          <Col span={21}>
            <Row>
              <Col>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{
                      marginLeft: 15,
                      marginRight: 15,
                      width: 60,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filterTable.start_date}
                      to={filterTable.end_date}
                      disabledDate={disabledEndDate}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          start_date: date[0]._d.setHours(0, 0, 0, 0),
                          end_date: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>
                  <ITitle
                    title="Ngành hàng"
                    level={4}
                    style={{
                      marginLeft: 15,
                      textAlign: 'right',
                      marginRight: 15,
                      width: 90,
                    }}
                  />
                  <div style={{width: 320, marginLeft: 15, marginRight: 15}}>
                    <ISelect
                      defaultValue="Tất cả ngành hàng"
                      data={dataProductType}
                      select={true}
                      onChange={(key) => {
                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          type_id: 0,
                          product_type_parent_id: key,
                          key: "",
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <ITitle
                    title="Dòng xe"
                    level={4}
                    style={{
                      marginLeft: 15,
                      textAlign: 'right',
                      marginRight: 15,
                      width: 60,
                    }}
                  />
                  <div style={{width: 320}}>
                    <ISelect
                      defaultValue="Tất cả dòng xe"
                      data={dataProductSubType}
                      select={true}
                      loading={loadingProduct}
                      onChange={(key) => {
                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          type_id: key,
                          key: "",
                          page: 1,
                        });
                      }}
                      value={dataProductSubType.length !== 0 ? filterTable.type_id : null}
                      onDropdownVisibleChange={(open) => {
                        if (open) {
                          setLoadingProduct(true);
                          _fetchListSubCategpry(filterTable.product_type_parent_id, search);
                          return;
                        }
                      }}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={3}>
            <div className="flex justify-end">
              <IButton
                color={colors.main}
                loading={loadingExport}
                title="Xuất báo cáo"
                onClick={async () => {
                  setLoadingExport(true);
                  await getAPIReportWareHouseLog(filterTable);
                }}
              />
            </div>
          </Col>
        </Row>
      </div>

      <ITable
        data={data.report_inventory}
        columns={columns}
        rowKey={"rowTable"}
        sizeItem={data.size}
        loading={isLoading}
        indexPage={filterTable.page}
        maxpage={data.total}
        bordered={true}
        backgroundWhite={true}
        onChangePage={(page) => {
          setLoading(true);
          setFilterTable({...filterTable, page: page});
        }}
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => {
              // setFilter({
              //   receipt_code: record.
              //     version_code:record.product_code
              // })
            }, // click row
          };
        }}
      />
    </div>
  );
}
