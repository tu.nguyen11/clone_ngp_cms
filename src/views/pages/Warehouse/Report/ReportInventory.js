import React, {useState, useEffect} from "react";
import {Row, Col, message, Tooltip} from "antd";
import {ISearch, ITable, IButton, ITitle, ISelect} from "../../../components";
import {colors, images} from "../../../../assets";
import {APIService} from "../../../../services";
import {useHistory} from "react-router-dom";
import {priceFormat} from "../../../../utils";
import {Drawer} from 'antd';
import RenderFrames from "./drawerFrames";

export default function ReportInventory(props) {
  const history = useHistory();

  const [data, setData] = useState({
    report_inventory: [],
    size: 10,
    total: 0,
  });

  const [dataRepo, setDataRepo] = useState([]);
  const [dataProductType, setDataProductType] = useState([]);
  const [dataProductSubType, setDataProductSubType] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null)
  const [isLoading, setLoading] = useState(true);
  const [loadingSubCate, setLoadingSubCate] = useState(false)
  const [visible, setVisible] = useState(false);
  const [filterTable, setFilterTable] = useState({
    // start_date: NewDate,
    warehouse_id: "",
    product_type_parent_id: 0,
    type_id: 0,
    key: "",
    page: 1,
  });
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };
  const _getDataDropListWareHouse = async () => {
    try {
      const data = await APIService._getReponsitoryCenterTower();
      let arrNew = data.dataRepo.map((item, index) => {
        const key = item.id_repo;
        const value = item.name_repo;
        return {key, value};
      });

      setFilterTable({
        ...filterTable,
        warehouse_id: data.dataRepo[0].id_repo,
      });
      setDataRepo(arrNew);
    } catch (error) {
    }
  };

  const getListBrandCategory = async () => {
    try {
      const data = await APIService._getListBrandCategory();
      let arrNew = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {key, value};
      });

      arrNew.unshift({
        key: 0,
        value: "Tất cả ngành hàng",
      });
      setDataProductType(arrNew);
    } catch (error) {
    }
  };

  const _fetchListSubCategpry = async (id) => {
    try {
      if (typeof id === 'undefined') id = 0
      const data = await APIService._getListSubCategpry(id);
      let sub_category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      sub_category.unshift({
        key: 0,
        value: "Tất cả dòng xe",
      });
      setLoadingSubCate(false)
      setDataProductSubType(sub_category);
    } catch (err) {
      setLoadingSubCate(false)
      console.log(err);
    }
  };

  const _getAPIList = async (filterTable) => {
    try {
      const data = await APIService._getListReportInventory(filterTable);

      data.report_inventory.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.product_id,
        });
      });
      setLoading(false);
      setData(data);
    } catch (error) {
      setLoading(false);
    }
  };

  const _getAPI = async () => {
    await _getDataDropListWareHouse();
    await getListBrandCategory();
    await _fetchListSubCategpry();
  };

  useEffect(() => {
    _getAPI();
  }, []);

  useEffect(() => {
    if (filterTable.warehouse_id !== "") {
      _getAPIList(filterTable);
    }
  }, [filterTable]);

  const columns = [
    {
      title: "Danh mục phiên bản",
      dataIndex: "product_name",
      key: "product_name",
      align: "left",
      width: 300,
      fixed: 'left',
      render: (product_name) => (
        <Tooltip title={product_name}>
          <span><u>{!product_name ? "-" : product_name}</u></span>
        </Tooltip>
      ),
    },
    {
      title: "Mã phiên bản",
      dataIndex: "product_code",
      key: "product_code",
      align: "left",
      width: 120,
      render: (product_code) => (
        <Tooltip title={product_code}>
          <span>{!product_code ? "-" : product_code}</span>
        </Tooltip>
      ),
    },
    {
      title: "Đơn vị tính",
      dataIndex: "attribute_name",
      key: "attribute_name",
      align: "center",
      width: 140,
      render: (attribute_name) => (
        <span>{!attribute_name ? "-" : attribute_name}</span>
      ),
    },
    {
      title: "Tồn đầu kỳ",
      dataIndex: "total_inventory_yesterday",
      key: "total_inventory_yesterday",
      align: "center",
      width: 140,
      render: (total_inventory_yesterday) => (
        <span>
          {!total_inventory_yesterday
            ? "0"
            : priceFormat(total_inventory_yesterday)}
        </span>
      ),
    },
    {
      title: "Nhập trong kỳ",
      dataIndex: "total_input_today",
      key: "total_input_today",
      align: "center",
      width: 140,
      render: (total_input_today) => (
        <span>{!total_input_today ? "0" : priceFormat(total_input_today)}</span>
      ),
    },
    {
      title: "Xuất trong kỳ",
      dataIndex: "total_output_today",
      key: "total_output_today",
      align: "center",
      width: 140,
      render: (total_output_today) => (
        <span>
          {!total_output_today ? "0" : priceFormat(total_output_today)}
        </span>
      ),
    },
    {
      title: "Tồn thực tế",
      dataIndex: "total_inventory",
      key: "total_inventory",
      align: "center",
      width: 140,
      render: (total_inventory) => (
        <span>{!total_inventory ? "0" : priceFormat(total_inventory)}</span>
      ),
    },
    {
      title: "Chờ giao",
      dataIndex: "total_delivery",
      key: "total_delivery",
      align: "center",
      width: 140,
      render: (total_delivery) => (
        <span>{!total_delivery ? "0" : priceFormat(total_delivery)}</span>
      ),
    },
    {
      title: "Chờ xác nhận",
      dataIndex: "book",
      key: "book",
      align: "center",
      width: 140,
      render: (book) => <span>{!book ? "0" : priceFormat(book)}</span>,
    },
    {
      title: "Tồn khả dụng",
      dataIndex: "availability",
      key: "availability",
      align: "center",
      width: 140,
      render: (availability) => (
        <span>{!availability ? "0" : priceFormat(availability)}</span>
      ),
    },
  ];

  return (
    <div style={{width: "100%"}}>
      <Row>
        <Col span={12}>
          <div>
            <ITitle
              level={1}
              title="Báo cáo tồn kho"
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </div>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo mã phiên bản, tên phiên bản">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo mã phiên bản, tên phiên bản"
                onPressEnter={(e) => {
                  setLoading(true);
                  setFilterTable({
                    ...filterTable,
                    key: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{width: 16, height: 16}}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{margin: "32px 0px"}}>
        <Row>
          <Col span={20}>
            <Row>
              <Col>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  {/* <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{
                      marginLeft: 25,
                      marginRight: 25,
                      width: 60,
                    }}
                  />
                  <div>
                    <IStyleDatePicker
                      showTime
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                        height: 42,
                        minWidth: 140,
                      }}
                      format={"DD-MM-YYYY"}
                      onChange={(date) => {
                        setFilterTable({
                          ...filterTable,
                          start_date: date._d.getTime(),
                        });

                        console.log(date._d.getTime());
                      }}
                      value={moment(
                        new Date(filterTable.start_date),
                        "DD-MM-YYYY"
                      )}
                    />
                  </div>
                  */}
                  <ITitle
                    title="Kho hàng"
                    level={4}
                    style={{
                      marginRight: 15,
                      width: 70,
                    }}
                  />
                  <div style={{width: 300}}>
                    <ISelect
                      value={filterTable.warehouse_id}
                      data={dataRepo}
                      select={true}
                      onChange={(key) => {
                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          warehouse_id: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <ITitle
                    title="Ngành hàng"
                    level={4}
                    style={{
                      textAlign: 'right',
                      marginLeft: 15,
                      marginRight: 15,
                      width: 90,
                    }}
                  />
                  <div style={{width: 300}}>
                    <ISelect
                      defaultValue="Tất cả ngành hàng"
                      data={dataProductType}
                      select={true}
                      style={{width: 200}}
                      onChange={(key) => {
                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          type_id: 0,
                          product_type_parent_id: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <ITitle
                    title="Dòng xe"
                    level={4}
                    style={{
                      textAlign: 'right',
                      marginLeft: 15,
                      marginRight: 15,
                      width: 90,
                    }}
                  />
                  <div style={{width: 300}}>
                    <ISelect
                      defaultValue="Tất cả dòng xe"
                      data={dataProductSubType}
                      select={true}
                      loading={loadingSubCate}
                      style={{width: 200}}
                      value={dataProductSubType.length !== 0 ? filterTable.type_id : null}
                      onChange={(key) => {
                        setLoading(true);
                        setFilterTable({
                          ...filterTable,
                          type_id: key,
                          page: 1,
                        });
                      }}
                      onDropdownVisibleChange={(open) => {
                        if (open) {
                          setLoadingSubCate(true)
                          _fetchListSubCategpry(filterTable.product_type_parent_id);
                          return;
                        }

                      }}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={4}>
            <div className="flex justify-end">
              <IButton
                color={colors.main}
                title="Lịch sử tồn kho"
                style={{marginRight: 15}}
                onClick={() => {
                  history.push(
                    "/warehouse/history/inventory/list/" +
                    filterTable.warehouse_id
                  );
                }}
              />
              {/*<IButton*/}
              {/*  color={colors.main}*/}
              {/*  title="Cập nhật"*/}
              {/*  onClick={() => {*/}
              {/*    setLoading(true);*/}
              {/*    _getAPIList(filterTable);*/}
              {/*  }}*/}
              {/*/>*/}
            </div>
          </Col>
        </Row>
      </div>

      <ITable
        data={data.report_inventory}
        columns={columns}
        rowKey={"rowTable"}
        sizeItem={data.size}
        loading={isLoading}
        indexPage={filterTable.page}
        maxpage={data.total}
        bordered={true}
        backgroundWhite={true}
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => {
              showDrawer()
              setSelectedProduct(record.product_code)
            }, // click row
          };
        }}
        onChangePage={(page) => {
          setLoading(true);
          setFilterTable({...filterTable, page: page});
        }}
      />
      <Drawer title="Thông tin xe" placement="right" width={500} onClose={onClose} visible={visible}>
        <RenderFrames type={'INVENTORY'} code={selectedProduct}/>
      </Drawer>
    </div>
  );
}
