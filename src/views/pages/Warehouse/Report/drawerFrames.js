import React, {useEffect, useState} from "react";
import {Row, Col, Empty, Spin} from "antd";
import {priceFormat} from "../../../../utils";
import {ITableHtml} from "../../../components";
import {APIService} from "../../../../services";

export default function RenderFrames(props) {
  const {isAsync, data, type} = props
  const [dataFrames, setDataFrames] = useState({
    full_name: "",
    product: [],
    version_code: ""
  })

  const [loading, setLoading] = useState(true)
  const fetchInventory = async code => {
    try {
      setLoading(true)
      const res = await APIService._seeFramesNumber(code)
      setDataFrames(res)
      setLoading(false)
    } catch (e) {
      setLoading(false)
      setDataFrames({
        full_name: "",
        product: [],
        version_code: ""
      })
      console.log(e)
    }
  }

  async function fetchHistory(obj) {
    try {
      setLoading(true)
      const res = await APIService._getListFramesInHistoryInventory(obj)
      setDataFrames(res)
      setLoading(false)
    } catch (e) {
      setLoading(false)
      setDataFrames({
        full_name: "",
        product: [],
        version_code: ""
      })
      console.log(e)
    }
  }

  function setDataReportOutput() {
    setLoading(false)
    setDataFrames({
      ...dataFrames,
      version_code:data.version_code,
      full_name: data.product_name,
      product: [
        {
          seri_number: data.seri_number,
          frame_number: data.frame_number
        }

      ]
    })
  }

  useEffect(() => {
    switch (type) {
      case "INVENTORY":
        fetchInventory(props.code)
        break
      case "REPORT_INPUT":
        fetchHistory(props.code)
        break
      case "REPORT_OUTPUT":
        setDataReportOutput()
      default:
        return
    }
  }, [props.code, data])
  let headerTable = [
    {
      name: "Số khung",
      align: "left",
    },
    {
      name: "Số seri",
      align: "left",
    },
  ];
  const headerTableProduct = (headerTable) => {
    return (
      <tr className="scroll">
        {headerTable.map((item, index) => (
          <th
            className="th-table"
            style={{
              textAlign: item.align,
              fontWeight: 700,
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        height="100px"
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td colspan="7" style={{padding: 20}}>
          <Empty description="Không có dữ liệu"/>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => {
        return (
          <tr className="tr-table" style={{maxHeight: 500, overflow: "auto"}}>
            <td
              className="td-table"
              style={{textAlign: "left", maxWidth: 200, fontWeight: "normal"}}
            >
              {!item.frame_number ? "-" : item.frame_number}
            </td>
            <td className="td-table" style={{fontWeight: "normal"}}>
              {!item.seri_number ? "-" : item.seri_number}
            </td>
          </tr>
        );
      })
    );
  };
  return (
    <div>
      <Row>
        {
          loading ?
            <Col className={'mb-3 d-flex align-items-center justify-content-center'} span={24}> <Spin tip="Loading...">
            </Spin> </Col> : <> <Col className={'mb-3'} span={24}>
              <h6>Tên xe</h6>
              <p>{dataFrames.full_name || '-'}</p>
            </Col>
              <Col className={'mb-3'} span={24}>
                <h6>Mã phiên bản</h6>
                <p>{dataFrames.version_code || '-'}</p>
              </Col>
              <Col className={'mb-5'} span={24}>
                <ITableHtml
                  childrenBody={bodyTableProduct(
                    dataFrames.product
                  )}
                  childrenHeader={headerTableProduct(headerTable)}
                />
              </Col>
            </>
        }
      </Row>
    </div>
  )
}