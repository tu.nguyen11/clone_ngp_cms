import React, {useState, useEffect} from "react";
import {Row, Col, Skeleton, Empty, message} from "antd";
import {ITitle, ITableHtml, IButton, ISvg} from "../../../components";
import {colors} from "../../../../assets";

import {APIService} from "../../../../services";
import {useParams, useHistory} from "react-router-dom";
import FormatterDay from "../../../../utils/FormatterDay";
import {priceFormat} from "../../../../utils";
import {useDispatch} from "react-redux";
import {
  ASSIGNED_DATA_EDIT,
  CREATE_DATA_PRODUCT,
} from "../../../store/reducers";

export default function DetailStockReceipt(props) {
  const history = useHistory();
  const params = useParams();
  const {id} = params;
  const dispatch = useDispatch();
  const [showButton, setShowButton] = useState(false)
  const [isLoading, setIsLoading] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);
  const [dataDetail, setDataDetail] = useState({list_product: []});

  const getDetailStockReceipt = async (id) => {
    try {
      const data = await APIService._getDetailStockReceipt(id);
      data.receipt.list_product.map((item, index) => {
        item.stt = index + 1;
        return item;
      });
      if (data.receipt.status === 2) {
        setShowButton(true)
      } else {
        setShowButton(false)
      }
      setDataDetail(data.receipt);
      await _getListTreeProduct("", 0);
      setIsLoading(false);
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  };

  const _getListTreeProduct = async (search, product_type) => {
    try {
      const data = await APIService._getListTreeProductEventGift(search, product_type);
      const keyDispatch = {
        key: "dataProduct",
        value: data.product_type,
      };
      dispatch(CREATE_DATA_PRODUCT(keyDispatch));
    } catch (error) {
      console.log(error);
    }
  };

  const postUpdateStatusStockReceipt = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postUpdateStatusStockReceiptForBuild112(obj);
      setLoadingButton(false);
      await getDetailStockReceipt(id);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postRequestApprovalReceipt = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postRequestApprovalReceipt(obj);
      setLoadingButton(false);
      message.success("Yêu cầu duyệt thành công");
      setIsLoading(true);
      await getDetailStockReceipt(id);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  useEffect(() => {
    getDetailStockReceipt(id);
  }, []);

  let headerTable = [
    {
      name: "STT",
      align: "center",
    },
    {
      name: "Tên sản phẩm",
      align: "left",
    },
    {
      name: "Mã sản phẩm",
      align: "left",
    },
    {
      name: "Số lượng",
      align: "center",
    },
    {
      name: "Đơn vị tính",
      align: "center",
    },
    {
      name: "Số seri",
      align: "center",
    },
    {
      name: "Số khung",
      align: "center",
    },
    // {
    //   name: "Tỉ lệ quy đổi",
    //   align: "center",
    // },
    // {
    //   name: "SL quy đổi",
    //   align: "center",
    // },
  ];

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="scroll">
        {headerTable.map((item, index) => (
          <th
            className="th-table"
            style={{
              textAlign: item.align,
              fontWeight: 700,
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        height="100px"
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td colspan="7" style={{padding: 20}}>
          <Empty description="Không có dữ liệu"/>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => {
        return (
          <tr className="tr-table" style={{maxHeight: 500, overflow: "auto"}}>
            <td
              className="td-table"
              style={{
                textAlign: "center",
                maxWidth: 150,
                fontWeight: "normal",
              }}
            >
              {!item.stt ? "-" : item.stt}
            </td>
            <td
              className="td-table"
              style={{textAlign: "left", maxWidth: 200, fontWeight: "normal"}}
            >
              {!item.name ? "-" : item.name}
            </td>
            <td className="td-table" style={{fontWeight: "normal"}}>
              {!item.code ? "-" : item.code}
            </td>
            <td
              className="td-table"
              style={{textAlign: "center", fontWeight: "normal"}}
            >
              {!item.quantity ? "-" : priceFormat(item.quantity)}
            </td>
            <td
              className="td-table"
              style={{textAlign: "center", fontWeight: "normal"}}
            >
              {!item.attribute ? "-" : item.attribute}
            </td>
            <td
              className="td-table"
              style={{textAlign: "center", fontWeight: "normal"}}
            >
              {!item.frame_numbers ? "-" : item.frame_numbers}
            </td>
            <td
              className="td-table"
              style={{textAlign: "center", fontWeight: "normal"}}
            >
              {!item.seri_numbers ? "-" : item.seri_numbers}
            </td>
            {/*<td*/}
            {/*  className="td-table"*/}
            {/*  style={{ textAlign: "center", fontWeight: "normal" }}*/}
            {/*>*/}
            {/*  {!item.attribute_ratio ? "-" : priceFormat(item.attribute_ratio)}*/}
            {/*</td>*/}
            {/*<td*/}
            {/*  className="td-table"*/}
            {/*  style={{ textAlign: "center", fontWeight: "normal" }}*/}
            {/*>*/}
            {/*  {!item.quantity_min ? "-" : priceFormat(item.quantity_min)}*/}
            {/*</td>*/}
          </tr>
        );
      })
    );
  };

  return (
    <div style={{width: "100%"}}>
      <Row gutter={[12, 24]}>
        <Col span={24}>
          <ITitle
            level={1}
            title="Chi tiết phiếu nhập kho"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        {showButton && (
          <Col span={24}>
            <div className="flex justify-end">
              <IButton
                color={colors.mainDark}
                title="Nhập kho"
                // icon={ISvg.NAME.WRITE}
                style={{marginRight: 15}}
                onClick={() => {
                  const obj = {
                    enum_update_status_receipt: 0,
                    id: [
                      Number(id)
                    ],
                    status: 1
                  };
                  postUpdateStatusStockReceipt(obj);
                  message.success("Đã cập nhật tồn kho");
                }}
              />
              {/*{dataDetail.is_import === 0 && (*/}
                <IButton
                  color={colors.main}
                  title="Chỉnh sửa"
                  loading={loadingButton}
                  style={{marginRight: 15}}
                  onClick={async () => {
                    // if (dataDetail.is_import === 0) {
                      const dataAction = {
                        listProduct: dataDetail.list_product,
                      };
                      dispatch(ASSIGNED_DATA_EDIT(dataAction));
                      history.push("/stock/receipt/edit/" + id);
                    // }
                  }}
                />
              {/*)}*/}
              <IButton
                color={colors.oranges}
                title="Hủy"
                icon={ISvg.NAME.CROSS}
                onClick={() => {
                  const obj = {
                    enum_update_status_receipt: 1,
                    id: [
                      Number(id)
                    ],
                    status: -1
                  };
                  postUpdateStatusStockReceipt(obj);
                  message.success("Hủy thành công");
                }}
              />
            </div>
          </Col>
        )}
        {/*{dataDetail.status === 1 || dataDetail.status === -1 ? null : (*/}
        {/*  <Col span={24}>*/}
        {/*    <div className="flex justify-end" style={{ marginRight: 10 }}>*/}
        {/*      {dataDetail.status !== 0 ? null : (*/}
        {/*        <IButton*/}
        {/*          color={colors.main}*/}
        {/*          label="Nhập kho"*/}
        {/*          loading={loadingButton}*/}
        {/*          onClick={() => {*/}
        {/*            const obj = {*/}
        {/*              id: [Number(id)],*/}
        {/*              status: 1,*/}
        {/*            };*/}
        {/*            postUpdateStatusStockReceipt(obj);*/}
        {/*            message.success("Đã cập nhật tồn kho");*/}
        {/*          }}*/}
        {/*        />*/}
        {/*      )}*/}
        {/*      {dataDetail.status !== 3 ? null : (*/}
        {/*        <>*/}
        {/*          <IButton*/}
        {/*            color={colors.main}*/}
        {/*            title="Chỉnh sửa"*/}
        {/*            loading={loadingButton}*/}
        {/*            style={{ marginRight: 15 }}*/}
        {/*            onClick={async () => {*/}
        {/*              if (dataDetail.is_import === 0) {*/}
        {/*                const dataAction = {*/}
        {/*                  listProduct: dataDetail.list_product,*/}
        {/*                };*/}
        {/*                dispatch(ASSIGNED_DATA_EDIT(dataAction));*/}
        {/*              }*/}
        {/*              history.push("/stock/receipt/edit/" + id);*/}
        {/*            }}*/}
        {/*          />*/}
        {/*          <IButton*/}
        {/*            color={colors.main}*/}
        {/*            title="Yêu cầu duyệt"*/}
        {/*            loading={loadingButton}*/}
        {/*            style={{ marginRight: 15 }}*/}
        {/*            onClick={async () => {*/}
        {/*              const obj = {*/}
        {/*                id: [Number(id)],*/}
        {/*                status: 0,*/}
        {/*              };*/}
        {/*              await postRequestApprovalReceipt(obj);*/}
        {/*            }}*/}
        {/*          />*/}
        {/*        </>*/}
        {/*      )}*/}
        {/*      {dataDetail.status !== 0 ? null : (*/}
        {/*        <IButton*/}
        {/*          color={colors.oranges}*/}
        {/*          title="Hủy"*/}
        {/*          icon={ISvg.NAME.CROSS}*/}
        {/*          onClick={() => {*/}
        {/*            const obj = {*/}
        {/*              id: [Number(id)],*/}
        {/*              status: -1,*/}
        {/*            };*/}
        {/*            postUpdateStatusStockReceipt(obj);*/}
        {/*            message.success("Hủy thành công");*/}
        {/*          }}*/}
        {/*        />*/}
        {/*      )}*/}
        {/*    </div>*/}
        {/*  </Col>*/}
        {/*)}*/}

        <Col span={24}>
          <div
            style={{
              height: "100%",
              marginTop:
                dataDetail.status === 1 ||
                dataDetail.status === -1 ||
                dataDetail.status === 3
                  ? 25
                  : 0,
            }}
          >
            <Row gutter={[25, 0]} style={{height: "100%"}} type="flex">
              <Col span={7}>
                <div
                  style={{
                    padding: "30px 40px",
                    background: colors.white,
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                    height: 650,
                  }}
                >
                  {isLoading ? (
                    <div>
                      <Skeleton
                        loading={true}
                        active
                        paragraph={{rows: 16}}
                      />
                    </div>
                  ) : (
                    <Row gutter={[12, 20]}>
                      <Col>
                        <ITitle
                          level={2}
                          title="Thông tin phiếu nhập kho"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                      </Col>
                      <Col span={24}>
                        <ITitle
                          level={4}
                          title={"Mã phiếu nhập kho"}
                          style={{
                            fontWeight: 600,
                          }}
                        />
                        <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.code ? "-" : dataDetail.code}
                        </span>
                      </Col>
                      <Col span={24}>
                        <ITitle
                          level={4}
                          title={"Tên kho"}
                          style={{
                            fontWeight: 600,
                          }}
                        />
                        <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.warehouse_name
                            ? "-"
                            : dataDetail.warehouse_name}
                        </span>
                      </Col>

                      <Col span={24}>
                        <ITitle
                          level={4}
                          title={"Trạng thái"}
                          style={{
                            fontWeight: 600,
                          }}
                        />
                        <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.status_name
                            ? "-"
                            : dataDetail.status_name}
                        </span>
                      </Col>
                      <Col span={24}>
                        <ITitle
                          level={4}
                          title={"Thời gian nhập kho"}
                          style={{
                            fontWeight: 600,
                          }}
                        />
                        <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.finish_date
                            ? "-"
                            : FormatterDay.dateFormatWithString(
                              dataDetail.finish_date,
                              "#YYYY#-#MM#-#DD# #hhhh#:#mm#:#ss#"
                            )}
                        </span>
                      </Col>
                      <Col span={24}>
                        <ITitle
                          level={4}
                          title={"Ghi chú"}
                          style={{
                            fontWeight: 600,
                          }}
                        />
                        <div
                          style={{
                            wordWrap: "break-word",
                            maxHeight: 160,
                            overflow: "auto",
                          }}
                        >
                          {!dataDetail.note ? "-" : dataDetail.note}
                        </div>
                      </Col>
                    </Row>
                  )}
                </div>
              </Col>
              <Col span={17}>
                <div
                  style={{
                    padding: "30px 40px",
                    background: colors.white,
                    height: "100%",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  {isLoading ? (
                    <div>
                      <Skeleton
                        loading={true}
                        active
                        paragraph={{rows: 16}}
                      />
                    </div>
                  ) : (
                    <Row gutter={[12, 20]}>
                      <Col>
                        <ITitle
                          level={2}
                          // title="Sản phẩm nhập kho"
                          title="Phiên bản nhập kho"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                      </Col>

                      <Col span={24}>
                        <ITableHtml
                          childrenBody={bodyTableProduct(
                            dataDetail.list_product
                          )}
                          childrenHeader={headerTableProduct(headerTable)}
                        />
                      </Col>
                    </Row>
                  )}
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}
