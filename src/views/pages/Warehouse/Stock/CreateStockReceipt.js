import React, {useState, useEffect} from "react";
import {Row, Col, Modal, message, Skeleton, Form} from "antd";
import {
  ITitle,
  IButton,
  ISvg,
  ISelect,
  ITableHtml, IInput,
} from "../../../components";
import {
  IFormItem,
  IInputText,
  IError,
  StyledInputNumber,
} from "../../../components/common";
import {colors} from "../../../../assets";

import {APIService} from "../../../../services";
import {priceFormat} from "../../../../utils";
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams} from "react-router-dom";
import useModalTreeProduct from "./useModalTreeProduct";
import {
  CHANGE_ITEM,
  REMOVE_PRODUCT_CONFIRM_NEW,
  CANCEL_ALL_TREE, ASSIGN_LIST_KEY_CLONE, CREATE_DATA_PRODUCT, ASSIGNED_DATA_EDIT,
} from "../../../store/reducers";

const {default: styled} = require("styled-components");

const StyledInputNumberNew = styled(StyledInputNumber)`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  .ant-input-number-input {
    text-align: center;
    height: 38px !important;
  }
`;

function CreateStockReceipt(props) {
  const param = useParams();
  const {getFieldDecorator} = props.form;
  const reduxForm = props.form;
  const {id, type} = param;
  const history = useHistory();
  const [isModal, setIsModal] = useState(false);
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const {treeProduct} = dataRoot;

  const [isLoading, setIsLoading] = useState(false);
  const [loadingButton, setLoadingButton] = useState(false);

  const [data, setData] = useState([]);

  const [dataRepo, setDataRepo] = useState([]);

  const _getDataDropListWareHouse = async () => {
    try {
      const data = await APIService._getReponsitoryCenterTower();
      let arrNew = data.dataRepo.map((item, index) => {
        const key = item.id_repo;
        const value = item.name_repo;
        return {key, value};
      });

      setDataRepo(arrNew);
    } catch (error) {
    }
  };

  const postCreateStockReceipt = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postCreateStockReceipt(obj);
      message.success("Tạo phiếu nhập thành công");
      dispatch(CANCEL_ALL_TREE());
      setLoadingButton(false);
      history.push("/stock/receipt/list");
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postUpdateStockReceipt = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postUpdateStockReceipt(obj);
      message.success("Chỉnh sửa phiếu nhập thành công");
      dispatch(CANCEL_ALL_TREE());
      setLoadingButton(false);
      history.push("/detail/stock/receipt/" + id);
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const postConvertQuantityProduct = async (obj) => {
    try {
      const data = await APIService._postConvertQuantityProduct(obj);

      let objUpdateShowNew = {
        quantity: data.quantity,
        attributesId: data.attribute_id,
        attribute_ratio: data.attribute_ratio,
        quantity_min: data.quantity_min,
      };

      const dataAction = {
        keyRoot: "treeProduct",
        objUpdateShow: "objUpdateShow",
        objUpdateConfirm: "objUpdateConfirm",
        idProduct: obj.product_id,
        valueUpdate: objUpdateShowNew,
      };

      dispatch(CHANGE_ITEM(dataAction));
    } catch (error) {
      console.log(error);
    }
  };

  const getDetailStockReceipt = async (id) => {
    try {
      const data = await APIService._getDetailStockReceipt(id);
      data.receipt.list_product.map((item, index) => {
        item.stt = index + 1;
        return item;
      });

      reduxForm.setFieldsValue({
        code: data.receipt.code,
        name: data.receipt.warehouse_id,
        note: !data.receipt.note ? "" : data.receipt.note,
      });

      // if (data.receipt.is_import === 1) {
      //   setData(data.receipt.list_product);
      // }
      setIsLoading(false);
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  };
  useEffect(() => {
    _getDataDropListWareHouse();
    if (type === "edit") {
      getDetailStockReceipt(id);
    }
  }, []);

  const getAPITemplateReceipt = async () => {
    try {
      const data = await APIService._getAPITemplateReceipt();
      window.open(`${data.file_url}?${new Date().getTime()}`);
      setLoadingButton(false)
    } catch (error) {
      setLoadingButton(false)
    }
  };

  let headerTable = [
    {
      name: "STT",
      align: "center",
    },
    {
      name: "Tên sản phẩm",
      align: "left",
    },
    {
      name: "Mã sản phẩm",
      align: "left",
    },
    {
      name: "Số lượng",
      align: "center",
    },
    {
      name: "Đơn vị tính",
      align: "center",
    },
    {
      name: "Số khung",
      align: "center",
    },
    {
      name: "Số seri",
      align: "center",
    },
    // {
    //   name: "Tỷ lệ quy đổi",
    //   align: "center",
    // },
    // {
    //   name: "SL quy đổi",
    //   align: "center",
    // },
    {
      name: "",
      align: "right",
    },
  ];

  const bodyTableProduct = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr className="tr-table">
        <td className="td-table" colSpan="8">
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 4,
              paddingLeft: 10,
            }}
          >
            <div
              style={{cursor: "pointer", display: "flex"}}
              onClick={() => setIsModal(true)}
            >
              <span
                style={{
                  margin: "0px 12px",
                  color: colors.main,
                  fontWeight: 500,
                }}
              >
                Thêm phiên bản
              </span>
              <div>
                <ISvg
                  name={ISvg.NAME.ADDUPLOAD}
                  width={20}
                  height={20}
                  fill={colors.main}
                />
              </div>
            </div>
          </div>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => {
        return (
          <tr className="tr-table scroll">
            <td className="td-table" style={{textAlign: "center"}}>
              {item.stt}
            </td>
            <td className="td-table" style={{textAlign: "left"}}>
              {item.name}
            </td>
            <td className="td-table" style={{textAlign: "left"}}>
              {item.code}
            </td>
            <td className="td-table" style={{textAlign: "center"}}>
              {priceFormat(item.quantity)}
            </td>
            <td className="td-table" style={{textAlign: "center"}}>
              {item.attribute}
            </td>
            <td className="td-table" style={{textAlign: "center"}}>
              {item.frame_numbers}
            </td>
            <td className="td-table" style={{textAlign: "center"}}>
              {item.seri_numbers}
            </td>
            {/*<td className="td-table" style={{ textAlign: "center" }}>*/}
            {/*  {priceFormat(item.attribute_ratio)}*/}
            {/*</td>*/}
            {/*<td className="td-table" style={{ textAlign: "center" }}>*/}
            {/*  {priceFormat(item.quantity_min)}*/}
            {/*</td>*/}
            <td className="td-table">
              <div
                style={{
                  height: 32,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-end",
                  paddingRight: 10,
                }}
              >
                <div
                  style={{
                    width: 20,
                    height: 20,
                    borderRadius: 10,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    background: "rgba(227, 95, 75, 0.1)",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    let dataTmp = [...data];
                    dataTmp.splice(index, 1);
                    dataTmp.map((itemTmp, idxTmp) => {
                      itemTmp.stt = idxTmp + 1;
                    });
                    setData(dataTmp);
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.CROSS}
                    width={8}
                    height={8}
                    fill={colors.oranges}
                  />
                </div>
              </div>
            </td>
          </tr>
        );
      })
    );
  };

  const bodyTableTreeProduct = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr className="tr-table">
        <td className="td-table" colSpan="8">
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 4,
              paddingLeft: 10,
            }}
          >
            <div
              style={{cursor: "pointer", display: "flex"}}
              onClick={() => setIsModal(true)}
            >
              <span
                style={{
                  margin: "0px 12px",
                  color: colors.main,
                  fontWeight: 500,
                }}
              >
                Thêm phiên bản
              </span>
              <div>
                <ISvg
                  name={ISvg.NAME.ADDUPLOAD}
                  width={20}
                  height={20}
                  fill={colors.main}
                />
              </div>
            </div>
          </div>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => {
        return (
          <tr className="tr-table scroll">
            <td
              className="td-table"
              style={{textAlign: "center", fontWeight: "normal"}}
            >
              {item.stt}
            </td>
            <td
              className="td-table"
              style={{textAlign: "left", fontWeight: "normal"}}
            >
              {item.name}
            </td>
            <td
              className="td-table"
              style={{textAlign: "left", fontWeight: "normal"}}
            >
              {item.code}
            </td>
            <td
              className="td-table"
              style={{
                textAlign: "center",
                maxWidth: 120,
                fontWeight: "normal",
              }}
            >
              <StyledInputNumberNew
                value={
                  !treeProduct.objUpdateConfirm[item.id] ||
                  !treeProduct.objUpdateConfirm[item.id].quantity
                    ? 1
                    : treeProduct.objUpdateConfirm[item.id].quantity
                }
                style={{width: "100%", height: 40}}
                onChange={(value) => {
                  updateGroupInput(item.id, {quantity: value});
                  let obj = {
                    product_id: item.id,
                    attribute_id:
                      !treeProduct.objUpdateConfirm[item.id] ||
                      !treeProduct.objUpdateConfirm[item.id].attributesId
                        ? item.list_attribute[0].id
                        : treeProduct.objUpdateConfirm[item.id].attributesId,
                    quantity: value,
                  };
                  postConvertQuantityProduct(obj);
                }}
                min={1}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              />
            </td>
            <td
              className="td-table"
              style={{
                textAlign: "center",
                maxWidth: 140,
                fontWeight: "normal",
              }}
            >
              <ISelect
                isBackground={false}
                value={
                  !treeProduct.objUpdateConfirm[item.id] ||
                  !treeProduct.objUpdateConfirm[item.id].attributesId
                    ? item.list_attribute[0].id
                    : treeProduct.objUpdateConfirm[item.id].attributesId
                }
                select={true}
                data={item.list_attribute}
                onChange={(key) => {
                  updateGroupInput(item.id, {attributesId: key});
                  let obj = {
                    product_id: item.id,
                    attribute_id: key,
                    quantity:
                      !treeProduct.objUpdateConfirm[item.id] ||
                      !treeProduct.objUpdateConfirm[item.id].quantity
                        ? 1
                        : treeProduct.objUpdateConfirm[item.id].quantity,
                  };
                  postConvertQuantityProduct(obj);
                }}
              />
            </td>
            <td
              className="td-table"
              style={{textAlign: "center", fontWeight: "normal"}}
            >
              <IInput
                placeholder={'Số khung'}
                value={!treeProduct.objUpdateConfirm[item.id] ||
                !treeProduct.objUpdateConfirm[item.id].frame_numbers
                  ? null
                  : treeProduct.objUpdateConfirm[item.id].frame_numbers}
                onChange={(e) => {
                  updateGroupInput(item.id, {frame_numbers: e.target.value});
                }}
              />
            </td>
            <td
              className="td-table"
              style={{textAlign: "center", fontWeight: "normal"}}
            >
              <IInput
                placeholder={'Số seri'}
                value={!treeProduct.objUpdateConfirm[item.id] ||
                !treeProduct.objUpdateConfirm[item.id].seri_numbers
                  ? null
                  : treeProduct.objUpdateConfirm[item.id].seri_numbers}
                onChange={(e) => {
                  updateGroupInput(item.id, {seri_numbers: e.target.value});
                }}
              />
            </td>
            {/*<td*/}
            {/*  className="td-table"*/}
            {/*  style={{ textAlign: "center", fontWeight: "normal" }}*/}
            {/*>*/}
            {/*  {priceFormat(*/}
            {/*    treeProduct.objUpdateConfirm[item.id].attribute_ratio*/}
            {/*  )}*/}
            {/*</td>*/}
            {/*<td*/}
            {/*  className="td-table"*/}
            {/*  style={{ textAlign: "center", fontWeight: "normal" }}*/}
            {/*>*/}
            {/*  {priceFormat(treeProduct.objUpdateConfirm[item.id].quantity_min)}*/}
            {/*</td>*/}
            <td className="td-table">
              <div
                style={{
                  height: 32,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-end",
                  paddingRight: 10,
                }}
              >
                <div
                  style={{
                    width: 20,
                    height: 20,
                    borderRadius: 10,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    background: "rgba(227, 95, 75, 0.1)",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    const dataAction = {
                      keyRoot: "treeProduct",
                      key_listTreeClone: "listTreeClone",
                      key_listTreeShow: "listTreeShow",
                      key_arrConfirm: "arrConfirm",
                      key_listConfirm: "listConfirm",
                      key_listKeyClone: "listKeyClone",
                      key_listKeyConfirm: "listKeyConfirm",
                      key_objUpdateShow: "objUpdateShow",
                      indexProduct: index,
                      product: item,
                      listTreeClone: treeProduct.listTreeClone,
                      listConfirm: treeProduct.listConfirm,
                      listTreeShow: treeProduct.listTreeShow,
                      arrConfirm: treeProduct.arrConfirm,
                      listKeyConfirm: treeProduct.listKeyConfirm,
                      listKeyClone: treeProduct.listKeyClone,
                      objUpdateShow: treeProduct.objUpdateShow,
                    };
                    console.log(dataAction)
                    dispatch(REMOVE_PRODUCT_CONFIRM_NEW(dataAction));
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.CROSS}
                    width={8}
                    height={8}
                    fill={colors.oranges}
                  />
                </div>
              </div>
            </td>
          </tr>
        );
      })
    );
  };

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="scroll" style={{background: "#E4EFFF"}}>
        {headerTable.map((item, index) => (
          <th className="th-table" style={{textAlign: item.align}}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const updateGroupInput = (id, updates) => {
    const dataAction = {
      keyRoot: "treeProduct",
      objUpdateShow: "objUpdateShow",
      objUpdateConfirm: "objUpdateConfirm",
      idProduct: id,

      valueUpdate: updates,
    };

    dispatch(CHANGE_ITEM(dataAction));
  };
  return (
    <div style={{width: "100%", overflow: "auto"}}>
      <Row>
        <Col span={24}>
          <div>
            <ITitle
              level={1}
              title={
                type === "create"
                  ? "Tạo phiếu nhập kho"
                  : "Chỉnh sửa phiếu nhập kho"
              }
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </div>
        </Col>
      </Row>
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          reduxForm.validateFields((err, obj) => {
            if (!err) {
              if (data.length === 0 && treeProduct.arrConfirm.length === 0) {
                IError("Vui lòng thêm hoặc import phiên bản!");
                return;
              }
              let dataAdd = [];
              if (data.length > 0) {
                dataAdd = data.map((item) => {
                  let id = item.id ;
                  let quantity = item.quantity;
                  let attribute_id = item.attribute_id;
                  let seri_numbers = item.seri_numbers
                  let frame_numbers = item.frame_numbers
                  return {id, quantity, attribute_id,seri_numbers,frame_numbers};
                });
              } else {
                let arrIDProduct = treeProduct.arrConfirm.map(
                  (item) => item.id
                );
                arrIDProduct.forEach((item) => {
                  if ([item] in treeProduct.objUpdateShow) {
                    dataAdd.push({
                      id: item,
                      quantity: treeProduct.objUpdateShow[item].quantity,
                      attribute_id:
                      treeProduct.objUpdateShow[item].attributesId,
                      seri_numbers: treeProduct.objUpdateShow[item].seri_numbers,
                      frame_numbers: treeProduct.objUpdateShow[item].frame_numbers,
                    });
                  }
                });
              }
              let checkError = {
                code: true,
                message: ""
              }
              for (let i = 0; i < dataAdd.length; i++) {
                let item = dataAdd[i]
                let quantity = item.quantity
                if (!item.frame_numbers || !item.seri_numbers) {
                  checkError = {
                    code: false,
                    message: "Vui lòng nhập đầy đủ seri hoặc số khung"
                  }
                  break;
                } else if (item.frame_numbers.split(",").length === 0
                  || item.frame_numbers.split(",").length !== quantity
                  || item.seri_numbers.split(",").length === 0
                  || item.seri_numbers.split(",").length !== quantity) {
                  checkError = {
                    code: false,
                    message: "Vui lòng nhập đầy đủ seri hoặc số khung theo số lượng xe"
                  }
                  break;
                } else if (item.frame_numbers.split(",").includes('')
                  || item.seri_numbers.split(",").includes("")) {
                  checkError = {
                    code: false,
                    message: "Vui lòng nhập seri hoặc số khung hợp lệ"
                  }
                  break;
                } else if (item.frame_numbers.split(",").filter((it, index) => item.frame_numbers.split(",").indexOf(it) !== index).length !== 0
                  || item.seri_numbers.split(",").filter((it, index) => item.seri_numbers.split(",").indexOf(it) !== index).length !== 0) {
                  checkError = {
                    code: false,
                    message: "Vui lòng không nhập trùng seri hoặc số khung trong mã sản phẩm"
                  }
                  break;
                }
              }
              if (!checkError.code) {
                message.error(checkError.message)
                return;
              }
              if (type === "create") {
                const objAdd = {
                  code: obj.code,
                  warehouse_id: obj.name,
                  note: obj.note,
                  is_import: treeProduct.arrConfirm.length > 0 ? 0 : 1,
                  list_product: dataAdd,
                };
                postCreateStockReceipt(objAdd);
              } else {
                const objEdit = {
                  code: obj.code,
                  warehouse_id: obj.name,
                  note: obj.note,
                  is_import: treeProduct.arrConfirm.length > 0 ? 0 : 1,
                  list_product: dataAdd,
                };
                postUpdateStockReceipt(objEdit);
              }
            }
          });
        }}
      >
        <div style={{margin: "32px 0px"}}>
          <Row>
            <Col span={24}>
              <div className="flex justify-end" style={{marginRight: 10}}>
                <IButton
                  color={colors.main}
                  title="Lưu"
                  icon={ISvg.NAME.SAVE}
                  loading={loadingButton}
                  style={{marginRight: 20}}
                  htmlType="submit"
                />
                <IButton
                  color={colors.red}
                  title="Hủy"
                  // style={{ width: 140 }}
                  icon={ISvg.NAME.CROSS}
                  onClick={() => {
                    dispatch(CANCEL_ALL_TREE());
                    if (type === "create") {
                      history.push("/stock/receipt/list");
                    } else {
                      history.push("/detail/stock/receipt/" + id);
                    }
                  }}
                />
              </div>
            </Col>
          </Row>
          <div
            style={{
              margin: "32px 0px 12px 0px",
            }}
          >
            <Row type="flex">
              <Col
                span={5}
                style={{
                  background: colors.white,
                  paddingLeft: 0,
                  paddingRight: 0,
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                }}
              >
                {isLoading ? (
                  <div style={{padding: "0px 40px", paddingTop: 30}}>
                    <Skeleton loading={true} active paragraph={{rows: 16}}/>
                  </div>
                ) : (
                  <div>
                    <Row gutter={[0, 20]}>
                      <div
                        style={{
                          width: "100%",
                          height: 735,
                          padding: "0px 40px",
                          paddingTop: 30,
                        }}
                      >
                        <Col
                          span={24}
                          style={{paddingLeft: 0, paddingRight: 0}}
                        >
                          <ITitle
                            level={2}
                            title="Thông tin phiếu nhập kho"
                            style={{
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col span={24}>
                          <div style={{marginTop: 12}}>
                            <p style={{fontWeight: 500}}>Mã phiếu nhập kho</p>
                            <IFormItem>
                              {getFieldDecorator("code", {
                                rules: [
                                  {
                                    required: type === "edit" ? false : true,
                                    message: "Vui lòng nhập mã phiếu nhập kho",
                                  },
                                ],
                              })(
                                <IInputText
                                  placeholder="Nhập mã phiếu nhập kho"
                                  value={reduxForm.getFieldValue("code")}
                                  disabled={type === "edit" ? true : false}
                                  onChange={(e) => {
                                    let codeTmp = e.target.value.replace(
                                      /[^a-zA-Z0-9]/g,
                                      ""
                                    );
                                    reduxForm.setFieldsValue({
                                      code: codeTmp,
                                    });
                                  }}
                                />
                              )}
                            </IFormItem>
                          </div>
                        </Col>
                        <Col span={24}>
                          <div>
                            <p style={{fontWeight: 500}}>Tên kho</p>
                            <IFormItem>
                              {getFieldDecorator("name", {
                                rules: [
                                  {
                                    required: type === "edit" ? false : true,
                                    message: "Vui lòng chọn kho",
                                  },
                                ],
                              })(
                                <ISelect
                                  select={true}
                                  isBackground={false}
                                  onChange={(key) => {
                                    reduxForm.setFieldsValue({
                                      name: key,
                                    });
                                  }}
                                  value={reduxForm.getFieldValue("name")}
                                  placeholder="Chọn kho"
                                  data={dataRepo}
                                />
                              )}
                            </IFormItem>
                          </div>
                        </Col>
                        <Col span={24}>
                          <div>
                            <p style={{fontWeight: 500}}>Ghi chú</p>
                            <IFormItem>
                              {getFieldDecorator("note", {
                                rules: [
                                  {
                                    required: false,
                                    message: "Vui lòng nhập ghi chú",
                                  },
                                ],
                              })(
                                <textarea
                                  style={{
                                    width: "100%",
                                    color: "#000",
                                    overflow: "auto",
                                    margin: 0,
                                    border: "1px solid #bfbfbf",
                                    padding: 10,
                                    fontsize: "1em",
                                    background: "white",
                                  }}
                                  className="textArea"
                                  placeholder="Nhập ghi chú"
                                  onChange={(e) => {
                                    reduxForm.setFieldsValue({
                                      note: e.target.value,
                                    });
                                  }}
                                />
                              )}
                            </IFormItem>
                          </div>
                        </Col>
                      </div>
                    </Row>
                  </div>
                )}
              </Col>

              <Col span={19} style={{paddingBottom: 0}}>
                <div
                  style={{
                    padding: "30px 40px",
                    background: colors.white,
                    height: "100%",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                    marginLeft: 20,
                    marginRight: 10,
                  }}
                >
                  {isLoading ? (
                    <div>
                      <Skeleton
                        loading={true}
                        active
                        paragraph={{rows: 16}}
                      />
                    </div>
                  ) : (
                    <Row gutter={[0, 10]}>
                      <Col span={24}>
                        <Row>
                          <Col span={8}>
                            <ITitle
                              level={2}
                              title="Phiên bản nhập kho"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col span={16}>
                            <div className="flex justify-end">
                              <IButton
                                color={
                                  treeProduct.arrConfirm.length > 0
                                    ? "#ccc"
                                    : colors.main
                                }
                                disabled={
                                  treeProduct.arrConfirm.length > 0
                                }
                                title="Xuất Template"
                                style={{marginRight: 20}}
                                loading={loadingButton}
                                onClick={() => {
                                  setLoadingButton(true);
                                  getAPITemplateReceipt();
                                }}
                              />
                              <IButton
                                color={
                                  treeProduct.arrConfirm.length > 0
                                    ? "#ccc"
                                    : colors.main
                                }
                                title="Import file"
                                disabled={
                                  treeProduct.arrConfirm.length > 0
                                }
                                // icon={svgs.IconDownload}
                                loading={loadingButton}
                                onClick={() => {
                                  var input =
                                    document.getElementById("my-file");
                                  input.click();
                                  input.onchange = async function (e) {
                                    try {
                                      var file = input.files[0];
                                      console.log(file)
                                      if (!file) return;
                                      setLoadingButton(true);
                                      const data =
                                        await APIService._postImportExcelProductStock(
                                          file
                                        );
                                      const dataNew = data.list_product.map(
                                        (item, index) => {
                                          item.stt = index + 1;
                                          return item;
                                        }
                                      );
                                      setData(dataNew);
                                      setLoadingButton(false);
                                      if (data === true) {
                                        setLoadingButton(false);
                                        e.target.value = null;
                                        return;
                                      }
                                      e.target.value = null;
                                    } catch (error) {
                                      e.target.value = null;
                                      setLoadingButton(false);
                                    }
                                  };
                                }}
                              />
                              <input
                                type="file"
                                id="my-file"
                                accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                style={{display: "none"}}
                              />
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <div style={{marginTop: 12}}>
                          <ITableHtml
                            childrenBody={
                              treeProduct.arrConfirm.length > 0
                                ? bodyTableTreeProduct(treeProduct.arrConfirm)
                                : bodyTableProduct(data)
                            }
                            childrenHeader={headerTableProduct(headerTable)}
                          />
                          {treeProduct.arrConfirm.length === 0 ? null : (
                            <table>
                              <tr
                                className="tr-table"
                                style={{
                                  borderLeft:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                  borderRight:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                }}
                              >
                                <td className="td-table" colSpan="8">
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                      marginBottom: 4,
                                      paddingLeft: 10,
                                    }}
                                  >
                                    <div
                                      style={{
                                        cursor: "pointer",
                                        display: "flex",
                                      }}
                                      onClick={() => setIsModal(true)}
                                    >
          <span
            style={{
              margin: "0px 12px",
              color: colors.main,
              fontWeight: 500,
            }}
          >
          Thêm sản phẩm
          </span>
                                      <div>
                                        <ISvg
                                          name={ISvg.NAME.ADDUPLOAD}
                                          width={20}
                                          height={20}
                                          fill={colors.main}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          )}
                        </div>
                      </Col>
                    </Row>
                  )}
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </Form>

      <Modal
        visible={isModal}
        // visible={true}
        footer={null}
        width={1200}
        onCancel={() => setIsModal(false)}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalTreeProduct(() => {
          setIsModal(false);
        })}
      </Modal>
    </div>
  );
}

const createStockReceipt = Form.create({name: "CreateStockReceipt"})(
  CreateStockReceipt
);

export default createStockReceipt;
