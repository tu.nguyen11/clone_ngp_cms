import React, {useState, useEffect} from "react";
import {Row, Col, Tooltip, message} from "antd";
import {
  ISearch,
  ISvg,
  ITable,
  ITitle,
  ISelect,
  IDatePicker,
  IButton,
} from "../../../components";
import {colors, images} from "../../../../assets";
import {StyledITitle} from "../../../components/common/Font/font";
import {APIService} from "../../../../services";
import {useHistory} from "react-router-dom";
import FormatterDay from "../../../../utils/FormatterDay";
import {CANCEL_ALL_TREE} from "../../../store/reducers";
import {useDispatch} from "react-redux";

const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

export default function ListStockReceipt(props) {
  const history = useHistory();
  const [filter, setFilter] = useState({
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
    warehouse_id: 0,
    staff_id: 0,
    status: 3,
    key: "",
    page: 1,
  });

  const [data, setData] = useState({
    list_receipt: [],
    size: 15,
    total: 0,
  });

  const [dataRepo, setDataRepo] = useState([]);
  const [dataStaff, setDataStaff] = useState([]);

  const [loadingTable, setLoadingTable] = useState(true);
  const dispatch = useDispatch()
  const arrStatus = [
    {
      key: 3,
      value: "Tất cả",
    },
    {
      key: 2,
      value: "Nháp",
    },
    // {
    //   key: 0,
    //   value: "Chờ xử lý",
    // },
    {
      key: 1,
      value: "Hoàn thành",
    },
    {
      key: -1,
      value: "Hủy",
    },
  ];

  const _getDataDropListWareHouse = async () => {
    try {
      const data = await APIService._getReponsitoryCenterTower();
      let arrNew = data.dataRepo.map((item, index) => {
        const key = item.id_repo;
        const value = item.name_repo;
        return {key, value};
      });

      arrNew.unshift({
        key: 0,
        value: "Tất cả kho",
      });

      setDataRepo(arrNew);
    } catch (error) {
    }
  };

  const _getDataDropListStaff = async (key = 1, page = 1) => {
    try {
      const data = await APIService._getListStaff(key, page);
      console.log(data)
      let arrNew = data.list_staff.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {key, value};
      });

      arrNew.unshift({
        key: 0,
        value: "Tất cả",
      });

      setDataStaff(arrNew);
    } catch (error) {
    }
  };

  const _getAPIList = async (filter) => {
    try {
      const data = await APIService._getListStockReceipt(filter);
      data.list_receipt.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
        });
      });
      setLoadingTable(false);

      setData(data);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  const _getAPI = async (filter) => {
    await _getDataDropListWareHouse();
    // await _getDataDropListStaff(1, 1);
    await _getAPIList(filter);
  };

  useEffect(() => {
    _getAPI(filter);
  }, [filter]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      width: 80,
      key: "stt",
      align: "center",
    },
    {
      title: "Thời gian nhập",
      dataIndex: "finish_date",
      key: "finish_date",
      render: (finish_date) => (
        <span>
          {!finish_date || finish_date <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
              finish_date,
              "#DD#/#MM#/#YYYY# #hhh#:#mm#"
            )}
        </span>
      ),
    },
    {
      title: "Mã phiếu nhập",
      dataIndex: "code",
      key: "code",
      render: (code) => <span>{!code ? "-" : code}</span>,
    },

    {
      title: "Tên kho",
      dataIndex: "warehouse_name",
      key: "warehouse_name",
      render: (warehouse_name) => (
        <Tooltip title={warehouse_name}>
          <span>{!warehouse_name ? "-" : warehouse_name}</span>
        </Tooltip>
      ),
    },
    {
      title: "Người nhập kho",
      dataIndex: "nguoi_lap_phieu",
      key: "nguoi_lap_phieu",
      render: (nguoi_lap_phieu) => <span>{!nguoi_lap_phieu ? "-" : nguoi_lap_phieu}</span>,
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
    },

    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      render: (note) => <span>{!note ? "-" : note}</span>,
    },
  ];

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{color: colors.main, fontSize: 22}}>
                Danh sách phiếu nhập kho
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo mã phiếu">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo mã phiếu"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{width: 16, height: 16}}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{margin: "26px 0px"}}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <div
                  style={{display: "flex", justifyContent: "space-between"}}
                >
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <ISvg
                      name={ISvg.NAME.EXPERIMENT}
                      with={16}
                      height={16}
                      fill={colors.icon.default}
                    />
                    <ITitle
                      title="Từ ngày"
                      level={4}
                      style={{
                        marginLeft: 10,
                        width: 60,
                      }}
                    />
                    <div>
                      <IDatePicker
                        isBackground
                        from={filter.start_date}
                        to={filter.end_date}
                        onChange={(date, dateString) => {
                          if (date.length <= 1) {
                            return;
                          }

                          setLoadingTable(true);
                          setFilter({
                            ...filter,
                            start_date: date[0]._d.setHours(0, 0, 0),
                            end_date: date[1]._d.setHours(23, 59, 59),
                            page: 1,
                          });
                        }}
                        style={{
                          background: colors.white,
                          borderWidth: 1,
                          borderColor: colors.line,
                          borderStyle: "solid",
                        }}
                      />
                    </div>

                    <div style={{width: 200, margin: "0px 15px 0px 15px"}}>
                      <ISelect
                        defaultValue="Tất cả kho"
                        data={dataRepo}
                        select={true}
                        onChange={(key) => {
                          setLoadingTable(true);
                          setFilter({
                            ...filter,
                            warehouse_id: key,
                            page: 1,
                          });
                        }}
                      />
                    </div>
                    <div style={{width: 200, marginLeft: 15}}>
                      <ISelect
                        defaultValue="Trạng thái"
                        data={arrStatus}
                        select={true}
                        style={{width: 200}}
                        onChange={(key) => {
                          setLoadingTable(true);
                          setFilter({
                            ...filter,
                            status: key,
                            page: 1,
                          });
                        }}
                      />
                    </div>
                  </div>
                  <div>
                    <IButton
                      title="Tạo mới"
                      color={colors.main}
                      icon={ISvg.NAME.ARROWUP}
                      styleHeight={{
                        width: 140,
                      }}
                      isRight={true}
                      onClick={() => {
                        dispatch(CANCEL_ALL_TREE());
                        history.push(`/stock/receipt/create/0`);
                      }}
                    />
                  </div>
                </div>
              </Col>

              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={data.list_receipt}
                    style={{width: "100%"}}
                    defaultCurrent={1}
                    sizeItem={data.size}
                    indexPage={filter.page}
                    size="small"
                    maxpage={data.total}
                    loading={loadingTable}
                    scroll={{x: data.list_receipt.length === 0 ? 0 : 1600}}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = data.list_receipt[indexRow].id;
                          const pathname = "/detail/stock/receipt/" + id;
                          history.push(pathname);
                        }, // click row
                        onMouseDown: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = data.list_receipt[indexRow].id;
                          const pathname = "/detail/stock/receipt/" + id;
                          if (event.button == 2 || event == 4) {
                            window.open(pathname, "_blank");
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({...filter, page: page});
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}
