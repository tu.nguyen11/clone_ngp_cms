import {Col, Row, Spin, Input, message} from "antd";
import React, {useEffect, useState} from "react";
import {ITitle, IButton, ISvg, ISelect} from "../../../components";
import {StyledInputNumber} from "../../../components/common";
import {colors} from "../../../../assets";

import {APIService} from "../../../../services";

import {useDispatch, useSelector} from "react-redux";
import {
  SAVE_TREE,
  ADD_TREE,
  SHOW_TREE,
  REMOVE_PRODUCT,
  CREATE_DATA_PRODUCT,
  CANCEL_TREE,
  CANCEL_TREE_NEW,
  CHANGE_ITEM,
  REMOVE_OBJ_UNCHECK,
  ADD_ARR_CONFIRMED,
  ADD_OBJ_UPDATE_CONFIRMED,
  ADD_NEW_OBJ_FROM_SERVER,
  ASSIGN_LIST_KEY_CLONE,
  ADD_TREE_KEY_CLONE,
} from "../../../store/reducers";
import ITreeNew from "../../../components/common/Tree/ITreeNew";
import {useParams} from "react-router-dom";

const {default: styled} = require("styled-components");
const {Search} = Input;
const StyledSearchCustomNew = styled(Search)`
  height: 42px;

  width: 100%;
  border: 0px !important;

  .ant-input:focus {
    box-shadow: none !important;
  }

  .ant-input-affix-wrapper .ant-input {
    padding-left: 0px !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
  .ant-input-search: not(.ant-input-search-enter-button) {
    padding-right: 6px !important;
  }
  .ant-input-suffix {
    font-size: 20px;
  }

  .ant-input {
    padding-left: 0 !important;
  }
`;
const StyledInputNumberNew = styled(StyledInputNumber)`
  .ant-input-number-input {
    text-align: center;
    height: 38px !important;
  }
`;

const ISelectNew = styled(ISelect)`
  .ant-select-selection,
  .ant-select,
  .ant-cascader-picker {
    min-width: 50px !important;
    width: 100% !important;
  }
`;

export default function useModalTreeProduct(callbackClose = () => {
}) {
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const {treeProduct, dataProduct} = dataRoot;
  const [loadingList, setLoadingList] = useState(true);
  const [listUnCheck, setListUnCheck] = useState([]);
  const [search, setSearch] = useState("");
  const _getListTreeProduct = async (search) => {
    try {
      const data = await APIService._getListTreeProductEventGift(search, 0);

      data.product_type.map((itemLv1) => {
        itemLv1.children.map((itemLv2) => {
          itemLv2.children.map((itemLv3) => {
            itemLv3.list_attribute.map((itemAttribute) => {
              let key = itemAttribute.id;
              let value = itemAttribute.name;
              return {key, value};
            });
          });
        });
      });

      let listKeyCloneNew = [];

      treeProduct.listKeyClone.forEach((item) => {
        let ids = item.split("-");
        if (ids.length === 3) {
          listKeyCloneNew.push(item);
        }
      });

      const dataAction = {
        keyRoot: "treeProduct",
        key: "listKeyClone",
        value: listKeyCloneNew,
      };

      dispatch(ASSIGN_LIST_KEY_CLONE(dataAction));

      const keyDispatch = {
        key: "dataProduct",
        value: data.product_type,
      };
      dispatch(CREATE_DATA_PRODUCT(keyDispatch));
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  const postConvertQuantityAllProduct = async (obj) => {
    try {
      let dataClone = [...obj]
      const data = await APIService._postConvertQuantityAllProduct(obj);
      console.log(dataClone)
      const objUpdateShowNew = {};
      data.list_product.forEach((item) => {
        objUpdateShowNew[item.id] = {
          quantity: item.quantity,
          attributesId: item.attribute_id,
          attribute_ratio: item.attribute_ratio,
          quantity_min: item.quantity_min,
          frame_numbers: dataClone.find(it1 => it1.product_id === item.id).frame_numbers,
          seri_numbers: dataClone.find(it1 => it1.product_id === item.id).seri_numbers
        };
      });

      const dataAction = {
        keyRoot: "treeProduct",
        key: "objUpdateShow",
        key_objConfirm: "objUpdateConfirm",
        value: objUpdateShowNew,
      };

      dispatch(ADD_NEW_OBJ_FROM_SERVER(dataAction));
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    _getListTreeProduct(search);
  }, [search]);

  const updateGroupInput = (id, updates) => {
    const dataAction = {
      keyRoot: "treeProduct",
      objUpdateShow: "objUpdateShow",
      objUpdateConfirm: "objUpdateConfirm",
      idProduct: id,

      valueUpdate: updates,
    };

    dispatch(CHANGE_ITEM(dataAction));
  };

  const treeProct = (dataProduct, idx) => {
    return (
      <ul style={{padding: 0}}>
        {dataProduct.map((item, index) => {
          if (item.children && item.children.length) {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  padding: "6px 0px",
                }}
              >
                <li
                  style={{
                    fontWeight: "bold",
                    borderBottom: "0.5px dashed #7A7B7B",
                    padding:
                      item.children[0].children === undefined
                        ? "6px 15px"
                        : "6px 6px",
                  }}
                >
                  {item.name}
                </li>
                {treeProct(item.children)}
              </ul>
            );
          } else {
            return (
              <ul
                style={{
                  listStyleType: "none",
                  borderBottom: "0.5px dashed #7A7B7B",
                  padding: 0,
                }}
              >
                <li style={{padding: "6px 0px"}}>
                  <Row gutter={[10, 0]} type="flex">
                    <Col span={9}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          paddingLeft: 30,
                        }}
                      >
                        <span>{item.name}</span>
                      </div>
                    </Col>
                    <Col span={5}>
                      <div
                        style={{
                          wordWrap: "break-word",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span>{item.code}</span>
                      </div>
                    </Col>
                    <Col span={4}>
                      <div style={{textAlign: "center", width: "100%"}}>
                        <ISelectNew
                          style={{minWidth: 50}}
                          value={
                            !treeProduct.objUpdateShow[item.id] ||
                            !treeProduct.objUpdateShow[item.id].attributesId
                              ? item.list_attribute[0].id
                              : treeProduct.objUpdateShow[item.id].attributesId
                          }
                          select={true}
                          data={item.list_attribute}
                          onChange={(key) => {
                            updateGroupInput(item.id, {attributesId: key});
                          }}
                        />
                      </div>
                    </Col>
                    <Col span={4}>
                      <div style={{textAlign: "center", width: "100%"}}>
                        <StyledInputNumberNew
                          value={
                            !treeProduct.objUpdateShow[item.id] ||
                            !treeProduct.objUpdateShow[item.id].quantity
                              ? 1
                              : treeProduct.objUpdateShow[item.id].quantity
                          }
                          style={{width: "100%", height: 40}}
                          onChange={(value) => {
                            console.log("value ", value);
                            updateGroupInput(item.id, {quantity: value});
                          }}
                          min={1}
                          formatter={(value) =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                        />
                      </div>
                    </Col>
                    <Col span={2}>
                      <div
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "flex-end",
                          paddingRight: 10,
                        }}
                      >
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            const dataAction = {
                              keyRoot: "treeProduct",
                              key_listTreeClone: "listTreeClone",
                              key_listTreeShow: "listTreeShow",
                              key_listKeyClone: "listKeyClone",
                              key_objUpdateShow: "objUpdateShow",
                              indexProduct: index,
                              product: item,
                              listTreeClone: treeProduct.listTreeClone,
                              listTreeShow: treeProduct.listTreeShow,
                              listKeyClone: treeProduct.listKeyClone,
                              objUpdateShow: treeProduct.objUpdateShow,
                            };

                            dispatch(REMOVE_PRODUCT(dataAction));
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill={colors.oranges}
                          />
                        </div>
                      </div>
                    </Col>
                  </Row>
                </li>
              </ul>
            );
          }
        })}
      </ul>
    );
  };

  return (
    <div style={{width: "100%", height: "100%"}}>
      <Row gutter={[0, 16]}>
        <Col>
          <ITitle
            level={3}
            title={"Chọn phiên bản"}
            style={{
              fontWeight: 600,
            }}
          />
        </Col>
        <Col span={24}>
          <div style={{width: "100%", height: "100%"}}>
            <Row gutter={[26, 0]}>
              <Col span={10}>
                <div style={{width: "100%", height: "100%"}}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{width: "100%"}}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <StyledSearchCustomNew
                          style={{width: 465, padding: 0}}
                          onPressEnter={(e) => {
                            setLoadingList(true);
                            setSearch(e.target.value);
                          }}
                          placeholder="Tìm kiếm theo mã, tên phiên bản"
                        />
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          height: 435,
                          overflow: "auto",
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                          paddingLeft: 18,
                          paddingRight: 18,
                        }}
                      >
                        {loadingList ? (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              height: "100%",
                            }}
                          >
                            <Spin/>
                          </div>
                        ) : (
                          <ITreeNew
                            checkable
                            checkedKeys={treeProduct.listKeyClone}
                            dataLoop={dataProduct}
                            defaultExpandAll
                            loadingList
                            // blockNode={true}
                            // checkStrictly={true}
                            onCheck={(checkedKeys, e) => {
                              const tree = [];
                              let arrUnCheck = [];
                              if (e.checked === false) {
                                const id = e.node.props.eventKey;
                                const arrId = id.split("-");
                                switch (arrId.length) {
                                  case 1: {
                                    e.node.props.dataProps.children.forEach(
                                      (item) => {
                                        item.children.forEach((item2) => {
                                          arrUnCheck.push(item2.tree_id);
                                        });
                                      }
                                    );
                                  }
                                    break;
                                  case 2: {
                                    e.node.props.dataProps.children.forEach(
                                      (item) => {
                                        arrUnCheck.push(item.tree_id);
                                      }
                                    );
                                  }
                                    break;
                                  case 3: {
                                    arrUnCheck.push(
                                      e.node.props.dataProps.tree_id
                                    );
                                  }
                                    break;
                                  default:
                                    break;
                                }
                              }
                              checkedKeys.forEach((key, _, itself) => {
                                const keys = key.split("-");
                                const [id1, id2, id3] = keys;
                                const selectedItem1 = dataProduct.find(
                                  (item) => item.id.toString() === id1
                                );

                                const selectedItem2 =
                                  selectedItem1 &&
                                  selectedItem1.children.find(
                                    (item) => item.id.toString() === id2
                                  );

                                const selectedItem3 =
                                  selectedItem2 &&
                                  selectedItem2.children.find(
                                    (item) => item.id.toString() === id3
                                  );

                                switch (keys.length) {
                                  case 1: {
                                    if (selectedItem1) {
                                      tree.push(selectedItem1);
                                    }
                                    break;
                                  }

                                  case 2: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );
                                    if (!selectedItem1InShow) {
                                      tree.unshift({
                                        ...selectedItem1,
                                        children: [selectedItem2],
                                      });
                                    } else {
                                      selectedItem1InShow.children.unshift(
                                        selectedItem2
                                      );
                                    }
                                    break;
                                  }
                                  case 3: {
                                    if (itself.find((item) => item === id1))
                                      return;
                                    if (
                                      itself.find(
                                        (item) => item === [id1, id2].join("-")
                                      )
                                    )
                                      return;
                                    let selectedItem1InShow = tree.find(
                                      (item) => item.id.toString() === id1
                                    );

                                    if (!selectedItem1InShow) {
                                      selectedItem1InShow = {
                                        ...selectedItem1,
                                        children: [
                                          {...selectedItem2, children: []},
                                        ],
                                      };
                                      tree.push(selectedItem1InShow);
                                    }

                                    const selectedItem2InShow =
                                      selectedItem1InShow.children.find(
                                        (item) => item.id.toString() === id2
                                      );

                                    if (!selectedItem2InShow) {
                                      selectedItem1InShow.children.push({
                                        ...selectedItem2,
                                        children: [selectedItem3],
                                      });
                                    } else {
                                      selectedItem2InShow.children.push(
                                        selectedItem3
                                      );
                                    }
                                  }
                                  default:
                                    break;
                                }
                              });

                              const dataActionKey = {
                                keyRoot: "treeProduct",
                                initial_listClone: "listKeyClone",
                                data:
                                  e.checked === true
                                    ? {checked: true, arr: [...checkedKeys]}
                                    : {checked: false, arr: [...arrUnCheck]},
                              };

                              const dataAction = {
                                keyRoot: "treeProduct",
                                initial_listClone: "listTreeClone",
                                data: {
                                  checked: e.checked,
                                  tree: tree,
                                },
                              };

                              dispatch(ADD_TREE(dataAction));
                              dispatch(ADD_TREE_KEY_CLONE(dataActionKey));
                            }}
                          />
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.ARROWRIGHT}
                          style={{
                            // width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            const dataAction = {
                              keyRoot: "treeProduct",
                              initial_listClone: "listTreeClone",
                              initial_listShow: "listTreeShow",
                            };

                            dispatch(SHOW_TREE(dataAction));

                            const dataAction2 = {
                              keyRoot: "treeProduct",
                              key: "objUpdateShow",
                              listUnCheck: listUnCheck,
                            };

                            dispatch(REMOVE_OBJ_UNCHECK(dataAction2));
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={14}>
                <Row>
                  <Col span={24}>
                    <Row
                      style={{
                        borderTop: "1px solid rgba(122, 123, 123, 0.5)",
                        borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                        borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                        padding: "10px 0px",
                        color: "#000",
                        fontWeight: 600,
                      }}
                    >
                      <Col span={9}>
                        <div style={{padding: "0px 5px"}}>Tên sản phẩm</div>
                      </Col>
                      <Col span={5}>
                        <div style={{padding: "0px 5px"}}>Mã sản phẩm</div>
                      </Col>
                      <Col span={4}>
                        <div
                          style={{textAlign: "center", padding: "0px 5px"}}
                        >
                          Đơn vị tính
                        </div>
                      </Col>
                      <Col span={4}>
                        <div
                          style={{textAlign: "center", padding: "0px 5px"}}
                        >
                          Số lượng
                        </div>
                      </Col>
                      <Col span={2}></Col>
                    </Row>
                  </Col>
                  <Col span={24}>
                    <div
                      style={{
                        height: 500,
                        border: "1px solid rgba(122, 123, 123, 0.5)",
                        overflowY: "auto",
                        overflowX: "hidden",
                      }}
                    >
                      {treeProduct.listTreeShow.length > 0
                        ? treeProct(treeProduct.listTreeShow)
                        : null}
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 45,
              right: -25,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              style={{
                marginRight: 15,
              }}
              onClick={() => {
                const dataAction = {
                  keyRoot: "treeProduct",
                  initial_listShow: "listTreeShow",
                  initial_listConfirm: "listConfirm",
                  initial_listKeyConfirm: "listKeyConfirm",
                  dataListKeyConfirm: treeProduct.listKeyClone,
                  assigned_condition: true,
                };
                if (treeProduct.listTreeShow.length === 0) {
                  message.error('Vui lòng thêm ít nhất 1 phiên bản');
                  return;
                }
                dispatch(SAVE_TREE(dataAction));

                let arrProductConfirm = [];
                let arrId = [];
                let arrObj = [];

                treeProduct.listTreeShow.forEach((itemLv1) => {
                  itemLv1.children.forEach((itemLv2) => {
                    itemLv2.children.forEach((itemLv3, indexLv3) => {
                      arrObj.push({
                        product_id: itemLv3.id,
                        attribute_id:
                          treeProduct.objUpdateShow[itemLv3.id] &&
                          treeProduct.objUpdateShow[itemLv3.id].attributesId
                            ? treeProduct.objUpdateShow[itemLv3.id].attributesId
                            : itemLv3.list_attribute[0].id,
                        quantity:
                          treeProduct.objUpdateShow[itemLv3.id] &&
                          treeProduct.objUpdateShow[itemLv3.id].quantity
                            ? treeProduct.objUpdateShow[itemLv3.id].quantity
                            : 1,
                        seri_numbers: treeProduct.objUpdateShow[itemLv3.id] &&
                        treeProduct.objUpdateShow[itemLv3.id].seri_numbers
                          ? treeProduct.objUpdateShow[itemLv3.id].seri_numbers
                          : null,
                        frame_numbers: treeProduct.objUpdateShow[itemLv3.id] &&
                        treeProduct.objUpdateShow[itemLv3.id].frame_numbers
                          ? treeProduct.objUpdateShow[itemLv3.id].frame_numbers
                          : null,
                      });
                      arrProductConfirm.push({
                        ...itemLv3,
                      });
                      arrId.push({
                        id: itemLv3.id,
                        attributesId: itemLv3.list_attribute[0].id,
                        quantity: 1,
                      });
                    });
                  });
                });

                const arrConfirm = arrProductConfirm.map((item, index) => {
                  item.stt = index + 1;
                  return item;
                });

                const dataAction2 = {
                  keyRoot: "treeProduct",
                  key: "arrConfirm",
                  arrConfirm: arrConfirm,
                };

                dispatch(ADD_ARR_CONFIRMED(dataAction2));

                const dataAction3 = {
                  keyRoot: "treeProduct",
                  key: "objUpdateShow",
                  key_objConfirm: "objUpdateConfirm",
                  arrId: arrId,
                };

                dispatch(ADD_OBJ_UPDATE_CONFIRMED(dataAction3));

                postConvertQuantityAllProduct(arrObj);

                callbackClose();
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              onClick={() => {
                const arrIdConfirm = treeProduct.listKeyConfirm.map((key) => {
                  let ids = key.split("-");
                  return ids[ids.length - 1];
                });
                const dataAction = {
                  keyRoot: "treeProduct",
                  initial_listClone: "listTreeClone",
                  initial_listShow: "listTreeShow",
                  dataListConfirm: treeProduct.listConfirm,
                  initial_listKeyClone: "listKeyClone",
                  dataListKeyConfirm: treeProduct.listKeyConfirm,
                  objUpdate: "objUpdateShow",
                  initial_listkeyConfirm: 'listKeyConfirm',
                  arrIdConfirm: arrIdConfirm,
                };
                dispatch(CANCEL_TREE_NEW(dataAction));
                callbackClose();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
