import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip } from "antd";
import { ITable } from "../../components";
import { colors } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { APIService } from "../../../services";

export default function ListWarehousePage(props) {
  const [filter, setFilter] = useState({
    key: "",
    page: 1,
  });

  const [dataTable, setDataTable] = useState({
    list_warehouses: [],
    total: 1,
    size: 10,
  });

  const [loadingTable, setLoadingTable] = useState(true);

  const _fetchAPIListWarehouse = async (filter) => {
    try {
      const data = await APIService._getListAllWarehouse(filter);
      data.list_warehouses.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        return item;
      });

      setDataTable({
        list_warehouses: data.list_warehouses,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIListWarehouse(filter);
  }, [filter]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },

    {
      title: "Mã kho",
      dataIndex: "code",
      key: "code",
      render: (code) => <span>{!code ? "-" : code}</span>,
    },
    {
      title: "Tên kho",
      dataIndex: "name",
      key: "name",
      render: (name) => (
          <Tooltip title={name}>
            <span>{!name ? "-" : name}</span>
          </Tooltip>
      ),
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
      render: (address) => (
        <Tooltip title={address}>
          <span>{!address ? "-" : address}</span>
        </Tooltip>
      ),
    },
    {
      title: "Người tạo",
      dataIndex: "creator",
      key: "creator",
      render: (creator) => <span>{!creator ? "-" : creator}</span>,
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Danh sách kho hàng
              </StyledITitle>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "60px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.list_warehouses}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    scroll={{ x: 0 }}
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.list_warehouses[indexRow].id;
                          props.history.push("/warehouse/detail/" + id);
                        }, // click row
                        onMouseDown: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.list_warehouses[indexRow].id;
                          if (event.button == 2 || event == 4) {
                            window.open("/warehouse/detail/" + id, "_blank");
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}
