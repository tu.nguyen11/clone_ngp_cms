import React, { useState, useEffect } from "react";
import "../../../App.css";
import { Row, Col, Skeleton, message, Form } from "antd";
import { ITitle, IButton, ISvg, ISelect } from "../../components";
import { IFormItem, IInputText } from "../../components/common";
import { colors } from "../../../assets";

import { APIService } from "../../../services";
import { useHistory, useParams } from "react-router-dom";

function CreateWarehousePage(props) {
  const history = useHistory();
  const param = useParams();
  const { id, type } = param;
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;

  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingButton, setIsLoadingButton] = useState(false);

  const [cities, setCities] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);

  const [cityId, setCityId] = useState(-1);
  const [districtId, setDistrictId] = useState(-1);
  const [wardId, setWardId] = useState(-1);

  const getListCities = async (key) => {
    try {
      const data = await APIService._getListCities(key);
      let dataNew = data.cities.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });

      setCities(dataNew);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const getListDistrict = async (city_id, key) => {
    try {
      const data = await APIService._getListCounty(city_id, key);
      let dataNew = data.district.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });

      setDistricts(dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  const getListWard = async (district_id, key) => {
    try {
      const data = await APIService._getListWard(district_id, key);
      const dataNew = data.wards.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setWards(dataNew);
    } catch (err) {
      console.log(err);
    }
  };

  const getDetailWarehouse = async (id) => {
    try {
      const data = await APIService._getDetailWarehouse(id);

      const objData = data.warehouse;
      reduxForm.setFieldsValue({
        name: objData.name,
        code: objData.code,
        address: objData.address,
        city: objData.city_id,
        district: objData.district_id,
        ward: !objData.ward_id ? undefined : objData.ward_id,
      });
      setCityId(objData.city_id);
      setDistrictId(objData.district_id);
      setWardId(objData.ward_id);
      await getListDistrict(reduxForm.getFieldValue("city"), "");
      await getListWard(reduxForm.getFieldValue("district"), "");

      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const callAPI = async () => {
    await getListCities("");
    if (type === "edit") {
      await getDetailWarehouse(id);
    }
  };

  useEffect(() => {
    callAPI();
  }, []);

  const postAddNewWarehouse = async (obj) => {
    try {
      setIsLoadingButton(true);
      const data = await APIService._postAddNewWarehouse(obj);
      setIsLoadingButton(false);
      message.success("Tạo kho mới thành công");
      history.replace("/warehouse/list");
      // await getDetailWarehouse(id);
    } catch (error) {}
  };

  const postUpdateWarehouse = async (obj) => {
    try {
      setIsLoadingButton(true);
      const data = await APIService._postUpdateWarehouse(obj);
      setIsLoadingButton(false);
      message.success("Chỉnh sửa kho thành công");
      history.replace("/warehouse/detail/" + id);
    } catch (error) {}
  };

  const handleSubmit = (e) => {
    setIsLoadingButton(true);
    e.preventDefault();
    reduxForm.validateFields((err, values) => {
      if (err) {
        setIsLoadingButton(false);
        return;
      }
      if (type === "create") {
        const objAdd = {
          name: values.name,
          ma_kho: values.code,
          address: values.address,
          city_id: values.city,
          district_id: values.district,
          ward_id: values.ward,
          status: 1,
        };
        postAddNewWarehouse(objAdd);
      } else {
        const objEdit = {
          id: id,
          name: values.name,
          address: values.address,
          city_id: values.city,
          district_id: values.district,
          ward_id: values.ward,
        };
        postUpdateWarehouse(objEdit);
      }
    });
  };

  return (
    <div style={{ width: "100%" }}>
      <Form onSubmit={handleSubmit}>
        <Row gutter={[12, 16]}>
          <Col span={24}>
            <ITitle
              level={1}
              title={param.type === "edit" ? "Chỉnh sửa kho" : "Tạo mới kho"}
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </Col>
          <Col span={24}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              <IButton
                icon={ISvg.NAME.SAVE}
                loading={isLoadingButton}
                title="Lưu"
                htmlType="submit"
                color={colors.main}
              />
              <IButton
                icon={ISvg.NAME.CROSS}
                style={{ marginLeft: 15 }}
                title="Hủy"
                color={colors.red}
                onClick={() => {
                  history.push("/warehouse/list");
                }}
              />
            </div>
          </Col>
          <Col span={24} xl={16} xxl={14}>
            <div
              style={{
                height: "100%",
              }}
            >
              <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
                <Col span={16}>
                  <div
                    style={{
                      padding: "30px 40px",
                      background: colors.white,
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      height: 660,
                    }}
                  >
                    {isLoading ? (
                      <div>
                        <Skeleton
                          loading={true}
                          active
                          paragraph={{ rows: 16 }}
                        />
                      </div>
                    ) : (
                      <>
                        <Row gutter={[12, 20]}>
                          <Col>
                            <ITitle
                              level={2}
                              title="Thông tin kho"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col span={24}>
                            <IFormItem>
                              {getFieldDecorator("name", {
                                rules: [
                                  {
                                    required: true,
                                    message: "Vui lòng nhập tên kho",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <ITitle
                                      level={4}
                                      title={"Tên kho"}
                                      style={{
                                        fontWeight: 600,
                                      }}
                                    />
                                    <IInputText
                                      style={{ color: "#000" }}
                                      placeholder="Nhập tên kho"
                                      // onChange={(key) => {
                                      //   reduxForm.setFieldsValue({
                                      //     title: key,
                                      //   });
                                      // }}
                                      value={reduxForm.getFieldValue("name")}
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          {type === "edit" ? null : (
                            <Col span={12}>
                              <IFormItem>
                                {getFieldDecorator("code", {
                                  rules: [
                                    {
                                      required: true,
                                      message: "Vui lòng nhập mã kho",
                                    },
                                  ],
                                })(
                                  <div>
                                    <Row>
                                      <ITitle
                                        level={4}
                                        title={"Mã kho"}
                                        style={{
                                          fontWeight: 600,
                                        }}
                                      />
                                      <IInputText
                                        style={{ color: "#000" }}
                                        placeholder="Nhập mã kho"
                                        // onChange={(key) => {
                                        //   reduxForm.setFieldsValue({
                                        //     title: key,
                                        //   });
                                        // }}
                                        value={reduxForm.getFieldValue("code")}
                                      />
                                    </Row>
                                  </div>
                                )}
                              </IFormItem>
                            </Col>
                          )}
                          <Col span={24}>
                            <Row gutter={[12, 12]}>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("address", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "Vui lòng nhập địa chỉ",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Địa chỉ"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <IInputText
                                          style={{ color: "#000" }}
                                          placeholder="Nhập địa chỉ"
                                          // onChange={(key) => {
                                          //   reduxForm.setFieldsValue({
                                          //     title: key,
                                          //   });
                                          // }}
                                          value={reduxForm.getFieldValue(
                                            "address"
                                          )}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("city", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "Vui lòng chọn tỉnh/thành phố",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Tỉnh/Thành phố"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <ISelect
                                          data={cities}
                                          select={true}
                                          isBackground={false}
                                          style={{ marginTop: 1 }}
                                          placeholder="Chọn tỉnh/thành phố"
                                          value={reduxForm.getFieldValue(
                                            "city"
                                          )}
                                          onChange={(key) => {
                                            setCityId(key);
                                            setDistrictId(-1);
                                            setWardId(-1);
                                            reduxForm.setFieldsValue({
                                              city: key,
                                            });
                                            reduxForm.setFieldsValue({
                                              district: undefined,
                                            });
                                            reduxForm.setFieldsValue({
                                              ward: undefined,
                                            });
                                            getListDistrict(
                                              reduxForm.getFieldValue("city"),
                                              ""
                                            );
                                          }}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={24}>
                            <Row gutter={[12, 12]}>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("district", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "Vui lòng chọn quận/huyện",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Quận/Huyện"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <ISelect
                                          data={districts}
                                          isBackground={false}
                                          style={{ marginTop: 1 }}
                                          placeholder="Chọn quận/huyện"
                                          onChange={(key) => {
                                            setDistrictId(key);
                                            setWardId(-1);
                                            reduxForm.setFieldsValue({
                                              district: key,
                                            });
                                            reduxForm.setFieldsValue({
                                              ward: undefined,
                                            });
                                            getListWard(
                                              reduxForm.getFieldValue(
                                                "district"
                                              ),
                                              ""
                                            );
                                          }}
                                          value={reduxForm.getFieldValue(
                                            "district"
                                          )}
                                          select={cityId === -1 ? false : true}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("ward", {
                                    rules: [
                                      {
                                        required:
                                          wards.length === 0 ? false : true,
                                        message: "Vui lòng chọn phường/xã",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Phường/Xã"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <ISelect
                                          data={wards}
                                          isBackground={false}
                                          style={{ marginTop: 1 }}
                                          placeholder="Chọn phường/xã"
                                          onChange={(key) => {
                                            reduxForm.setFieldsValue({
                                              ward: key,
                                            });
                                          }}
                                          value={reduxForm.getFieldValue(
                                            "ward"
                                          )}
                                          select={
                                            districtId === -1 ? false : true
                                          }
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </>
                    )}
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const createWarehousePage = Form.create({ name: "CreateWarehousePage" })(
  CreateWarehousePage
);

export default createWarehousePage;
