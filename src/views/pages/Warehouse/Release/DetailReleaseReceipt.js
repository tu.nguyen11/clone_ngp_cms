import React, {useState, useEffect} from "react";
import {Row, Col, Skeleton, Empty, message} from "antd";
import {ITitle, ITableHtml, ISelect, IButton} from "../../../components";
import {colors} from "../../../../assets";
import {useParams} from "react-router-dom";
import FormatterDay from "../../../../utils/FormatterDay";
import {priceFormat} from "../../../../utils";
import {APIService} from "../../../../services";
import {StyledITitle} from "../../../components/common/Font/font";
import styled from 'styled-components'

const ITableHtmlStyle = styled(ITableHtml)`
  table{
  border-top: 1px solid rgba(122, 123, 123, 0.5);
  }
  .td-table,.th-table{
  }
`
export default function DetailReleaseReceipt() {
  const param = useParams();
  const {id} = param;
  const [isLoading, setIsLoading] = useState(true);
  const [showButton, setShowbutton] = useState(false)
  const [dataDetail, setDataDetail] = useState({});
  const [dataProduct, setDataProduct] = useState({})
  const [loadingButton, setLoadingButton] = useState(false)
  const [dataRepo, setDataRepo] = useState([]);
  const [warehouseId, setWarehouseId] = useState(1)
  const [dataVer, setDataVer] = useState([])
  const [listFrames, setListFrames] = useState([])
  const [objSeri, setObjSeri] = useState({
    product_version_id: null,
    frame: null,
    seri: null
  })

  const getDetailReleasesStockReceipt = async (id) => {
    try {
      const data = await APIService._getDetailOutPut(id);
      setDataProduct(data.product.product)
      setDataDetail(data);
      setDataVer(data.product.product.listVersion)
      if (data.status === 0) {
        await _getDataDropListWareHouse()
        setShowbutton(true)
      } else setShowbutton(false)
      setIsLoading(false);
      setObjSeri({
        ...objSeri,
        product_version_id: data.product.product.listVersion[0].id
      })
      await fetchListFrame({product_version_id: data.product.product.listVersion[0].id, frame: ""})
    } catch (err) {
      setIsLoading(false)
      console.log(err);
    }
  };
  const _getDataDropListWareHouse = async () => {
    try {
      const data = await APIService._getReponsitoryCenterTower();
      let arrNew = data.dataRepo.map((item, index) => {
        const key = item.id_repo;
        const value = item.name_repo;
        return {key, value};
      });
      setWarehouseId(arrNew[0].key)
      setDataRepo(arrNew);
    } catch (error) {
    }
  };
  useEffect(() => {
    getDetailReleasesStockReceipt(id);
  }, []);
  const postOutPutWarehouse = async () => {
    try {
      let obj = {
        warehouse_id: warehouseId,
        id,
        frame_number: objSeri.frame,
        seri_number: objSeri.seri
      }
      const response = await APIService._postOutPutWarehouse(obj)
      setLoadingButton(false)
      message.success('Xuất kho thành công!')
      await getDetailReleasesStockReceipt(id);
    } catch (e) {
      setLoadingButton(false)
      console.log(e)
    }
  }
  let headerTable = [
    {
      name: "Mã sản phẩm",
      align: "left",
    },
    {
      name: "Tên sản phẩm",
      align: "left",
    },
    {
      name: "Ngành hàng",
      align: "left",
    },
    {
      name: "Dòng xe",
      align: "left",
    },
    {
      name: "Loại xe",
      align: "left",
    },
    {
      name: "Thương hiệu",
      align: "left",
    },
  ];
  let headerVersion = [
    {
      name: "Mã phiên bản",
      align: "left",
    },
    {
      name: "Năm sản xuất",
      align: "left",
    },
    {
      name: "Màu sắc",
      align: "left",
    },
    {
      name: "Loại thùng",
      align: "left",
    },
    {
      name: "Giá công bố",
      align: "left",
    },
    {
      name: "Giá thùng phíếu",
      align: "left",
    },
    {
      name: "DKDK",
      align: "left",
    },
  ];
  const bodyTableProduct = () => {
    return (
      <tr className="tr-table" style={{maxHeight: 500, overflow: "auto"}}>
        <td
          className="td-table"
          style={{
            textAlign: "left",
            overflowWrap: "break-word",
            maxWidth: 250,
          }}
        >
              <span>
                {!dataProduct
                  ? "-"
                  : dataProduct.productCode}</span>
        </td>
        <td
          className="td-table"
          style={{
            textAlign: "left",
            overflowWrap: "break-all",
            maxWidth: 150,
          }}
        >
              <span>{!dataProduct
                ? "-"
                : dataProduct.name}</span>
        </td>
        <td className="td-table" style={{textAlign: "left"}}>
              <span>
                {!dataProduct
                  ? "-"
                  : dataProduct.typeParentName}
              </span>
        </td>
        <td className="td-table" style={{textAlign: "left"}}>
              <span> {!dataProduct
                ? "-"
                : dataProduct.typeName}</span>
        </td>
        <td className="td-table" style={{textAlign: "left"}}>
              <span> {!dataProduct
                ? "-"
                : dataProduct.categoryName}</span>
        </td>
        <td className="td-table" style={{textAlign: "left"}}>
              <span> {!dataProduct
                ? "-"
                : dataProduct.brandName}</span>
        </td>
      </tr>
    );
  };

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="scroll" style={{background: "#E4EFFF"}}>
        {headerTable.map((item, index) => (
          <th className="th-table" style={{textAlign: item.align, width: item.width}}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };
  const headerTableVersion = (headerTable) => {
    return (
      <tr className="scroll" style={{background: "#E4EFFF"}}>
        {headerTable.map((item, index) => (
          <th className="th-table" style={{textAlign: item.align}}>
            {item.name}
          </th>
        ))}
      </tr>
    );
  };
  const bodyTableVersion = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td colspan="7" style={{padding: 20}}>
          <Empty description="Không có dữ liệu"/>
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => {
        return (
          <tr className="tr-table" style={{maxHeight: 500, overflow: "auto"}}>
            <td className="td-table" style={{textAlign: "left", width: 150}}>
              <span>{item.versionCode}</span>
            </td>
            <td
              className="td-table"
              style={{
                textAlign: "left",
                overflowWrap: "break-word",
                maxWidth: 250,
              }}
            >
              <span>{item.year_of_manufacture}</span>
            </td>
            <td
              className="td-table"
              style={{
                textAlign: "left",
                overflowWrap: "break-all",
                maxWidth: 150,
              }}
            >
              <span>{item.colorName}</span>
            </td>
            <td className="td-table" style={{textAlign: "left"}}>
              <span>{priceFormat(item.characteristics)}</span>
            </td>
            <td className="td-table" style={{textAlign: "left"}}>
              <span>{priceFormat(item.price)}</span>
            </td>
            <td className="td-table" style={{textAlign: "left"}}>
              <span>{priceFormat(item.price_package)}</span>
            </td>
            <td className="td-table" style={{textAlign: "left"}}>
              <span>{priceFormat(item.price_register)}</span>
            </td>
          </tr>
        );
      })
    );
  };
  let timeout;
  let currentValue;

  /**
   * Fetch debouce data
   * @param value
   * @param callback
   */
  function fetch(key, callback) {
    if (timeout) {
      clearTimeout(timeout);
      timeout = null;
    }
    currentValue = key;

    const fetchData = async () => {
      try {
        const obj = {
          ...objSeri,
          frame: key
        }
        const res = await APIService._getListFramesbyProduct(obj)
        let render = res.list.map((item, index) => {
          return {
            key: item.frame_number,
            value: item.frame_number
          }
        })
        callback(render)
      } catch (e) {
        console.log(e)
      }
    }


    timeout = setTimeout(fetchData, 900);
  }

  const handleSearch = async (key) => {
    try {
      await fetch(key, data => setListFrames(data))
    } catch (e) {
      console.log(e)
    }
  }
  const fetchListFrame = async obj => {
    try {
      const res = await APIService._getListFramesbyProduct(obj)
      let render = res.list.map((item, index) => {
        return {
          key: item.frame_number,
          value: item.frame_number
        }
      })
      setListFrames(render)
      let objNew = {
        frame: res.list[0].frame_number,
        product_version_id: obj.product_version_id
      }
      setObjSeri({
        ...objSeri,
        frame: res.list[0].frame_number,
        product_version_id: obj.product_version_id
      })
      await fetchListSeri(objNew, true)
    } catch (e) {
      console.log(e)
    }
  }
  /**
   *
   * @param obj
   * @param first [check if it render the first time]
   * @returns {Promise<void>}
   */
  const fetchListSeri = async (obj, first) => {
    try {
      const res = await APIService._getListSeriByFrames(obj)
      if (first) {
        setObjSeri({
          ...obj,
          seri: res.serri
        })
      } else setObjSeri({
        ...objSeri,
        frame: obj.frame,
        seri: res.serri
      })
    } catch (e) {
      console.log(e)
    }
  }
  return (
    <div style={{width: "100%", overflow: "auto"}}>
      <div style={{margin: "0px 0px 32px 0px"}}>
        <div
          style={{
            margin: "18px 0px 12px 0px",
          }}
        >
          <Row>
            <Col span={24}>
              <div style={{marginBottom: 30}}>
                <StyledITitle style={{color: colors.main, marginTop: 8}}>
                  Chi tiết phiếu xuất kho
                </StyledITitle>
              </div>
            </Col>
            {showButton && (
              <Col span={24}>
                <div style={{marginBottom: 30, display: 'flex', justifyContent: 'space-between'}}>
                  <div style={{display: 'flex', alignItems: 'center'}}>
                    <label style={{width: 100}}>Kho xuất</label>
                    <ISelect
                      defaultValue={dataRepo[0].value}
                      data={dataRepo}
                      select={true}
                      style={{width: 300}}
                      onChange={(key) => {
                        setWarehouseId(key)
                      }}
                    />
                  </div>
                  <IButton
                    loading={loadingButton}
                    title={'Xuất kho'}
                    onClick={() => {
                      setLoadingButton(true)
                      postOutPutWarehouse()
                    }}
                    color={colors.main}
                  />
                </div>
              </Col>
            )}
          </Row>
          <Row style={{display: 'flex'}}>
            <Col
              span={5}
              style={{
                background: colors.white,
                paddingLeft: 0,
                paddingRight: 0,
                boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
              }}
            >
              <Row gutter={[0, 30]}>
                <div
                  style={{
                    width: "100%",
                    height: 700,
                    padding: "44px 40px",
                  }}
                >
                  <Skeleton loading={isLoading} active paragraph={{rows: 16}}>
                    <Col
                      span={24}
                      style={{paddingLeft: 0, paddingRight: 0, paddingTop: 0}}
                    >
                      <ITitle
                        level={2}
                        title="Thông tin phiếu xuất kho"
                        style={{
                          fontWeight: "bold",
                        }}
                      />
                    </Col>
                    <Col span={24}>
                      <div>
                        <p className={'mb-2'} style={{fontWeight: 500}}>Số khung {dataDetail.status !== 1 &&
                        <span style={{
                          color: "red"
                        }}>*</span>}</p>
                        {dataDetail.status === 1 ? <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.frame_number ? "-" : dataDetail.frame_number}
                            </span> : (
                          <ISelect
                            showSearch
                            defaultActiveFirstOption={true}
                            showArrow={false}
                            filterOption={false}
                            onSearch={handleSearch}
                            notFoundContent={"Không tìm thấy kết quả"}
                            data={listFrames}
                            select={true}
                            value={objSeri.frame}
                            style={{width: 300}}
                            onChange={async (key) => {
                              console.log(key)
                              let obj = {
                                ...objSeri,
                                frame: key
                              }
                              setObjSeri(obj)
                              await fetchListSeri(obj)
                            }}
                          />
                        )}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div>
                        <p className={'mb-2'} style={{fontWeight: 500}}>Số seri {dataDetail.status !== 1 &&
                        <span style={{
                          color: "red"
                        }}>*</span>}</p>
                        {dataDetail.status === 1 ? <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.seri_number ? "-" : dataDetail.seri_number}
                            </span> : (<span style={{wordWrap: "break-word"}}>
                          {!objSeri.seri
                            ? "-"
                            : objSeri.seri}
                            </span>)}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div>
                        <p style={{fontWeight: 500}}>Mã hợp đồng</p>
                        <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.order_child_s1_code ? "-" : dataDetail.order_child_s1_code}
                            </span>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div>
                        <p style={{fontWeight: 500}}>Showroom</p>
                        <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.showroom_name
                            ? "-"
                            : dataDetail.showroom_name}
                            </span>
                      </div>
                    </Col>
                    {dataDetail.status == 1 && (
                      <Col span={24}>
                        <div>
                          <p style={{fontWeight: 500}}>Tên kho</p>
                          <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.warehouse_delivery_name
                            ? "-"
                            : dataDetail.warehouse_delivery_name}
                            </span>
                        </div>
                      </Col>
                    )}
                    <Col span={24}>
                      <div>
                        <p style={{fontWeight: 500}}>Khách hàng</p>
                        <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.name_customer ? "-" : dataDetail.name_customer}
                            </span>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div>
                        <p style={{fontWeight: 500}}>Địa chỉ</p>
                        <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.address
                            ? "-"
                            : dataDetail.address.replace((/^,/g), '')}
                            </span>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div>
                        <p style={{fontWeight: 500}}>Trạng thái</p>
                        <span>
                          {dataDetail.status == 1 ? 'Hoàn thành' : 'Chờ xuất'}
                            </span>
                      </div>
                    </Col>
                    {dataDetail.status == 1 && (
                      <Col span={24}>
                        <div>
                          <p style={{fontWeight: 500}}>Thời gian xuất kho</p>
                          <span style={{wordWrap: "break-word"}}>
                          {!dataDetail.time_output
                            ? "-"
                            : FormatterDay.dateFormatWithString(
                              dataDetail.time_output,
                              "#YYYY#-#MM#-#DD# #hhhh#:#mm#:#ss#"
                            )}
                            </span>
                        </div>
                      </Col>
                    )}
                  </Skeleton>
                </div>
              </Row>
            </Col>

            <Col span={19} style={{paddingBottom: 0}}>
              <div
                style={{
                  padding: 30,
                  background: colors.white,
                  height: "100%",
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  marginLeft: 20,
                }}
              >
                <Skeleton loading={isLoading} active paragraph={{rows: 16}}>
                  <Row gutter={[0, 10]}>
                    <Col span={24}>
                      <ITitle
                        level={2}
                        title="Sản phẩm đặt mua"
                        style={{
                          fontWeight: "bold",
                        }}
                      />
                    </Col>
                    <Col span={24}>
                      <div style={{marginTop: 12}}>
                        <ITableHtmlStyle
                          childrenBody={bodyTableProduct()}
                          childrenHeader={headerTableProduct(headerTable)}
                        />
                      </div>
                    </Col>
                    <Col className={'mt-5'} style={{marginTop: 30}} span={24}>
                      <ITitle
                        level={2}
                        title="Phiên bản sản phẩm"
                        style={{
                          fontWeight: "bold",
                        }}
                      />
                    </Col>
                    <Col span={24}>
                      <div style={{marginTop: 12}}>
                        <ITableHtmlStyle
                          childrenBody={bodyTableVersion(dataVer)}
                          childrenHeader={headerTableVersion(headerVersion)}
                        />
                      </div>
                    </Col>
                  </Row>
                </Skeleton>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}
