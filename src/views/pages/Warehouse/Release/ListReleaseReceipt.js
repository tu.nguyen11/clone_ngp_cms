import React, {useState, useEffect} from "react";
import {Row, Col, Tooltip, message} from "antd";
import {
  ISearch,
  ISvg,
  ITable,
  ITitle,
  ISelect,
  IDatePicker,
} from "../../../components";
import {colors, images} from "../../../../assets";
import {StyledITitle} from "../../../components/common/Font/font";
import {APIService} from "../../../../services";
import {useHistory} from "react-router-dom";
import FormatterDay from "../../../../utils/FormatterDay";

const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

export default function ListReleaseReceipt(props) {
  const history = useHistory();
  const [filter, setFilter] = useState({
    start: startValue,
    end: NewDate.setHours(23, 59, 59, 999),
    warehouse_id: 0,
    showroom_id: 0,
    saleman_id: 0,
    key: "",
    page: 1,
  });

  const [data, setData] = useState({
    list: [],
    size: 10,
    total: 0,
  });

  const [dataRepo, setDataRepo] = useState([]);
  const [dataStaff, setDataStaff] = useState([]);
  const [dataShowroom, setDataShowroom] = useState([])
  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingSubCate, setLodingSubCate] = useState(false)
  const _getDataDropListWareHouse = async () => {
    try {
      const data = await APIService._getReponsitoryCenterTower();
      let arrNew = data.dataRepo.map((item, index) => {
        const key = item.id_repo;
        const value = item.name_repo;
        return {key, value};
      });

      arrNew.unshift({
        key: 0,
        value: "Tất cả",
      });

      setDataRepo(arrNew);
    } catch (error) {
    }
  };

  const _getDataDropListStaff = async (idShowroom) => {
    try {
      const data = await APIService._getListStaffByShowRoom(idShowroom);
      let arrNew = data.data.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {key, value};
      });

      arrNew.unshift({
        key: 0,
        value: "Tất cả",
      });
      setLodingSubCate(false)
      setDataStaff(arrNew);
    } catch (error) {
      setLodingSubCate(false)
      console.log(error)
    }
  };

  const _getAPIList = async (filter) => {
    try {
      const data = await APIService._getListOutPutWarehouse(filter);
      data.list.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          status_id: item.status_id,
          id: item.id,
        });
      });
      setLoadingTable(false);

      setData(data);
    } catch (error) {
      setLoadingTable(false);
    }
  };
  const getListShowroom = async () => {
    try {
      const response = await APIService._getListShowroom()
      let arrNew = response.data.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {key, value};
      });
      arrNew.unshift({
        key: 0,
        value: "Tất cả",
      });
      setDataShowroom(arrNew)
    } catch (e) {
      console.log(e)
    }
  }
  const _getAPI = async (filter) => {
    await _getAPIList(filter);
  };

  useEffect(() => {
    _getAPI(filter);
  }, [filter]);

  useEffect(() => {
    _getDataDropListStaff(filter.showroom_id);
  }, [filter.showroom_id]);

  useEffect(() => {
    _getDataDropListWareHouse();
    getListShowroom()
  }, [])
  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      width: 80,
      align: "center",
      key: "stt",
    },
    {
      title: "Thời gian xuất",
      dataIndex: "time",
      key: "time",
      render: (time) => (
        <span>
          {!time || time <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
              time,
              "#DD#-#MM#-#YYYY# #hhh#:#mm#"
            )}
        </span>
      ),
    },
    {
      title: "Mã hợp đồng",
      dataIndex: "order_child_s1_code",
      key: "order_child_s1_code",
      render: (order_child_s1_code) => <span>{!order_child_s1_code ? "-" : order_child_s1_code}</span>,
    },

    {
      title: "Tên kho",
      dataIndex: "name_warehouse",
      key: "name_warehouse",
      render: (name_warehouse) => (
        <span>{!name_warehouse ? "-" : name_warehouse}</span>
      ),
    },
    {
      title: "Tên showroom",
      dataIndex: "name_showroom",
      key: "name_showroom",
      render: (name_showroom) => <span>{!name_showroom ? "-" : name_showroom}</span>,
    },
    {
      title: "Tên khách hàng",
      dataIndex: "shop_name",
      key: "shop_name",
      render: (shop_name) => <span>{!shop_name ? "-" : shop_name}</span>,
    },
    {
      title: "Nhân viên bán hàng",
      dataIndex: "sale_name",
      key: "sale_name",
      render: (sale_name) => <span>{!sale_name ? "-" : sale_name}</span>,
    },
    {
      title: "Trạng thái",
      dataIndex: "name_status",
      key: "name_status",
      render: (name_status) => <span>{!name_status ? "-" : name_status}</span>,
    },
  ];

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{color: colors.main, fontSize: 22}}>
                Danh sách phiếu xuất kho
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo mã hợp đồng">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo mã hợp đồng"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{width: 16, height: 16}}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{margin: "26px 0px"}}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{
                      marginLeft: 15,
                      marginRight: 15,
                      width: 60,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filter.start}
                      to={filter.end}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          start: date[0]._d.setHours(0, 0, 0),
                          end: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>

                  <div style={{width: 200, margin: "0px 15px 0px 15px"}}>
                    <ISelect
                      defaultValue="Kho"
                      data={dataRepo}
                      select={true}
                      onChange={(key) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          warehouse_id: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{width: 200, marginRight: 15}}>
                    <ISelect
                      defaultValue="Showroom"
                      data={dataShowroom}
                      select={true}
                      style={{width: 200}}
                      onChange={(key) => {
                        setLodingSubCate(true)
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          showroom_id: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                  <div style={{width: 200}}>
                    <ISelect
                      defaultValue="NV bán hàng"
                      data={dataStaff}
                      select={true}
                      loading={loadingSubCate}
                      style={{width: 200}}
                      onChange={(key) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          saleman_id: key,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>

              <Col span={24}>
                <Row>
                  <ITable
                    rowSelection={[]}
                    columns={columns}
                    data={data.list}
                    style={{width: "100%"}}
                    defaultCurrent={1}
                    sizeItem={data.size}
                    indexPage={filter.page}
                    size="small"
                    maxpage={data.total}
                    loading={loadingTable}
                    scroll={{
                      x: data.list.length === 0 ? 0 : 1600,
                    }}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = data.list[indexRow].id;
                          const pathname = "/release/receipt/detail/" + id;
                          history.push(pathname);
                        }, // click row
                        onMouseDown: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = data.list[indexRow].id;
                          const pathname = "/release/receipt/detail/" + id;
                          if (event.button == 2 || event == 4) {
                            window.open(pathname, "_blank");
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({...filter, page: page});
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}
