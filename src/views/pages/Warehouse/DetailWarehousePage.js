import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton } from "antd";
import { ISvg, IButton, ITitle } from "../../components";
import { colors } from "../../../assets";
import { StyledITitle } from "../../components/common/Font/font";
import { APIService } from "../../../services";
import { useHistory, useParams } from "react-router-dom";

export default function DetailWarehousePage() {
  const param = useParams();
  const { id, type } = param;
  const history = useHistory();

  const [isLoading, setIsLoading] = useState(true);

  const [dataDetail, setDataDetail] = useState();

  const getDetailWarehouse = async (id) => {
    try {
      const data = await APIService._getDetailWarehouse(id);
      setDataDetail(data.warehouse);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getDetailWarehouse(id);
  }, []);

  return (
    <div style={{ width: "100%", overflow: "auto" }}>
      <Row>
        <Col flex="auto">
          <div>
            <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
              Chi tiết kho
            </StyledITitle>
          </div>
        </Col>
      </Row>

      <div style={{ margin: "32px 0px" }}>
        <Row>
          <Col flex="auto">
            <div className="flex justify-end">
              <IButton
                color={colors.main}
                title= {"Chỉnh sửa kho"}
                style={{ width: 160, marginRight: 10 }}
                icon={ISvg.NAME.WRITE}
                onClick={() => {
                  history.push("/warehouse/edit/" + param.id);
                }}
              />
            </div>
          </Col>
        </Row>
        <div
          style={{
            margin: "32px 0px 12px 0px",
          }}
        >
          <Row>
            <Col
              span={12}
              xxl={9}
              style={{
                background: colors.white,
                paddingLeft: 0,
                paddingRight: 0,
                boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
              }}
            >
              {isLoading ? (
                <div style={{ padding: "0px 40px", paddingTop: 30 }}>
                  <Skeleton loading={true} active paragraph={{ rows: 16 }} />
                </div>
              ) : (
                <div>
                  <Row gutter={[0, 30]}>
                    <div
                      style={{
                        width: "100%",
                        height: 650,
                        padding: "0px 40px",
                        paddingTop: 30,
                      }}
                    >
                      <Col
                        span={24}
                        style={{ paddingLeft: 0, paddingRight: 0 }}
                      >
                        <ITitle
                          level={2}
                          title="Thông tin kho"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                      </Col>
                      <Col span={24}>
                        <div>
                          <p style={{ fontWeight: 500 }}>Tên kho</p>
                          <span>
                            {!dataDetail.name ? "-" : dataDetail.name}
                          </span>
                        </div>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[18, 0]} style={{ marginTop: 12 }}>
                          <Col span={12}>
                            <div>
                              <p style={{ fontWeight: 500 }}>Mã kho</p>
                              <span>
                                {!dataDetail.code ? "-" : dataDetail.code}
                              </span>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div>
                              <p style={{ fontWeight: 500 }}>Người tạo</p>
                              <span>
                                {!dataDetail.creator_name ? "-" : dataDetail.creator_name}
                              </span>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[18, 0]}>
                          <Col span={12}>
                            <div style={{ marginTop: 12 }}>
                              <p style={{ fontWeight: 500 }}>Địa chỉ</p>
                              <span>
                                {!dataDetail.address ? "-" : dataDetail.address}
                              </span>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div style={{ marginTop: 12 }}>
                              <p style={{ fontWeight: 500 }}>Tỉnh/Thành phố</p>
                              <span>
                                {!dataDetail.city_name
                                  ? "-"
                                  : dataDetail.city_name}
                              </span>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[18, 0]}>
                          <Col span={12}>
                            <div style={{ marginTop: 12 }}>
                              <p style={{ fontWeight: 500 }}>Quận/Huyện</p>
                              <span>
                                {!dataDetail.district_name
                                  ? "-"
                                  : dataDetail.district_name}
                              </span>
                            </div>
                          </Col>
                          <Col span={12}>
                            <div style={{ marginTop: 12 }}>
                              <p style={{ fontWeight: 500 }}>Phường/Xã</p>
                              <span>
                                {!dataDetail.ward_name
                                  ? "-"
                                  : dataDetail.ward_name}
                              </span>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                    </div>
                  </Row>
                </div>
              )}
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}
