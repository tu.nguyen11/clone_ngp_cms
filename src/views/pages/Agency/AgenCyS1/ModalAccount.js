import { Col, message, Row, Skeleton, Empty } from "antd";
import React, { useEffect, useState } from "react";
import { colors } from "../../../../assets";
import { ITitle, IButton, ISvg, ITableHtml, IInput } from "../../../components";
import { APIService } from "../../../../services";

function ModalAccount(popupAccount, user_agency_id, acc_default) {
  const [data, setData] = useState([]);
  const [accountEdit, setAccountEdit] = useState({});

  const [loading, setLoading] = useState(true);

  const getListAccountAgencyC1 = async (user_agency_id) => {
    try {
      const data = await APIService._getListAccountAgencyC1(user_agency_id);
      const dataNew = data.data.map((item, index) => {
        item.isShow = false;
        item.isEdit = false;
        item.password = item.password_show;
        item.status_name =
          item.status === 1 ? "Đang hoạt động" : "Ngưng hoạt động";
        return item;
      });

      setData([...dataNew]);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const postCreateExtraAccount = async (obj) => {
    try {
      setLoading(true);
      const data = await APIService._postCreateExtraAccount(obj);
      message.success("Tạo tài khoản thành công");
      await getListAccountAgencyC1(user_agency_id);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const postUpdateInfoAndStatusExtraAccount = async (obj) => {
    try {
      setLoading(true);
      const data = await APIService._postUpdateInfoAndStatusExtraAccount(obj);
      message.success("Chỉnh sửa tài khoản thành công");
      await getListAccountAgencyC1(user_agency_id);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (popupAccount) {
      getListAccountAgencyC1(Number(user_agency_id));
    }
  }, [popupAccount]);

  const headerTable = [
    {
      name: "Số điện thoại",
      align: "center",
    },
    {
      name: "Mật khẩu",
      align: "center",
    },
    {
      name: "",
      align: "center",
    },
    {
      name: "Trạng thái",
      align: "center",
    },
    {
      name: "",
      align: "center",
    },
    {
      name: "",
      align: "center",
    },
  ];

  const headerTableProduct = (headerTable) => {
    return (
      <tr className="tr-table">
        {headerTable.map((item, index) => (
          <th
            className={"th-table"}
            style={{ textAlign: item.align, background: colors.white }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (contentTable) => {
    return contentTable.length === 0 ? (
      <tr
        height="411px"
        style={{
          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
        }}
      >
        <td colspan="6" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => (
        <tr className="tr-table" id="tr">
          <td
            className="td-table"
            style={{
              fontWeight: 400,
              fontSize: "clamp(8px, 4vw, 14px)",
              textAlign: "center",
              color: item.status === 0 ? colors.gray._400 : colors.blackChart,
              width: 240,
            }}
          >
            {item.isEdit || item.isCreate ? (
              <IInput
                loai="input"
                maxLength={10}
                style={{ textAlign: "center" }}
                onChange={(e) => {
                  if (/[0-9]/.test(e.target.value)) {
                    if (item.isEdit) {
                      setAccountEdit({
                        ...accountEdit,
                        username: e.target.value,
                      });
                    } else {
                      let dataNew = [...data];
                      dataNew[index].username = e.target.value;
                      setData([...dataNew]);
                    }
                  }
                  if (e.target.value === "") {
                    if (item.isEdit) {
                      setAccountEdit({ ...accountEdit, username: "" });
                    } else {
                      let dataNew = [...data];
                      dataNew[index].username = "";
                      setData([...dataNew]);
                    }
                  }
                }}
                placeholder="nhập số điện thoại"
                value={item.isEdit ? accountEdit.username : item.username}
              />
            ) : (
              item.username
            )}
          </td>
          <td
            className="td-table"
            style={{
              fontWeight: 400,
              fontSize: "clamp(8px, 4vw, 14px)",
              textAlign: "center",
              color: item.status === 0 ? colors.gray._400 : colors.blackChart,
              width: 230,
            }}
          >
            {item.isEdit || item.isCreate ? (
              <IInput
                loai="input"
                placeholder="nhập mật khẩu"
                value={item.isEdit ? accountEdit.password : item.password}
                style={{ textAlign: "center" }}
                onChange={(e) => {
                  if (
                    /^[a-zA-Z0-9\\s!@#$%^&*()-_=+`~]*$/.test(e.target.value)
                  ) {
                    if (item.isEdit) {
                      setAccountEdit({
                        ...accountEdit,
                        password: e.target.value,
                      });
                    } else {
                      let dataNew = [...data];
                      dataNew[index].password = e.target.value;
                      setData([...dataNew]);
                    }
                  }
                }}
              />
            ) : item.isShow ? (
              <div style={{ marginTop: 3 }}>{item.password}</div>
            ) : (
              <div style={{ marginTop: 6 }}>******</div>
            )}
          </td>
          <td style={{ width: 30, textAlign: "center" }}>
            {item.status === 0 ||
            item.isEdit ||
            item.isCreate ? null : item.isShow ? (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  let dataNew = [...data];
                  dataNew[index].isShow = false;
                  setData([...dataNew]);
                }}
              >
                <ISvg
                  name={ISvg.NAME.HIDE_PASSWORD}
                  width={20}
                  height={20}
                  fill={colors.gray._500}
                />
              </div>
            ) : (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  let dataNew = [...data];
                  dataNew[index].isShow = true;
                  setData([...dataNew]);
                }}
              >
                <ISvg name={ISvg.NAME.SHOW_PASSWORD} width={20} height={20} />
              </div>
            )}
          </td>
          <td
            className="td-table"
            style={{
              fontWeight: 400,
              fontSize: "clamp(8px, 4vw, 14px)",
              textAlign: "center",
              width: 250,
            }}
          >
            {item.isEdit || item.isCreate ? null : item.status_name}
          </td>
          <td style={{ width: 170 }}>
            <div
              style={{
                // textAlign: "center",
                display: "flex",
                justifyContent: "center",
              }}
            >
              {item.isEdit ? (
                <IButton
                  title="Hủy"
                  iconRight={true}
                  icon={ISvg.NAME.CROSS}
                  onClick={() => {
                    let dataNew = [...data];
                    dataNew[index].isEdit = false;
                    setData([...dataNew]);
                    setAccountEdit({});
                  }}
                  style={{ width: 140 }}
                  color={colors.oranges}
                />
              ) : item.isCreate ? (
                <IButton
                  title="Hủy"
                  iconRight={true}
                  icon={ISvg.NAME.CROSS}
                  onClick={() => {
                    let dataNew = [...data];
                    dataNew.splice(index, 1);
                    setData([...dataNew]);
                  }}
                  style={{ width: 140 }}
                  color={colors.oranges}
                />
              ) : item.status === 1 ? (
                <div
                  style={{
                    cursor: "not-allowed ",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.WRITE}
                    width={20}
                    height={20}
                    fill={item.status === 0 ? colors.main : colors.icon.default}
                  />
                </div>
              ) : (
                <div
                  style={{
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    let dataNew = [...data];
                    dataNew[index].isEdit = true;
                    setData([...dataNew]);
                    setAccountEdit({ ...dataNew[index] });
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.WRITE}
                    width={20}
                    height={20}
                    fill={item.status === 0 ? colors.main : colors.icon.default}
                  />
                </div>
              )}
            </div>
          </td>
          <td
            className="td-table"
            style={{
              display: "flex",
              justifyContent: "center",
              width: 170,
            }}
          >
            {item.isEdit ? (
              <IButton
                title="Lưu"
                iconRight={true}
                icon={ISvg.NAME.SAVE}
                onClick={() => {
                  let username = accountEdit.username.trim();
                  let headNumber = username.slice(0, 1);
                  if (
                    !username ||
                    username === "" ||
                    !accountEdit.password ||
                    accountEdit.password === ""
                  ) {
                    message.warning(
                      "Vui lòng nhập đầy đủ thông tin tài khoản!"
                    );
                    return;
                  }
                  if (
                    headNumber !== "0" ||
                    username.length > 10 ||
                    username.length < 10
                  ) {
                    message.error(
                      "Vui lòng nhập đúng định dạng số điện thoại."
                    );
                    return;
                  }

                  if (accountEdit.password.length < 6) {
                    message.error("Mật khẩu phải từ 6 ký tự trở lên.");
                    return;
                  }

                  let objEdit = {
                    user_agency_id: accountEdit.user_agency_id,
                    user_agency_login_id: accountEdit.id,
                    password: accountEdit.password,
                    username: username,
                    status: accountEdit.status,
                  };

                  postUpdateInfoAndStatusExtraAccount(objEdit);
                }}
                style={{ width: 140 }}
                color={colors.main}
              />
            ) : item.isCreate ? (
              <IButton
                title="Tạo"
                icon={ISvg.NAME.SAVE}
                iconRight={true}
                onClick={() => {
                  let username = item.username.trim();
                  let headNumber = username.slice(0, 1);
                  if (
                    !username ||
                    username === "" ||
                    !item.password ||
                    item.password === ""
                  ) {
                    message.warning(
                      "Vui lòng nhập đầy đủ thông tin tài khoản!"
                    );
                    return;
                  }
                  if (
                    headNumber !== "0" ||
                    username.length > 10 ||
                    username.length < 10
                  ) {
                    message.error(
                      "Vui lòng nhập đúng định dạng số điện thoại."
                    );
                    return;
                  }

                  if (item.password.length < 6) {
                    message.error("Mật khẩu phải từ 6 ký tự trở lên.");
                    return;
                  }

                  let objAdd = {
                    default_acc: Number(user_agency_id),
                    deviceid: "string",
                    os: "string",
                    password: item.password,
                    status: 0,
                    token: "string",
                    username: username,
                  };

                  postCreateExtraAccount(objAdd);
                }}
                style={{ width: 140 }}
                color={colors.main}
              />
            ) : item.username === acc_default ? null : item.status === 1 ? (
              <IButton
                title="Tạm dừng"
                onClick={() => {
                  if (item.username === acc_default) {
                    message.warning(
                      "Bạn không thể tạm dừng tài khoản mặc định!"
                    );
                    return;
                  }
                  let objEdit = {
                    user_agency_id: item.user_agency_id,
                    password: item.password,
                    username: item.username,
                    user_agency_login_id: item.id,
                    status: 0,
                  };
                  postUpdateInfoAndStatusExtraAccount(objEdit);
                }}
                style={{ width: 140 }}
                color={colors.oranges}
              />
            ) : (
              <IButton
                title="Kích hoạt"
                onClick={() => {
                  let objEdit = {
                    user_agency_id: item.user_agency_id,
                    password: item.password,
                    username: item.username,
                    user_agency_login_id: item.id,
                    status: 1,
                  };

                  postUpdateInfoAndStatusExtraAccount(objEdit);
                }}
                style={{ width: 140 }}
                color={colors.main}
              />
            )}
          </td>
        </tr>
      ))
    );
  };

  return (
    <div style={{ width: "100%", height: "100%", padding: 15 }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <ITitle
            level={2}
            title="DANH SÁCH TÀI KHOẢN PHỤ ĐẠI LÝ CẤP 1"
            style={{ color: colors.main, fontWeight: "bold" }}
          />
        </Col>
        <Col span={24}>
          {loading ? (
            <div>
              <Skeleton loading={true} active paragraph={{ rows: 14 }} />
            </div>
          ) : (
            <>
              <div
                style={{
                  height: 450,
                  borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                  borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                  borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                }}
              >
                <ITableHtml
                  style={{ maxHeight: 449 }}
                  childrenBody={bodyTableProduct(data)}
                  childrenHeader={headerTableProduct(headerTable)}
                  isBorder={false}
                  id="table"
                />
              </div>
              <div
                style={{
                  padding: "10px 0px",
                  borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                  borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                  borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <div
                    style={{ display: "flex", alignItems: "center" }}
                    className="cursor"
                    onClick={() => {
                      let dataNew = [...data];
                      if (dataNew[dataNew.length - 1].isCreate !== undefined) {
                        message.warning(
                          "Vui lòng hoàn tất thao tác tạo tài khoản đang diễn ra!"
                        );
                        return;
                      }
                      dataNew.unshift({
                        username: "",
                        password: "",
                        isShow: false,
                        isCreate: true,
                      });
                      setData([...dataNew]);

                      let table = document.getElementById("table");

                      table.scrollTo(0, 0);
                    }}
                  >
                    <span
                      style={{
                        margin: "3px 10px 0 0",
                        color: colors.main,
                        fontWeight: 500,
                        fontSize: "18px",
                      }}
                    >
                      THÊM TÀI KHOẢN
                    </span>
                    <div>
                      <ISvg
                        name={ISvg.NAME.ADDUPLOAD}
                        width={25}
                        height={25}
                        fill={colors.main}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </Col>
      </Row>
    </div>
  );
}

export default ModalAccount;
