import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelectBranch,
  IButton,
  ITable,
  ISelectCity,
  ISelect,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import { message, Tooltip } from "antd";
import { priceFormat } from "../../../../utils";

function AgencyS1Page(props) {
  let timeSearch = null;
  const [dataTable, setDataTable] = useState([]);
  const [isLoadingTable, setLoadingTable] = useState(true);
  const [pagination, setPagination] = useState({
    total: 0,
    sizeItem: 0,
  });

  const [dataBranch, setDataBranch] = useState([]);
  const [listMembership, setListMembership] = useState([]);

  const [filterTable, setFilterTable] = useState({
    status: 2,
    page: 1,
    city_id: 0,
    // district_id: 0,
    agency_id: -1,
    keySearch: "",
    membership_id: 0,
  });

  const status = [
    {
      key: 2,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Chờ duyệt",
    },
    {
      key: 1,
      value: "Đang hoạt động",
    },
    {
      key: 3,
      value: "Tạm ngưng",
    },
  ];

  const _getAPIListMembership = async () => {
    try {
      const data = await APIService._getListMemberships();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setListMembership(dataNew);
    } catch (error) {}
  };

  const _getAPIListBranch = async () => {
    try {
      const data = await APIService._getBranchDropList();
      const dataNew = data.agency.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: -1,
      });
      setDataBranch(dataNew);
    } catch (error) {}
  };

  const _getAPIListAgencyS1 = async () => {
    try {
      const data = await APIService._getListAgencyS1(
        filterTable.status,
        filterTable.page,
        filterTable.city_id,
        // filterTable.district_id,
        filterTable.agency_id,
        filterTable.keySearch,
        filterTable.membership_id
      );
      let useragency = data.useragency;
      useragency.map((item, index) => {
        useragency[index].stt = (filterTable.page - 1) * 10 + index + 1;
        useragency[index].sttAndId = {
          id: item.id,
          stt: index,
        };
      });
      _getAPIListMembership();
      _getAPIListBranch();
      setDataTable(useragency);
      setPagination({
        ...pagination,
        total: data.total,
        sizeItem: data.size,
      });
      setLoadingTable(false);
    } catch (error) {
      message.error(error);
      setLoadingTable(false);
    }
  };

  const onChangeCity = (key, value) => {
    setFilterTable({ ...filterTable, city_id: key, page: 1 });
  };
  const onChangeBranch = (key, value) => {
    setFilterTable({ ...filterTable, agency_id: key, page: 1 });
  };
  const onChangePage = (page) => {
    setFilterTable({ ...filterTable, page: page });
  };

  useEffect(() => {
    _getAPIListAgencyS1();
  }, [filterTable]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      width: 80,
      align: "center",
    },
    {
      title: "Tên đại lý",
      dataIndex: "shop_name",
      key: "shop_name",
      width: 250,
      render: (shop_name) => (
        <Tooltip title={shop_name}>
          <span>{!shop_name ? "-" : shop_name}</span>
        </Tooltip>
      ),
    },
    {
      title: "Mã đại lý",
      dataIndex: "dms_code",
      width: 200,
      key: "dms_code",
      render: (dms_code) => (
        <Tooltip title={dms_code}>
          <span>{!dms_code ? "-" : dms_code}</span>
        </Tooltip>
      ),
    },
    {
      title: "Trạng Thái",
      dataIndex: "status_name",
      key: "status_name",
      width: 100,
      render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
    },
    {
      title: "Doanh thu",
      dataIndex: "tong_tien_nap",
      width: 170,
      key: "tong_tien_nap",
      align: "center",
      render: (tong_tien_nap) => (
        <span>{!tong_tien_nap ? "0" : priceFormat(tong_tien_nap)}</span>
      ),
    },
    {
      title: "Hạn Mức Tín Dụng",
      dataIndex: "credit_limit",
      width: 170,
      key: "credit_limit",
      align: "center",
      render: (credit_limit) => (
        <span>{!credit_limit ? "0" : priceFormat(credit_limit)}</span>
      ),
    },
    {
      title: "Hạn Mức Khả Dụng",
      dataIndex: "hmkd",
      key: "hmkd",
      align: "center",
      width: 170,
      render: (hmkd) => <span>{!hmkd ? "0" : priceFormat(hmkd)}</span>,
    },
    {
      title: "Hạn Mức Gối Đầu",
      dataIndex: "knee_limit",
      key: "knee_limit",
      align: "center",
      width: 170,
      render: (knee_limit) => (
        <span>{!knee_limit ? "0" : priceFormat(knee_limit)}</span>
      ),
    },
    {
      title: "Công nợ cuối kỳ",
      width: 150,
      dataIndex: "cong_no_cuoi_ky",
      key: "cong_no_cuoi_ky",
      align: "center",
      render: (cong_no_cuoi_ky) => (
        <span>{!cong_no_cuoi_ky ? "0" : priceFormat(cong_no_cuoi_ky)}</span>
      ),
    },
    {
      title: "Nợ quá hạn",
      width: 150,
      dataIndex: "overdue",
      key: "overdue",
      align: "center",
      render: (overdue) => <span>{!overdue ? "0" : priceFormat(overdue)}</span>,
    },
    {
      title: "Hạn nợ",
      dataIndex: "debt_term",
      key: "debt_term",
      align: "center",
      render: (debt_term) => (
        <span>{!debt_term ? "0" : `${debt_term} Ngày`}</span>
      ),
    },

    {
      title: "Phòng kinh doanh",
      width: 150,
      dataIndex: "agency_name",
      key: "agency_name",
      render: (agency_name) => <span>{!agency_name ? "-" : agency_name}</span>,
    },

    {
      title: "Cấp bậc",
      dataIndex: "membership",
      key: "membership",
      render: (membership) => <span>{!membership ? "-" : membership}</span>,
    },
    // {
    //   title: "Xác nhận OTP",
    //   dataIndex: "otp_confirm",
    //   key: "otp_confirm",
    //   render: (otp_confirm) => <span>{!otp_confirm ? "-" : otp_confirm}</span>,
    // },
    {
      title: "Hạng khách hàng",
      dataIndex: "class_user",
      key: "class_user",
      align: "center",
      render: (class_user) => <span>{!class_user ? "-" : class_user}</span>,
    },
    // {
    //   dataIndex: "id",
    //   key: "id",
    //   align: "right",
    //   width: "80",

    //   render: (id) => (
    //     <div style={{ display: "flex", flexDirection: "row" }}>
    //       <div
    //         onClick={(e) => {
    //           e.stopPropagation();

    //           props.history.push("/agencyS1/edit/" + id);
    //         }} // idAndStt.index vị trí của mảng
    //         className="cell"
    //         style={{ width: 50 }}
    //       >
    //         <ISvg
    //           name={ISvg.NAME.WRITE}
    //           width={20}
    //           height={20}
    //           fill={colors.icon.default}
    //         />
    //       </div>
    //       <div
    //         className="cursor"
    //         style={{ width: 50 }}
    //         onClick={(e) => {
    //           e.stopPropagation();
    //           props.history.push("/detailAgency/" + id);
    //         }}
    //       >
    //         <ISvg
    //           name={ISvg.NAME.CHEVRONRIGHT}
    //           width={11}
    //           height={20}
    //           fill={colors.icon.default}
    //         />
    //       </div>
    //     </div>
    //   ),
    // },
  ];

  return (
    <Container fluid>
      <Row className="p-0">
        <ITitle
          level={1}
          title="Đại lý cấp 1"
          style={{
            color: colors.main,
            fontWeight: "bold",
            flex: 1,
            display: "flex",
            alignItems: "flex-end",
          }}
        />
        <ISearch
          onChange={(e) => {
            let search = e.target.value;
            if (timeSearch) {
              clearTimeout(timeSearch);
            }
            timeSearch = setTimeout(() => {
              setLoadingTable(true);
              setFilterTable({ ...filterTable, keySearch: search, page: 1 });
            }, 500);
          }}
          placeholder="Nhập từ khóa"
          onPressEnter={() => _getAPIListAgencyS1()}
          icon={
            <div
              style={{
                display: "flex",
                width: 42,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <img src={images.icSearch} style={{ width: 20, height: 20 }} />
            </div>
          }
        />
      </Row>

      <Row className="center" style={{ marginTop: 36 }}>
        <Col
          style={{
            display: "flex",
            justifyContent: "flex-start",
            padding: 0,
          }}
        >
          <Col className="mt-0 center mr-4 ml-3 p-0" xs="auto">
            <Row className="center">
              <div style={{ marginLeft: 10 }}>
                <ITitle title="Trạng thái" level={4} />
              </div>
            </Row>
          </Col>
          <div style={{ width: 160 }}>
            <ISelect
              defaultValue="Tất cả"
              select={true}
              data={status}
              onChange={(key) => {
                setFilterTable({
                  ...filterTable,
                  status: key,
                  page: 1,
                });
              }}
            />
          </div>
          <Col className="mt-0 center mr-4 ml-4 p-0" xs="auto">
            <Row className="center">
              <div style={{ marginLeft: 10 }}>
                <ITitle title="Cấp bậc" level={4} />
              </div>
            </Row>
          </Col>
          <Col style={{ padding: 0 }}>
            <div style={{ width: 160 }}>
              <ISelect
                select={true}
                data={listMembership}
                defaultValue="Tất cả"
                style={{
                  background: colors.white,
                  borderWidth: 1,
                  borderColor: colors.line,
                  borderStyle: "solid",
                }}
                onChange={(key) => {
                  setFilterTable({
                    ...filterTable,
                    membership_id: key,
                    page: 1,
                  });
                }}
              />
            </div>
          </Col>
        </Col>
        <Col
          style={{
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Row>
            <Col className="mt-0 center mr-4 ml-4 p-0" xs="auto">
              <Row className="center">
                <ISvg
                  name={ISvg.NAME.LOCATION}
                  width={20}
                  height={20}
                  fill={colors.icon.default}
                />
                <div style={{ marginLeft: 10 }}>
                  <ITitle title="Khu vực" level={4} />
                </div>
              </Row>
            </Col>
            <Col className="mt-0 p-0">
              <ISelectCity
                select={true}
                defaultValue="Chọn khu vực"
                onChange={onChangeCity}
                fillerAll={true}
                select={true}
              />
            </Col>
            <Col>
              <div style={{ width: 160 }}>
                <ISelectBranch
                  select={true}
                  data={dataBranch}
                  defaultValue="Phòng kinh doanh"
                  style={{
                    background: colors.white,
                    borderWidth: 1,
                    borderColor: colors.line,
                    borderStyle: "solid",
                  }}
                  onChange={onChangeBranch}
                />
              </div>
            </Col>

            <Col xs="auto">
              <Row>
                <IButton
                  icon={ISvg.NAME.ARROWUP}
                  title="Tạo mới"
                  color={colors.main}
                  // style={{ marginRight: 12 }}
                  onClick={() => {
                    props.history.push("agencyS1/add/0");
                  }}
                />{" "}
                {/* <IButton
                  icon={ISvg.NAME.DOWLOAND}
                  title='Xuất file'
                  color={colors.main}
                  onClick={() => {
                    message.warning('Chức năng đang cập nhật')
                  }}
                /> */}
                {/* <Popconfirm
                  placement="bottom"
                  title={"Nhấn đồng ý để xóa ."}
                  onConfirm={() => {
                    if (selectedRowKeys.length === 0) {
                      message.warning("Bạn chưa chọn đại lý để xóa.");
                      return;
                    }
                    const objRemove = {
                      id: arrRemove,
                      status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                    };
                    setIsLoadingRemove(true);
                    triggerDelete(objRemove);
                  }}
                  okText="Đồng ý"
                  cancelText="Hủy"
                >
                  <IButton
                    icon={ISvg.NAME.DELETE}
                    title="Xóa"
                    color={colors.oranges}
                    style={{}}
                    loading={isLoadingRemove}
                  />
                </Popconfirm> */}
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>

      <Row style={{ marginTop: 36 }}>
        <ITable
          data={dataTable}
          columns={columns}
          style={{ width: "100%" }}
          rowKey={"id"}
          defaultCurrent={1}
          loading={isLoadingTable}
          maxpage={pagination.total}
          sizeItem={pagination.sizeItem}
          indexPage={filterTable.page}
          scroll={{ x: 2200 }}
          onRow={(record, rowIndex) => {
            return {
              onClick: (event) => {
                const indexRow = Number(
                  event.currentTarget.attributes[1].ownerElement.dataset.rowKey
                );
                props.history.push("/detailAgency/" + indexRow);
              }, // click row
              onMouseDown: (event) => {
                const indexRow = Number(
                  event.currentTarget.attributes[1].ownerElement.dataset.rowKey
                );
                if (event.button == 2 || event == 4) {
                  window.open("/detailAgency/" + indexRow, "_blank");
                }
              },
            };
          }}
          onChangePage={(page) => {
            setLoadingTable(true);
            onChangePage(page);
          }}
        />
      </Row>
    </Container>
  );
}

export default AgencyS1Page;
