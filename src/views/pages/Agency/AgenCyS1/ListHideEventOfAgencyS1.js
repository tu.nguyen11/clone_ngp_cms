import React, { useState, useEffect } from "react";
import { Row, Col, Modal, message, Tooltip } from "antd";
import { useHistory, useParams } from "react-router-dom";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  IButton,
  ITable,
  IDatePicker,
} from "../../../components";
import FormatterDay from "../../../../utils/FormatterDay";
import { images, colors } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";

function ListHideEventOfAgencyS1(props) {
  const history = useHistory();
  const params = useParams();
  const formatString = "#DD#/#MM#/#YYYY# #hhh#:#mm#";

  const NewDate = new Date();
  const DateStart = new Date();
  let startValue = DateStart.setMonth(DateStart.getMonth() - 1);
  const [filter, setFilter] = useState({
    user_id: params.id,
    search: "",
    page: 1,
    status: 2,
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
  });

  const [loadingTable, setLoadingTable] = useState(true);

  const _fetchAPIGetHideEvent = async (filter) => {
    try {
      const data = await APIService._getListHideEventAgencyS1(filter);
      data.listEvent.map((item, index) => {
        item.stt = (filter.page - 1) * 15 + index + 1;
        item.disabled = item.status === 2 ? true : false;
        item.rowTable = JSON.stringify({
          id: item.event_id,
          type_increment: item.type_increment,
        });
        return item;
      });
      setData(data);
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIGetHideEvent(filter);
  }, [filter]);

  const [data, setData] = useState({
    listEvent: [],
    total: 1,
    size: 15,
  });

  const status = [
    {
      key: 2,
      value: "Tất cả",
    },
    {
      key: 1,
      value: "Đang áp dụng",
    },
    {
      key: 0,
      value: "Tạm dừng",
    },
  ];

  const renderTitle = (title) => {
    return (
      <div>
        <ITitle title={title} style={{ fontWeight: "bold" }} />
      </div>
    );
  };

  const columns = [
    {
      title: renderTitle("STT"),
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
      render: (stt) => <span>{stt}</span>,
    },
    {
      title: renderTitle("Tên chính sách"),
      dataIndex: "event_name",
      key: "event_name",
      wordWrap: "break-word",
      wordBreak: "break-word",
      render: (event_name) => <span>{!event_name ? "-" : event_name}</span>,
    },
    {
      title: renderTitle("Mã chính sách"),
      dataIndex: "event_code",
      key: "event_code",
      wordWrap: "break-word",
      wordBreak: "break-word",
      render: (event_code) => <span>{!event_code ? "-" : event_code}</span>,
    },
    // {
    //   title: renderTitle("Loại sản phẩm"),
    //   dataIndex: "group_name",
    //   key: "group_name",
    //   wordWrap: "break-word",
    //   wordBreak: "break-word",
    //   render: (group_name) => <span>{!group_name ? "-" : group_name}</span>,
    // },
    // {
    //   title: renderTitle("Ngày bắt đầu"),
    //   render: (obj) => (
    //     <span>
    //       {!obj.start_date || obj.start_date <= 0 || obj.status === 0
    //         ? "-"
    //         : FormatterDay.dateFormatWithString(obj.start_date, formatString)}
    //     </span>
    //   ),
    // },

    // {
    //   title: renderTitle("Điều kiện áp dụng"),
    //   dataIndex: "type_condition_apply_name",
    //   key: "type_condition_apply_name",
    //   render: (type_condition_apply_name) => (
    //     <span>
    //       {!type_condition_apply_name ? "-" : type_condition_apply_name}
    //     </span>
    //   ),
    // },
    // {
    //   title: renderTitle("Hình thức khuyến mãi"),
    //   dataIndex: "type_reward_name",
    //   key: "type_reward_name",
    //   render: (type_reward_name) => (
    //     <span>{!type_reward_name ? "-" : type_reward_name}</span>
    //   ),
    // },
    // {
    //   title: renderTitle("Trạng thái"),
    //   dataIndex: "status_name",
    //   key: "status_name",
    //   render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
    // },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Danh sách chính sách bán hàng tặng hàng ẩn
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm theo tên , mã chương trình">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm theo tên , mã chương trình"
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilter({ ...filter, search: e.target.value, page: 1 });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={24}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.EXPERIMENT}
                    with={16}
                    height={16}
                    fill={colors.icon.default}
                  />
                  <ITitle
                    title="Từ ngày"
                    level={4}
                    style={{
                      marginLeft: 15,
                      marginRight: 15,
                    }}
                  />
                  <div>
                    <IDatePicker
                      isBackground
                      from={filter.start_date}
                      to={filter.end_date}
                      onChange={(date, dateString) => {
                        if (date.length <= 1) {
                          return;
                        }

                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          start_date: date[0]._d.setHours(0, 0, 0),
                          end_date: date[1]._d.setHours(23, 59, 59),
                          page: 1,
                        });
                      }}
                      style={{
                        background: colors.white,
                        borderWidth: 1,
                        borderColor: colors.line,
                        borderStyle: "solid",
                      }}
                    />
                  </div>
                  {/* <div style={{ margin: "0px 15px 0px 15px" }}>
                    <ISelect
                      defaultValue="Trạng thái"
                      data={status}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          status: value,
                          page: 1,
                        });
                      }}
                    />
                  </div> */}
                  {/* <div style={{ width: 150 }}>
                    <ISelect
                      defaultValue="Loại sản phẩm"
                      data={productType}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilter({
                          ...filter,
                          group_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div> */}
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={data.listEvent}
                style={{ width: "100%" }}
                defaultCurrent={1}
                sizeItem={data.size}
                indexPage={filter.page}
                maxpage={data.total}
                rowKey="rowTable"
                size="small"
                scroll={{ x: 0 }}
                loading={loadingTable}
                onRow={(record, rowIndex) => {
                  return {
                    onClick: (event) => {
                      const rowValue =
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey;
                      const objRow = JSON.parse(rowValue);
                      history.push(
                        objRow.type_increment === 0
                          ? `/event/eventGift/detailSalesPolicyEventGiftPage/${objRow.id}`
                          : `/detail/event/immediate/${objRow.id}`
                      );
                    },
                    onMouseDown: (event) => {
                      const rowValue =
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey;
                      const objRow = JSON.parse(rowValue);
                      if (event.button == 2 || event == 4) {
                        window.open(
                          objRow.type_increment === 0
                            ? `/event/eventGift/detailSalesPolicyEventGiftPage/${objRow.id}`
                            : `/detail/event/immediate/${objRow.id}`,
                          "_blank"
                        );
                      }
                    },
                  };
                }}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilter({ ...filter, page: page });
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default ListHideEventOfAgencyS1;
