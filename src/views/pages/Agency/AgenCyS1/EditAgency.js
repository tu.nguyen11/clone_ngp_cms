import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  message,
  DatePicker,
  Form,
  Input,
} from "antd";
import moment from "moment";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  ISelectCity,
  ISelectCounty,
  ILoading,
} from "../../../components";

import { colors } from "../../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";
import { ImageType } from "../../../../constant";

const { RangePicker } = DatePicker;

class EditAgency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      write: true,
      dateEnd: "",
      errEnd: true,
      dateStart: "",
      data: {
        supplier: {
          contract: {},
        },
      },
      loading: false,
      imageEdit: [],
      url: "",
      dataEdit: {},
      listCities: [], // danh sách thành phố
      listCounty: [], // danh sách quận / huyện
      errStart: true,
      errCity: true, // lỗi khi chưa có dữ liệu sẽ báo lỗi
      errCounty: true, // lỗi khi chưa có dữ liệu sẽ báo lỗi
      disabledCounty: false, // disabled quận huyện khi chưa được select thành phố
      cityID: 0, // cityID  thành phố,
      countyID: 0, // countyID lấy quận huyên
      cityName: "",
      keySearchCounty: "",
    };
    // this.check = this.check.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onOkDatePickerStart = this.onOkDatePickerStart.bind(this);
    this.onOkDatePickerEnd = this.onOkDatePickerEnd.bind(this);
    this.onChangeCity = this.onChangeCity.bind(this);
    this.agencyID = Number(this.props.match.params.id);
  }

  // API Start
  componentDidMount() {
    this._getAPIDetailAgency(this.agencyID);
  }
  _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailSuppliers(idAgency);
      this.setState(
        {
          data: data,
          url: data.image_url,
          imageEdit: [data.supplier.avatar],
          loading: true,
        },
        () => {
          this._getAPIListCounty(
            this.state.data.supplier.cityID,
            this.state.keySearchCounty
          );
        }
      );
    } catch (error) {}
  };

  _postAPIEditSuppliers = async (obj) => {
    const data = await APIService._postEditSuppliers(obj);
    message.success("Cập nhập thành công");
    this.props.history.push("/agencyPage");
  };

  _getAPIListCounty = async (city_id, searh) => {
    const data = await APIService._getListCounty(city_id, searh);
    const dataNew = data.district.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    // this.state.data.supplier.districtID = dataNew[0].key;
    this.setState({
      data: this.state.data,
      listCounty: dataNew,
    });
  };

  onChangeCity = (key, value) => {
    this.state.data.supplier.cityID = key;
    this.setState(
      {
        // data: this.state.data,
        disabledCounty: true,
        errCity: false,
      },
      () => {
        this._getAPIListCounty(
          this.state.data.supplier.cityID,
          this.state.keySearchCounty
        );
        this.props.form.validateFields(["cityID"], { force: true });
      }
    );
  };

  onChangeCounty = (key, value) => {
    this.state.data.supplier.districtID = key;

    this.setState(
      {
        errCounty: false,
      },
      () => {
        this.props.form.validateFields(["districtID"], { force: true });
      }
    );
  };

  onOkDatePickerStart = (date, dateString) => {
    this.setState(
      {
        dateStart: Date.parse(dateString),
        errStart: false,
      },
      () => {
        this.props.form.validateFields(["dateBegin"], { force: true });
      }
    );
  };

  onOkDatePickerEnd = (date, dateString) => {
    this.setState(
      {
        dateEnd: Date.parse(dateString),
        errEnd: false,
      },
      () => {
        this.props.form.validateFields(["dateEnd"], { force: true });
      }
    );
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      const data = {
        id: this.agencyID,
        supplierCode: this.state.data.supplier.supplierCode,
        address: this.state.data.supplier.address,
        avatar: this.state.imageEdit,
        supplierName: this.state.data.supplier.supplierName,
        phone: this.state.data.supplier.phone,
        cityID: this.state.data.supplier.cityID,
        districtID: this.state.data.supplier.districtID,
        email: this.state.data.supplier.email,
        // img: this.state.imageEdit
      };

      this.setState(
        {
          dataEdit: data,
        },
        () => {
          this._postAPIEditSuppliers(this.state.dataEdit);
        }
      );
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Container fluid>
        <div>
          <Row className="mt-3" xs="auto">
            <ITitle
              level={1}
              title="Chỉnh sửa đại lý phân phối"
              style={{ color: colors.main, fontWeight: "bold" }}
            />
          </Row>
          {!this.state.loading ? (
            <ILoading />
          ) : (
            <Form onSubmit={this.handleSubmit}>
              <Row
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  paddingTop: 32,
                  paddingBottom: 32,
                }}
                xs="auto"
                sm={12}
                md={12}
              >
                <Form.Item>
                  <IButton
                    icon={ISvg.NAME.SAVE}
                    title="Lưu"
                    color={colors.main}
                    htmlType="submit"
                    style={{ marginRight: 20 }}
                    onClick={() => this.handleSubmit}
                  />
                </Form.Item>
                <Form.Item>
                  <IButton
                    icon={ISvg.NAME.CROSS}
                    title="Hủy"
                    color={colors.oranges}
                    style={{}}
                    onClick={() => {
                      this.props.history.push("/agencyPage");
                    }}
                  />
                </Form.Item>
              </Row>

              <Row
                style={{ maxWidth: 815, minWidth: 615, flex: 1 }}
                className="p-0 shadow"
              >
                <Col style={{ background: colors.white }} className="pl-5 ml-0">
                  <Row className="p-0 m-0" style={{}}>
                    <Col
                      className="pl-0 pr-0 pt-0 m-0"
                      style={{ paddingBottom: 40 }}
                    >
                      <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                        <ITitle
                          level={2}
                          title="Thông tin đại lý"
                          style={{ color: colors.black, fontWeight: "bold" }}
                        />
                      </Row>

                      <Form.Item>
                        {getFieldDecorator("supplierName", {
                          rules: [
                            {
                              message: "nhập tên cửa hàng đại lý",
                            },
                          ],
                        })(
                          <Row className="p-0 m-0" xs="auto">
                            <Col className="p-0 m-0">
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Tên đại lý"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  write={this.state.write}
                                  value={this.state.data.supplier.supplierName}
                                  placeholder="Nhập tên cửa hàng đại lý"
                                  onChange={(event) => {
                                    this.state.data.supplier.supplierName =
                                      event.target.value;
                                    this.setState({});
                                  }}
                                />
                              </Row>
                            </Col>
                          </Row>
                        )}
                      </Form.Item>

                      <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                        <Col className="p-0 m-0">
                          <Form.Item>
                            {getFieldDecorator("supplierCode", {
                              rules: [
                                {
                                  message: "nhập mã đại lý",
                                },
                              ],
                            })(
                              <div>
                                <Row className="p-0 m-0 mb-2">
                                  <ITitle
                                    level={4}
                                    title={"Mã đại lý"}
                                    style={{ fontWeight: 600 }}
                                  />
                                </Row>
                                <Row className="p-0 m-0">
                                  <IInput
                                    value={
                                      this.state.data.supplier.supplierCode
                                    }
                                    placeholder="Nhập mã đại lý"
                                    onChange={(event) => {
                                      this.state.data.supplier.supplierCode =
                                        event.target.value;
                                      this.setState({});
                                    }}
                                  />
                                </Row>
                              </div>
                            )}
                          </Form.Item>
                        </Col>
                        <Col
                          className="p-0"
                          style={{ marginLeft: 28, marginTop: 0 }}
                        >
                          <Form.Item>
                            {getFieldDecorator("email", {
                              rules: [
                                {
                                  type: "email",
                                  message:
                                    "không đúng định dạng (abc@gmail.com)",
                                },
                                {
                                  message: "nhập địa chỉ email liên hệ",
                                },
                              ],
                            })(
                              <div>
                                <Row className="p-0 m-0 mb-2">
                                  <ITitle
                                    level={4}
                                    title={"Email"}
                                    style={{ fontWeight: 600 }}
                                  />
                                </Row>
                                <Row className="p-0 m-0">
                                  <IInput
                                    value={this.state.data.supplier.email}
                                    placeholder="Nhập địa chỉ email liên hệ"
                                    onChange={(event) => {
                                      this.state.data.supplier.email =
                                        event.target.value;
                                      this.setState({});
                                    }}
                                  />
                                </Row>
                              </div>
                            )}
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                        <Col className="p-0 m-0">
                          <Form.Item>
                            {getFieldDecorator("phone", {
                              rules: [
                                {
                                  message: "nhập số điện thoại",
                                },
                              ],
                            })(
                              <div>
                                <Row className="p-0 m-0 mb-2">
                                  <ITitle
                                    level={4}
                                    title={"Điện thoại"}
                                    style={{ fontWeight: 600 }}
                                  />
                                </Row>
                                <Row className="p-0 m-0">
                                  <IInput
                                    value={this.state.data.supplier.phone}
                                    placeholder="Nhập số điện thoại"
                                    onChange={(event) => {
                                      this.state.data.supplier.phone =
                                        event.target.value;
                                      this.setState({});
                                    }}
                                  />
                                </Row>
                              </div>
                            )}
                          </Form.Item>
                        </Col>
                        <Col
                          className="p-0"
                          style={{ marginLeft: 28, marginTop: 0 }}
                        >
                          <Form.Item>
                            {getFieldDecorator("address", {
                              rules: [
                                {
                                  message: "nhập địa chỉ đại lý",
                                },
                              ],
                            })(
                              <div>
                                <Row className="p-0 m-0 mb-2">
                                  <ITitle
                                    level={4}
                                    title={"Địa chỉ"}
                                    style={{ fontWeight: 600 }}
                                  />
                                </Row>
                                <Row className="p-0 m-0">
                                  <IInput
                                    // style={{ height: 100 }}
                                    value={
                                      // this.state.data.supplier.address +
                                      // ", " +
                                      // this.state.data.supplier.districtsName +
                                      // ", " +
                                      // this.state.data.supplier.cityName
                                      this.state.data.supplier.address
                                    }
                                    placeholder="Nhập địa chỉ đại lý"
                                    onChange={(event) => {
                                      this.state.data.supplier.address =
                                        event.target.value;
                                      this.setState({});
                                    }}
                                    // loai="textarea"
                                  />
                                </Row>
                              </div>
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                        <ITitle
                          level={2}
                          title="Thông tin hợp đồng"
                          style={{ color: colors.black, fontWeight: "bold" }}
                        />
                      </Row>

                      <Row>
                        <Col style={{ opacity: 0.5 }}>
                          <Row
                            className="p-0 mt-0 ml-0 mb-0 mr-0"
                            xs="auto"
                            style={{
                              opacity: 30,
                              evaluation: 3,
                            }}
                          >
                            <Col
                              className="p-0 m-0"
                              style={{ opacity: 30, evaluation: 3 }}
                            >
                              <Form.Item>
                                {getFieldDecorator("numberContract", {
                                  rules: [
                                    {
                                      // required: this.state.checkNick,

                                      message: "nhập số ghi trên hợp đồng",
                                    },
                                  ],
                                })(
                                  <div>
                                    <Row className="p-0 m-0 mb-2">
                                      <ITitle
                                        level={4}
                                        title={"Số hợp đồng"}
                                        style={{ fontWeight: 600 }}
                                      />
                                    </Row>
                                    <Row className="p-0 m-0">
                                      <ITitle
                                        placeholder="Nhập số ghi trên hợp đồng"
                                        disabled={true}
                                        title={
                                          this.state.data.supplier.contract
                                            .numberContract
                                        }
                                      />
                                    </Row>
                                  </div>
                                )}
                              </Form.Item>
                            </Col>
                            <Col
                              className="p-0"
                              style={{ marginLeft: 28, marginTop: 0 }}
                            >
                              <Form.Item>
                                {getFieldDecorator("companyName", {
                                  rules: [
                                    {
                                      // required: this.state.checkNick,

                                      message:
                                        "nhập tên công ty ký kết hợp đồng",
                                    },
                                  ],
                                })(
                                  <div>
                                    <Row className="p-0 m-0 mb-2">
                                      <ITitle
                                        level={4}
                                        title={"Tên công ty"}
                                        style={{ fontWeight: 600 }}
                                      />
                                    </Row>
                                    <Row className="p-0 m-0">
                                      <ITitle
                                        placeholder="Nhập tên công ty ký kết hợp đồng"
                                        disabled={true}
                                        title={
                                          this.state.data.supplier.contract
                                            .companyName
                                        }
                                      />
                                    </Row>
                                  </div>
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                            <Col className="p-0 m-0">
                              <Form.Item>
                                {getFieldDecorator("companyOnwer", {
                                  rules: [
                                    {
                                      // required: this.state.checkNick,

                                      message: "nhập tên người địa diện",
                                    },
                                  ],
                                })(
                                  <div>
                                    <Row className="p-0 m-0 mb-2">
                                      <ITitle
                                        level={4}
                                        title={"Đại diện"}
                                        style={{ fontWeight: 600 }}
                                      />
                                    </Row>
                                    <Row className="p-0 m-0">
                                      <ITitle
                                        placeholder="Nhập tên người địa diện"
                                        disabled={true}
                                        title={
                                          this.state.data.supplier.contract
                                            .companyOnwer
                                        }
                                      />
                                    </Row>
                                  </div>
                                )}
                              </Form.Item>
                            </Col>
                            <Col
                              className="p-0"
                              style={{ marginLeft: 28, marginTop: 0 }}
                            >
                              <Form.Item>
                                {getFieldDecorator("taxtNumber", {
                                  rules: [
                                    {
                                      // required: this.state.checkNick,

                                      message: "nhập mã số ĐKKD/MST công ty",
                                    },
                                  ],
                                })(
                                  <div>
                                    <Row className="p-0 m-0 mb-2">
                                      <ITitle
                                        level={4}
                                        title={"Mã số thuế"}
                                        style={{ fontWeight: 600 }}
                                      />
                                    </Row>
                                    <Row className="p-0 m-0">
                                      <ITitle
                                        placeholder="Nhập mã số ĐKKD/MST công ty"
                                        disabled={true}
                                        title={
                                          this.state.data.supplier.contract
                                            .taxtNumber
                                        }
                                      />
                                    </Row>
                                  </div>
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                            <Col className="p-0 m-0">
                              <Form.Item>
                                {getFieldDecorator("idCard", {
                                  rules: [
                                    {
                                      // required: this.state.checkNick,

                                      message: "nhập số CMND đại diện",
                                    },
                                  ],
                                })(
                                  <div>
                                    <Row className="p-0 m-0 mb-2">
                                      <ITitle
                                        level={4}
                                        title={"CMND"}
                                        style={{ fontWeight: 600 }}
                                      />
                                    </Row>
                                    <Row className="p-0 m-0">
                                      <ITitle
                                        placeholder="Nhập số CMND đại diện"
                                        disabled={true}
                                        title={
                                          this.state.data.supplier.contract
                                            .idCard
                                        }
                                      />
                                    </Row>
                                  </div>
                                )}
                              </Form.Item>
                            </Col>
                            <Col
                              className="p-0"
                              style={{ marginLeft: 28, marginTop: 0 }}
                            >
                              <Form.Item>
                                {getFieldDecorator("adddressContract", {
                                  rules: [
                                    {
                                      // required: this.state.checkNick,

                                      message: "nhập nơi cấp giấy phép ĐKKD",
                                    },
                                  ],
                                })(
                                  <div>
                                    <Row className="p-0 m-0 mb-2">
                                      <ITitle
                                        level={4}
                                        title={"Nơi cấp"}
                                        style={{ fontWeight: 600 }}
                                      />
                                    </Row>
                                    <Row className="p-0 m-0">
                                      <ITitle
                                        placeholder="Nhập nơi cấp giấy phép ĐKKD"
                                        disabled={true}
                                        title={
                                          this.state.data.supplier.contract
                                            .adddressContract
                                        }
                                      />
                                    </Row>
                                  </div>
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Form.Item>
                            {getFieldDecorator("addressCompany", {
                              rules: [
                                {
                                  // required: this.state.checkNick,

                                  message: "nhập địa điểm ĐKKD",
                                },
                              ],
                            })(
                              <Row
                                className="p-0 mt-0 ml-0 mb-0 mr-0"
                                xs="auto"
                              >
                                <Col className="p-0 m-0">
                                  <Row className="p-0 m-0 mb-2">
                                    <ITitle
                                      level={4}
                                      title={"Địa chỉ công ty"}
                                      style={{ fontWeight: 600 }}
                                    />
                                  </Row>
                                  <Row className="p-0 m-0">
                                    <ITitle
                                      placeholder="Nhập địa điểm ĐKKD"
                                      disabled={true}
                                      title={
                                        this.state.data.supplier.contract
                                          .addressCompany
                                      }
                                    />
                                  </Row>
                                </Col>
                              </Row>
                            )}
                          </Form.Item>
                          <Row className="p-0 mt-3 ml-0 mb-0 mr-0" xs="auto">
                            <Col className="p-0 m-0">
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Ngày kí hợp đồng"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Form.Item>
                                {getFieldDecorator("dateBegin", {
                                  rules: [
                                    {
                                      // required: this.state.errStart,
                                      message: "nhập ngày kí kết",
                                    },
                                  ],
                                })(
                                  <Row className="p-0 m-0">
                                    <ITitle
                                      title={FormatterDay.dateFormatWithString(
                                        this.state.data.supplier.contract
                                          .date_begin,
                                        "#DD#-#MM#-#YYYY#"
                                      )}
                                    />
                                  </Row>
                                )}
                              </Form.Item>
                            </Col>

                            <Col
                              className="p-0"
                              style={{ marginLeft: 28, marginTop: 0 }}
                            >
                              <Row className="p-0 m-0">
                                <ITitle
                                  level={4}
                                  title={"Ngày hết hợp đồng"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>

                              <Form.Item>
                                {getFieldDecorator("dateEnd", {
                                  rules: [
                                    {
                                      message: "nhập ngày chấm dứt",
                                      type: "object",
                                    },
                                  ],
                                })(
                                  <Row className="p-0 m-0">
                                    <ITitle
                                      title={FormatterDay.dateFormatWithString(
                                        this.state.data.supplier.contract
                                          .date_end,
                                        "#DD#-#MM#-#YYYY#"
                                      )}
                                    />
                                  </Row>
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col className="m-0 p-0 ml-5 " xs="auto">
                      <div
                        style={{
                          width: 2,
                          height: "100%",
                          background: colors.line_2,
                        }}
                      ></div>
                    </Col>
                    <Col
                      className="pt-0 pl-4 pr-0 pb-0 ml-3 mr-3 mt-0 mb-0"
                      style={{
                        paddingBottom: 40,
                      }}
                      xs={4}
                    >
                      <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                        <ITitle
                          level={2}
                          title="Khu vực"
                          style={{ color: colors.black, fontWeight: "bold" }}
                        />
                      </Row>
                      <Row className="p-0 m-0">
                        <Col className="ml-0 pl-0">
                          <Form.Item>
                            {getFieldDecorator("cityID", {
                              rules: [
                                {
                                  // required: this.state.checkNick,
                                  // required: this.state.errCity,

                                  message: "chọn tỉnh/TP",
                                },
                              ],
                            })(
                              <Col className="p-0 m-0">
                                <Row className="p-0 m-0 mb-2">
                                  <ITitle
                                    level={4}
                                    title={"Tỉnh/ Thành phố"}
                                    style={{ fontWeight: 600 }}
                                  />
                                </Row>
                                <Row className="p-0 m-0">
                                  <ISelectCity
                                    className="clear-pad"
                                    showSearch
                                    type="write"
                                    select={this.state.write}
                                    value={this.state.data.supplier.cityID}
                                    onChange={this.onChangeCity}
                                    placeholder="Chọn tỉnh/TP"
                                    isBackground={false}
                                  />
                                </Row>
                              </Col>
                            )}
                          </Form.Item>

                          <Form.Item>
                            {getFieldDecorator("districtID", {
                              rules: [
                                {
                                  // required: this.state.checkNick,
                                  // required: this.state.errCounty,
                                  message: "chọn quận/huyện",
                                },
                              ],
                            })(
                              <Col className="p-0 mt-0 ml-0 mb-0 mr-0">
                                <Row className="p-0 m-0 mb-2">
                                  <ITitle
                                    level={4}
                                    title={"Quận/ Huyện"}
                                    style={{ fontWeight: 600 }}
                                  />
                                </Row>
                                <Row className="p-0 m-0">
                                  <ISelect
                                    style={{
                                      background: colors.white,
                                      borderStyle: "solid",
                                      borderWidth: 0,
                                      borderColor: colors.line,
                                      borderBottomWidth: 1,
                                    }}
                                    type="write"
                                    showSearch
                                    select={true}
                                    onChange={this.onChangeCounty}
                                    optionFilterProp="children"
                                    showSearch
                                    onSearch={(keySearch) => {
                                      this.setState(
                                        {
                                          keySearchCounty: keySearch,
                                        },
                                        () =>
                                          this._getAPIListCounty(
                                            this.state.data.supplier.cityID,

                                            this.state.keySearchCounty
                                          )
                                      );
                                    }}
                                    value={this.state.data.supplier.districtID}
                                    placeholder="Chọn quận/huyện"
                                    data={this.state.listCounty}
                                    isBackground={false}
                                  />
                                </Row>
                                {/* <ISelectCounty
                                  placeholder="Chọn quận/huyện"
                                  city_id={this.state.cityID}
                                  showSearch
                                  onChange={this.onChangeCounty}
                                  callAPI={this.state.disabledCounty}
                                  select={this.state.disabledCounty}
                                /> */}
                              </Col>
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row className="p-0 mt-5 ml-0 mr-0">
                        <ITitle
                          level={2}
                          title="Hình ảnh đại lý"
                          style={{
                            color: colors.black,
                            fontWeight: "bold",
                            marginTop: 53,
                          }}
                        />

                        <Form.Item>
                          {getFieldDecorator("avatar", {
                            rules: [
                              {
                                // required: this.state.checkNick,

                                message: "chọn hình",
                                valuePropName: "fileList",
                                getValueFromEvent: this.normFile,
                              },
                            ],
                          })(
                            <Col className="p-0 mt-4">
                              <IUpload
                                url={this.state.url}
                                type={ImageType.USER}
                                // fileList={this.state.data.supplier.i}
                                name={this.state.data.supplier.avatar}
                                callback={(file) => {
                                  this.state.imageEdit = file;
                                  this.setState({});
                                }}
                              />
                            </Col>
                          )}
                        </Form.Item>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>

              <Row></Row>
            </Form>
          )}
        </div>
      </Container>
    );
  }
}

const editAgency = Form.create({ name: "EditAgency" })(EditAgency);

export default editAgency;
