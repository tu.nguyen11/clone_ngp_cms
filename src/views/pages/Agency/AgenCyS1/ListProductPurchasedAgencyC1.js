import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip } from "antd";
import { useHistory, useParams } from "react-router-dom";
import { ITitle, ITable } from "../../../components";
import FormatterDay from "../../../../utils/FormatterDay";
import { colors } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";

function ListProductPurchasedAgencyC1(props) {
  const history = useHistory();
  const params = useParams();
  const formatString = "#DD#/#MM#/#YYYY# #hhh#:#mm#";

  const NewDate = new Date();
  const DateStart = new Date();
  let startValue = DateStart.setMonth(DateStart.getMonth() - 1);
  const [filter, setFilter] = useState({
    user_id: params.id,
    key: "",
    page: 1,
    status: 3,
    type: 3,
    start_date: startValue,
    group_id: 0,
    end_date: NewDate.setHours(23, 59, 59, 999),
  });

  const [loadingTable, setLoadingTable] = useState(true);

  const _fetchAPIGetEvent = async (filter) => {
    try {
      const data = await APIService._getListEventAgencyS1(filter);
      data.listEvent.map((item, index) => {
        item.stt = (filter.page - 1) * 15 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
        });
        return item;
      });
      setData(data);
      setLoadingTable(false);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIGetEvent(filter);
  }, [filter]);

  const [data, setData] = useState({
    listEvent: [],
    total: 1,
    size: 15,
  });

  const renderTitle = (title) => {
    return (
      <div>
        <ITitle title={title} style={{ fontWeight: "bold" }} />
      </div>
    );
  };

  const columns = [
    {
      title: renderTitle("STT"),
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
      render: (stt) => <span>{stt}</span>,
    },
    {
      title: renderTitle("Tên phiên bản"),
      render: (obj) =>
        !obj.event_name ? (
          "-"
        ) : (
          <Tooltip title={obj.event_name}>
            <span
              onClick={(e) => {
                e.stopPropagation();
                history.push(`/detailAgency/${obj.event_id}`);
              }}
            >
              {obj.event_name}
            </span>
          </Tooltip>
        ),
    },
    {
      title: renderTitle("Mã phiên bản"),
      dataIndex: "event_code",
      key: "event_code",
      wordWrap: "break-word",
      wordBreak: "break-word",
      render: (event_code) => <span>{!event_code ? "-" : event_code}</span>,
    },
    {
      title: renderTitle("ĐVT"),
      dataIndex: "group_name",
      key: "group_name",
      render: (group_name) => <span>{!group_name ? "-" : group_name}</span>,
    },
    {
      title: renderTitle("Số lượng"),
      dataIndex: "group_name",
      key: "group_name",
      render: (group_name) => <span>{!group_name ? "-" : group_name}</span>,
    },
    {
      title: renderTitle("Giá bán"),
      dataIndex: "group_name",
      key: "group_name",
      render: (group_name) => <span>{!group_name ? "-" : group_name}</span>,
    },
    {
      title: renderTitle("Mã đơn hàng"),
      render: (obj) =>
        !obj.event_name ? (
          "-"
        ) : (
          <Tooltip title={obj.event_name}>
            <span
              onClick={(e) => {
                e.stopPropagation();
                history.push(`/detailAgency/${obj.event_id}`);
              }}
            >
              {obj.event_name}
            </span>
          </Tooltip>
        ),
    },
    {
      title: renderTitle("Ngày giao hàng"),
      render: (obj) => (
        <span>
          {!obj.start_date || obj.start_date <= 0 || obj.status === 0
            ? "-"
            : FormatterDay.dateFormatWithString(obj.start_date, formatString)}
        </span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Danh sách sản phẩm đã mua
          </StyledITitle>
        </Col>
      </Row>
      <div style={{ marginTop: 45 }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={data.listEvent}
                style={{ width: "100%" }}
                defaultCurrent={1}
                sizeItem={data.size}
                indexPage={filter.page}
                maxpage={data.total}
                rowKey="rowTable"
                size="small"
                scroll={{ x: 0 }}
                loading={loadingTable}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilter({ ...filter, page: page });
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default ListProductPurchasedAgencyC1;
