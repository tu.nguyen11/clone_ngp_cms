import React, { useState, useEffect } from "react";
import { Row, Form, Col, DatePicker, message } from "antd";
import {
  ITitle,
  IButton,
  ISvg,
  IInput,
  ISelect,
  ISelectCity,
  IUpload,
} from "../../../components";
import { IDatePickerFrom } from "../../../components/common";
import { colors } from "../../../../assets";
import { StyledForm } from "../../../components/styled/form/styledForm";
import { ImageType } from "../../../../constant";
import { useParams } from "react-router-dom";
import locale from "antd/es/date-picker/locale/vi_VN";
import moment from "moment";

import {
  _getAPIListCounty,
  _getAPIListWard,
} from "../../../containers/hookcustom/ApiCustomHook";
import { APIService } from "../../../../services";

function CreateAgency(props) {
  const params = useParams();
  const [dataCities, setDataCities] = useState([
    {
      key: 0,
    },
  ]);
  const [dataWard, setDataWard] = useState([
    {
      key: 0,
    },
  ]);
  const [images, setImages] = useState("");
  const [add, setAdd] = useState(params.type == "add" ? true : false);
  const [url, setUrl] = useState("");
  const [dataBranch, setDataBranch] = useState([]);
  const [dataShopType, setDataShopType] = useState([]);
  const [keyIndustry, getKeyIndustry] = useState([]);
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;
  const dateFormat = "DD/MM/YYYY";
  const [dataDetail, setDataDetail] = useState({
    user_agency: {},
    debt_info: {},
  });

  const [cityId, setCityId] = useState(-1);
  const [districtId, setDistrictId] = useState(-1);
  const [wardId, setWardId] = useState(-1);

  const arrGender = [
    {
      key: 1,
      value: "Nam",
    },
    {
      key: 2,
      value: "Nữ",
    },
  ];

  useEffect(() => {
    _getAPIListBranch();
    if (!add) {
      _getAPIDetailAgency(params.id);
      return;
    }
  }, []);

  const _getAPIListShopType = async () => {
    try {
      const data = await APIService._getListShopType();
      const dataNew = data.shoptype.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setDataShopType(dataNew);
    } catch (error) {}
  };

  const getListKeyIndustry = async () => {
    try {
      const data = await APIService._getListKeyIndustry();
      const dataNew = data.list_type_main.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      getKeyIndustry(dataNew);
    } catch (error) {}
  };

  const _getAPIListBranch = async () => {
    try {
      const data = await APIService._getBranchDropList();
      const dataNew = data.agency.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setDataBranch(dataNew);
      _getAPIListShopType();
      getListKeyIndustry();
    } catch (error) {}
  };

  const _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailAgencyS1(idAgency);
      props.form.setFieldsValue({
        agency_id: data.user_agency.agency_id,
        board_name: data.user_agency.board_name,
        address: data.user_agency.address,
        shop_name: data.user_agency.shop_name,
        bussiness_tax_code: data.user_agency.bussiness_tax_code,
        business_type_id: data.user_agency.business_type_id,
        city_id: data.user_agency.cities_id,
        cmnd_city: data.user_agency.cmnd_city_name,
        cmnd_code: data.user_agency.cmnd_code,
        cmnd_create_date: data.user_agency.cmnd_create_date,
        create_date_contract: data.user_agency.cmnd_valid_date_from,
        end_date_contract: data.user_agency.cmnd_valid_date_to,
        company_name: data.user_agency.company_name,
        contract_code: data.user_agency.contract_number,
        district_id: data.user_agency.district_id,
        id: data.user_agency.id,
        avatar: data.user_agency.image,
        phone: data.user_agency.phone,
        direct_branches_id: data.user_agency.direct_branches_id,
        ward_id: data.user_agency.ward_id,
        bussiness_name: data.user_agency.representative,
        password: "",
        status: data.user_agency.status,
        birth_date: data.user_agency.birth_date,
        email: data.user_agency.email,
        code: data.user_agency.code,
        shop_type_id: data.user_agency.business_type_id,
        type_main_id:
          data.user_agency.type_main_id === 0
            ? undefined
            : data.user_agency.type_main_id,
        gender: data.user_agency.gender,
      });
      setCityId(data.user_agency.cities_id);
      setDistrictId(data.user_agency.district_id);
      setWardId(data.user_agency.ward_id);
      setDataDetail(data);
      setUrl(data.image_url);
      _getAPIListCounty(reduxForm.getFieldValue("city_id"), "", (data) => {
        setDataCities(data);
      });
      _getAPIListWard(reduxForm.getFieldValue("district_id"), "", (data) => {
        setDataWard(data);
      });
    } catch (error) {
      console.log(error);
    }
  };

  async function _postAPIAddNewAgencyS1(obj) {
    try {
      const data = await APIService._postAddNewAgencyS1(obj);
      message.success("Tạo đại lý cấp 1 thành công");
      props.history.push("/agencyPage");
    } catch (error) {}
  }

  async function _postAPIUpdateAgencyS1(obj) {
    try {
      const data = await APIService._postUpdateAgencyS1(obj);
      message.success("Cập nhập thành công");
      props.history.goBack();
    } catch (error) {}
  }

  const onSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields(
      {
        force: true,
      },
      (err, values) => {
        if (err) {
          return;
        }

        let birth_date;
        if (typeof values.birth_date === "string") {
          let date = values.birth_date.replace(/\s+/g, "");

          let dateSplit = date.split("-");
          let dateFormat = [dateSplit[1], dateSplit[0], dateSplit[2]].join("-");
          birth_date = new Date(dateFormat).getTime();
        } else {
          birth_date = values.birth_date;
        }

        const objAdd = {
          shop_name: values.shop_name,
          address: values.address,
          phone: values.phone,
          password: values.password,
          contract_code: !values.contract_code ? "" : values.contract_code,
          company_name: !values.company_name ? "" : values.company_name,
          bussiness_name: values.bussiness_name,
          bussiness_tax_code: !values.bussiness_tax_code
            ? ""
            : values.bussiness_tax_code,
          cmnd_code: !values.cmnd_code ? "" : values.cmnd_code,
          cmnd_city: !values.cmnd_city ? "" : values.cmnd_city,
          cmnd_create_date: !values.cmnd_create_date
            ? 0
            : values.cmnd_create_date,
          create_date_contract: !values.create_date_contract
            ? 0
            : values.create_date_contract,
          end_date_contract: !values.end_date_contract
            ? 0
            : values.end_date_contract,
          city_id: values.city_id,
          district_id: values.district_id,
          ward_id: values.ward_id,
          agency_id: !values.agency_id ? 0 : values.agency_id,
          image: !values.avatar ? "" : values.avatar,
          level: "",
          shop_type_id: values.shop_type_id,
          status: 1,
          gender: values.gender,
          type_main_id: values.type_main_id,
          birth_date: !values.birth_date ? 0 : birth_date,
          email: !values.email ? "" : values.email,
          board_name: values.board_name,
        };

        const objEdit = {
          id: params.id,
          shop_name: values.shop_name,
          address: values.address,
          phone: values.phone,
          password: values.password,
          contract_code: !values.contract_code ? "" : values.contract_code,
          company_name: !values.company_name ? "" : values.company_name,
          bussiness_name: values.bussiness_name,
          bussiness_tax_code: !values.bussiness_tax_code
            ? ""
            : values.bussiness_tax_code,
          cmnd_code: !values.cmnd_code ? "" : values.cmnd_code,
          cmnd_city: !values.cmnd_city ? "" : values.cmnd_city,
          cmnd_create_date: !values.cmnd_create_date
            ? 0
            : values.cmnd_create_date,
          create_date_contract: values.create_date_contract,
          end_date_contract: values.end_date_contract,
          // city_id: values.city_id,
          district_id: values.district_id,
          ward_id: values.ward_id,
          agency_id: !values.agency_id ? 0 : values.agency_id,
          image: !values.avatar ? "" : values.avatar,
          level: "",
          shop_type_id: values.shop_type_id,
          status: 1,
          gender: values.gender,
          type_main_id: !values.type_main_id ? 0 : values.type_main_id,
          birth_date: !values.birth_date ? 0 : birth_date,
          email: !values.email ? "" : values.email,
          board_name: values.board_name,
        };

        add ? _postAPIAddNewAgencyS1(objAdd) : _postAPIUpdateAgencyS1(objEdit);
      }
    );
  };
  return (
    <div
      style={{
        width: "100%",
      }}
    >
      <Row gutter={[12, 16]}>
        <Col>
          <ITitle
            level={1}
            title={
              params.type === "edit"
                ? "Chỉnh sửa đại lý cấp 1"
                : "Tạo đại lý cấp 1 mới"
            }
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />{" "}
        </Col>{" "}
        <Col>
          <StyledForm onSubmit={onSubmit}>
            <Row gutter={[12, 20]}>
              <Col>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    icon={ISvg.NAME.SAVE}
                    title="Lưu"
                    color={colors.main}
                    onClick={onSubmit}
                  />{" "}
                  <IButton
                    icon={ISvg.NAME.CROSS}
                    title="Hủy"
                    onClick={() => props.history.goBack()}
                    style={{
                      margin: "0px 0px 0px 20px",
                    }}
                    color={colors.oranges}
                  />{" "}
                </div>{" "}
              </Col>{" "}
              <Col>
                <div
                  style={{
                    maxWidth: 1200,
                    minWidth: 815,
                    height: "100%",
                    // background: colors.white,
                  }}
                >
                  <Row
                    gutter={[25, 0]}
                    style={{
                      height: "100%",
                    }}
                    type="flex"
                  >
                    <Col span={18} style={{}}>
                      <div
                        style={{
                          background: colors.white,
                          boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        }}
                      >
                        <Row>
                          <Col span={16}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col
                                  style={{
                                    marginTop: 6,
                                  }}
                                >
                                  <ITitle
                                    level={2}
                                    title="Thông tin đại lý"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />{" "}
                                </Col>{" "}
                                {add ? (
                                  <>
                                    <Col span={24}>
                                      <Form.Item>
                                        {" "}
                                        {getFieldDecorator("shop_name", {
                                          rules: [
                                            {
                                              required: add ? true : false,
                                              message:
                                                "nhập tên cửa hàng đại lý",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Tên đại lý{" "}
                                              {add ? (
                                                <span
                                                  style={{
                                                    color: "red",
                                                    fontSize: 14,
                                                  }}
                                                >
                                                  *
                                                </span>
                                              ) : null}{" "}
                                            </p>{" "}
                                            <IInput
                                              disabled={add ? false : true}
                                              loai="input"
                                              placeholder="nhập tên cửa hàng đại lý"
                                              value={reduxForm.getFieldValue(
                                                "shop_name"
                                              )}
                                            />{" "}
                                          </div>
                                        )}{" "}
                                      </Form.Item>{" "}
                                    </Col>{" "}
                                    <Col span={24}>
                                      <Form.Item>
                                        {getFieldDecorator("board_name", {
                                          rules: [
                                            {
                                              required: false,
                                              message: "nhập tên bảng hiệu",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Tên bảng hiệu
                                            </p>
                                            <IInput
                                              loai="input"
                                              placeholder="nhập tên bảng hiệu"
                                              value={reduxForm.getFieldValue(
                                                "board_name"
                                              )}
                                            />
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col span={24}>
                                      <Row gutter={[12, 6]}>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("phone", {
                                              rules: [
                                                {
                                                  required: add ? true : false,
                                                  pattern: new RegExp(
                                                    /0[0-9]([0-9]{8})\b/
                                                  ),
                                                  message:
                                                    "nhập tối đa 10 số, không được nhập chữ, khoảng cách và ký tự đặc biệt",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Số điện thoại đăng nhập{" "}
                                                  {add ? (
                                                    <span
                                                      style={{
                                                        color: "red",
                                                        fontSize: 14,
                                                      }}
                                                    >
                                                      *
                                                    </span>
                                                  ) : null}
                                                </p>
                                                <IInput
                                                  style={{
                                                    marginTop: 1,
                                                  }}
                                                  disabled={add ? false : true}
                                                  loai="input"
                                                  maxLength={10}
                                                  onKeyPress={(e) => {
                                                    console.log(e.target.value);
                                                    var char =
                                                      String.fromCharCode(
                                                        e.which
                                                      );
                                                    if (!/[0-9]/.test(char)) {
                                                      e.preventDefault();
                                                    }
                                                  }}
                                                  placeholder="nhập số điện thoại"
                                                  value={reduxForm.getFieldValue(
                                                    "phone"
                                                  )}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("password", {
                                              rules: [
                                                {
                                                  required: add,
                                                  type: "string",
                                                  message:
                                                    "mật khẩu không được để trống và tối thiểu 6 ký tự, tối đa 32 ký tự, không dấu ",
                                                  min: 6,
                                                  max: 32,
                                                  pattern: new RegExp(
                                                    "^[a-zA-Z0-9\\s!@#$%^&*()-_=+`~]*$"
                                                  ),
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Mật khẩu{" "}
                                                  {add ? (
                                                    <span
                                                      style={{
                                                        color: "red",
                                                        fontSize: 14,
                                                      }}
                                                    >
                                                      *
                                                    </span>
                                                  ) : null}
                                                </p>
                                                <IInput
                                                  style={{
                                                    marginTop: 1,
                                                  }}
                                                  loai="input"
                                                  placeholder="nhập mật khẩu"
                                                  value={reduxForm.getFieldValue(
                                                    "password"
                                                  )}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                      </Row>
                                    </Col>
                                    <Col span={24}>
                                      <Row gutter={[12, 6]}>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("gender", {
                                              rules: [
                                                {
                                                  required: add ? true : false,
                                                  message: "chọn giới tính",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Giới tính
                                                  {add ? (
                                                    <span
                                                      style={{
                                                        color: "red",
                                                        fontSize: 14,
                                                      }}
                                                    >
                                                      *
                                                    </span>
                                                  ) : null}
                                                </p>
                                                <ISelect
                                                  className="clear-pad"
                                                  data={arrGender}
                                                  style={{
                                                    background: colors.white,
                                                    borderStyle: "solid",
                                                    borderWidth: 0,
                                                    borderColor: colors.line,
                                                    borderBottomWidth: 1,
                                                  }}
                                                  type="write"
                                                  select={true}
                                                  placeholder="chọn giới tính"
                                                  isBackground={false}
                                                  onChange={(key) => {
                                                    reduxForm.setFieldsValue({
                                                      gender: key,
                                                    });
                                                  }}
                                                  value={reduxForm.getFieldValue(
                                                    "gender"
                                                  )}
                                                  // value={1}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("birth_date", {
                                              rules: [
                                                {
                                                  required: true,
                                                  message: "nhập ngày sinh",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Ngày sinh{" "}
                                                  {add ? (
                                                    <span
                                                      style={{
                                                        color: "red",
                                                        fontSize: 14,
                                                      }}
                                                    >
                                                      *
                                                    </span>
                                                  ) : null}
                                                </p>
                                                <IDatePickerFrom
                                                  showTime={false}
                                                  // locale={locale}
                                                  inputReadOnly={false}
                                                  isBorderBottom={true}
                                                  format={"DD-MM-YYYY"}
                                                  paddingInput="0px"
                                                  style={{
                                                    marginTop: 8,
                                                  }}
                                                  placeholder="nhập ngày sinh"
                                                  onChange={(date) => {
                                                    reduxForm.setFieldsValue({
                                                      birth_date:
                                                        date._d.getTime(),
                                                    });
                                                  }}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                      </Row>
                                    </Col>
                                    <Col span={24}>
                                      <Form.Item>
                                        {getFieldDecorator("address", {
                                          rules: [
                                            {
                                              required: true,
                                              message: "nhập địa chỉ đại lý",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Địa chỉ{" "}
                                              <span
                                                style={{
                                                  color: "red",
                                                  fontSize: 14,
                                                }}
                                              >
                                                *
                                              </span>
                                            </p>
                                            <IInput
                                              loai="input"
                                              placeholder="nhập địa chỉ đại lý"
                                              value={reduxForm.getFieldValue(
                                                "address"
                                              )}
                                            />
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col span={24}>
                                      <Form.Item>
                                        {getFieldDecorator("email", {
                                          rules: [
                                            {
                                              required: true,
                                              type: "email",
                                              message: "nhập email",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Email{" "}
                                              <span
                                                style={{
                                                  color: "red",
                                                  fontSize: 14,
                                                }}
                                              >
                                                *
                                              </span>
                                            </p>
                                            <IInput
                                              loai="input"
                                              placeholder="nhập email"
                                              value={reduxForm.getFieldValue(
                                                "email"
                                              )}
                                            />
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </>
                                ) : (
                                  <>
                                    <Col span={24}>
                                      <Form.Item>
                                        {getFieldDecorator("shop_name", {
                                          rules: [
                                            {
                                              required: true,
                                              message:
                                                "nhập tên cửa hàng đại lý",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Tên đại lý{" "}
                                              <span
                                                style={{
                                                  color: "red",
                                                  fontSize: 14,
                                                }}
                                              >
                                                *
                                              </span>
                                            </p>
                                            <IInput
                                              loai="input"
                                              placeholder="nhập tên cửa hàng đại lý"
                                              value={reduxForm.getFieldValue(
                                                "shop_name"
                                              )}
                                            />
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col span={24}>
                                      <Form.Item>
                                        {getFieldDecorator("board_name", {
                                          rules: [
                                            {
                                              required: false,
                                              message: "nhập tên bảng hiệu",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Tên bảng hiệu
                                            </p>
                                            <IInput
                                              loai="input"
                                              placeholder="nhập tên bảng hiệu"
                                              value={reduxForm.getFieldValue(
                                                "board_name"
                                              )}
                                            />
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col span={24}>
                                      <Form.Item>
                                        {getFieldDecorator("code", {
                                          rules: [
                                            {
                                              required: add ? true : false,
                                              message: "nhập mã đại lý",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Mã đại lý{" "}
                                              {add ? (
                                                <span
                                                  style={{
                                                    color: "red",
                                                    fontSize: 14,
                                                  }}
                                                >
                                                  *
                                                </span>
                                              ) : null}
                                            </p>
                                            <IInput
                                              style={{
                                                marginTop: 1,
                                              }}
                                              disabled={true}
                                              loai="input"
                                              placeholder="nhập mã đại lý"
                                              value={reduxForm.getFieldValue(
                                                "code"
                                              )}
                                            />
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col span={24}>
                                      <Row gutter={[12, 6]}>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("phone", {
                                              rules: [
                                                {
                                                  required: false,
                                                  pattern: new RegExp(
                                                    /0[0-9]([0-9]{8})\b/
                                                  ),
                                                  message:
                                                    "nhập tối đa 10 số, không được nhập chữ, khoảng cách và ký tự đặc biệt",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Số điện thoại đăng nhập
                                                </p>
                                                <IInput
                                                  style={{
                                                    marginTop: 1,
                                                  }}
                                                  disabled={true}
                                                  loai="input"
                                                  placeholder="nhập số điện thoại"
                                                  value={reduxForm.getFieldValue(
                                                    "phone"
                                                  )}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("password", {
                                              rules: [
                                                {
                                                  required: add,
                                                  type: "string",
                                                  message:
                                                    "mật khẩu không được để trống và tối thiểu 6 ký tự, tối đa 32 ký tự, không dấu ",
                                                  min: 6,
                                                  max: 32,
                                                  pattern: new RegExp(
                                                    "^[a-zA-Z0-9\\s!@#$%^&*()-_=+`~]*$"
                                                  ),
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Mật khẩu
                                                </p>
                                                <IInput
                                                  style={{
                                                    marginTop: 1,
                                                  }}
                                                  loai="input"
                                                  disabled={true}
                                                  placeholder="nhập mật khẩu"
                                                  // value={reduxForm.getFieldValue(
                                                  //   "password"
                                                  // )}
                                                  value="******"
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                      </Row>
                                    </Col>
                                    <Col span={24}>
                                      <Row gutter={[12, 12]}>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("gender", {
                                              rules: [
                                                {
                                                  required: true,
                                                  message: "chọn giới tính",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Giới tính{" "}
                                                  <span
                                                    style={{
                                                      color: "red",
                                                      fontSize: 14,
                                                    }}
                                                  >
                                                    *
                                                  </span>
                                                </p>
                                                <ISelect
                                                  className="clear-pad"
                                                  data={arrGender}
                                                  style={{
                                                    background: colors.white,
                                                    borderStyle: "solid",
                                                    borderWidth: 0,
                                                    borderColor: colors.line,
                                                    borderBottomWidth: 1,
                                                  }}
                                                  type="write"
                                                  select={true}
                                                  placeholder="chọn giới tính"
                                                  isBackground={false}
                                                  onChange={(key) => {
                                                    reduxForm.setFieldsValue({
                                                      // gender_id: key,
                                                      gender: key,
                                                    });
                                                  }}
                                                  value={
                                                    !reduxForm.getFieldValue(
                                                      "gender"
                                                    )
                                                      ? undefined
                                                      : reduxForm.getFieldValue(
                                                          "gender"
                                                        )
                                                  }
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("birth_date", {
                                              rules: [
                                                {
                                                  required: true,
                                                  message: "nhập ngày sinh",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Ngày sinh{" "}
                                                  <span
                                                    style={{
                                                      color: "red",
                                                      fontSize: 14,
                                                    }}
                                                  >
                                                    *
                                                  </span>
                                                </p>
                                                <IDatePickerFrom
                                                  showTime
                                                  isBorderBottom={true}
                                                  format={"DD-MM-YYYY"}
                                                  paddingInput="0px"
                                                  style={{
                                                    marginTop: 8,
                                                  }}
                                                  placeholder="nhập ngày sinh"
                                                  value={
                                                    reduxForm.getFieldValue(
                                                      "birth_date"
                                                    ) === 0 ||
                                                    reduxForm.getFieldValue(
                                                      "birth_date"
                                                    ) === null
                                                      ? undefined
                                                      : moment(
                                                          new Date(
                                                            reduxForm.getFieldValue(
                                                              "birth_date"
                                                            )
                                                          ),
                                                          "DD-MM-YYYY"
                                                        )
                                                  }
                                                  onChange={(date) => {
                                                    reduxForm.setFieldsValue({
                                                      birth_date:
                                                        date._d.getTime(),
                                                    });
                                                  }}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                      </Row>
                                    </Col>
                                    <Col span={24}>
                                      <Form.Item>
                                        {getFieldDecorator("address", {
                                          rules: [
                                            {
                                              required: true,
                                              message: "nhập địa chỉ đại lý",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Địa chỉ{" "}
                                              <span
                                                style={{
                                                  color: "red",
                                                  fontSize: 14,
                                                }}
                                              >
                                                *
                                              </span>
                                            </p>
                                            <IInput
                                              loai="input"
                                              placeholder="nhập địa chỉ đại lý"
                                              value={reduxForm.getFieldValue(
                                                "address"
                                              )}
                                            />
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col span={24}>
                                      <Form.Item>
                                        {getFieldDecorator("email", {
                                          rules: [
                                            {
                                              required: true,
                                              type: "email",
                                              message: "nhập email",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <p
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            >
                                              Email{" "}
                                              <span
                                                style={{
                                                  color: "red",
                                                  fontSize: 14,
                                                }}
                                              >
                                                *
                                              </span>
                                            </p>
                                            <IInput
                                              style={{
                                                marginTop: 1,
                                              }}
                                              loai="input"
                                              placeholder="nhập email"
                                              value={reduxForm.getFieldValue(
                                                "email"
                                              )}
                                            />
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </>
                                )}
                              </Row>
                              <div
                                style={{
                                  marginTop: 42,
                                }}
                              >
                                <Row gutter={[12, 20]}>
                                  <Col
                                    style={{
                                      marginTop: 6,
                                    }}
                                  >
                                    <ITitle
                                      level={2}
                                      title="Thông tin hợp đồng"
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    />
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={12}>
                                        <Form.Item>
                                          {getFieldDecorator("contract_code", {
                                            rules: [
                                              {
                                                required: false,
                                                message:
                                                  "nhập số ghi trên hợp đồng",
                                              },
                                            ],
                                          })(
                                            <div>
                                              <p
                                                style={{
                                                  fontWeight: 600,
                                                }}
                                              >
                                                Số hợp đồng
                                              </p>
                                              <IInput
                                                placeholder="nhập số ghi trên hợp đồng"
                                                value={reduxForm.getFieldValue(
                                                  "contract_code"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                      <Col span={12}>
                                        <Form.Item>
                                          {getFieldDecorator("company_name", {
                                            rules: [
                                              {
                                                required: false,
                                                message: "nhập tên công ty",
                                              },
                                            ],
                                          })(
                                            <div>
                                              <p
                                                style={{
                                                  fontWeight: 600,
                                                }}
                                              >
                                                Tên công ty
                                              </p>
                                              <IInput
                                                placeholder="nhập tên công ty"
                                                value={reduxForm.getFieldValue(
                                                  "company_name"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={12}>
                                        <Form.Item>
                                          {getFieldDecorator("bussiness_name", {
                                            rules: [
                                              {
                                                required: true,
                                                message:
                                                  "nhập tên người đại diện",
                                              },
                                            ],
                                          })(
                                            <div>
                                              <p
                                                style={{
                                                  fontWeight: 600,
                                                }}
                                              >
                                                Người đại diện{" "}
                                                <span
                                                  style={{
                                                    color: "red",
                                                    fontSize: 14,
                                                  }}
                                                >
                                                  *
                                                </span>
                                              </p>
                                              <IInput
                                                placeholder="nhập tên người đại diện"
                                                value={reduxForm.getFieldValue(
                                                  "bussiness_name"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                      <Col span={12}>
                                        <Form.Item>
                                          {getFieldDecorator(
                                            "bussiness_tax_code",
                                            {
                                              rules: [
                                                {
                                                  required: false,
                                                  message:
                                                    "nhập mã số ĐKKD/MST công ty",
                                                },
                                              ],
                                            }
                                          )(
                                            <div>
                                              <p
                                                style={{
                                                  fontWeight: 600,
                                                }}
                                              >
                                                Mã số thuế
                                              </p>
                                              <IInput
                                                placeholder="nhập mã số ĐKKD/MST công ty"
                                                value={reduxForm.getFieldValue(
                                                  "bussiness_tax_code"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={12}>
                                        <Form.Item>
                                          {getFieldDecorator("cmnd_code", {
                                            rules: [
                                              {
                                                required: false,
                                                message:
                                                  "nhập số CMND đại diện",
                                              },
                                            ],
                                          })(
                                            <div>
                                              <p
                                                style={{
                                                  fontWeight: 600,
                                                }}
                                              >
                                                CMND
                                              </p>
                                              <IInput
                                                placeholder="nhập số CMND đại diện"
                                                value={reduxForm.getFieldValue(
                                                  "cmnd_code"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                      <Col span={12}>
                                        <Form.Item>
                                          {getFieldDecorator("cmnd_city", {
                                            rules: [
                                              {
                                                required: false,
                                                message:
                                                  "nhập nơi cấp giấy phép ĐKKD",
                                              },
                                            ],
                                          })(
                                            <div>
                                              <p
                                                style={{
                                                  fontWeight: 600,
                                                }}
                                              >
                                                Nơi cấp
                                              </p>
                                              <IInput
                                                placeholder="nhập nơi cấp giấy phép ĐKKD"
                                                value={reduxForm.getFieldValue(
                                                  "cmnd_city"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <div
                                        style={{
                                          display: "flex",
                                          flexDirection: "row",
                                        }}
                                      >
                                        {/* <Col>
                                              <Row> */}
                                        {/* <Col span={12}> */}
                                        <div
                                          style={{
                                            flex: 1,
                                            paddingLeft: 5,
                                            paddingRight: 5,
                                          }}
                                        >
                                          <Form.Item>
                                            {getFieldDecorator(
                                              "cmnd_create_date",
                                              {
                                                rules: [
                                                  {
                                                    required: false,
                                                    message:
                                                      "nhập số CMND đại diện",
                                                  },
                                                ],
                                              }
                                            )(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Ngày cấp
                                                </p>
                                                {add ? (
                                                  <DatePicker
                                                    // allowClear={false}
                                                    className="clear-pad"
                                                    locale={locale}
                                                    showTime
                                                    suffixIcon={() => null}
                                                    placeholder="nhập ngày cấp"
                                                    size="large"
                                                    style={{
                                                      width: "100%",
                                                      marginTop: 10,
                                                      background: colors.white,
                                                      borderStyle: "solid",
                                                      borderWidth: 0,
                                                      borderColor: colors.line,
                                                      borderBottomWidth: 1,
                                                    }}
                                                    onChange={(
                                                      date,
                                                      dateString
                                                    ) => {
                                                      reduxForm.setFieldsValue({
                                                        cmnd_create_date:
                                                          Date.parse(
                                                            dateString
                                                          ),
                                                      });
                                                    }}
                                                  />
                                                ) : (
                                                  <DatePicker
                                                    // allowClear={false}
                                                    className="clear-pad"
                                                    locale={locale}
                                                    showTime
                                                    suffixIcon={() => null}
                                                    placeholder="nhập ngày cấp"
                                                    size="large"
                                                    style={{
                                                      width: "100%",
                                                      marginTop: 10,
                                                      background: colors.white,
                                                      borderStyle: "solid",
                                                      borderWidth: 0,
                                                      borderColor: colors.line,
                                                      borderBottomWidth: 1,
                                                    }}
                                                    onChange={(
                                                      date,
                                                      dateString
                                                    ) => {
                                                      reduxForm.setFieldsValue({
                                                        cmnd_create_date:
                                                          Date.parse(
                                                            dateString
                                                          ),
                                                      });
                                                    }}
                                                    value={
                                                      add
                                                        ? undefined
                                                        : reduxForm.getFieldValue(
                                                            "cmnd_create_date"
                                                          ) < 3600
                                                        ? undefined
                                                        : moment(
                                                            new Date(
                                                              reduxForm.getFieldValue(
                                                                "cmnd_create_date"
                                                              )
                                                            ),
                                                            dateFormat
                                                          )
                                                    }
                                                  />
                                                )}
                                              </div>
                                            )}
                                          </Form.Item>
                                        </div>
                                        {/* </Col>
                                              </Row> */}
                                        {/* </Col> */}
                                        {/* <Col>
                                              <Row gutter={[12, 12]}> */}
                                        {/* <Col span={12}> */}
                                        {/* <div
                                          style={{
                                            flex: 1,
                                            paddingLeft: 5,
                                            paddingRight: 5,
                                          }}
                                        >
                                          <Form.Item>
                                            {getFieldDecorator(
                                              "create_date_contract",
                                              {
                                                rules: [
                                                  {
                                                    required: false,
                                                    message:
                                                      "nhập ngày kí kết  ",
                                                  },
                                                ],
                                              }
                                            )(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Ngày hiệu lực
                                                </p>
                                                {add ? (
                                                  <DatePicker
                                                    allowClear={false}
                                                    className="clear-pad"
                                                    locale={locale}
                                                    showTime
                                                    suffixIcon={() => null}
                                                    placeholder="nhập ngày kí kết"
                                                    size="large"
                                                    style={{
                                                      width: "100%",
                                                      marginTop: 10,
                                                      background: colors.white,
                                                      borderStyle: "solid",
                                                      borderWidth: 0,
                                                      borderColor: colors.line,
                                                      borderBottomWidth: 1,
                                                    }}
                                                    onChange={(
                                                      date,
                                                      dateString
                                                    ) => {
                                                      reduxForm.setFieldsValue({
                                                        create_date_contract: Date.parse(
                                                          dateString
                                                        ),
                                                      });
                                                    }}
                                                  />
                                                ) : (
                                                  <DatePicker
                                                    allowClear={false}
                                                    className="clear-pad"
                                                    locale={locale}
                                                    showTime
                                                    suffixIcon={() => null}
                                                    placeholder="nhập ngày kí kết"
                                                    size="large"
                                                    style={{
                                                      width: "100%",
                                                      marginTop: 10,
                                                      background: colors.white,
                                                      borderStyle: "solid",
                                                      borderWidth: 0,
                                                      borderColor: colors.line,
                                                      borderBottomWidth: 1,
                                                    }}
                                                    onChange={(
                                                      date,
                                                      dateString
                                                    ) => {
                                                      reduxForm.setFieldsValue({
                                                        create_date_contract: Date.parse(
                                                          dateString
                                                        ),
                                                      });
                                                    }}
                                                    value={
                                                      add
                                                        ? null
                                                        : reduxForm.getFieldValue(
                                                            "create_date_contract"
                                                          ) === null ||
                                                          !reduxForm.getFieldValue(
                                                            "create_date_contract"
                                                          )
                                                        ? undefined
                                                        : moment(
                                                            new Date(
                                                              reduxForm.getFieldValue(
                                                                "create_date_contract"
                                                              )
                                                            ),
                                                            dateFormat
                                                          )
                                                    }
                                                  />
                                                )}
                                              </div>
                                            )}
                                          </Form.Item>
                                        </div> */}
                                        {/* </Col> */}
                                        {/* </Row>
                                            </Col> */}
                                      </div>
                                      {/* <Col>
                                        <Row gutter={[12, 12]}>
                                          <Col span={12}>
                                            <Form.Item>
                                              {getFieldDecorator(
                                                "end_date_contract",
                                                {
                                                  rules: [
                                                    {
                                                      required: false,
                                                      message:
                                                        "nhập ngày chấm dứt",
                                                    },
                                                  ],
                                                }
                                              )(
                                                <div>
                                                  <p
                                                    style={{
                                                      fontWeight: 600,
                                                    }}
                                                  >
                                                    Ngày hết hiệu lực
                                                  </p>
                                                  {add ? (
                                                    <DatePicker
                                                      allowClear={false}
                                                      className="clear-pad"
                                                      locale={locale}
                                                      showTime
                                                      suffixIcon={() => null}
                                                      placeholder="nhập ngày chấm dứt"
                                                      size="large"
                                                      style={{
                                                        width: "100%",
                                                        marginTop: 10,
                                                        background:
                                                          colors.white,
                                                        borderStyle: "solid",
                                                        borderWidth: 0,
                                                        borderColor:
                                                          colors.line,
                                                        borderBottomWidth: 1,
                                                      }}
                                                      onChange={(
                                                        date,
                                                        dateString
                                                      ) => {
                                                        reduxForm.setFieldsValue(
                                                          {
                                                            end_date_contract: Date.parse(
                                                              dateString
                                                            ),
                                                          }
                                                        );
                                                      }}
                                                    />
                                                  ) : (
                                                    <DatePicker
                                                      allowClear={false}
                                                      className="clear-pad"
                                                      locale={locale}
                                                      showTime
                                                      suffixIcon={() => null}
                                                      placeholder="nhập ngày chấm dứt"
                                                      size="large"
                                                      style={{
                                                        width: "100%",
                                                        marginTop: 10,
                                                        background:
                                                          colors.white,
                                                        borderStyle: "solid",
                                                        borderWidth: 0,
                                                        borderColor:
                                                          colors.line,
                                                        borderBottomWidth: 1,
                                                      }}
                                                      onChange={(
                                                        date,
                                                        dateString
                                                      ) => {
                                                        reduxForm.setFieldsValue(
                                                          {
                                                            end_date_contract: Date.parse(
                                                              dateString
                                                            ),
                                                          }
                                                        );
                                                      }}
                                                      value={
                                                        add
                                                          ? null
                                                          : reduxForm.getFieldValue(
                                                              "end_date_contract"
                                                            ) === null ||
                                                            !reduxForm.getFieldValue(
                                                              "end_date_contract"
                                                            )
                                                          ? undefined
                                                          : moment(
                                                              new Date(
                                                                reduxForm.getFieldValue(
                                                                  "end_date_contract"
                                                                )
                                                              ),
                                                              dateFormat
                                                            )
                                                      }
                                                    />
                                                  )}{" "}
                                                </div>
                                              )}{" "}
                                            </Form.Item>{" "}
                                          </Col>{" "}
                                        </Row>{" "}
                                      </Col>{" "}
                                    */}
                                    </Row>{" "}
                                  </Col>{" "}
                                </Row>{" "}
                              </div>{" "}
                            </div>{" "}
                          </Col>{" "}
                          <Col span={8}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col
                                  style={{
                                    marginTop: 6,
                                  }}
                                >
                                  <ITitle
                                    level={2}
                                    title="Khu vực"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col>
                                  <Form.Item>
                                    {getFieldDecorator("city_id", {
                                      rules: [
                                        {
                                          required: add ? true : false,
                                          message: "chọn tỉnh/TP",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        >
                                          Tỉnh / Thành Phố{" "}
                                          {add ? (
                                            <span
                                              style={{
                                                color: "red",
                                                fontSize: 14,
                                              }}
                                            >
                                              *
                                            </span>
                                          ) : null}
                                        </p>
                                        <ISelectCity
                                          className="clear-pad"
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                            height: 40,
                                          }}
                                          type="write"
                                          select={add ? true : false}
                                          placeholder="chọn tỉnh/TP"
                                          isBackground={false}
                                          onChange={(key) => {
                                            setCityId(key);
                                            setDistrictId(-1);
                                            setWardId(-1);
                                            reduxForm.setFieldsValue({
                                              city_id: key,
                                              district_id: undefined,
                                              ward_id: undefined,
                                            });
                                            _getAPIListCounty(
                                              reduxForm.getFieldValue(
                                                "city_id"
                                              ),
                                              "",
                                              (data) => {
                                                setDataCities(data);
                                                // reduxForm.setFieldsValue({
                                                //   district_agency: 0,
                                                // });
                                              }
                                            );
                                          }}
                                          value={reduxForm.getFieldValue(
                                            "city_id"
                                          )}
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                                <Col>
                                  <Form.Item>
                                    {getFieldDecorator("district_id", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn quận/huyện",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        >
                                          Quận / Huyện
                                          <span
                                            style={{
                                              color: "red",
                                              fontSize: 14,
                                            }}
                                          >
                                            *
                                          </span>
                                        </p>
                                        <ISelect
                                          data={dataCities}
                                          select={cityId === -1 ? false : true}
                                          onChange={(key) => {
                                            // if (
                                            //   reduxForm.getFieldValue(
                                            //     "district_id"
                                            //   ) == 543
                                            // ) {
                                            //   reduxForm.setFieldsValue({
                                            //     ward_id: 0,
                                            //   });
                                            // }
                                            reduxForm.setFieldsValue({
                                              district_id: key,
                                              ward_id: undefined,
                                            });
                                            setDistrictId(key);
                                            setWardId(-1);
                                            _getAPIListWard(
                                              reduxForm.getFieldValue(
                                                "district_id"
                                              ),
                                              "",
                                              (data) => {
                                                setDataWard(data);
                                                // reduxForm.setFieldsValue({
                                                //   ward_agency: 0,
                                                // });
                                              }
                                            );
                                          }}
                                          isBackground={false}
                                          placeholder="chọn quận/huyện"
                                          value={
                                            districtId === -1
                                              ? undefined
                                              : districtId
                                          }
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                                <Col>
                                  <Form.Item>
                                    {getFieldDecorator("ward_id", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn phường / xã",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        >
                                          Phường / Xã{" "}
                                          <span
                                            style={{
                                              color: "red",
                                              fontSize: 14,
                                            }}
                                          >
                                            *
                                          </span>
                                        </p>
                                        <ISelect
                                          className="clear-pad"
                                          data={dataWard}
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                          }}
                                          type="write"
                                          select={
                                            districtId === -1 ? false : true
                                          }
                                          placeholder="chọn phường / xã"
                                          isBackground={false}
                                          onChange={(key) => {
                                            reduxForm.setFieldsValue({
                                              ward_id: key,
                                            });
                                            setWardId(key);
                                          }}
                                          value={
                                            wardId === -1 ? undefined : wardId
                                          }
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                                <Col>
                                  <Form.Item>
                                    {getFieldDecorator("agency_id", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn phòng kinh doanh",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        >
                                          Phòng kinh doanh{" "}
                                          <span
                                            style={{
                                              color: "red",
                                              fontSize: 14,
                                            }}
                                          >
                                            *
                                          </span>
                                        </p>
                                        <ISelect
                                          className="clear-pad"
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                          }}
                                          type="write"
                                          select={true}
                                          placeholder="chọn phòng kinh doanh"
                                          isBackground={false}
                                          onChange={(key) => {
                                            reduxForm.setFieldsValue({
                                              agency_id: key,
                                            });
                                          }}
                                          data={dataBranch}
                                          value={
                                            !reduxForm.getFieldValue(
                                              "agency_id"
                                            )
                                              ? undefined
                                              : reduxForm.getFieldValue(
                                                  "agency_id"
                                                )
                                          }
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                                <div
                                  style={{
                                    marginTop: 42,
                                  }}
                                >
                                  <Row gutter={[12, 20]}>
                                    <Col
                                      style={{
                                        marginTop: 6,
                                      }}
                                    >
                                      <ITitle
                                        level={2}
                                        title="Hình ảnh đại lý"
                                        style={{
                                          fontWeight: "bold",
                                        }}
                                      />
                                    </Col>
                                    <Col>
                                      <Form.Item>
                                        {getFieldDecorator("avatar", {
                                          rules: [
                                            {
                                              required: false,
                                              message: "chọn hình",
                                            },
                                          ],
                                        })(
                                          <div>
                                            {add ? (
                                              <IUpload
                                                type={ImageType.USER}
                                                callback={(array) => {
                                                  setImages(array[0]);
                                                  reduxForm.setFieldsValue({
                                                    avatar: array[0],
                                                  });
                                                }}
                                              />
                                            ) : (
                                              <IUpload
                                                type={ImageType.USER}
                                                callback={(array) => {
                                                  setImages(array[0]);
                                                  reduxForm.setFieldsValue({
                                                    avatar: array[0],
                                                  });
                                                }}
                                                name={reduxForm.getFieldValue(
                                                  "avatar"
                                                )}
                                                url={
                                                  !reduxForm.getFieldValue(
                                                    "avatar"
                                                  )
                                                    ? null
                                                    : url
                                                }
                                              />
                                            )}{" "}
                                          </div>
                                        )}{" "}
                                      </Form.Item>{" "}
                                    </Col>{" "}
                                  </Row>{" "}
                                </div>{" "}
                              </Row>{" "}
                            </div>{" "}
                          </Col>{" "}
                        </Row>{" "}
                      </div>{" "}
                    </Col>{" "}
                    <Col
                      span={6}
                      style={{
                        background: colors.white,
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        padding: "30px 40px",
                      }}
                    >
                      <div style={{}}>
                        <Row gutter={[12, 20]}>
                          <Col
                            style={{
                              marginTop: 6,
                            }}
                          >
                            <ITitle
                              level={2}
                              title="Loại hình kinh doanh"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col>
                            <p
                              style={{
                                fontWeight: 600,
                              }}
                            >
                              Cấp đại lý
                            </p>
                            <span> Đại lý cấp 1 </span>
                          </Col>
                          <Col>
                            <Form.Item>
                              {getFieldDecorator("shop_type_id", {
                                rules: [
                                  {
                                    required: false,
                                    message: "chọn loại hình kinh doanh",
                                  },
                                ],
                              })(
                                <div>
                                  <p
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  >
                                    Loại hình kinh doanh{" "}
                                  </p>
                                  <ISelect
                                    data={dataShopType}
                                    select={true}
                                    isBackground={false}
                                    placeholder="chọn loại hình kinh doanh"
                                    onChange={(key) => {
                                      reduxForm.setFieldsValue({
                                        shop_type_id: key,
                                      });
                                    }}
                                    value={
                                      reduxForm.getFieldValue("shop_type_id") ==
                                      0
                                        ? reduxForm.getFieldValue(
                                            "shop_type_id" == 0
                                          )
                                        : reduxForm.getFieldValue(
                                            "shop_type_id"
                                          )
                                    }
                                  />
                                </div>
                              )}
                            </Form.Item>
                          </Col>
                          {/* {add ? ( */}
                          <Col>
                            <Form.Item>
                              {getFieldDecorator("type_main_id", {
                                rules: [
                                  {
                                    required: true,
                                    message: "chọn ngành hàng chủ lực",
                                  },
                                ],
                              })(
                                <div>
                                  <p
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  >
                                    Ngành hàng chủ lực{" "}
                                    <span
                                      style={{
                                        color: "red",
                                        fontSize: 14,
                                      }}
                                    >
                                      *
                                    </span>
                                  </p>
                                  <ISelect
                                    className="clear-pad"
                                    data={keyIndustry}
                                    style={{
                                      background: colors.white,
                                      borderStyle: "solid",
                                      borderWidth: 0,
                                      borderColor: colors.line,
                                      borderBottomWidth: 1,
                                    }}
                                    type="write"
                                    select={true}
                                    placeholder="chọn ngành hàng chủ lực"
                                    isBackground={false}
                                    onChange={(key) => {
                                      reduxForm.setFieldsValue({
                                        type_main_id: key,
                                      });
                                    }}
                                    value={
                                      reduxForm.getFieldValue(
                                        "type_main_id"
                                      ) === 0
                                        ? undefined
                                        : reduxForm.getFieldValue(
                                            "type_main_id"
                                          )
                                    }
                                  />
                                </div>
                              )}
                            </Form.Item>
                          </Col>
                          {/* ) : (
                            <Col>
                              <p
                                style={{
                                  fontWeight: 600,
                                }}
                              >
                                Ngành hàng chủ lực{" "}
                              </p>{" "}
                              <span>
                                {" "}
                                {!dataDetail.user_agency.type_main_name
                                  ? "-"
                                  : dataDetail.user_agency.type_main_name}{" "}
                              </span>{" "}
                            </Col>
                          )}{" "} */}
                          {add ? null : (
                            <Col>
                              <p
                                style={{
                                  fontWeight: 600,
                                }}
                              >
                                {" "}
                                Cấp bậc{" "}
                              </p>{" "}
                              <span>
                                {" "}
                                {!dataDetail.user_agency.membership
                                  ? "-"
                                  : dataDetail.user_agency.membership}{" "}
                              </span>{" "}
                            </Col>
                          )}{" "}
                        </Row>{" "}
                      </div>{" "}
                    </Col>{" "}
                  </Row>{" "}
                </div>{" "}
              </Col>{" "}
            </Row>{" "}
          </StyledForm>{" "}
        </Col>{" "}
      </Row>{" "}
    </div>
  );
}

const createAgency = Form.create({
  name: "CreateAgency",
})(CreateAgency);

export default createAgency;
