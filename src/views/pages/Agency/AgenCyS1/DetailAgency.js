import React, { useState, useEffect } from "react";
import { ITitle, IButton, ISvg } from "../../../components";
import { Row, Col, message, Modal, Skeleton } from "antd";
import { colors, images } from "../../../../assets";
import { useParams, Link } from "react-router-dom";
import { APIService } from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";
import ITransfer from "../../../components/styled/modalTransfer/ITransfer";
import { priceFormat } from "../../../../utils";
import { useHistory } from "react-router-dom";
import ModalAccount from "./ModalAccount";

export default function DetailAgency(props) {
  const paramsAgency = useParams();
  const history = useHistory();
  const toDate = new Date();

  const [isVisbleModal, setVisbleModal] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingButton, setIsLoadingButton] = useState(false);
  const [popupAccount, setPopupAccount] = useState(false);

  const [isLoadingRemove, setIsLoadingRemove] = useState(false);
  const [dataDetail, setDataDetail] = useState({
    user_agency: {},
    image_url: "",
    loading: false,
  });
  const [dataAgencyS2, setDataAgencyS2] = useState([]);
  useEffect(() => {
    _getAPIDetailAgency(paramsAgency.id);
  }, []);

  const _getAPIListS2InS1 = async () => {
    try {
      const data = await APIService._getListS2InS1(paramsAgency.id);
      let s2 = data.s2;
      s2.map((item, index) => {
        s2[index].stt = index + 1;
        s2[index].sttAndId = {
          id: item.id,
          stt: index,
        };
      });
      setDataAgencyS2(data.s2);
    } catch (error) {
      console.log(error);
    }
  };

  const _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailAgencyS1(idAgency);
      // _getAPIListS2InS1();
      setDataDetail(data);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const postUpdateUserAgencyDebt = async (obj) => {
    try {
      const data = await APIService._postUpdateUserAgencyDebt(obj);
      console.log("data-debt: ", data);
      setIsLoadingButton(false);
      setIsLoading(true);
      message.success("Cập nhật thành công");
      _getAPIDetailAgency(paramsAgency.id);
    } catch (error) {
      console.log(error);
      setIsLoadingButton(false);
    }
  };

  const postUpdateStatusAgencyC1 = async (obj, type_check) => {
    try {
      const data = await APIService._postUpdateStatusAgencyC1(obj);
      setIsLoadingButton(false);
      setIsLoading(true);
      message.success(
        type_check === 1 ? "Duyệt thành công" : "Kích hoạt thành công"
      );
      _getAPIDetailAgency(paramsAgency.id);
    } catch (error) {
      console.log(error);
      setIsLoadingButton(false);
    }
  };

  const postDeActiveUserAgencyC1 = async (obj) => {
    try {
      const data = await APIService._postDeActiveUserAgencyC1(obj);
      setIsLoadingButton(false);
      setIsLoading(true);
      message.success("Tạm dừng thành công");
      _getAPIDetailAgency(paramsAgency.id);
    } catch (error) {
      console.log(error);
      setIsLoadingButton(false);
    }
  };

  const triggerDelete = async (objRemove) => {
    try {
      await APIService._updateStatususer_agencys(objRemove);
      setIsLoadingRemove(false);
      message.success("Xóa thành công");
      props.history.push("/agencyPage");
    } catch (error) {
      console.log(error);
      setIsLoadingRemove(false);
    }
  };

  const handleCloseModal = () => {
    setVisbleModal(false);
  };

  const { user_agency } = dataDetail;
  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 26]}>
        <div style={{ marginTop: 12 }}>
          <Col>
            <ITitle
              level={1}
              title="Chi tiết đại lý cấp 1"
              style={{ color: colors.main, fontWeight: "bold" }}
            />
          </Col>
          <Col>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              {user_agency.status === 1 ? (
                <div style={{ marginRight: 20 }}>
                  <IButton
                    title="Tạm dừng"
                    onClick={() => {
                      const obj = { id: paramsAgency.id, status: 3 };
                      postDeActiveUserAgencyC1(obj);
                    }}
                    color={colors.oranges}
                  />
                </div>
              ) : user_agency.status === 3 ? (
                <div style={{ marginRight: 20 }}>
                  <IButton
                    title="Kích hoạt"
                    onClick={() => {
                      const obj = { id: paramsAgency.id, status: 1 };
                      postUpdateStatusAgencyC1(obj, 2);
                    }}
                    color={colors.main}
                  />
                </div>
              ) : (
                <div style={{ marginRight: 20 }}>
                  <IButton
                    title="Duyệt"
                    onClick={() => {
                      if (
                        !user_agency.type_main_id ||
                        user_agency.type_main_id === "" ||
                        user_agency.type_main_id === 0
                      ) {
                        message.error(
                          "Vui lòng thêm ngành hàng chủ lực để duyệt!"
                        );
                      } else {
                        const obj = { id: paramsAgency.id, status: 1 };
                        postUpdateStatusAgencyC1(obj, 1);
                      }
                    }}
                    color={colors.main}
                  />
                </div>
              )}
              <IButton
                icon={ISvg.NAME.WRITE}
                title="Chỉnh sửa"
                onClick={() =>
                  props.history.push("/agencyS1/edit/" + paramsAgency.id)
                }
                color={colors.main}
              />
              {/* <Popconfirm
                placement="bottom"
                title={"Nhấn đồng ý để xóa ."}
                onConfirm={() => {
                  const objRemove = {
                    id: [Number(paramsAgency.id)],
                    status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                  };
                  setIsLoadingRemove(true);
                  triggerDelete(objRemove);
                }}
                okText="Đồng ý"
                cancelText="Hủy"
              >
                <IButton
                  icon={ISvg.NAME.DELETE}
                  title="Xóa"
                  color={colors.oranges}
                  loading={isLoadingRemove}
                />
              </Popconfirm>
             */}
            </div>
          </Col>
          <Col span={24} xxl={23}>
            <div
              style={{
                minWidth: 815,
                height: "100%",
                // background: colors.white,
              }}
            >
              <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
                <Col span={11}>
                  <div
                    style={{
                      background: colors.white,
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                    }}
                  >
                    <Row>
                      <Col span={14}>
                        <div
                          style={{
                            padding: "30px 40px",
                            background: colors.white,
                            borderRight: "1px solid #f0f0f0",
                          }}
                        >
                          {isLoading ? (
                            <div>
                              <Skeleton
                                loading={true}
                                active
                                paragraph={{ rows: 16 }}
                              />
                            </div>
                          ) : (
                            <Row gutter={[12, 20]}>
                              <Col>
                                <ITitle
                                  level={2}
                                  title="Thông tin đại lý"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col span={24}>
                                <p style={{ fontWeight: 600 }}>Tên đại lý</p>
                                <span style={{ wordWrap: "break-word" }}>
                                  {!user_agency.shop_name
                                    ? "-"
                                    : user_agency.shop_name}
                                </span>
                              </Col>
                              <Col span={24}>
                                <p style={{ fontWeight: 600 }}>Tên Bảng hiệu</p>
                                <span style={{ wordWrap: "break-word" }}>
                                  {!user_agency.board_name
                                    ? "-"
                                    : user_agency.board_name}
                                </span>
                              </Col>
                              <Col span={24}>
                                <p style={{ fontWeight: 600 }}>Mã đại lý</p>
                                <span style={{ wordBreak: "break-all" }}>
                                  {!user_agency.code ? "-" : user_agency.code}
                                </span>
                              </Col>
                              <Col span={24} style={{ marginTop: 8 }}>
                                <Row gutter={[12, 6]}>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>
                                      Số điện thoại đăng nhập
                                    </p>
                                    <span style={{ wordWrap: "break-word" }}>
                                      {!user_agency.phone
                                        ? "-"
                                        : user_agency.phone}
                                    </span>
                                  </Col>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>Mật khẩu</p>
                                    <span>******</span>
                                  </Col>
                                </Row>
                              </Col>

                              <Col span={24} style={{ marginTop: 8 }}>
                                <Row gutter={[12, 6]}>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>Giới tính</p>
                                    <span style={{ wordWrap: "break-word" }}>
                                      {!user_agency.gender_name
                                        ? "-"
                                        : user_agency.gender_name}
                                    </span>
                                  </Col>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>Ngày sinh</p>
                                    <span>
                                      {!user_agency.birth_date
                                        ? "-"
                                        : FormatterDay.dateFormatWithString(
                                            user_agency.birth_date,
                                            "#DD#/#MM#/#YYYY#"
                                          )}
                                    </span>
                                  </Col>
                                </Row>
                              </Col>

                              <Col span={24}>
                                <p style={{ fontWeight: 600 }}>Địa chỉ</p>
                                <span style={{ wordWrap: "break-word" }}>
                                  {!user_agency.address
                                    ? "-"
                                    : `${user_agency.address}, ${user_agency.ward_name}, ${user_agency.district_name}, ${user_agency.cities_name}`}
                                </span>
                              </Col>
                              <Col span={24}>
                                <p style={{ fontWeight: 600 }}>Email</p>
                                <span style={{ wordBreak: "break-all" }}>
                                  {!user_agency.email ? "-" : user_agency.email}
                                </span>
                              </Col>
                              <Col span={24}>
                                <p style={{ fontWeight: 600 }}>Trạng thái</p>
                                <span style={{ wordWrap: "break-word" }}>
                                  {!user_agency.status_name
                                    ? "-"
                                    : user_agency.status_name}
                                </span>
                              </Col>

                              <Col span={24} style={{ marginTop: 32 }}>
                                <ITitle
                                  level={2}
                                  title="Thông tin hợp đồng"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col span={24}>
                                <Row gutter={[12, 6]}>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>
                                      Số hợp đồng
                                    </p>
                                    <span>
                                      {!user_agency.contract_number
                                        ? "-"
                                        : user_agency.contract_number}
                                    </span>
                                  </Col>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>
                                      Tên công ty
                                    </p>
                                    <span>
                                      {!user_agency.company_name
                                        ? "-"
                                        : user_agency.company_name}
                                    </span>
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={24}>
                                <Row gutter={[12, 6]}>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>
                                      Người đại diện
                                    </p>
                                    <span>{user_agency.representative}</span>
                                  </Col>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>
                                      Mã số thuế
                                    </p>
                                    <span>
                                      {!user_agency.bussiness_tax_code
                                        ? "-"
                                        : user_agency.bussiness_tax_code}
                                    </span>
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={24}>
                                <Row gutter={[12, 6]}>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>CMND</p>
                                    <span>
                                      {!user_agency.cmnd_code
                                        ? "-"
                                        : user_agency.cmnd_code}
                                    </span>
                                  </Col>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>Nơi cấp</p>
                                    <span>
                                      {!user_agency.cmnd_city_name
                                        ? "-"
                                        : user_agency.cmnd_city_name}
                                    </span>
                                  </Col>
                                </Row>
                              </Col>

                              <Col span={24}>
                                <Row gutter={[12, 6]}>
                                  <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>Ngày cấp</p>
                                    <span>
                                      {user_agency.cmnd_create_date < 3600 ||
                                      !user_agency.cmnd_create_date // Bắt trường hợp ko tạo ngày cấp
                                        ? "-"
                                        : FormatterDay.dateFormatWithString(
                                            user_agency.cmnd_create_date,
                                            "#DD#/#MM#/#YYYY#"
                                          )}
                                    </span>
                                  </Col>
                                  {/* <Col span={12}>
                                    <p style={{ fontWeight: 600 }}>
                                      Ngày hiệu lực
                                    </p>
                                    <span>
                                      {!user_agency.cmnd_valid_date_from
                                        ? "-"
                                        : FormatterDay.dateFormatWithString(
                                            user_agency.cmnd_valid_date_from,
                                            "#DD#/#MM#/#YYYY#"
                                          )}
                                    </span>
                                  </Col> */}
                                </Row>
                              </Col>

                              {/* <Col span={24}>
                                <p style={{ fontWeight: 600 }}>
                                  Ngày hết hiệu lực
                                </p>
                                <span>
                                  {!user_agency.cmnd_valid_date_to
                                    ? "-"
                                    : FormatterDay.dateFormatWithString(
                                        user_agency.cmnd_valid_date_to,
                                        "#DD#/#MM#/#YYYY#"
                                      )}
                                </span>
                              </Col> */}
                            </Row>
                          )}
                        </div>
                      </Col>
                      <Col span={10}>
                        <div style={{ padding: "30px 40px" }}>
                          {isLoading ? (
                            <div>
                              <Skeleton
                                loading={true}
                                active
                                paragraph={{ rows: 16 }}
                              />
                            </div>
                          ) : (
                            <Row gutter={[12, 20]}>
                              <Col>
                                <ITitle
                                  level={2}
                                  title="Khu vực"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col style={{ marginTop: 8 }}>
                                <p style={{ fontWeight: 600 }}>
                                  Tỉnh/ Thành phố
                                </p>
                                <span>
                                  {!user_agency.cities_name
                                    ? "-"
                                    : user_agency.cities_name}
                                </span>
                              </Col>
                              <Col>
                                <p style={{ fontWeight: 600 }}>Quận/ Huyện</p>
                                <span>
                                  {!user_agency.district_name
                                    ? "-"
                                    : user_agency.district_name}
                                </span>
                              </Col>
                              <Col>
                                <p style={{ fontWeight: 600 }}>Phường/ Xã</p>
                                <span>
                                  {!user_agency.ward_name
                                    ? "-"
                                    : user_agency.ward_name}
                                </span>
                              </Col>
                              <Col>
                                <p style={{ fontWeight: 600 }}>
                                  Phòng kinh doanh
                                </p>
                                <span>
                                  {!user_agency.agency_name
                                    ? "-"
                                    : user_agency.agency_name}
                                </span>
                              </Col>
                              <Col style={{ marginTop: 22 }}>
                                <ITitle
                                  level={2}
                                  title="Hình ảnh đại lý"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col style={{ marginTop: 22 }}>
                                <img
                                  src={dataDetail.image_url + user_agency.image}
                                  style={{ width: 165, height: 165 }}
                                />
                              </Col>
                            </Row>
                          )}
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
                <Col
                  span={6}
                  style={{
                    background: "white",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <div
                    style={{
                      padding: "30px 20px",
                    }}
                  >
                    {isLoading ? (
                      <div>
                        <Skeleton
                          loading={true}
                          active
                          paragraph={{ rows: 16 }}
                        />
                      </div>
                    ) : (
                      <Row gutter={[12, 20]}>
                        <Col>
                          <ITitle
                            level={2}
                            title="Loại hình kinh doanh"
                            style={{
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col style={{ marginTop: 8 }}>
                          <p style={{ fontWeight: 600 }}>Cấp đại lý</p>
                          <span>Đại lý cấp 1</span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>
                            Loại hình kinh doanh
                          </p>
                          <span>
                            {!user_agency.business_type_name
                              ? "-"
                              : user_agency.business_type_name}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Ngành hàng chủ lực</p>
                          <span>
                            {!user_agency.type_main_name
                              ? "-"
                              : user_agency.type_main_name}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Cấp bậc</p>
                          <span>
                            {!user_agency.membership
                              ? "-"
                              : user_agency.membership}
                          </span>
                        </Col>

                        {/* <Col span={24} style={{ marginTop: 82 }}>
                          <IButton
                            title="Tài khoản phụ đại lý cấp 1"
                            color={colors.main}
                            onClick={() => {
                              setPopupAccount(true);
                            }}
                          />
                        </Col> */}
                        <Col span={24}>
                          <IButton
                            title="Chính sách bán hàng áp dụng"
                            color={colors.main}
                            onClick={() => {
                              history.push(
                                "/agency/c1/event/list/" + paramsAgency.id
                              );
                            }}
                          />
                        </Col>
                        <Col span={24}>
                          <IButton
                            title="Chính sách bán hàng ẩn"
                            color={colors.main}
                            onClick={() => {
                              history.push(
                                "/agency/c1/event/hide/list/" + paramsAgency.id
                              );
                            }}
                          />
                        </Col>
                        {/* <Col span={24}>
                          <IButton
                            title="Sản phẩm đã mua"
                            color={colors.main}
                            onClick={() => {
                              history.push(
                                "/agency/c1/product/purchased/list/" +
                                  paramsAgency.id
                              );
                            }}
                          />
                        </Col>
                        <Col span={24}>
                          <IButton
                            title="Sản phẩm ẩn/hiện"
                            color={colors.main}
                            onClick={() => {
                              history.push(
                                "/agency/c1/product/hideOrShow/list/" +
                                  paramsAgency.id
                              );
                            }}
                          />
                        </Col>
                       */}
                      </Row>
                    )}
                  </div>
                </Col>
                <Col span={7}>
                  <div
                    style={{
                      background: "white",
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      padding: "30px 30px",
                      height: "100%",
                    }}
                  >
                    {isLoading ? (
                      <div>
                        <Skeleton
                          loading={true}
                          active
                          paragraph={{ rows: 16 }}
                        />
                      </div>
                    ) : (
                      <Row gutter={[12, 20]}>
                        <Col span={24}>
                          <ITitle
                            level={2}
                            title="Thông tin công nợ"
                            style={{
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col span={24}>
                          <Row gutter={[0, 35]}>
                            <Col span={24}>
                              <Row gutter={[12, 6]}>
                                <Col span={24}>
                                  <Row gutter={[6, 20]}>
                                    <Col span={24}>
                                      <p style={{ fontWeight: 600 }}>
                                        Hạn mức tín dụng
                                      </p>
                                      <span style={{ fontWeight: "bold" }}>
                                        {priceFormat(
                                          dataDetail.debt_info.debt_limit + "đ"
                                        )}
                                      </span>
                                    </Col>
                                    <Col span={24}>
                                      <p style={{ fontWeight: 600 }}>
                                        Hạn mức gối đầu
                                      </p>
                                      <span style={{ fontWeight: "bold" }}>
                                        {priceFormat(
                                          dataDetail.debt_info.knee_limit + "đ"
                                        )}
                                      </span>
                                    </Col>
                                    <Col span={24}>
                                      <p style={{ fontWeight: 600 }}>
                                        Hạn mức khả dụng
                                      </p>
                                      <span style={{ fontWeight: "bold" }}>
                                        {priceFormat(
                                          dataDetail.debt_info
                                            .residual_end_term + "đ"
                                        )}
                                      </span>
                                    </Col>
                                    <Col span={24}>
                                      <p style={{ fontWeight: 600 }}>
                                        Công nợ cuối kỳ
                                      </p>
                                      <span style={{ fontWeight: "bold" }}>
                                        {priceFormat(
                                          dataDetail.debt_info.debt_end_term +
                                            "đ"
                                        )}
                                      </span>
                                    </Col>
                                    <Col span={24}>
                                      <div style={{ paddingLeft: 30 }}>
                                        <p style={{ fontWeight: 600 }}>
                                          Nợ quá hạn
                                        </p>
                                        <span style={{ fontWeight: "bold" }}>
                                          {priceFormat(
                                            dataDetail.debt_info.overdue + "đ"
                                          )}
                                        </span>
                                      </div>
                                    </Col>
                                    <Col span={24}>
                                      <div style={{ paddingLeft: 30 }}>
                                        <p style={{ fontWeight: 600 }}>
                                          Nợ đến hạn
                                        </p>
                                        <span style={{ fontWeight: "bold" }}>
                                          {priceFormat(
                                            dataDetail.debt_info.due_debt + "đ"
                                          )}
                                        </span>
                                      </div>
                                    </Col>
                                    <Col span={24}>
                                      <div style={{ paddingLeft: 30 }}>
                                        <p style={{ fontWeight: 600 }}>
                                          Nợ trong hạn
                                        </p>
                                        <span style={{ fontWeight: "bold" }}>
                                          {priceFormat(
                                            dataDetail.debt_info.debt_in_term +
                                              "đ"
                                          )}
                                        </span>
                                      </div>
                                    </Col>
                                    <Col span={24}>
                                      <p style={{ fontWeight: 600 }}>Hạn nợ</p>
                                      <span style={{ fontWeight: "bold" }}>
                                        {dataDetail.debt_info.debt_term} ngày
                                      </span>
                                    </Col>
                                  </Row>
                                </Col>
                                {/* <Col span={12}>
                                  <IButton
                                    title="Cập nhật"
                                    color={colors.main}
                                    loading={isLoadingButton}
                                    onClick={() => {
                                      setIsLoadingButton(true);
                                      const obj = {
                                        id: paramsAgency.id,
                                        todate: toDate.getTime(),
                                      };
                                      postUpdateUserAgencyDebt(obj);
                                    }}
                                  />
                                </Col> */}
                              </Row>
                            </Col>
                            <Col span={24}>
                              <Link
                                to={
                                  "/debit/detailOrderDebit/" + paramsAgency.id
                                }
                              >
                                <IButton
                                  title="Chi tiết công nợ theo đơn hàng"
                                  color={colors.main}
                                />
                              </Link>
                              <Link to={"/debit/history/" + paramsAgency.id}>
                                <IButton
                                  title="Lịch sử cập nhật công nợ"
                                  color={colors.main}
                                  style={{ marginTop: 20 }}
                                />
                              </Link>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    )}
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </div>
      </Row>
      <Modal
        visible={isVisbleModal}
        width={1200}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 0 }}
      >
        <ITransfer
          callbackClose={() => handleCloseModal()}
          title="Danh sách Đại lý Cấp 2 Trực thuộc"
          dataList={[
            { title: "ABCD123456" },
            { title: "ABCD123456" },
            { title: "ABCD123456" },
          ]}
          dataHeader={[
            { name: "STT", align: "center", width: "50px" },
            { name: "Mã đại lý", align: "left", width: "80px" },
            { name: "Tên đại lý", align: "left", width: "80px" },
            { name: "Địa chỉ", align: "left", width: "250px" },
            { name: "Số điện thoại", align: "left", width: "120px" },
            { name: "Người đại diện", align: "left", width: "97px" },
            { name: "", align: "center", width: "47px" },
          ]}
          keyId={[
            "stt",
            "dms_code",
            "shop_name",
            "address",
            "phone",
            "bussiness_name",
          ]}
          align={["center", "left", "left", "left", "left", "left", "center"]}
          dataBody={dataAgencyS2}
        />
      </Modal>
      <Modal
        visible={popupAccount}
        width={1200}
        footer={null}
        closable={false}
        onCancel={() => {
          setPopupAccount(false);
        }}
      >
        {ModalAccount(popupAccount, paramsAgency.id, user_agency.phone)}
      </Modal>
    </div>
  );
}
