import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton } from "antd";
import { ITitle, ISvg, ITable, IDatePicker } from "../../../components";
import FormatterDay from "../../../../utils/FormatterDay";
import { colors } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { priceFormat } from "../../../../utils";
import { useParams } from "react-router-dom";
import { APIService } from "../../../../services";

const DetailDebtAgencyS2 = (props) => {
  const params = useParams();
  const { id } = params;
  const format = "#DD#/#MM#/#YYYY#";

  const NewDate = new Date();
  const DateStart = new Date();
  let startValue = DateStart.setMonth(DateStart.getMonth() - 1);
  const [filter, setFilter] = useState({
    start_date: startValue,
    end_date: NewDate.setHours(23, 59, 59, 999),
    page: 1,
    id: id,
  });

  const [loading, setLoading] = useState(true);
  const [loadingTable, setLoadingTable] = useState(false);

  const [dataTable, setDataTable] = useState({
    listDebt: [],
    total: 0,
    size: 10,
  });

  const [data, setData] = useState({});

  const getAPIDetailDebtAgencyS2 = async (filter) => {
    try {
      const data = await APIService._getAPIDetailDebtAgencyS2(filter);
      console.log("data-user-agency: ", data.user_agency);
      setData(data.user_agency);
      const newData = data.lst.result.map((item, index) => {
        item.stt = index + 1;
        return item;
      });
      setDataTable({
        ...dataTable,
        listDebt: newData,
        total: data.lst.total,
        size: data.lst.size,
      });
      setLoading(false);
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    getAPIDetailDebtAgencyS2(filter);
  }, [filter]);

  const renderTitle = (title) => {
    return (
      <div>
        <ITitle title={title} style={{ fontWeight: "bold" }} />
      </div>
    );
  };

  const columns = [
    {
      title: renderTitle("STT"),
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },
    {
      title: renderTitle("Số hóa đơn"),
      dataIndex: "bill_code",
      key: "bill_code",
      wordWrap: "break-word",
      wordBreak: "break-word",
    },
    {
      title: renderTitle("Ngày xuất hóa đơn"),
      dataIndex: "export_date",
      key: "export_date",
      render: (export_date) => {
        return FormatterDay.dateFormatWithString(export_date, format);
      },
    },
    {
      title: renderTitle("Giá trị hóa đơn"),
      dataIndex: "total_money",
      key: "total_money",
      render: (total_money) => {
        return priceFormat(total_money + "đ");
      },
    },
    {
      title: renderTitle("Đã thanh toán"),
      dataIndex: "total_paid",
      key: "total_paid",
      render: (total_paid) => {
        return priceFormat(total_paid + "đ");
      },
    },
    {
      title: renderTitle("Còn phải thu từ đại lý"),
      dataIndex: "total_debt",
      key: "total_debt",
      render: (total_debt) => {
        return priceFormat(total_debt + "đ");
      },
    },
    {
      title: renderTitle("Ngày hoạch toán sổ cái"),
      dataIndex: "update_ledger_date",
      key: "update_ledger_date",
      render: (update_ledger_date) => {
        return FormatterDay.dateFormatWithString(update_ledger_date, format);
      },
    },
    {
      title: renderTitle("Số SO"),
      dataIndex: "code_so",
      key: "code_so",
      wordWrap: "break-word",
      wordBreak: "break-word",
    },
    {
      title: renderTitle("Số DO"),
      dataIndex: "code_do",
      key: "code_do",
      wordWrap: "break-word",
      wordBreak: "break-word",
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      {loading ? (
        <div>
          <Row gutter={[0, 24]}>
            <Col span={24}>
              <div>
                <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                  Chi tiết công nợ đại lý cấp 2 theo hóa đơn
                </StyledITitle>
              </div>
            </Col>
            <Col
              span={24}
              style={{ padding: "18px", marginTop: 24 }}
              className="box-shadow"
            >
              <Skeleton loading={loading} active paragraph={{ rows: 16 }} />
            </Col>
          </Row>
        </div>
      ) : (
        <Row>
          <Col span={12}>
            <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
              Chi tiết công nợ đại lý cấp 2 theo hóa đơn
            </StyledITitle>
          </Col>
          <Col style={{ margin: "35px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col
                    span={14}
                    style={{
                      paddingLeft: 20,
                    }}
                  >
                    <Row gutter={[20, 0]}>
                      <Col span={4}>
                        <p style={{ fontWeight: 600 }}>Tên đại lý</p>
                        <span>{data.shop_name}</span>
                      </Col>
                      <Col span={4}>
                        <p style={{ fontWeight: 600 }}>Mã đại lý</p>
                        <span>{data.code}</span>
                      </Col>
                      <Col span={8} xl={7} xxl={5}>
                        <p style={{ fontWeight: 600 }}>Phòng kinh doanh</p>
                        <span>{data.agency_name}</span>
                      </Col>
                      <Col span={6}>
                        <p style={{ fontWeight: 600 }}>Hạn mức công nợ</p>
                        <span>{priceFormat(data.debt_limit + "đ")}</span>
                      </Col>
                    </Row>
                  </Col>
                  <Col
                    span={10}
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                      paddingRight: 14,
                    }}
                  >
                    <Row gutter={[15, 0]}>
                      <Col span={8}>
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            marginTop: 10,
                            justifyContent: "flex-end",
                          }}
                        >
                          <ISvg
                            name={ISvg.NAME.EXPERIMENT}
                            with={16}
                            height={16}
                            fill={colors.icon.default}
                          />
                          <ITitle
                            title="Từ ngày"
                            level={4}
                            style={{
                              marginLeft: 25,
                              marginRight: 25,
                            }}
                          />
                        </div>
                      </Col>
                      <Col span={16}>
                        <IDatePicker
                          isBackground
                          from={filter.start_date}
                          to={filter.end_date}
                          onChange={(date, dateString) => {
                            if (date.length <= 1) {
                              return;
                            }

                            setLoadingTable(true);
                            setFilter({
                              ...filter,
                              start_date: date[0]._d.setHours(0, 0, 0),
                              end_date: date[1]._d.setHours(23, 59, 59),
                              page: 1,
                            });
                          }}
                          style={{
                            background: colors.white,
                            borderWidth: 1,
                            borderColor: colors.line,
                            borderStyle: "solid",
                          }}
                        />
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listDebt}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      )}
    </div>
  );
};

export default DetailDebtAgencyS2;
