import React, { useState, useEffect } from "react";
import { Row, Col, Modal, Skeleton, message } from "antd";
import { ITitle, IButton, ISvg } from "../../../components";
import { colors, images } from "../../../../assets";

import ITransfer from "../../../components/styled/modalTransfer/ITransfer";
import { APIService } from "../../../../services";
import { useParams, Link } from "react-router-dom";
import { priceFormat } from "../../../../utils";

const toDate = new Date();
function DetailAgencyS2(props) {
  const [isVisbleModal, setVisbleModal] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingButton, setIsLoadingButton] = useState(false);
  const paramsAgencyS2 = useParams();

  let dataBody = [];
  for (let i = 0; i < 4; i++) {
    dataBody.push({
      stt: i + 1,
      code_agency: "ABC 123456",
      name_agency: "Semi Admin",
      address: "153 Hồ Văn Huê, Phường 9, Quận Phú Nhuận, Hồ Chí Minh",
      sdt: "0123456789",
    });
  }
  const handleCloseModal = () => {
    setVisbleModal(false);
  };

  const [dataDetail, setDataDetail] = useState({
    supplier: {
      contract: {},
      image: "",
    },
    debt_info: {},
    loading: false,
    image_url: "",
  });

  useEffect(() => {
    _getAPIDetailAgency(paramsAgencyS2.id);
  }, []);

  const _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailSuppliers(idAgency);
      setDataDetail(data);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const postUpdateUserAgencyS2Debt = async (obj) => {
    try {
      const data = await APIService._postUpdateUserAgencyS2Debt(obj);
      console.log("data-debt: ", data);
      setIsLoadingButton(false);
      setIsLoading(true);
      message.success("Cập nhật thành công");
      _getAPIDetailAgency(paramsAgencyS2.id);
    } catch (error) {
      console.log(error);
      setIsLoadingButton(false);
    }
  };

  const { supplier, image_url } = dataDetail;
  const { contract } = supplier;

  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 16]}>
        <Col>
          <ITitle
            level={1}
            title="Chi tiết đại lý cấp 2 "
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        <Col>
          <Row gutter={[12, 20]}>
            <Col>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  icon={ISvg.NAME.SAVE}
                  title="Chỉnh sửa"
                  color={colors.main}
                  onClick={() =>
                    props.history.push("/agencyS2/edit/" + paramsAgencyS2.id)
                  }
                />
                {/* <IButton
                  icon={ISvg.NAME.CROSS}
                  title="Xóa"
                  style={{ margin: "0px 0px 0px 20px" }}
                  color={colors.oranges}
                  onClick={() => props.history.push("/agencyPageS2")}
                /> */}
              </div>
            </Col>
            <Col span={24} xxl={17}>
              <div
                style={{
                  // maxWidth: 1000,
                  // minWidth: 680,
                  height: "100%",
                  // background: colors.white,
                }}
              >
                <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
                  <Col span={15}>
                    <div
                      style={{
                        background: colors.white,
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      }}
                    >
                      {isLoading ? (
                        <div style={{ padding: "30px 40px" }}>
                          <Skeleton
                            loading={true}
                            active
                            paragraph={{ rows: 16 }}
                          />
                        </div>
                      ) : (
                        <Row>
                          <Col span={14}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col style={{ marginTop: 6 }}>
                                  <ITitle
                                    level={2}
                                    title="Thông tin đại lý"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col>
                                  <div>
                                    <p style={{ fontWeight: 600 }}>
                                      Tên đại lý
                                    </p>
                                    <span style={{ wordWrap: "break-word" }}>
                                      {!supplier.supplier_name
                                        ? "-"
                                        : supplier.supplier_name}
                                    </span>
                                  </div>
                                </Col>
                                <Col>
                                  <Row gutter={[12, 12]}>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Mã đại lý
                                        </p>
                                        <span>{supplier.supplier_code}</span>
                                      </div>
                                    </Col>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Trạng thái
                                        </p>
                                        <span>{supplier.status}</span>
                                      </div>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col>
                                  <Row gutter={[12, 12]}>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Mật khẩu
                                        </p>
                                        <span>******</span>
                                      </div>
                                    </Col>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Số điện thoại đăng nhập
                                        </p>
                                        <span>{supplier.supplier_phone}</span>
                                      </div>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col>
                                  <div>
                                    <p style={{ fontWeight: 600 }}>Địa chỉ</p>
                                    <span style={{ wordWrap: "break-word" }}>
                                      {
                                        supplier.supplier_address
                                        // +
                                        //   ", " +
                                        //   supplier.district_name +
                                        //   ", " +
                                        //   supplier.city_name
                                      }
                                    </span>
                                  </div>
                                </Col>
                              </Row>
                              <div style={{ marginTop: 42 }}>
                                <Row gutter={[12, 20]}>
                                  <Col style={{ marginTop: 6 }}>
                                    <ITitle
                                      level={2}
                                      title="Thông tin hợp đồng"
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    />
                                  </Col>
                                  <Col>
                                    <div>
                                      <p style={{ fontWeight: 600 }}>
                                        Người đại diện
                                      </p>
                                      <span>{supplier.user_represent}</span>
                                    </div>
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={10}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Số điện thoại 2
                                          </p>
                                          <span>
                                            {!supplier.user_phone
                                              ? "-"
                                              : supplier.user_phone}
                                          </span>
                                        </div>
                                      </Col>
                                      <Col span={14}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Tuyến
                                          </p>
                                          <span>
                                            {!supplier.route_name
                                              ? "-"
                                              : supplier.route_name}
                                          </span>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={10}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Nhân viên bán hàng
                                          </p>
                                          <span>
                                            {!supplier.sale_man_name
                                              ? "-"
                                              : supplier.sale_man_name}
                                          </span>
                                        </div>
                                      </Col>
                                      <Col span={14}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Nhân viên Dịch vụ Nông nghiệp
                                          </p>
                                          <span>
                                            {!supplier.sale_service_farming
                                              ? "-"
                                              : supplier.sale_service_farming}
                                          </span>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={10}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Đại lý Cấp 1 trực thuộc
                                          </p>
                                          <span>
                                            {!supplier.manager_supplier_tier_1
                                              ? "-"
                                              : supplier.manager_supplier_tier_1}
                                          </span>
                                        </div>
                                      </Col>
                                      <Col span={14}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Đại lý Cấp 1 trực thuộc (bổ sung)
                                          </p>
                                          <span>
                                            {!supplier.manager_supplier_tier_1_addtional
                                              ? "-"
                                              : supplier.manager_supplier_tier_1_addtional}
                                          </span>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Col>
                                </Row>
                              </div>
                            </div>
                          </Col>
                          <Col span={8}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                // borderRight: "1px solid #f0f0f0",
                                width: "100%",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col style={{ marginTop: 6 }}>
                                  <ITitle
                                    level={2}
                                    title="Khu vực"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col>
                                  <div>
                                    <p style={{ fontWeight: 600 }}>
                                      Tỉnh/ Thành Phố
                                    </p>
                                    <span>{supplier.city_name}</span>
                                  </div>
                                </Col>
                                <Col>
                                  <div>
                                    <p style={{ fontWeight: 600 }}>
                                      Quận/ Huyện
                                    </p>
                                    <span>{supplier.district_name}</span>
                                  </div>
                                </Col>
                                <Col>
                                  <div>
                                    <p style={{ fontWeight: 600 }}>
                                      Phường / Xã
                                    </p>
                                    <span>
                                      {!supplier.ward_name
                                        ? "-"
                                        : supplier.ward_name}
                                    </span>
                                  </div>
                                </Col>
                                <div style={{ marginTop: 42 }}>
                                  <Row gutter={[12, 20]}>
                                    <Col style={{ marginTop: 6 }}>
                                      <ITitle
                                        level={2}
                                        title="Hình ảnh đại lý"
                                        style={{
                                          fontWeight: "bold",
                                        }}
                                      />
                                    </Col>
                                    <Col>
                                      <img
                                        src={
                                          !supplier.image
                                            ? images.avatar
                                            : image_url + supplier.image
                                        }
                                        style={{
                                          height: 190,
                                          width: "100%",
                                          padding: 12,
                                          border: "1px solid #D8DBDC",
                                        }}
                                      />
                                    </Col>
                                    {/* <Col>
                                    <div
                                      style={{
                                        wordWrap: "break-word",
                                        textAlign: "center",
                                        border: "1px solid #004C91",
                                        paddingLeft: 30,
                                        paddingRight: 30,
                                      }}
                                      onClick={() => setVisbleModal(true)}
                                    >
                                      <span
                                        style={{
                                          color: colors.main,
                                          fontWeight: "bold",
                                          fontSize: 14,
                                        }}
                                      >
                                        Danh sách nông dân phụ thuộc
                                      </span>
                                    </div>
                                  </Col> */}
                                  </Row>
                                </div>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                      )}
                    </div>
                  </Col>
                  <Col span={9}>
                    <div
                      style={{
                        padding: "30px 32.5px",
                        background: "white",
                        height: "100%",
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      }}
                    >
                      {isLoading ? (
                        <div>
                          <Skeleton
                            loading={true}
                            active
                            paragraph={{ rows: 16 }}
                          />
                        </div>
                      ) : (
                        <Row gutter={[12, 20]}>
                          <Col span={24}>
                            <ITitle
                              level={2}
                              title="Thông tin công nợ"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col span={24}>
                            <Row gutter={[0, 35]}>
                              <Col span={24}>
                                <Row gutter={[12, 6]}>
                                  <Col span={12}>
                                    <Row gutter={[6, 20]}>
                                      <Col span={24}>
                                        <p style={{ fontWeight: 600 }}>
                                          Hạn mức công nợ
                                        </p>
                                        <span style={{ fontWeight: "bold" }}>
                                          {priceFormat(
                                            dataDetail.debt_info.debt_limit +
                                              "đ"
                                          )}
                                        </span>
                                      </Col>
                                      <Col span={24}>
                                        <p style={{ fontWeight: 600 }}>
                                          Dư nợ cuối kỳ
                                        </p>
                                        <span style={{ fontWeight: "bold" }}>
                                          {priceFormat(
                                            dataDetail.debt_info.debt_end_term +
                                              "đ"
                                          )}
                                        </span>
                                      </Col>
                                      <Col span={24}>
                                        <p style={{ fontWeight: 600 }}>
                                          Dư có cuối kỳ
                                        </p>
                                        <span style={{ fontWeight: "bold" }}>
                                          {priceFormat(
                                            dataDetail.debt_info
                                              .residual_end_term + "đ"
                                          )}
                                        </span>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col span={12}>
                                    <IButton
                                      title="Cập nhật"
                                      color={colors.main}
                                      loading={isLoadingButton}
                                      onClick={() => {
                                        setIsLoadingButton(true);
                                        const obj = {
                                          id: paramsAgencyS2.id,
                                        };
                                        postUpdateUserAgencyS2Debt(obj);
                                      }}
                                    />
                                  </Col>
                                </Row>
                              </Col>
                              <Col span={24}>
                                <Link
                                  to={
                                    "/agencyS2/debt/detail/" + paramsAgencyS2.id
                                  }
                                >
                                  <IButton
                                    title="Chi tiết công nợ theo hóa đơn"
                                    color={colors.main}
                                  />
                                </Link>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        visible={isVisbleModal}
        width={1200}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 0 }}
      >
        <ITransfer
          callbackClose={() => handleCloseModal()}
          title="Danh sách Nông dân Trực thuộc"
          dataList={[
            { title: "ABCD123451", id: 1, active: false },
            { title: "ABCD123452", id: 2, active: true },
            { title: "ABCD123453", id: 3, active: false },
            { title: "ABCD123454", id: 4, active: false },
            { title: "ABCD123455", id: 5, active: true },
            { title: "ABCD123456", id: 6, active: false },
            { title: "ABCD123457", id: 7, active: false },
            { title: "ABCD123458", id: 8, active: true },
            { title: "ABCD123459", id: 9, active: false },
          ]}
          dataHeader={[
            { name: "STT", align: "center", width: "50px" },
            { name: "Mã nông dân", align: "left", width: "150px" },
            { name: "Tên nông dân", align: "left", width: "150px" },
            { name: "Địa chỉ", align: "left", width: "350px" },
            { name: "Số điện thoại", align: "left", width: "120px" },
            { name: "", align: "center", width: "36px" },
          ]}
          dataBody={dataBody}
          keyId={["stt", "code_agency", "name_agency", "address", "sdt"]}
          align={["center", "left", "left", "left", "left", "center"]}
        />
      </Modal>
    </div>
  );
}

export default DetailAgencyS2;
