import React, { useState, useEffect } from "react";
import { Row, Form, Col, message } from "antd";
import {
  ITitle,
  IButton,
  ISvg,
  IInput,
  ISelect,
  ISelectCity,
  IUpload,
} from "../../../components";
import { colors } from "../../../../assets";
import { StyledForm } from "../../../components/styled/form/styledForm";
import { ImageType } from "../../../../constant";
import { useParams, useHistory } from "react-router-dom";
import { APIService } from "../../../../services";
import {
  _getAPIListCounty,
  _getAPIListWard,
} from "../../../containers/hookcustom/ApiCustomHook";

function CreateAgencyS2(props) {
  const params = useParams();
  const history = useHistory();
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;

  const [dataCities, setDataCities] = useState([{ key: 0 }]);
  const [dataWard, setDataWard] = useState([{ key: 0 }]);
  const [dataAgencyS1, setDataAgencyS1] = useState([{ key: 0 }]);
  const [images, setImages] = useState([]);
  const [add, setAdd] = useState(params.type == "add" ? true : false);
  const [url, setUrl] = useState("");
  const [loadingButton, setLoadingButton] = useState(false);

  useEffect(() => {
    _getAPIListAgencyS1();
    if (!add) {
      _getAPIDetailAgency(params.id);

      return;
    }
  }, []);

  const arrStatus = [
    {
      key: 0,
      value: "Chờ xác nhận",
    },
    {
      key: 1,
      value: "Đang hoạt động",
    },
  ];

  async function _postAPIAddNewAgencyS2(obj) {
    try {
      setLoadingButton(true);
      const data = await APIService._postAddNewAgencyS2(obj);
      setLoadingButton(false);
      message.success("Tạo đại lý cấp 2 thành công");
      history.push("/agencyPageS2");
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  }

  const _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailSuppliers(idAgency);
      props.form.setFieldsValue({
        name_agency: data.supplier.supplier_name,
        code_agency: data.supplier.supplier_code,
        address_agency: data.supplier.supplier_address,
        avatar: data.supplier.image,
        sdt_agency: data.supplier.supplier_phone,
        city_agency: data.supplier.city_id,
        district_agency: data.supplier.district_id,
        ward_agency: data.supplier.ward_id,
        sdt2_agency: data.supplier.user_phone,
        represent_agency: data.supplier.user_represent,
        under_agency: data.supplier.user_agency_id,
        under_agency_additional: data.supplier.user_agency_id_addtional,
        password: "",
        status: data.supplier.status_id,
      });
      setUrl(data.image_url);
      setImages([data.supplier.image]);
      _getAPIListCounty(reduxForm.getFieldValue("city_agency"), "", (data) => {
        setDataCities(data);
      });
      _getAPIListWard(
        reduxForm.getFieldValue("district_agency"),
        "",
        (data) => {
          setDataWard(data);
        }
      );
    } catch (error) {
      console.log(error);
    }
  };

  const _postAPIEditSuppliers = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postEditSuppliers(obj);
      setLoadingButton(false);
      message.success("Cập nhập thành công");
      history.goBack();
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
    }
  };

  const onSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields({ force: true }, (err, values) => {
      if (err) {
        return;
      }

      const objAdd = {
        // supplierCode: values.code_agency,
        address: values.address_agency,
        avatar: !images ? [] : images,
        supplierName: values.name_agency,
        phone: values.sdt_agency,
        cityID: values.city_agency,
        districtID: values.district_agency,
        wardID: values.ward_agency,
        phone_2: !values.sdt2_agency ? "" : values.sdt2_agency,
        user_represent: values.represent_agency,
        supplier_id_manager: values.under_agency,
        password: values.password,
        supplier_id_addtional: !values.under_agency_additional
          ? ""
          : values.under_agency_additional,
        id: 0,
      };

      const objEdit = {
        status: values.status,
        address: values.address_agency,
        avatar: !images ? [] : images,
        supplierName: values.name_agency,
        phone: values.sdt_agency,
        cityID: values.city_agency,
        districtID: values.district_agency,
        wardID: values.ward_agency,
        phone_2: !values.sdt2_agency ? "" : values.sdt2_agency,
        user_represent: values.represent_agency,
        supplier_id_manager: values.under_agency,
        password: values.password,
        supplier_id_addtional: !values.under_agency_additional
          ? ""
          : values.under_agency_additional,
        id: params.id,
      };
      add ? _postAPIAddNewAgencyS2(objAdd) : _postAPIEditSuppliers(objEdit);
    });
  };

  async function _getAPIListAgencyS1() {
    try {
      const data = await APIService._getListAgencyS1Name("");
      const dataNew = data.suppliers_name.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setDataAgencyS1(dataNew);
    } catch (error) {}
  }

  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 16]}>
        <Col>
          <ITitle
            level={1}
            title={
              params.type === "edit"
                ? "Chỉnh sửa đại lý cấp 2"
                : "Tạo đại lý cấp 2 mới"
            }
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        <Col>
          <StyledForm onSubmit={onSubmit}>
            <Row gutter={[12, 20]}>
              <Col>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    icon={ISvg.NAME.SAVE}
                    title="Lưu"
                    color={colors.main}
                    loading={loadingButton}
                    htmlType="submit"
                  />
                  <IButton
                    icon={ISvg.NAME.CROSS}
                    title="Hủy"
                    onClick={() => props.history.goBack()}
                    style={{ margin: "0px 0px 0px 20px" }}
                    color={colors.oranges}
                  />
                </div>
              </Col>
              <Col>
                <div
                  style={{
                    maxWidth: 815,
                    minWidth: 680,
                    height: "100%",
                    paddingBottom: 32,
                    // background: colors.white,
                  }}
                >
                  <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
                    <Col span={24} style={{}}>
                      <div
                        style={{
                          background: colors.white,
                          boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        }}
                      >
                        <Row>
                          <Col span={16}>
                            <div
                              style={{
                                padding: "30px 40px 60px 40px",
                                background: colors.white,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col style={{ marginTop: 6 }}>
                                  <ITitle
                                    level={2}
                                    title="Thông tin đại lý"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col>
                                  <Form.Item>
                                    {getFieldDecorator("name_agency", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "nhập tên cửa hàng đại lý",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Tên đại lý{" "}
                                          <span
                                            style={{
                                              color: "red",
                                              fontSize: 14,
                                            }}
                                          >
                                            *
                                          </span>
                                        </p>
                                        <IInput
                                          loai="input"
                                          placeholder="nhập tên cửa hàng đại lý"
                                          value={reduxForm.getFieldValue(
                                            "name_agency"
                                          )}
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                                {add ? (
                                  <Col span={24}>
                                    <Row gutter={[12, 12]}>
                                      <Col span={12}>
                                        <Form.Item>
                                          {getFieldDecorator("sdt_agency", {
                                            rules: [
                                              {
                                                required: true,
                                                // message:
                                                //   "nhập điện thoại đăng nhập",
                                                pattern: new RegExp(
                                                  /0[0-9]([0-9]{8})\b/
                                                ),
                                                message:
                                                  "nhập tối đa 10 số, không được nhập chữ, khoảng cách và ký tự đặc biệt",
                                              },
                                            ],
                                          })(
                                            <div>
                                              <p style={{ fontWeight: 600 }}>
                                                Số điện thoại đăng nhập{" "}
                                                <span
                                                  style={{
                                                    color: "red",
                                                    fontSize: 14,
                                                  }}
                                                >
                                                  *
                                                </span>
                                              </p>
                                              <IInput
                                                loai="input"
                                                maxLength={10}
                                                onKeyPress={(e) => {
                                                  console.log(e.target.value);
                                                  var char =
                                                    String.fromCharCode(
                                                      e.which
                                                    );
                                                  if (!/[0-9]/.test(char)) {
                                                    e.preventDefault();
                                                  }
                                                }}
                                                placeholder="nhập số điện thoại"
                                                value={reduxForm.getFieldValue(
                                                  "sdt_agency"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>

                                      <Col span={12}>
                                        <Form.Item>
                                          {getFieldDecorator("password", {
                                            rules: [
                                              {
                                                required: add,
                                                type: "string",
                                                message:
                                                  "mật khẩu không được để trống và tối thiểu 6 ký tự, tối đa 32 ký tự, không dấu ",
                                                min: 6,
                                                max: 32,
                                                pattern: new RegExp(
                                                  "^[a-zA-Z0-9\\s!@#$%^&*()-_=+`~]*$"
                                                ),
                                              },
                                            ],
                                          })(
                                            <div>
                                              <p style={{ fontWeight: 600 }}>
                                                Mật khẩu
                                                <span
                                                  style={{
                                                    color: "red",
                                                    fontSize: 14,
                                                  }}
                                                >
                                                  *
                                                </span>
                                              </p>
                                              <IInput
                                                loai="input"
                                                placeholder="nhập mật khẩu"
                                                value={reduxForm.getFieldValue(
                                                  "password"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                    </Row>
                                  </Col>
                                ) : (
                                  <>
                                    <Col span={24}>
                                      <Row gutter={[12, 12]}>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("code_agency", {
                                              rules: [
                                                {
                                                  required: false,
                                                  message: "nhập mã đại lý",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                >
                                                  Mã đại lý{" "}
                                                  <span
                                                    style={{
                                                      color: "red",
                                                      fontSize: 14,
                                                    }}
                                                  >
                                                    *
                                                  </span>
                                                </p>
                                                <IInput
                                                  loai="input"
                                                  disabled={true}
                                                  placeholder="nhập mã đại lý"
                                                  value={reduxForm.getFieldValue(
                                                    "code_agency"
                                                  )}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("sdt_agency", {
                                              rules: [
                                                {
                                                  required: true,
                                                  // message:
                                                  //   "nhập điện thoại đăng nhập",
                                                  pattern: new RegExp(
                                                    /0[0-9]([0-9]{8})\b/
                                                  ),
                                                  message:
                                                    "nhập tối đa 10 số, không được nhập chữ, khoảng cách và ký tự đặc biệt",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p style={{ fontWeight: 600 }}>
                                                  Số điện thoại đăng nhập{" "}
                                                  <span
                                                    style={{
                                                      color: "red",
                                                      fontSize: 14,
                                                    }}
                                                  >
                                                    *
                                                  </span>
                                                </p>
                                                <IInput
                                                  loai="input"
                                                  maxLength={10}
                                                  onKeyPress={(e) => {
                                                    console.log(e.target.value);
                                                    var char =
                                                      String.fromCharCode(
                                                        e.which
                                                      );
                                                    if (!/[0-9]/.test(char)) {
                                                      e.preventDefault();
                                                    }
                                                  }}
                                                  placeholder="nhập số điện thoại"
                                                  value={reduxForm.getFieldValue(
                                                    "sdt_agency"
                                                  )}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                      </Row>
                                    </Col>
                                    <Col span={24}>
                                      <Row gutter={[12, 12]}>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("password", {
                                              rules: [
                                                {
                                                  required: add,
                                                  type: "string",
                                                  message:
                                                    "mật khẩu không được để trống và tối thiểu 6 ký tự, tối đa 32 ký tự, không dấu ",
                                                  min: 6,
                                                  max: 32,
                                                  pattern: new RegExp(
                                                    "^[a-zA-Z0-9\\s!@#$%^&*()-_=+`~]*$"
                                                  ),
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p style={{ fontWeight: 600 }}>
                                                  Mật khẩu
                                                  <span
                                                    style={{
                                                      color: "red",
                                                      fontSize: 14,
                                                    }}
                                                  >
                                                    *
                                                  </span>
                                                </p>
                                                <IInput
                                                  loai="input"
                                                  placeholder="nhập mật khẩu"
                                                  value={reduxForm.getFieldValue(
                                                    "password"
                                                  )}
                                                  style={{ marginTop: 1 }}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                          <Form.Item>
                                            {getFieldDecorator("status", {
                                              rules: [
                                                {
                                                  required: false,
                                                  message: "chọn trạng thái",
                                                },
                                              ],
                                            })(
                                              <div>
                                                <p style={{ fontWeight: 600 }}>
                                                  Trạng thái
                                                </p>
                                                <ISelect
                                                  data={arrStatus}
                                                  select={
                                                    reduxForm.getFieldValue(
                                                      "status"
                                                    ) === 1
                                                      ? false
                                                      : true
                                                  }
                                                  onChange={(key) => {
                                                    reduxForm.setFieldsValue({
                                                      status: key,
                                                    });
                                                  }}
                                                  isBackground={false}
                                                  placeholder="chọn trạng thái"
                                                  value={reduxForm.getFieldValue(
                                                    "status"
                                                  )}
                                                />
                                              </div>
                                            )}
                                          </Form.Item>
                                        </Col>
                                      </Row>
                                    </Col>
                                  </>
                                )}

                                <Col span={24}>
                                  <Form.Item>
                                    {getFieldDecorator("address_agency", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "nhập địa chỉ đại lý",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Địa chỉ{" "}
                                          <span
                                            style={{
                                              color: "red",
                                              fontSize: 14,
                                            }}
                                          >
                                            *
                                          </span>
                                        </p>
                                        <IInput
                                          loai="input"
                                          placeholder="nhập địa chỉ đại lý"
                                          value={
                                            reduxForm.getFieldValue(
                                              "address_agency"
                                            )
                                            //  +
                                            // ", " +
                                            // reduxForm.getFieldValue(
                                            //   "ward_agency"
                                            // ) +
                                            // ", " +
                                            // reduxForm.getFieldValue(
                                            //   "district_agency"
                                            // ) +
                                            // ", " +
                                            // reduxForm.getFieldValue(
                                            //   "city_agency"
                                            // )
                                          }
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                              </Row>
                              <div style={{ marginTop: 42 }}>
                                <Row gutter={[12, 20]}>
                                  <Col style={{ marginTop: 6 }}>
                                    <ITitle
                                      level={2}
                                      title="Thông tin vận hành"
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    />
                                  </Col>
                                  <Col>
                                    <Form.Item>
                                      {getFieldDecorator("represent_agency", {
                                        rules: [
                                          {
                                            required: true,
                                            message: "nhập tên người đại diện",
                                          },
                                        ],
                                      })(
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Người đại diện{" "}
                                            <span
                                              style={{
                                                color: "red",
                                                fontSize: 14,
                                              }}
                                            >
                                              *
                                            </span>
                                          </p>
                                          <IInput
                                            loai="input"
                                            placeholder="nhập tên người đại diện"
                                            value={reduxForm.getFieldValue(
                                              "represent_agency"
                                            )}
                                          />
                                        </div>
                                      )}
                                    </Form.Item>
                                  </Col>
                                  <Col>
                                    <Form.Item>
                                      {getFieldDecorator("sdt2_agency", {
                                        rules: [
                                          {
                                            required: true,
                                            // message:
                                            //   "nhập điện thoại đăng nhập",
                                            pattern: new RegExp(
                                              /0[0-9]([0-9]{8})\b/
                                            ),
                                            message:
                                              "nhập tối đa 10 số, không được nhập chữ, khoảng cách và ký tự đặc biệt",
                                          },
                                        ],
                                      })(
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Số điện thoại 2{" "}
                                            <span
                                              style={{
                                                color: "red",
                                                fontSize: 14,
                                              }}
                                            >
                                              *
                                            </span>
                                          </p>
                                          <IInput
                                            loai="input"
                                            maxLength={10}
                                            onKeyPress={(e) => {
                                              console.log(e.target.value);
                                              var char = String.fromCharCode(
                                                e.which
                                              );
                                              if (!/[0-9]/.test(char)) {
                                                e.preventDefault();
                                              }
                                            }}
                                            placeholder="nhập số điện thoại"
                                            value={reduxForm.getFieldValue(
                                              "sdt2_agency"
                                            )}
                                          />
                                        </div>
                                      )}
                                    </Form.Item>
                                  </Col>

                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={10}>
                                        <Form.Item>
                                          {getFieldDecorator("under_agency", {
                                            rules: [
                                              {
                                                required: true,
                                                message: "chọn đại lý",
                                              },
                                            ],
                                          })(
                                            <div>
                                              <p style={{ fontWeight: 600 }}>
                                                Đại lý Cấp 1 trực thuộc{" "}
                                                <span
                                                  style={{
                                                    color: "red",
                                                    fontSize: 14,
                                                  }}
                                                >
                                                  *
                                                </span>
                                              </p>
                                              <ISelect
                                                data={dataAgencyS1}
                                                select={true}
                                                isBackground={false}
                                                placeholder="chọn đại lý"
                                                onChange={(key) => {
                                                  if (
                                                    key ==
                                                    reduxForm.getFieldValue(
                                                      "under_agency_additional"
                                                    )
                                                  ) {
                                                    message.warning(
                                                      "Bị trùng. vui lòng chọn lại"
                                                    );
                                                    return null;
                                                  }
                                                  reduxForm.setFieldsValue({
                                                    under_agency: key,
                                                  });
                                                }}
                                                value={reduxForm.getFieldValue(
                                                  "under_agency"
                                                )}
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                      <Col span={14}>
                                        <Form.Item>
                                          {getFieldDecorator(
                                            "under_agency_additional",
                                            {
                                              rules: [
                                                {
                                                  required: false,
                                                  message: "chọn đại lý",
                                                },
                                              ],
                                            }
                                          )(
                                            <div>
                                              <p style={{ fontWeight: 600 }}>
                                                Đại lý Cấp 1 trực thuộc (bổ
                                                sung)
                                              </p>
                                              <ISelect
                                                data={dataAgencyS1}
                                                select={true}
                                                isBackground={false}
                                                placeholder="chọn đại lý"
                                                onChange={(key) => {
                                                  if (
                                                    key ==
                                                    reduxForm.getFieldValue(
                                                      "under_agency"
                                                    )
                                                  ) {
                                                    message.warning(
                                                      "Bị trùng. vui lòng chọn lại"
                                                    );
                                                    return null;
                                                  }
                                                  reduxForm.setFieldsValue({
                                                    under_agency_additional:
                                                      key,
                                                  });
                                                }}
                                                value={
                                                  reduxForm.getFieldValue(
                                                    "under_agency_additional"
                                                  ) == 0
                                                    ? reduxForm.getFieldValue(
                                                        "under_agency_additional" ==
                                                          0
                                                      )
                                                    : reduxForm.getFieldValue(
                                                        "under_agency_additional"
                                                      )
                                                }
                                              />
                                            </div>
                                          )}
                                        </Form.Item>
                                      </Col>
                                    </Row>
                                  </Col>
                                </Row>
                              </div>
                            </div>
                          </Col>
                          <Col span={8}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col style={{ marginTop: 6 }}>
                                  <ITitle
                                    level={2}
                                    title="Khu vực"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col>
                                  <Form.Item>
                                    {getFieldDecorator("city_agency", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn tỉnh/TP",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Tỉnh/ Thành Phố{" "}
                                          <span
                                            style={{
                                              color: "red",
                                              fontSize: 14,
                                            }}
                                          >
                                            *
                                          </span>
                                        </p>
                                        <ISelectCity
                                          className="clear-pad"
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                            height: 40,
                                          }}
                                          type="write"
                                          select={true}
                                          placeholder="chọn tỉnh/TP"
                                          isBackground={false}
                                          onChange={(key) => {
                                            reduxForm.setFieldsValue({
                                              city_agency: key,
                                            });
                                            _getAPIListCounty(
                                              reduxForm.getFieldValue(
                                                "city_agency"
                                              ),
                                              "",
                                              (data) => {
                                                setDataCities(data);
                                                // reduxForm.setFieldsValue({
                                                //   district_agency: 0,
                                                // });
                                              }
                                            );
                                          }}
                                          value={reduxForm.getFieldValue(
                                            "city_agency"
                                          )}
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                                <Col>
                                  <Form.Item>
                                    {getFieldDecorator("district_agency", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn quận/huyện",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Quận/ Huyện{" "}
                                          <span
                                            style={{
                                              color: "red",
                                              fontSize: 14,
                                            }}
                                          >
                                            *
                                          </span>
                                        </p>
                                        <ISelect
                                          data={dataCities}
                                          select={true}
                                          // onSearch={(e) => {
                                          //   _getAPIListCounty(
                                          // reduxForm.getFieldValue(
                                          //   "city_agency"
                                          // ),
                                          //     e,
                                          //     (data) => {
                                          //       setDataCities(data);
                                          //     }
                                          //   );
                                          // }}
                                          onChange={(key) => {
                                            reduxForm.setFieldsValue({
                                              district_agency: key,
                                            });
                                            _getAPIListWard(
                                              reduxForm.getFieldValue(
                                                "district_agency"
                                              ),
                                              "",
                                              (data) => {
                                                setDataWard(data);
                                                // reduxForm.setFieldsValue({
                                                //   ward_agency: 0,
                                                // });
                                              }
                                            );
                                          }}
                                          isBackground={false}
                                          placeholder="chọn quận/huyện"
                                          value={reduxForm.getFieldValue(
                                            "district_agency"
                                          )}
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                                <Col>
                                  <Form.Item>
                                    {getFieldDecorator("ward_agency", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "chọn phường / xã",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Phường / Xã{" "}
                                          <span
                                            style={{
                                              color: "red",
                                              fontSize: 14,
                                            }}
                                          >
                                            *
                                          </span>
                                        </p>
                                        <ISelect
                                          className="clear-pad"
                                          data={dataWard}
                                          style={{
                                            background: colors.white,
                                            borderStyle: "solid",
                                            borderWidth: 0,
                                            borderColor: colors.line,
                                            borderBottomWidth: 1,
                                          }}
                                          type="write"
                                          select={true}
                                          placeholder="chọn phường / xã"
                                          isBackground={false}
                                          onChange={(key) => {
                                            reduxForm.setFieldsValue({
                                              ward_agency: key,
                                            });
                                          }}
                                          value={reduxForm.getFieldValue(
                                            "ward_agency"
                                          )}
                                        />
                                      </div>
                                    )}
                                  </Form.Item>
                                </Col>
                                <div style={{ marginTop: 42 }}>
                                  <Row gutter={[12, 20]}>
                                    <Col style={{ marginTop: 6 }}>
                                      <ITitle
                                        level={2}
                                        title="Hình ảnh đại lý"
                                        style={{
                                          fontWeight: "bold",
                                        }}
                                      />
                                    </Col>
                                    <Col>
                                      <Form.Item>
                                        {getFieldDecorator("avatar", {
                                          rules: [
                                            {
                                              required: false,
                                              message: "chọn hình",
                                            },
                                          ],
                                        })(
                                          <div>
                                            {add ? (
                                              <IUpload
                                                type={ImageType.USER}
                                                callback={(array) => {
                                                  setImages(array);
                                                  reduxForm.setFieldsValue({
                                                    avatar: array[0],
                                                  });
                                                }}
                                              />
                                            ) : (
                                              <IUpload
                                                type={ImageType.USER}
                                                callback={(array) => {
                                                  setImages(array);
                                                  reduxForm.setFieldsValue({
                                                    avatar: array[0],
                                                  });
                                                }}
                                                name={reduxForm.getFieldValue(
                                                  "avatar"
                                                )}
                                                url={url}
                                              />
                                            )}
                                          </div>
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                </div>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </StyledForm>
        </Col>
      </Row>
    </div>
  );
}

const createAgencyS2 = Form.create({ name: "CreateAgencyS2" })(CreateAgencyS2);

export default createAgencyS2;
