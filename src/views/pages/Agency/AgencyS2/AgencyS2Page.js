import React, { useState, useEffect, Component } from "react";
import { Container, Row, Col } from "reactstrap";
import {
  ITitle,
  ISearch,
  ISvg,
  ICascader,
  ISelectBranch,
  IButton,
  ITable,
  ISelectCity,
  ISelect,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import { message, Popconfirm } from "antd";
import { useHistory } from "react-router-dom";

const widthScreen = window.innerWidth;

function AgencyS2Page(props) {
  let timeSearch = null;
  const history = useHistory();
  const [selectedRowKeys, setSelectRowKeys] = useState([]);
  const [arrRemove, setArrRemove] = useState([]);
  const [dataTable, setDataTable] = useState([]);
  const [isLoadingTable, setLoadingTable] = useState(true);
  const [pagination, setPagination] = useState({
    total: 0,
    size: 0,
  });
  const [isLoadingRemove, setIsLoadingRemove] = useState(false);
  const [dataBranch, setDataBranch] = useState([]);

  const [filterTable, setFilterTable] = useState({
    page: 1,
    city_id: 0,
    // district_id: 0,
    supplier_id: 0,
    keySearch: "",
    status: 2,
  });

  const status = [
    {
      key: 2,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Chờ duyệt",
    },
    {
      key: 1,
      value: "Đang hoạt động",
    },
  ];

  useEffect(() => {
    _getAPIListSuppliers();
  }, [filterTable]);

  const _getAPIListBranch = async () => {
    try {
      const data = await APIService._getBranchDropList();
      const dataNew = data.agency.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDataBranch(dataNew);
    } catch (error) {}
  };

  const _getAPIListSuppliers = async () => {
    try {
      const data = await APIService._getListSuppliers(
        filterTable.city_id,
        // filterTable.district_id,
        filterTable.supplier_id,
        filterTable.keySearch,
        filterTable.page,
        filterTable.status
      );
      let suppliers = data.suppliers;
      suppliers.map((item, index) => {
        suppliers[index].stt = (filterTable.page - 1) * 10 + index + 1;
        suppliers[index].sttAndId = {
          id: item.id,
          stt: index,
        };
      });
      setDataTable([...suppliers]);
      setPagination({
        ...pagination,
        total: data.total,
        size: data.size,
      });
      setLoadingTable(false);
      _getAPIListBranch();
    } catch (error) {
      message.error(error);
      setLoadingTable(false);
    }
  };

  const onChangeCity = (key, value) => {
    setLoadingTable(true);
    setFilterTable({ ...filterTable, city_id: key, page: 1 });
  };
  const onChangeBranch = (key, value) => {
    setLoadingTable(true);

    setFilterTable({ ...filterTable, supplier_id: key, page: 1 });
  };
  const onChangePage = (page) => {
    setLoadingTable(true);

    setFilterTable({ ...filterTable, page: page });
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tên đại lý",
      dataIndex: "supplierName",
      key: "supplierName",
      render: (supplierName) => (
        <span>{!supplierName ? "-" : supplierName}</span>
      ),
    },
    {
      title: "Mã đại lý",
      dataIndex: "supplierCode",
      key: "supplierCode",
      render: (supplierCode) => (
        <span>{!supplierCode ? "-" : supplierCode}</span>
      ),
    },
    {
      title: "Trạng Thái",
      dataIndex: "statusName",
      key: "statusName",
      render: (statusName) => <span>{!statusName ? "-" : statusName}</span>,
    },
    {
      title: "Tỉnh/ Thành",
      dataIndex: "cityName",
      key: "cityName",
      render: (cityName) => <span>{!cityName ? "-" : cityName}</span>,
    },
    {
      title: "Đại diện",
      dataIndex: "user_represent",
      key: "user_represent",
      render: (user_represent) => (
        <span>{!user_represent ? "-" : user_represent}</span>
      ),
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      align: "center",
      render: (phone) => <span>{!phone ? "-" : phone}</span>,
    },
    {
      title: "Đại lý cấp 1 trực thuộc",
      dataIndex: "supplierNameFull",
      key: "supplierNameFull",
      render: (supplierNameFull) => (
        <span>{!supplierNameFull ? "-" : supplierNameFull}</span>
      ),
    },
    {
      title: "Phòng kinh doanh",
      dataIndex: "agencyName",
      key: "agencyName",
      render: (agencyName) => <span>{!agencyName ? "-" : agencyName}</span>,
    },
  ];

  const triggerDelete = (objRemove) => {
    try {
      APIService._updateStatusSuppliers(objRemove);

      message.success("Xóa thành công");
      setIsLoadingRemove(false);
      setLoadingTable(true);
      setSelectRowKeys([]);

      _getAPIListSuppliers();
    } catch (error) {
      console.log(error);

      setIsLoadingRemove(false);
    }
  };

  const onSelectChange = (selectedRowKeys) => {
    // let arrID = [];
    // selectedRowKeys.map((item, index) => {
    //
    //   const Id = dataTable[item].id;
    //   arrID.push(Id);
    // });

    setSelectRowKeys(selectedRowKeys);
    setArrRemove(selectedRowKeys);
    console.log(selectedRowKeys);
  };

  const _onFilter = async (value, selectedOptions) => {
    try {
      const city = selectedOptions.length > 0 ? selectedOptions[0].value : 0;
      const district =
        selectedOptions.length > 1 ? selectedOptions[1].value : 0;
      setLoadingTable(true);
      setFilterTable({ ...filterTable, city_id: city, district_id: district });
    } catch (err) {
      console.log(err);
    }
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    // hideDefaultSelections: false,
  };
  return (
    <Container fluid>
      <Row className="p-0">
        <ITitle
          level={1}
          title="Đại lý cấp 2"
          style={{
            color: colors.main,
            fontWeight: "bold",
            flex: 1,
            display: "flex",
            alignItems: "flex-end",
          }}
        />
        <ISearch
          onChange={(e) => {
            let search = e.target.value;
            if (timeSearch) {
              clearTimeout(timeSearch);
            }
            timeSearch = setTimeout(
              () =>
                setFilterTable({ ...filterTable, keySearch: search, page: 1 }),
              500
            );
          }}
          placeholder="Nhập từ khóa"
          onPressEnter={() => _getAPIListSuppliers()}
          icon={
            <div
              style={{
                display: "flex",
                width: 42,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <img src={images.icSearch} style={{ width: 20, height: 20 }} />
            </div>
          }
        />
      </Row>

      <Row className="center" style={{ marginTop: 36 }}>
        <Col
          style={{
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Row>
            <Col className="mt-0 center mr-3 ml-4 p-0" xs="auto">
              <Row className="center">
                <ITitle title="Trạng thái" level={4} />
              </Row>
            </Col>
            <Col>
              <div style={{ width: 160 }}>
                <ISelect
                  defaultValue="Tất cả"
                  data={status}
                  select={true}
                  onChange={(value) => {
                    setLoadingTable(true);
                    setFilterTable({
                      ...filterTable,
                      status: value,
                      page: 1,
                    });
                  }}
                />
              </div>
            </Col>
            <Col className="mt-0 center mr-4 ml-4 p-0" xs="auto">
              <Row className="center">
                <ISvg
                  name={ISvg.NAME.LOCATION}
                  width={20}
                  height={20}
                  fill={colors.icon.default}
                />
                <div style={{ marginLeft: 10 }}>
                  <ITitle title="Khu vực" level={4} />
                </div>
              </Row>
            </Col>
            <Col className="mt-0 p-0">
              {/* <ICascader
                style={{
                  background: colors.white,
                  borderStyle: "solid",
                  borderWidth: 1,
                  borderColor: colors.line,
                }}
                level={2}
                type="city"
                className="cascader-padding"
                onChange={(value, selectedOptions) => {
                  _onFilter(value, selectedOptions);
                }}
                placeholder="Chọn khu vực"
              /> */}
              <ISelectCity
                defaultValue="Chọn khu vực"
                onChange={onChangeCity}
                fillerAll={true}
                select={true}
                select={true}
              />
            </Col>
            <Col>
              <div style={{ width: 160 }}>
                <ISelectBranch
                  select={true}
                  defaultValue="Phòng kinh doanh"
                  style={{
                    background: colors.white,
                    borderWidth: 1,
                    borderColor: colors.line,
                    borderStyle: "solid",
                  }}
                  onChange={onChangeBranch}
                  data={dataBranch}
                />
              </div>
            </Col>

            <Col xs="auto">
              <Row>
                <IButton
                  icon={ISvg.NAME.ARROWUP}
                  title="Tạo mới"
                  color={colors.main}
                  // style={{ marginRight: 12 }}
                  onClick={() => {
                    history.push("agencyS2/add/0");
                  }}
                />{" "}
                {/* <IButton
                  icon={ISvg.NAME.DOWLOAND}
                  title='Xuất file'
                  color={colors.main}
                  onClick={() => {
                    message.warning('Chức năng đang cập nhật')
                  }}
                /> */}
                {/* <Popconfirm
                  placement="bottom"
                  title={"Nhấn đồng ý để xóa ."}
                  onConfirm={() => {
                    if (selectedRowKeys.length === 0) {
                      message.warning("Bạn chưa chọn đại lý để xóa.");
                      return;
                    }
                    const objRemove = {
                      id: arrRemove,
                      status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                    };
                    setIsLoadingRemove(true);
                    triggerDelete(objRemove);
                  }}
                  okText="Đồng ý"
                  cancelText="Hủy"
                >
                  <IButton
                    icon={ISvg.NAME.DELETE}
                    title="Xóa"
                    color={colors.oranges}
                    style={{}}
                    loading={isLoadingRemove}
                  />
                </Popconfirm> */}
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>

      <Row style={{ marginTop: 36 }}>
        <ITable
          data={dataTable}
          columns={columns}
          style={{ width: "100%" }}
          rowKey={"id"}
          defaultCurrent={1}
          loading={isLoadingTable}
          maxpage={pagination.total}
          sizeItem={pagination.size}
          indexPage={filterTable.page}
          onRow={(record, rowIndex) => {
            return {
              onClick: (event) => {
                const indexRow = Number(
                  event.currentTarget.attributes[1].ownerElement.dataset.rowKey
                );
                props.history.push("/DetailAgencyS2/" + indexRow);
              }, // click row
              onMouseDown: (event) => {
                const indexRow = Number(
                  event.currentTarget.attributes[1].ownerElement.dataset.rowKey
                );

                if (event.button == 2 || event == 4) {
                  window.open("/DetailAgencyS2/" + indexRow, "_blank");
                }
              },
            };
          }}
          onChangePage={(page) => {
            setLoadingTable(true);
            onChangePage(page);
          }}
        />
      </Row>
    </Container>
  );
}

export default AgencyS2Page;
