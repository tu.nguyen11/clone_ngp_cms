import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message, Modal } from "antd";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IButton,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import { useHistory } from "react-router-dom";

function ListEndUsersPage(props) {
  const history = useHistory();

  const [filter, setFilter] = useState({
    key: "",
    page: 1,
    status: -1,
    citi_id: -1,
    district_id: -1,
  });

  const status = [
    {
      key: -1,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Ngưng hoạt động",
    },
    {
      key: 1,
      value: "Đang hoạt động",
    },
  ];

  const [dataTable, setDataTable] = useState({
    listEndUser: [],
    total: 1,
    size: 10,
    image_url: "",
  });

  const [dataCities, setDataCities] = useState([]);
  const [dataDistricts, setDataDistricts] = useState([]);

  const [loadingDistrict, setLoadingDistrict] = useState(false);
  const [loadingTable, setLoadingTable] = useState(true);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      console.log(selectedRowKeys);
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const getListEndUser = async (filter) => {
    try {
      const data = await APIService._getListEndUser(filter);

      data.listFarmer.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        return item;
      });

      setDataTable({
        listEndUser: data.listFarmer,
        size: data.size,
        total: data.total,
        image_url: data.image_url,
      });
      getListCities();
      setLoadingTable(false);
      console.log("data: ", data);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const getListCities = async () => {
    try {
      const data = await APIService._getListCities("");
      const listCities = data.cities.map((item) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      listCities.unshift({
        key: -1,
        value: "Tất cả",
      });
      setDataCities(listCities);
    } catch (err) {
      console.log(err);
    }
  };

  const getListDistricts = async (citi_id, key) => {
    try {
      const data = await APIService._getListCounty(citi_id, key);
      const listDistricts = data.district.map((item) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      listDistricts.unshift({
        key: -1,
        value: "Tất cả",
      });
      setDataDistricts(listDistricts);
      setLoadingDistrict(false);
    } catch (err) {
      console.log(err);
      setLoadingDistrict(false);
    }
  };

  const postRemoveEndUser = async (obj) => {
    try {
      const data = await APIService._postRemoveEndUser(obj);
      setSelectedRowKeys([]);
      await getListEndUser(filter);
      message.success("Xóa thành công");
    } catch (err) {}
  };

  useEffect(() => {
    getListEndUser(filter);
  }, [filter]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },
    {
      title: "Mã người dùng",
      dataIndex: "code",
      key: "code",
      render: (code) => <span>{code}</span>,
    },
    {
      title: "Tên người dùng",
      dataIndex: "name",
      key: "name",
      render: (name) => <span>{name}</span>,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <span>{phone}</span>,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      render: (email) => <span>{email}</span>,
    },
    {
      title: "Hình ảnh",
      dataIndex: "image",

      key: "image",
      render: (image) => {
        return (
          <div>
            <img
              src={dataTable.image_url + image}
              alt="avatar"
              style={{
                width: 35,
                height: 35,
                borderRadius: 20,
              }}
            />
          </div>
        );
      },
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Phường/Xã",
      dataIndex: "ward_name",
      key: "ward_name",
    },
    {
      title: "Quận/Huyện",
      dataIndex: "district_name",
      key: "district_name",
    },
    {
      title: "Tỉnh/Thành",
      dataIndex: "city_name",
      key: "city_name",
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Danh sách người dùng cuối
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên, mã người dùng cuối">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên, mã người dùng cuối"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={18}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <ISvg
                        name={ISvg.NAME.EXPERIMENT}
                        with={20}
                        height={20}
                        fill={colors.icon.default}
                      />
                      <ITitle
                        title="Lọc theo"
                        level={4}
                        style={{
                          marginLeft: 20,
                        }}
                      />
                      <div style={{ width: 200, margin: "0px 15px 0px 15px" }}>
                        <ISelect
                          defaultValue="Tỉnh/Thành"
                          data={dataCities}
                          select={true}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setFilter({
                              ...filter,
                              citi_id: value,
                              district_id: -1,
                              page: 1,
                            });
                          }}
                          onDropdownVisibleChange={(open) => {
                            if (!open) {
                              if (dataCities.length === 0) {
                                return;
                              }
                              if (filter.citi_id === -1) {
                                return;
                              }
                              setLoadingDistrict(true);
                              getListDistricts(filter.citi_id, "");
                              return;
                            }
                          }}
                        />
                      </div>
                      <div style={{ width: 200 }}>
                        <ISelect
                          defaultValue="Quận/Huyện"
                          data={dataDistricts}
                          select={filter.citi_id !== -1 ? true : false}
                          loading={loadingDistrict}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setFilter({
                              ...filter,
                              district_id: value,
                              page: 1,
                            });
                          }}
                          onDropdownVisibleChange={(open) => {
                            if (open) {
                              if (filter.citi_id === -1) return;
                              setLoadingDistrict(true);
                              getListDistricts(filter.citi_id, "");
                              return;
                            }
                          }}
                          value={
                            dataDistricts.length === 0
                              ? "Quận/Huyện"
                              : filter.district_id
                          }
                        />
                      </div>
                      <div style={{ width: 200, marginLeft: 15 }}>
                        <ISelect
                          defaultValue="Trạng thái"
                          data={status}
                          select={true}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setFilter({
                              ...filter,
                              status: value,
                              page: 1,
                            });
                          }}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className="flex justify-end">
                      <IButton
                        icon={ISvg.NAME.DELETE}
                        title={"Xóa"}
                        color={colors.red}
                        styleHeight={{
                          width: 120,
                        }}
                        onClick={() => {
                          let arrayID = [];

                          selectedRowKeys.map((item, index) => {
                            arrayID.push(dataTable.listEndUser[item]);
                          });
                          let arrNotRemove = arrayID.filter((item) => {
                            return item.status_name === "Đang hoạt động";
                          });
                          if (arrNotRemove.length > 0) {
                            Modal.error({
                              title: "Thất bại",
                              content:
                                "Bạn không thể xóa người dùng đang hoạt động",
                            });
                            return;
                          }
                          if (arrayID.length === 0) {
                            message.warning(
                              "Bạn chưa chọn mã người dùng để xóa."
                            );
                            return;
                          }
                          let arrID = arrayID.map((item) => item.id);
                          const objRemove = {
                            ids: arrID,
                            status: -1,
                          };
                          postRemoveEndUser(objRemove);
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listEndUser}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.listEndUser[indexRow].id;
                          history.push("/end/user/detail/" + id);
                        }, // click row
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default ListEndUsersPage;
