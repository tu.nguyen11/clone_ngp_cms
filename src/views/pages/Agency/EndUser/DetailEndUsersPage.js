import React, { useState, useEffect } from "react";
import { ITitle, IButton, ISvg } from "../../../components";
import { Row, Col, Popconfirm, message, Modal, Skeleton } from "antd";
import { colors, images } from "../../../../assets";
import { useParams, useHistory } from "react-router-dom";
import { APIService } from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";
import ITransfer from "../../../components/styled/modalTransfer/ITransfer";
import { priceFormat } from "../../../../utils";

const DetailEndUsersPage = (props) => {
  const paramsEndUser = useParams();
  const history = useHistory();
  const toDate = new Date();

  const [isLoading, setIsLoading] = useState(true);

  const [isLoadingRemove, setIsLoadingRemove] = useState(false);
  const [dataDetail, setDataDetail] = useState({
    farmer_response: {},
    image_url: "",
    loading: false,
  });

  useEffect(() => {
    _getAPIDetailEndUser(paramsEndUser.id);
  }, []);

  const _getAPIDetailEndUser = async (id) => {
    try {
      const data = await APIService._getDetailEndUser(id);

      setDataDetail(data);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const postRemoveEndUser = async (obj) => {
    try {
      const data = await APIService._postRemoveEndUser(obj);
      setIsLoadingRemove(false);
      message.success("Xóa thành công");
      history.push("/end/user/list");
    } catch (err) {
      setIsLoadingRemove(false);
    }
  };

  const { farmer_response } = dataDetail;
  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 26]}>
        <div style={{ marginTop: 12 }}>
          <Col>
            <ITitle
              level={1}
              title="Chi tiết người dùng cuối"
              style={{ color: colors.main, fontWeight: "bold" }}
            />
          </Col>
          <Col>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              <IButton
                icon={ISvg.NAME.WRITE}
                title="Chỉnh sửa"
                onClick={() =>
                  props.history.push("/end/user/edit/" + paramsEndUser.id)
                }
                color={colors.main}
              />
              <IButton
                icon={ISvg.NAME.WRITE}
                title="Xóa"
                color={colors.red}
                loading={isLoadingRemove}
                styleHeight={{
                  width: 120,
                  marginLeft: 25,
                }}
                onClick={() => {
                  setIsLoadingRemove(true);
                  if (farmer_response.status === 1) {
                    Modal.error({
                      title: "Thất bại",
                      content: "Bạn không thể xóa người dùng đang hoạt động",
                    });
                    setIsLoadingRemove(false);
                    return;
                  } else {
                    const objRemove = {
                      ids: [Number(paramsEndUser.id)],
                      status: -1,
                    };
                    postRemoveEndUser(objRemove);
                  }
                }}
              />
            </div>
          </Col>
          <Col span={24} xxl={22}>
            <div
              style={{
                width: 850,
                height: "100%",
                background: colors.white,
                boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                height: "100%",
              }}
            >
              <Row type="flex">
                <Col span={15}>
                  <div
                    style={{
                      padding: "30px 40px",
                      background: colors.white,
                      borderRight: "1px solid #f0f0f0",
                      height: "100%",
                    }}
                  >
                    {isLoading ? (
                      <div>
                        <Skeleton
                          loading={true}
                          active
                          paragraph={{ rows: 16 }}
                        />
                      </div>
                    ) : (
                      <Row gutter={[12, 20]}>
                        <Col>
                          <ITitle
                            level={2}
                            title="Thông tin người dùng"
                            style={{
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col style={{ marginTop: 8 }}>
                          <p style={{ fontWeight: 600 }}>Tên người dùng</p>
                          <span>
                            {!farmer_response.name ? "-" : farmer_response.name}
                          </span>
                        </Col>
                        <Col>
                          <Row gutter={[12, 6]}>
                            <Col span={12}>
                              <p style={{ fontWeight: 600 }}>Mã người dùng</p>
                              <span>
                                {!farmer_response.code
                                  ? "-"
                                  : farmer_response.code}
                              </span>
                            </Col>
                            <Col span={12}>
                              <p style={{ fontWeight: 600 }}>Điện thoại</p>
                              <span>
                                {!farmer_response.phone
                                  ? "-"
                                  : farmer_response.phone}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                        <Col>
                          <Row gutter={[12, 6]}>
                            <Col span={12}>
                              <p style={{ fontWeight: 600 }}>Giới tính</p>
                              <span>
                                {!farmer_response.gender
                                  ? "-"
                                  : farmer_response.gender}
                              </span>
                            </Col>
                            <Col span={12}>
                              <p style={{ fontWeight: 600 }}>Địa chỉ</p>
                              <span>
                                {!farmer_response.address
                                  ? "-"
                                  : farmer_response.address}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                        <Col>
                          <Row gutter={[12, 6]}>
                            <Col span={12}>
                              <p style={{ fontWeight: 600 }}>Mật khẩu</p>
                              <span>******</span>
                            </Col>
                            <Col span={12}>
                              <p style={{ fontWeight: 600 }}>Trạng thái</p>
                              <span>
                                {!farmer_response.status_name
                                  ? "-"
                                  : farmer_response.status_name}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Email</p>
                          <span>
                            {!farmer_response.email
                              ? "-"
                              : farmer_response.email}
                          </span>
                        </Col>

                        <Col style={{ marginTop: 25 }}>
                          <Row>
                            <Col span={20}>
                              <ITitle
                                level={2}
                                title="Sản phẩm quan tâm"
                                style={{
                                  fontWeight: "bold",
                                }}
                              />
                            </Col>
                            <Col span={4}>
                              <div
                                className="flex justify-end"
                                style={{
                                  paddingTop: 4,
                                }}
                              >
                                <ISvg
                                  name={ISvg.NAME.LISTVIEW}
                                  with={20}
                                  height={20}
                                  fill={colors.icon.default}
                                />
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    )}
                  </div>
                </Col>
                <Col span={9}>
                  <div style={{ padding: "30px 40px" }}>
                    {isLoading ? (
                      <div>
                        <Skeleton
                          loading={true}
                          active
                          paragraph={{ rows: 16 }}
                        />
                      </div>
                    ) : (
                      <Row gutter={[12, 20]}>
                        <Col>
                          <ITitle
                            level={2}
                            title="Khu vực"
                            style={{
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col style={{ marginTop: 8 }}>
                          <p style={{ fontWeight: 600 }}>Tỉnh/ Thành phố</p>
                          <span>
                            {!farmer_response.city_name
                              ? "-"
                              : farmer_response.city_name}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Quận/ Huyện</p>
                          <span>
                            {!farmer_response.district_name
                              ? "-"
                              : farmer_response.district_name}
                          </span>
                        </Col>
                        <Col>
                          <p style={{ fontWeight: 600 }}>Phường/ Xã</p>
                          <span>
                            {!farmer_response.ward_name
                              ? "-"
                              : farmer_response.ward_name}
                          </span>
                        </Col>
                        <Col style={{ marginTop: 35 }}>
                          <ITitle
                            level={2}
                            title="Hình ảnh người dùng cuối"
                            style={{
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col style={{ marginTop: 22 }}>
                          <img
                            src={
                              !farmer_response.image
                                ? images.avatar
                                : dataDetail.image_url + farmer_response.image
                            }
                            style={{ width: 165, height: 165 }}
                          />
                        </Col>
                      </Row>
                    )}
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </div>
      </Row>
    </div>
  );
};

export default DetailEndUsersPage;
