import React, { useState, useEffect, useLayoutEffect } from "react";
import { ITitle, IButton, ISvg, ISelect, IUpload } from "../../../components";
import { IFormItem, IInputText } from "../../../components/common";
import { Row, Col, Skeleton, Form, message } from "antd";
import { colors, images } from "../../../../assets";
import { useParams, useHistory } from "react-router-dom";
import { APIService } from "../../../../services";
import { ImageType } from "../../../../constant";

const EditEndUserPage = (props) => {
  const paramsEndUser = useParams();
  const history = useHistory();
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;

  const [isLoading, setIsLoading] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);

  const [url, setUrl] = useState("");
  const [dataCities, setDataCities] = useState([]);
  const [dataDistricts, setDataDistricts] = useState([]);
  const [dataWards, setDataWards] = useState([]);

  const status = [
    { key: 0, value: "Ngưng hoạt động" },
    { key: 1, value: "Đang hoạt động" },
  ];

  const gender = [
    { key: 0, value: "Khác" },
    { key: 1, value: "Nam" },
    { key: 2, value: "Nữ" },
  ];

  useEffect(() => {
    if (Object.keys(reduxForm.getFieldsValue()).length === 0) {
      _getAPIDetailEndUser(paramsEndUser.id);
    }
  }, [reduxForm]);

  const getListCities = async () => {
    try {
      const data = await APIService._getListCities("");
      const listCities = data.cities.map((item) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setDataCities(listCities);
    } catch (err) {
      console.log(err);
    }
  };

  const getListDistricts = async (city_id, key) => {
    try {
      const data = await APIService._getListCounty(city_id, key);
      const listDistricts = data.district.map((item) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setDataDistricts(listDistricts);
    } catch (err) {
      console.log(err);
    }
  };

  const getAPIListWard = async (district_id, search) => {
    try {
      const data = await APIService._getListWard(district_id, search);
      const dataNew = data.wards.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setDataWards(dataNew);
    } catch (err) {
      console.log(err);
    }
  };

  const _getAPIDetailEndUser = async (id) => {
    try {
      const data = await APIService._getDetailEndUser(id);
      reduxForm.setFieldsValue({
        user_name: data.farmer_response.name,
        user_code: data.farmer_response.code,
        phone: data.farmer_response.phone,
        gender:
          data.farmer_response.gender === "Nam"
            ? 1
            : data.farmer_response.gender === "Nữ"
            ? 2
            : 0,
        address: data.farmer_response.address,
        password: "",
        status: data.farmer_response.status,
        email: data.farmer_response.email,
        city_id: data.farmer_response.city_id,
        district_id: data.farmer_response.district_id,
        ward_id: data.farmer_response.ward_id,
        avatar: data.farmer_response.image,
      });
      setUrl(data.image_url);
      getListCities();
      getListDistricts(data.farmer_response.city_id, "");
      getAPIListWard(data.farmer_response.district_id, "");
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const postUpdateEndUser = async (obj) => {
    try {
      const data = await APIService._postUpdateEndUser(obj);
      setLoadingButton(false);
      message.success("Cập nhật thành công");
      history.push("/end/user/detail/" + paramsEndUser.id);
    } catch (error) {
      setLoadingButton(false);
    }
  };

  const handleSubmit = (e) => {
    setLoadingButton(true);
    e.preventDefault();
    reduxForm.validateFields((err, values) => {
      if (err) {
        setLoadingButton(false);
        return;
      }
      console.log("gender: ", values.gender);
      console.log("status: ", values.status);
      const obj = {
        id: Number(paramsEndUser.id),
        code: !values.user_code ? "" : values.user_code,
        name: !values.user_name ? "" : values.user_name,
        phone: !values.phone ? "" : values.phone,
        gender: values.gender,
        address: !values.address ? "" : values.address,
        password: "",
        status: values.status,
        email: !values.email ? "" : values.email,
        citi_id: values.city_id,
        district_id: values.district_id,
        ward_id: values.ward_id,
        image: !values.avatar ? "" : values.avatar,
      };
      console.log("obj: ", obj);
      postUpdateEndUser(obj);
    });
  };

  return (
    <div style={{ width: "100%" }}>
      <Form onSubmit={handleSubmit}>
        <Row gutter={[12, 26]}>
          <div style={{ marginTop: 12 }}>
            <Col>
              <ITitle
                level={1}
                title="Chỉnh sửa người dùng cuối"
                style={{ color: colors.main, fontWeight: "bold" }}
              />
            </Col>

            <Col>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  icon={ISvg.NAME.SAVE}
                  title="Lưu"
                  htmlType="submit"
                  color={colors.main}
                />
              </div>
            </Col>

            <Col span={24} xxl={22}>
              <div
                style={{
                  width: 850,
                  height: "100%",
                  background: colors.white,
                  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  height: "100%",
                }}
              >
                <Row type="flex">
                  <Col span={15}>
                    <div
                      style={{
                        padding: "30px 40px",
                        background: colors.white,
                        borderRight: "1px solid #f0f0f0",
                        height: "100%",
                      }}
                    >
                      {isLoading ? (
                        <div>
                          <Skeleton
                            loading={true}
                            active
                            paragraph={{ rows: 16 }}
                          />
                        </div>
                      ) : (
                        <Row gutter={[12, 20]}>
                          <Col>
                            <ITitle
                              level={2}
                              title="Thông tin người dùng"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col style={{ marginTop: 8 }}>
                            <IFormItem>
                              {getFieldDecorator("user_name", {
                                rules: [
                                  {
                                    required: false,
                                    message: "nhập tên người dùng",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <ITitle
                                      level={4}
                                      title={"Tên người dùng"}
                                      style={{
                                        fontWeight: 600,
                                      }}
                                    />
                                    <IInputText
                                      disabled={true}
                                      style={{ color: "#000" }}
                                      // placeholder="nhập tên người dùng"
                                      value={reduxForm.getFieldValue(
                                        "user_name"
                                      )}
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          <Col>
                            <Row gutter={[12, 6]}>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("user_code", {
                                    rules: [
                                      {
                                        required: false,
                                        message: "nhập mã người dùng",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Mã người dùng"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <IInputText
                                          disabled={true}
                                          style={{ color: "#000" }}
                                          // placeholder="nhập mã người dùng"
                                          value={reduxForm.getFieldValue(
                                            "user_code"
                                          )}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("phone", {
                                    rules: [
                                      {
                                        required: false,
                                        message: "nhập số điện thoại",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Điện thoại"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <IInputText
                                          disabled={true}
                                          style={{ color: "#000" }}
                                          // placeholder="nhập số điện thoại"
                                          value={reduxForm.getFieldValue(
                                            "phone"
                                          )}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                            </Row>
                          </Col>
                          <Col>
                            <Row gutter={[12, 6]}>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("gender", {
                                    rules: [
                                      {
                                        required: false,
                                        message: "nhập giới tính",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Giới tính"}
                                          style={{
                                            fontWeight: 600,
                                            marginBottom: 1,
                                          }}
                                        />
                                        <ISelect
                                          data={gender}
                                          select={false}
                                          isBackground={false}
                                          // placeholder="chọn giới tính"
                                          value={reduxForm.getFieldValue(
                                            "gender"
                                          )}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("address", {
                                    rules: [
                                      {
                                        required: false,
                                        message: "nhập địa chỉ",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Địa chỉ"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <IInputText
                                          disabled={true}
                                          style={{ color: "#000" }}
                                          // placeholder="nhập địa chỉ"
                                          value={reduxForm.getFieldValue(
                                            "address"
                                          )}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                            </Row>
                          </Col>
                          <Col>
                            <Row gutter={[12, 6]}>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("password", {
                                    rules: [
                                      {
                                        required: false,
                                        message: "nhập mật khẩu",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Mật khẩu"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        <IInputText
                                          disabled={true}
                                          style={{ color: "#000" }}
                                          // placeholder="nhập mật khẩu"
                                          value={"******"}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                              <Col span={12}>
                                <IFormItem>
                                  {getFieldDecorator("status", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "nhập trạng thái",
                                      },
                                    ],
                                  })(
                                    <div>
                                      <Row>
                                        <ITitle
                                          level={4}
                                          title={"Trạng thái"}
                                          style={{
                                            fontWeight: 600,
                                            marginBottom: 1,
                                          }}
                                        />
                                        <ISelect
                                          data={status}
                                          select={true}
                                          onChange={(key) => {
                                            reduxForm.setFieldsValue({
                                              status: key,
                                            });
                                          }}
                                          isBackground={false}
                                          placeholder="chọn trạng thái"
                                          value={reduxForm.getFieldValue(
                                            "status"
                                          )}
                                        />
                                      </Row>
                                    </div>
                                  )}
                                </IFormItem>
                              </Col>
                            </Row>
                          </Col>
                          <Col>
                            <IFormItem>
                              {getFieldDecorator("email", {
                                rules: [
                                  {
                                    required: false,
                                    message: "nhập email",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <ITitle
                                      level={4}
                                      title={"Email"}
                                      style={{
                                        fontWeight: 600,
                                        marginTop: 1,
                                      }}
                                    />
                                    <IInputText
                                      disabled={true}
                                      style={{ color: "#000" }}
                                      // placeholder="nhập email"
                                      value={reduxForm.getFieldValue("email")}
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          <Col style={{ marginTop: 25 }}>
                            <Row>
                              <Col span={20}>
                                <ITitle
                                  level={2}
                                  title="Sản phẩm quan tâm"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col span={4}>
                                <div
                                  className="flex justify-end"
                                  style={{
                                    paddingTop: 4,
                                  }}
                                >
                                  <ISvg
                                    name={ISvg.NAME.LISTVIEW}
                                    with={20}
                                    height={20}
                                    fill={colors.icon.default}
                                  />
                                </div>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </div>
                  </Col>
                  <Col span={9}>
                    <div style={{ padding: "30px 40px" }}>
                      {isLoading ? (
                        <div>
                          <Skeleton
                            loading={true}
                            active
                            paragraph={{ rows: 16 }}
                          />
                        </div>
                      ) : (
                        <Row gutter={[12, 20]}>
                          <Col>
                            <ITitle
                              level={2}
                              title="Khu vực"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col style={{ marginTop: 8 }}>
                            <IFormItem>
                              {getFieldDecorator("city_id", {
                                rules: [
                                  {
                                    required: false,
                                    message: "nhập Tỉnh/Thành phố",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <ITitle
                                      level={4}
                                      title={"Tỉnh/Thành phố"}
                                      style={{
                                        fontWeight: 600,
                                      }}
                                    />
                                    <ISelect
                                      data={dataCities}
                                      select={false}
                                      isBackground={false}
                                      // placeholder="chọn Tỉnh/Thành phố"
                                      value={reduxForm.getFieldValue("city_id")}
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          <Col>
                            <IFormItem>
                              {getFieldDecorator("district_id", {
                                rules: [
                                  {
                                    required: false,
                                    message: "nhập Quận/Huyện",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <ITitle
                                      level={4}
                                      title={"Quận/Huyện"}
                                      style={{
                                        fontWeight: 600,
                                      }}
                                    />
                                    <ISelect
                                      data={dataDistricts}
                                      select={false}
                                      isBackground={false}
                                      // placeholder="chọn Quận/Huyện"
                                      value={reduxForm.getFieldValue(
                                        "district_id"
                                      )}
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          <Col>
                            <IFormItem>
                              {getFieldDecorator("ward_id", {
                                rules: [
                                  {
                                    required: false,
                                    message: "nhập Phường/Xã",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <ITitle
                                      level={4}
                                      title={"Phường/Xã"}
                                      style={{
                                        fontWeight: 600,
                                      }}
                                    />
                                    <ISelect
                                      data={dataWards}
                                      select={false}
                                      isBackground={false}
                                      // placeholder="chọn Phường/Xã"
                                      value={reduxForm.getFieldValue("ward_id")}
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          <Col style={{ marginTop: 35 }}>
                            <ITitle
                              level={2}
                              title="Hình ảnh người dùng cuối"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col style={{ marginTop: 22 }}>
                            <IFormItem>
                              {getFieldDecorator("avatar", {
                                rules: [
                                  {
                                    required: false,
                                    message: "chọn hình",
                                  },
                                ],
                              })(
                                <div>
                                  <IUpload
                                    disabled={true}
                                    type={ImageType.USER}
                                    name={reduxForm.getFieldValue("avatar")}
                                    url={url}
                                  />
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                        </Row>
                      )}
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </div>
        </Row>
      </Form>
    </div>
  );
};

const editEndUserPage = Form.create({ name: "EditEndUserPage" })(
  EditEndUserPage
);

export default editEndUserPage;
