import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  message,
  DatePicker,
  Form,
  Input,
} from "antd";
import moment from "moment";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  ISelectCity,
  ISelectCounty,
  ILoading,
  ISelectAttrubie,
} from "../../../components";

import { colors } from "../../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";
import { ImageType } from "../../../../constant";

const { RangePicker } = DatePicker;

class EditBranchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      write: true,
      dateEnd: "",
      errEnd: true,
      dateStart: "",
      data: {
        supplier: {
          contract: {},
        },
      },
      loading: false,
      imageEdit: [],
      url: "",
      dataEdit: {},
      listCities: [], // danh sách thành phố
      listCounty: [], // danh sách quận / huyện
      errStart: true,
      errCity: true, // lỗi khi chưa có dữ liệu sẽ báo lỗi
      errCounty: true, // lỗi khi chưa có dữ liệu sẽ báo lỗi
      disabledCounty: false, // disabled quận huyện khi chưa được select thành phố
      cityID: 0, // cityID  thành phố,
      countyID: 0, // countyID lấy quận huyên
      cityName: "",
      keySearchCounty: "",
    };
    // this.check = this.check.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onOkDatePickerStart = this.onOkDatePickerStart.bind(this);
    this.onOkDatePickerEnd = this.onOkDatePickerEnd.bind(this);
    this.onChangeCity = this.onChangeCity.bind(this);
    this.agencyID = Number(this.props.match.params.id);
  }

  // API Start
  componentDidMount() {
    // this._getAPIDetailAgency(this.agencyID);
  }
  _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailSuppliers(idAgency);
      this.setState(
        {
          data: data,
          url: data.image_url,
          imageEdit: [data.supplier.avatar],
          loading: true,
        },
        () => {
          this._getAPIListCounty(
            this.state.data.supplier.cityID,
            this.state.keySearchCounty
          );
        }
      );
    } catch (error) {}
  };

  _postAPIEditSuppliers = async (obj) => {
    const data = await APIService._postEditSuppliers(obj);
    message.success("Cập nhập thành công");
    this.props.history.push("/agencyPage");
  };

  _getAPIListCounty = async (city_id, searh) => {
    const data = await APIService._getListCounty(city_id, searh);
    const dataNew = data.district.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    // this.state.data.supplier.districtID = dataNew[0].key;
    this.setState({
      data: this.state.data,
      listCounty: dataNew,
    });
  };

  onChangeCity = (key, value) => {
    this.state.data.supplier.cityID = key;
    this.setState(
      {
        // data: this.state.data,
        disabledCounty: true,
        errCity: false,
      },
      () => {
        this._getAPIListCounty(
          this.state.data.supplier.cityID,
          this.state.keySearchCounty
        );
        this.props.form.validateFields(["cityID"], { force: true });
      }
    );
  };

  onChangeCounty = (key, value) => {
    this.state.data.supplier.districtID = key;

    this.setState(
      {
        errCounty: false,
      },
      () => {
        this.props.form.validateFields(["districtID"], { force: true });
      }
    );
  };

  onOkDatePickerStart = (date, dateString) => {
    this.setState(
      {
        dateStart: Date.parse(dateString),
        errStart: false,
      },
      () => {
        this.props.form.validateFields(["dateBegin"], { force: true });
      }
    );
  };

  onOkDatePickerEnd = (date, dateString) => {
    this.setState(
      {
        dateEnd: Date.parse(dateString),
        errEnd: false,
      },
      () => {
        this.props.form.validateFields(["dateEnd"], { force: true });
      }
    );
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      const data = {
        id: this.agencyID,
        supplierCode: this.state.data.supplier.supplierCode,
        address: this.state.data.supplier.address,
        avatar: this.state.imageEdit,
        supplierName: this.state.data.supplier.supplierName,
        phone: this.state.data.supplier.phone,
        cityID: this.state.data.supplier.cityID,
        districtID: this.state.data.supplier.districtID,
        email: this.state.data.supplier.email,
        // img: this.state.imageEdit
      };

      this.setState(
        {
          dataEdit: data,
        },
        () => {
          this._postAPIEditSuppliers(this.state.dataEdit);
        }
      );
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Container fluid>
        <div>
          <Row className="mt-3" xs="auto">
            <ITitle
              level={1}
              title="Chỉnh sửa phòng kinh doanh"
              style={{ color: colors.main, fontWeight: "bold" }}
            />
          </Row>

          <Form onSubmit={this.handleSubmit}>
            <Row
              style={{
                display: "flex",
                justifyContent: "flex-end",
                paddingTop: 12,
                paddingBottom: 32,
              }}
              xs="auto"
              sm={12}
              md={12}
            >
              <Form.Item>
                <IButton
                  icon={ISvg.NAME.SAVE}
                  title="Lưu"
                  color={colors.main}
                  htmlType="submit"
                  style={{ marginRight: 20 }}
                  // onClick={() => this.handleSubmit}
                />
              </Form.Item>
              <Form.Item>
                <IButton
                  icon={ISvg.NAME.CROSS}
                  title="Hủy"
                  color={colors.oranges}
                  style={{}}
                  onClick={() => {
                    this.props.history.goBack();
                  }}
                />
              </Form.Item>
            </Row>

            <Row
              style={{ maxWidth: 815, minWidth: 615, flex: 1 }}
              className="p-0 shadow"
            >
              <Col style={{ background: colors.white }} className="pl-5 ml-0">
                <Row className="p-0 m-0" style={{}}>
                  <Col
                    className="pl-0 pr-0 pt-0 m-0"
                    style={{ paddingBottom: 40 }}
                  >
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Thông tin phòng kinh doanh"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>

                    <Form.Item>
                      {getFieldDecorator("supplierName", {
                        rules: [
                          {
                            message: "nhập tên cửa hàng đại lý",
                          },
                        ],
                      })(
                        <Row className="p-0 m-0" xs="auto">
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Tên phòng kinh doanh"}
                                style={{ fontWeight: 500 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                write={this.state.write}
                                value={this.state.data.supplier.supplierName}
                                placeholder="Nhập tên cửa hàng đại lý"
                                onChange={(event) => {
                                  this.state.data.supplier.supplierName =
                                    event.target.value;
                                  this.setState({});
                                }}
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("supplierCode", {
                            rules: [
                              {
                                message: "nhập mã đại lý",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Mã phòng kinh doanh"}
                                  style={{ fontWeight: 500 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  value={this.state.data.supplier.supplierCode}
                                  placeholder="Nhập mã đại lý"
                                  onChange={(event) => {
                                    this.state.data.supplier.supplierCode =
                                      event.target.value;
                                    this.setState({});
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("supplierCode", {
                            rules: [
                              {
                                message: "nhập mã đại lý",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Địa chỉ"}
                                  style={{ fontWeight: 500 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  value={this.state.data.supplier.supplierCode}
                                  placeholder="Nhập mã đại lý"
                                  onChange={(event) => {
                                    this.state.data.supplier.supplierCode =
                                      event.target.value;
                                    this.setState({});
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("phone", {
                            rules: [
                              {
                                message: "nhập số điện thoại",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Tên người đại diện"}
                                  style={{ fontWeight: 500 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  value={this.state.data.supplier.phone}
                                  placeholder="Nhập số điện thoại"
                                  onChange={(event) => {
                                    this.state.data.supplier.phone =
                                      event.target.value;
                                    this.setState({});
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("address", {
                            rules: [
                              {
                                message: "nhập địa chỉ đại lý",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Điện thoại"}
                                  style={{ fontWeight: 500 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  // style={{ height: 100 }}
                                  value={
                                    // this.state.data.supplier.address +
                                    // ", " +
                                    // this.state.data.supplier.districtsName +
                                    // ", " +
                                    // this.state.data.supplier.cityName
                                    this.state.data.supplier.address
                                  }
                                  placeholder="Nhập địa chỉ đại lý"
                                  onChange={(event) => {
                                    this.state.data.supplier.address =
                                      event.target.value;
                                    this.setState({});
                                  }}
                                  // loai="textarea"
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("phone", {
                            rules: [
                              {
                                message: "nhập số điện thoại",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Tài khoản đăng nhập"}
                                  style={{ fontWeight: 500 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  value={this.state.data.supplier.phone}
                                  placeholder="Nhập số điện thoại"
                                  onChange={(event) => {
                                    this.state.data.supplier.phone =
                                      event.target.value;
                                    this.setState({});
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("address", {
                            rules: [
                              {
                                message: "nhập địa chỉ đại lý",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Mật khẩu"}
                                  style={{ fontWeight: 500 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  // style={{ height: 100 }}
                                  value={
                                    // this.state.data.supplier.address +
                                    // ", " +
                                    // this.state.data.supplier.districtsName +
                                    // ", " +
                                    // this.state.data.supplier.cityName
                                    this.state.data.supplier.address
                                  }
                                  placeholder="Nhập địa chỉ đại lý"
                                  onChange={(event) => {
                                    this.state.data.supplier.address =
                                      event.target.value;
                                    this.setState({});
                                  }}
                                  // loai="textarea"
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row
                      className="p-0  ml-0 mb-0 mr-0"
                      xs="auto"
                      style={{ marginTop: 70 }}
                    >
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("phone", {
                            rules: [
                              {
                                message: "nhập số điện thoại",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={2}
                                  title={"Thông tin vận hành"}
                                  style={{ fontWeight: "bold" }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectAttrubie
                                  isBackground={false}
                                  placeholder="chai/túi/bao 10kg...."
                                  reponsive
                                  type="min"
                                  onChange={(value) => {
                                    this.setState(
                                      {
                                        attributeIdMin: value,
                                        errAttributesMin: false,
                                      },
                                      () =>
                                        this.props.form.validateFields(
                                          ["attributeIdMin"],
                                          { force: true }
                                        )
                                    );
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        {" "}
                      </Col>
                    </Row>
                  </Col>
                  <Col className="m-0 p-0 ml-5 " xs="auto">
                    <div
                      style={{
                        width: 2,
                        height: "100%",
                        background: "#f0f0f0",
                      }}
                    ></div>
                  </Col>
                  <Col
                    style={{
                      paddingBottom: 40,
                    }}
                    xs={4}
                  >
                    <Row
                      style={{ marginTop: 30, marginLeft: 30 }}
                      justify="middle"
                    >
                      <ITitle
                        level={2}
                        title="Hình ảnh"
                        style={{
                          color: colors.black,
                          fontWeight: "bold",
                          // marginTop: 53,
                        }}
                      />

                      <Form.Item>
                        {getFieldDecorator("avatar", {
                          rules: [
                            {
                              // required: this.state.checkNick,

                              message: "chọn hình",
                              valuePropName: "fileList",
                              getValueFromEvent: this.normFile,
                            },
                          ],
                        })(
                          <Col className="p-0 mt-4">
                            <IUpload
                              url={this.state.url}
                              type={ImageType.USER}
                              // fileList={this.state.data.supplier.i}
                              name={this.state.data.supplier.avatar}
                              callback={(file) => {
                                this.state.imageEdit = file;
                                this.setState({});
                              }}
                            />
                          </Col>
                        )}
                      </Form.Item>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row></Row>
          </Form>
        </div>
      </Container>
    );
  }
}

const editBranchPage = Form.create({ name: "EditBranchPage" })(EditBranchPage);

export default editBranchPage;
