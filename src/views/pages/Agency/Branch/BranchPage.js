import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import {
  ITitle,
  ISearch,
  ISvg,
  ICascader,
  ISelectBranch,
  IButton,
  ITable,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import { message, Popconfirm } from "antd";

const widthScreen = window.innerWidth;

function BranchPage(props) {
  let timeSearch = null;
  const [selectedRowKeys, setSelectRowKeys] = useState([]);
  const [arrRemove, setArrRemove] = useState([]);
  const [dataTable, setDataTable] = useState([]);
  const [isLoadingTable, setLoadingTable] = useState(true);
  const [pagination, setPagination] = useState({
    total: 0,
    sizeItem: 0,
  });
  const [isLoadingRemove, setIsLoadingRemove] = useState(false);

  const [filterTable, setFilterTable] = useState({
    status: 0,
    page: 1,
    city_id: 0,
    district_id: 0,
    keySearch: "",
  });

  const _getAPIListBranch = async () => {
    try {
      const data = await APIService._getListBranch(
        filterTable.page,
        filterTable.keySearch
      );
      let agency = data.agency;
      agency.map((item, index) => {
        agency[index].stt = (filterTable.page - 1) * 10 + index + 1;
        agency[index].sttAndId = {
          id: item.id,
          stt: index,
        };
      });

      setDataTable(agency);
      setPagination({
        ...pagination,
        total: data.total,
        sizeItem: data.size,
      });
      setLoadingTable(false);
    } catch (error) {
      message.error(error);
      setLoadingTable(false);
    }
  };

  const onChangePage = (page) => setFilterTable({ ...filterTable, page: page });

  useEffect(() => {
    _getAPIListBranch();
  }, [filterTable]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tên phòng kinh doanh",
      dataIndex: "name",
      key: "name",
      render: (name) => <span>{name}</span>,
    },
    {
      title: "Mã phòng kinh doanh",
      dataIndex: "code",
      key: "code",
      render: (code) => <span>{code}</span>,
    },

    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
      width: 380,
      render: (address) => <span>{address}</span>,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      align: "center",
      render: (phone) => <span>{phone}</span>,
    },
    {
      title: "Địa bàn quản lý",
      dataIndex: "region_name",
      key: "region_name",
      render: (region_name) => <span>{region_name}</span>,
    },

    {
      title: "Số đại lý cấp 1",
      dataIndex: "count_s1",
      key: "count_s1",
      render: (count_s1) => <span>{count_s1}</span>,
    },
    {
      title: "Người đại diện",
      dataIndex: "representative",
      key: "representative",
      render: (representative) => <span>{representative}</span>,
    },

    // {
    //   dataIndex: "id",
    //   key: "id",
    //   align: "center",
    //   width: 100,

    //   render: (id) => (
    //     <Row>
    //       {/* <Col
    //         onClick={() => props.history.push("/agencyS2/edit/" + id)}
    //         className="cursor scale"
    //       >
    //         <ISvg
    //           name={ISvg.NAME.WRITE}
    //           width={20}
    //           height={20}
    //           fill={colors.icon.default}
    //         />
    //       </Col> */}
    //       <Col
    //         onClick={(e) => {
    //           e.stopPropagation();
    //           props.history.push("/detailbranch/" + id);
    //         }}
    //         className="cursor scale"
    //       >
    //         <ISvg
    //           name={ISvg.NAME.CHEVRONRIGHT}
    //           width={11}
    //           height={20}
    //           fill={colors.icon.default}
    //         />
    //       </Col>
    //     </Row>
    //   ),
    // },
  ];

  const triggerDelete = (objRemove) => {
    try {
      APIService._updateStatusSuppliers(objRemove);

      message.success("Xóa thành công");
      setIsLoadingRemove(false);
      setLoadingTable(true);
      setSelectRowKeys([]);

      _getAPIListBranch();
    } catch (error) {
      console.log(error);

      setIsLoadingRemove(false);
    }
  };

  const onSelectChange = (selectedRowKeys) => {
    let arrID = [];
    selectedRowKeys.map((item, index) => {
      const Id = dataTable[item].id;
      arrID.push(Id);
    });

    setSelectRowKeys(selectedRowKeys);
    setArrRemove(arrID);
  };

  const _onFilter = async (value, selectedOptions) => {
    try {
      const city = selectedOptions.length > 0 ? selectedOptions[0].value : 0;
      const district =
        selectedOptions.length > 1 ? selectedOptions[1].value : 0;
      setLoadingTable(true);
      setFilterTable({ ...filterTable, city_id: city, district_id: district });
    } catch (err) {
      console.log(err);
    }
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    hideDefaultSelections: false,
  };
  return (
    <Container fluid>
      <Row className="p-0">
        <ITitle
          level={1}
          title="Phòng kinh doanh"
          style={{
            color: colors.main,
            fontWeight: "bold",
            flex: 1,
            display: "flex",
            alignItems: "flex-end",
          }}
        />
        <ISearch
          onChange={(e) => {
            let search = e.target.value;
            if (timeSearch) {
              clearTimeout(timeSearch);
            }
            timeSearch = setTimeout(() => {
              setLoadingTable(true);
              setFilterTable({ ...filterTable, keySearch: search, page: 1 });
            }, 500);
          }}
          placeholder="Nhập từ khóa"
          onPressEnter={() => _getAPIListBranch()}
          icon={
            <div
              style={{
                display: "flex",
                width: 42,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <img src={images.icSearch} style={{ width: 20, height: 20 }} />
            </div>
          }
        />
      </Row>

      {/* <Row className='center' style={{ marginTop: 36 }}>
        <Col
          style={{
            display: 'flex',
            justifyContent: 'flex-end'
          }}
        >
          <Row>
            <Col xs='auto'>
              <Row>
                <IButton
                  icon={ISvg.NAME.ARROWUP}
                  title="Tạo mới"
                  color={colors.main}
                  style={{ marginRight: 12 }}
                  onClick={() =>
                    this.props.history.push({
                      pathname: "/agencyS1/add/0",
                      state: {
                        type: "create",
                        selectedRowKeys: this.state.selectedRowKeys,
                        data: this.state.data,
                      }, // 1 là thêm đại lý
                    })
                  }
                />
                <IButton
                  icon={ISvg.NAME.DOWLOAND}
                  title='Xuất file'
                  color={colors.main}
                  style={{ marginRight: 12 }}
                  onClick={() => {
                    message.warning('Chức năng đang cập nhật')
                  }}
                />
              </Row>
            </Col>
          </Row>
        </Col>
      </Row> */}

      <Row style={{ marginTop: 45 }}>
        <ITable
          data={dataTable}
          columns={columns}
          style={{ width: "100%" }}
          defaultCurrent={1}
          loading={isLoadingTable}
          maxpage={pagination.total}
          sizeItem={pagination.sizeItem}
          indexPage={filterTable.page}
          onRow={(record, rowIndex) => {
            return {
              onClick: (event) => {
                const indexRow = Number(
                  event.currentTarget.attributes[1].ownerElement.dataset.rowKey
                );
                const id = dataTable[indexRow].id;
                props.history.push("/detailbranch/" + id);
              }, // click row
              onMouseDown: (event) => {
                const indexRow = Number(
                  event.currentTarget.attributes[1].ownerElement.dataset.rowKey
                );
                const id = dataTable[indexRow].id;
                if (event.button == 2 || event == 4) {
                  window.open("/detailbranch/" + id, "_blank");
                }
              },
            };
          }}
          onChangePage={(page) => {
            setLoadingTable(true);
            onChangePage(page);
          }}
        />
      </Row>
    </Container>
  );
}

export default BranchPage;
