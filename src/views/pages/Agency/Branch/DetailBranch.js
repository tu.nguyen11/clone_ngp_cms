import React, { useState, useEffect } from "react";
import { Row, Col, Modal } from "antd";
import { ITitle, IButton, ISvg } from "../../../components";
import { colors } from "../../../../assets";

import ITransfer from "../../../components/styled/modalTransfer/ITransfer";
import { APIService } from "../../../../services";
import { useParams } from "react-router-dom";

function DetailAgencyS2(props) {
  const [isVisbleModal, setVisbleModal] = useState(false);
  const params = useParams();

  let dataBody = [];
  for (let i = 0; i < 4; i++) {
    dataBody.push({
      stt: i + 1,
      code_agency: "ABC 123456",
      name_agency: "Semi Admin",
      address: "153 Hồ Văn Huê, Phường 9, Quận Phú Nhuận, Hồ Chí Minh",
      sdt: "0123456789",
      name_represent: "Semi Admin",
    });
  }
  const handleCloseModal = () => {
    setVisbleModal(false);
  };

  const [dataDetail, setDataDetail] = useState({
    agency_detail: {},
    image_url: "",
    loading: false,
  });
  const [dataListS1, setDataListS1] = useState({
    list_s1: [],
    loading: false,
  });

  useEffect(() => {
    _getAPIDetailAgency(params.id);
    _getAPIListS1(params.id);
  }, []);

  const _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailBranch(idAgency);
      setDataDetail(data);
    } catch (error) {
      console.log(error);
    }
  };
  const _getAPIListS1 = async (idAgency) => {
    try {
      const data = await APIService._getListS1(idAgency);
      let agencyS1 = data.list_s1;
      agencyS1.map((item, index) => {
        agencyS1[index].stt = index + 1;
        agencyS1[index].sttAndId = {
          id: item.id,
          stt: index,
        };
      });
      setDataListS1(agencyS1);
    } catch (error) {
      console.log(error);
    }
  };

  const { agency_detail } = dataDetail;
  const { contract } = agency_detail;
  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 16]}>
        <Col>
          <ITitle
            level={1}
            title="Chi tiết phòng kinh doanh"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        <Col>
          <Row gutter={[12, 20]}>
            <Col>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  icon={ISvg.NAME.CROSS}
                  title="Hủy"
                  style={{ margin: "0px 0px 0px 20px" }}
                  color={colors.oranges}
                  onClick={() => props.history.push("/branchpage")}
                />
              </div>
            </Col>
            <Col>
              <div
                style={{
                  maxWidth: 815,
                  minWidth: 680,
                  height: "100%",
                  // background: colors.white,
                }}
              >
                <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
                  <Col span={24} style={{}}>
                    <div
                      style={{
                        background: colors.white,
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      }}
                    >
                      <Row>
                        <Col span={16}>
                          <div
                            style={{
                              padding: "30px 40px",
                              background: colors.white,
                              borderRight: "1px solid #f0f0f0",
                            }}
                          >
                            <Row gutter={[12, 20]}>
                              <Col style={{ marginTop: 6 }}>
                                <ITitle
                                  level={2}
                                  title="Thông tin phòng kinh doanh"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col>
                                <div>
                                  <p style={{ fontWeight: 600 }}>
                                    Tên phòng kinh doanh
                                  </p>
                                  <span>{agency_detail.name}</span>
                                </div>
                              </Col>
                              <Col>
                                <div>
                                  <p style={{ fontWeight: 600 }}>
                                    Mã phòng kinh doanh
                                  </p>
                                  <span>{agency_detail.code}</span>
                                </div>
                              </Col>
                              <Col>
                                <div>
                                  <p style={{ fontWeight: 600 }}>Địa chỉ</p>
                                  <span>{agency_detail.address}</span>
                                </div>
                              </Col>
                              <Col>
                                <Row gutter={[12, 20]}>
                                  <Col span={12}>
                                    <div>
                                      <p style={{ fontWeight: 600 }}>
                                        Tên người đại diện
                                      </p>
                                      <span>
                                        {agency_detail.representative}
                                      </span>
                                    </div>
                                  </Col>
                                  <Col span={12}>
                                    <div>
                                      <p style={{ fontWeight: 600 }}>
                                        Điện thoại liên hệ
                                      </p>
                                      <span>{agency_detail.phone}</span>
                                    </div>
                                  </Col>
                                </Row>
                              </Col>
                              <Col>
                                <Row gutter={[12, 20]}>
                                  <Col span={12}>
                                    <div>
                                      <p style={{ fontWeight: 600 }}>
                                        Tài khoản đăng nhập
                                      </p>
                                      <span>{agency_detail.username}</span>
                                    </div>
                                  </Col>
                                  <Col span={12}>
                                    <div>
                                      <p style={{ fontWeight: 600 }}>
                                        Mật khẩu
                                      </p>
                                      <span>{agency_detail.password}</span>
                                    </div>
                                  </Col>
                                </Row>
                              </Col>
                              <Col style={{ marginTop: 36 }}>
                                <ITitle
                                  level={2}
                                  title="Thông tin vận hành"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col>
                                <div>
                                  <p style={{ fontWeight: 600 }}>
                                    Địa bàn phụ trách
                                  </p>
                                  <span>{agency_detail.region_name}</span>
                                </div>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                        <Col span={8}>
                          <div
                            style={{
                              padding: "30px 40px",
                              background: colors.white,
                              borderRight: "1px solid #f0f0f0",
                            }}
                          >
                            <Row gutter={[12, 20]}>
                              <Col style={{ marginTop: 6 }}>
                                <ITitle
                                  level={2}
                                  title="Hình ảnh"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>

                              <Col>
                                <img
                                  src={
                                    dataDetail.image_url + agency_detail.image
                                  }
                                  style={{
                                    height: 190,
                                    width: "100%",
                                    padding: 12,
                                    border: "1px solid #D8DBDC",
                                  }}
                                />
                              </Col>
                              <Col>
                                <div>
                                  <p style={{ fontWeight: 600 }}>
                                    Số đại lý cấp 1 trực thuộc
                                  </p>
                                  <span>{agency_detail.count_s1}</span>
                                </div>
                              </Col>
                              <Col>
                                <IButton
                                  title="Xem danh sách đại lý"
                                  color={colors.main}
                                  onClick={() => setVisbleModal(true)}
                                />
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        visible={isVisbleModal}
        width={1200}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 0 }}
      >
        <ITransfer
          callbackClose={() => handleCloseModal()}
          title="Danh sách đại lý cấp 1 phụ thuộc"
          dataList={[
            { title: "ABCD123451", id: 1, active: false },
            { title: "ABCD123452", id: 2, active: true },
            { title: "ABCD123453", id: 3, active: false },
            { title: "ABCD123454", id: 4, active: false },
            { title: "ABCD123455", id: 5, active: true },
            { title: "ABCD123456", id: 6, active: false },
            { title: "ABCD123457", id: 7, active: false },
            { title: "ABCD123458", id: 8, active: true },
            { title: "ABCD123459", id: 9, active: false },
          ]}
          dataHeader={[
            { name: "STT", align: "center", width: "50px" },
            { name: "Mã đại lý", align: "left", width: "80px" },
            { name: "Tên đại lý", align: "left", width: "80px" },
            { name: "Địa chỉ", align: "left", width: "250px" },
            { name: "Số điện thoại", align: "left", width: "120px" },
            { name: "Người đại diện", align: "left", width: "97px" },
            { name: "", align: "center", width: "47px" },
          ]}
          keyId={[
            "stt",
            "dms_code",
            "shop_name",
            "address",
            "phone",
            "bussiness_name",
          ]}
          align={["center", "left", "left", "left", "left", "left", "center"]}
          dataBody={dataListS1}
        />
      </Modal>
    </div>
  );
}

export default DetailAgencyS2;
