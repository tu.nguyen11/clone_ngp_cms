import React, { useState, useEffect } from "react";
import { Row, Col, Modal } from "antd";
import { ITitle, IButton, ISvg } from "../../../components";
import { colors } from "../../../../assets";

import ITransfer from "../../../components/styled/modalTransfer/ITransfer";
import { APIService } from "../../../../services";
import { useParams } from "react-router-dom";

function DetailDistribution(props) {
  const param = useParams();
  const distributionId = param.id;
  console.log(distributionId);

  const [isVisbleModal, setVisbleModal] = useState(false);

  let dataBody = [];
  for (let i = 0; i < 4; i++) {
    dataBody.push({
      stt: i + 1,
      code_agency: "ABC 123456",
      name_agency: "Semi Admin",
      address: "153 Hồ Văn Huê, Phường 9, Quận Phú Nhuận, Hồ Chí Minh",
      sdt: "0123456789",
      name_represent: "Semi Admin",
    });
  }
  const handleCloseModal = () => {
    setVisbleModal(false);
  };

  const [dataDetail, setDataDetail] = useState({
    distribution: {
      address: "",
      agency_count: 0,
      city_id: 0,
      city_name: "",
      code: "",
      id: 0,
      image: "",
      name: "",
      phone: "",
      region: "",
      representative: "",
      status: 0,
      thumbnail: "",
      username: "",
    },
    loading: false,
  });
  const [image_url, setImageUrl] = useState("");
  useEffect(() => {
    _getAPIDetailDistribution(distributionId);
  }, []);

  const _getAPIDetailDistribution = async (id) => {
    try {
      setDataDetail({ ...dataDetail, loading: true });
      const data = await APIService._getDetailDistribution(id);
      if (data.data) {
        setDataDetail({ distribution: data.data, loading: false });
        setImageUrl(data.image_url);
        console.log(data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const { distribution } = dataDetail;
  console.log(distribution);
  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 16]}>
        <Col>
          <ITitle
            level={1}
            title="Chi tiết phòng điều vận"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        <Col>
          <Row gutter={[12, 38]}>
            {/*<Col>*/}
            {/*  <div*/}
            {/*    style={{*/}
            {/*      display: "flex",*/}
            {/*      flexDirection: "row",*/}
            {/*      justifyContent: "flex-end"*/}
            {/*    }}*/}
            {/*  >*/}
            {/*    <IButton*/}
            {/*      icon={ISvg.NAME.WRITE}*/}
            {/*      title="Chỉnh sửa"*/}
            {/*      style={{ margin: "0px 0px 0px 20px" }}*/}
            {/*      color={colors.main}*/}
            {/*      onClick={() => {*/}
            {/*        props.history.push("/editDistributionPage/a");*/}
            {/*      }}*/}
            {/*    />*/}
            {/*  </div>*/}
            {/*</Col>*/}
            <Col>
              <div
                style={{
                  maxWidth: 815,
                  minWidth: 680,
                  height: "100%",
                  // background: colors.white,
                }}
              >
                <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
                  <Col span={24} style={{}}>
                    <div
                      style={{
                        background: colors.white,
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        height: 580,
                      }}
                    >
                      <Row style={{ height: "100%" }}>
                        <Col
                          span={16}
                          style={{
                            borderRight: "1px solid #f0f0f0",
                            height: "100%",
                          }}
                        >
                          <div
                            style={{
                              padding: "30px 40px",
                              // background: colors.white,
                            }}
                          >
                            <Row gutter={[12, 20]}>
                              <Col style={{ marginTop: 6 }}>
                                <ITitle
                                  level={2}
                                  title="Thông tin phòng điều vận"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>
                              <Col>
                                <div>
                                  <p style={{ fontWeight: 500 }}>Tên PĐV</p>
                                  <span>{distribution.name}</span>
                                </div>
                              </Col>
                              <Col>
                                <div>
                                  <p style={{ fontWeight: 500 }}>Mã PĐV</p>
                                  <span>{distribution.code}</span>
                                </div>
                              </Col>

                              <Col>
                                <Row gutter={[12, 20]}>
                                  <Col span={12}>
                                    <div>
                                      <p style={{ fontWeight: 500 }}>
                                        Tên người đại diện
                                      </p>
                                      <span>{distribution.representative}</span>
                                    </div>
                                  </Col>
                                  <Col span={12}>
                                    <div>
                                      <p style={{ fontWeight: 500 }}>
                                        Điện thoại liên hệ
                                      </p>
                                      <span>{distribution.phone}</span>
                                    </div>
                                  </Col>
                                </Row>
                              </Col>

                              <Col>
                                <div>
                                  <p style={{ fontWeight: 500 }}>Địa chỉ</p>
                                  <span>{distribution.address}</span>
                                </div>
                              </Col>

                              <Col>
                                <Row gutter={[12, 20]}>
                                  <Col span={12}>
                                    <div>
                                      <p style={{ fontWeight: 500 }}>
                                        Tài khoản đăng nhập
                                      </p>
                                      <span>{distribution.username}</span>
                                    </div>
                                  </Col>
                                  <Col span={12}>
                                    <div>
                                      <p style={{ fontWeight: 500 }}>
                                        Mật khẩu
                                      </p>
                                      <span style={{ fontWeight: 500 }}>
                                        ****************
                                      </span>
                                    </div>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </div>
                        </Col>
                        <Col span={8}>
                          <div
                            style={{
                              padding: "30px 40px",
                              background: colors.white,
                              borderRight: "1px solid #f0f0f0",
                            }}
                          >
                            <Row gutter={[12, 20]}>
                              <Col style={{ marginTop: 6 }}>
                                <ITitle
                                  level={2}
                                  title="Hình ảnh"
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                />
                              </Col>

                              <Col>
                                <img
                                  src={image_url + distribution.image}
                                  style={{
                                    height: 190,
                                    width: "100%",
                                    padding: 12,
                                    border: "1px solid #D8DBDC",
                                  }}
                                />
                              </Col>
                            </Row>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        visible={isVisbleModal}
        width={1200}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 0 }}
      >
        <ITransfer
          callbackClose={() => handleCloseModal()}
          title="Danh sách đại lý cấp 1 phụ thuộc"
          dataList={[
            { title: "ABCD123451", id: 1, active: false },
            { title: "ABCD123452", id: 2, active: true },
            { title: "ABCD123453", id: 3, active: false },
            { title: "ABCD123454", id: 4, active: false },
            { title: "ABCD123455", id: 5, active: true },
            { title: "ABCD123456", id: 6, active: false },
            { title: "ABCD123457", id: 7, active: false },
            { title: "ABCD123458", id: 8, active: true },
            { title: "ABCD123459", id: 9, active: false },
          ]}
          dataHeader={[
            { name: "STT", align: "center", width: "50px" },
            { name: "Mã đại lý", align: "left", width: "80px" },
            { name: "Tên đại lý", align: "left", width: "80px" },
            { name: "Địa chỉ", align: "left", width: "250px" },
            { name: "Số điện thoại", align: "left", width: "120px" },
            { name: "Người đại diện", align: "left", width: "97px" },
            { name: "", align: "center", width: "47px" },
          ]}
          dataBody={dataBody}
        />
      </Modal>
    </div>
  );
}

export default DetailDistribution;
