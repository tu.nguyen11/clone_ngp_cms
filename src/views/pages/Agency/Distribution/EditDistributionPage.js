import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  message,
  DatePicker,
  Form,
  Input,
} from "antd";
import moment from "moment";
import React, { Component, useState, useEffect } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  ISelectCity,
  ISelectCounty,
  ILoading,
} from "../../../components";

import { colors } from "../../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";
import { ImageType } from "../../../../constant";

const { RangePicker } = DatePicker;

function EditDistributionPage(props) {
  const { getFieldDecorator } = props.form;
  const onSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields({ force: true }, (err, values) => {
      if (err) {
        return;
      }
      const objAdd = {};
    });
  };
  return (
    <Container fluid>
      <div>
        <Row className="mt-3" xs="auto">
          <ITitle
            level={1}
            title="Chỉnh sửa phòng điều vận mới"
            style={{ color: colors.main, fontWeight: "bold" }}
          />
        </Row>

        <Form onSubmit={onSubmit}>
          <Row
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingTop: 12,
              paddingBottom: 32,
            }}
            xs="auto"
            sm={12}
            md={12}
          >
            <Form.Item>
              <IButton
                icon={ISvg.NAME.SAVE}
                title="Lưu"
                color={colors.main}
                htmlType="submit"
                style={{ marginRight: 20 }}
                // onClick={() => this.handleSubmit}
              />
            </Form.Item>
            <Form.Item>
              <IButton
                icon={ISvg.NAME.CROSS}
                title="Hủy"
                color={colors.oranges}
                style={{}}
                onClick={() => {
                  props.history.goBack();
                }}
              />
            </Form.Item>
          </Row>

          <Row
            style={{ maxWidth: 815, minWidth: 615, flex: 1 }}
            className="p-0 shadow"
          >
            <Col style={{ background: colors.white }} className="pl-5 ml-0">
              <Row className="p-0 m-0" style={{}}>
                <Col
                  className="pl-0 pr-0 pt-0 m-0"
                  style={{ paddingBottom: 40 }}
                >
                  <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                    <ITitle
                      level={2}
                      title="Thông tin phòng điều vận"
                      style={{ color: colors.black, fontWeight: "bold" }}
                    />
                  </Row>

                  <Form.Item>
                    {getFieldDecorator("supplierName", {
                      rules: [
                        {
                          message: "nhập tên cửa hàng đại lý",
                        },
                      ],
                    })(
                      <Row className="p-0 m-0" xs="auto">
                        <Col className="p-0 m-0">
                          <Row className="p-0 m-0 mb-2">
                            <ITitle
                              level={4}
                              title={"Tên PĐV"}
                              style={{ fontWeight: 500 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <IInput placeholder="Nhập tên cửa hàng đại lý" />
                          </Row>
                        </Col>
                      </Row>
                    )}
                  </Form.Item>

                  <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("supplierCode", {
                          rules: [
                            {
                              message: "nhập mã đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Mã PĐV"}
                                style={{ fontWeight: 500 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput placeholder="Nhập mã đại lý" />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("phone", {
                          rules: [
                            {
                              message: "nhập số điện thoại",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Tài khoản đăng nhập"}
                                style={{ fontWeight: 500 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput placeholder="Nhập số điện thoại" />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                    <Col
                      className="p-0"
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator("address", {
                          rules: [
                            {
                              message: "nhập địa chỉ đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Mật khẩu"}
                                style={{ fontWeight: 500 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                // style={{ height: 100 }}

                                placeholder="Nhập địa chỉ đại lý"
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("phone", {
                          rules: [
                            {
                              message: "nhập số điện thoại",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Tên người đại diện"}
                                style={{ fontWeight: 500 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput placeholder="Nhập số điện thoại" />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                    <Col
                      className="p-0"
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Form.Item>
                        {getFieldDecorator("address", {
                          rules: [
                            {
                              message: "nhập địa chỉ đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Điện thoại"}
                                style={{ fontWeight: 500 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                // style={{ height: 100 }}

                                placeholder="Nhập địa chỉ đại lý"

                                // loai="textarea"
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="p-0 mt-0 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("supplierCode", {
                          rules: [
                            {
                              message: "nhập mã đại lý",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Địa chỉ"}
                                style={{ fontWeight: 500 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput placeholder="Nhập mã đại lý" />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
                <Col className="m-0 p-0 ml-5 " xs="auto">
                  <div
                    style={{
                      width: 2,
                      height: "100%",
                      background: "#f0f0f0",
                    }}
                  ></div>
                </Col>
                <Col
                  style={{
                    paddingBottom: 40,
                  }}
                  xs={4}
                >
                  <Row
                    style={{ marginTop: 30, marginLeft: 30 }}
                    justify="middle"
                  >
                    <ITitle
                      level={2}
                      title="Hình ảnh"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        // marginTop: 53,
                      }}
                    />

                    <Form.Item>
                      {getFieldDecorator("avatar", {
                        rules: [
                          {
                            // required: this.state.checkNick,

                            message: "chọn hình",
                            valuePropName: "fileList",
                          },
                        ],
                      })(
                        <Col className="p-0 mt-4">
                          <IUpload
                            // url={this.state.url}
                            type={ImageType.USER}
                            // fileList={this.state.data.supplier.i}
                            callback={(file) => {
                              // this.state.imageEdit = file;
                              // this.setState({});
                            }}
                          />
                        </Col>
                      )}
                    </Form.Item>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row></Row>
        </Form>
      </div>
    </Container>
  );
}
const editDistributionPage = Form.create({ name: "EditDistributionPage" })(
  EditDistributionPage
);
export default editDistributionPage;
