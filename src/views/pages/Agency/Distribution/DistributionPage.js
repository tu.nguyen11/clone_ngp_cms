import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import {
  ITitle,
  ISearch,
  ISvg,
  ICascader,
  ISelectBranch,
  IButton,
  ITable,
  ISelectCity,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { APIService } from "../../../../services";
import { message, Popconfirm } from "antd";

const widthScreen = window.innerWidth;

function DistributionPage(props) {
  let timeSearch = null;
  const [selectedRowKeys, setSelectRowKeys] = useState([]);
  const [arrRemove, setArrRemove] = useState([]);
  const [dataTable, setDataTable] = useState([]);
  const [isLoadingTable, setLoadingTable] = useState(false);
  const [pagination, setPagination] = useState({
    total: 0,
    sizeItem: 0,
  });
  const [isLoadingRemove, setIsLoadingRemove] = useState(false);

  const [filterTable, setFilterTable] = useState({
    status: 0,
    page: 1,
    city_id: 0,
    district_id: 0,
    keySearch: "",
  });

  const _getListCentralTower = async () => {
    try {
      const data = await APIService._getListCentralTower(
        filterTable.page,
        filterTable.city_id,
        filterTable.district_id,
        filterTable.keySearch
      );
      const centertowers = data.centertowers.map((centralTower, index) => {
        centralTower.stt = (filterTable.page - 1) * 10 + index + 1;
        centralTower.warehouse_count = 0;
        return centralTower;
      });
      setDataTable(centertowers || []);
      setPagination({
        ...pagination,
        total: data.total || 0,
        sizeItem: data.size || 0,
      });
      setLoadingTable(false);
    } catch (error) {
      message.error(error);
      setLoadingTable(false);
    }
  };

  const onChangePage = (page) => setFilterTable({ ...filterTable, page: page });

  useEffect(() => {
    _getListCentralTower();
  }, [filterTable]);

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tên PĐV",
      dataIndex: "name",
      key: "name",
      render: (name) => <span>{name}</span>,
    },
    {
      title: "Mã PĐV",
      dataIndex: "code",
      key: "code",
      render: (code) => <span>{code}</span>,
    },
    {
      title: "Đại diện",
      dataIndex: "representative",
      key: "representative",
      render: (representative) => <span>{representative}</span>,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      align: "center",
      render: (phone) => <span>{phone}</span>,
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",

      render: (address) => <span>{address}</span>,
    },

    {
      title: "Phòng kinh doanh",
      dataIndex: "agency_count",
      key: "agency_count",
      render: (agency_count) => <span>{agency_count}</span>,
    },
    {
      title: "Kho trực thuộc",
      dataIndex: "warehouse_count",
      key: "warehouse_count",
      render: (warehouse_count) => <span>{warehouse_count}</span>,
    },
    // {
    //   dataIndex: "id",
    //   key: "id",
    //   align: "center",
    //   width: 100,

    //   render: (id) => (
    //     <Row>
    //       <Col
    //         onClick={(e) => {
    //           e.stopPropagation();
    //           props.history.push("/detailDistribution/" + id);
    //         }}
    //         className="cursor scale"
    //       >
    //         <ISvg
    //           name={ISvg.NAME.CHEVRONRIGHT}
    //           width={11}
    //           height={20}
    //           fill={colors.icon.default}
    //         />
    //       </Col>
    //     </Row>
    //   ),
    // },
  ];

  const onSelectChange = (selectedRowKeys) => {
    let arrID = [];
    selectedRowKeys.map((item, index) => {
      const Id = dataTable[item].id;
      arrID.push(Id);
    });

    setSelectRowKeys(selectedRowKeys);
    setArrRemove(arrID);
  };

  const _onFilter = async (value, selectedOptions) => {
    try {
      const city = value;
      setLoadingTable(true);
      setFilterTable({ ...filterTable, city_id: city });
    } catch (err) {
      console.log(err);
    }
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    hideDefaultSelections: false,
  };
  return (
    <Container fluid>
      <Row className="p-0">
        <ITitle
          level={1}
          title="Phòng điều vận"
          style={{
            color: colors.main,
            fontWeight: "bold",
            flex: 1,
            display: "flex",
            alignItems: "flex-end",
          }}
        />
        <ISearch
          onChange={(e) => {
            let search = e.target.value;
            if (timeSearch) {
              clearTimeout(timeSearch);
            }
            timeSearch = setTimeout(() => {
              setLoadingTable(true);
              setFilterTable({ ...filterTable, keySearch: search, page: 1 });
            }, 500);
          }}
          placeholder="Nhập từ khóa"
          onPressEnter={() => _getListCentralTower()}
          icon={
            <div
              style={{
                display: "flex",
                width: 42,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <img src={images.icSearch} style={{ width: 20, height: 20 }} />
            </div>
          }
        />
      </Row>

      {/* <Row className='center' style={{ marginTop: 36 }}>
        <Col
          style={{
            display: 'flex',
            justifyContent: 'flex-end'
          }}
        >
          <Row>
            <Col />
            <Col>
             <ISelectBranch
               select={true}
               defaultValue="Phòng kinh doanh"
               style={{
                 background: colors.white,
                 borderWidth: 1,
                 borderColor: colors.line,
                 borderStyle: "solid",
               }}
               onChange={(key) => {
                 
               }}
             />
            </Col>

            <Col xs='auto'>
              <Row>
                <IButton
                  icon={ISvg.NAME.DOWLOAND}
                  title='Xuất file'
                  color={colors.main}
                  style={{ marginRight: 12 }}
                  onClick={() => {
                    message.warning('Chức năng đang cập nhật')
                  }}
                />
                <Popconfirm
                  placement="bottom"
                  title={"Nhấn đồng ý để xóa ."}
                  onConfirm={() => {
                    if (selectedRowKeys.length === 0) {
                      message.warning("Bạn chưa chọn đại lý để xóa.");
                      return;
                    }
                    const objRemove = {
                      id: arrRemove,
                      status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                    };
                    setIsLoadingRemove(true);
                    triggerDelete(objRemove);
                  }}
                  okText="Đồng ý"
                  cancelText="Hủy"
                >
                  <IButton
                    icon={ISvg.NAME.DELETE}
                    title="Xóa"
                    color={colors.oranges}
                    style={{}}
                    loading={isLoadingRemove}
                  />
                </Popconfirm>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row> */}

      <Row style={{ marginTop: 45 }}>
        <ITable
          data={dataTable}
          columns={columns}
          style={{ width: "100%" }}
          defaultCurrent={1}
          loading={isLoadingTable}
          maxpage={pagination.total}
          sizeItem={pagination.sizeItem}
          indexPage={filterTable.page}
          onRow={(record, rowIndex) => {
            return {
              onClick: (event) => {
                const indexRow = Number(
                  event.currentTarget.attributes[1].ownerElement.dataset.rowKey
                );
                const id = dataTable[indexRow].id;
                props.history.push("/detailDistribution/" + id);
              }, // click row
              onMouseDown: (event) => {
                const indexRow = Number(
                  event.currentTarget.attributes[1].ownerElement.dataset.rowKey
                );
                const id = dataTable[indexRow].id;
                if (event.button == 2 || event == 4) {
                  window.open("/detailDistribution/" + id, "_blank");
                }
              },
            };
          }}
          onChangePage={(page) => {
            setLoadingTable(true);
            onChangePage(page);
          }}
        />
      </Row>
    </Container>
  );
}

export default DistributionPage;
