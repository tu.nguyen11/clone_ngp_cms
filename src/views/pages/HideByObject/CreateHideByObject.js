import { Col, Row, Modal, Empty, message, Popconfirm, Tag } from "antd";
import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { colors } from "../../../assets";
import { IButton, ISvg, ITableHtml, ISelect } from "../../components";
import {
  IError,
  useModalAgencyS1FilterState,
  StyledISelect,
} from "../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../components/common/Font/font";
import {
  CLEAR_REDUX_PRODUCTS_BY_OBJECT,
  REMOVE_PRODUCT_BY_OBJECT_CONFIRM,
} from "../../store/reducers";
import { useDispatch, useSelector } from "react-redux";
import { APIService } from "../../../services";
import useModalTreeProductByObject from "../../components/common/ModalAgency/useModalTreeProductByObject";
import useModal from "./useModal";

export default function CreateHideByObject(props) {
  const params = useParams();
  const { type } = params;
  const history = useHistory();
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { productsByObject, treeProduct } = dataRoot;

  const [loadingButton, setLoadingButton] = useState(false);

  const [visiblePopup, setVisiblePopup] = useState(false);
  const [visibleTreeProduct, setVisibleTreeProduct] = useState(false);
  const [isVisibleAgency1, setVisibleAgency1] = useState(false);
  const [clearS1, setClearS1] = useState(false);
  const [isRemove, setIsRemove] = useState(false);

  const [listS1Confirm, setListS1Confirm] = useState([]);
  const [listS1ConfirmID, setListS1ConfirmID] = useState([]);
  const [listConfirm, setListConfirm] = useState([]);

  const [typeObject, setTypeObject] = useState(0);

  const [dropdownRegion, setDropdownRegion] = useState([]);
  const [dropdownCity, setDropdownCity] = useState([]);
  const [loadingCity, setLoadingCity] = useState(false);

  const [arrayCity, setArrayCity] = useState([]);
  const [arrayCityClone, setArrayCityClone] = useState([]);
  const [arrayRegion, setArrayRegion] = useState([]);
  const [checkConditionRegion, setCheckConditionRegion] = useState(false);
  const [checkConditionCity, setCheckConditionCity] = useState(false);

  const [dropdownMembership, setDropdownMembership] = useState([]);
  const [arrayMembership, setArrayMembership] = useState([]);
  const [checkConditionMembership, setCheckConditionMembership] =
    useState(false);

  const renderTableAgencyC1 = () => {
    return (
      <Col span={24}>
        <Row>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Col span={24}>
              <ITableHtml
                childrenBody={bodyTable(listS1Confirm)}
                childrenHeader={headerTable(dataHeader)}
                style={{
                  border: "1px solid rgba(122, 123, 123, 0.5)",
                  borderTop: "none",
                  maxHeight: 510,
                }}
                isBorder={false}
              />
            </Col>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                    marginTop: 15,
                  }}
                  onClick={() => {
                    if (type === "event") {
                      setListConfirm([]);
                    }
                    setListS1Confirm([]);
                    setClearS1(true);
                    setTypeObject(0);
                  }}
                />
              </div>
            </Col>
          </div>
        </Row>
      </Col>
    );
  };

  const renderArea = () => {
    return (
      <>
        <Col span={24}>
          <div
            style={{
              padding: "10px 12px 12px 12px",
              border: "1px solid #d9d9d9",
            }}
          >
            <div
              style={{
                borderBottom: "1px solid #d9d9d9",
                paddingBottom: 10,
              }}
            >
              <p style={{ fontWeight: 500 }}>Cấp người dùng</p>
            </div>
            <ISelect
              isBackground={false}
              placeholder="chọn cấp người dùng"
              select={false}
              value={1}
              // data={
              //   listSelectNotificationGroupUser
              // }
              data={[
                {
                  key: 1,
                  value: "Đại lý Cấp 1",
                },
                {
                  key: 2,
                  value: "Đại lý Cấp 2",
                },
              ]}
            />
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              padding: "10px 12px 12px 12px",
              border:
                checkConditionRegion === true
                  ? "1px solid red"
                  : "1px solid #d9d9d9",
            }}
          >
            <Row>
              <Col span={24}>
                <div
                  style={{
                    borderBottom: "1px solid #d9d9d9",
                    paddingBottom: 10,
                  }}
                >
                  <Row>
                    <Col span={12}>
                      <p style={{ fontWeight: 500 }}>Vùng địa lý</p>
                    </Col>
                    <Col span={12}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <Popconfirm
                          placement="bottom"
                          title={
                            <span>
                              Xóa tất cả dữ liệu vùng địa lý đang chọn,
                            </span>
                          }
                          onConfirm={() => {
                            if (type === "event") {
                              setListConfirm([]);
                            }
                            setArrayRegion([]);
                          }}
                          okText="Đồng ý"
                          cancelText="Hủy"
                        >
                          <Tag color={colors.main}>Xóa</Tag>
                        </Popconfirm>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={24}>
                <div>
                  <StyledISelect
                    mode="multiple"
                    isBorderBottom="1px solid #d9d9d9"
                    placeholder="chọn khu vực"
                    data={dropdownRegion}
                    value={arrayRegion}
                    style={{
                      maxHeight: 200,
                      overflow: "auto",
                      paddingTop: 10,
                    }}
                    onChange={(arrKey) => {
                      if (type === "event") {
                        setListConfirm([]);
                      }
                      if (arrKey.length === 0) {
                        setArrayCity([]);
                        setArrayCityClone([]);
                      }
                      const isAll = arrKey.indexOf(0);
                      if (isAll >= 0) {
                        let arrayAddress = [...dropdownRegion];
                        arrayAddress.splice(0, 1);
                        let idArray = arrayAddress.map((item) => item.key);

                        setArrayRegion([...idArray]);
                        setCheckConditionRegion(false);
                      } else {
                        setArrayRegion([...arrKey]);
                        setCheckConditionRegion(false);
                      }
                    }}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              padding: "10px 12px 12px 12px",
              border:
                checkConditionCity === true
                  ? "1px solid red"
                  : "1px solid #d9d9d9",
            }}
          >
            <Row>
              <Col span={24}>
                <div
                  style={{
                    borderBottom: "1px solid #d9d9d9",
                    paddingBottom: 10,
                  }}
                >
                  <Row>
                    <Col span={12}>
                      <p style={{ fontWeight: 500 }}>Tỉnh/Thành</p>
                    </Col>
                    <Col span={12}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <Popconfirm
                          placement="bottom"
                          title={
                            <span>
                              Xóa tất cả dữ liệu tỉnh/thành đang chọn.
                            </span>
                          }
                          onConfirm={() => {
                            if (type === "event") {
                              setListConfirm([]);
                            }
                            setArrayCity([]);
                            setArrayCityClone([]);
                          }}
                          okText="Đồng ý"
                          cancelText="Hủy"
                        >
                          <Tag color={colors.main}>Xóa</Tag>
                        </Popconfirm>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={24}>
                <div>
                  <StyledISelect
                    mode="multiple"
                    isBorderBottom="1px solid #d9d9d9"
                    placeholder="chọn tỉnh/thành phố"
                    style={{
                      maxHeight: 200,
                      overflow: "auto",
                      paddingTop: 10,
                    }}
                    loading={loadingCity}
                    onChange={(arrKey) => {
                      if (type === "event") {
                        setListConfirm([]);
                      }
                      const isAll = arrKey.indexOf(0);
                      if (isAll !== -1) {
                        let arrayCity = [...dropdownCity];
                        arrayCity.splice(0, 1);
                        let arrKeyID = [...arrayCity];
                        let arrID = arrKeyID.map((item) => item.key);

                        setArrayCity([...arrID]);
                        setArrayCityClone([...arrKeyID]);
                        setCheckConditionCity(false);
                      } else {
                        let arrKeyID = arrKey.map((item) => {
                          return dropdownCity.find((item1) => {
                            return item1.key === item;
                          });
                        });

                        setArrayCity([...arrKey]);
                        setArrayCityClone([...arrKeyID]);

                        setCheckConditionCity(false);
                      }
                    }}
                    value={arrayCity}
                    data={dropdownCity}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div className="flex justify-end">
            <IButton
              title="Hủy"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              loading={loadingButton}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                if (type === "event") {
                  setListConfirm([]);
                }
                setArrayCity([]);
                setArrayCityClone([]);
                setArrayRegion([]);
                setCheckConditionCity(false);
                setCheckConditionRegion(false);
                setTypeObject(0);
              }}
            />
          </div>
        </Col>
      </>
    );
  };

  const renderMembership = () => {
    return (
      <>
        <Col span={24}>
          <div
            style={{
              padding: "10px 12px 12px 12px",
              border:
                checkConditionMembership === true
                  ? "1px solid red"
                  : "1px solid #d9d9d9",
            }}
          >
            <Row>
              <Col span={24}>
                <div
                  style={{
                    borderBottom: "1px solid #d9d9d9",
                    paddingBottom: 10,
                  }}
                >
                  <Row>
                    <Col span={12}>
                      <p style={{ fontWeight: 500 }}>Cấp bậc</p>
                    </Col>
                    <Col span={12}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <Popconfirm
                          placement="bottom"
                          title={
                            <span>Xóa tất cả dữ liệu cấp bậc đang chọn.</span>
                          }
                          onConfirm={() => {
                            if (type === "event") {
                              setListConfirm([]);
                            }
                            setArrayMembership([]);
                          }}
                          okText="Đồng ý"
                          cancelText="Hủy"
                        >
                          <Tag color={colors.main}>Xóa</Tag>
                        </Popconfirm>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={24}>
                <div>
                  <StyledISelect
                    mode="multiple"
                    isBorderBottom="1px solid #d9d9d9"
                    placeholder="chọn cấp bậc"
                    style={{
                      maxHeight: 200,
                      overflow: "auto",
                      paddingTop: 10,
                    }}
                    loading={loadingCity}
                    onChange={(arrKey) => {
                      if (type === "event") {
                        setListConfirm([]);
                      }
                      const isAll = arrKey.indexOf(0);
                      if (isAll >= 0) {
                        let arrayMembership = [...dropdownMembership];
                        arrayMembership.splice(0, 1);
                        let idArray = arrayMembership.map((item) => item.key);

                        setArrayMembership([...idArray]);
                      } else {
                        setArrayMembership([...arrKey]);
                      }

                      setCheckConditionMembership(false);
                    }}
                    value={arrayMembership}
                    data={dropdownMembership}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div className="flex justify-end">
            <IButton
              title="Hủy"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              loading={loadingButton}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                if (type === "event") {
                  setListConfirm([]);
                }
                setArrayMembership([]);
                setCheckConditionMembership(false);
                setTypeObject(0);
              }}
            />
          </div>
        </Col>
      </>
    );
  };

  const _fetchGetListRegion = async () => {
    try {
      const data = await APIService._getAllAgencyGetRegion();
      let dataNew = data.agency.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      dataNew.unshift({
        key: 0,
        value: "Toàn quốc",
      });
      setDropdownRegion(() => dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchGetListCity = async () => {
    const data = await APIService._getListCities("");
    const dataNew = data.cities.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });

    dataNew.unshift({
      value: "Tất cả",
      key: 0,
    });

    setDropdownCity([...dataNew]);
  };

  const _getAPIListMembership = async () => {
    try {
      const data = await APIService._getListMemberships();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDropdownMembership([...dataNew]);
    } catch (error) {
      console.log(error);
    }
  };

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp bậc",
      align: "center",
    },
  ];

  const headerTable = (dataHeader) => {
    return (
      <tr>
        {dataHeader.map((item, index) => (
          <th
            className="th-table"
            style={{
              textAlign: item.align,
              paddingLeft: index === 0 ? 20 : 6,
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (data) => {
    return data.length === 0 ? (
      <tr height="100px">
        <td colspan="3" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      data.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.shop_name ? "-" : item.shop_name}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.dms_code ? "-" : item.dms_code}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "center", fontWeight: "normal" }}
          >
            {!item.membership ? "-" : item.membership}
          </td>
        </tr>
      ))
    );
  };

  let dataHeaderProduct = [
    {
      name: "Tên phiên bản",
      align: "left",
    },
    {
      name: "Mã phiên bản",
      align: "left",
    },
    {
      name: "ĐVT",
      align: "center",
    },
    {
      name: "",
      align: "center",
    },
  ];

  let dataHeaderList = [
    {
      name: `Tên ${
        type === "cate"
          ? "ngành hàng"
          : type === "subcate"
          ? "ngành hàng con"
          : type === "brand"
          ? "thương hiệu"
          : "CSBH tặng hàng"
      }`,
      align: "left",
    },
    {
      name: "",
      align: "center",
    },
  ];

  const bodyTableProduct = (data) => {
    return data.length === 0 ? (
      <tr height="100px">
        <td colspan="4" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      data.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{
              textAlign: "left",
              fontWeight: "normal",
              maxWidth: 300,
              paddingLeft: 20,
            }}
          >
            {!item.name ? "-" : item.name}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.code ? "-" : item.code}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "center", fontWeight: "normal" }}
          >
            {!item.list_attribute ? "-" : item.list_attribute[0].name}
          </td>
          <td
            className="td-table"
            style={{
              textAlign: "center",
              fontWeight: "normal",
              //   paddingRight: 20,
            }}
          >
            <div
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  background: "rgba(227, 95, 75, 0.1)",
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  cursor: "pointer",
                }}
                onClick={() => {
                  const dataAction = {
                    keyRoot: "treeProduct",
                    key_listTreeClone: "listTreeClone",
                    key_listTreeShow: "listTreeShow",
                    key_arrConfirm: "arrProduct",
                    key_listConfirm: "listConfirm",
                    key_listKeyClone: "listKeyClone",
                    key_listKeyConfirm: "listKeyConfirm",
                    indexProduct: index,
                    product: item,
                    listTreeClone: treeProduct.listTreeClone,
                    listConfirm: treeProduct.listConfirm,
                    listTreeShow: treeProduct.listTreeShow,
                    arrConfirm: productsByObject.arrProduct,
                    listKeyConfirm: treeProduct.listKeyConfirm,
                    listKeyClone: treeProduct.listKeyClone,
                  };

                  dispatch(REMOVE_PRODUCT_BY_OBJECT_CONFIRM(dataAction));
                }}
              >
                <ISvg
                  name={ISvg.NAME.CROSS}
                  width={8}
                  height={8}
                  fill="#E35F4B"
                />
              </div>
            </div>
          </td>
        </tr>
      ))
    );
  };

  const bodyTableList = (data) => {
    return data.length === 0 ? (
      <tr height="100px">
        <td colspan="2" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      data.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{
              textAlign: "left",
              fontWeight: "normal",
              maxWidth: 300,
              paddingLeft: 20,
            }}
          >
            {!item.name ? "-" : item.name}
          </td>
          <td
            className="td-table"
            style={{
              textAlign: "center",
              fontWeight: "normal",
              paddingRight: 20,
            }}
          >
            <div
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <div
                style={{
                  background: "rgba(227, 95, 75, 0.1)",
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  cursor: "pointer",
                }}
                onClick={() => {
                  let listConfirmTmp = [...listConfirm];
                  listConfirmTmp.splice(index, 1);
                  setListConfirm([...listConfirmTmp]);
                }}
              >
                <ISvg
                  name={ISvg.NAME.CROSS}
                  width={8}
                  height={8}
                  fill="#E35F4B"
                />
              </div>
            </div>
          </td>
        </tr>
      ))
    );
  };

  const renderTitle = (type) => {
    switch (type) {
      case "cate":
        return "ngành hàng";
      case "subcate":
        return "ngành hàng con";
      case "product":
        return "phiên bản";
      case "brand":
        return "thương hiệu";
      case "event":
        return "CSBH";
      default:
        return "";
    }
  };

  const renderPopup = (type) => {
    switch (type) {
      case "cate":
        setIsRemove(true);
        setVisiblePopup(true);
        break;
      case "subcate":
        setIsRemove(true);
        setVisiblePopup(true);
        break;
      case "product":
        setVisibleTreeProduct(true);
        break;
      case "brand":
        setIsRemove(true);
        setVisiblePopup(true);
        break;
      case "event":
        setIsRemove(true);
        if (
          listS1Confirm.length === 0 &&
          arrayRegion.length === 0 &&
          arrayMembership.length === 0 &&
          arrayCity.length === 0
        ) {
          message.warning("Vui lòng chọn đối tượng hiển thị trước!");
          return;
        }
        setVisiblePopup(true);
        break;
      default:
        return;
    }
  };

  const renderBodyTable = (type) => {
    switch (type) {
      case "cate":
        return bodyTableList(listConfirm);
      case "subcate":
        return bodyTableList(listConfirm);
      case "product":
        return bodyTableProduct(productsByObject.arrProduct);
      case "brand":
        return bodyTableList(listConfirm);
      case "event":
        return bodyTableList(listConfirm);
      default:
        return;
    }
  };

  const renderHeaderTable = (type) => {
    switch (type) {
      case "cate":
        return headerTable(dataHeaderList);
      case "subcate":
        return headerTable(dataHeaderList);
      case "product":
        return headerTable(dataHeaderProduct);
      case "brand":
        return headerTable(dataHeaderList);
      case "event":
        return headerTable(dataHeaderList);
      default:
        return;
    }
  };

  const postCreateBlock = async (obj) => {
    try {
      setLoadingButton(true);
      let data;
      switch (type) {
        case "cate": {
          data = await APIService._postCreateCateBlock(obj);
          break;
        }
        case "subcate": {
          data = await APIService._postCreateSubCateBlock(obj);
          break;
        }
        case "brand": {
          data = await APIService._postCreateBrandBlock(obj);
          break;
        }
        case "product": {
          data = await APIService._postCreateProductBlock(obj);
          break;
        }
        case "event": {
          data = await APIService._postCreateEventBlock(obj);
          break;
        }
      }

      setLoadingButton(false);
      message.success(
        `Thiết lập ẩn ${renderTitle(type)} theo đối tượng thành công!`
      );
      history.push(`/hide/${type === "subcate" ? "sub/cate" : type}/list`);
    } catch (err) {
      console.log(err);
      setLoadingButton(false);
    }
  };

  const handleSubmit = () => {
    if (typeObject === 0) {
      const content = (
        <span>
          Vui lòng chọn{" "}
          <span style={{ fontWeight: 600 }}>Đối tượng hiển thị</span>!
        </span>
      );
      return IError(content);
    }

    if (
      listS1Confirm.length === 0 &&
      arrayMembership.length === 0 &&
      arrayCity.length === 0 &&
      arrayRegion.length === 0
    ) {
      const content = (
        <span>
          Vui lòng chọn{" "}
          <span style={{ fontWeight: 600 }}>Đối tượng hiển thị</span>!
        </span>
      );
      return IError(content);
    }

    if (type === "product") {
      if (productsByObject.arrProduct.length === 0) {
        const content = (
          <span>
            Vui lòng chọn{" "}
            <span style={{ fontWeight: 600, textTransform: "capitalize" }}>
              phiên bản
            </span>
            !
          </span>
        );
        return IError(content);
      }

      const listUserId = listS1Confirm.map((item) => item.id);
      const listId = productsByObject.arrProduct.map((item) => item.id);
      let obj = {
        list_user_id: listUserId,
        list_product_type_id: listId,
        list_member_ship: typeObject === 2 ? arrayMembership : [],
        list_agency_id: [],
        list_region_id: typeObject === 3 ? arrayRegion : [],
        list_city_id: typeObject === 3 ? arrayCity : [],
      };

      postCreateBlock(obj);
    } else {
      if (listConfirm.length === 0) {
        const content = (
          <span>
            Vui lòng chọn{" "}
            <span style={{ fontWeight: 600, textTransform: "capitalize" }}>
              {renderTitle(type)}
            </span>
            !
          </span>
        );
        return IError(content);
      }

      const listUserId = listS1Confirm.map((item) => item.id);

      let obj;
      if (type === "event") {
        const listId = listConfirm.map((item) => item.event_child_id);
        obj = {
          user_id: listUserId,
          event_id: listId,
          status: 0,
          list_member_ship: typeObject === 2 ? arrayMembership : [],
          list_agency_id: [],
          list_region_id: typeObject === 3 ? arrayRegion : [],
          list_city_id: typeObject === 3 ? arrayCity : [],
        };
      } else {
        const listId = listConfirm.map((item) => item.id);
        obj = {
          list_user_id: listUserId,
          list_product_type_id: listId,
          list_member_ship: typeObject === 2 ? arrayMembership : [],
          list_agency_id: [],
          list_region_id: typeObject === 3 ? arrayRegion : [],
          list_city_id: typeObject === 3 ? arrayCity : [],
        };
      }

      postCreateBlock(obj);
    }
  };

  useEffect(() => {
    dispatch(CLEAR_REDUX_PRODUCTS_BY_OBJECT());
    _fetchGetListRegion();
    _fetchGetListCity();
    _getAPIListMembership();
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Thiết lập ẩn {renderTitle(type)} theo đối tượng
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Lưu"
                  color={colors.main}
                  icon={ISvg.NAME.SAVE}
                  loading={loadingButton}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={handleSubmit}
                />
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                  }}
                  onClick={() => {
                    switch (type) {
                      case "cate":
                        history.push("/hide/cate/list");
                        break;
                      case "subcate":
                        history.push("/hide/sub/cate/list");
                        break;
                      case "brand":
                        history.push("/hide/brand/list");
                        break;
                      case "product":
                        dispatch(CLEAR_REDUX_PRODUCTS_BY_OBJECT());
                        history.push("/hide/product/list");
                        break;
                      case "event":
                        history.push("/hide/event/list");
                        break;
                    }
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1300 }}>
            <Row gutter={[24, 24]} type="flex">
              <Col span={10}>
                <div
                  style={{
                    padding: 24,
                    height: 700,
                  }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 30]}>
                    <Col span={24}>
                      <Row gutter={[0, 20]}>
                        {listS1Confirm.length === 0 ? (
                          <Col span={24}>
                            <StyledITileHeading minFont="10px" maxFont="16px">
                              Đối tượng hiển thị
                            </StyledITileHeading>
                          </Col>
                        ) : (
                          <Col span={24}>
                            <Row>
                              <Col span={12}>
                                <div
                                  style={{
                                    padding: "11px 0px",
                                  }}
                                >
                                  <StyledITileHeading
                                    minFont="10px"
                                    maxFont="16px"
                                  >
                                    Đối tượng hiển thị
                                  </StyledITileHeading>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div className="flex justify-end">
                                  <IButton
                                    title="Chỉnh sửa ĐLC1"
                                    color={colors.main}
                                    styleHeight={{
                                      borderRadius: 18,
                                      fontWeight: "bold",
                                      width: 150,
                                      background: "white",
                                    }}
                                    onClick={() => {
                                      setVisibleAgency1(true);
                                    }}
                                  />
                                </div>
                              </Col>
                            </Row>
                          </Col>
                        )}

                        {typeObject === 0 ? (
                          <Col span={24}>
                            <Row gutter={[0, 15]}>
                              <Col span={24}>
                                <IButton
                                  title="THEO ĐẠI LÝ"
                                  color={colors.main}
                                  styleHeight={{
                                    borderRadius: 18,
                                    fontWeight: "bold",
                                    background: "white",
                                  }}
                                  onClick={() => {
                                    setListS1Confirm([]);
                                    setListS1ConfirmID([]);
                                    setArrayRegion([]);
                                    setArrayCity([]);
                                    setArrayMembership([]);
                                    setVisibleAgency1(true);
                                  }}
                                />
                              </Col>
                              <Col span={24}>
                                <IButton
                                  title="THEO CẤP BẬC"
                                  color={colors.main}
                                  styleHeight={{
                                    borderRadius: 18,
                                    fontWeight: "bold",
                                    background: "white",
                                  }}
                                  onClick={() => {
                                    if (type === "event") {
                                      setListConfirm([]);
                                    }
                                    setListS1Confirm([]);
                                    setListS1ConfirmID([]);
                                    setArrayRegion([]);
                                    setArrayCity([]);
                                    setArrayMembership([]);
                                    setTypeObject(2);
                                  }}
                                />
                              </Col>
                              <Col span={24}>
                                <IButton
                                  title="THEO KHU VỰC QUẢN LÝ"
                                  color={colors.main}
                                  styleHeight={{
                                    borderRadius: 18,
                                    fontWeight: "bold",
                                    background: "white",
                                  }}
                                  onClick={() => {
                                    if (type === "event") {
                                      setListConfirm([]);
                                    }
                                    setListS1Confirm([]);
                                    setListS1ConfirmID([]);
                                    setArrayRegion([]);
                                    setArrayCity([]);
                                    setArrayMembership([]);
                                    setTypeObject(3);
                                  }}
                                />
                              </Col>
                            </Row>
                          </Col>
                        ) : typeObject === 1 ? (
                          renderTableAgencyC1()
                        ) : typeObject === 2 ? (
                          renderMembership()
                        ) : (
                          renderArea()
                        )}
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={14}>
                <div
                  style={{ padding: 24, height: "100%" }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Danh sách {renderTitle(type)}
                      </StyledITileHeading>
                    </Col>
                    {listConfirm.length === 0 &&
                    productsByObject.arrProduct.length === 0 ? (
                      <Col span={24}>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                            className="cursor"
                            onClick={() => {
                              renderPopup(type);
                            }}
                          >
                            <span
                              style={{
                                marginRight: 12,
                                color: colors.main,
                                fontWeight: 500,
                              }}
                            >
                              {`Thêm ${renderTitle(type)}${
                                type === "event" ? " tặng quà" : ""
                              }`}
                            </span>
                            <div>
                              <ISvg
                                name={ISvg.NAME.ADDUPLOAD}
                                width={20}
                                height={20}
                                fill={colors.main}
                              />
                            </div>
                          </div>
                        </div>
                      </Col>
                    ) : (
                      <Col span={24}>
                        <div style={{ marginTop: 12, height: "100%" }}>
                          <ITableHtml
                            style={{
                              height: "100%",
                              // borderBottom:
                              //   "1px solid rgba(122, 123, 123, 0.5)",
                            }}
                            childrenBody={renderBodyTable(type)}
                            childrenHeader={renderHeaderTable(type)}
                          />
                          {listConfirm.length === 0 &&
                          productsByObject.arrProduct.length === 0 ? null : (
                            <table>
                              <tr
                                className="tr-table"
                                style={{
                                  borderLeft:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                  borderRight:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                }}
                              >
                                <td
                                  className="td-table"
                                  style={{ opacity: 1 }}
                                  colSpan="5"
                                >
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                      marginBottom: 4,
                                      paddingLeft: 15,
                                    }}
                                  >
                                    <div
                                      style={{
                                        cursor: "pointer",
                                        display: "flex",
                                      }}
                                      onClick={() => renderPopup(type)}
                                    >
                                      <span
                                        style={{
                                          marginRight: 12,
                                          marginTop: 2,
                                          color: colors.main,
                                          fontWeight: 500,
                                        }}
                                      >
                                        {`Thêm ${renderTitle(type)}${
                                          type === "event" ? " tặng quà" : ""
                                        }`}
                                      </span>
                                      <div>
                                        <ISvg
                                          name={ISvg.NAME.ADDUPLOAD}
                                          width={20}
                                          height={20}
                                          fill={colors.main}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          )}
                        </div>
                      </Col>
                    )}
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isVisibleAgency1}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1FilterState(
          (listS1Confirm) => {
            let listS1ConfirmID = listS1Confirm.map((item) => item.id);
            setListS1ConfirmID([...listS1ConfirmID]);
            setListS1Confirm([...listS1Confirm]);
            setClearS1(false);
            setTypeObject(1);
            setVisibleAgency1(false);
          },
          () => {
            setClearS1(false);
            setVisibleAgency1(false);
          },
          listS1Confirm,
          clearS1,
          0,
          isVisibleAgency1
        )}
      </Modal>
      <Modal
        visible={visiblePopup}
        footer={null}
        width={type === "event" ? 1200 : 900}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModal(
          (listConfirmNew) => {
            if (listConfirm.length !== listConfirmNew.length) {
              setListConfirm([]);
            }
            setListConfirm([...listConfirmNew]);
            setIsRemove(false);
            setVisiblePopup(false);
          },
          () => {
            setIsRemove(false);
            setVisiblePopup(false);
          },
          listConfirm,
          isRemove,
          type,
          {
            list_user_id: listS1ConfirmID,
            list_membership_id: arrayMembership,
            list_city_id: arrayCity,
            list_region_id: arrayRegion,
            type:
              typeObject === 3
                ? arrayCity.length > 0 && arrayRegion.length > 0
                  ? 5
                  : arrayCity.length > 0 && arrayRegion.length === 0
                  ? 3
                  : 4
                : typeObject,
          },
          visiblePopup
        )}
      </Modal>
      <Modal
        visible={visibleTreeProduct}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalTreeProductByObject(() => {
          setVisibleTreeProduct(false);
        })}
      </Modal>
    </div>
  );
}
