import React, { useState, useEffect, useRef, useCallback } from "react";
import { Row, Col, Tooltip, message, Modal } from "antd";
import { ISearch, ISvg, ISelect, ITable, IButton } from "../../components";
import { colors, images } from "../../../assets";
import FormatterDay from "../../../utils/FormatterDay";
import { StyledITitle } from "../../components/common/Font/font";
import { APIService } from "../../../services";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { CLEAR_REDUX_PRODUCTS_BY_OBJECT } from "../../store/reducers";
import styled from "styled-components";

const ISelectSearch = styled(ISelect)`
  input {
    padding: 0px !important;
  }
`;

function ListHideCateByObject(props) {
  const typingTimeoutRef = useRef(null);
  const history = useHistory();
  const dispatch = useDispatch();

  const [filter, setFilter] = useState({
    status: 2,
    search: "",
    page: 1,
    user_agency_id: 0,
  });

  const [dataTable, setDataTable] = useState({
    listProductType: [],
    total: 1,
    size: 10,
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  // const [listMembership, setListMembership] = useState([]);

  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);

  const [search, setSearch] = useState("");
  const [agencyS1Id, setAgencyS1Id] = useState(-1);
  const [dataAgencyS1, setDataAgencyS1] = useState([]);
  const [loadingAgencyS1, setLoadingAgencyS1] = useState(false);

  const [isModal, setIsModal] = useState(false);
  const [checkUpdate, setCheckUpdate] = useState(0);

  const dataStatus = [
    {
      key: 2,
      value: "Tất cả",
    },
    {
      key: -1,
      value: "Chờ chạy",
    },
    {
      key: 0,
      value: "Đang áp dụng",
    },
    {
      key: 1,
      value: "Tạm dừng",
    },
  ];

  const getListCateBlock = async (filter) => {
    try {
      const data = await APIService._getListCateBlock(filter);
      data.list_product_type.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          id: item.id,
          status: item.status,
        });
        return item;
      });

      setDataTable({
        listProductType: data.list_product_type,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const _getAPIListAgencyS1 = async (search, membership_id) => {
    try {
      const data = await APIService._getListAllAgencyS1(
        1,
        0,
        0,
        -1,
        search,
        membership_id
      );
      let dataNew = data.useragency.map((item) => {
        return {
          key: item.id,
          value: item.shop_name,
        };
      });

      dataNew.unshift({
        key: 0,
        value: "Tất cả",
      });
      setDataAgencyS1([...dataNew]);
      setSearch("");
      setLoadingAgencyS1(false);
    } catch (error) {
      console.log(error);
      setLoadingAgencyS1(false);
    }
  };

  // const _getAPIListMembership = async () => {
  //   try {
  //     const data = await APIService._getListMemberships();
  //     const dataNew = data.membership.map((item, index) => {
  //       const key = item.id;
  //       const value = item.name;
  //       return {
  //         key,
  //         value,
  //       };
  //     });
  //     dataNew.unshift({
  //       value: "Tất cả",
  //       key: 0,
  //     });
  //     setListMembership([...dataNew]);
  //   } catch (error) {}
  // };

  const postUpdateStatusCateBlock = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postUpdateStatusCateBlock(obj);
      setSelectedRowKeys([]);
      setLoadingButton(false);
      setIsModal(false);
      await getListCateBlock(filter);
      message.success(
        `${checkUpdate === 1 ? "Kích hoạt" : "Tạm dừng"} thành công!`
      );
    } catch (err) {
      message.error(err);
      setLoadingButton(false);
    }
  };

  const callAPI = async () => {
    // await _getAPIListMembership();
    await _getAPIListAgencyS1("", 0);
  };

  useEffect(() => {
    callAPI();
  }, []);

  useEffect(() => {
    getListCateBlock(filter);
  }, [filter]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },
    {
      title: "Tên ngành hàng",
      render: (obj) =>
        !obj.product_type_name ? (
          "-"
        ) : (
          <Tooltip title={obj.product_type_name}>
            <span
              onClick={(e) => {
                e.stopPropagation();
                history.push(`/category/detail/1/${obj.product_type_id}`);
              }}
            >
              {obj.product_type_name}
            </span>
          </Tooltip>
        ),
    },

    {
      title: "Tên ĐLC1",

      render: (obj) =>
        !obj.username ? (
          "-"
        ) : (
          <Tooltip title={obj.username}>
            <span
              onClick={(e) => {
                e.stopPropagation();
                history.push(`/detailAgency/${obj.user_id}`);
              }}
            >
              {obj.username}
            </span>
          </Tooltip>
        ),
    },
    {
      title: "Mã ĐLC1",
      dataIndex: "dms_code",
      key: "dms_code",
      render: (dms_code) =>
        !dms_code ? (
          "-"
        ) : (
          <Tooltip title={dms_code}>
            <span>{dms_code}</span>
          </Tooltip>
        ),
    },
    {
      title: "Cấp bậc",
      dataIndex: "membership_name",
      key: "membership_name",
      render: (membership_name) => (
        <span>{!membership_name ? "-" : membership_name}</span>
      ),
    },
    {
      title: "Tỉnh/Thành phố",
      dataIndex: "city_name",
      key: "city_name",
      render: (city_name) => <span>{!city_name ? "-" : city_name}</span>,
    },
    {
      title: "Vùng địa lý",
      dataIndex: "region_name",
      key: "region_name",
      render: (region_name) => <span>{!region_name ? "-" : region_name}</span>,
    },
    {
      title: "Ngày áp dụng",
      dataIndex: "applicable_date",
      key: "applicable_date",
      align: "center",
      width: 150,
      render: (applicable_date) => (
        <span>
          {!applicable_date || applicable_date <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
                applicable_date,
                "#DD#/#MM#/#YYYY# #hhh#:#mm#"
              )}
        </span>
      ),
    },
    {
      title: "Ngày kết thúc",
      dataIndex: "end_date",
      key: "end_date",
      align: "center",
      width: 150,
      render: (end_date) => (
        <span>
          {!end_date || end_date <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
                end_date,
                "#DD#/#MM#/#YYYY# #hhh#:#mm#"
              )}
        </span>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      align: "center",
      render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Ẩn ngành hàng theo đối tượng
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên ngành hàng">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên ngành hàng"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setSelectedRowKeys([]);
                      setFilter({
                        ...filter,
                        search: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={18}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      {/* <div style={{ margin: "0px 15px 0px 0px" }}>
                        <ISelect
                          defaultValue="Cấp bậc"
                          data={listMembership}
                          select={true}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setAgencyS1Id(-1);
                            setFilter({
                              ...filter,
                              membership_id: value,
                              page: 1,
                            });

                            setLoadingAgencyS1(true);
                            _getAPIListAgencyS1(search, value);
                          }}
                        />
                      </div> */}
                      <div style={{ width: 320 }}>
                        <ISelectSearch
                          defaultValue="Tên đại lý"
                          data={dataAgencyS1}
                          select={loadingAgencyS1 ? false : true}
                          style={{ padding: 0 }}
                          isTooltip={true}
                          showSearch
                          optionFilterProp="children"
                          loading={loadingAgencyS1}
                          filterOption={(input, option) => {
                            return (
                              option.props.dataProps.value
                                .toLowerCase()
                                .indexOf(input.toLowerCase()) >= 0
                            );
                          }}
                          onSearch={(value) => {
                            if (typingTimeoutRef.current) {
                              clearTimeout(typingTimeoutRef.current);
                              setSearch("");
                            }
                            typingTimeoutRef.current = setTimeout(() => {
                              setSearch(value);
                            }, 500);
                          }}
                          onChange={(key) => {
                            setLoadingTable(true);
                            setSelectedRowKeys([]);
                            setAgencyS1Id(key);
                            setFilter({
                              ...filter,
                              user_agency_id: key,
                              key: "",
                              page: 1,
                            });
                          }}
                          // onDropdownVisibleChange={(open) => {
                          //   if (open) {
                          //     setLoadingAgencyS1(true);
                          //     _getAPIListAgencyS1(search, filter.membership_id);
                          //     return;
                          //   }
                          // }}
                          value={agencyS1Id === -1 ? "Tên đại lý" : agencyS1Id}
                        />
                      </div>

                      <div style={{ margin: "0px 15px 0px 15px" }}>
                        <ISelect
                          defaultValue="Trạng thái"
                          data={dataStatus}
                          select={true}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setSelectedRowKeys([]);
                            setFilter({
                              ...filter,
                              status: value,
                              page: 1,
                            });
                          }}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className="flex justify-end">
                      <IButton
                        title={"Thiết lập ngành hàng"}
                        color={colors.main}
                        styleHeight={{
                          width: 180,
                        }}
                        onClick={() => {
                          dispatch(CLEAR_REDUX_PRODUCTS_BY_OBJECT());
                          history.push("/hide/cate/create");
                        }}
                      />
                      <IButton
                        title={"Kích hoạt"}
                        color={colors.main}
                        styleHeight={{
                          width: 120,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          if (selectedRowKeys.length === 0) {
                            message.error("Vui lòng chọn đối tượng");
                            return;
                          }

                          let arrCheck = selectedRowKeys.filter((item) => {
                            let itemNew = JSON.parse(item);
                            return itemNew.status === 0;
                          });

                          if (arrCheck.length > 0) {
                            message.error(
                              "Vui lòng chọn đối tượng ở trạng thái 'Chờ chạy' hoặc 'Tạm dừng'"
                            );
                            return;
                          }

                          setCheckUpdate(1);
                          setIsModal(true);
                        }}
                      />
                      <IButton
                        title={"Tạm dừng"}
                        color={colors.oranges}
                        styleHeight={{
                          width: 120,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          if (selectedRowKeys.length === 0) {
                            message.error("Vui lòng chọn đối tượng");
                            return;
                          }

                          let arrCheck = selectedRowKeys.filter((item) => {
                            let itemNew = JSON.parse(item);
                            return (
                              itemNew.status === 1 || itemNew.status === -1
                            );
                          });

                          if (arrCheck.length > 0) {
                            message.error(
                              "Vui lòng chọn đối tượng ở trạng thái 'Đang áp dụng'"
                            );
                            return;
                          }
                          setCheckUpdate(2);
                          setIsModal(true);
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listProductType}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    rowKey="rowTable"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    scroll={{ x: 1600 }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isModal}
        footer={null}
        width={320}
        onCancel={() => setIsModal(false)}
        centered={true}
        closable={false}
      >
        <div style={{ height: "100%" }}>
          <Row gutter={[0, 12]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 18 }}>
                  Thông báo
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  textAlign: "center",
                  fontWeight: 600,
                  color: colors.blackChart,
                }}
              >
                {checkUpdate === 1
                  ? "Bạn có muốn kích hoạt các đối tượng đã được chọn ?"
                  : "Bạn có muốn tạm dừng các đối tượng đã được chọn ?"}
              </div>
            </Col>
          </Row>
          <Row
            gutter={[17, 0]}
            style={{
              position: "absolute",
              bottom: -60,
              left: 0,
            }}
          >
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.oranges}
                  title="Hủy bỏ"
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    setCheckUpdate(0);
                    setIsModal(false);
                  }}
                  icon={ISvg.NAME.CROSS}
                />
              </div>
            </Col>
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.main}
                  title="Xác nhận"
                  loading={loadingButton}
                  icon={ISvg.NAME.CHECKMARK}
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    let arrID = selectedRowKeys.map((item) => {
                      let itemNew = JSON.parse(item);
                      return itemNew.id;
                    });

                    let obj = {
                      list_id: arrID,
                      status: checkUpdate === 1 ? 0 : 1,
                    };

                    postUpdateStatusCateBlock(obj);
                    setCheckUpdate(0);
                  }}
                />
              </div>
            </Col>
          </Row>
        </div>
      </Modal>
    </div>
  );
}

export default ListHideCateByObject;
