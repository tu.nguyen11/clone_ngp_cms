import { Checkbox, Col, List, message, Row, Input } from "antd";
import React, { useEffect, useState } from "react";
import { StyledITileHeading } from "../../components/common/Font/font";
import { colors } from "../../../assets";
import { IButton, ISvg, ISelect, ITableHtml } from "../../components";
import { APIService } from "../../../services";
import styled from "styled-components";

const { Search } = Input;

const StyledSearchCustomNew = styled(Search)`
  height: 42px;

  width: 100%;
  border: 0px !important;

  .ant-input:focus {
    box-shadow: none !important;
  }

  .ant-input-affix-wrapper .ant-input {
    padding-left: 0px !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
  .ant-input-search: not(.ant-input-search-enter-button) {
    padding-right: 6px !important;
  }
  .ant-input-suffix {
    font-size: 20px;
  }

  .ant-input {
    padding-left: 0 !important;
  }
`;

export default function useModal(
  callbackSave = () => {},
  callbackCancel = () => {},
  listCateConfirm,
  isRemove = false,
  type,
  object,
  visiblePopup
) {
  const [search, setSearch] = useState("");

  const [loadingList, setLoadingList] = useState(true);

  const [listCate, setListCate] = useState([]);
  const [listCateClone, setListCateClone] = useState([]);
  const [listCateShow, setListCateShow] = useState([]);

  const _fetchListCate = async (search) => {
    try {
      setLoadingList(true);
      const data = await APIService._getListBrandCategory(0, search);
      let category = data.categories.map((item, index) => {
        const idx = listCateClone.findIndex(
          (itemClone) => itemClone.id === item.id
        );
        if (idx === -1) {
          item.checked = false;
        } else {
          item.checked = true;
        }

        return item;
      });

      setListCate([...category]);
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  const _fetchListSubCate = async (search) => {
    try {
      setLoadingList(true);
      const data = await APIService._getListSubCategpry(0, search);
      let sub_category = data.categories.map((item, index) => {
        const idx = listCateClone.findIndex(
          (itemClone) => itemClone.id === item.id
        );
        if (idx === -1) {
          item.checked = false;
        } else {
          item.checked = true;
        }

        return item;
      });

      setListCate([...sub_category]);
      setLoadingList(false);
    } catch (err) {
      console.log(err);
      setLoadingList(false);
    }
  };

  const _fetchListBrand = async (search) => {
    try {
      setLoadingList(true);
      const data = await APIService._getListOutStandingBrand(search);
      let newData = data.data.map((item, index) => {
        const idx = listCateClone.findIndex(
          (itemClone) => itemClone.id === item.id
        );
        if (idx === -1) {
          item.checked = false;
        } else {
          item.checked = true;
        }

        return item;
      });

      setListCate([...newData]);
      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  const _fetchListEvent = async (search, obj) => {
    try {
      setLoadingList(true);
      const objNew = {
        ...obj,
        search: search,
      };
      const data = await APIService._getListEventRunningByObject(objNew);

      if (!data) {
        message.warning(
          "Những đại lý này hiện không được áp dụng bất kì CSBH hoặc CTKM nào!"
        );
        return;
      }

      if (!isRemove) {
        let newData = data.list_event.map((item, index) => {
          const idx = listCateClone.findIndex(
            (itemClone) => itemClone.id === item.id
          );
          if (idx === -1) {
            item.checked = false;
          } else {
            item.checked = true;
          }

          return item;
        });
        setListCate([...newData]);
      } else {
        let newData = data.list_event.map((item, index) => {
          item.checked = false;

          return item;
        });
        setListCate([...newData]);
      }

      setLoadingList(false);
    } catch (error) {
      console.log(error);
      setLoadingList(false);
    }
  };

  useEffect(() => {
    if (isRemove) {
      setLoadingList(true);
      let listCateTmp = [...listCate];
      const listCateNew = listCateTmp.map((item) => {
        const idx = listCateConfirm.findIndex(
          (itemConfirm) => itemConfirm.id === item.id
        );
        if (idx === -1) {
          item.checked = false;
        } else {
          item.checked = true;
        }

        return item;
      });
      setListCateClone([...listCateConfirm]);
      setListCateShow([...listCateConfirm]);
      setListCate([...listCateNew]);
      setLoadingList(false);
    }
  }, [isRemove]);

  useEffect(() => {
    if (visiblePopup) {
      switch (type) {
        case "cate":
          _fetchListCate(search);
          break;
        case "subcate":
          _fetchListSubCate(search);
          break;
        case "product":
          _fetchListCate(search);
          break;
        case "brand":
          _fetchListBrand(search);
          break;
        case "event":
          _fetchListEvent(search, object);
          break;
        default:
          return;
      }
    }
  }, [search, visiblePopup]);

  const renderTitle = (type) => {
    switch (type) {
      case "cate":
        return "ngành hàng";
      case "subcate":
        return "ngành hàng con";
      case "product":
        return "phiên bản";
      case "brand":
        return "thương hiệu";
      case "event":
        return "CSBH tặng hàng";
      default:
        return "";
    }
  };

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont="10px" maxFont="16px">
            Chọn {renderTitle(type)}
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: "100%", height: "100%" }}>
            <Row gutter={[26, 0]}>
              <Col span={12}>
                <div style={{ width: "100%", height: "100%" }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <Row gutter={[15, 0]}>
                        <Col span={24}>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              width: "100%",
                            }}
                          >
                            <StyledSearchCustomNew
                              style={{ padding: 0 }}
                              onPressEnter={(e) => {
                                setLoadingList(true);
                                setSearch(e.target.value);
                              }}
                              placeholder={`Tìm kiếm theo ${
                                type === "event" ? "mã, tên" : "tên"
                              } ${renderTitle(type)}`}
                            />
                          </div>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          border: "1px solid rgba(122, 123, 123, 0.5)",
                        }}
                      >
                        <Row>
                          {type === "event" ? (
                            <Col span={24}>
                              <Row
                                style={{
                                  background: colors.green._3,
                                  borderBottom:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                }}
                              >
                                <Col span={2}></Col>
                                <Col span={7}>
                                  <div
                                    style={{
                                      fontWeight: 600,
                                      textAlign: "left",
                                      color: colors.blackChart,
                                      padding: "8px 0px 0px 5px",
                                    }}
                                  >
                                    Mã CSBH
                                  </div>
                                </Col>
                                <Col span={10}>
                                  <div
                                    style={{
                                      fontWeight: 600,
                                      textAlign: "left",
                                      color: colors.blackChart,
                                      padding: "8px 0px",
                                    }}
                                  >
                                    Tên CSBH
                                  </div>
                                </Col>
                                <Col span={5}>
                                  <div
                                    style={{
                                      fontWeight: 600,
                                      textAlign: "left",
                                      color: colors.blackChart,
                                      padding: "8px 0px",
                                    }}
                                  >
                                    Loại sản phẩm
                                  </div>
                                </Col>
                              </Row>
                            </Col>
                          ) : null}
                          <Col span={24}>
                            <div style={{ height: 342, overflow: "auto" }}>
                              <List
                                dataSource={listCate}
                                bordered={false}
                                loading={loadingList}
                                renderItem={(item, index) => {
                                  return (
                                    <List.Item>
                                      <div
                                        style={{
                                          width: "100%",
                                          height: "100%",
                                        }}
                                      >
                                        {type === "event" ? (
                                          <Row>
                                            <Col span={2}>
                                              <div
                                                style={{ textAlign: "center" }}
                                              >
                                                <Checkbox
                                                  defaultChecked={item.checked}
                                                  checked={item.checked}
                                                  onChange={(e) => {
                                                    let checked =
                                                      e.target.checked;

                                                    let listCateTmp = [
                                                      ...listCate,
                                                    ];
                                                    listCateTmp[index].checked =
                                                      checked;
                                                    setListCate(listCateTmp);
                                                    let listCateCloneTmp = [
                                                      ...listCateClone,
                                                    ];
                                                    if (checked) {
                                                      listCateCloneTmp.push(
                                                        listCateTmp[index]
                                                      );
                                                      setListCateClone([
                                                        ...listCateCloneTmp,
                                                      ]);
                                                    } else {
                                                      const idx =
                                                        listCateCloneTmp.findIndex(
                                                          (item) =>
                                                            item.dms_code ===
                                                            listCateTmp[index]
                                                              .dms_code
                                                        );
                                                      listCateCloneTmp.splice(
                                                        idx,
                                                        1
                                                      );
                                                      setListCateClone([
                                                        ...listCateCloneTmp,
                                                      ]);
                                                    }
                                                  }}
                                                ></Checkbox>
                                              </div>
                                            </Col>
                                            <Col span={7}>
                                              <div
                                                style={{
                                                  textAlign: "left",
                                                  color: colors.blackChart,
                                                  padding: "0px 5px",
                                                }}
                                              >
                                                {item.code}
                                              </div>
                                            </Col>
                                            <Col span={10}>
                                              <div
                                                style={{
                                                  textAlign: "left",
                                                  color: colors.blackChart,
                                                  padding: "0px 5px",
                                                }}
                                              >
                                                {item.name}
                                              </div>
                                            </Col>
                                            <Col span={5}>
                                              <div
                                                style={{
                                                  textAlign: "center",
                                                  color: colors.blackChart,
                                                  padding: "0px 5px",
                                                }}
                                              >
                                                {item.group_id === 1
                                                  ? "Máy móc"
                                                  : "Phụ tùng"}
                                              </div>
                                            </Col>
                                          </Row>
                                        ) : (
                                          <Row>
                                            <Col span={3}>
                                              <div
                                                style={{ textAlign: "center" }}
                                              >
                                                <Checkbox
                                                  defaultChecked={item.checked}
                                                  checked={item.checked}
                                                  onChange={(e) => {
                                                    let checked =
                                                      e.target.checked;

                                                    let listCateTmp = [
                                                      ...listCate,
                                                    ];
                                                    listCateTmp[index].checked =
                                                      checked;
                                                    setListCate(listCateTmp);
                                                    let listCateCloneTmp = [
                                                      ...listCateClone,
                                                    ];
                                                    if (checked) {
                                                      listCateCloneTmp.push(
                                                        listCateTmp[index]
                                                      );
                                                      setListCateClone([
                                                        ...listCateCloneTmp,
                                                      ]);
                                                    } else {
                                                      const idx =
                                                        listCateCloneTmp.findIndex(
                                                          (item) =>
                                                            item.dms_code ===
                                                            listCateTmp[index]
                                                              .dms_code
                                                        );
                                                      listCateCloneTmp.splice(
                                                        idx,
                                                        1
                                                      );
                                                      setListCateClone([
                                                        ...listCateCloneTmp,
                                                      ]);
                                                    }
                                                  }}
                                                ></Checkbox>
                                              </div>
                                            </Col>

                                            <Col span={21}>
                                              <div
                                                style={{
                                                  textAlign: "left",
                                                  color: colors.blackChart,
                                                  padding: "0px 5px",
                                                }}
                                              >
                                                {item.name}
                                              </div>
                                            </Col>
                                          </Row>
                                        )}
                                      </div>
                                    </List.Item>
                                  );
                                }}
                              />
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <IButton
                          title="Thêm"
                          color={colors.main}
                          icon={ISvg.NAME.BUTTONRIGHT}
                          styleHeight={{
                            width: 140,
                          }}
                          isRight={true}
                          onClick={() => {
                            if (listCateClone.length === 0) {
                              message.error(
                                `Vui lòng chọn ${renderTitle(type)}`
                              );
                              return;
                            }

                            setListCateShow([...listCateClone]);
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={12}>
                <div
                  style={{
                    padding: 8,
                    border: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <span
                    style={{ fontWeight: 600, textTransform: "capitalize" }}
                  >
                    {renderTitle(type)}
                  </span>
                </div>
                <div
                  style={{
                    height: type === "event" ? 450 : 413,
                    borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                    borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                    overflowY: "auto",
                  }}
                >
                  {listCateShow.map((item, index) => {
                    return (
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          padding: "10px 12px",
                          borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                        }}
                      >
                        <div style={{ color: colors.blackChart }}>
                          {item.name}
                        </div>
                        <div
                          style={{
                            background: "rgba(227, 95, 75, 0.1)",
                            width: 20,
                            height: 20,
                            borderRadius: 10,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                          onClick={() => {
                            let listCateTmp = [...listCate];
                            let listCateShowTmp = [...listCateShow];

                            const idxCateRemove = listCateTmp.findIndex(
                              (itemCate) => {
                                return itemCate.id === item.id;
                              }
                            );

                            listCateShowTmp.splice(index, 1);
                            setListCateShow([...listCateShowTmp]);
                            setListCateClone([...listCateShowTmp]);

                            listCateTmp[idxCateRemove].checked = false;
                            setListCate(listCateTmp);
                          }}
                          className="cursor"
                        >
                          <ISvg
                            name={ISvg.NAME.CROSS}
                            width={8}
                            height={8}
                            fill={colors.oranges}
                          />
                        </div>
                      </div>
                    );
                  })}
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              position: "absolute",
              top: 40,
              right: -25,
            }}
          >
            <IButton
              title="Lưu"
              color={colors.main}
              icon={ISvg.NAME.SAVE}
              styleHeight={{
                width: 140,
                marginRight: 15,
              }}
              onClick={() => {
                callbackSave(listCateShow);
              }}
            />
            <IButton
              title="Hủy bỏ"
              color={colors.oranges}
              icon={ISvg.NAME.CROSS}
              styleHeight={{
                width: 140,
              }}
              onClick={() => {
                let listCateTmp = [...listCate];
                let listCateNew = listCateTmp.map((item) => {
                  const idx = listCateConfirm.findIndex(
                    (itemS1Confirm) => itemS1Confirm.id === item.id
                  );

                  if (idx === -1) {
                    item.checked = false;
                  } else {
                    item.checked = true;
                  }

                  return item;
                });
                setListCateShow([...listCateConfirm]);
                setListCateClone([...listCateConfirm]);
                setListCate([...listCateNew]);
                callbackCancel();
              }}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
