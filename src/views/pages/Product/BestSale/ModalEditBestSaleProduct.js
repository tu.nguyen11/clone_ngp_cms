import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  List,
  Checkbox,
  Input,
  message,
  notification,
  Spin,
} from "antd";
import { colors } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { ITitle, ISvg, IButton, ISelect } from "../../../components";
import { APIService } from "../../../../services";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

const { Search } = Input;

const reorder = (list, startIndex, endIndex) => {
  let result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const openNotification = () => {
  notification.error({
    message: "Thông báo",
    description: "Vượt quá số lượng cho phép. Vui lòng xóa bớt sản phẩm",
    onClick: () => {
      console.log("Notification Clicked!");
    },
  });
};

const ModalEditBestSaleProduct = (props) => {
  const [dataTable, setDataTable] = useState({
    rows: [],
    loadingList: true,
  });
  const [loading, setLoading] = useState(true);

  const [callAPI, setCallAPI] = useState(true);

  const [isLoadingSave, setIsLoadingSave] = useState(false);
  const [dataList, setDataList] = useState([]);
  const [dataIDCheck, setDataIDCheck] = useState([]);
  const [filter, setFilter] = useState({
    key: "",
  });

  const [filterLoadMore, setFilterLoadMore] = useState({
    key: "",
  });

  const columns = [
    { title: "STT", key: "stt", width: 60, select: false, textAlign: "center" },
    {
      title: "Tên phiên bản",
      key: "name",
      width: 281,
      select: false,
      textAlign: "left",
    },
    {
      title: "Mã phiên bản",
      key: "code",
      width: 180,
      select: false,
      textAlign: "center",
    },
    {
      title: "Loại hiển thị",
      key: "type",
      width: 180,
      select: true,
      textAlign: "left",
    },
    { title: "", key: "id", width: 60, textAlign: "center" },
  ];

  const typeShow = [
    // {
    //   key: 1,
    //   value: "Giảm giá",
    // },  // {
    //   key: 1,
    //   value: "Giảm giá",
    // },
    {
      key: 2,
      value: "Bán chạy",
    },
  ];

  const onDragEnd = (result) => {
    console.log(result);
    if (!result.destination) {
      return;
    }

    let items = reorder(
      dataTable.rows,
      result.source.index,
      result.destination.index
    );
    items.map((item, index) => {
      item.stt = index + 1;
    });
    let dataIDDrag = items.map((item) => item.id);
    dataList.map((item, index) => {
      if (dataIDDrag.indexOf(Number(item.id)) != -1) {
        item.number = dataIDDrag.indexOf(Number(item.id)) + 1;
        return;
      }
    });
    dataTable.rows = items;
    setDataTable({ ...dataTable, rows: dataTable.rows });
    setDataList(dataList);
    setDataIDCheck(dataIDDrag);
  };

  const _fetchAPIListHotProducts = async (filter) => {
    try {
      const data = await APIService._getAPIAllListHotProducts();
      data.product.map((item, index) => {
        item.stt = index + 1;
        return item;
      });
      debugger;
      let dataID = data.product.map((item) => {
        return item.id;
      });

      setDataTable({
        ...dataTable,
        rows: data.product,
      });

      // setHasMore(data.product.length > 0);
      setDataIDCheck(dataID);

      await _fetchAPIListSelectHotProducts(filter, dataID);

      setCallAPI(false);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setCallAPI(false);
      setLoading(false);
    }
  };

  const _fetchAPIListSelectHotProducts = async (filter, dataID) => {
    try {
      const data = await APIService._getAPIListSelectHotProducts(filter);
      if (dataID.length !== 0) {
        let arrDataID = [...dataID];
        setDataIDCheck(arrDataID);
        data.product.map((item, index) => {
          if (arrDataID.indexOf(Number(item.id)) != -1) {
            item.is_check = 1;
            item.number = arrDataID.indexOf(Number(item.id)) + 1;
            return;
          }
          return (item.is_check = 0);
        });
      } else {
        data.product.map((item, index) => {
          if (dataIDCheck.indexOf(Number(item.id)) != -1) {
            item.is_check = 1;
            item.number = dataIDCheck.indexOf(Number(item.id)) + 1;
            return;
          }
          return (item.is_check = 0);
        });
      }
      setDataList([...data.product]);
      setLoading(false);
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };

  const _postUpdateListHotProducts = async (arr) => {
    try {
      const data = await APIService._postUpdateHotProducts(arr);
      message.success("Cập nhật thành công");
      setIsLoadingSave(false);
      props.handleCancel();
      props.callback1();
    } catch (err) {
      setIsLoadingSave(false);
    }
  };

  const handleRemove = (id) => {
    let dataIDCheckClone = [...dataIDCheck];
    let index = dataTable.rows
      .map((item, index) => {
        return item.id;
      })
      .indexOf(id);
    dataTable.rows.splice(index, 1);
    dataList.map((item, index) => {
      if (item.id == id) {
        item.is_check = 0;
        item.number = 0;
      }
    });

    let indexIdCheck = dataIDCheckClone.indexOf(id);
    dataIDCheckClone.splice(indexIdCheck, 1);

    dataList.map((item, index) => {
      if (dataIDCheckClone.indexOf(Number(item.id)) != -1) {
        item.number = dataIDCheckClone.indexOf(Number(item.id)) + 1;
        return;
      }
    });
    dataTable.rows.map((item, index) => {
      item.stt = index + 1;
    });
    setDataTable({ ...dataTable, rows: dataTable.rows });
    setDataList(dataList);
    setDataIDCheck(dataIDCheckClone);
  };

  useEffect(() => {
    _fetchAPIListHotProducts(filter);
  }, [filter, props.openModalProps]);

  useEffect(() => {
    if (!callAPI) {
      _fetchAPIListSelectHotProducts(filterLoadMore, []);
    }
  }, [filterLoadMore]);

  const tableHeaders = (
    <thead>
      <div>
        <tr>
          {columns.map(function (column, index) {
            return (
              <th
                width={index === columns.length - 1 ? 79 : column.width}
                style={{
                  textAlign: column.textAlign,
                  padding:
                    // index === columns.length - 1
                    //   ? "6px 31px 6px 12px"
                    //   :
                    "6px 12px",
                  borderBottom: "0.5px solid rgba(122, 123, 123, 0.5)",
                  color: colors.blackChart,
                }}
              >
                {column.title}
              </th>
            );
          })}
        </tr>
      </div>
    </thead>
  );

  return (
    <Row gutter={[0, 24]}>
      <Col span={24}>
        <div>
          <StyledITitle>Thiết lập hiển thị sản phẩm nổi bật</StyledITitle>
        </div>
      </Col>
      <Col span={24}>
        <Row gutter={[24, 0]}>
          <Col span={10}>
            <Row gutter={[0, 15]}>
              <Col span={24}>
                <Search
                  placeholder="Tìm phiên bản"
                  onPressEnter={(e) => {
                    setFilterLoadMore({
                      ...filterLoadMore,
                      key: e.target.value,
                    });
                  }}
                />
              </Col>
              <Col span={24}>
                <Row>
                  <div
                    style={{
                      border: "1px solid rgba(122, 123, 123, 0.5)",
                      backgroundColor: "#E4EFFF",

                      display: "flex",
                      alignItems: "center",
                      paddingRight: dataList.length < 7 ? 0 : 18,
                    }}
                  >
                    <Col span={2}></Col>
                    <Col span={11}>
                      <div
                        style={{
                          fontWeight: "bold",
                          padding: "6px 12px",
                          color: colors.blackChart,
                        }}
                      >
                        Tên phiên bản
                      </div>
                    </Col>
                    <Col span={7}>
                      <div
                        style={{
                          fontWeight: "bold",
                          padding: "6px 12px",
                          color: colors.blackChart,
                        }}
                      >
                        Mã phiên bản
                      </div>
                    </Col>
                    <Col
                      span={4}
                      style={{ display: "flex", justifyContent: "center" }}
                    >
                      <div
                        style={{
                          fontWeight: "bold",
                          padding: "6px 12px",
                          color: colors.blackChart,
                        }}
                      >
                        Thứ tự
                      </div>
                    </Col>
                  </div>
                </Row>

                <div
                  style={{
                    height: 366,
                    overflowY: "auto",
                    overflowX: "hidden",
                    borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                    borderLeft: "1px solid rgba(122, 123, 123, 0.5)",
                    borderRight: "1px solid rgba(122, 123, 123, 0.5)",
                  }}
                >
                  <List
                    dataSource={dataList}
                    bordered={false}
                    loading={loading}
                    renderItem={(item, index) => (
                      <Row>
                        <List.Item key={index}>
                          <Col span={2}>
                            <div
                              style={{
                                textAlign: "center",
                              }}
                            >
                              <Checkbox
                                value={{ ...item, ...{ stt: index } }}
                                checked={item.is_check === 1 ? true : false}
                                // style={{ marginRight: 35 }}
                                defaultChecked={
                                  item.is_check === 1 ? true : false
                                }
                                onChange={(e) => {
                                  if (e.target.checked === true) {
                                    dataList[index].is_check = 1;
                                    setDataList(dataList.slice(0));
                                  } else {
                                    dataList[index].is_check = 0;
                                    setDataList(dataList.slice(0));
                                  }
                                }}
                              />
                            </div>
                          </Col>
                          <Col span={11}>
                            <div style={{ padding: "0px 12px" }}>
                              <ITitle level={4} title={item.name} />
                            </div>
                          </Col>
                          <Col span={7}>
                            <div style={{ padding: "0px 12px" }}>
                              <ITitle level={4} title={item.code} />
                            </div>
                          </Col>
                          <Col span={4}>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "center",
                                padding: "0px 12px",
                              }}
                            >
                              <ITitle
                                level={4}
                                title={item.number === 0 ? "" : item.number}
                              />
                            </div>
                          </Col>
                        </List.Item>
                      </Row>
                    )}
                    // columns={columns}
                    // bodyStyle={{ height: 400 }}
                    // rowSelection={rowSelection}
                  />
                </div>
              </Col>
              <Col span={24}>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    title="Thêm"
                    color={colors.main}
                    icon={ISvg.NAME.BUTTONRIGHT}
                    styleHeight={{
                      width: 140,
                    }}
                    isRight={true}
                    onClick={() => {
                      let dataCheck = dataList;
                      let dataIDCheckClone = dataIDCheck;
                      let dataNew = [];

                      if (dataTable.rows.length === 50) {
                        openNotification();
                        return;
                      } else {
                        dataCheck.map((item) => {
                          if (item.is_check === 1) {
                            let dataFilter = dataTable.rows.find((item1) => {
                              return item1.id === item.id;
                            });
                            if (!dataFilter) {
                              dataNew.push(item);
                            }
                          } else {
                            let indexFilter = dataTable.rows.findIndex(
                              (item1) => {
                                return item1.id === item.id;
                              }
                            );
                            let indexIdFilter = dataIDCheckClone.indexOf(
                              item.id
                            );
                            if (indexFilter > -1) {
                              dataTable.rows.splice(indexFilter, 1);
                              dataIDCheckClone.splice(indexIdFilter, 1);
                            }
                          }
                        });

                        dataNew.map((item) => {
                          item.type = 1;
                          return item;
                        });
                        dataNew.map((item) => {
                          dataTable.rows.unshift(item);
                          dataIDCheckClone.unshift(item.id);
                        });
                        dataTable.rows.map((item, index) => {
                          item.stt = index + 1;
                        });
                        dataList.map((item) => {
                          if (dataIDCheckClone.indexOf(Number(item.id)) != -1) {
                            item.number =
                              dataIDCheckClone.indexOf(Number(item.id)) + 1;
                            return;
                          }
                        });
                        setDataTable({ ...dataTable, rows: dataTable.rows });
                        setDataIDCheck(dataIDCheckClone);
                        setDataList(dataList);
                      }
                    }}
                  />
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={14}>
            <div
              style={{
                height: 505,
                // overflow: "auto",
                border: "1px solid rgba(122, 123, 123, 0.5)",
              }}
            >
              {callAPI ? (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "100%",
                  }}
                >
                  <Spin />
                </div>
              ) : (
                <DragDropContext onDragEnd={onDragEnd}>
                  <Droppable droppableId="droppable">
                    {(droppableProvided, droppableSnapshot) => (
                      <div ref={droppableProvided.innerRef}>
                        <table width="100%">
                          {tableHeaders}

                          <div
                            style={{
                              height: 469,
                              overflow: "auto",
                            }}
                          >
                            {dataTable.rows.map((row, index) => {
                              return (
                                <Draggable
                                  key={row.id}
                                  draggableId={row.code}
                                  index={index}
                                >
                                  {(draggableProvided, draggableSnapshot) => (
                                    <div
                                      className="table-edit"
                                      ref={draggableProvided.innerRef}
                                      {...draggableProvided.draggableProps}
                                      {...draggableProvided.dragHandleProps}
                                    >
                                      <tr
                                        style={{
                                          borderBottom:
                                            "1px solid rgba(122, 123, 123, 0.5)",
                                        }}
                                        /*ref={lastProductElementRef}*/
                                      >
                                        {columns.map(function (column) {
                                          return column.key == "id" ? (
                                            <td
                                              width={column.width}
                                              style={{
                                                padding: "12px 12px",
                                                textAlign:
                                                  column.key !== "name"
                                                    ? "center"
                                                    : "left",
                                                verticalAlign:
                                                  column.key !== "name"
                                                    ? "middle"
                                                    : "none ",
                                              }}
                                            >
                                              <div
                                                style={{
                                                  display: "flex",
                                                  justifyContent: "center",
                                                }}
                                              >
                                                <div
                                                  style={{
                                                    width: 20,
                                                    height: 20,
                                                    borderRadius: 10,
                                                    display: "flex",
                                                    justifyContent: "center",
                                                    alignItems: "center",
                                                    background:
                                                      "rgba(227, 95, 75, 0.1)",
                                                    cursor: "pointer",
                                                  }}
                                                  onClick={() => {
                                                    handleRemove(
                                                      row[column.key]
                                                    );
                                                  }}
                                                >
                                                  <ISvg
                                                    name={ISvg.NAME.CROSS}
                                                    width={7}
                                                    height={7}
                                                    fill={colors.oranges}
                                                  />
                                                </div>
                                              </div>
                                            </td>
                                          ) : column.select ? (
                                            <td
                                              width={column.width}
                                              style={{
                                                padding: "12px 12px",
                                                textAlign:
                                                  column.key !== "name"
                                                    ? "center"
                                                    : "left",
                                                verticalAlign:
                                                  column.key !== "name"
                                                    ? "middle"
                                                    : "none ",
                                              }}
                                            >
                                              <ISelect
                                                data={typeShow}
                                                select={true}
                                                defaultValue={
                                                  /**Todo
                                                   * Disable giảm giá
                                                   * */
                                                  // row.type === 1
                                                  //   ? typeShow[0].value
                                                    typeShow[0].value
                                                }
                                                onChange={(value) => {
                                                  dataTable.rows.map((item) => {
                                                    if (item.id === row.id) {
                                                      item.type = value;
                                                      return item;
                                                    }
                                                    return  item;
                                                  });
                                                }}
                                                isBackground={false}
                                              />
                                            </td>
                                          ) : (
                                            <td
                                              style={{
                                                padding: "6px 12px",
                                                textAlign:
                                                  column.key !== "name"
                                                    ? "center"
                                                    : "left",
                                                verticalAlign: "middle",
                                                width: column.width,
                                                color: colors.blackChart,
                                              }}
                                            >
                                              {row[column.key]}
                                            </td>
                                          );
                                        })}
                                      </tr>
                                    </div>
                                  )}
                                </Draggable>
                              );
                            })}
                          </div>
                          {droppableProvided.placeholder}
                        </table>
                      </div>
                    )}
                  </Droppable>
                </DragDropContext>
              )}
            </div>
          </Col>
        </Row>
      </Col>
      <Col span={24} style={{ padding: 0 }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "absolute",
            top: 30,
            right: -25,
          }}
        >
          <IButton
            title="Lưu"
            color={colors.main}
            icon={ISvg.NAME.SAVE}
            loading={isLoadingSave}
            styleHeight={{
              width: 140,
              marginRight: 24,
            }}
            onClick={() => {
              setIsLoadingSave(true);
              let arrId = dataTable.rows.map((item, index) => {
                return {
                  id: item.id,
                  /**
                   * Todo
                   * Disable Giảm giá
                   */
                  // type: item.type,
                  type: 2,
                };
              });
              _postUpdateListHotProducts(arrId);
            }}
          />
          <IButton
            title="Hủy bỏ"
            color={colors.oranges}
            icon={ISvg.NAME.CROSS}
            styleHeight={{
              width: 140,
            }}
            onClick={() => {
              props.handleCancel();
            }}
          />
        </div>
      </Col>
    </Row>
  );
};

export default ModalEditBestSaleProduct;
