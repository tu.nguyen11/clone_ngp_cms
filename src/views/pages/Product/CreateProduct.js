import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  Upload,
  Icon,
  Form,
  message,
} from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  ISelectStatus,
  ICascader,
  ISelectAttrubie,
} from "../../components";
import { priceFormat } from "../../../utils";

import InfiniteScroll from "react-infinite-scroller";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import IUploadMultiple from "../../components/IUploadMultiple";
import { ImageType } from "../../../constant";
const { Paragraph, Title } = Typography;

class CreateProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrayImagesAdd: [],
      loading: false,
      write: true,
      valueStoreProduct: 0,
      nameStoreProduct: "",
      status: 0,
      errStatus: true,
      errCategory: true,
      typeId: 0,
      categoryId: 0,
      errAttributesMax: true,
      errAttributesMin: true,
      attributeIdMin: 0,
      attributeIdMax: 0,
      value: "", // Check here to configure the default column
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeStatus = this.onChangeStatus.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
  }

  componentDidMount() {
    // const type = this.props.location.state.type;
    // if (type === "create") {
    //   return;
    // }
    // if (type === "edit") {
    //   this.props.form.setFieldsValue({
    //     supplierName: 100
    //   });
    // }
  }

  _APIpostAddProduct = async (objectAdd) => {
    try {
      const data = await APIService._postAddProduct(objectAdd);
      message.success("Thêm sản phẩm thành công");
      this.props.history.push("/productPage");
    } catch (err) {
      console.log(err);
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      fieldsValue.typeId = this.state.typeId;
      fieldsValue.categoryId = this.state.categoryId;
      fieldsValue.status = this.state.status;
      // fieldsValue.id = 0; // 0 là thêm server rằng
      fieldsValue.image = ["hinh12.png"];
      const objAdd = {
        id: 0, // 0 là thêm server rằng
        productCode: fieldsValue.productCode,
        productName: fieldsValue.productName,
        status: this.state.status,
        typeId: fieldsValue.typeId,
        categoryId: fieldsValue.categoryId,
        attributeIdMin: this.state.attributeIdMin,
        quantityMin: Number(fieldsValue.quantityMin),
        attributeIdMax: this.state.attributeIdMax,
        quantityMax: Number(fieldsValue.quantityMax),
        priceMin: Number(fieldsValue.priceMin),
        priceMax: Number(fieldsValue.priceMax),
        distributorOffer: 0,
        description: fieldsValue.description,
        element: fieldsValue.element,
        uses: fieldsValue.uses,
        note: fieldsValue.note,
        image: this.state.arrayImagesAdd,
      };

      this._APIpostAddProduct(objAdd);
    });
  };

  onChangeStatus = (value) => {
    this.setState({ status: value.key, errStatus: false }, () =>
      this.props.form.validateFields(["status"], { force: true })
    );
  };

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
    }
    if (value.length == 2) {
      this.setState({
        errCategory: false,
        typeId: value[0],
        categoryId: value[1],
      });
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Container fluid>
        <Row sm={12} md={12} className="mt-3">
          <ITitle
            level={1}
            title="Tạo mới sản phẩm"
            style={{ color: colors.main, fontWeight: "bold" }}
          />
        </Row>
        <Form onSubmit={this.handleSubmit}>
          <Row
            // className="mt-4"
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingTop: 32,
              paddingBottom: 32,
            }}
          >
            <IButton
              icon={ISvg.NAME.SAVE}
              title="Lưu"
              htmlType="submit"
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => this.handleSubmit}
            />
            <IButton
              icon={ISvg.NAME.CROSS}
              title="Hủy"
              color={colors.oranges}
              style={{}}
              onClick={() => {
                this.props.history.push("/productPage");
              }}
            />
          </Row>
          <div style={{}}>
            <Row
              style={{ height: "100%" }}
              // className="p-0 m-0 mt-4"
              sm={12}
              md={12}
            >
              <Col
                xs={6}
                className="mr-2 shadow"
                style={{ background: colors.white }}
              >
                <Row className="p-0 m-0" style={{ height: "100%" }}>
                  <Col className="pl-0 pr-0 pt-0 m-0 ml-2">
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Thông tin sản phẩm"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>

                    <Form.Item>
                      {getFieldDecorator("productName", {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: true,
                            message: "nhập tên gọi đầy đủ",
                          },
                        ],
                      })(
                        <Row className="p-0 m-0" xs="auto">
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Tên sản phẩm"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput placeholder="nhập tên gọi đầy đủ" />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("productCode", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: true,
                                message: "nhập mã",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Mã sản phẩm"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput placeholder="nhập mã" />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("status", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: this.state.errStatus,
                                message: "chọn trạng thái",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Trạng thái"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectStatus
                                  isBackground={false}
                                  style={{
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  onChange={this.onChangeStatus}
                                  placeholder=""
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0">
                      <Col className="p-0 mt-0">
                        <Form.Item>
                          {getFieldDecorator("category", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: this.state.errCategory,
                                message: "chọn ngành hàng",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Ngành hàng"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ICascader
                                  isBackground={false}
                                  type="category"
                                  level={2}
                                  placeholder="chọn ngành hàng"
                                  onChange={this.onChangeCategory}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                      <Col></Col>
                    </Row>
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Quy cách"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>
                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("attributeIdMin", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: this.state.errAttributesMin,

                                message: "chai/túi/bao 10kg....",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Đơn vị lẻ"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectAttrubie
                                  isBackground={false}
                                  placeholder="chai/túi/bao 10kg...."
                                  reponsive
                                  type="min"
                                  onChange={(value) => {
                                    this.setState(
                                      {
                                        attributeIdMin: value,
                                        errAttributesMin: false,
                                      },
                                      () =>
                                        this.props.form.validateFields(
                                          ["attributeIdMin"],
                                          { force: true }
                                        )
                                    );
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("attributeIdMax", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: this.state.errAttributesMax,
                                message: "vui lòng chọn",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Đơn vị lớn nhất"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectAttrubie
                                  isBackground={false}
                                  placeholder="vui lòng chọn"
                                  type="max"
                                  onChange={(value) => {
                                    this.setState(
                                      {
                                        attributeIdMax: value,
                                        errAttributesMax: false,
                                      },
                                      () =>
                                        this.props.form.validateFields(
                                          ["attributeIdMax"],
                                          { force: true }
                                        )
                                    );
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("quantityMin", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: true,
                                message: "nhập cân nặng mỗi đơn vị",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Quy cách đơn vị lẻ"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  placeholder="nhập cân nặng mỗi đơn vị"
                                  type="number"
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("quantityMax", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: true,
                                message: "nhập số lượng",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Quy cách đơn vị lớn nhất"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  placeholder="nhập số lượng"
                                  type="number"
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    {/* <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
      <ITitle
        level={2}
        title="Lưu kho"
        style={{ color: colors.black, fontWeight: "bold" }}
      />
    </Row>
    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
      <Col className="p-0 m-0">
        <Row className="p-0 m-0">
          <ITitle
            level={4}
            title={"Kho lưu trữ"}
            style={{ fontWeight: 600, marginBottom: 12 }}
          />
        </Row>
        {this.state.dataLuuKho.map((item, index) => {
          return (
            <Row className="p-0 mt-1 ml-0 mr-0 mb-0">
              <IInput
                write={this.state.write}
                defaultValue={item.kho}
                style={{ height: 20 }}
              />
            </Row>
          );
        })}
      </Col>

      <Col className="p-0" style={{ marginLeft: 28, marginTop: 0 }}>
        <Row className="p-0 m-0">
          <ITitle
            level={4}
            title={"Trọng lượng thùng/kiện"}
            style={{ fontWeight: 600, marginBottom: 12 }}
          />
        </Row>
        {this.state.dataLuuKho.map((item, index) => {
          return (
            <Row className="p-0 mt-1 ml-0 mr-0 mb-0">
              <IInput
                write={this.state.write}
                defaultValue={item.soluong}
                style={{ height: 20, flex: 1 }}
              />
              {!this.state.write ? null : (
                <div
                  style={{
                    width: 20,
                    height: 20,
                    background: "#FCEFED",
                    borderRadius: 10,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  className="cursor"
                  onClick={() => {
                    let a = this.state.dataLuuKho.splice(index, 1);
                    this.setState({
                      dataLuuKho: this.state.dataLuuKho
                    });
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.CROSS}
                    width={7}
                    height={7}
                    fill={colors.oranges}
                  />
                </div>
              )}
            </Row>
          );
        })}
      </Col>
    </Row>

    {!this.state.write ? null : (
      <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
        <Col className="p-0 m-0">
          <ISelect
            type="write"
            select={this.state.write}
            placeholder="Chọn thêm kho"
            // defaultValue={1}
            data={[
              { key: 1, value: "Kho KhoiDze" },
              { key: 2, value: "Kho KhoiDze1" }
            ]}
            onChange={(key, data) => {
              let value = data.props.children;
              this.setState({
                nameStoreProduct: value
              });
            }}
          />
        </Col>

        <Col
          className="p-0"
          style={{ marginLeft: 28, marginTop: 0 }}
        >
          <Row
            className="p-0 mt-0 ml-0 mr-0 mb-0"
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ISelect
              type="write"
              select={this.state.write}
              style={{ flex: 1 }}
              onChange={(key, data) => {
                let value = data.props.children;
                this.setState({
                  valueStoreProduct: value
                });
              }}
              placeholder="thêm số lượng còn lại"
              data={[
                { key: 1, value: 10000 },
                { key: 2, value: 20000 }
              ]}
            />
            <div
              style={{
                width: 20,
                height: 20,
                background: "#FCEFED",
                borderRadius: 10,
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
              className="cursor"
              onClick={index => {
                let obj = {
                  kho: this.state.nameStoreProduct,
                  soluong: this.state.valueStoreProduct
                };
                this.state.dataLuuKho = [
                  ...this.state.dataLuuKho,
                  obj
                ];
                this.state.nameStoreProduct = "";
                this.state.valueStoreProduct = 0;
                this.setState({});
              }}
            >
              <ISvg
                name={ISvg.NAME.ADD}
                width={7}
                height={7}
                fill={colors.main}
              />
            </div>
          </Row>
        </Col>
      </Row>
    )} */}
                  </Col>
                  <Col className="m-0 p-0 " xs="auto">
                    <div
                      style={{
                        width: 1,
                        height: "100%",
                        background: colors.gray._300,
                        marginLeft: 25,
                      }}
                    ></div>
                  </Col>
                  <Col className="pr-0 pt-0 m-0" xs={4}>
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Giá bán"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>
                    <Col className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("priceMin", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập giá tiền mỗi đơn vị",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Giá đơn vị lẻ"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0" style={{}}>
                              <IInput
                                type="number"
                                placeholder={"nhập giá tiền mỗi đơn vị"}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>

                      <Form.Item>
                        {getFieldDecorator("priceMax", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập giá tiền thùng/kiện",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Giá theo thùng/kiện"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                type="number"
                                placeholder={"nhập giá tiền thùng/kiện"}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>

                      {/* <Form.Item>
                        {getFieldDecorator("distributorOffer", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "nhập phần trăm chiết khấu "
                            }
                          ]
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Chiết khấu đại lý"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                type="number"
                                placeholder={"nhập phần trăm chiết khấu"}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item> */}
                    </Col>
                  </Col>
                </Row>
              </Col>

              <Col
                className="ml-2 shadow
"
                style={{ background: colors.white }}
                // xs={3}
              >
                <Row className="p-0 m-0 ml-2" style={{ marginLeft: 40 }}>
                  <Col
                    className="pl-0 pr-0 pt-0 m-0 mr-2"
                    // style={{paddingBottom: 40}}
                  >
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Mô tả sản phẩm"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>
                    <Form.Item>
                      {getFieldDecorator("description", {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: true,
                            message: "nhập mô tả sản phẩm",
                          },
                        ],
                      })(
                        <Row className="p-0 m-0" xs="auto">
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Mô tả"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                className="input-border"
                                loai="textArea"
                                style={{ height: 140, marginTop: 10 }}
                                placeholder="nhập mô tả sản phẩm"
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Form.Item>
                      {getFieldDecorator("element", {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: true,
                            message: "nhập thành phần sản phẩm",
                          },
                        ],
                      })(
                        <Row
                          className="p-0"
                          style={{
                            marginTop: 20,
                            marginLeft: 0,
                            marginBottom: 0,
                            marginRight: 0,
                          }}
                          xs="auto"
                        >
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Thành phần"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                className="input-border"
                                loai="textArea"
                                style={{ height: 140, marginTop: 10 }}
                                placeholder="nhập thành phần sản phẩm"
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Form.Item>
                      {getFieldDecorator("uses", {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: true,
                            message: "nhập hướng dẫn sử dụng",
                          },
                        ],
                      })(
                        <Row
                          className="p-0"
                          style={{
                            marginTop: 20,
                            marginLeft: 0,
                            marginBottom: 0,
                            marginRight: 0,
                          }}
                          xs="auto"
                        >
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Sử dụng"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                className="input-border"
                                loai="textArea"
                                style={{ height: 75, marginTop: 10 }}
                                placeholder="nhập hướng dẫn sử dụng"
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Form.Item>
                      {getFieldDecorator("note", {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            required: true,
                            message:
                              "nhập lưu ý, NSX, HSD hoặc cách thức bảo quản sản phẩm",
                          },
                        ],
                      })(
                        <Row
                          className="p-0"
                          style={{
                            marginTop: 20,
                            marginLeft: 0,
                            marginBottom: 0,
                            marginRight: 0,
                          }}
                          xs="auto"
                        >
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Lưu ý"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                className="input-border"
                                loai="textArea"
                                style={{ height: 75, marginTop: 10 }}
                                placeholder="nhập lưu ý, NSX, HSD hoặc cách thức bảo quản sản phẩm"
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col
                className="pl-0 pr-0 pt-0 m-0 ml-2 shadow
"
                style={{ background: colors.white }}
                xs="auto"
              >
                <Row className="p-0 mt-4 mb-4  mr-0">
                  <ITitle
                    level={2}
                    title="Hình ảnh sản phẩm"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      marginLeft: 42,
                    }}
                  />
                </Row>

                <Form.Item>
                  {getFieldDecorator("avatar", {
                    rules: [
                      {
                        // required: this.state.checkNick,
                        required: true,
                        message: "chọn hình",
                        valuePropName: "fileList",
                        getValueFromEvent: this.normFile,
                      },
                    ],
                  })(
                    <Row
                      className="p-0 mt-4 mb-4  mr-0"
                      style={{ width: 240, marginLeft: 30, marginRight: 40 }}
                    >
                      <IUploadMultiple
                        // images={data.images}
                        width={20}
                        type={ImageType.PRODUCT}
                        callback={(array) => {
                          this.state.arrayImagesAdd = this.state.arrayImagesAdd.concat(
                            array
                          );
                          this.setState({
                            // arrayImagesAdd: this.state.arrayImagesAdd
                          });
                        }}
                        remove={(index) => {
                          this.state.arrayImagesAdd.splice(index, 1);

                          this.setState({
                            arrayImagesAdd: this.state.arrayImagesAdd,
                          });
                        }}
                        // images={images}
                      />
                      {/* <IUpload
              url=""
              name=""
              type={ProductType.PRODUCT}
              callback={name => {
                //images.push(name)
                //this.setState({})
              }} 
            /> */}
                    </Row>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </div>
        </Form>
      </Container>
    );
  }
}

const createProduct = Form.create({ name: "CreateProduct" })(CreateProduct);

export default createProduct;
