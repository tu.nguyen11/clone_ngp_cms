import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message, Modal } from "antd";
import { ISearch, ISvg, ISelect, ITable, IButton } from "../../../components";
import { colors, images } from "../../../../assets";
import FormatterDay from "../../../../utils/FormatterDay";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import { priceFormat } from "../../../../utils";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { CLEAR_REDUX_PRODUCTS_BY_OBJECT } from "../../../store/reducers";

function ListProductByObject(props) {
  const history = useHistory();
  const dispatch = useDispatch();

  const [filter, setFilter] = useState({
    region_id: 0,
    city_id: 0,
    membership_id: 0,
    status: 0,
    search: "",
    page: 1,
  });

  const [dataTable, setDataTable] = useState({
    listProduct: [],
    total: 1,
    size: 10,
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [listRegion, setListRegion] = useState([]);
  const [regionId, setRegionId] = useState(-1);
  const [cityId, setCityId] = useState(-1);
  const [listCities, setListCities] = useState([]);
  const [listMembership, setListMembership] = useState([]);

  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);
  const [loadingRegions, setLoadingRegions] = useState(true);
  const [loadingCities, setLoadingCities] = useState(false);

  const [isModal, setIsModal] = useState(false);
  const [typeStatus, setTypeStatus] = useState(-1);

  const dataStatus = [
    {
      key: 0,
      value: "Tất cả",
    },
    {
      key: 2,
      value: "Chờ duyệt",
    },
    {
      key: 4,
      value: "Đã kích hoạt",
    },
    {
      key: 1,
      value: "Đang áp dụng",
    },
    {
      key: 3,
      value: "Tạm dừng",
    },
  ];

  const _fetchAPIListProductsByObject = async (filter) => {
    try {
      const data = await APIService._getListProductByObject(filter);
      data.data.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.disabled =
          item.status_id === 3 || item.status_id === 4 ? true : false;
        item.rowTable = JSON.stringify({
          id: item.id,
          status: item.status_id,
        });
        return item;
      });

      setDataTable({
        listProduct: data.data,
        size: data.size,
        total: data.total_record,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const _fetchAPIListRegion = async () => {
    try {
      const data = await APIService._getAllAgencyGetRegion();
      let dataNew = data.agency.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      dataNew.unshift({
        key: 0,
        value: "Tất cả",
      });
      setListRegion([...dataNew]);
      setLoadingRegions(false);
    } catch (error) {
      console.log(error);
      setLoadingRegions(false);
    }
  };

  const _fetchAPIListCitiesByRegion = async (arrRegionId) => {
    try {
      const data = await APIService._getListCityByRegion(arrRegionId);
      let dataNew = data.list_cities.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });

      dataNew.unshift({
        key: 0,
        value: "Tất cả",
      });

      setListCities(dataNew);
      setLoadingCities(false);
    } catch (error) {
      console.log(error);
      setLoadingCities(false);
    }
  };

  const _getAPIListMembership = async () => {
    try {
      const data = await APIService._getListMemberships();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setListMembership([...dataNew]);
    } catch (error) {}
  };

  const postStopProductByObject = async (arr) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postStopProductByObject(arr);
      setSelectedRowKeys([]);
      setLoadingButton(false);
      setTypeStatus(-1);
      setIsModal(false);
      await _fetchAPIListProductsByObject(filter);
      message.success("Tạm dừng thành công!");
    } catch (err) {
      message.error(err);
      setLoadingButton(false);
    }
  };

  const postApproveProductMultiPrice = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postApproveProductMultiPrice(obj);
      setSelectedRowKeys([]);
      setLoadingButton(false);
      setTypeStatus(-1);
      setIsModal(false);
      await _fetchAPIListProductsByObject(filter);
      message.success("Duyệt thành công!");
    } catch (err) {
      message.error(err);
      setLoadingButton(false);
    }
  };

  const callAPI = async () => {
    await _fetchAPIListRegion();
    await _getAPIListMembership();
  };

  useEffect(() => {
    callAPI();
  }, []);

  useEffect(() => {
    _fetchAPIListProductsByObject(filter);
  }, [filter]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      disabled: record.disabled,
    }),
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },

    {
      title: "Tên phiên bản",
      dataIndex: "fullname",
      key: "fullname",
      render: (fullname) =>
        !fullname ? (
          "-"
        ) : (
          <Tooltip title={fullname}>
            <span>{fullname}</span>
          </Tooltip>
        ),
    },
    {
      title: "Mã phiên bản",
      dataIndex: "product_code",
      key: "product_code",
      render: (product_code) =>
        !product_code ? (
          "-"
        ) : (
          <Tooltip title={product_code}>
            <span>{product_code}</span>
          </Tooltip>
        ),
    },
    {
      title: "ĐVT",
      dataIndex: "attribute_name",
      key: "attribute_name",
      align: "center",
      render: (attribute_name) => (
        <span>{!attribute_name ? "-" : attribute_name}</span>
      ),
    },
    {
      title: "Giá theo đối tượng",
      dataIndex: "price_multi_price",
      key: "price_multi_price",
      align: "center",
      width: 150,
      render: (price_multi_price) => (
        <span>{!price_multi_price ? 0 : priceFormat(price_multi_price)}</span>
      ),
    },
    {
      title: "Mã ĐLC1",
      dataIndex: "dms_code",
      key: "dms_code",
      render: (dms_code) =>
        !dms_code ? (
          "-"
        ) : (
          <Tooltip title={dms_code}>
            <span>{dms_code}</span>
          </Tooltip>
        ),
    },
    {
      title: "Tên ĐLC1",
      dataIndex: "shop_name",
      key: "shop_name",
      render: (shop_name) =>
        !shop_name ? (
          "-"
        ) : (
          <Tooltip title={shop_name}>
            <span>{shop_name}</span>
          </Tooltip>
        ),
    },
    {
      title: "Cấp bậc",
      dataIndex: "membership_name",
      key: "membership_name",
      render: (membership_name) => (
        <span>{!membership_name ? "-" : membership_name}</span>
      ),
    },
    {
      title: "Tỉnh/Thành phố",
      dataIndex: "cities_name",
      key: "cities_name",
      render: (cities_name) =>
        !cities_name ? (
          "-"
        ) : (
          <Tooltip title={cities_name}>
            <span>{cities_name}</span>
          </Tooltip>
        ),
    },
    {
      title: "Vùng địa lý",
      dataIndex: "region_name",
      key: "region_name",
      render: (region_name) =>
        !region_name ? (
          "-"
        ) : (
          <Tooltip title={region_name}>
            <span>{region_name}</span>
          </Tooltip>
        ),
    },
    {
      title: "Ngày áp dụng",
      dataIndex: "effectDate",
      key: "effectDate",
      align: "center",
      width: 150,
      render: (effectDate) => (
        <span>
          {!effectDate || effectDate <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
                effectDate,
                "#DD#/#MM#/#YYYY# #hhh#:#mm#"
              )}
        </span>
      ),
    },
    {
      title: "Ngày kết thúc",
      dataIndex: "endDate",
      key: "endDate",
      align: "center",
      width: 150,
      render: (endDate) => (
        <span>
          {!endDate || endDate <= 0
            ? "-"
            : FormatterDay.dateFormatWithString(
                endDate,
                "#DD#/#MM#/#YYYY# #hhh#:#mm#"
              )}
        </span>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      align: "center",
      render: (status) => <span>{!status ? "-" : status}</span>,
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Sản phẩm theo đối tượng
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo mã, tên phiên bản, mã ĐLC1">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo mã, tên phiên bản, mã ĐLC1"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        search: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={18}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <div style={{ margin: "0px 15px 0px 0px" }}>
                        <div style={{ width: 155 }}>
                          <ISelect
                            defaultValue="Vùng địa lý"
                            data={listRegion}
                            loading={loadingRegions}
                            select={true}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setRegionId(value);
                              setCityId(-1);
                              setFilter({
                                ...filter,
                                region_id: value,
                                city_id: 0,
                                page: 1,
                              });
                            }}
                            onDropdownVisibleChange={(open) => {
                              if (!open) {
                                if (listRegion.length === 0) {
                                  return;
                                }
                                setLoadingCities(true);
                                _fetchAPIListCitiesByRegion([filter.region_id]);
                                return;
                              }
                            }}
                          />
                        </div>
                      </div>

                      <div style={{ margin: "0px 15px 0px 0px" }}>
                        <div style={{ width: 190 }}>
                          <ISelect
                            defaultValue="Tỉnh/Thành phố"
                            data={listCities}
                            loading={loadingCities}
                            select={regionId === -1 ? false : true}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setCityId(value);
                              setFilter({
                                ...filter,
                                city_id: value,
                                page: 1,
                              });
                            }}
                            onDropdownVisibleChange={(open) => {
                              if (open) {
                                setLoadingCities(true);
                                _fetchAPIListCitiesByRegion([filter.region_id]);
                                return;
                              }
                            }}
                            value={cityId === -1 ? "Tỉnh/Thành phố" : cityId}
                          />
                        </div>
                      </div>

                      <div style={{ margin: "0px 15px 0px 0px" }}>
                        <ISelect
                          defaultValue="Cấp bậc"
                          data={listMembership}
                          select={true}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setFilter({
                              ...filter,
                              membership_id: value,
                              page: 1,
                            });
                          }}
                        />
                      </div>

                      <div style={{ margin: "0px 15px 0px 0px" }}>
                        <ISelect
                          defaultValue="Trạng thái"
                          data={dataStatus}
                          select={true}
                          onChange={(value) => {
                            setLoadingTable(true);
                            setFilter({
                              ...filter,
                              status: value,
                              page: 1,
                            });
                          }}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col span={6}>
                    <div className="flex justify-end">
                      <IButton
                        title={"Thiết lập sản phẩm"}
                        color={colors.main}
                        styleHeight={{
                          width: 180,
                        }}
                        onClick={() => {
                          dispatch(CLEAR_REDUX_PRODUCTS_BY_OBJECT());
                          history.push("/product/object/create");
                        }}
                      />
                      <IButton
                        title={"Duyệt"}
                        color={colors.main}
                        loading={loadingButton}
                        styleHeight={{
                          width: 100,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          if (selectedRowKeys.length === 0) {
                            message.error(
                              "Vui lòng chọn sản phẩm theo đối tượng"
                            );
                            return;
                          }

                          const arrCheck = selectedRowKeys.filter((item) => {
                            let itemParse = JSON.parse(item);
                            return itemParse.status === 1;
                          });

                          if (arrCheck.length > 0) {
                            message.error(
                              "Vui lòng chọn sản phẩm theo đối tượng ở trạng thái 'Chờ duyệt'"
                            );
                            return;
                          }

                          setTypeStatus(1);
                          setIsModal(true);
                        }}
                      />
                      <IButton
                        title={"Tạm dừng"}
                        color={colors.oranges}
                        loading={loadingButton}
                        styleHeight={{
                          width: 120,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          if (selectedRowKeys.length === 0) {
                            message.error(
                              "Vui lòng chọn sản phẩm theo đối tượng"
                            );
                            return;
                          }

                          const arrCheck = selectedRowKeys.filter((item) => {
                            let itemParse = JSON.parse(item);
                            return (
                              itemParse.status === 2 || itemParse.status === 3
                            );
                          });

                          if (arrCheck.length > 0) {
                            message.error(
                              "Vui lòng chọn sản phẩm theo đối tượng ở trạng thái 'Đang áp dụng'"
                            );
                            return;
                          }

                          setTypeStatus(0);
                          setIsModal(true);
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listProduct}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    rowKey="rowTable"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    scroll={{ x: 1600 }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isModal}
        footer={null}
        width={320}
        onCancel={() => setIsModal(false)}
        centered={true}
        closable={false}
      >
        <div style={{ height: "100%" }}>
          <Row gutter={[0, 12]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 18 }}>
                  Thông báo
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  textAlign: "center",
                  fontWeight: 600,
                  color: colors.blackChart,
                }}
              >
                {`Các sản phẩm bạn vừa chọn sẽ ${
                  typeStatus === 0 ? "tạm dừng ẩn" : "bị ẩn"
                } với đối tượng đã được thiết
                lập.`}
              </div>
            </Col>
          </Row>
          <Row
            gutter={[17, 0]}
            style={{
              position: "absolute",
              bottom: -60,
              left: 0,
            }}
          >
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.oranges}
                  title="Hủy bỏ"
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    setTypeStatus(-1);
                    setIsModal(false);
                  }}
                  icon={ISvg.NAME.CROSS}
                />
              </div>
            </Col>
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.main}
                  title="Xác nhận"
                  icon={ISvg.NAME.CHECKMARK}
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    console.log(selectedRowKeys);
                    let arrParse = selectedRowKeys.map((item) => {
                      let itemNew = JSON.parse(item);
                      return itemNew;
                    });

                    let arrID = arrParse.map((item) => item.id);
                    if (typeStatus === 0) {
                      postStopProductByObject(arrID);
                    } else {
                      const obj = {
                        ids: arrID,
                      };
                      postApproveProductMultiPrice(obj);
                    }
                  }}
                />
              </div>
            </Col>
          </Row>
        </div>
      </Modal>
    </div>
  );
}

export default ListProductByObject;
