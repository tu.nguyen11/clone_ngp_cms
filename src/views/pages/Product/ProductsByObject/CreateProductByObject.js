import { Col, Row, Modal, Popconfirm, Tag, Empty, message } from "antd";
import React, { useState, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { colors } from "../../../../assets";
import { IButton, ISvg, ITableHtml } from "../../../components";
import {
  IDatePickerFrom,
  IError,
  TagP,
  useModalAgencyS1Filter,
  StyledISelect,
  StyledInputNumber,
} from "../../../components/common";
import {
  StyledITitle,
  StyledITileHeading,
} from "../../../components/common/Font/font";
import {
  CLEAR_REDUX_PRODUCTS_BY_OBJECT,
  CREATE_PRODUCT_BY_OBJECT_INFO,
  RESET_MODAL,
  CHANGE_PRICE_PRODUCT_BY_OBJECT,
  CHECK_VALIDATE_PRICE_BY_OBJECT,
  REMOVE_PRODUCT_BY_OBJECT_CONFIRM,
} from "../../../store/reducers";
import moment from "moment";
import { APIService } from "../../../../services";
import useModalTreeProductByObject from "../../../components/common/ModalAgency/useModalTreeProductByObject";

export default function CreateProductByObject(props) {
  const history = useHistory();
  const typingTimeoutRef = useRef();

  const format = "DD-MM-YYYY HH:mm";
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);

  const [listMembership, setListMembership] = useState([]);
  const [dropdownRegion, setDropdownRegion] = useState([]);
  const [dropdownCity, setDropdownCity] = useState([]);

  const [loadingCity, setLoadingCity] = useState(false);
  const [loadingButton, setLoadingButton] = useState(false);
  const [isVisibleAgency1, setVisibleAgency1] = useState(false);
  const [visibleTreeProduct, setVisibleTreeProduct] = useState(false);

  const [checkMemberShip, setCheckMemberShip] = useState(false);
  const [checkRegion, setCheckRegion] = useState(false);
  const [checkCity, setCheckCity] = useState(false);

  const { productsByObject, modalS1, treeProduct } = dataRoot;

  const disabledStartDateApply = (startValue) => {
    const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() - 1);

    return startValue.valueOf() < dateNow.getTime();
  };

  const _getAPIListMembership = async () => {
    try {
      const data = await APIService._getListMemberships();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });

      setListMembership(dataNew);
    } catch (error) {}
  };

  const _fetchAPIListRegion = async () => {
    try {
      const data = await APIService._getAllAgencyGetRegion();
      let dataNew = data.agency.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });

      setDropdownRegion(() => dataNew);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchListCityByRegion = async (arrayRegion) => {
    try {
      const data = await APIService._getListCityByRegion(arrayRegion);
      let dataNew = data.list_cities.map((item) => {
        const key = item.id;
        const value = item.name;
        const parent = item.parent;
        return { key, value, parent };
      });

      setDropdownCity(dataNew);
      setLoadingCity(false);
    } catch (error) {
      console.log(error);
      setLoadingCity(false);
    }
  };

  const callAPI = async () => {
    await _getAPIListMembership();
    await _fetchAPIListRegion();
    await _fetchListCityByRegion(productsByObject.arrayRegion);
  };

  useEffect(() => {
    callAPI();
  }, []);

  const renderDropdownMembership = () => {
    return (
      <Col span={24}>
        <Row>
          <div
            style={{
              minHeight: 420,
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Col span={24}>
              <Row gutter={[0, 25]}>
                <Col span={24}>
                  <div
                    style={{
                      marginTop: 10,
                    }}
                  >
                    <TagP
                      style={{
                        fontWeight: 500,
                      }}
                    >
                      <div>Điều kiện cấp bậc</div>
                    </TagP>
                  </div>
                </Col>
                <Col span={24}>
                  <Row gutter={[0, 12]}>
                    <div
                      style={{
                        padding: "4px 12px 12px 12px",
                        border: checkMemberShip
                          ? "1px solid red"
                          : "1px solid #d9d9d9",
                      }}
                    >
                      <Col>
                        <div
                          style={{
                            borderBottom: "1px solid #d9d9d9",
                            paddingBottom: 6,
                          }}
                        >
                          <Row>
                            <Col span={12}>
                              <span>Cấp bậc</span>
                            </Col>
                            <Col span={12}>
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "flex-end",
                                }}
                              >
                                <Popconfirm
                                  placement="bottom"
                                  title={
                                    <span>
                                      Xóa dữ liệu đang chọn <br />
                                      của Cấp bậc
                                    </span>
                                  }
                                  onConfirm={() => {
                                    const data = {
                                      key: "arrMembership",
                                      value: [],
                                    };

                                    dispatch(
                                      CREATE_PRODUCT_BY_OBJECT_INFO(data)
                                    );
                                  }}
                                  okText="Đồng ý"
                                  cancelText="Hủy"
                                >
                                  <Tag color={colors.main}>Xóa</Tag>
                                </Popconfirm>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                      <Col>
                        <StyledISelect
                          mode="multiple"
                          isBorderBottom="1px solid #d9d9d9"
                          value={
                            productsByObject.arrMembership.length === 0
                              ? undefined
                              : productsByObject.arrMembership
                          }
                          placeholder="Chọn cấp bậc"
                          onChange={(arrKey) => {
                            const data = {
                              key: "arrMembership",
                              value: [...arrKey],
                            };
                            dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(data));
                            setCheckMemberShip(false);
                          }}
                          data={listMembership}
                        />
                      </Col>
                    </div>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                  }}
                  onClick={() => {
                    const dataAction1 = {
                      key: "arrMembership",
                      value: [],
                    };

                    dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(dataAction1));

                    const dataAction2 = {
                      key: "type_way",
                      value: 0,
                    };

                    dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(dataAction2));
                  }}
                />
              </div>
            </Col>
          </div>
        </Row>
      </Col>
    );
  };

  const renderDropdownRegion = () => {
    return (
      <Col span={24}>
        <Row>
          <div
            style={{
              minHeight: 540,
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Col span={24}>
              <Row gutter={[0, 25]}>
                {/* DK tham gia 1 */}
                <Col span={24}>
                  <div
                    style={{
                      marginTop: 10,
                    }}
                  >
                    <TagP
                      style={{
                        fontWeight: 500,
                      }}
                    >
                      <div>Điều kiện tham gia 1</div>
                    </TagP>
                  </div>
                </Col>
                <Col span={24}>
                  <Row gutter={[16, 16]}>
                    <Col span={24}>
                      <Row gutter={[0, 12]}>
                        <div
                          style={{
                            padding: 12,
                            border: "1px solid #d9d9d9",
                          }}
                        >
                          <Col>
                            <div>
                              <TagP
                                style={{
                                  borderBottom: "1px solid #d9d9d9",
                                  paddingBottom: 16,
                                }}
                              >
                                Cấp người dùng
                              </TagP>
                            </div>
                          </Col>
                          <Col>
                            <StyledISelect
                              isBorderBottom="1px solid #d9d9d9"
                              value={productsByObject.id_agency}
                              disabled={true}
                              placeholder="Chọn đại lý"
                              onChange={(key) => {
                                const data = {
                                  key: "id_agency",
                                  value: key,
                                };

                                dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(data));
                              }}
                              data={[
                                {
                                  value: "Đại lý cấp 1",
                                  key: 1,
                                },
                              ]}
                            />
                          </Col>
                        </div>
                      </Row>
                    </Col>
                  </Row>
                </Col>

                {/* DK tham gia 2 */}
                <Col span={24}>
                  <div>
                    <TagP style={{ fontWeight: 500 }}>
                      Điều kiện tham gia 2
                    </TagP>
                  </div>
                </Col>
                <Col span={24}>
                  <Row gutter={[16, 16]}>
                    <Col span={24}>
                      <Row gutter={[0, 12]}>
                        <div
                          style={{
                            padding: "6px 12px 12px 12px",
                            border: checkRegion
                              ? "1px solid red"
                              : "1px solid #d9d9d9",
                          }}
                        >
                          <Col>
                            <div
                              style={{
                                borderBottom: "1px solid #d9d9d9",
                                paddingBottom: 6,
                              }}
                            >
                              <Row>
                                <Col span={12}>
                                  <span style={{}}>Vùng địa lý</span>
                                </Col>
                                <Col span={12}>
                                  <div
                                    style={{
                                      display: "flex",
                                      justifyContent: "flex-end",
                                    }}
                                  >
                                    <Popconfirm
                                      placement="bottom"
                                      title={
                                        <span>
                                          Xóa dữ liệu đang chọn <br />
                                          của Vùng địa lý
                                        </span>
                                      }
                                      onConfirm={() => {
                                        const data = {
                                          key: "arrayCity",
                                          value: [],
                                        };

                                        const dataCityClone = {
                                          key: "arrayCityClone",
                                          value: [],
                                        };
                                        dispatch(
                                          CREATE_PRODUCT_BY_OBJECT_INFO(data)
                                        );
                                        dispatch(
                                          CREATE_PRODUCT_BY_OBJECT_INFO(
                                            dataCityClone
                                          )
                                        );
                                        const dataArrRegion = {
                                          key: "arrayRegion",
                                          value: [],
                                        };
                                        dispatch(
                                          CREATE_PRODUCT_BY_OBJECT_INFO(
                                            dataArrRegion
                                          )
                                        );
                                        // _fetchListCityByRegion([]);
                                      }}
                                      okText="Đồng ý"
                                      cancelText="Hủy"
                                    >
                                      <Tag color={colors.main}>Xóa</Tag>
                                    </Popconfirm>
                                  </div>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col>
                            <StyledISelect
                              mode="multiple"
                              isBorderBottom="1px solid #d9d9d9"
                              placeholder="Chọn vùng địa lý"
                              data={dropdownRegion}
                              onDropdownVisibleChange={(open) => {
                                if (!open) {
                                  if (
                                    productsByObject.arrayRegion.length === 0
                                  ) {
                                    return;
                                  }
                                  setLoadingCity(true);
                                  _fetchListCityByRegion(
                                    productsByObject.arrayRegion
                                  );
                                  return;
                                }
                              }}
                              value={productsByObject.arrayRegion}
                              onChange={(arrKey) => {
                                const data = {
                                  key: "arrayRegion",
                                  value: [...arrKey],
                                };
                                let arrCity =
                                  productsByObject.arrayCityClone.filter(
                                    (item) => {
                                      return arrKey.find((item1) => {
                                        return item1 === item.parent;
                                      });
                                    }
                                  );

                                let arrCityShow = arrCity.map(
                                  (item) => item.key
                                );
                                const dataCityRemove = {
                                  key: "arrayCityClone",
                                  value: [...arrCity],
                                };
                                const dataCityShow = {
                                  key: "arrayCity",
                                  value: [...arrCityShow],
                                };
                                dispatch(
                                  CREATE_PRODUCT_BY_OBJECT_INFO(dataCityRemove)
                                );
                                dispatch(
                                  CREATE_PRODUCT_BY_OBJECT_INFO(dataCityShow)
                                );

                                dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(data));
                                setLoadingCity(true);
                                setCheckRegion(false);
                                _fetchListCityByRegion(arrKey);
                              }}
                            />
                          </Col>
                        </div>
                      </Row>
                    </Col>
                  </Row>
                </Col>
                {/* DK tham gia 3 */}
                <Col span={24}>
                  <div>
                    <TagP style={{ fontWeight: 500 }}>
                      Điều kiện tham gia 3
                    </TagP>
                  </div>
                </Col>
                <Col span={24}>
                  <Row gutter={[16, 16]}>
                    <Col span={24}>
                      <Row gutter={[0, 12]}>
                        <div
                          style={{
                            padding: 12,
                            border: checkCity
                              ? "1px solid red"
                              : "1px solid #d9d9d9",
                          }}
                        >
                          <Col>
                            <div
                              style={{
                                borderBottom: "1px solid #d9d9d9",
                                paddingBottom: 6,
                              }}
                            >
                              <Row>
                                <Col span={12}>
                                  <span>Tỉnh / Thành phố</span>
                                </Col>
                                <Col span={12}>
                                  <div
                                    style={{
                                      display: "flex",
                                      justifyContent: "flex-end",
                                    }}
                                  >
                                    <Popconfirm
                                      placement="bottom"
                                      title={
                                        <span>
                                          Xóa dữ liệu đang chọn <br />
                                          của Tỉnh / Thành phố
                                        </span>
                                      }
                                      onConfirm={() => {
                                        const data = {
                                          key: "arrayCity",
                                          value: [],
                                        };

                                        const dataCityClone = {
                                          key: "arrayCityClone",
                                          value: [],
                                        };
                                        dispatch(
                                          CREATE_PRODUCT_BY_OBJECT_INFO(data)
                                        );
                                        dispatch(
                                          CREATE_PRODUCT_BY_OBJECT_INFO(
                                            dataCityClone
                                          )
                                        );
                                      }}
                                      okText="Đồng ý"
                                      cancelText="Hủy"
                                    >
                                      <Tag color={colors.main}>Xóa</Tag>
                                    </Popconfirm>
                                  </div>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col>
                            <StyledISelect
                              mode="multiple"
                              isBorderBottom="1px solid #d9d9d9"
                              placeholder="Chọn tỉnh/ thành phố"
                              style={{ maxHeight: 100, overflow: "auto" }}
                              loading={loadingCity}
                              disabled={
                                productsByObject.arrayRegion.length === 0
                                  ? true
                                  : false
                              }
                              onChange={(arrKey) => {
                                let arrKeyID = arrKey.map((item) => {
                                  return dropdownCity.find((item1) => {
                                    return item1.key === item;
                                  });
                                });
                                const data = {
                                  key: "arrayCity",
                                  value: [...arrKey],
                                };

                                const dataCityClone = {
                                  key: "arrayCityClone",
                                  value: [...arrKeyID],
                                };
                                dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(data));
                                dispatch(
                                  CREATE_PRODUCT_BY_OBJECT_INFO(dataCityClone)
                                );
                                setCheckCity(false);
                              }}
                              onDropdownVisibleChange={(open) => {
                                if (open) {
                                  setLoadingCity(true);
                                  _fetchListCityByRegion(
                                    productsByObject.arrayRegion
                                  );
                                  return;
                                }
                              }}
                              value={productsByObject.arrayCity}
                              data={dropdownCity}
                            />
                          </Col>
                        </div>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                  }}
                  onClick={() => {
                    const dataAction1 = {
                      key: "arrayRegion",
                      value: [],
                    };
                    const dataAction2 = {
                      key: "arrayCity",
                      value: [],
                    };
                    const dataAction3 = {
                      key: "arrayCityClone",
                      value: [],
                    };

                    dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(dataAction1));
                    dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(dataAction2));
                    dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(dataAction3));

                    const dataAction4 = {
                      key: "type_way",
                      value: 0,
                    };

                    dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(dataAction4));
                  }}
                />
              </div>
            </Col>
          </div>
        </Row>
      </Col>
    );
  };

  const renderTableAgencyC1 = () => {
    return (
      <Col span={24}>
        <Row>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Col span={24}>
              <ITableHtml
                childrenBody={bodyTable(modalS1.listS1Show)}
                childrenHeader={headerTable(dataHeader)}
                style={{
                  border: "1px solid rgba(122, 123, 123, 0.5)",
                  borderTop: "none",
                  height: 340,
                }}
                isBorder={false}
              />
            </Col>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                    marginTop: 15,
                  }}
                  onClick={() => {
                    dispatch(
                      RESET_MODAL({
                        keyInitial_listShow: "listS1Show",
                        keyInitial_root: "modalS1",
                        keyInitial_listAddClone: "listS1AddClone",
                        keyInitial_listAdd: "listS1Add",
                        keyInitial_list: "listS1",
                      })
                    );

                    const dataAction2 = {
                      key: "type_way",
                      value: 0,
                    };

                    dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(dataAction2));
                  }}
                />
              </div>
            </Col>
          </div>
        </Row>
      </Col>
    );
  };

  let dataHeader = [
    {
      name: "Tên đại lý",
      align: "left",
    },
    {
      name: "Mã đại lý",
      align: "left",
    },
    {
      name: "Cấp bậc",
      align: "center",
    },
  ];

  const headerTable = (dataHeader) => {
    return (
      <tr className="scroll">
        {dataHeader.map((item, index) => (
          <th
            className="th-table"
            style={{
              textAlign: item.align,
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTable = (data) => {
    return data.length === 0 ? (
      <tr height="100px">
        <td colspan="3" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      data.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.shop_name ? "-" : item.shop_name}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.dms_code ? "-" : item.dms_code}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "center", fontWeight: "normal" }}
          >
            {!item.membership ? "-" : item.membership}
          </td>
        </tr>
      ))
    );
  };

  let dataHeaderProduct = [
    {
      name: "Tên phiên bản",
      align: "left",
    },
    {
      name: "Mã phiên bản",
      align: "left",
    },
    {
      name: "ĐVT",
      align: "center",
    },
    {
      name: "Giá theo đối tượng",
      align: "center",
    },
    {
      name: "",
      align: "center",
    },
  ];

  const headerTableProduct = (dataHeader) => {
    return (
      <tr className="scroll">
        {dataHeader.map((item, index) => (
          <th
            className="th-table"
            style={{
              textAlign: item.align,
            }}
          >
            {item.name}
          </th>
        ))}
      </tr>
    );
  };

  const bodyTableProduct = (data) => {
    return data.length === 0 ? (
      <tr height="100px">
        <td colspan="3" style={{ padding: 20 }}>
          <Empty description="Không có dữ liệu" />
        </td>
      </tr>
    ) : (
      data.map((item, index) => (
        <tr className="tr-table">
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal", maxWidth: 300 }}
          >
            {!item.name ? "-" : item.name}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "left", fontWeight: "normal" }}
          >
            {!item.code ? "-" : item.code}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "center", fontWeight: "normal" }}
          >
            {!item.list_attribute ? "-" : item.list_attribute[0].name}
          </td>
          <td
            className="td-table"
            style={{ textAlign: "center", fontWeight: "normal" }}
          >
            <StyledInputNumber
              className="disabled-up"
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              value={item.priceByObject}
              min={0}
              onChange={(value) => {
                if (typingTimeoutRef.current) {
                  clearTimeout(typingTimeoutRef.current);
                }

                typingTimeoutRef.current = setTimeout(() => {
                  const reg = /^-?\d*[.,]?\d*$/.test(value);
                  if (reg) {
                    const data = {
                      key: "arrProduct",
                      keyData: "priceByObject",
                      idxProduct: index,
                      valueData: value,
                    };
                    if (value) {
                      const data1 = {
                        key: "arrProduct",
                        keyData: "checkPriceByObject",
                        idxProduct: index,
                        check: false,
                      };
                      dispatch(CHECK_VALIDATE_PRICE_BY_OBJECT(data1));
                    }
                    dispatch(CHANGE_PRICE_PRODUCT_BY_OBJECT(data));
                  }
                }, 300);
              }}
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              style={{
                borderRadius: 0,
                width: "100%",
                border: item.checkPriceByObject ? "1px solid red" : "",
              }}
            />
          </td>
          <td
            className="td-table"
            style={{ textAlign: "center", fontWeight: "normal" }}
          >
            <div
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <div
                style={{
                  background: "rgba(227, 95, 75, 0.1)",
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  cursor: "pointer",
                }}
                onClick={() => {
                  const dataAction = {
                    keyRoot: "treeProduct",
                    key_listTreeClone: "listTreeClone",
                    key_listTreeShow: "listTreeShow",
                    key_arrConfirm: "arrProduct",
                    key_listConfirm: "listConfirm",
                    key_listKeyClone: "listKeyClone",
                    key_listKeyConfirm: "listKeyConfirm",
                    indexProduct: index,
                    product: item,
                    listTreeClone: treeProduct.listTreeClone,
                    listConfirm: treeProduct.listConfirm,
                    listTreeShow: treeProduct.listTreeShow,
                    arrConfirm: productsByObject.arrProduct,
                    listKeyConfirm: treeProduct.listKeyConfirm,
                    listKeyClone: treeProduct.listKeyClone,
                  };

                  dispatch(REMOVE_PRODUCT_BY_OBJECT_CONFIRM(dataAction));
                }}
                className="cursor"
              >
                <ISvg
                  name={ISvg.NAME.CROSS}
                  width={8}
                  height={8}
                  fill="#E35F4B"
                />
              </div>
            </div>
          </td>
        </tr>
      ))
    );
  };

  const postCreatePriceByObject = async (obj) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postCreatePriceByObject(obj);
      message.success("Thiết lập sản phẩm hiển thị theo đối tượng thành công!");
      setLoadingButton(false);
      history.push("/product/object/list");
    } catch (err) {
      console.log(err);
      setLoadingButton(false);
    }
  };

  const handleSubmit = () => {
    let dateNow = new Date();
    if (productsByObject.startDate === "" || productsByObject.startDate <= 0) {
      const content = (
        <span>
          Vui lòng chọn <span style={{ fontWeight: 600 }}>Ngày áp dụng</span>!
        </span>
      );
      return IError(content);
    }

    if (productsByObject.startDate <= dateNow.getTime()) {
      const content = (
        <span>
          Vui lòng chọn <span style={{ fontWeight: 600 }}>Ngày áp dụng</span>{" "}
          lớn hơn thời gian hiện tại
        </span>
      );
      return IError(content);
    }
    if (productsByObject.type_way === 0) {
      const content = (
        <span>
          Vui lòng chọn{" "}
          <span style={{ fontWeight: 600 }}>Đối tượng hiển thị</span>!
        </span>
      );
      return IError(content);
    } else {
      if (productsByObject.type_way === 1) {
        if (modalS1.listS1Show.length === 0) {
          const content = (
            <span>
              Vui lòng chọn{" "}
              <span style={{ fontWeight: 600 }}>Đối tượng hiển thị</span>!
            </span>
          );
          return IError(content);
        }
      } else if (productsByObject.type_way === 2) {
        if (productsByObject.arrMembership.length === 0) {
          setCheckMemberShip(true);
          const content = (
            <span>
              Vui lòng chọn{" "}
              <span style={{ fontWeight: 600 }}>Đối tượng hiển thị</span>!
            </span>
          );
          return IError(content);
        }
      } else {
        if (productsByObject.arrayRegion.length === 0) {
          setCheckRegion(true);
          const content = (
            <span>
              Vui lòng chọn{" "}
              <span style={{ fontWeight: 600 }}>Đối tượng hiển thị</span>!
            </span>
          );
          return IError(content);
        }

        if (productsByObject.arrayCity.length !== 0) {
          let arrRegionNotExistCity = [];
          productsByObject.arrayRegion.forEach((itemRegion) => {
            let arrCheckExist = productsByObject.arrayCityClone.filter(
              (itemCity) => {
                return itemRegion === itemCity.parent;
              }
            );

            if (arrCheckExist.length === 0) {
              arrRegionNotExistCity.push(itemRegion);
            }
          });

          if (arrRegionNotExistCity.length !== 0) {
            setCheckCity(true);
            const content = (
              <span>
                Vui lòng chọn thành phố ứng với vùng địa lý ở trên hoặc loại bỏ
                vùng địa lý đó!
              </span>
            );
            return IError(content);
          }
        }
      }
    }

    if (productsByObject.arrProduct.length === 0) {
      const content = (
        <span>
          Vui lòng chọn <span style={{ fontWeight: 600 }}>sản phẩm</span>!
        </span>
      );
      return IError(content);
    } else {
      let check = false;

      productsByObject.arrProduct.forEach((item, index) => {
        if (
          item.priceByObject < 0 ||
          item.priceByObject === null ||
          item.priceByObject === undefined ||
          item.priceByObject === ""
        ) {
          check = true;
          const data1 = {
            key: "arrProduct",
            keyData: "checkPriceByObject",
            idxProduct: index,
            check: true,
          };
          dispatch(CHECK_VALIDATE_PRICE_BY_OBJECT(data1));
        }
      });

      if (check) {
        const content = (
          <span>
            Vui lòng nhập{" "}
            <span style={{ fontWeight: 600 }}>giá theo đối tượng</span>!
          </span>
        );
        return IError(content);
      }
    }

    const arrProduct = productsByObject.arrProduct.map((item) => {
      let id = item.id;
      let price = item.priceByObject;
      return { id, price };
    });

    let listS1 = [];
    if (modalS1.listS1Show.length !== 0) {
      listS1 = modalS1.listS1Show.map((item) => item.id);
    }

    const dataAdd = {
      effect_date: productsByObject.startDate,
      listCities: productsByObject.arrayCity,
      listMembership: productsByObject.arrMembership,
      listProduct: arrProduct,
      listRegion: productsByObject.arrayRegion,
      listUser: listS1,
      priceType: productsByObject.type_way,
      userType: 1,
    };

    postCreatePriceByObject(dataAdd);
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[0, 24]}>
        <Col span={24}>
          <div>
            <StyledITitle style={{ color: colors.main }}>
              Thiết lập sản phẩm theo đối tượng
            </StyledITitle>
          </div>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  title="Lưu"
                  color={colors.main}
                  icon={ISvg.NAME.SAVE}
                  loading={loadingButton}
                  styleHeight={{
                    width: 140,
                  }}
                  onClick={handleSubmit}
                />
                <IButton
                  title="Hủy"
                  color={colors.oranges}
                  icon={ISvg.NAME.CROSS}
                  styleHeight={{
                    width: 140,
                    marginLeft: 15,
                  }}
                  onClick={() => {
                    dispatch(CLEAR_REDUX_PRODUCTS_BY_OBJECT());
                    history.push("/product/object/list");
                  }}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <div style={{ width: 1200 }}>
            <Row gutter={[24, 24]} type="flex">
              <Col span={9}>
                <div
                  style={{
                    padding: 24,
                    height: productsByObject.type_way === 0 ? 650 : "100%",
                  }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 30]}>
                    <Col span={24}>
                      <Row gutter={[16, 0]}>
                        <Col span={12}>
                          <Row gutter={[0, 15]}>
                            <Col span={24}>
                              <TagP style={{ fontWeight: 500 }}>
                                Ngày áp dụng
                              </TagP>
                            </Col>
                            <Col span={24}>
                              <IDatePickerFrom
                                showTime={false}
                                isBorderBottom={true}
                                disabledDate={disabledStartDateApply}
                                paddingInput="0px"
                                placeholder="nhập thời gian bắt đầu"
                                format={format}
                                value={
                                  productsByObject.startDate === "" ||
                                  productsByObject.startDate === 0
                                    ? undefined
                                    : moment(
                                        new Date(productsByObject.startDate),
                                        format
                                      )
                                }
                                onChange={(date) => {
                                  let dateNow = new Date();

                                  if (
                                    date._d.getHours() <= dateNow.getHours()
                                  ) {
                                    let dateNew = date._d.setHours(
                                      date._d.getHours() + 1
                                    );

                                    let dateNew2 = new Date(dateNew).setMinutes(
                                      dateNow.getMinutes()
                                    );
                                    const data = {
                                      key: "startDate",
                                      value: dateNew2,
                                    };
                                    dispatch(
                                      CREATE_PRODUCT_BY_OBJECT_INFO(data)
                                    );
                                  } else {
                                    let dateNew = date._d.setMinutes(
                                      dateNow.getMinutes()
                                    );
                                    const data = {
                                      key: "startDate",
                                      value: dateNew,
                                    };
                                    dispatch(
                                      CREATE_PRODUCT_BY_OBJECT_INFO(data)
                                    );
                                  }
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>

                    <Col span={24}>
                      <Row gutter={[0, 20]}>
                        {productsByObject.type_way !== 1 ? (
                          <Col span={24}>
                            <StyledITileHeading minFont="10px" maxFont="16px">
                              Đối tượng hiển thị
                            </StyledITileHeading>
                          </Col>
                        ) : (
                          <Col span={24}>
                            <Row>
                              <Col span={12}>
                                <div
                                  style={{
                                    padding: "11px 0px",
                                  }}
                                >
                                  <StyledITileHeading
                                    minFont="10px"
                                    maxFont="16px"
                                  >
                                    Đối tượng hiển thị
                                  </StyledITileHeading>
                                </div>
                              </Col>
                              <Col span={12}>
                                <div className="flex justify-end">
                                  <IButton
                                    title="Chỉnh sửa ĐLC1"
                                    color={colors.main}
                                    styleHeight={{
                                      borderRadius: 18,
                                      fontWeight: "bold",
                                      width: 150,
                                      background: "white",
                                    }}
                                    onClick={() => {
                                      setVisibleAgency1(true);
                                    }}
                                  />
                                </div>
                              </Col>
                            </Row>
                          </Col>
                        )}

                        {productsByObject.type_way === 0 ? (
                          <Col span={24}>
                            <Row gutter={[0, 15]}>
                              <Col span={24}>
                                <IButton
                                  title="THEO ĐẠI LÝ"
                                  color={
                                    productsByObject.type_way === 1
                                      ? "white"
                                      : colors.main
                                  }
                                  styleHeight={{
                                    borderRadius: 18,
                                    fontWeight: "bold",
                                    background:
                                      productsByObject.type_way === 1
                                        ? colors.main
                                        : "white",
                                  }}
                                  onClick={() => {
                                    setVisibleAgency1(true);
                                  }}
                                />
                              </Col>
                              <Col span={24}>
                                <IButton
                                  title="THEO CẤP BẬC"
                                  color={
                                    productsByObject.type_way === 2
                                      ? "white"
                                      : colors.main
                                  }
                                  styleHeight={{
                                    borderRadius: 18,
                                    fontWeight: "bold",
                                    background:
                                      productsByObject.type_way === 2
                                        ? colors.main
                                        : "white",
                                  }}
                                  onClick={() => {
                                    const data = {
                                      key: "type_way",
                                      value: 2,
                                    };
                                    dispatch(
                                      CREATE_PRODUCT_BY_OBJECT_INFO(data)
                                    );
                                  }}
                                />
                              </Col>
                              <Col span={24}>
                                <IButton
                                  title="THEO KHU VỰC QUẢN LÝ"
                                  color={
                                    productsByObject.type_way === 3
                                      ? "white"
                                      : colors.main
                                  }
                                  styleHeight={{
                                    borderRadius: 18,
                                    fontWeight: "bold",
                                    background:
                                      productsByObject.type_way === 3
                                        ? colors.main
                                        : "white",
                                  }}
                                  onClick={() => {
                                    const data = {
                                      key: "type_way",
                                      value: 3,
                                    };
                                    dispatch(
                                      CREATE_PRODUCT_BY_OBJECT_INFO(data)
                                    );
                                  }}
                                />
                              </Col>
                            </Row>
                          </Col>
                        ) : productsByObject.type_way === 1 ? (
                          renderTableAgencyC1()
                        ) : productsByObject.type_way === 2 ? (
                          renderDropdownMembership()
                        ) : (
                          renderDropdownRegion()
                        )}
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={15}>
                <div
                  style={{ padding: 24, height: "100%" }}
                  className="box-shadow"
                >
                  <Row gutter={[0, 12]}>
                    <Col span={24}>
                      <StyledITileHeading minFont="10px" maxFont="16px">
                        Danh sách sản phẩm
                      </StyledITileHeading>
                    </Col>
                    {productsByObject.arrProduct.length === 0 ? (
                      <Col span={24}>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                            className="cursor"
                            onClick={() => {
                              setVisibleTreeProduct(true);
                            }}
                          >
                            <span
                              style={{
                                marginRight: 12,
                                color: colors.main,
                                fontWeight: 500,
                              }}
                            >
                              Thêm sản phẩm
                            </span>
                            <div>
                              <ISvg
                                name={ISvg.NAME.ADDUPLOAD}
                                width={20}
                                height={20}
                                fill={colors.main}
                              />
                            </div>
                          </div>
                        </div>
                      </Col>
                    ) : (
                      <Col span={24}>
                        <div style={{ marginTop: 12, height: "100%" }}>
                          <ITableHtml
                            style={{
                              height: "100%",
                            }}
                            childrenBody={bodyTableProduct(
                              productsByObject.arrProduct
                            )}
                            childrenHeader={headerTableProduct(
                              dataHeaderProduct
                            )}
                          />
                          {productsByObject.arrProduct.length === 0 ? null : (
                            <table>
                              <tr
                                className="tr-table"
                                style={{
                                  borderLeft:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                  borderRight:
                                    "1px solid rgba(122, 123, 123, 0.5)",
                                }}
                              >
                                <td
                                  className="td-table"
                                  style={{ opacity: 1 }}
                                  colSpan="5"
                                >
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                      marginBottom: 4,
                                      paddingLeft: 10,
                                    }}
                                  >
                                    <div
                                      style={{
                                        cursor: "pointer",
                                        display: "flex",
                                      }}
                                      onClick={() =>
                                        setVisibleTreeProduct(true)
                                      }
                                    >
                                      <span
                                        style={{
                                          marginRight: 12,
                                          color: colors.main,
                                          fontWeight: 500,
                                        }}
                                      >
                                        Thêm sản phẩm
                                      </span>
                                      <div>
                                        <ISvg
                                          name={ISvg.NAME.ADDUPLOAD}
                                          width={20}
                                          height={20}
                                          fill={colors.main}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          )}
                        </div>
                      </Col>
                    )}
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isVisibleAgency1}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalAgencyS1Filter(
          () => {
            const data = {
              key: "type_way",
              value: 1,
            };
            dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(data));
            setVisibleAgency1(false);
          },
          () => {
            const data = {
              key: "type_way",
              value: 0,
            };
            dispatch(CREATE_PRODUCT_BY_OBJECT_INFO(data));
            setVisibleAgency1(false);
          }
        )}
      </Modal>
      <Modal
        visible={visibleTreeProduct}
        footer={null}
        width={1200}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        {useModalTreeProductByObject(() => {
          setVisibleTreeProduct(false);
        })}
      </Modal>
    </div>
  );
}
