import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message, Modal } from "antd";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IButton,
  IImage,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import FormatterDay from "../../../../utils/FormatterDay";
import ModalEditHotProduct from "./ModalEditHotProduct";

const formatStringDate = "#hhh#:#mm#:#ss# #DD#/#MM#/#YYYY#";
function ListHotProductPage(props) {
  const [filter, setFilter] = useState({
    key: "",
    page: 1,
    type: 0,
  });

  const [dataTable, setDataTable] = useState({
    listHotProduct: [],
    total: 1,
    size: 10,
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loadingButton, setLoadingButton] = useState(false);
  const [loadingTable, setLoadingTable] = useState(true);
  const [visibleModalPrioritize, setVisibleModalPrioritize] = useState(false);

  const _fetchAPIListHotProducts = async (filter) => {
    try {
      const data = await APIService._getListHotProductsNew(filter);
      data.product.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        return item;
      });

      setDataTable({
        listHotProduct: data.product,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  useEffect(() => {
    _fetchAPIListHotProducts(filter);
  }, [filter]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const _APIPostRemoveHotProduct = async (objRemove) => {
    try {
      setLoadingButton(true);
      const data = await APIService._postRemoveHotProductNew(objRemove);
      setSelectedRowKeys([]);
      setLoadingButton(false);
      await _fetchAPIListHotProducts(filter);
      message.success("Xóa thành công");
    } catch (err) {
      console.log(err);
      setLoadingButton(false);
    }
  };

  const typeShow = [
    {
      key: 0,
      value: "Tất cả",
    },
    {
      key: 1,
      value: "Giảm giá",
    },
    {
      key: 2,
      value: "Bán chạy",
    },
  ];

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tên phiên bản",
      dataIndex: "name",
      key: "name",
      render: (name) => (
        <Tooltip title={name}>
          <span>{name}</span>
        </Tooltip>
      ),
    },
    {
      title: "Loại hiển thị",
      dataIndex: "type_show",
      key: "type_show",
      align: "center",
      render: (type_show) => <span>{type_show}</span>,
    },
    {
      title: "Hình ảnh",
      dataIndex: "thumb",
      align: "center",
      key: "thumb",
      render: (thumb) => {
        return <IImage src={thumb} style={{ width: 40, height: 40 }} />;
      },
    },
    {
      title: "Thứ tự",
      dataIndex: "number",
      key: "number",
      align: "center",
      render: (number) => <span>{number}</span>,
    },
    {
      title: "Ngày thêm",
      dataIndex: "create_date",
      key: "create_date",
      render: (create_date) => (
        <span>
          {FormatterDay.dateFormatWithString(create_date, formatStringDate)}
        </span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Danh sách sản phẩm hot
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên, mã sản phẩm hot">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên, mã sản phẩm hot"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={12}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <ITitle
                        title="Loại hiển thị"
                        level={4}
                        style={{
                          marginLeft: 20,
                        }}
                      />
                      <div style={{ margin: "0px 15px 0px 15px" }}>
                        <div style={{ width: 200 }}>
                          <ISelect
                            defaultValue="Tất cả"
                            data={typeShow}
                            select={true}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setFilter({
                                ...filter,
                                type: value,
                                page: 1,
                              });
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col span={12}>
                    <div className="flex justify-end">
                      <IButton
                        title={"Thiết lập hiển thị"}
                        color={colors.main}
                        styleHeight={{
                          width: 160,
                          marginLeft: 25,
                        }}
                        onClick={() => {
                          setVisibleModalPrioritize(true);
                        }}
                      />
                      <IButton
                        icon={ISvg.NAME.DELETE}
                        title={"Xóa"}
                        loading={loadingButton}
                        color={colors.red}
                        styleHeight={{
                          width: 120,
                          marginLeft: 25,
                        }}
                        onClick={() => {
                          let arrayID = [];

                          selectedRowKeys.forEach((item, index) => {
                            arrayID.push(dataTable.listHotProduct[item].id);
                          });
                          if (arrayID.length === 0) {
                            message.warning(
                              "Bạn chưa chọn mã sản phẩm để xóa."
                            );
                            return;
                          }

                          const objRemove = {
                            ids: arrayID,
                          };
                          _APIPostRemoveHotProduct(objRemove);
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listHotProduct}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={visibleModalPrioritize}
        footer={null}
        width={1400}
        centered={true}
        closable={false}
        maskClosable={false}
      >
        <ModalEditHotProduct
          openModalProps={visibleModalPrioritize}
          handleCancel={() => {
            setVisibleModalPrioritize(false);
          }}
          callback1={() => {
            _fetchAPIListHotProducts(filter);
          }}
        />
      </Modal>
    </div>
  );
}

export default ListHotProductPage;
