import { Table, Typography, Avatar, Button, message } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../../utils";
import { APIService } from "../../../../services";
const widthScreen = window.innerWidth;

const { Paragraph, Title } = Typography;
function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 13;
  } else widthColumn = (widthScreen - 300) / 13;
  return widthColumn * type;
}

export default class ProductTypePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      width: 0,
      loading: false,
      loadingTable: true,
      bool: false,
      status: 1,
      keySearch: "",
      page: 1,
      arrayRemove: [],
      data: [],
      url: "",
      value: "", // Check here to configure the default column
    };
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.onChangeButton = this.onChangeButton.bind(this);
    this.handleResize = this.handleResize.bind(this);
  }
  handleResize() {
    console.log(window.innerWidth);

    this.setState({
      width: window.innerWidth,
    });
  }
  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  componentDidMount() {
    this._getListTypeProduct(
      this.state.status,
      this.state.page,
      this.state.keySearch
    );
    // if (this.props.location.state.callback == "undefined") {
    //
    // }
    window.addEventListener("resize", this.handleResize);
  }

  _getListTypeProduct = async (status, page, keySearch) => {
    try {
      const data = await APIService._getListTypeProduct(
        status,
        page,
        keySearch
      );

      data.product_brand_category.map((item, index) => {
        data.product_brand_category[index].stt =
          (this.state.page - 1) * 10 + index + 1;
      });
      this.setState({
        data: data,
        loadingTable: false,
        url: data.image_url,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  _APIpostRemoveTypeProduct = async (obj) => {
    try {
      const data = APIService._postRemoveTypeProduct(obj);

      this.setState(
        {
          selectedRowKeys: [],
          loadingTable: true,
        },
        () => {
          this._getListTypeProduct(
            this.state.status,
            this.state.page,
            this.state.keySearch
          );
          message.success("Xóa thành công");
        }
      );
    } catch (err) {
      console.log(err);
    }
  };

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];
    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.data.product_brand_category[item].id);
    });

    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys, arrayRemove: arrayID });
  };

  onChangeSearch = ({ target: { value } }) => {
    //
    // this.setState({ value });
  };
  onChangeButton = () => {
    this.setState({
      loading: true,
    });

    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 3000);
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        // fixed: "left",
        width: 60,
        render: (id) => _renderColumns(id),
      },
      // {
      //   title: _renderTitle("Mã phân loại"),
      //   dataIndex: "code",
      //   key: "code",
      //   align: "left",
      //   // fixed: "left",

      //   render: (code) => <ITitle level={4} title={code} />,
      // },
      {
        title: _renderTitle("Tên thương hiệu"),
        dataIndex: "name",
        key: "name",
        align: "left",
        render: (name) => _renderColumns(name),
      },
      {
        title: _renderTitle("Ngành hàng"),
        dataIndex: "productTypeName",
        key: "productTypeName",
        align: "left",
        render: (productTypeName) => _renderColumns(productTypeName),
      },
      {
        title: _renderTitle("Nhóm hàng"),
        dataIndex: "productCate",
        key: "productCate",
        align: "left",
        width: 250,
        render: (productCate) => (
          <span>{!productCate ? "-" : productCate}</span>
        ),
      },
      {
        title: _renderTitle("Hình ảnh"),
        dataIndex: "images",
        key: "images",
        align: "center",
        render: (images) => {
          return (
            <IImage
              src={this.state.url + images[0]}
              style={{ width: 40, height: 40 }}
            />
          );
        },
      },
      // {
      //   title: _renderTitle("Trạng Thái"),
      //   dataIndex: "statusName",
      //   key: "statusName",
      //   align: "center",
      //   render: (statusName) => _renderColumns(statusName),
      // },
      {
        title: "",
        dataIndex: "id",
        key: "id",
        align: "right",
        width: 100,
        fixed: widthScreen <= 1200 ? "right" : "",

        render: (id) => (
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div
              onClick={(e) => {
                e.stopPropagation();

                this.props.history.push("/productEditType/" + id);
              }} // idAndStt.index vị trí của mảng
              className="cell"
              style={{ width: 50 }}
            >
              <ISvg
                name={ISvg.NAME.WRITE}
                width={20}
                height={20}
                fill={colors.icon.default}
              />
            </div>
            <div
              className="cursor"
              style={{ width: 50 }}
              onClick={(e) => {
                e.stopPropagation();
                this.props.history.push("/detailTypeProduct/" + id);
              }}
            >
              <ISvg
                name={ISvg.NAME.CHEVRONRIGHT}
                width={11}
                height={20}
                fill={colors.icon.default}
              />
            </div>
          </div>
        ),
      },
    ];

    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Row>
          <ITitle
            level={1}
            title="Danh mục sản phẩm"
            style={{
              color: colors.main,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <ISearch
            onChange={this.onChangeSearch}
            onChange={(e) => {
              this.state.keySearch = e.target.value;
              this.setState({
                keySearch: this.state.keySearch,
              });
            }}
            onPressEnter={() =>
              this.setState(
                {
                  page: 1,
                },
                () =>
                  this._getListTypeProduct(
                    this.state.status,
                    this.state.page,
                    this.state.keySearch
                  )
              )
            }
            icon={
              <div
                style={{
                  display: "flex",
                  width: 42,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                className="cursor"
                onClick={() =>
                  this._getListTypeProduct(
                    this.state.status,
                    this.state.page,
                    this.state.keySearch
                  )
                }
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>
        <Row className=" mt-4">
          <Col style={{ width: "100%" }}></Col>
          <Col lg="auto" style={{}}>
            <Row>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-around",
                  flexDirection: "row",
                }}
              >
                <IButton
                  icon={ISvg.NAME.ARROWUP}
                  title="Tạo mới"
                  color={colors.main}
                  // style={{ marginRight: 20 }}
                  onClick={() => this.props.history.push("/productCreateType")}
                />
                {/* <IButton
                  icon={ISvg.NAME.DOWLOAND}
                  title='Xuất file'
                  color={colors.main}
                  style={{ marginRight: 20 }}
                  onClick={() => {
                    message.warning('Chức năng đang cập nhật')
                  }}
                /> */}

                {/* <IButton
                  icon={ISvg.NAME.DELETE}
                  title="Xóa"
                  color={colors.oranges}
                  style={{}}
                  onClick={() => {
                    if (this.state.arrayRemove.length == 0) {
                      message.warning("Bạn chưa chọn mã phân loại để xóa.");
                      return;
                    }
                    const objRemove = {
                      id: this.state.arrayRemove,
                      status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                    };
                    this._APIpostRemoveTypeProduct(objRemove);
                  }}
                /> */}
              </div>
            </Row>
          </Col>
        </Row>

        <Row className="mt-4">
          <ITable
            data={this.state.data.product_brand_category}
            columns={columns}
            loading={this.state.loadingTable}
            maxpage={this.state.data.total}
            sizeItem={this.state.data.size}
            style={{ width: "100%" }}
            scroll={{ x: 0 }}
            indexPage={this.state.page}
            // rowSelection={rowSelection}
            defaultCurrent={this.state.page}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );
                  const id = this.state.data.product_brand_category[indexRow]
                    .id;
                  this.props.history.push("/detailTypeProduct/" + id);
                }, // click row
              };
            }}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                  loadingTable: true,
                },
                () =>
                  this._getListTypeProduct(
                    this.state.status,
                    this.state.page,
                    this.state.keySearch
                  )
              );
            }}
          />
        </Row>
      </Container>
    );
  }
}
