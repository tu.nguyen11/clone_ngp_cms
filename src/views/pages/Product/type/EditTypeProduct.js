import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  Form,
  message,
} from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  ISelectMultiple,
  IUpload,
  ICascader,
  ISelectStatus,
} from "../../../components";
import { colors } from "../../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../../services";
import { ImageType } from "../../../../constant";
const { Paragraph, Title } = Typography;

class EditTypeProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nameTypeProduct: "",
      codeTypeProduct: "",
      idCategory: 0,
      url: "",
      idChildrenCategory: 0,
      status: 0,
      imageEdit: [],
      statusName: "",

      value: "", // Check here to configure the default column,
      objAdd: {
        brandName: "",
        codeProductName: "",
        brandProductID: 0,
        brandChildrenId: 0,
        status: 0,
      }, // dữ liệu thêm,
      errStatus: true,
      errBrandId: true,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeBarndCode = this.onChangeBarndCode.bind(this);
    this.onChangeBarndName = this.onChangeBarndName.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
    this.onChangeStatus = this.onChangeStatus.bind(this);
    this.productBrand_id = Number(this.props.match.params.id);
  }

  componentDidMount() {
    this._APIgetDetailTypeProduct(this.productBrand_id);
  }

  _APIpostEditTypeProduct = async (object) => {
    try {
      const data = APIService._postEditTypeProduct(object);

      message.success("Cập nhập thành công");
      this.props.history.push("/detailTypeProduct/" + this.productBrand_id);
    } catch (err) {
      console.log(err);
    }
  };

  _APIgetDetailTypeProduct = async (id) => {
    try {
      const data = await APIService._getDetailTypeProduct(id);
      const product_brand_category = data.product_brand_category;

      this.setState({
        url: data.image_url,
        data: product_brand_category,
        nameTypeProduct: product_brand_category.name,
        codeTypeProduct: product_brand_category.code,
        idCategory: product_brand_category.productTypeId,
        status: product_brand_category.status,
        errBrandId: false,
        errStatus: false,
        imageEdit: product_brand_category.images,
      });
    } catch (err) {
      console.log(err);
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      fieldsValue.brandName = this.state.nameTypeProduct;
      fieldsValue.codeProductName = this.state.codeTypeProduct;
      fieldsValue.status = this.state.status;
      fieldsValue.brandProductID = this.state.idCategory;
      fieldsValue.image = this.state.imageEdit;
      this.props.form.setFieldsValue(
        {
          fieldsValue: fieldsValue,
        },
        () => {
          const Object = {
            id: this.productBrand_id,
            name: fieldsValue.brandName,
            code: fieldsValue.codeProductName,
            parent_id: fieldsValue.brandProductID,
            status: fieldsValue.status,
            image: fieldsValue.image,
          };
          this._APIpostEditTypeProduct(Object);
        }
      );

      console.log(fieldsValue);
    });
  };

  _postAPIAddTypeProduct = async (obj) => {
    try {
      const data = await APIService._postAddTypeProduct(obj);
    } catch (err) {
      console.log(err);
    }
  };

  onChangeBarndName = (event) => {
    const { value } = event.target;
    this.state.objAdd.brandName = value;
    this.setState({
      objAdd: this.state.objAdd,
    });
  };

  onChangeBarndCode = (event) => {
    const { value } = event.target;
    this.state.objAdd.codeProductName = value;
    this.setState({
      objAdd: this.state.objAdd,
    });
  };

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
      this.state.objAdd.brandProductID = value[0];
      this.setState(
        {
          objAdd: this.state.objAdd,
          errBrandId: false,
        },
        () =>
          this.props.form.validateFields(["brandProductID"], { force: true })
      );
    }
    if (value.length == 2) {
      this.state.objAdd.brandChildrenId = value[2];
      this.setState({
        objAdd: this.state.objAdd,
      });
    }
  };

  onChangeStatus = (value) => {
    this.state.status = value;
    this.setState({ status: this.state.status, errStatus: false }, () =>
      this.props.form.validateFields(["status"], { force: true })
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Container fluid>
        <Row xs="auto" className="mt-3">
          <ITitle
            level={1}
            title="Chỉnh sửa danh mục sản phẩm"
            style={{ color: colors.main, fontWeight: "bold" }}
          />
        </Row>
        <Form onSubmit={this.handleSubmit}>
          <Row
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingTop: 32,
              paddingBottom: 32,
            }}
            xs="auto"
            sm={12}
            md={12}
          >
            <IButton
              icon={ISvg.NAME.SAVE}
              title="Lưu"
              htmlType="submit"
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => this.handleSubmit}
            />
            <IButton
              icon={ISvg.NAME.CROSS}
              title="Hủy"
              color={colors.oranges}
              style={{}}
              onClick={() => {
                this.props.history.push("/typeProductPage");
              }}
            />
          </Row>
          <div style={{ maxWidth: 815, minWidth: 615, flex: 1 }}>
            <Row className="p-0">
              <Col style={{ background: colors.white }} className="pl-5 m-0">
                <Row className="p-0 m-0" style={{}}>
                  <Col
                    className="pl-0 pr-0 pt-0 m-0"
                    style={{ paddingBottom: 40 }}
                  >
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Thông tin danh mục sản phẩm"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>

                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("brandName", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                message: "nhập mã đại lý",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Tên nhóm hàng"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  placeholder="Nhập tên nhãn hàng"
                                  value={this.state.nameTypeProduct}
                                  onChange={(e) => {
                                    this.setState({
                                      nameTypeProduct: e.target.value,
                                    });
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {
                            getFieldDecorator("codeProductName", {
                              rules: [
                                {
                                  // required: this.state.checkNick,
                                  message: "nhập mã",
                                },
                              ],
                            })
                            // (
                            //   <div>
                            //     <Row className="p-0 m-0 mb-2">
                            //       <ITitle
                            //         level={4}
                            //         title={"Mã phân loại"}
                            //         style={{ fontWeight: 600 }}
                            //       />
                            //     </Row>
                            //     <Row className="p-0 m-0">
                            //       <IInput
                            //         placeholder="Nhập mã"
                            //         value={this.state.codeTypeProduct}
                            //         onChange={e => {
                            //           this.setState({
                            //             codeTypeProduct: e.target.value
                            //           });
                            //         }}
                            //       />
                            //     </Row>
                            //   </div>
                            // )
                          }
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("brandProductID", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: this.state.errBrandId,
                                message: "nhập tên nhóm",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Tên ngành hàng"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ICascader
                                  style={{
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                    padding: 0,
                                  }}
                                  type="category"
                                  level={1}
                                  value={[this.state.idCategory]}
                                  placeholder="Nhập tên nhóm"
                                  onChange={this.onChangeCategory}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("status", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: this.state.errStatus,
                                message: "chọn trạng thái",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Trạng thái"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectStatus
                                  style={{
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  onChange={this.onChangeStatus}
                                  placeholder="Chọn trạng thái"
                                  // value={{ key: 1 }}
                                  value={this.state.status}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  </Col>
                  <Col className="m-0 p-0 ml-5" xs="auto">
                    <div
                      style={{
                        width: 3,
                        height: "100%",
                        background: colors.background,
                        // marginLeft: 10,
                      }}
                    ></div>
                  </Col>
                  <Col
                    className="pl-5 pr-0 pt-0 m-0"
                    // style={{paddingBottom: 40}}
                    xs={5}
                  >
                    <Row
                      className="p-0 mt-4 mb-4 ml-0 mr-0"
                      style={{
                        display: "flex",
                        justifyContent: "center",
                      }}
                    >
                      <ITitle
                        level={2}
                        title="Hình ảnh danh mục sản phẩm"
                        style={{
                          color: colors.black,
                          fontWeight: "bold",
                        }}
                      />
                      <Form.Item>
                        {getFieldDecorator("image", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              message: "chọn hình",
                              valuePropName: "fileList",
                              getValueFromEvent: this.normFile,
                            },
                          ],
                        })(
                          <Row
                            className="p-0 mt-4 mb-4 ml-0 mr-0"
                            style={{
                              display: "flex",
                              justifyContent: "center",
                            }}
                          >
                            <IUpload
                              // url={this.state.data.image_url}\
                              url={this.state.url}
                              type={ImageType.CATE}
                              name={this.state.imageEdit[0]}
                              callback={(file) => {
                                this.state.imageEdit = file;
                                this.setState({});
                              }}
                            />
                          </Row>
                        )}
                      </Form.Item>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Form>
      </Container>
    );
  }
}

const editTypeProduct = Form.create({ name: "EditTypeProduct" })(
  EditTypeProduct
);
export default editTypeProduct;
