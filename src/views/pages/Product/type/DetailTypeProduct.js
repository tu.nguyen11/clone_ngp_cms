import { Typography, message } from "antd";
import React, { Component } from "react";
import { ISvg, IButton, ITitle, IImage } from "../../../components";
import { priceFormat } from "../../../../utils";

import InfiniteScroll from "react-infinite-scroller";
import { colors } from "../../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../../services";
const { Paragraph, Title } = Typography;

export default class DetailTypeProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      loading: false,
      modal2Visible: false,
      write: false,
      valueStoreProduct: 0,
      nameStoreProduct: "",
      objAddStore: {},
      idImg: 0,
      data: {
        list_sub_product_brand: [],
        product_brand: {},
        images: [],
      },
      visible: false,
      dataLuuKho: [
        { kho: "Kho A Quận 11", soluong: 1000 },
        { kho: "Kho T Bình Qưới", soluong: 470 },
      ],
      url: "",
      value: "", // Check here to configure the default column
    };
    this.productBrand_id = Number(this.props.match.params.id);
  }

  onSelectChange = (selectedRowKeys) => {
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  _APIpostRemoveTypeProduct = async (obj) => {
    try {
      const data = APIService._postRemoveTypeProduct(obj);
      message.success("Xóa thành công");
      this.props.history.push("/typeProductPage");
    } catch (err) {
      console.log(err);
    }
  };

  componentDidMount() {
    this._APIgetDetailTypeProduct(this.productBrand_id);
  }

  /* API (Start) */

  _APIgetDetailTypeProduct = async (id) => {
    try {
      const data = await APIService._getDetailTypeProduct(id);
      this.setState({
        data: data.product_brand_category,
        url: data.image_url,
      });
    } catch (err) {
      console.log(err);
    }
  };
  /* API (End) */

  render() {
    const { data } = this.state;

    return (
      <Container fluid>
        <div>
          <Row className="mt-3 p-0">
            <ITitle
              level={1}
              title="Chi tiết danh mục sản phẩm"
              style={{ color: colors.main, fontWeight: "bold" }}
            />
          </Row>
          <Row
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingTop: 32,
              paddingBottom: 32,
            }}
            xs="auto"
            sm={12}
            md={12}
          >
            <IButton
              icon={ISvg.NAME.ARROWUP}
              title="Tạo mới"
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => this.props.history.push("/productCreateType")}
            />
            <IButton
              icon={ISvg.NAME.WRITE}
              title="Chỉnh sửa"
              onClick={() =>
                this.props.history.push(
                  "/productEditType/" + this.productBrand_id
                )
              }
              color={colors.main}
              // style={{ marginRight: 20 }}
            />
            {/* <IButton
              icon={ISvg.NAME.DELETE}
              title='Xóa'
              color={colors.oranges}
              style={{}}
              onClick={() => {
                const objRemove = {
                  id: [this.productBrand_id],
                  status: -1 // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                }
                this._APIpostRemoveTypeProduct(objRemove)
              }}
            /> */}
          </Row>

          <Row
            style={{ maxWidth: 815, minWidth: 615, flex: 1 }}
            // className="p-0"
          >
            <Col style={{ background: colors.white }} className="pl-5 shadow">
              <Row className="p-0 m-0" style={{}}>
                <Col
                  className="pl-0 pr-0 pt-0 m-0"
                  style={{ paddingBottom: 40 }}
                >
                  <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                    <ITitle
                      level={2}
                      title="Thông tin danh mục sản phẩm"
                      style={{ color: colors.black, fontWeight: "bold" }}
                    />
                  </Row>
                  <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Row className="p-0 m-0">
                        <ITitle
                          level={4}
                          title={"Tên thương hiệu"}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className="p-0 m-0">
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={!data.name ? "-" : data.name}
                        />
                      </Row>
                    </Col>
                    {/* <Col className='p-0 m-0'>
                      <Row className='p-0 m-0'>
                        <ITitle
                          level={4}
                          title={'Mã phân loại'}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className='p-0 m-0'>
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={data.code}
                        />
                      </Row>
                    </Col> */}
                  </Row>

                  <Row className="p-0 mt-4 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Row className="p-0 m-0">
                        <ITitle
                          level={4}
                          title={"Ngành hàng"}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className="p-0 m-0">
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={
                            !data.productTypeName ? "-" : data.productTypeName
                          }
                        />
                      </Row>
                    </Col>

                    <Col
                      className="p-0 m-0"
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Row className="p-0 m-0">
                        <ITitle
                          level={4}
                          title={"Nhóm hàng"}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className="p-0 m-0">
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={!data.categoryName ? "-" : data.categoryName}
                        />
                      </Row>
                    </Col>
                  </Row>
                  {/* <Row className='p-0 mt-4 ml-0 mb-0 mr-0' xs='auto'>
                    <Col
                      className='p-0 m-0'
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Row className='p-0 m-0'>
                        <ITitle
                          level={4}
                          title={'Trạng thái'}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className='p-0 m-0'>
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={data.statusName}
                        />
                      </Row>
                    </Col>
                  </Row> */}

                  {/* <Row className="p-0 mt-3 ml-0 mb-0 mr-0" xs="auto">
                    <Col className="p-0 m-0">
                      <Row className="p-0 m-0">
                        <ITitle
                          level={4}
                          title={"Nhóm phụ thuộc"}
                          style={{ fontWeight: 600, marginBottom: 12 }}
                        />
                      </Row>
                      {list_sub_product_brand.map((item, index) => {
                        return (
                          <Row className="p-0 mt-1 ml-0 mr-0 mb-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={product_brand.name}
                            />
                          </Row>
                        );
                      })}
                    </Col>

                    <Col
                      className="p-0"
                      style={{ marginLeft: 28, marginTop: 0 }}
                    >
                      <Row className="p-0 m-0">
                        <ITitle
                          level={4}
                          title={"Trạng thái"}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className="p-0 m-0">
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={product_brand.statusName}
                        />
                      </Row>
                    </Col>
                  </Row> */}

                  {/* {!this.state.write ? null : (
                    <Row className="p-0 mt-5 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Row className="p-0 m-0">
                          <ISelect
                            type="write"
                            select={this.state.write}
                            placeholder="Chọn nhóm phụ thuộc"
                            // defaultValue={1}
                            data={[
                              { id: 1, value: "Kho KhoiDze" },
                              { id: 2, value: "Kho KhoiDze1" }
                            ]}
                            style={{ flex: 1 }}
                            onChange={(key, data) => {
                              let value = data.props.children;
                              this.setState({
                                nameStoreProduct: value
                              });
                            }}
                          />
                          <div
                            style={{
                              width: 20,
                              height: 20,
                              background: "#FCEFED",
                              borderRadius: 10,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center"
                            }}
                            className="cursor"
                            onClick={index => {
                              let obj = {
                                kho: this.state.nameStoreProduct,
                                soluong: this.state.valueStoreProduct
                              };
                              this.state.dataLuuKho = [
                                ...this.state.dataLuuKho,
                                obj
                              ];
                              this.state.nameStoreProduct = "";
                              this.state.valueStoreProduct = 0;
                              this.setState({});
                            }}
                          >
                            <ISvg
                              name={ISvg.NAME.ADD}
                              width={7}
                              height={7}
                              fill={colors.main}
                            />
                          </div>
                        </Row>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      ></Col>
                    </Row>
                  )} */}
                </Col>
                <Col className="m-0 p-0 mr-4" xs="auto">
                  <div
                    style={{
                      width: 3,
                      height: "100%",
                      background: colors.background,
                      // marginLeft: 10,
                    }}
                  ></div>
                </Col>
                <Col
                  className="pr-0 pt-0 m-0"
                  style={{ paddingBottom: 40 }}
                  xs={5}
                >
                  <Row
                    className="p-0 mt-4 mb-4 ml-0 mr-0"
                    style={
                      {
                        // display: "flex",
                        // justifyContent: "center"
                      }
                    }
                  >
                    <ITitle
                      level={2}
                      title="Hình ảnh"
                      style={{ color: colors.black, fontWeight: "bold" }}
                    />
                  </Row>
                  <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                    {data.images.map((item, index) => {
                      let url = this.state.url + item;

                      return (
                        <IImage src={url} style={{ width: 175, height: 175 }} />
                      );
                    })}
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </Container>
    );
  }
}
