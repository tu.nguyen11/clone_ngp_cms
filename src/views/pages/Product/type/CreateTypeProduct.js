import {
  Typography,
  Form,
  message,
} from "antd";
import React, { Component } from "react";
import {
  ISvg,
  IButton,
  ITitle,
  ISelect,
  ICascader,
  IInput,
  IUpload,
  ISelectStatus,
} from "../../../components";
import { priceFormat } from "../../../../utils";

import InfiniteScroll from "react-infinite-scroller";
import { colors } from "../../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../../services";
import { ImageType } from "../../../../constant";
const { Paragraph, Title } = Typography;

class CreateTypeProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      loading: false,
      modal2Visible: false,
      valueStoreProduct: 0,
      nameStoreProduct: "",
      objAddStore: {},
      dataLuuKho: [],
      imgAdd: [],
      typeIMGCate: "", // Nhom phu thuoc -> nganh hang
      typeIMGCatePro: "", // Nhom hang
      checkType: true,
      value: "", // Check here to configure the default column,
      objAdd: {
        brandName: "",
        codeProductName: "",
        brandProductID: 0, //nhom phu thuoc -> nganh hang
        brandChildrenId: 0,
        cateProductID: 0, //nhom hang
        cateProductName: "",
        cateChildrenId: 0,
        status: 0,
      }, // dữ liệu thêm,
      errStatus: true,
      errBrandId: true,
      errCateProId: true, 
    };
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.onChangeButton = this.onChangeButton.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeBarndCode = this.onChangeBarndCode.bind(this);
    this.onChangeBarndName = this.onChangeBarndName.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
    this.onChangeStatus = this.onChangeStatus.bind(this);
  }

  onSelectChange = (selectedRowKeys) => {
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  onChangeSearch = ({ target: { value } }) => {
    //
    // this.setState({ value });
  };
  onChangeButton = () => {
    this.setState({
      loading: true,
    });

    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 3000);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      fieldsValue.status = this.state.objAdd.status;
      fieldsValue.brandProductID = this.state.objAdd.brandProductID;
      fieldsValue.cateProductID = this.state.objAdd.cateProductID;
      fieldsValue.image = ["hinh12.png"];
      this.props.form.setFieldsValue(
        {
          fieldsValue: fieldsValue,
        },
        () => {
          const Object = {
            id: 0,
            name: fieldsValue.brandName,
            code: fieldsValue.codeProductName,
            parent_id: fieldsValue.brandProductID, //nganh hang
            children_id: fieldsValue.cateProductID, //nhom hang
            status: fieldsValue.status,
            image: this.state.imgAdd,
          };
          this._postAPIAddTypeProduct(Object);
        }
      );
    });
  };

  _postAPIAddTypeProduct = async (obj) => {
    try {
      const data = await APIService._postAddTypeProduct(obj);
      message.success("Thêm loại sản phẩm thành công");
      this.props.history.push("/typeProductPage");
    } catch (err) {
      console.log(err);
    }
  };

  onChangeBarndName = (event) => {
    const { value } = event.target;
    this.state.objAdd.brandName = value;
    this.setState({
      objAdd: this.state.objAdd,
    });
  };

  onChangeBarndCode = (event) => {
    const { value } = event.target;
    this.state.objAdd.codeProductName = value;
    this.setState({
      objAdd: this.state.objAdd,
    });
  };

  //Ten nhom phu thuoc -> Nganh hang
  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
      this.state.objAdd.brandProductID = value[0];
      if(value[0] == 0) {
        this.state.typeIMGCate = "root";
      }
      this.setState(
        {
          objAdd: this.state.objAdd,
          errBrandId: false,
          typeIMGCate: this.state.typeIMGCate,
          checkType: false,
        },
        () =>
          this.props.form.validateFields(["brandProductID"], { force: true })
      );
    }
    if (value.length == 2) {
      this.state.objAdd.brandChildrenId = value[2];
      this.setState({
        objAdd: this.state.objAdd,
      });
    }
  };

  //Them moi nhom hang phu thuoc nganh hàng
  onChangeCatetoryProduct = (value, selectedOptions) => {
    if (value.length == 1){
      this.state.objAdd.cateProductID = value[0];
      if(value[0] == 0){
        this.state.typeIMGCatePro = "root";    
      }
      this.setState(
        {
          objAdd: this.state.objAdd,
          errCateProId: false,
          typeIMGCatePro: this.state.typeIMGCatePro,
          checkType: false,
        },
        () => this.props.form.validateFields(["cateProductID"], {force: true})
      );
    }
    if(value.length == 2){
      this.state.objAddStore.cateChildrenId = value[2];
      this.setState({
        objAdd: this.state.objAdd,
      });
    }
  }

  onChangeStatus = (value) => {
    this.state.objAdd.status = Number(value);
    this.setState({ objAdd: this.state.objAdd, errStatus: false }, () =>
      this.props.form.validateFields(["status"], { force: true })
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Container fluid>
        <Row className="mt-3" xs="auto">
          <ITitle
            level={1}
            title="Tạo danh mục sản phẩm mới"
            style={{ color: colors.mainDark, fontWeight: "bold" }}
          />
        </Row>
        <Form onSubmit={this.handleSubmit}>
          <Row
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingTop: 32,
              paddingBottom: 32,
            }}
            xs="auto"
            sm={12}
            md={12}
          >
            <IButton
              icon={ISvg.NAME.SAVE}
              title="Lưu"
              htmlType="submit"
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => this.handleSubmit}
            />
            <IButton
              icon={ISvg.NAME.CROSS}
              title="Hủy"
              color={colors.oranges}
              style={{}}
              onClick={() => {
                this.props.history.push("/typeProductPage");
              }}
            />
          </Row>
          <div
            style={{ maxWidth: 815, minWidth: 615, flex: 1, height: "100%" }}
          >
            <Row className="p-0">
              <Col style={{ background: colors.white }} className="pl-5">
                <Row className="p-0 m-0" style={{}}>
                  <Col
                    className="pl-0 pr-0 pt-0 m-0"
                    style={{ paddingBottom: 40 }}
                  >
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Thông tin danh mục sản phẩm"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>

                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("brandName", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: true,
                                message: "Vui lòng nhập tên thương hiệu",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Tên thương hiệu"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  placeholder="Nhập tên thương hiệu"
                                  onChange={this.onChangeBarndName}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      {/* <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {
                            getFieldDecorator("codeProductName", {
                              rules: [
                                {
                                  // required: this.state.checkNick,
                                  required: true,
                                  message: "nhập mã",
                                },
                              ],
                            })
                            (
                              <div>
                                <Row className="p-0 m-0  mb-2">
                                  <ITitle
                                    level={4}
                                    title={"Mã phân loại"}
                                    style={{ fontWeight: 600 }}
                                  />
                                </Row>
                                <Row className="p-0 m-0">
                                  <IInput
                                    placeholder="Nhập mã"
                                    onChange={this.onChangeBarndCode}
                                  />
                                </Row>
                              </div>
                            )
                          }
                        </Form.Item>
                      </Col> */}
                    </Row>

                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("brandProductID", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: this.state.errBrandId,
                                message: "Vui lòng chọn ngành hàng",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Ngành hàng"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ICascader
                                  style={{
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  type="category"
                                  level={1}
                                  addRoot={true}
                                  placeholder="Nhập tên ngành hàng"
                                  onChange={this.onChangeCategory}
                                  isBackground={false}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                       <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("cateProduct", {
                            rules: [
                              {
                                required: this.state.errCateProId,
                                message: "Vui lòng chọn nhóm hàng",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Nhóm hàng"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      {/* <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("status", {
                            rules: [
                              {
                                // required: this.state.checkNick,
                                required: this.state.errStatus,
                                message: "chọn trạng thái",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Trạng thái"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectStatus
                                  style={{
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  onChange={this.onChangeStatus}
                                  placeholder="Chọn trạng thái"
                                  isBackground={false}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col> */}
                    </Row>
                  </Col>
                  <Col className="m-0 p-0 ml-5" xs="auto">
                    <div
                      style={{
                        width: 3,
                        height: "100%",
                        background: colors.background,
                        // marginLeft: 10,
                      }}
                    ></div>
                  </Col>
                  <Col
                    className="pr-0 pt-0 m-0"
                    style={{ paddingBottom: 40 }}
                    xs={5}
                  >
                    <Row
                      className="p-0 mt-4 mb-4 ml-0 mr-0"
                      style={{
                        display: "flex",
                        justifyContent: "center",
                      }}
                    >
                      <ITitle
                        level={2}
                        title="Hình ảnh danh mục sản phẩm"
                        align="left"
                        style={{
                          color: colors.black,
                          fontWeight: "bold",
                        }}
                      />
                      <Form.Item>
                        {getFieldDecorator("image", {
                          rules: [
                            {
                              // required: this.state.checkNick,
                              required: true,
                              message: "chọn hình",
                              valuePropName: "fileList",
                              getValueFromEvent: this.normFile,
                            },
                          ],
                        })(
                          <Row
                            className="p-0 mt-4 mb-4 ml-0 mr-0"
                            style={{
                              display: "flex",
                              justifyContent: "center",
                            }}
                          >
                            <IUpload
                              type={
                                this.state.typeIMGCate == "root"
                                  ? ImageType.PRODUCT_TYPE
                                  : ImageType.CATE
                              }
                              disabled={this.state.checkType}
                              callback={(array) => {
                                this.state.imgAdd = array;

                                this.setState({});
                              }}
                              remove={(index) => {
                                this.setState({});
                              }}
                            />
                          </Row>
                        )}
                      </Form.Item>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Form>
      </Container>
    );
  }
}

const createTypeProduct = Form.create({ name: "CreateTypeProduct" })(
  CreateTypeProduct
);
export default createTypeProduct;
