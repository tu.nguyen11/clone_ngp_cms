import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton, message } from "antd";
import { ITitle, IButton, ISvg, IImage } from "../../../components";
import { colors } from "../../../../assets";

import { APIService } from "../../../../services";
import { useParams, useHistory } from "react-router-dom";
import { priceFormat } from "../../../../utils";

function DetailNPPProductPage(props) {
  const params = useParams();
  const { id } = params;
  const history = useHistory();

  const [isLoading, setIsLoading] = useState(true);
  const [dataDetail, setDataDetail] = useState({ images: [] });

  const _fetchAPIDetailProductC1 = async (id) => {
    try {
      const data = await APIService._getDetailProductC1(id);
      setDataDetail(data.product);
      setIsLoading(false);
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  };

  const _APIpostRemoveProduct = (objRemove) => {
    try {
      const data = APIService._postRemoveProduct(objRemove);
      message.success("Xóa thành công.");
      history.push("/products/npp/list");
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    _fetchAPIDetailProductC1(id);
  }, []);

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row gutter={[12, 16]}>
        <Col>
          <ITitle
            level={1}
            title="Chi tiết sản phẩm Đại lý cấp 1"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
            }}
          />
        </Col>
        <Col>
          <Row gutter={[12, 20]}>
            <Col span={24}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-end",
                }}
              >
                <IButton
                  icon={ISvg.NAME.DELETE}
                  title="Xóa"
                  color={colors.red}
                  onClick={() => {
                    const objRemove = {
                      id: [id],
                      status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                    };
                    _APIpostRemoveProduct(objRemove);
                  }}
                />
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                }}
              >
                <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
                  <Col span={12}>
                    <div
                      style={{
                        background: colors.white,
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      }}
                    >
                      {false ? (
                        <div style={{ padding: "30px 40px" }}>
                          <Skeleton
                            loading={isLoading}
                            active
                            paragraph={{ rows: 16 }}
                          />
                        </div>
                      ) : (
                        <Row>
                          <Col span={16}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col style={{ marginTop: 6 }}>
                                  <ITitle
                                    level={2}
                                    title="Thông tin sản phẩm"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col>
                                  <div>
                                    <p style={{ fontWeight: 600 }}>
                                      Tên sản phẩm
                                    </p>
                                    <span>{dataDetail.name}</span>
                                  </div>
                                </Col>
                                <Col>
                                  <Row gutter={[12, 12]}>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Mã sản phẩm
                                        </p>
                                        <span>{dataDetail.productCode}</span>
                                      </div>
                                    </Col>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Trạng thái
                                        </p>
                                        <span>{dataDetail.statusName}</span>
                                      </div>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col>
                                  <Row gutter={[12, 12]}>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Ngành hàng
                                        </p>
                                        <span>{dataDetail.typeName}</span>
                                      </div>
                                    </Col>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Thương hiệu
                                        </p>
                                        <span>
                                          {dataDetail.trade_mark_name}
                                        </span>
                                      </div>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col>
                                  <Row gutter={[12, 12]}>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Thời gian bảo hành
                                        </p>
                                        <span>
                                          {dataDetail.warranty_period_name}
                                        </span>
                                      </div>
                                    </Col>
                                    <Col span={12}>
                                      <div>
                                        <p style={{ fontWeight: 600 }}>
                                          Nhà cung cấp
                                        </p>
                                        <span>{dataDetail.supplier_name}</span>
                                      </div>
                                    </Col>
                                  </Row>
                                </Col>
                              </Row>
                              <div style={{ marginTop: 30 }}>
                                <Row gutter={[12, 20]}>
                                  <Col style={{ marginTop: 6 }}>
                                    <ITitle
                                      level={2}
                                      title="Quy cách"
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    />
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={12}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Đơn vị lẻ
                                          </p>
                                          <span>
                                            {dataDetail.variationNameMin}
                                          </span>
                                        </div>
                                      </Col>
                                      <Col span={12}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Đơn vị lớn nhất
                                          </p>
                                          <span>
                                            {dataDetail.variationNameMax}
                                          </span>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={12}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Quy cách đơn vị lẻ
                                          </p>
                                          <span>
                                            {`${dataDetail.quantityMin} ${dataDetail.variationNameMin}`}
                                          </span>
                                        </div>
                                      </Col>
                                      <Col span={12}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Quy cách đơn vị lớn nhất
                                          </p>
                                          <span>
                                            {`${dataDetail.quantityMax} ${dataDetail.variationNameMin}`}
                                          </span>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Col>
                                </Row>
                              </div>
                              <div style={{ marginTop: 30 }}>
                                <Row gutter={[12, 20]}>
                                  <Col style={{ marginTop: 6 }}>
                                    <ITitle
                                      level={2}
                                      title="Thông tin đại lý cấp 1"
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    />
                                  </Col>
                                  <Col>
                                    <Row gutter={[12, 12]}>
                                      <Col span={12}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Mã đại lý
                                          </p>
                                          <span>
                                            {dataDetail.user_agency_code}
                                          </span>
                                        </div>
                                      </Col>
                                      <Col span={12}>
                                        <div>
                                          <p style={{ fontWeight: 600 }}>
                                            Tên đại lý
                                          </p>
                                          <span>
                                            {dataDetail.user_agency_name}
                                          </span>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Col>
                                </Row>
                              </div>
                            </div>
                          </Col>
                          <Col span={8}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col style={{ marginTop: 6 }}>
                                  <ITitle
                                    level={2}
                                    title="Giá bán"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col>
                                  <div>
                                    <p style={{ fontWeight: 600 }}>
                                      Giá đơn vị lẻ
                                    </p>
                                    <span>
                                      {priceFormat(dataDetail.price_min) + "đ"}
                                    </span>
                                  </div>
                                </Col>
                                <Col>
                                  <div>
                                    <p style={{ fontWeight: 600 }}>
                                      Giá theo thùng/ kiện
                                    </p>
                                    <span>
                                      {priceFormat(dataDetail.price_max) + "đ"}
                                    </span>
                                  </div>
                                </Col>
                              </Row>

                              <div style={{ marginTop: 30 }}>
                                <Row gutter={[12, 20]}>
                                  <Col style={{ marginTop: 6 }}>
                                    <ITitle
                                      level={2}
                                      title="Số lượng tồn"
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    />
                                  </Col>
                                  <Col>
                                    <div>
                                      <p style={{ fontWeight: 600 }}>
                                        Số lượng
                                      </p>
                                      <span>{dataDetail.remain_amount}</span>
                                    </div>
                                  </Col>
                                </Row>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      )}
                    </div>
                  </Col>
                  <Col span={7}>
                    <div
                      style={{
                        padding: "30px 32.5px",
                        background: "white",
                        height: "100%",
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      }}
                    >
                      {false ? (
                        <div>
                          <Skeleton
                            loading={isLoading}
                            active
                            paragraph={{ rows: 16 }}
                          />
                        </div>
                      ) : (
                        <Row gutter={[12, 20]}>
                          <Col span={24}>
                            <ITitle
                              level={2}
                              title="Mô tả sản phẩm"
                              style={{
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col span={24}>
                            <span style={{ wordBreak: "break-word" }}>
                              {dataDetail.description}
                            </span>
                          </Col>
                          <div style={{ marginTop: 30 }}>
                            <Col span={24}>
                              <ITitle
                                level={2}
                                title="Phụ kiện đi kèm"
                                style={{
                                  fontWeight: "bold",
                                }}
                              />
                            </Col>
                            <Col span={24}>
                              <span style={{ wordBreak: "break-word" }}>
                                {dataDetail.accessory}
                              </span>
                            </Col>
                          </div>
                        </Row>
                      )}
                    </div>
                  </Col>

                  <Col span={5}>
                    <div
                      style={{
                        padding: "30px 32.5px",
                        background: "white",
                        height: "100%",
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      }}
                    >
                      <Row gutter={[0, 24]}>
                        <Col span={24}>
                          <ITitle
                            level={2}
                            title="Hình ảnh sản phẩm"
                            style={{
                              color: colors.black,
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col span={24}>
                          {dataDetail.images.map((item, index) => {
                            return (
                              <IImage
                                src={dataDetail.image_url + item}
                                style={{
                                  width: 100,
                                  height: 100,
                                  marginRight: 15,
                                  marginBottom: 15,
                                }}
                              />
                            );
                          })}
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}

export default DetailNPPProductPage;
