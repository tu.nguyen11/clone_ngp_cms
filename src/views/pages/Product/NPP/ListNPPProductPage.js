import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message } from "antd";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IButton,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import { priceFormat } from "../../../../utils";
import { useHistory } from "react-router-dom";
function ListNPPProductPage(props) {
  const history = useHistory();

  const [filter, setFilter] = useState({
    key: "",
    page: 1,
    status: 2,
    type: 0,
    category_id: 0,
    user_agency_id: -1,
  });

  const [dataTable, setDataTable] = useState({
    listProduct: [],
    total: 1,
    size: 10,
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [dataBranch, setDataBranch] = useState([]);
  const [dataSubCategory, setDataSubCategory] = useState([]);
  const [dataSupplier, setDataSupplier] = useState([]);

  const [loadingGroupProduct2, setLoadingGroupProduct2] = useState(false);
  const [loadingTable, setLoadingTable] = useState(true);

  const _fetchAPIListProductsC1 = async (filter) => {
    try {
      const data = await APIService._getListProductsC1(filter);
      data.product.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        return item;
      });

      setDataTable({
        listProduct: data.product,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const _fetchListBrandCategory = async () => {
    try {
      const data = await APIService._getListBrandCategory();
      let category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      category.unshift({
        key: 0,
        value: "Tất cả",
      });
      setDataBranch(category);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchListSubCategpry = async (parent_id) => {
    try {
      const data = await APIService._getListSubCategpry(parent_id);
      let sub_category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      sub_category.unshift({
        key: 0,
        value: "Tất cả",
      });
      setDataSubCategory(sub_category);
      setLoadingGroupProduct2(false);
    } catch (err) {
      console.log(err);
      setLoadingGroupProduct2(false);
    }
  };

  const _fetchAPIListNPP = async () => {
    try {
      const data = await APIService._getListAgencyS1Name("");
      let suppliersName = data.suppliers_name.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      suppliersName.unshift({
        key: -1,
        value: "Tất cả",
      });
      setDataSupplier(suppliersName);
    } catch (err) {
      console.log(err);
    }
  };

  const _APIPostRemoveProduct = async (objRemove) => {
    try {
      const data = await APIService._postRemoveProduct(objRemove);
      setSelectedRowKeys([]);
      await _fetchAPIListProductsC1(filter);
      message.success("Xóa thành công");
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    _fetchAPIListProductsC1(filter);
    _fetchListBrandCategory();
    _fetchAPIListNPP();
  }, [filter]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },

    {
      title: "Tên sản phẩm",
      dataIndex: "name",
      key: "name",
      render: (name) => (
        <Tooltip title={name}>
          <span>{name}</span>
        </Tooltip>
      ),
    },
    {
      title: "Mã sản phẩm",
      dataIndex: "code",
      key: "code",
      render: (code) => <span>{code}</span>,
    },
    {
      title: "Ngành hàng",
      dataIndex: "cate_name",
      key: "cate_name",
      render: (cate_name) => <span>{cate_name}</span>,
    },
    {
      title: "Nhóm hàng",
      dataIndex: "sub_cate",
      key: "sub_cate",
      render: (sub_cate) => <span>{sub_cate}</span>,
    },
    {
      title: "Đơn vị",
      dataIndex: "min_unit",
      key: "min_unit",
      align: "center",
      render: (min_unit) => <span>{min_unit}</span>,
    },
    {
      title: "Giá đơn vị",
      dataIndex: "price",
      key: "price",
      render: (price) => <span>{priceFormat(price) + "đ"}</span>,
    },
    {
      title: "Giá thùng kiện",
      dataIndex: "price_max_unit",
      key: "price_max_unit",
      render: (price_max_unit) => (
        <span>{priceFormat(price_max_unit) + "đ"}</span>
      ),
    },
    {
      title: "Thuế VAT",
      dataIndex: "vat",
      key: "vat",
      align: "center",
      render: (vat) => <span>{vat + "%"}</span>,
    },
    {
      title: "Hình ảnh",
      dataIndex: "image",
      align: "center",
      key: "image",
      render: (image) => {
        return (
          <div>
            <img
              src={image}
              alt="image"
              style={{
                width: 40,
                height: 40,
              }}
            />
          </div>
        );
      },
    },
    {
      title: "Số lượng còn",
      dataIndex: "remain_amount",
      key: "remain_amount",
      align: "center",
      render: (remain_amount) => <span>{remain_amount}</span>,
    },
    {
      title: "Đại lý cấp 1",
      dataIndex: "user_agency_name",
      key: "user_agency_name",
      render: (user_agency_name) => <span>{user_agency_name}</span>,
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      align: "center",
      render: (status) => (status === 1 ? <span>Hiện</span> : <span>Ẩn</span>),
    },
    // {
    //   align: "right",
    //   fixed: widthScreen <= 1440 ? "right" : "",
    //   width: 80,
    //   render: (data) => {
    //     return (
    //       <div style={{ width: 40 }}>
    //         <Tooltip title="Chi tiết sản phẩm">
    //           <div
    //             id="nextDetail"
    //             // className="cursor-push"
    //             onClick={(e) => {
    //               e.stopPropagation();
    //               history.push("/product/npp/detail/" + data.id);
    //             }}
    //             // className="cursor scale"
    //           >
    //             <ISvg
    //               name={ISvg.NAME.CHEVRONRIGHT}
    //               width={11}
    //               height={20}
    //               fill={colors.icon.default}
    //             />
    //           </div>
    //         </Tooltip>
    //       </div>
    //     );
    //   },
    // },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Danh sách sản phẩm Đại lý cấp 1
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên, mã sản phẩm đại lý cấp 1">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên, mã sản phẩm đại lý cấp 1"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={22} lg={23}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <ITitle
                        title="Ngành hàng"
                        level={4}
                        style={{
                          marginLeft: 10,
                        }}
                      />
                      <div style={{ margin: "0px 15px 0px 15px" }}>
                        <div style={{ width: 200 }}>
                          <ISelect
                            defaultValue="Tất cả"
                            data={dataBranch}
                            select={true}
                            value={filter.type}
                            onChange={(value) => {
                              setLoadingTable(true);

                              setFilter({
                                ...filter,
                                type: value,
                                category_id: 0,
                                page: 1,
                              });
                            }}
                            onDropdownVisibleChange={(open) => {
                              if (!open) {
                                if (dataBranch.length === 0) {
                                  return;
                                }

                                setLoadingGroupProduct2(true);
                                _fetchListSubCategpry(filter.category_id);
                                return;
                              }
                            }}
                          />
                        </div>
                      </div>

                      <ITitle
                        title="Nhóm hàng"
                        level={4}
                        style={{
                          marginLeft: 10,
                        }}
                      />
                      <div style={{ margin: "0px 15px 0px 15px" }}>
                        <div style={{ width: 200 }}>
                          <ISelect
                            defaultValue="Tất cả"
                            loading={loadingGroupProduct2}
                            data={dataSubCategory}
                            select={true}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setFilter({
                                ...filter,
                                category_id: value,
                                page: 1,
                              });
                            }}
                            onDropdownVisibleChange={(open) => {
                              if (open) {
                                setLoadingGroupProduct2(true);
                                _fetchListSubCategpry(filter.type);
                                return;
                              }
                            }}
                            value={
                              filter.category_id === 0
                                ? "Tất cả"
                                : filter.category_id
                            }
                          />
                        </div>
                      </div>
                      <ITitle
                        title="NPP"
                        level={4}
                        style={{
                          marginLeft: 10,
                        }}
                      />
                      <div style={{ margin: "0px 15px 0px 15px" }}>
                        <div style={{ width: 200 }}>
                          <ISelect
                            defaultValue="Tất cả"
                            data={dataSupplier}
                            select={true}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setFilter({
                                ...filter,
                                user_agency_id: value,
                                page: 1,
                              });
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col span={2} lg={1}>
                    <div className="flex justify-end">
                      <IButton
                        icon={ISvg.NAME.DELETE}
                        title={"Xóa"}
                        color={colors.red}
                        styleHeight={{
                          width: 120,
                        }}
                        onClick={() => {
                          let arrayID = [];

                          selectedRowKeys.map((item, index) => {
                            arrayID.push(dataTable.listProduct[item].id);
                          });
                          if (arrayID.length === 0) {
                            message.warning(
                              "Bạn chưa chọn mã sản phẩm để xóa."
                            );
                            return;
                          }
                          const objRemove = {
                            status: -1,
                            id: arrayID,
                          };
                          _APIPostRemoveProduct(objRemove);
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listProduct}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    scroll={{ x: 1600 }}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.listProduct[indexRow].id;
                          history.push("/product/npp/detail/" + id);
                        }, // click row
                        onMouseDown: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.listProduct[indexRow].id;
                          if (event.button == 2 || event == 4) {
                            window.open("/product/npp/detail/" + id, "_blank");
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></ITable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default ListNPPProductPage;
