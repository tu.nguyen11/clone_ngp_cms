import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  Upload,
  Icon,
  Form,
  message,
} from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  ISelectStatus,
  ICascader,
  ISelectAttrubie,
} from "../../components";
import { priceFormat } from "../../../utils";

import InfiniteScroll from "react-infinite-scroller";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";
import IUploadMultiple from "../../components/IUploadMultiple";
import { ImageType } from "../../../constant";
const { Paragraph, Title } = Typography;

class EditProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      write: true,
      valueStoreProduct: 0,
      nameStoreProduct: "",
      status: 0,
      errStatus: true,
      errCategory: true,
      typeId: 0,
      categoryId: 0,
      errAttributesMax: true,
      errAttributesMin: true,
      arrayImagesAdd: [],
      attributeIdMin: 0,
      attributeIdMax: 0,
      value: "", // Check here to configure the default column
      objEdit: {
        name: "",
        productCode: "",
        status: 0,
        typeId: 0,
        categoryId: 0,
        attributeIdMin: 0,
        quantityMin: 0,
        attributeIdMax: 0,
        quantityMax: 0,
        price_min: 0,
        price_max: 0,
        oldPrice_max: 0,
        oldPrice_min: 0,
        distributorOffer: 0,
        description: "",
        element: "",
        uses: "",
        note: "",
        images: [],
      },
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeStatus = this.onChangeStatus.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
    this.productId = Number(this.props.match.params.id); // params Product ID
  }

  componentDidMount() {
    this._getAPIDetailProduct(this.productId);
  }

  _getAPIDetailProduct = async (orderId) => {
    try {
      const data = await APIService._getDetailProduct(orderId);

      this.setState({
        objEdit: data.product,
        arrayImagesAdd: data.product.images,
        loading: true,
      });
    } catch (err) {
      console.log(err);
    }
  };

  _postAPIEditProduct = async (objEdit) => {
    try {
      const data = await APIService._postEditProduct(objEdit);
      message.success("Cập nhập thành công");
      this.props.history.push("/productDetail/" + this.productId);
    } catch (err) {
      console.log(err);
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      const objAdd = {
        id: this.productId,
        productCode: this.state.objEdit.productCode,
        productName: this.state.objEdit.name,
        status: this.state.objEdit.status,
        typeId: this.state.objEdit.typeId,
        categoryId: this.state.objEdit.categoryId,
        attributeIdMin: this.state.objEdit.attributeIdMin,
        quantityMin: this.state.objEdit.quantityMin,
        attributeIdMax: this.state.objEdit.attributeIdMax,
        quantityMax: this.state.objEdit.quantityMax,
        priceMin: Number(this.state.objEdit.price_min),
        priceMax: Number(this.state.objEdit.price_max),
        distributorOffer: this.state.objEdit.distributorOffer,
        description: this.state.objEdit.description,
        element: this.state.objEdit.element,
        uses: this.state.objEdit.uses,
        note: this.state.objEdit.note,
        image: this.state.arrayImagesAdd,
      };

      this._postAPIEditProduct(objAdd);
    });
  };

  onChangeStatus = (value) => {
    this.state.objEdit.status = value;
    this.setState({ objEdit: this.state.objEdit, errStatus: false }, () =>
      this.props.form.validateFields(["status"], { force: true })
    );
  };

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
    }
    if (value.length == 2) {
      this.state.objEdit.typeId = value[0];
      this.state.objEdit.categoryId = value[1];

      this.setState({
        errCategory: false,
        typeId: value[0],
        categoryId: value[1],
        objEdit: this.state.objEdit,
      });
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Container fluid>
        <Row sm={12} md={12}>
          <ITitle
            level={1}
            title="Chỉnh sửa sản phẩm"
            style={{ color: colors.main, fontWeight: "bold" }}
          />
        </Row>
        <Form onSubmit={this.handleSubmit}>
          <Row
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingTop: 32,
              paddingBottom: 32,
            }}
          >
            <IButton
              icon={ISvg.NAME.SAVE}
              title="Lưu"
              htmlType="submit"
              color={colors.main}
              style={{ marginRight: 20 }}
              onClick={() => this.handleSubmit}
            />
            <IButton
              icon={ISvg.NAME.CROSS}
              title="Hủy"
              color={colors.oranges}
              style={{}}
              onClick={() => {
                this.props.history.push("/productPage");
              }}
            />
          </Row>
          <div style={{}}>
            <Row style={{ height: "100%" }} className="p-0" sm={12} md={12}>
              <Col
                xs={6}
                className="mr-2 shadow"
                style={{ background: colors.white }}
              >
                <Row className="p-0 m-0" style={{}}>
                  <Col className="pl-0 pr-0 pt-0 m-0 ml-4">
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Thông tin sản phẩm"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>

                    <Form.Item>
                      {getFieldDecorator("productName", {
                        rules: [
                          {
                            // required: this.state.checkNick,
                            message: "nhập tên gọi đầy đủ",
                          },
                        ],
                      })(
                        <Row className="p-0 m-0" xs="auto">
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Tên sản phẩm"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                placeholder="Nhập tên gọi đầy đủ"
                                value={this.state.objEdit.name}
                                onChange={(e) => {
                                  this.state.objEdit.name = e.target.value;
                                  this.setState({
                                    objEdit: this.state.objEdit,
                                  });
                                }}
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("productCode", {
                            rules: [
                              {
                                message: "nhập mã",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Mã sản phẩm"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  placeholder="Nhập mã"
                                  value={this.state.objEdit.productCode}
                                  onChange={(e) => {
                                    this.state.objEdit.productCode =
                                      e.target.value;
                                    this.setState({
                                      objEdit: this.state.objEdit,
                                    });
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("status", {
                            rules: [
                              {
                                message: "chọn trạng thái",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Trạng thái"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectStatus
                                  onChange={this.onChangeStatus}
                                  placeholder="Chọn trạng thái"
                                  value={this.state.objEdit.status}
                                  isBackground={false}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 mt-0">
                        <Form.Item>
                          {getFieldDecorator("category", {
                            rules: [
                              {
                                message: "chọn nghành hàng",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Ngành hàng"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ICascader
                                  className="clear-pad"
                                  type="category"
                                  level={2}
                                  optionData="edit"
                                  // style={{width: '100%'}}
                                  placeholder="Chọn nhóm sản phẩm"
                                  value={[
                                    this.state.objEdit.typeId,
                                    this.state.objEdit.categoryId,
                                  ]}
                                  onChange={this.onChangeCategory}
                                  isBackground={false}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                      <Col></Col>
                    </Row>
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Quy cách"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>
                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("attributeIdMin", {
                            rules: [
                              {
                                message: "chai/túi/bao 10kg....",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Đơn vị lẻ"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectAttrubie
                                  placeholder="chai/túi/bao 10kg..."
                                  type="min"
                                  value={this.state.objEdit.attributeIdMin}
                                  onChange={(value) => {
                                    this.state.objEdit.attributeIdMin = value;
                                    this.setState(
                                      {
                                        objEdit: this.state.objEdit,
                                        errAttributesMin: false,
                                      },
                                      () =>
                                        this.props.form.validateFields(
                                          ["attributeIdMin"],
                                          { force: true }
                                        )
                                    );
                                  }}
                                  isBackground={false}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("attributeIdMax", {
                            rules: [
                              {
                                message: "chọn đơn vị lớn nhất",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Đơn vị lớn nhất"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <ISelectAttrubie
                                  style={{
                                    background: colors.white,
                                    borderStyle: "solid",
                                    borderWidth: 0,
                                    borderColor: colors.line,
                                    borderBottomWidth: 1,
                                  }}
                                  placeholder="Chọn đơn vị lớn nhất"
                                  type="max"
                                  value={this.state.objEdit.attributeIdMax}
                                  onChange={(value) => {
                                    this.state.objEdit.attributeIdMax = value;
                                    this.setState(
                                      {
                                        objEdit: this.state.objEdit,
                                        errAttributesMax: false,
                                      },
                                      () =>
                                        this.props.form.validateFields(
                                          ["attributeIdMax"],
                                          { force: true }
                                        )
                                    );
                                  }}
                                  isBackground={false}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                      <Col className="p-0 m-0">
                        <Form.Item>
                          {getFieldDecorator("quantityMin", {
                            rules: [
                              {
                                message: "nhập cân nặng mỗi đơn vị",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Quy cách đơn vị lẻ"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  placeholder="Nhập cân nặng mỗi đơn vị"
                                  type="number"
                                  value={
                                    this.state.objEdit.quantityMin
                                    // " " +
                                    // this.state.data.variationNameMin
                                  }
                                  onChange={(e) => {
                                    this.state.objEdit.quantityMin =
                                      e.target.value;
                                    this.setState({
                                      objEdit: this.state.objEdit,
                                    });
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>

                      <Col
                        className="p-0"
                        style={{ marginLeft: 28, marginTop: 0 }}
                      >
                        <Form.Item>
                          {getFieldDecorator("quantityMax", {
                            rules: [
                              {
                                message: "nhập số lượng",
                              },
                            ],
                          })(
                            <div>
                              <Row className="p-0 m-0 mb-2">
                                <ITitle
                                  level={4}
                                  title={"Quy cách đơn vị lớn nhất"}
                                  style={{ fontWeight: 600 }}
                                />
                              </Row>
                              <Row className="p-0 m-0">
                                <IInput
                                  placeholder="Nhập số lượng"
                                  type="number"
                                  value={
                                    this.state.objEdit.quantityMax
                                    // " " +
                                    // this.state.data.variationNameMin
                                  }
                                  onChange={(e) => {
                                    this.state.objEdit.quantityMax =
                                      e.target.value;
                                    this.setState({
                                      objEdit: this.state.objEdit,
                                    });
                                  }}
                                />
                              </Row>
                            </div>
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    {/* <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
      <ITitle
        level={2}
        title="Lưu kho"
        style={{ color: colors.black, fontWeight: "bold" }}
      />
    </Row>
    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
      <Col className="p-0 m-0">
        <Row className="p-0 m-0">
          <ITitle
            level={4}
            title={"Kho lưu trữ"}
            style={{ fontWeight: 600, marginBottom: 12 }}
          />
        </Row>
        {this.state.dataLuuKho.map((item, index) => {
          return (
            <Row className="p-0 mt-1 ml-0 mr-0 mb-0">
              <IInput
                write={this.state.write}
                defaultValue={item.kho}
                style={{ height: 20 }}
              />
            </Row>
          );
        })}
      </Col>

      <Col className="p-0" style={{ marginLeft: 28, marginTop: 0 }}>
        <Row className="p-0 m-0">
          <ITitle
            level={4}
            title={"Trọng lượng thùng/kiện"}
            style={{ fontWeight: 600, marginBottom: 12 }}
          />
        </Row>
        {this.state.dataLuuKho.map((item, index) => {
          return (
            <Row className="p-0 mt-1 ml-0 mr-0 mb-0">
              <IInput
                write={this.state.write}
                defaultValue={item.soluong}
                style={{ height: 20, flex: 1 }}
              />
              {!this.state.write ? null : (
                <div
                  style={{
                    width: 20,
                    height: 20,
                    background: "#FCEFED",
                    borderRadius: 10,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  className="cursor"
                  onClick={() => {
                    let a = this.state.dataLuuKho.splice(index, 1);
                    this.setState({
                      dataLuuKho: this.state.dataLuuKho
                    });
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.CROSS}
                    width={7}
                    height={7}
                    fill={colors.oranges}
                  />
                </div>
              )}
            </Row>
          );
        })}
      </Col>
    </Row>

    {!this.state.write ? null : (
      <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
        <Col className="p-0 m-0">
          <ISelect
            type="write"
            select={this.state.write}
            placeholder="Chọn thêm kho"
            // defaultValue={1}
            data={[
              { key: 1, value: "Kho KhoiDze" },
              { key: 2, value: "Kho KhoiDze1" }
            ]}
            onChange={(key, data) => {
              let value = data.props.children;
              this.setState({
                nameStoreProduct: value
              });
            }}
          />
        </Col>

        <Col
          className="p-0"
          style={{ marginLeft: 28, marginTop: 0 }}
        >
          <Row
            className="p-0 mt-0 ml-0 mr-0 mb-0"
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ISelect
              type="write"
              select={this.state.write}
              style={{ flex: 1 }}
              onChange={(key, data) => {
                let value = data.props.children;
                this.setState({
                  valueStoreProduct: value
                });
              }}
              placeholder="thêm số lượng còn lại"
              data={[
                { key: 1, value: 10000 },
                { key: 2, value: 20000 }
              ]}
            />
            <div
              style={{
                width: 20,
                height: 20,
                background: "#FCEFED",
                borderRadius: 10,
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
              className="cursor"
              onClick={index => {
                let obj = {
                  kho: this.state.nameStoreProduct,
                  soluong: this.state.valueStoreProduct
                };
                this.state.dataLuuKho = [
                  ...this.state.dataLuuKho,
                  obj
                ];
                this.state.nameStoreProduct = "";
                this.state.valueStoreProduct = 0;
                this.setState({});
              }}
            >
              <ISvg
                name={ISvg.NAME.ADD}
                width={7}
                height={7}
                fill={colors.main}
              />
            </div>
          </Row>
        </Col>
      </Row>
    )} */}
                  </Col>
                  {/* <div
      style={{
        width: 1,
        height: '100%',
        background: 'red',
        marginLeft: 30,
      }}
    ></div> */}
                  <Col className="m-0 p-0 " xs="auto">
                    <div
                      style={{
                        width: 1,
                        height: "100%",
                        background: colors.gray._300,
                        marginLeft: 25,
                      }}
                    ></div>
                  </Col>
                  <Col className="pr-0 pt-0 m-0" xs={3}>
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Giá bán"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>
                    <Row className="p-0 m-0">
                      <Form.Item>
                        {getFieldDecorator("priceMin", {
                          rules: [
                            {
                              message: "nhập giá tiền mỗi đơn vị",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Giá đơn vị"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                type="number"
                                placeholder={"Nhập giá tiền mỗi đơn vị"}
                                value={
                                  this.state.objEdit.price_min
                                  // " " +
                                  // this.state.data.variationNameMin
                                }
                                onChange={(e) => {
                                  this.state.objEdit.price_min = e.target.value;
                                  this.setState({
                                    objEdit: this.state.objEdit,
                                  });
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>

                      <Form.Item>
                        {getFieldDecorator("priceMax", {
                          rules: [
                            {
                              message: "nhập giá tiền thùng/kiện",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Giá theo thùng/kiện"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                type="number"
                                disabled={true}
                                placeholder={"Giá đơn vị lớn nhất"}
                                value={
                                  this.state.objEdit.price_max
                                  // " " +
                                  // this.state.data.variationNameMin
                                }
                                onChange={(e) => {
                                  this.state.objEdit.price_max = e.target.value;
                                  this.setState({
                                    objEdit: this.state.objEdit,
                                  });
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>

                      {/* <Form.Item>
                        {getFieldDecorator("distributorOffer", {
                          rules: [
                            {
                              message: "nhập phần trăm chiết khấu ",
                            },
                          ],
                        })(
                          <div>
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Chiết khấu đại lý"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                type="number"
                                placeholder={"Nhập phần trăm chiết khấu"}
                                value={
                                  this.state.objEdit.distributorOffer
                                  // " " +
                                  // this.state.data.variationNameMin
                                }
                                onChange={(e) => {
                                  this.state.objEdit.distributorOffer =
                                    e.target.value;
                                  this.setState({
                                    objEdit: this.state.objEdit,
                                  });
                                }}
                              />
                            </Row>
                          </div>
                        )}
                      </Form.Item>
                   */}
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col
                className="ml-2"
                style={{ background: colors.white }}
                // xs={3}
              >
                <Row className="p-0 m-0 ml-4" style={{ marginLeft: 40 }}>
                  <Col
                    className="pl-0 pr-0 pt-0 m-0 mr-2"
                    // style={{paddingBottom: 40}}
                  >
                    <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                      <ITitle
                        level={2}
                        title="Mô tả sản phẩm"
                        style={{ color: colors.black, fontWeight: "bold" }}
                      />
                    </Row>
                    <Form.Item>
                      {getFieldDecorator("description", {
                        rules: [
                          {
                            message: "nhập mô tả sản phẩm",
                          },
                        ],
                      })(
                        <Row className="p-0 m-0" xs="auto">
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Mô tả"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                className="input-border"
                                loai="textArea"
                                style={{ height: 140, marginTop: 10 }}
                                placeholder="Nhập mô tả sản phẩm"
                                value={
                                  this.state.objEdit.description
                                  // " " +
                                  // this.state.data.variationNameMin
                                }
                                onChange={(e) => {
                                  this.state.objEdit.description =
                                    e.target.value;
                                  this.setState({
                                    objEdit: this.state.objEdit,
                                  });
                                }}
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Form.Item>
                      {getFieldDecorator("element", {
                        rules: [
                          {
                            message: "nhập thành phần sản phẩm",
                          },
                        ],
                      })(
                        <Row
                          className="p-0"
                          style={{
                            marginTop: 20,
                            marginLeft: 0,
                            marginBottom: 0,
                            marginRight: 0,
                          }}
                          xs="auto"
                        >
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Thành phần"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                className="input-border"
                                loai="textArea"
                                style={{ height: 140, marginTop: 10 }}
                                placeholder="Nhập thành phần sản phẩm"
                                value={
                                  this.state.objEdit.element
                                  // " " +
                                  // this.state.data.variationNameMin
                                }
                                onChange={(e) => {
                                  this.state.objEdit.element = e.target.value;
                                  this.setState({
                                    objEdit: this.state.objEdit,
                                  });
                                }}
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Form.Item>
                      {getFieldDecorator("uses", {
                        rules: [
                          {
                            message: "nhập hướng dẫn sử dụng",
                          },
                        ],
                      })(
                        <Row
                          className="p-0"
                          style={{
                            marginTop: 20,
                            marginLeft: 0,
                            marginBottom: 0,
                            marginRight: 0,
                          }}
                          xs="auto"
                        >
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Sử dụng"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                className="input-border"
                                loai="textArea"
                                style={{ marginTop: 10 }}
                                placeholder="Nhập hướng dẫn sử dụng"
                                value={
                                  this.state.objEdit.uses
                                  // " " +
                                  // this.state.data.variationNameMin
                                }
                                onChange={(e) => {
                                  this.state.objEdit.uses = e.target.value;
                                  this.setState({
                                    objEdit: this.state.objEdit,
                                  });
                                }}
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>

                    <Form.Item>
                      {getFieldDecorator("note", {
                        rules: [
                          {
                            message:
                              "nhập lưu ý, NSX, HSD hoặc cách thức bảo quản sản phẩm",
                          },
                        ],
                      })(
                        <Row
                          className="p-0"
                          style={{
                            marginTop: 20,
                            marginLeft: 0,
                            marginBottom: 0,
                            marginRight: 0,
                          }}
                          xs="auto"
                        >
                          <Col className="p-0 m-0">
                            <Row className="p-0 m-0 mb-2">
                              <ITitle
                                level={4}
                                title={"Lưu ý"}
                                style={{ fontWeight: 600 }}
                              />
                            </Row>
                            <Row className="p-0 m-0">
                              <IInput
                                className="input-border"
                                loai="textArea"
                                style={{ height: 75, marginTop: 10 }}
                                placeholder="Nhập lưu ý, NSX, HSD hoặc cách thức bảo quản sản phẩm"
                                value={
                                  this.state.objEdit.note
                                  // " " +
                                  // this.state.data.variationNameMin
                                }
                                onChange={(e) => {
                                  this.state.objEdit.note = e.target.value;
                                  this.setState({
                                    objEdit: this.state.objEdit,
                                  });
                                }}
                              />
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col
                className="pl-0 pr-0 pt-0 m-0 ml-3"
                style={{ background: colors.white }}
                xs="auto"
              >
                <Row className="p-0 mt-4 mb-4  mr-0">
                  <ITitle
                    level={2}
                    title="Hình ảnh sản phẩm"
                    style={{
                      color: colors.black,
                      fontWeight: "bold",
                      marginLeft: 42,
                    }}
                  />
                </Row>

                <Form.Item>
                  {getFieldDecorator("avatar", {
                    rules: [
                      {
                        message: "chọn hình",
                        valuePropName: "fileList",
                        getValueFromEvent: this.normFile,
                      },
                    ],
                  })(
                    <Row
                      className="p-0 mt-4 mb-4  mr-0"
                      style={{ width: 240, marginLeft: 30, marginRight: 40 }}
                    >
                      <IUploadMultiple
                        images={this.state.objEdit.images}
                        url={this.state.objEdit.image_url}
                        callback={(array) => {
                          this.state.arrayImagesAdd = this.state.arrayImagesAdd.concat(
                            array
                          );
                          this.setState({});
                        }}
                        remove={(index) => {
                          this.state.arrayImagesAdd.splice(index, 1);
                          this.setState({
                            // arrayImagesAdd: this.state.arrayImagesAdd
                          });
                        }}
                      />
                      {/* <IUpload
              url=""
              name=""
              type={ProductType.PRODUCT}
              callback={name => {
                //images.push(name)
                //this.setState({})
              }} 
            /> */}
                    </Row>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </div>
        </Form>
      </Container>
    );
  }
}

const editProduct = Form.create({ name: "EditProduct" })(EditProduct);

export default editProduct;
