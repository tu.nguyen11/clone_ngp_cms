import {
  Table,
  Typography,
  Avatar,
  Button,
  List,
  Modal,
  Card,
  message,
} from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IInput,
  IList,
  IUpload,
  IImage,
  ILoading,
} from "../../components";
import { priceFormat } from "../../../utils";

import InfiniteScroll from "react-infinite-scroller";
import { colors } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { APIService } from "../../../services";

export default class DetailProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: {
        product: {},
        images: [],
      },
      write: false,
      visible: false,
      valueStoreProduct: 0,
      nameStoreProduct: "",
      idImg: 0,
      objAddStore: {},

      value: "", // Check here to configure the default column
    };
    this.productId = Number(this.props.match.params.id); // params Product ID
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.onChangeButton = this.onChangeButton.bind(this);
  }

  componentDidMount() {
    this._getAPIDetailProduct(this.productId);
  }
  _getAPIDetailProduct = async (orderId) => {
    try {
      const data = await APIService._getDetailProduct(orderId);

      this.setState({
        data: data.product,
        loading: true,
      });
    } catch (err) {
      console.log(err);
    }
  };

  onSelectChange = (selectedRowKeys) => {
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  setModal2Visible(modal2Visible) {
    this.setState({ modal2Visible });
  }
  onChangeSearch = ({ target: { value } }) => {
    //
    // this.setState({ value });
  };
  onChangeButton = () => {
    this.setState({
      loading: true,
    });

    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 3000);
  };

  pareDataIMG() {
    const arr = this.state.data.images.map((item, index) => {
      const uid = index;
      const name = "jpg/png";
      const url = item;
      return { uid, name, url };
    });
    return arr;
  }

  _APIpostRemoveProduct = (objRemove) => {
    try {
      const data = APIService._postRemoveProduct(objRemove);
      message.success("Xóa thành công.");
      this.props.history.push("/productPage");
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { data } = this.state;

    return (
      <Container fluid>
        <Row sm={12} md={12}>
          <ITitle
            level={1}
            title="Chi tiết sản phẩm"
            style={{ color: colors.main, fontWeight: "bold" }}
          />
        </Row>
        {!this.state.loading ? (
          <ILoading />
        ) : (
          <div>
            <Row
              style={{
                display: "flex",
                justifyContent: "flex-end",
                paddingTop: 32,
                paddingBottom: 32,
              }}
              xs="auto"
              sm={12}
              md={12}
            >
              <IButton
                icon={ISvg.NAME.ARROWUP}
                title="Tạo mới"
                color={colors.main}
                onClick={() => {
                  this.props.history.push("/productCreate");
                }}
                style={{ marginRight: 20 }}
              />
              <IButton
                icon={ISvg.NAME.WRITE}
                title="Chỉnh sửa"
                onClick={() => {
                  this.props.history.push("/productEdit/" + this.productId);
                }}
                color={colors.main}
                style={{ marginRight: 20 }}
              />
              <IButton
                icon={ISvg.NAME.DELETE}
                title="Xóa"
                color={colors.oranges}
                style={{}}
                onClick={() => {
                  const objRemove = {
                    id: [this.productId],
                    status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                  };
                  this._APIpostRemoveProduct(objRemove);
                }}
              />
            </Row>
            <div style={{}}>
              <Row style={{ height: "100%" }} className="p-0" sm={12} md={12}>
                <Col
                  xs={6}
                  className="mr-2 shadow"
                  style={{ background: colors.white }}
                >
                  <Row className="p-0 m-0" style={{}}>
                    <Col className="pl-0 pr-0 pt-0 m-0 ml-4">
                      <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                        <ITitle
                          level={2}
                          title="Thông tin sản phẩm"
                          style={{ color: colors.black, fontWeight: "bold" }}
                        />
                      </Row>

                      <Row className="p-0 m-0 mb-2" xs="auto">
                        <Col className="p-0 m-0">
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Tên sản phẩm"}
                              style={{ fontWeight: 600 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={data.name}
                            />
                          </Row>
                        </Col>
                      </Row>

                      <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                        <Col className="p-0 m-0">
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Mã sản phẩm"}
                              style={{ fontWeight: 600 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={data.productCode}
                            />
                          </Row>
                        </Col>

                        <Col
                          className="p-0"
                          style={{ marginLeft: 28, marginTop: 0 }}
                        >
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Trạng thái"}
                              style={{ fontWeight: 600 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={data.statusName}
                            />
                          </Row>
                        </Col>
                      </Row>

                      <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                        <Col className="p-0 mt-0">
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Ngành hàng"}
                              style={{ fontWeight: 600 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={data.typeName + "/" + data.categoryName}
                            />
                          </Row>
                        </Col>
                      </Row>
                      <Row></Row>
                      <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                        <ITitle
                          level={2}
                          title="Quy cách"
                          style={{ color: colors.black, fontWeight: "bold" }}
                        />
                      </Row>
                      <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
                        <Col className="p-0 m-0">
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Đơn vị lẻ"}
                              style={{ fontWeight: 600 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={data.variationNameMin}
                            />
                          </Row>
                        </Col>

                        <Col
                          className="p-0"
                          style={{ marginLeft: 28, marginTop: 0 }}
                        >
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Đơn vị lớn nhất"}
                              style={{ fontWeight: 600 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={data.variationNameMax}
                            />
                          </Row>
                        </Col>
                      </Row>
                      <Row className="p-0 mt-2 ml-0 mb-4 mr-0" xs="auto">
                        <Col className="p-0 m-0">
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Quy cách đơn vị lẻ"}
                              style={{ fontWeight: 600 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={
                                data.quantityMin + " " + data.variationNameMin
                              }
                            />
                          </Row>
                        </Col>

                        <Col
                          className="p-0"
                          style={{ marginLeft: 28, marginTop: 0 }}
                        >
                          <Row className="p-0 m-0 ">
                            <ITitle
                              level={4}
                              title={"Quy cách đơn vị lớn nhất"}
                              style={{ fontWeight: 600 }}
                            />
                          </Row>
                          <Row className="p-0 m-0">
                            <ITitle
                              style={{ marginTop: 10 }}
                              level={4}
                              title={
                                data.quantityMax + " " + data.variationNameMin
                              }
                            />
                          </Row>
                        </Col>
                      </Row>

                      {/* <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
      <ITitle
        level={2}
        title="Lưu kho"
        style={{ color: colors.black, fontWeight: "bold" }}
      />
    </Row>
    <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
      <Col className="p-0 m-0">
        <Row className="p-0 m-0">
          <ITitle
            level={4}
            title={"Kho lưu trữ"}
            style={{ fontWeight: 600, marginBottom: 12 }}
          />
        </Row>
        {this.state.dataLuuKho.map((item, index) => {
          return (
            <Row className="p-0 mt-1 ml-0 mr-0 mb-0">
              <IInput
                write={this.state.write}
                defaultValue={item.kho}
                style={{ height: 20 }}
              />
            </Row>
          );
        })}
      </Col>

      <Col className="p-0" style={{ marginLeft: 28, marginTop: 0 }}>
        <Row className="p-0 m-0">
          <ITitle
            level={4}
            title={"Trọng lượng thùng/kiện"}
            style={{ fontWeight: 600, marginBottom: 12 }}
          />
        </Row>
        {this.state.dataLuuKho.map((item, index) => {
          return (
            <Row className="p-0 mt-1 ml-0 mr-0 mb-0">
              <IInput
                write={this.state.write}
                defaultValue={item.soluong}
                style={{ height: 20, flex: 1 }}
              />
              {!this.state.write ? null : (
                <div
                  style={{
                    width: 20,
                    height: 20,
                    background: "#FCEFED",
                    borderRadius: 10,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  className="cursor"
                  onClick={() => {
                    let a = this.state.dataLuuKho.splice(index, 1);
                    this.setState({
                      dataLuuKho: this.state.dataLuuKho
                    });
                  }}
                >
                  <ISvg
                    name={ISvg.NAME.CROSS}
                    width={7}
                    height={7}
                    fill={colors.oranges}
                  />
                </div>
              )}
            </Row>
          );
        })}
      </Col>
    </Row>

    {!this.state.write ? null : (
      <Row className="p-0 mt-2 ml-0 mb-0 mr-0" xs="auto">
        <Col className="p-0 m-0">
          <ISelect
            type="write"
            select={this.state.write}
            placeholder="Chọn thêm kho"
            // defaultValue={1}
            data={[
              { key: 1, value: "Kho KhoiDze" },
              { key: 2, value: "Kho KhoiDze1" }
            ]}
            onChange={(key, data) => {
              let value = data.props.children;
              this.setState({
                nameStoreProduct: value
              });
            }}
          />
        </Col>

        <Col
          className="p-0"
          style={{ marginLeft: 28, marginTop: 0 }}
        >
          <Row
            className="p-0 mt-0 ml-0 mr-0 mb-0"
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ISelect
              type="write"
              select={this.state.write}
              style={{ flex: 1 }}
              onChange={(key, data) => {
                let value = data.props.children;
                this.setState({
                  valueStoreProduct: value
                });
              }}
              placeholder="thêm số lượng còn lại"
              data={[
                { key: 1, value: 10000 },
                { key: 2, value: 20000 }
              ]}
            />
            <div
              style={{
                width: 20,
                height: 20,
                background: "#FCEFED",
                borderRadius: 10,
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
              className="cursor"
              onClick={index => {
                let obj = {
                  kho: this.state.nameStoreProduct,
                  soluong: this.state.valueStoreProduct
                };
                this.state.dataLuuKho = [
                  ...this.state.dataLuuKho,
                  obj
                ];
                this.state.nameStoreProduct = "";
                this.state.valueStoreProduct = 0;
                this.setState({});
              }}
            >
              <ISvg
                name={ISvg.NAME.ADD}
                width={7}
                height={7}
                fill={colors.main}
              />
            </div>
          </Row>
        </Col>
      </Row>
    )} */}
                    </Col>
                    {/* <div
                            style={{
                              width: 1,
                              height: '100%',
                              background: 'red',
                              marginLeft: 30,
                            }}
                          ></div> */}
                    <Col className="m-0 p-0 " xs="auto">
                      <div
                        style={{
                          width: 1,
                          height: "100%",
                          background: colors.gray._300,
                          marginLeft: 25,
                        }}
                      ></div>
                    </Col>
                    <Col className="pr-0 pt-0 m-0" xs={3}>
                      <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                        <ITitle
                          level={2}
                          title="Giá bán"
                          style={{ color: colors.black, fontWeight: "bold" }}
                        />
                      </Row>

                      <Row className="p-0 m-0 ">
                        <ITitle
                          level={4}
                          title={"Giá đơn vị lẻ"}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className="p-0 m-0 mb-2">
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={priceFormat(data.price_min) + "đ"}
                        />
                      </Row>
                      {/* <div style={{marginTop: 20, marginBottom: 20}}> */}
                      <Row className="p-0 m-0 ">
                        <ITitle
                          level={4}
                          title={"Giá theo thùng/kiện"}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className="p-0 m-0 mb-2">
                        <ITitle
                          style={{ marginTop: 10 }}
                          level={4}
                          title={priceFormat(data.price_max) + "đ"}
                        />
                      </Row>
                      {/* </div> */}
                      {/* <Row className='p-0 m-0'>
                        <ITitle
                          level={4}
                          title={'Chiết khấu đại lý'}
                          style={{ fontWeight: 600 }}
                        />
                      </Row>
                      <Row className='p-0 m-0 mb-2'>
                        <ITitle
                          level={4}
                          style={{ marginTop: 10 }}
                          title={data.distributorOffer + '%'}
                        />
                      </Row> */}
                    </Col>
                  </Row>
                </Col>
                <Col
                  className="ml-2 shadow"
                  style={{ background: colors.white }}
                  // xs={3}
                >
                  <Row className="p-0 m-0 ml-4" style={{ marginLeft: 40 }}>
                    <Col
                      className="pl-0 pr-0 pt-0 m-0 mr-2"
                      style={{ paddingBottom: 40 }}
                    >
                      <Row className="p-0 mt-4 mb-4 ml-0 mr-0">
                        <ITitle
                          level={2}
                          title="Mô tả sản phẩm"
                          style={{ color: colors.black, fontWeight: "bold" }}
                        />
                      </Row>

                      <Row className="p-0 m-0" xs="auto">
                        <Col className="p-0 m-0">
                          <ITitle
                            level={4}
                            title={"Mô tả"}
                            style={{ fontWeight: 600, marginBottom: 10 }}
                          />
                          <span style={{ wordBreak: "break-word" }}>
                            {data.description}
                          </span>
                        </Col>
                      </Row>

                      <Row
                        className="p-0"
                        style={{
                          marginTop: 20,
                          marginLeft: 0,
                          marginBottom: 0,
                          marginRight: 0,
                        }}
                        xs="auto"
                      >
                        <Col className="p-0 m-0">
                          <ITitle
                            level={4}
                            title={"Thành phần"}
                            style={{ fontWeight: 600, marginBottom: 10 }}
                          />
                          <span style={{ wordBreak: "break-word" }}>
                            {data.element}
                          </span>
                        </Col>
                      </Row>

                      <Row
                        className="p-0"
                        style={{
                          marginTop: 20,
                          marginLeft: 0,
                          marginBottom: 0,
                          marginRight: 0,
                        }}
                        xs="auto"
                      >
                        <Col className="p-0 m-0">
                          <ITitle
                            level={4}
                            title={"Sử dụng"}
                            style={{ fontWeight: 600, marginBottom: 10 }}
                          />

                          <span style={{ wordBreak: "break-word" }}>
                            {data.uses}
                          </span>
                        </Col>
                      </Row>

                      <Row
                        className="p-0"
                        style={{
                          marginTop: 20,
                          marginLeft: 0,
                          marginBottom: 0,
                          marginRight: 0,
                        }}
                        xs="auto"
                      >
                        <Col className="p-0 m-0">
                          <ITitle
                            level={4}
                            title={"Lưu ý"}
                            style={{ fontWeight: 600, marginBottom: 10 }}
                          />
                          <span style={{ wordBreak: "break-word" }}>
                            {data.note}
                          </span>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
                <Col
                  className="pl-0 pr-0 pt-0 m-0 ml-3 shadow"
                  style={{ background: colors.white }}
                  xs="auto"
                >
                  <Row className="p-0 mt-4 mb-4 mr-0">
                    <ITitle
                      level={2}
                      title="Hình ảnh sản phẩm"
                      style={{
                        color: colors.black,
                        fontWeight: "bold",
                        marginLeft: 42,
                      }}
                    />
                  </Row>

                  <Row
                    className="p-0 mt-4 mb-4  mr-0"
                    style={{ width: 240, marginLeft: 30, marginRight: 40 }}
                  >
                    {data.images.map((item, index) => {
                      let url = data.image_url + item;
                      return (
                        <IImage
                          src={url}
                          style={{ width: 100, height: 100, marginRight: 15 }}
                        />
                      );
                    })}
                    {/* <IUpload
              url=""
              name=""
              type={ProductType.PRODUCT}
              callback={name => {
                //images.push(name)
                //this.setState({})
              }}
            /> */}
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
        )}
      </Container>
    );
  }
}
