import { Table, Typography, Avatar, Button, message } from "antd";
import React, { Component } from "react";
import {
  ISvg,
  ISearch,
  IButton,
  ITitle,
  ISelect,
  ITable,
  IImage,
  ICascader,
} from "../../components";
import { colors, images } from "../../../assets";
import { Container, Row, Col } from "reactstrap";
import { priceFormat } from "../../../utils";
import { APIService } from "../../../services";
import { thisExpression } from "@babel/types";
import QueueAnim from "rc-queue-anim";
const widthScreen = window.innerWidth;

function _renderTitle(title) {
  return (
    <div style={{}}>
      <ITitle level={4} title={title} style={{ fontWeight: "bold" }} />
    </div>
  );
}

function _renderColumns(title) {
  return (
    <div style={{ wordWrap: "break-word", wordBreak: "break-word" }}>
      <ITitle level={4} title={title} />
    </div>
  );
}

function _widthColumn(widthScreen, type) {
  // type có nghĩa là width gấp mấy lần so với trung bình widthScreen
  let widthColumn;
  if (widthScreen <= 1170) {
    widthColumn = 1170 / 13;
  } else widthColumn = (widthScreen - 300) / 13;
  return widthColumn * type;
}

export default class ProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      width: 0,
      loading: false,
      bool: false,
      status: 0,
      dataTable: [],
      data: {},
      keySearch: "",
      category_id: 0,
      type: 0,
      loadingTable: true,
      page: 1,
      value: "", // Check here to configure the default column
      arrayRemove: [],
    };
    this.handleResize = this.handleResize.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
  }
  handleResize() {
    console.log(window.innerWidth);
    this.setState({
      width: window.innerWidth,
    });
  }
  componentDidMount() {
    this._getListProduct(
      this.state.page,
      this.state.status,
      this.state.keySearch,
      this.state.category_id,
      this.state.type // caterory cha
    );
    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount() {
    this.handleResize();
    window.removeEventListener("resize", this.handleResize);
  }

  _getListProduct = async (page, status, keySearch, category_id, type) => {
    try {
      const data = await APIService._getListProduct(
        page,
        status,
        keySearch,
        category_id,
        type
      );

      let product = data.product;
      product.map((item, index) => {
        product[index].stt = (this.state.page - 1) * 10 + index + 1;
        product[index].idAndStt = {
          id: item.id,
          index: index,
        };
      });
      this.setState({
        dataTable: product,
        data: data,
        loadingTable: false,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        loadingTable: false,
      });
    }
  };

  onSelectChange = (selectedRowKeys) => {
    let arrayID = [];

    selectedRowKeys.map((item, index) => {
      arrayID.push(this.state.dataTable[item].id);
    });
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys, arrayRemove: arrayID });
  };

  onChangeCategory = (value, selectedOptions) => {
    if (value.length == 1) {
      this.setState(
        {
          type: value[0],
          category_id: 0,
          page: 1,
        },
        () =>
          this._getListProduct(
            this.state.page,
            this.state.status,
            this.state.keySearch,
            this.state.category_id,
            this.state.type // caterory cha
          )
      );
    }
    if (value.length == 2) {
      this.setState(
        {
          type: value[0],
          category_id: value[1],
          page: 1,
        },
        () =>
          this._getListProduct(
            this.state.page,
            this.state.status,
            this.state.keySearch,
            this.state.category_id,
            this.state.type // caterory cha
          )
      );
    }
  };

  _APIpostRemoveProduct = (objRemove) => {
    try {
      const data = APIService._postRemoveProduct(objRemove);
      message.success("Xóa thành công");
      this.setState(
        {
          selectedRowKeys: [],
        },
        () => {
          this._getListProduct(
            this.state.page,
            this.state.status,
            this.state.keySearch,
            this.state.category_id,
            this.state.type // caterory cha
          );
        }
      );
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const columns = [
      {
        title: _renderTitle("STT"),
        dataIndex: "stt",
        key: "stt",
        align: "center",
        width: 60,
        // fixed: "left",

        render: (id) => _renderColumns(id),
      },
      {
        title: _renderTitle("Mã sản phẩm"),
        dataIndex: "code",
        key: "code",
        align: "left",

        render: (code) => _renderColumns(code),
      },
      {
        title: _renderTitle("Tên sản phẩm"),
        dataIndex: "name",
        key: "name",
        align: "left",
        render: (name) => _renderColumns(name),
      },
      {
        title: _renderTitle("Ngành hàng"),
        dataIndex: "cate_name",
        key: "cate_name",
        align: "center",

        render: (cate_name) => <ITitle level={4} title={cate_name} />,
      },
      {
        title: _renderTitle("Nhóm hàng"),
        dataIndex: "sub_cate",
        key: "sub_cate",
        align: "left",

        render: (sub_cate) => <ITitle level={4} title={sub_cate} />,
      },
      {
        title: _renderTitle("Đơn vị"),
        dataIndex: "min_unit",
        key: "min_unit",

        render: (min_unit) => <ITitle level={4} title={min_unit} />,
      },
      {
        title: _renderTitle("Giá đơn vị"),
        dataIndex: "price",
        key: "price",
        align: "right",

        render: (price) => _renderColumns(priceFormat(price) + "đ"),
      },
      {
        title: _renderTitle("Giá thùng/kiện"),
        dataIndex: "price_max_unit",
        key: "price_max_unit",
        align: "right",

        render: (price_max_unit) =>
          _renderColumns(priceFormat(price_max_unit) + "đ"),
      },
      {
        title: _renderTitle("Hình ảnh"),
        dataIndex: "image",
        key: "image",
        align: "center",

        render: (image) => (
          <IImage src={image} style={{ width: 40, height: 40 }} />
        ),
      },
      {
        title: _renderTitle("Số lượng còn"),
        dataIndex: "remain_amount",
        key: "remain_amount",

        render: (remain_amount) => _renderColumns(remain_amount),
      },
      {
        title: _renderTitle("Trạng Thái"),
        dataIndex: "status",
        key: "status",
        align: "center",

        render: (status) => _renderColumns(status == 1 ? "Hiện" : "Ẩn"),
      },
      {
        title: "",
        dataIndex: "idAndStt",
        key: "idAndStt",
        align: "right",
        width: 100,
        fixed: widthScreen <= 1400 ? "right" : "",

        render: (idAndStt) => (
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div
              onClick={(e) => {
                e.stopPropagation();
                this.props.history.push("/productEdit/" + idAndStt.id);
              }} // idAndStt.index vị trí của mảng
              className="cell"
              style={{ width: 50 }}
            >
              <ISvg
                name={ISvg.NAME.WRITE}
                width={20}
                height={20}
                fill={colors.icon.default}
              />
            </div>
            <div
              className="cursor"
              style={{ width: 50 }}
              onClick={(e) => {
                e.stopPropagation();
                this.props.history.push(
                  "/productDetail/" + idAndStt.id,
                  idAndStt.id
                );
              }}
            >
              <ISvg
                name={ISvg.NAME.CHEVRONRIGHT}
                width={11}
                height={20}
                fill={colors.icon.default}
              />
            </div>
          </div>
        ),
      },
    ];

    const { selectedRowKeys, value } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Container fluid>
        <Row className="p-0">
          <ITitle
            level={1}
            title="Sản phẩm"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
            }}
          />
          <ISearch
            onChange={(e) => {
              this.state.keySearch = e.target.value;
              this.setState({
                keySearch: this.state.keySearch,
              });
            }}
            placeholder="Tìm mã sản phẩm , tên sản phẩm..."
            onPressEnter={() =>
              this.setState({ page: 1 }, () =>
                this._getListProduct(
                  this.state.page,
                  this.state.status,
                  this.state.keySearch,
                  this.state.category_id,
                  this.state.type
                )
              )
            }
            icon={
              <div
                style={{
                  display: "flex",
                  width: 42,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                className="cursor"
                onClick={() =>
                  this._getListProduct(
                    this.state.page,
                    this.state.status,
                    this.state.keySearch,
                    this.state.category_id,
                    this.state.type
                  )
                }
                className="cursor"
              >
                <img src={images.icSearch} style={{ width: 20, height: 20 }} />
              </div>
            }
          />
        </Row>

        <Row className="center mt-4" style={{}}>
          <Col
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Row style={{}}>
              <Col className="mt-0  " xs="auto">
                <Row className="center">
                  <Col>
                    <Row>
                      <ISvg
                        name={ISvg.NAME.SVGCheckList}
                        width={20}
                        height={20}
                        fill={colors.icon.default}
                      />
                      <div style={{ marginLeft: 10 }}>
                        <ITitle title="Nhóm sản phẩm" level={4} />
                      </div>
                    </Row>
                  </Col>

                  <Col>
                    <ICascader
                      style={
                        {
                          // background: colors.white,
                          // borderStyle: 'solid',
                          // borderWidth: 1,
                          // borderColor: colors.line,
                        }
                      }
                      type="category"
                      level={2}
                      placeholder="Tất cả"
                      onChange={this.onChangeCategory}
                    />
                  </Col>
                </Row>
              </Col>

              <Col xs="auto" className=" ml-2">
                <Row>
                  <IButton
                    icon={ISvg.NAME.ARROWUP}
                    title="Tạo mới"
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() => this.props.history.push("/productCreate")}
                  />
                  {/* <IButton
                    icon={ISvg.NAME.DOWLOAND}
                    title='Xuất file'
                    color={colors.main}
                    style={{ marginRight: 20 }}
                    onClick={() => {
                      message.warning('Chức năng đang cập nhật')
                    }}
                  /> */}

                  <IButton
                    icon={ISvg.NAME.DELETE}
                    title="Xóa"
                    color={colors.oranges}
                    style={{}}
                    onClick={() => {
                      if (this.state.arrayRemove.length == 0) {
                        message.warning("Bạn chưa chọn mã sản phẩm để xóa.");
                        return;
                      }
                      const objRemove = {
                        id: this.state.arrayRemove,
                        status: -1, // API chưa sử dụng đặt sao cũng đc, -1 là xóa
                      };
                      this._APIpostRemoveProduct(objRemove);
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mt-4">
          <ITable
            data={this.state.dataTable}
            columns={columns}
            loading={this.state.loadingTable}
            style={{ width: "100%" }}
            rowSelection={rowSelection}
            defaultCurrent={this.state.page}
            sizeItem={this.state.data.size}
            indexPage={this.state.page}
            maxpage={this.state.data.total}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  const indexRow = Number(
                    event.currentTarget.attributes[1].ownerElement.dataset
                      .rowKey
                  );
                  const id = this.state.dataTable[indexRow].id;
                  this.props.history.push("/productDetail/" + id);
                }, // click row
              };
            }}
            onChangePage={(page) => {
              this.setState(
                {
                  page: page,
                  loadingTable: true,
                },
                () =>
                  this._getListProduct(
                    this.state.page,
                    this.state.status,
                    this.state.keySearch,
                    this.state.category_id,
                    this.state.type
                  )
              );
            }}
            // scroll={{ x: this.state.width }}
          />
        </Row>
      </Container>
    );
  }
}
