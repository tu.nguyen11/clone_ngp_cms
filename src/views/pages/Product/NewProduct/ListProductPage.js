import React, { useState, useEffect } from "react";
import { Row, Col, Tooltip, message, Modal } from "antd";
import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IButton,
} from "../../../components";
import { colors, images } from "../../../../assets";
import { StyledITitle } from "../../../components/common/Font/font";
import { APIService } from "../../../../services";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { CLEAR_REDUX_PRODUCT } from "../../../store/reducers";

function ListProductPage(props) {
  const history = useHistory();
  const dispatch = useDispatch();

  const [filter, setFilter] = useState({
    vehicleId: 0, // dòng xe
    page: 1,
    status: 2,
    key: "",
    rangeOfVehicleId: 0, // loại xe
    industryId: 0, // ngành hàng
  });

  const [dataTable, setDataTable] = useState({
    listProduct: [],
    total: 1,
    size: 10,
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [dataBranch, setDataBranch] = useState([]);
  const [dataSubCategory, setDataSubCategory] = useState([]);
  const [typeCar, setTypeCar] = useState([]);

  const [loadingGroupProduct1, setLoadingGroupProduct1] = useState(false);
  const [loadingGroupProduct2, setLoadingGroupProduct2] = useState(false);
  const [loadingGroupProduct3, setLoadingGroupProduct3] = useState(false);
  const [loadingTable, setLoadingTable] = useState(true);
  const [isModal, setIsModal] = useState(false);
  const [checkReview, setCheckReview] = useState(1);

  const [dataReview, setDataReview] = useState([
    {
      name: "Hiển thị sản phẩm đã chọn",
      active: true,
      type: 1,
    },
    {
      name: "Ẩn sản phẩm đã chọn",
      active: false,
      type: 2,
    },
  ]);

  const dataStatus = [
    {
      key: 2,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Ẩn",
    },
    {
      key: 1,
      value: "Hiện",
    },
  ];

  const _fetchAPIListProducts = async (filter) => {
    try {
      const data = await APIService._getListAllProduct(filter);
      data.product.map((item, index) => {
        item.stt = (filter.page - 1) * 10 + index + 1;
        item.status_name = item.status === 1 ? "Hiện" : "Ẩn";
        return item;
      });

      setDataTable({
        listProduct: data.product,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const _fetchListBrandCategory = async () => {
    try {
      const data = await APIService._getListBrandCategory();
      let category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      category.unshift({
        key: 0,
        value: "Tất cả",
      });
      setDataBranch(category);
      setLoadingGroupProduct1(false);
    } catch (error) {
      console.log(error);
      setLoadingGroupProduct1(false);
    }
  };

  const _fetchListSubCategpry = async (parent_id) => {
    try {
      const data = await APIService._getListSubCategpry(parent_id);
      let sub_category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      sub_category.unshift({
        key: 0,
        value: "Tất cả",
      });
      setDataSubCategory(sub_category);
      setLoadingGroupProduct2(false);
    } catch (err) {
      console.log(err);
      setLoadingGroupProduct2(false);
    }
  };

  const getListTypeCar = async (product_type_id) => {
    try {
      const data = await APIService._getListNewBrand(product_type_id);
      const newData = data.categories.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });

      newData.unshift({
        key: 0,
        value: "Tất cả",
      });
      setTypeCar(newData);
      setLoadingGroupProduct3(false);
    } catch (err) {
      console.log(err);
      setLoadingGroupProduct3(false);
    }
  };

  const postUpdateStatusProduct = async (obj) => {
    try {
      const data = await APIService._postUpdateStatusProduct(obj);
      setIsModal(false);
      setSelectedRowKeys([]);
      await _fetchAPIListProducts(filter);
      message.success("Duyệt thành công");
    } catch (err) {
      message.error(err);
    }
  };

  useEffect(() => {
    _fetchListBrandCategory();
  }, []);

  useEffect(() => {
    _fetchAPIListProducts(filter);
  }, [filter]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 60,
    },
    {
      title: "Mã sản phẩm",
      dataIndex: "code",
      key: "code",
      render: (code) =>
        !code ? (
          "-"
        ) : (
          <Tooltip title={code}>
            <span>{code}</span>{" "}
          </Tooltip>
        ),
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "name",
      key: "name",
      render: (name) =>
        !name ? (
          "-"
        ) : (
          <Tooltip title={name}>
            <span>{name}</span>
          </Tooltip>
        ),
    },
    {
      title: "Loại xe",
      dataIndex: "industryName",
      key: "industryName",
      render: (industryName) =>
        !industryName ? (
          "-"
        ) : (
          <Tooltip title={industryName}>
            <span>{industryName}</span>
          </Tooltip>
        ),
    },
    {
      title: "Dòng xe",
      dataIndex: "vehicleName",
      key: "vehicleName",
      render: (vehicleName) =>
        !vehicleName ? (
          "-"
        ) : (
          <Tooltip title={vehicleName}>
            <span>{vehicleName}</span>
          </Tooltip>
        ),
    },
    {
      title: "Thương hiệu",
      dataIndex: "rangeOfVehicleName",
      key: "rangeOfVehicleName",
      render: (rangeOfVehicleName) =>
        !rangeOfVehicleName ? (
          "-"
        ) : (
          <Tooltip title={rangeOfVehicleName}>
            <span>{rangeOfVehicleName}</span>
          </Tooltip>
        ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      align: "center",
      render: (status_name) =>
        !status_name ? "-" : <span>{status_name}</span>,
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Danh sách sản phẩm
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên, mã sản phẩm">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên, mã sản phẩm"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={19}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <ITitle title="Lọc theo" level={4} />

                      <div style={{ margin: "0px 15px 0px 15px" }}>
                        <div style={{ width: 180 }}>
                          <ISelect
                            defaultValue="Loại xe"
                            data={dataBranch}
                            select={true}
                            isTooltip={true}
                            loading={loadingGroupProduct1}
                            // value={filter.type}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setFilter({
                                ...filter,
                                industryId: value,
                                vehicleId: 0,
                                rangeOfVehicleId: 0,
                                page: 1,
                              });
                            }}
                            onDropdownVisibleChange={(open) => {
                              if (!open) {
                                if (dataBranch.length === 0) {
                                  return;
                                }
                                setLoadingGroupProduct2(true);
                                _fetchListSubCategpry(filter.type);
                                return;
                              }
                            }}
                          />
                        </div>
                      </div>

                      <div style={{ margin: "0px 15px 0px 0px" }}>
                        <div style={{ width: 200 }}>
                          <ISelect
                            defaultValue="Dòng xe"
                            loading={loadingGroupProduct2}
                            data={dataSubCategory}
                            select={true}
                            isTooltip={true}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setFilter({
                                ...filter,
                                vehicleId: value,
                                rangeOfVehicleId: 0,
                                page: 1,
                              });
                            }}
                            onDropdownVisibleChange={(open) => {
                              if (open) {
                                setLoadingGroupProduct2(true);
                                _fetchListSubCategpry(filter.industryId);
                                return;
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div style={{ width: 200, margin: "0px 15px 0px 0px" }}>
                        <div>
                          <ISelect
                            defaultValue="Thương hiệu"
                            data={typeCar}
                            select={true}
                            isTooltip={true}
                            loading={loadingGroupProduct3}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setFilter({
                                ...filter,
                                rangeOfVehicleId: value,
                                page: 1,
                              });
                            }}
                            onDropdownVisibleChange={(open) => {
                              if (open) {
                                setLoadingGroupProduct3(true);
                                getListTypeCar(filter.vehicleId);
                                return;
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div style={{ margin: "0px 15px 0px 0px" }}>
                        <div>
                          <ISelect
                            defaultValue="Trạng thái"
                            data={dataStatus}
                            select={true}
                            onChange={(value) => {
                              setLoadingTable(true);
                              setFilter({
                                ...filter,
                                status: value,
                                page: 1,
                              });
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col span={5}>
                    <div className="flex justify-end">
                      <IButton
                        icon={ISvg.NAME.CHECK_CIRCLE}
                        title={"Duyệt"}
                        color={colors.main}
                        styleHeight={{
                          width: 120,
                        }}
                        onClick={() => {
                          setIsModal(true);
                        }}
                      />
                      <IButton
                        icon={ISvg.NAME.ARROWUP}
                        title={"Tạo mới"}
                        color={colors.main}
                        styleHeight={{
                          width: 120,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          dispatch(CLEAR_REDUX_PRODUCT());
                          history.push("/product/create/0");
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24} style={{ paddingTop: 0 }}>
                <Row>
                  <ITable
                    columns={columns}
                    data={dataTable.listProduct}
                    style={{ width: "100%" }}
                    defaultCurrent={1}
                    sizeItem={dataTable.size}
                    indexPage={filter.page}
                    size="small"
                    maxpage={dataTable.total}
                    loading={loadingTable}
                    rowSelection={rowSelection}
                    scroll={{ x: 1300 }}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.listProduct[indexRow].id;
                          history.push("/product/detail/" + id);
                        }, // click row
                        onMouseDown: (event) => {
                          const indexRow = Number(
                            event.currentTarget.attributes[1].ownerElement
                              .dataset.rowKey
                          );
                          const id = dataTable.listProduct[indexRow].id;
                          if (event.button == 2 || event == 4) {
                            window.open("/product/detail/" + id, "_blank");
                          }
                        },
                      };
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  />
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Modal
        visible={isModal}
        footer={null}
        width={320}
        onCancel={() => setIsModal(false)}
        centered={true}
        closable={false}
      >
        <div style={{ height: "100%", paddingLeft: 35, paddingRight: 35 }}>
          <Row gutter={[0, 18]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 18 }}>
                  Duyệt sản phẩm
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div>
                {dataReview.map((item, index) => {
                  return (
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        paddingTop: index === 0 ? 0 : 15,
                      }}
                    >
                      <div
                        style={{
                          width: 20,
                          height: 20,
                          border: item.active
                            ? "5px solid" + `${colors.main}`
                            : "1px solid" + `${colors.line_2}`,
                          borderRadius: 10,
                        }}
                        onClick={() => {
                          let data = [...dataReview];
                          data.map((item, index1) => {
                            if (index == index1) {
                              item.active = true;
                            } else {
                              item.active = false;
                            }
                            setCheckReview(data[index].type);
                            setDataReview(data);
                          });
                        }}
                      />
                      <div style={{ flex: 1, marginLeft: 10 }}>
                        <span
                          style={{ fontSize: 14, color: colors.text.black }}
                        >
                          {item.name}
                        </span>
                      </div>
                    </div>
                  );
                })}
              </div>
            </Col>
          </Row>
          <Row
            gutter={[17, 0]}
            style={{
              position: "absolute",
              bottom: -60,
              left: 0,
            }}
          >
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.oranges}
                  title="Hủy bỏ"
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    setIsModal(false);
                  }}
                  icon={ISvg.NAME.CROSS}
                />
              </div>
            </Col>
            <Col span={12}>
              <div style={{ width: 153 }}>
                <IButton
                  color={colors.main}
                  title="Xác nhận"
                  icon={ISvg.NAME.CHECKMARK}
                  styleHeight={{ minWidth: 153 }}
                  onClick={() => {
                    let arrayTmp = [];
                    let arrayShow = [];
                    let arrayHide = [];
                    let arrFinal = [];

                    selectedRowKeys.forEach((item, index) => {
                      arrayTmp.push(dataTable.listProduct[item]);
                    });

                    if (arrayTmp.length === 0) {
                      message.warning("Bạn chưa chọn sản phẩm");
                      return;
                    }

                    arrayTmp.forEach((item) => {
                      if (item.status === 1) {
                        arrayShow.push(item.id);
                      } else {
                        arrayHide.push(item.id);
                      }
                      arrFinal.push(item.id);
                    });

                    if (arrayHide.length > 0 && arrayShow.length > 0) {
                      message.warning(
                        "Vui lòng chọn các sản phẩm có cùng trạng thái"
                      );
                      return;
                    }

                    if (checkReview === 2) {
                      if (arrayHide.length > 0) {
                        message.warning("Các sản phẩm đang ở trạng thái 'Ẩn'");
                        return;
                      } else {
                        const objReview = {
                          status: 0,
                          id: arrFinal,
                        };

                        postUpdateStatusProduct(objReview);
                      }
                    }

                    if (checkReview === 1) {
                      if (arrayShow.length > 0) {
                        message.warning(
                          "Các sản phẩm đang ở trạng thái 'Hiện'"
                        );
                        return;
                      } else {
                        const objReview = {
                          status: 1,
                          id: arrFinal,
                        };

                        postUpdateStatusProduct(objReview);
                      }
                    }
                  }}
                />
              </div>
            </Col>
          </Row>
        </div>
      </Modal>
    </div>
  );
}

export default ListProductPage;
