import React, {useState, useEffect} from "react";
import {Row, Col, Skeleton, Empty, Typography, Icon} from "antd";
import styled from "styled-components";
import {ITitle, ITableHtml, IImage, IButton, ISvg} from "../../../components";
import {colors} from "../../../../assets";
import IUploadMultiple from "../../../components/IUploadMultiple";
import {ImageType} from "../../../../constant";

import {APIService} from "../../../../services";
import {useParams, useHistory} from "react-router-dom";
import {priceFormat} from "../../../../utils";
import {useDispatch, useSelector} from "react-redux";
import {Fancybox} from "../../../components/common";

function DetailProductVersion(props) {
  const {Paragraph, Text} = Typography;
  const params = useParams();
  const {id, idVersion, index} = params;
  const history = useHistory();
  const dataRoot = useSelector((state) => state);
  const {product} = dataRoot;

  const [isLoading, setIsLoading] = useState(true);
  const [dataDetail, setDataDetail] = useState({listSpecification: []});
  const [ellipsis, setEllipsis] = useState(true);

  const getDetailProductVersion = async (idVersion) => {
    try {
      const data = await APIService._getDetailProductVersion(idVersion);

      setDataDetail(data.version);
      setIsLoading(false);
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getDetailProductVersion(idVersion);
  }, []);

  return (
    <div style={{width: "100%", padding: "18px 0px 0px 0px"}}>
      <Row gutter={[12, 50]}>
        <Col span={12}>
          <ITitle
            level={1}
            title={`Chi tiết phiên bản ${dataDetail.versionCode}`}
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
            }}
          />
        </Col>
        {/*<Col span={12}>*/}
        {/*  <div style={{*/}
        {/*    display: 'flex',*/}
        {/*    alignItems: 'center',*/}
        {/*    justifyContent: 'flex-end'*/}
        {/*  }}>*/}
        {/*      <IButton*/}
        {/*      color={colors.main}*/}
        {/*      title={'Chỉnh sửa'}*/}
        {/*      icon={ISvg.NAME.WRITE}*/}
        {/*      onClick={()=>{*/}
        {/*        props.history.push(`/product/edit/${id}/version/${idVersion}/${index}`)*/}
        {/*      }}*/}
        {/*      />*/}

        {/*  </div>*/}
        {/*</Col>*/}
        <Col span={24}>
          <Row gutter={[12, 20]}>
            <Col span={24}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                }}
              >
                <Row gutter={[25, 0]} style={{height: "100%"}} type="flex">
                  <Col span={18} xxl={15}>
                    <div
                      style={{
                        background: colors.white,
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        height: "100%",
                      }}
                    >
                      {isLoading ? (
                        <div style={{padding: "30px 40px"}}>
                          <Skeleton
                            loading={true}
                            active
                            paragraph={{rows: 16}}
                          />
                        </div>
                      ) : (
                        <Row type="flex">
                          <Col span={15} xxl={16}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                borderRight: "1px solid #f0f0f0",
                                height: "100%",
                              }}
                            >
                              <Row gutter={[0, 20]}>
                                <Col>
                                  <ITitle
                                    level={3}
                                    title="Thông tin phiên bản"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col span={24}>
                                  <Row gutter={[12, 0]}>
                                    <Col span={12}>
                                      <ITitle
                                        level={4}
                                        title={"Mã phiên bản"}
                                        style={{
                                          fontWeight: 600,
                                        }}
                                      />
                                      <span
                                        style={{
                                          wordWrap: "break-word",
                                          wordBreak: "break-all",
                                        }}
                                      >
                                        {!dataDetail.versionCode
                                          ? "-"
                                          : dataDetail.versionCode}
                                      </span>
                                    </Col>
                                    <Col span={12}>
                                      <ITitle
                                        level={4}
                                        title={"Năm sản xuất"}
                                        style={{
                                          fontWeight: 600,
                                        }}
                                      />
                                      <span
                                        style={{
                                          wordWrap: "break-word",
                                        }}
                                      >
                                        {!dataDetail.year_of_manufacture
                                          ? "-"
                                          : dataDetail.year_of_manufacture}
                                      </span>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col span={12}>
                                  <ITitle
                                    level={4}
                                    title={"Màu sắc"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  <span
                                    style={{
                                      wordWrap: "break-word",
                                    }}
                                  >
                                    {!dataDetail.color_name
                                      ? "-"
                                      : dataDetail.color_name}
                                  </span>
                                </Col>

                                <Col span={24}>
                                  <ITitle
                                    level={4}
                                    title={"Ghi chú"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />

                                  <pre
                                    style={{
                                      wordWrap: "break-word",
                                    }}
                                  >
                                    {!dataDetail.note ? "-" : dataDetail.note}
                                  </pre>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col span={9} xxl={8}>
                            <div
                              style={{
                                padding: "30px 40px",
                                background: colors.white,
                                width: "100%",
                                height: 500,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[0, 20]}>
                                <Col span={24}>
                                  <ITitle
                                    level={3}
                                    title="Đơn vị sản phẩm"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col span={24}>
                                  <ITitle
                                    level={4}
                                    title={"Loại thùng"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  <span>
                                    {!dataDetail.type_bin
                                      ? "-"
                                      : dataDetail.type_bin}
                                  </span>
                                </Col>
                                <Col span={24}>
                                  <ITitle
                                    level={4}
                                    title={"Giá công bố"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  {!dataDetail.price ? (
                                    <span>0 VNĐ</span>
                                  ) : (
                                    <span>
                                      {priceFormat(dataDetail.price) + " VNĐ"}
                                    </span>
                                  )}
                                </Col>
                                <Col span={24}>
                                  <ITitle
                                    level={4}
                                    title={"Giá thùng phiếu"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  <span>
                                    {!dataDetail.price_package ? (
                                      <span>0 VNĐ</span>
                                    ) : (
                                      <span>
                                        {priceFormat(dataDetail.price_package) +
                                        " VNĐ"}
                                      </span>
                                    )}
                                  </span>
                                </Col>
                                <Col span={24}>
                                  <ITitle
                                    level={4}
                                    title={"Đăng ký đăng kiểm"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  <span>
                                    {!dataDetail.price_register ? (
                                      <span>0 VNĐ</span>
                                    ) : (
                                      <span>
                                        {priceFormat(
                                          dataDetail.price_register
                                        ) + " VNĐ"}
                                      </span>
                                    )}
                                  </span>
                                </Col>
                                <Col span={24}>
                                  <ITitle
                                    level={4}
                                    title={"Khuyến mãi"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  <span>
                                    {!dataDetail.gia_khuyen_mai ? (
                                      <span>0 VNĐ</span>
                                    ) : (
                                      <span>
                                        {priceFormat(
                                          dataDetail.gia_khuyen_mai
                                        ) + " VNĐ"}
                                      </span>
                                    )}
                                  </span>
                                </Col>
                                <Col span={24}>
                                  <ITitle
                                    level={4}
                                    title={"Tổng cộng"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  {!dataDetail.total ? (
                                    <span>0 VNĐ</span>
                                  ) : (
                                    <span>
                                      {priceFormat(dataDetail.total) + " VNĐ"}
                                    </span>
                                  )}
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                      )}
                    </div>
                  </Col>
                  <Col span={4}>
                    <div
                      style={{
                        padding: "30px 30px",
                        background: "white",
                        height: "100%",
                        width: 290,
                        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      }}
                    >
                      <Row gutter={[0, 20]}>
                        <Col span={24}>
                          <ITitle
                            level={3}
                            title="Hình ảnh sản phẩm"
                            style={{
                              color: colors.black,
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col span={24}>
                          {!dataDetail.image
                            ? null
                            : (
                              <Fancybox>
                                <p>
                                  {dataDetail.image.map((item, index) => {
                                    let url = dataDetail.image_url + item;
                                    return (
                                      <a
                                        key={index}
                                        data-fancybox="gallery"
                                        href={url}
                                      >
                                        <IImage
                                          src={url}
                                          style={{
                                            width: 100,
                                            height: 100,
                                            marginRight: 15,
                                          }}
                                        />
                                      </a>
                                    );
                                  })}
                                </p>
                              </Fancybox>
                            )}
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}

export default DetailProductVersion;
