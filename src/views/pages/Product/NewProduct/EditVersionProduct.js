import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton, message, Form, Modal } from "antd";
import { ITitle, IButton, ISvg, ISelect, IInput } from "../../../components";
import { colors } from "../../../../assets";
import IUploadMultiple from "../../../components/IUploadMultiple";
import { ImageType } from "../../../../constant";
import { priceFormat } from "../../../../utils";
import { APIService } from "../../../../services";
import { useParams, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  IFormItem,
  IInputText,
  IInputNumber,
} from "../../../components/common";
import {
  CREATE_PRODUCT_VERSION_INFO,
  CLEAR_REDUX_PRODUCT_VERSION,
  SORT_LIST_VERSION,
  CHECK_NULL_VALUE_LIST_SPECIFICATION,
} from "../../../store/reducers";

function EditVersionProduct(props) {
  const params = useParams();
  const { index, type, idVersion } = params;
  const history = useHistory();
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);
  const { product } = dataRoot;
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;

  const [isLoading, setIsLoading] = useState(true);
  const [dataDetail, setDataDetail] = useState({ images: [] });
  const [colorArr, setColorArr] = useState([]);

  const [show, setShow] = useState([
    { key: 1, value: "Hiện", active: 1 },
    { key: 0, value: "Ẩn", active: 0 },
  ]);

  const getListColor = async () => {
    try {
      const data = await APIService._getListColorProductVersion();
      let colors = data.colors.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });

      setColorArr(colors);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getListColor();
    let arrShowTmp = JSON.parse(JSON.stringify(show));

    reduxForm.setFieldsValue(
      {
        product_version_code:
          product.arrProductVersion[index].productVersionCode,
        product_version_description:
          product.arrProductVersion[index].productVersionDescription,
        publishedPrice: product.arrProductVersion[index].publishedPrice,
        boxPrice: product.arrProductVersion[index].boxPrice,
        registrationPrice: product.arrProductVersion[index].registrationPrice,
        totalPrice: product.arrProductVersion[index].totalPrice,
        price_promotion: product.arrProductVersion[index].price_promotion
      },
      () => setIsLoading(false)
    );
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields((err, values) => {
      if (err) {
        return;
      } else {
        if (
          !product.arrProductVersion[index].arrayImages ||
          product.arrProductVersion[index].arrayImages.length === 0
        ) {
          Modal.error({
            title: "Thông báo",
            content: "Bạn chưa nhập ảnh cho phiên bản sản phẩm",
          });
          return;
        }

        let arrVersion = [...product.arrProductVersion];
        arrVersion.splice(index, 1);

        let versionExist = arrVersion.find((version) => {
          return (
            version.colorID === product.arrProductVersion[index].colorID &&
            version.colorName.toLowerCase() ===
              product.arrProductVersion[index].colorName.toLowerCase() &&
            version.barrelType.toLowerCase() ===
              product.arrProductVersion[index].barrelType.toLowerCase() &&
            version.year === product.arrProductVersion[index].year
          );
        });

        if (versionExist) {
          message.warning("Thông tin phiên bản đã tồn tại");
          return;
        }

        if (product.arrProductVersion[index].totalPrice < 0) {
          message.warning("Giá tổng cộng không được âm");
          return;
        }
        history.goBack();
      }
    });
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Form onSubmit={handleSubmit}>
        <Row gutter={[12, 16]}>
          <Col>
            <Row gutter={[12, 20]}>
              <Col span={12}>
                <ITitle
                  level={1}
                  title={
                    !product.arrProductVersion[index].productVersionCode ||
                    product.arrProductVersion[index].productVersionCode === ""
                      ? "Tạo mới phiên bản"
                      : `Chỉnh sửa phiên bản ${product.arrProductVersion[index].productVersionCode}`
                  }
                  style={{
                    color: colors.mainDark,
                    fontWeight: "bold",
                  }}
                />
              </Col>
              <Col span={12}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-end",
                  }}
                >
                  <IButton
                    icon={ISvg.NAME.SAVE}
                    title="Lưu"
                    htmlType="submit"
                    color={colors.main}
                  />
                  <IButton
                    icon={ISvg.NAME.CROSS}
                    title="Huỷ"
                    styleHeight={{ marginLeft: 15 }}
                    color={colors.red}
                    onClick={() => {
                      if (type === "create") {
                        const obj = {
                          publishedPrice: 0,
                          boxPrice: 0,
                          registrationPrice: 0,
                          totalPrice: 0,
                          price_promotion :0
                        };

                        const data = {
                          keyRoot: "arrProductVersion",
                          idx: index,
                          value: obj,
                        };

                        dispatch(CLEAR_REDUX_PRODUCT_VERSION(data));
                      }
                      history.goBack();
                    }}
                  />
                </div>
              </Col>
              <Col span={24}>
                <div
                  style={{
                    marginTop:40,
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <Row gutter={[15, 0]} style={{ height: "100%" }} type="flex">
                    <Col span={18} xxl={15}>
                      <div
                        style={{
                          background: colors.white,
                          boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        }}
                      >
                        {/* {isLoading ? (
                          <div style={{ padding: "30px 40px" }}>
                            <Skeleton
                              loading={true}
                              active
                              paragraph={{ rows: 16 }}
                            />
                          </div>
                        ) : ( */}
                        <Row>
                          <Col span={15} xxl={16}>
                            <div
                              style={{
                                padding: "25px 30px",
                                background: colors.white,
                                height: 500,
                                borderRight: "1px solid #f0f0f0",
                              }}
                            >
                              <Row gutter={[0, 20]}>
                                <Col style={{ paddingTop: 0 }}>
                                  <ITitle
                                    level={3}
                                    title="Thông tin phiên bản"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col span={24}>
                                  <Row gutter={[12, 0]}>
                                    <Col span={12}>
                                      <IFormItem>
                                        {getFieldDecorator(
                                          "product_version_code",
                                          {
                                            rules: [
                                              {
                                                required: true,
                                                message:
                                                  "Vui lòng mã phiên bản",
                                              },
                                            ],
                                          }
                                        )(
                                          <div>
                                            <Row>
                                              <div style={{ display: "flex" }}>
                                                <ITitle
                                                  level={4}
                                                  title={"Mã phiên bản"}
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                />
                                                <span
                                                  style={{
                                                    color: "red",
                                                    marginLeft: 4,
                                                  }}
                                                >
                                                  (*)
                                                </span>
                                              </div>
                                              <IInputText
                                                style={{ color: "#000" }}
                                                placeholder="Nhập mã phiên bản"
                                                onChange={(e) => {
                                                  const data = {
                                                    keyRoot:
                                                      "arrProductVersion",
                                                    key: "productVersionCode",
                                                    idx: index,
                                                    value:
                                                      e.target.value.replace(
                                                        /\s/g,
                                                        ""
                                                      ),
                                                  };
                                                  dispatch(
                                                    CREATE_PRODUCT_VERSION_INFO(
                                                      data
                                                    )
                                                  );
                                                }}
                                                value={
                                                  !product.arrProductVersion[
                                                    index
                                                  ].productVersionCode
                                                    ? undefined
                                                    : product.arrProductVersion[
                                                        index
                                                      ].productVersionCode
                                                }
                                              />
                                            </Row>
                                          </div>
                                        )}
                                      </IFormItem>
                                    </Col>
                                    <Col span={12}>
                                      <IFormItem>
                                        {getFieldDecorator("year", {
                                          rules: [
                                            {
                                              required: false,
                                              message:
                                                "Vui lòng nhập năm sản xuất",
                                            },
                                          ],
                                        })(
                                          <div>
                                            <Row>
                                              <div style={{ display: "flex" }}>
                                                <ITitle
                                                  level={4}
                                                  title={"Năm sản xuất"}
                                                  style={{
                                                    fontWeight: 600,
                                                  }}
                                                />
                                                <span
                                                  style={{
                                                    color: "red",
                                                    marginLeft: 4,
                                                  }}
                                                >
                                                  (*)
                                                </span>
                                              </div>
                                              <IInputText
                                                style={{ color: "#000" }}
                                                placeholder="Nhập năm sản xuất"
                                                onChange={(e) => {
                                                  const data = {
                                                    keyRoot:
                                                      "arrProductVersion",
                                                    key: "year",
                                                    idx: index,
                                                    value: e.target.value,
                                                  };
                                                  dispatch(
                                                    CREATE_PRODUCT_VERSION_INFO(
                                                      data
                                                    )
                                                  );
                                                }}
                                                value={
                                                  !product.arrProductVersion[
                                                    index
                                                  ].year
                                                    ? undefined
                                                    : product.arrProductVersion[
                                                        index
                                                      ].year
                                                }
                                              />
                                            </Row>
                                          </div>
                                        )}
                                      </IFormItem>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col span={12}>
                                  <IFormItem>
                                    {getFieldDecorator("colorID", {
                                      rules: [
                                        {
                                          required: false,
                                          message: "Vui lòng chọn màu sắc",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <div style={{ display: "flex" }}>
                                            <ITitle
                                              level={4}
                                              title={"Màu sắc"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                            <span
                                              style={{
                                                color: "red",
                                                marginLeft: 4,
                                              }}
                                            >
                                              (*)
                                            </span>
                                          </div>
                                          <ISelect
                                            data={colorArr}
                                            select={true}
                                            isBackground={false}
                                            style={{
                                              marginTop: 1,
                                            }}
                                            placeholder="Chọn màu sắc"
                                            onChange={(key, option) => {
                                              const data = {
                                                keyRoot: "arrProductVersion",
                                                key: "colorID",
                                                idx: index,
                                                value: Number(key),
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data
                                                )
                                              );

                                              const data1 = {
                                                keyRoot: "arrProductVersion",
                                                key: "colorName",
                                                idx: index,
                                                value: option.props.children,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data1
                                                )
                                              );
                                            }}
                                            value={
                                              !product.arrProductVersion[index]
                                                .colorID
                                                ? undefined
                                                : product.arrProductVersion[
                                                    index
                                                  ].colorID
                                            }
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                                <Col span={24}>
                                  <IFormItem>
                                    {getFieldDecorator(
                                      "product_version_description",
                                    )(
                                      <div>
                                        <Row>
                                          <div
                                            style={{
                                              display: "flex",
                                            }}
                                          >
                                            <ITitle
                                              level={4}
                                              title={"Ghi chú"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                          </div>
                                          <IInput
                                            style={{
                                              color: "#000",
                                              marginTop: 12,
                                              minHeight: 80,
                                              maxHeight: 250,
                                            }}
                                            className="input-border"
                                            loai="textArea"
                                            placeholder="Nhập ghi chú"
                                            onChange={(e) => {
                                              const data = {
                                                keyRoot: "arrProductVersion",
                                                key: "productVersionDescription",
                                                idx: index,
                                                value: e.target.value,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data
                                                )
                                              );
                                            }}
                                            value={
                                              !product.arrProductVersion[index]
                                                .productVersionDescription
                                                ? undefined
                                                : product.arrProductVersion[
                                                    index
                                                  ].productVersionDescription
                                            }
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                          <Col span={9} xxl={8}>
                            <div
                              style={{
                                padding: "25px 30px",
                                background: colors.white,
                                width: "100%",
                                height: "100%",
                              }}
                            >
                              <Row gutter={[12, 20]}>
                                <Col span={24} style={{ paddingTop: 0 }}>
                                  <ITitle
                                    level={3}
                                    title="Đơn vị phiên bản"
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  />
                                </Col>
                                <Col span={24}>
                                  <IFormItem>
                                    {getFieldDecorator("barrelType", {
                                      rules: [
                                        {
                                          required: false,
                                          message: "Vui lòng nhập loại thùng",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <div style={{ display: "flex" }}>
                                            <ITitle
                                              level={4}
                                              title={"Loại thùng"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                            <span
                                              style={{
                                                color: "red",
                                                marginLeft: 4,
                                              }}
                                            >
                                              (*)
                                            </span>
                                          </div>
                                          <IInputText
                                            style={{ color: "#000" }}
                                            placeholder="Nhập loại thùng"
                                            onChange={(e) => {
                                              const data = {
                                                keyRoot: "arrProductVersion",
                                                key: "barrelType",
                                                idx: index,
                                                value: e.target.value,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data
                                                )
                                              );
                                            }}
                                            value={
                                              !product.arrProductVersion[index]
                                                .barrelType
                                                ? undefined
                                                : product.arrProductVersion[
                                                    index
                                                  ].barrelType
                                            }
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                                <Col span={24}>
                                  <IFormItem>
                                    {getFieldDecorator("publishedPrice", {
                                      rules: [
                                        {
                                          required: true,
                                          message: "Vui lòng nhập giá công bố",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <div style={{ display: "flex" }}>
                                            <ITitle
                                              level={4}
                                              title={"Giá công bố"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                            <span
                                              style={{
                                                color: "red",
                                                marginLeft: 4,
                                              }}
                                            >
                                              (*)
                                            </span>
                                          </div>
                                          <IInputNumber
                                            suffix="VNĐ"
                                            min={0}
                                            style={{ color: "#000" }}
                                            placeholder="Nhập giá công bố"
                                            style={{ width: "100%" }}
                                            formatter={(value) =>
                                              `${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              )
                                            }
                                            parser={(value) =>
                                              value.replace(/\$\s?|(,*)/g, "")
                                            }
                                            onChange={(price) => {
                                              const data = {
                                                keyRoot: "arrProductVersion",
                                                key: "publishedPrice",
                                                idx: index,
                                                value: price,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data
                                                )
                                              );

                                              const data1 = {
                                                keyRoot: "arrProductVersion",
                                                key: "totalPrice",
                                                idx: index,
                                                value:
                                                  price +
                                                  product.arrProductVersion[
                                                    index
                                                  ].boxPrice +
                                                  product.arrProductVersion[
                                                    index
                                                  ].registrationPrice - product.arrProductVersion[
                                                    index
                                                    ].price_promotion ,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data1
                                                )
                                              );
                                            }}
                                            value={
                                              product.arrProductVersion[index]
                                                .publishedPrice ||
                                              product.arrProductVersion[index]
                                                .publishedPrice >= 0
                                                ? product.arrProductVersion[
                                                    index
                                                  ].publishedPrice
                                                : undefined
                                            }
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                                <Col span={24}>
                                  <IFormItem>
                                    {getFieldDecorator("boxPrice", {
                                      rules: [
                                        {
                                          required: true,
                                          message:
                                            "Vui lòng nhập giá thùng phiếu",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <div style={{ display: "flex" }}>
                                            <ITitle
                                              level={4}
                                              title={"Giá thùng phiếu"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                            <span
                                              style={{
                                                color: "red",
                                                marginLeft: 4,
                                              }}
                                            >
                                              (*)
                                            </span>
                                          </div>
                                          <IInputNumber
                                            suffix="VNĐ"
                                            min={0}
                                            style={{ color: "#000" }}
                                            placeholder="Nhập giá thùng phiếu"
                                            style={{ width: "100%" }}
                                            formatter={(value) =>
                                              `${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              )
                                            }
                                            parser={(value) =>
                                              value.replace(/\$\s?|(,*)/g, "")
                                            }
                                            onChange={(price) => {
                                              const data = {
                                                keyRoot: "arrProductVersion",
                                                key: "boxPrice",
                                                idx: index,
                                                value: price,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data
                                                )
                                              );

                                              const data1 = {
                                                keyRoot: "arrProductVersion",
                                                key: "totalPrice",
                                                idx: index,
                                                value:
                                                  price +
                                                  product.arrProductVersion[
                                                    index
                                                  ].publishedPrice +
                                                  product.arrProductVersion[
                                                    index
                                                  ].registrationPrice - product.arrProductVersion[
                                                    index
                                                    ].price_promotion,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data1
                                                )
                                              );
                                            }}
                                            value={
                                              product.arrProductVersion[index]
                                                .boxPrice ||
                                              product.arrProductVersion[index]
                                                .boxPrice >= 0
                                                ? product.arrProductVersion[
                                                    index
                                                  ].boxPrice
                                                : undefined
                                            }
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                                <Col span={24}>
                                  <IFormItem>
                                    {getFieldDecorator("registrationPrice", {
                                      rules: [
                                        {
                                          required: true,
                                          message:
                                            "Vui lòng nhập giá đăng ký đăng kiểm",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <div style={{ display: "flex" }}>
                                            <ITitle
                                              level={4}
                                              title={"Đăng ký đăng kiểm"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                            <span
                                              style={{
                                                color: "red",
                                                marginLeft: 4,
                                              }}
                                            >
                                              (*)
                                            </span>
                                          </div>
                                          <IInputNumber
                                            suffix="VNĐ"
                                            min={0}
                                            style={{ color: "#000" }}
                                            placeholder="Nhập giá đăng ký đăng kiểm"
                                            style={{ width: "100%" }}
                                            formatter={(value) =>
                                              `${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              )
                                            }
                                            parser={(value) =>
                                              value.replace(/\$\s?|(,*)/g, "")
                                            }
                                            onChange={(price) => {
                                              const data = {
                                                keyRoot: "arrProductVersion",
                                                key: "registrationPrice",
                                                idx: index,
                                                value: price,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data
                                                )
                                              );

                                              const data1 = {
                                                keyRoot: "arrProductVersion",
                                                key: "totalPrice",
                                                idx: index,
                                                value:
                                                  price +
                                                  product.arrProductVersion[
                                                    index
                                                  ].publishedPrice +
                                                  product.arrProductVersion[
                                                    index
                                                  ].boxPrice - product.arrProductVersion[
                                                    index
                                                    ].price_promotion ,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data1
                                                )
                                              );
                                            }}
                                            value={
                                              product.arrProductVersion[index]
                                                .registrationPrice ||
                                              product.arrProductVersion[index]
                                                .registrationPrice >= 0
                                                ? product.arrProductVersion[
                                                    index
                                                  ].registrationPrice
                                                : undefined
                                            }
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                                <Col span={24}>
                                  <IFormItem>
                                    {getFieldDecorator("price_promotion", {
                                      rules: [
                                        {
                                          required: true,
                                          message:
                                            "Vui lòng nhập giá khuyến mãi",
                                        },
                                      ],
                                    })(
                                      <div>
                                        <Row>
                                          <div style={{ display: "flex" }}>
                                            <ITitle
                                              level={4}
                                              title={"Khuyến mãi"}
                                              style={{
                                                fontWeight: 600,
                                              }}
                                            />
                                            <span
                                              style={{
                                                color: "red",
                                                marginLeft: 4,
                                              }}
                                            >
                                              (*)
                                            </span>
                                          </div>
                                          <IInputNumber
                                            suffix="VNĐ"
                                            min={0}
                                            style={{ color: "#000" }}
                                            placeholder="Nhập giá khuyến mãi"
                                            style={{ width: "100%" }}
                                            formatter={(value) =>
                                              `${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              )
                                            }
                                            parser={(value) =>
                                              value.replace(/\$\s?|(,*)/g, "")
                                            }
                                            onChange={(price) => {
                                              const data = {
                                                keyRoot: "arrProductVersion",
                                                key: "price_promotion",
                                                idx: index,
                                                value: price,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data
                                                )
                                              );

                                              const data1 = {
                                                keyRoot: "arrProductVersion",
                                                key: "totalPrice",
                                                idx: index,
                                                value:
                                                  product.arrProductVersion[
                                                    index
                                                    ].publishedPrice +
                                                  product.arrProductVersion[
                                                    index
                                                    ].boxPrice +
                                                  product.arrProductVersion[
                                                    index
                                                    ].registrationPrice - price,
                                              };
                                              dispatch(
                                                CREATE_PRODUCT_VERSION_INFO(
                                                  data1
                                                )
                                              );
                                            }}
                                            value={
                                              product.arrProductVersion[index]
                                                .price_promotion ||
                                              product.arrProductVersion[index]
                                                .price_promotion >= 0
                                                ? product.arrProductVersion[
                                                  index
                                                  ].price_promotion
                                                : undefined
                                            }
                                          />
                                        </Row>
                                      </div>
                                    )}
                                  </IFormItem>
                                </Col>
                                <Col span={24}>
                                  <ITitle
                                    level={4}
                                    title={"Tổng cộng"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  <div
                                    style={{
                                      display: "flex",
                                      justifyContent: "space-between",
                                      color: colors.blackChart,
                                      marginTop: 6,
                                    }}
                                  >
                                    <div style={{ paddingRight: 40 }}>
                                      {!product.arrProductVersion[index]
                                        .totalPrice
                                        ? 0
                                        : priceFormat(
                                            product.arrProductVersion[index]
                                              .totalPrice
                                          )}
                                    </div>
                                    <div>VNĐ</div>
                                  </div>
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                        {/* )} */}
                      </div>
                    </Col>
                    <Col span={4}>
                      <div
                        style={{
                          padding: "25px 30px",
                          background: "white",
                          height: "100%",
                          width: 284,
                          boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                        }}
                      >
                        <Row gutter={[0, 20]}>
                          <Col span={24} style={{ paddingTop: 0 }}>
                            <ITitle
                              level={3}
                              title="Hình ảnh phiên bản"
                              style={{
                                color: colors.black,
                                fontWeight: "bold",
                              }}
                            />
                          </Col>
                          <Col span={24}>
                            <IFormItem>
                              {getFieldDecorator("img", {
                                rules: [
                                  {
                                    required: false,
                                    message: "chọn hình",
                                    valuePropName: "fileList",
                                  },
                                ],
                              })(
                                <IUploadMultiple
                                  images={
                                    !product.arrProductVersion[index]
                                      .arrayImages
                                      ? []
                                      : product.arrProductVersion[index]
                                          .arrayImages
                                  }
                                  url={
                                    product.arrProductVersion[index].urlImages
                                  }
                                  width={20}
                                  type={ImageType.PRODUCT}
                                  callback={(array, url) => {
                                    let arrayImagesAddTmp = !product
                                      .arrProductVersion[index].arrayImages
                                      ? []
                                      : JSON.parse(
                                          JSON.stringify(
                                            product.arrProductVersion[index]
                                              .arrayImages
                                          )
                                        );
                                    if (arrayImagesAddTmp.length === 4) {
                                      message.warning(
                                        "Bạn chỉ được nhập tối đa 4 hình ảnh"
                                      );
                                      return;
                                    }
                                    let newArrayImagesAdd =
                                      arrayImagesAddTmp.concat(array);
                                    const data = {
                                      keyRoot: "arrProductVersion",
                                      key: "arrayImages",
                                      idx: index,
                                      value: newArrayImagesAdd,
                                    };
                                    dispatch(CREATE_PRODUCT_VERSION_INFO(data));

                                    const data1 = {
                                      keyRoot: "arrProductVersion",
                                      key: "urlImages",
                                      idx: index,
                                      value: url,
                                    };
                                    dispatch(
                                      CREATE_PRODUCT_VERSION_INFO(data1)
                                    );
                                  }}
                                  remove={(index1) => {
                                    let newArrayImagesRemove = JSON.parse(
                                      JSON.stringify(
                                        product.arrProductVersion[index]
                                          .arrayImages
                                      )
                                    );
                                    newArrayImagesRemove.splice(index1, 1);
                                    const data = {
                                      keyRoot: "arrProductVersion",
                                      key: "arrayImages",
                                      idx: index,
                                      value: newArrayImagesRemove,
                                    };
                                    dispatch(CREATE_PRODUCT_VERSION_INFO(data));
                                  }}
                                />
                              )}
                            </IFormItem>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const editVersionProduct = Form.create({ name: "EditVersionProduct" })(
  EditVersionProduct
);

export default editVersionProduct;
