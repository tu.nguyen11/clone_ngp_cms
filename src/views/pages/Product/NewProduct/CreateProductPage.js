import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton, message, Form, Modal, Tooltip, Empty } from "antd";
import { ITitle, IButton, ISvg, ISelect, ITable } from "../../../components";
import { IFormItem, IInputText } from "../../../components/common";
import { colors } from "../../../../assets";
import { priceFormat } from "../../../../utils";
import useWindowSize from "../../../../utils/useWindowSize";

import { APIService } from "../../../../services";
import { useHistory, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  CREATE_PRODUCT_INFO,
  CLEAR_REDUX_PRODUCT,
  CREATE_PRODUCT_VERSION,
} from "../../../store/reducers";

function CreateProductPage(props) {
  let size = useWindowSize();
  const history = useHistory();
  const params = useParams();
  const { getFieldDecorator } = props.form;
  const reduxForm = props.form;
  const dispatch = useDispatch();
  const dataRoot = useSelector((state) => state);

  const { product } = dataRoot;
  const add = params.type === "create";
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingButton, setIsLoadingButton] = useState(false);
  const [loadingImport, setLoadingImport] = useState(false);

  const [loadingCate, setLoadingCate] = useState(false);
  const [loadingSubCate, setLoadingSubCate] = useState(false);
  const [loadingOutStandingBrand, setLoadingOutStandingBrand] = useState(false);
  const [loadingNewSubCate, setLoadingNewSubCate] = useState(false);

  const [cate, setCate] = useState([]); // nganh hang
  const [subCate, setSubCate] = useState([]); // dòng xe
  const [newSubCate, setNewSubCate] = useState([]); // loại xe
  const [outStandingBrand, setOutStandingBrand] = useState([]); // thương hiệu
  const [colorArr, setColorArr] = useState([]);
  const [objVersionTmp, setObjVersionTmp] = useState({});

  //Nganh hang
  const getListBrandCategory = async (productTypeId) => {
    try {
      setLoadingCate(true);
      const data = await APIService._getListBrandCategory(productTypeId);
      const newData = data.categories.map((item) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      setCate(newData);
      setLoadingCate(false);
    } catch (err) {
      console.log(err);
      setLoadingCate(false);
    }
  };

  //Dòng xe
  const getListSubCate = async (parent_id) => {
    try {
      const data = await APIService._getListSubCategpry(parent_id);
      let sub_category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });

      setSubCate([...sub_category]);
      setLoadingSubCate(false);
    } catch (err) {
      console.log(err);
      setLoadingSubCate(false);
    }
  };

  //Loại xe
  const getListNewBrand = async (product_type_id) => {
    try {
      const data = await APIService._getListNewBrand(product_type_id);
      let dataTemp = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });

      setNewSubCate(dataTemp);
      setLoadingNewSubCate(false);
    } catch (err) {
      console.log(err);
      setLoadingNewSubCate(false);
    }
  };

  //Thuong hieu
  const getListOutStandingBrand = async () => {
    try {
      const data = await APIService._getListOutStandingBrand("");
      let newData = data.data.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });

      setOutStandingBrand(newData);
    } catch (error) {
      console.log(error);
    }
  };

  const getListColor = async () => {
    try {
      const data = await APIService._getListColorProductVersion();
      let colors = data.colors.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });

      setColorArr(colors);
    } catch (err) {
      console.log(err);
    }
  };

  const postCreateProduct = async (obj) => {
    try {
      const data = await APIService._postAddProduct(obj);
      dispatch(CLEAR_REDUX_PRODUCT());
      setIsLoadingButton(false);
      message.success("Tạo sản phẩm thành công");
      history.push("/product/list");
    } catch (err) {
      console.log(err);
      setIsLoadingButton(false);
    }
  };

  const postEditProduct = async (obj) => {
    try {
      const data = await APIService._postEditProduct(obj);
      dispatch(CLEAR_REDUX_PRODUCT());
      setIsLoadingButton(false);
      message.success("Chỉnh sửa sản phẩm thành công");
      history.goBack();
    } catch (err) {
      console.log(err);
      setIsLoadingButton(false);
    }
  };

  const callAPI = async () => {
    await getListBrandCategory();
    await getListColor();
    await getListOutStandingBrand(); // thuong hieu noi bat
    if (product.cate && product.cate !== "") {
      await getListSubCate(product.cate);
      if (product.brand && product.brand !== "") {
        await getListNewBrand(product.brand);
      }
    }
    if (!add) {
      await getListSubCate(product.cate);
      await getListNewBrand(product.brand);
    }
  };

  useEffect(() => {
    reduxForm.setFieldsValue(
      {
        product_name: product.productName,
        product_code: product.productCode,
        cate: product.cate === 0 ? undefined : product.cate,
        brand: product.brand === 0 ? undefined : product.brand,
        outStandingBrand:
          product.outStandingBrand === 0 ? undefined : product.outStandingBrand,
        newBrand: product.newBrand === 0 ? undefined : product.newBrand,
      },
      () => setIsLoading(false)
    );
  }, []);

  useEffect(() => {
    callAPI();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    reduxForm.validateFields((err, values) => {
      if (err) {
        return;
      } else {
        let arrVersionProduct = JSON.parse(
          JSON.stringify(product.arrProductVersion)
        );

        if (arrVersionProduct.length === 0) {
          Modal.error({
            title: "Thông báo",
            content: "Vui lòng nhập ít nhất 1 phiên bản sản phẩm.",
          });
          return;
        }
        let checkTotal = product.arrProductVersion.filter(
          (item) => item.totalPrice < 0
        );
        if (checkTotal.length !== 0) {
          message.error("Giá phiên bản sản phẩm không được âm");
          return;
        }
        const objFullValue = arrVersionProduct.find((item) => {
          return (
            (!item.productVersionCode &&
              !item.year &&
              !item.colorID &&
              !item.colorName &&
              !item.productVersionDescription &&
              !item.barrelType &&
              !item.publishedPrice &&
              !item.boxPrice &&
              !item.registrationPrice &&
              !item.price_promotion &&
              !item.totalPrice &&
              !item.arrayImages) ||
            (item.productVersionCode === "" &&
              item.year === "" &&
              item.colorID === 0 &&
              item.colorName === "" &&
              item.productVersionDescription === "" &&
              item.barrelType === "" &&
              item.arrayImages.length === 0)
          );
        });

        if (objFullValue) {
          Modal.error({
            title: "Thông báo",
            content: "Vui lòng điền các thông tin phiên bản được yêu cầu.",
          });
          setIsLoadingButton(false);
          return;
        }

        let arrVersion = product.arrProductVersion.map((version) => {
          return {
            attributeId: 1,
            colorId: version.colorID,
            id: add ? 0 : version.id,
            image_version: version.arrayImages,
            note: version.productVersionDescription,
            price: version.publishedPrice,
            price_package: version.boxPrice,
            price_register: version.registrationPrice,
            price_promotion: version.price_promotion,
            status: 1,
            versionCode: version.productVersionCode,
            year_of_manufacture: version.year,
            type_bin: version.barrelType,
          };
        });

        const objAdd = {
          productCode: product.productCode,
          productName: product.productName,
          imageProductUrl: [],
          fileSpecificationUrl: product.pdf_name,
          industryId: values.cate, //ngành hàng
          vehicleId: values.brand, // dòng xe
          rangeOfVehicleId: values.newBrand, //loại xe
          brandId: values.outStandingBrand, // thuong hieu
          listVersion: arrVersion,
        };

        const objEdit = {
          id: Number(params.id),
          productCode: product.productCode,
          productName: product.productName,
          imageProductUrl: [],
          fileSpecificationUrl: product.pdf_name,
          industryId: values.cate, //ngành hàng
          vehicleId: values.brand, // dòng xe
          rangeOfVehicleId: values.newBrand, //loại xe
          brandId: values.outStandingBrand, // thuong hieu
          listVersion: arrVersion,
        };
        setIsLoadingButton(true);
        add ? postCreateProduct(objAdd) : postEditProduct(objEdit);
      }
    });
  };

  const renderTitleColumn = (title) => {
    return <span style={{ fontSize: 12, fontWeight: 700 }}>{title}</span>;
  };

  const columns = [
    {
      title: renderTitleColumn("MÃ PHIÊN BẢN"),
      dataIndex: "productVersionCode",
      key: "productVersionCode",
      align: "center",
      render: (productVersionCode) => (
        <span>
          {!productVersionCode ? (
            "-"
          ) : (
            <Tooltip title={productVersionCode}>{productVersionCode}</Tooltip>
          )}
        </span>
      ),
    },
    {
      title: renderTitleColumn("NĂM SẢN XUẤT"),
      dataIndex: "year",
      key: "year",
      align: "center",
      render: (year) => <span>{!year ? "-" : year}</span>,
    },
    {
      title: renderTitleColumn("MÀU SẮC"),
      dataIndex: "colorName",
      key: "colorName",
      align: "center",
      render: (colorName) => <span>{!colorName ? "-" : colorName}</span>,
    },
    {
      title: renderTitleColumn("LOẠI THÙNG"),
      dataIndex: "barrelType",
      key: "barrelType",
      align: "center",
      render: (barrelType) => <span>{!barrelType ? "-" : barrelType}</span>,
    },
    {
      title: renderTitleColumn("GIÁ CÔNG BỐ"),
      dataIndex: "publishedPrice",
      key: "publishedPrice",
      align: "center",
      render: (publishedPrice) => (
        <span>
          {publishedPrice === null || publishedPrice === undefined
            ? "-"
            : priceFormat(publishedPrice)}
        </span>
      ),
    },
    {
      title: renderTitleColumn("GIÁ THÙNG PHIẾU"),
      dataIndex: "boxPrice",
      key: "boxPrice",
      align: "center",
      render: (boxPrice) => (
        <span>
          {boxPrice === null || boxPrice === undefined
            ? "-"
            : priceFormat(boxPrice)}
        </span>
      ),
    },
    {
      title: renderTitleColumn("DKDK"),
      dataIndex: "registrationPrice",
      key: "registrationPrice",
      align: "center",
      render: (registrationPrice) => (
        <span>
          {registrationPrice === null || registrationPrice === undefined
            ? "-"
            : priceFormat(registrationPrice)}
        </span>
      ),
    },
    {
      title: renderTitleColumn("GIÁ KHUYẾN MÃI"),
      dataIndex: "price_promotion",
      key: "price_promotion",
      align: "center",
      render: (price_promotion) => (
        <span>
          {price_promotion === null || price_promotion === undefined
            ? "-"
            : priceFormat(price_promotion)}
        </span>
      ),
    },
    {
      title: renderTitleColumn("TỔNG CỘNG"),
      dataIndex: "totalPrice",
      key: "totalPrice",
      align: "center",
      render: (totalPrice) => (
        <span>
          {totalPrice === null || totalPrice === undefined
            ? "-"
            : priceFormat(totalPrice)}
        </span>
      ),
    },
    {
      title: "",
      width: 50,
      align: "center",
      fixed: "right",
      render: (text, item, index) => {
        return (
          <div
            onClick={(e) => {
              e.stopPropagation();

              add
                ? history.push(`/product/create/0/version/0/${index}`)
                : history.push(
                    `/product/edit/${params.id}/version/${item.id}/${index}`
                  );
            }}
          >
            <ISvg
              name={ISvg.NAME.WRITE}
              width={20}
              height={20}
              fill={colors.main}
            />
          </div>
        );
      },
    },
  ];

  return (
    <div
      style={{
        width: "100%",
      }}
    >
      <Form onSubmit={handleSubmit}>
        <Row gutter={[0, 12]}>
          <Col span={24}>
            <ITitle
              level={1}
              title={add ? "Tạo mới sản phẩm" : "Chỉnh sửa sản phẩm"}
              style={{
                color: colors.mainDark,
                fontWeight: "bold",
                marginTop: 14,
              }}
            />
          </Col>
          <Col span={24}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              <IButton
                icon={ISvg.NAME.SAVE}
                loading={isLoadingButton}
                title="Lưu"
                htmlType="submit"
                color={colors.main}
              />
              <IButton
                loading={isLoadingButton}
                icon={ISvg.NAME.CROSS}
                style={{
                  marginLeft: 15,
                }}
                title="Hủy"
                color={colors.red}
                onClick={() => {
                  dispatch(CLEAR_REDUX_PRODUCT());
                  add ? history.push("/product/list") : history.goBack();
                }}
              />
            </div>
          </Col>
          <Col span={24}>
            <div
              style={{
                height: "100%",
              }}
            >
              <Row
                gutter={[12, 0]}
                style={{
                  height: "100%",
                }}
                type="flex"
              >
                <Col span={8}>
                  <div
                    style={{
                      padding: "25px 30px",
                      background: colors.white,
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                      height: "100%",
                    }}
                  >
                    {/* {isLoading ? (
                            <div>
                              <Skeleton
                                loading={true}
                                active
                                paragraph={{ rows: 16 }}
                              />
                            </div>
                          ) : ( */}
                    <Row gutter={[12, 20]}>
                      <Col style={{ paddingTop: 0 }}>
                        <ITitle
                          level={3}
                          title="Thông tin sản phẩm"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                      </Col>
                      <Col>
                        <IFormItem>
                          {getFieldDecorator("product_name", {
                            rules: [
                              {
                                required: true,
                                message: "Vui lòng nhập tên sản phẩm",
                              },
                            ],
                          })(
                            <div>
                              <Row>
                                <div
                                  style={{
                                    display: "flex",
                                  }}
                                >
                                  <ITitle
                                    level={4}
                                    title={"Tên sản phẩm"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  <span
                                    style={{
                                      color: "red",
                                      marginLeft: 4,
                                    }}
                                  >
                                    (*)
                                  </span>
                                </div>
                                <IInputText
                                  style={{
                                    color: "#000",
                                  }}
                                  // value={reduxForm.getFieldValue(
                                  //   "product_name"
                                  // )}
                                  placeholder="Nhập tên sản phẩm"
                                  onChange={(e) => {
                                    const data = {
                                      key: "productName",
                                      value: e.target.value,
                                    };
                                    dispatch(CREATE_PRODUCT_INFO(data));
                                  }}
                                  value={product.productName}
                                />
                              </Row>
                            </div>
                          )}
                        </IFormItem>
                      </Col>
                      <Col>
                        <IFormItem>
                          {getFieldDecorator("product_code", {
                            rules: [
                              {
                                required: true,
                                message: "Vui lòng nhập mã sản phẩm",
                              },
                            ],
                          })(
                            <div>
                              <Row>
                                <div
                                  style={{
                                    display: "flex",
                                  }}
                                >
                                  <ITitle
                                    level={4}
                                    title={"Mã sản phẩm"}
                                    style={{
                                      fontWeight: 600,
                                    }}
                                  />
                                  <span
                                    style={{
                                      color: "red",
                                      marginLeft: 4,
                                    }}
                                  >
                                    (*)
                                  </span>
                                </div>
                                <IInputText
                                  style={{
                                    color: "#000",
                                  }}
                                  placeholder="Nhập mã sản phẩm"
                                  disabled={add ? false : true}
                                  onChange={(e) => {
                                    const data = {
                                      key: "productCode",
                                      value: e.target.value.replace(/\s/g, ""),
                                    };
                                    dispatch(CREATE_PRODUCT_INFO(data));
                                  }}
                                  value={product.productCode}
                                />
                              </Row>
                            </div>
                          )}
                        </IFormItem>
                      </Col>
                      <Col>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <IFormItem>
                              {getFieldDecorator("cate", {
                                rules: [
                                  {
                                    required: true,
                                    message: "Vui lòng chọn loại xe",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <div
                                      style={{
                                        display: "flex",
                                      }}
                                    >
                                      <ITitle
                                        level={4}
                                        title={"Loại xe"}
                                        style={{
                                          fontWeight: 600,
                                        }}
                                      />
                                      <span
                                        style={{
                                          color: "red",
                                          marginLeft: 4,
                                        }}
                                      >
                                        (*)
                                      </span>
                                    </div>
                                    <ISelect
                                      data={cate}
                                      select={loadingCate ? false : true}
                                      isBackground={false}
                                      style={{
                                        marginTop: 1,
                                      }}
                                      placeholder="Chọn loại xe"
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          cate: key,
                                          brand: undefined,
                                          newBrand: undefined,
                                        });
                                        const data = {
                                          key: "cate",
                                          value: key,
                                        };
                                        dispatch(CREATE_PRODUCT_INFO(data));

                                        const data1 = {
                                          key: "brand",
                                          value: undefined,
                                        };
                                        dispatch(CREATE_PRODUCT_INFO(data1));

                                        const data2 = {
                                          key: "newBrand",
                                          value: undefined,
                                        };
                                        dispatch(CREATE_PRODUCT_INFO(data2));
                                        setLoadingSubCate(true);
                                        getListSubCate(key);
                                      }}
                                      value={
                                        !product.cate ? undefined : product.cate
                                      }
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          <Col span={12}>
                            <IFormItem>
                              {getFieldDecorator("brand", {
                                rules: [
                                  {
                                    required: true,
                                    message: "Vui lòng chọn dòng xe",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <div
                                      style={{
                                        display: "flex",
                                      }}
                                    >
                                      <ITitle
                                        level={4}
                                        title={"Dòng xe"}
                                        style={{
                                          fontWeight: 600,
                                        }}
                                      />
                                      <span
                                        style={{
                                          color: "red",
                                          marginLeft: 4,
                                        }}
                                      >
                                        (*)
                                      </span>
                                    </div>
                                    <ISelect
                                      data={subCate}
                                      select={
                                        !product.cate || loadingSubCate
                                          ? false
                                          : true
                                      }
                                      isBackground={false}
                                      style={{
                                        marginTop: 1,
                                      }}
                                      placeholder="Chọn dòng xe"
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          brand: key,
                                          newBrand: undefined,
                                        });
                                        const data = {
                                          key: "brand",
                                          value: key,
                                        };
                                        dispatch(CREATE_PRODUCT_INFO(data));

                                        const data2 = {
                                          key: "newBrand",
                                          value: undefined,
                                        };
                                        dispatch(CREATE_PRODUCT_INFO(data2));
                                        setLoadingNewSubCate(true);
                                        getListNewBrand(key);
                                      }}
                                      value={
                                        !product.cate
                                          ? undefined
                                          : !product.brand
                                          ? undefined
                                          : product.brand
                                      }
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                        </Row>
                      </Col>
                      <Col>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <IFormItem>
                              {getFieldDecorator("newBrand", {
                                rules: [
                                  {
                                    required: true,
                                    message: "Vui lòng chọn thương hiệu",
                                  },
                                ],
                              })(
                                <div>
                                  <Row>
                                    <div
                                      style={{
                                        display: "flex",
                                      }}
                                    >
                                      <ITitle
                                        level={4}
                                        title={"Thương hiệu"}
                                        style={{
                                          fontWeight: 600,
                                        }}
                                      />
                                      <span
                                        style={{
                                          color: "red",
                                          marginLeft: 4,
                                        }}
                                      >
                                        (*)
                                      </span>
                                    </div>
                                    <ISelect
                                      data={newSubCate}
                                      select={
                                        !product.brand || loadingNewSubCate
                                          ? false
                                          : true
                                      }
                                      isBackground={false}
                                      style={{
                                        marginTop: 1,
                                      }}
                                      placeholder="Chọn thương hiệu"
                                      onChange={(key) => {
                                        reduxForm.setFieldsValue({
                                          newBrand: key,
                                        });
                                        const data = {
                                          key: "newBrand",
                                          value: key,
                                        };
                                        dispatch(CREATE_PRODUCT_INFO(data));
                                      }}
                                      value={
                                        !product.brand
                                          ? undefined
                                          : !product.newBrand
                                          ? undefined
                                          : product.newBrand
                                      }
                                    />
                                  </Row>
                                </div>
                              )}
                            </IFormItem>
                          </Col>
                          <Col span={12}>
                            <Col>
                              <IFormItem>
                                {getFieldDecorator("outStandingBrand", {
                                  rules: [
                                    {
                                      required: false,
                                      message: "Vui lòng chọn thương hiệu",
                                    },
                                  ],
                                })(
                                  <div>
                                    <Row>
                                      <div
                                        style={{
                                          display: "flex",
                                        }}
                                      >
                                        <ITitle
                                          level={4}
                                          title={"Thương hiệu"}
                                          // title={"Nhà sản xuất"}
                                          style={{
                                            fontWeight: 600,
                                          }}
                                        />
                                        {/* <span
                                    style={{
                                      color: "red",
                                      marginLeft: 4,
                                    }}
                                  >
                                    (*)
                                  </span> */}
                                      </div>
                                      <ISelect
                                        data={outStandingBrand}
                                        select={
                                          loadingOutStandingBrand ? false : true
                                        }
                                        isBackground={false}
                                        style={{
                                          marginTop: 1,
                                        }}
                                        placeholder="Chọn thương hiệu"
                                        onChange={(key) => {
                                          reduxForm.setFieldsValue({
                                            outStandingBrand: key,
                                          });
                                          const data = {
                                            key: "outStandingBrand",
                                            value: key,
                                          };
                                          dispatch(CREATE_PRODUCT_INFO(data));
                                        }}
                                        value={
                                          !product.outStandingBrand
                                            ? undefined
                                            : product.outStandingBrand
                                        }
                                      />
                                    </Row>
                                  </div>
                                )}
                              </IFormItem>
                            </Col>
                          </Col>
                        </Row>
                      </Col>
                      <Col>
                        <ITitle
                          level={3}
                          title="Thông số kỹ thuật"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                      </Col>
                      <Col>
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "flex-start",
                          }}
                        >
                          {product.pdf_name === "" ? (
                            <div
                              style={{
                                textDecoration: "underline",
                                color: colors.blackChart,
                                marginRight: 40,
                              }}
                            >
                              Vui lòng upload file giới thiệu sản phẩm
                            </div>
                          ) : (
                            <a
                              href={`${product.pdf_url}${product.pdf_name}`}
                              target="_blank"
                              download={product.pdf_name}
                              style={{
                                textDecoration: "underline",
                                color: colors.blackChart,
                                marginRight: 15,
                                cursor: "pointer",
                              }}
                            >
                              {product.pdf_name}
                            </a>
                          )}
                          <div>
                            <IButton
                              title="Upload"
                              color={colors.main}
                              loading={loadingImport}
                              styleHeight={{
                                width: "100%",
                              }}
                              icon={ISvg.NAME.UPLOAD}
                              onClick={() => {
                                var input = document.getElementById("my-file");
                                input.click();
                                input.onchange = async function (e) {
                                  try {
                                    var file = input.files[0];

                                    if (!file) return;
                                    setLoadingImport(true);
                                    const name_file = file.name.split(".");
                                    console.log({ name_file });
                                    const data = await APIService._uploadFile(
                                      "PRODUCT",
                                      file,
                                      name_file[0]
                                    );

                                    const dataAction1 = {
                                      key: "pdf_url",
                                      value: data.file_url,
                                    };
                                    dispatch(CREATE_PRODUCT_INFO(dataAction1));

                                    const dataAction2 = {
                                      key: "pdf_name",
                                      value: data.file[0],
                                    };
                                    dispatch(CREATE_PRODUCT_INFO(dataAction2));

                                    if (data === true) {
                                      setLoadingImport(false);
                                      e.target.value = null;
                                      return;
                                    }
                                    setLoadingImport(false);

                                    e.target.value = null;
                                  } catch (error) {
                                    message.error("Upload file thất bại");
                                    setLoadingImport(false);
                                  }
                                };
                              }}
                            />
                            <input
                              type="file"
                              id="my-file"
                              accept=".pdf"
                              style={{ display: "none" }}
                            />
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
                <Col span={16}>
                  <div
                    style={{
                      padding: "25px 30px",
                      background: colors.white,
                      height: "100%",
                      boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                    }}
                  >
                    {isLoading ? (
                      <div>
                        <Skeleton
                          loading={true}
                          active
                          paragraph={{
                            rows: 16,
                          }}
                        />
                      </div>
                    ) : (
                      <Row gutter={[12, 20]}>
                        <Col>
                          <ITitle
                            level={3}
                            title="Phiên bản sản phẩm"
                            style={{
                              fontWeight: "bold",
                            }}
                          />
                        </Col>
                        <Col span={24} xxl={20} xl={24}>
                          <Row gutter={[25, 0]}>
                            <Col span={5}>
                              <div
                                style={{
                                  display: "flex",
                                }}
                              >
                                <ITitle
                                  level={4}
                                  title={"Năm sản xuất"}
                                  style={{
                                    fontWeight: 600,
                                  }}
                                />
                                <span
                                  style={{
                                    color: "red",
                                    marginLeft: 4,
                                  }}
                                >
                                  (*)
                                </span>
                              </div>
                              <IInputText
                                style={{
                                  color: "#000",
                                }}
                                placeholder="Nhập năm sản xuất"
                                onChange={(e) => {
                                  setObjVersionTmp({
                                    ...objVersionTmp,
                                    year: e.target.value,
                                  });
                                }}
                                value={objVersionTmp.year}
                              />
                            </Col>

                            <Col span={5}>
                              <div
                                style={{
                                  display: "flex",
                                }}
                              >
                                <ITitle
                                  level={4}
                                  title={"Màu sắc"}
                                  style={{
                                    fontWeight: 600,
                                  }}
                                />
                                <span
                                  style={{
                                    color: "red",
                                    marginLeft: 4,
                                  }}
                                >
                                  (*)
                                </span>
                              </div>
                              <ISelect
                                data={colorArr}
                                select={true}
                                isBackground={false}
                                style={{
                                  marginTop: 1,
                                }}
                                placeholder="Chọn màu sắc"
                                onChange={(key, option) => {
                                  setObjVersionTmp({
                                    ...objVersionTmp,
                                    colorID: Number(key),
                                    colorName: option.props.children,
                                  });
                                }}
                                value={
                                  !objVersionTmp.colorID
                                    ? undefined
                                    : objVersionTmp.colorID
                                }
                              />
                            </Col>
                            <Col span={5}>
                              <div
                                style={{
                                  display: "flex",
                                }}
                              >
                                <ITitle
                                  level={4}
                                  title={"Loại thùng"}
                                  style={{
                                    fontWeight: 600,
                                  }}
                                />
                                <span
                                  style={{
                                    color: "red",
                                    marginLeft: 4,
                                  }}
                                >
                                  (*)
                                </span>
                              </div>
                              <IInputText
                                style={{
                                  color: "#000",
                                }}
                                placeholder="Nhập loại thùng"
                                onChange={(e) => {
                                  setObjVersionTmp({
                                    ...objVersionTmp,
                                    barrelType: e.target.value,
                                  });
                                }}
                                value={objVersionTmp.barrelType}
                              />
                            </Col>
                          </Row>
                        </Col>

                        <Col span={24}>
                          <div>
                            <ITable
                              data={product.arrProductVersion}
                              columns={columns}
                              hidePage={true}
                              backgroundWhite={true}
                              bordered
                              footer={() => {
                                return (
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                      justifyContent: "center",
                                    }}
                                  >
                                    <div
                                      style={{
                                        cursor: "pointer",
                                        display: "flex",
                                        alignItems: "center",
                                      }}
                                      onClick={() => {
                                        let arrProduct = JSON.parse(
                                          JSON.stringify(
                                            product.arrProductVersion
                                          )
                                        );

                                        if (
                                          Object.keys(objVersionTmp).length ===
                                            0 ||
                                          objVersionTmp.color === 0 ||
                                          objVersionTmp.colorName === "" ||
                                          objVersionTmp.year === "" ||
                                          objVersionTmp.barrelType === ""
                                        ) {
                                          message.warning(
                                            "Vui lòng điền đầy đủ thông tin phiên bản"
                                          );
                                          return;
                                        }

                                        let objTmp = arrProduct.find((item) => {
                                          return (
                                            item.colorID ===
                                              objVersionTmp.colorID &&
                                            item.colorName.toLowerCase() ===
                                              objVersionTmp.colorName.toLowerCase() &&
                                            item.barrelType.toLowerCase() ===
                                              objVersionTmp.barrelType.toLowerCase() &&
                                            item.year === objVersionTmp.year
                                          );
                                        });

                                        if (objTmp) {
                                          message.warning(
                                            "Thông tin phiên bản đã tồn tại"
                                          );
                                          return;
                                        }

                                        const data = {
                                          key: "arrProductVersion",
                                          value: {
                                            ...objVersionTmp,
                                            publishedPrice: 0,
                                            boxPrice: 0,
                                            registrationPrice: 0,
                                            totalPrice: 0,
                                            price_promotion: 0,
                                          },
                                        };
                                        dispatch(CREATE_PRODUCT_VERSION(data));
                                        setObjVersionTmp({});
                                      }}
                                    >
                                      <span
                                        style={{
                                          marginRight: "12px",
                                          color: colors.main,
                                          fontWeight: 600,
                                        }}
                                      >
                                        Thêm phiên bản
                                      </span>
                                      <div>
                                        <ISvg
                                          name={ISvg.NAME.ADDUPLOAD}
                                          width={20}
                                          height={20}
                                          fill={colors.main}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                );
                              }}
                              style={{
                                width: "100%",
                                border: "1px solid rgba(122, 123, 123, 0.3)",
                                borderRadius: 4,
                              }}
                              scroll={{
                                y: 550,
                                x:
                                  product.arrProductVersion.length === 0
                                    ? 0
                                    : 1400,
                              }}
                              locale={{
                                emptyText: (
                                  <Empty description="Không có phiên bản" />
                                ),
                              }}
                              size="small"
                              pagination={false}
                            ></ITable>
                          </div>
                        </Col>
                      </Row>
                    )}
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

const createProductPage = Form.create({
  name: "CreateProductPage",
})(CreateProductPage);

export default createProductPage;
