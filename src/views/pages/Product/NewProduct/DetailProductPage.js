import React, { useState, useEffect } from "react";
import { Row, Col, Skeleton, Tooltip, Empty } from "antd";
import { ITitle, IButton, ISvg, ITableNotFooter } from "../../../components";
import { colors } from "../../../../assets";
import { priceFormat } from "../../../../utils";

import { APIService } from "../../../../services";
import { useHistory, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  ASSIGNED_DATA_PRODUCT_REDUX_DETAIL,
  ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL,
  CLEAR_REDUX_PRODUCT,
} from "../../../store/reducers";

function DetailProductPage(props) {
  const { id } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(true);
  const [dataDetail, setDataDetail] = useState({ listVersion: [] });

  const getDetailProduct = async (id) => {
    try {
      dispatch(CLEAR_REDUX_PRODUCT());
      const data = await APIService._getDetailProduct(id);
      setDataDetail(data.product);
      dispatch(ASSIGNED_DATA_PRODUCT_REDUX_DETAIL(data.product));
      if (data.product.listVersion.length > 0) {
        data.product.listVersion.forEach(async (item, indexVersion) => {
          await getDetailProductVersion(item.id, indexVersion);
        });
      }
      setIsLoading(false);
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  };

  const getDetailProductVersion = async (id, indexVersion) => {
    try {
      const data = await APIService._getDetailProductVersion(id, indexVersion);
      if (data.version.listSpecification.length === 0) {
        data.version.listSpecification = [
          {
            id: 0,
            name: "",
            content: "",
          },
        ];
      }
      const dataRedux = {
        idx: indexVersion,
        value: data.version,
      };
      dispatch(ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL(dataRedux));
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getDetailProduct(id);
  }, []);

  const renderTitleColumn = (title) => {
    return <span style={{ fontSize: 12, fontWeight: 700 }}>{title}</span>;
  };

  const columns = [
    {
      title: renderTitleColumn("MÃ PHIÊN BẢN"),
      dataIndex: "versionCode",
      key: "versionCode",
      align: "center",
      render: (versionCode) => (
        <span>
          {!versionCode ? (
            "-"
          ) : (
            <Tooltip title={versionCode}>{versionCode}</Tooltip>
          )}
        </span>
      ),
    },
    {
      title: renderTitleColumn("NĂM SẢN XUẤT"),
      dataIndex: "year_of_manufacture",
      key: "year_of_manufacture",
      align: "center",
      render: (year_of_manufacture) => (
        <span>{!year_of_manufacture ? "-" : year_of_manufacture}</span>
      ),
    },
    {
      title: renderTitleColumn("MÀU SẮC"),
      dataIndex: "colorName",
      key: "colorName",
      align: "center",
      render: (colorName) => <span>{!colorName ? "-" : colorName}</span>,
    },
    {
      title: renderTitleColumn("LOẠI THÙNG"),
      dataIndex: "characteristics",
      key: "characteristics",
      align: "center",
      render: (characteristics) => (
        <span>{!characteristics ? "-" : characteristics}</span>
      ),
    },
    {
      title: renderTitleColumn("GIÁ CÔNG BỐ"),
      dataIndex: "price",
      key: "price",
      align: "center",
      render: (price) => (
        <span>
          {price === null || price === undefined ? "-" : priceFormat(price)}
        </span>
      ),
    },
    {
      title: renderTitleColumn("GIÁ THÙNG PHIẾU"),
      dataIndex: "price_package",
      key: "price_package",
      align: "center",
      render: (price_package) => (
        <span>
          {price_package === null || price_package === undefined
            ? "-"
            : priceFormat(price_package)}
        </span>
      ),
    },
    {
      title: renderTitleColumn("DKDK"),
      dataIndex: "price_register",
      key: "price_register",
      align: "center",
      render: (price_register) => (
        <span>
          {price_register === null || price_register === undefined
            ? "-"
            : priceFormat(price_register)}
        </span>
      ),
    },
    {
      title: renderTitleColumn("GIÁ KHUYẾN MÃI"),
      dataIndex: "price_promotion",
      key: "price_promotion",
      align: "center",
      render: (price_promotion) => (
        <span>
          {price_promotion === null || price_promotion === undefined
            ? "-"
            : priceFormat(price_promotion)}
        </span>
      ),
    },
    {
      title: renderTitleColumn("TỔNG CỘNG"),
      dataIndex: "total",
      key: "total",
      align: "center",
      render: (total) => (
        <span>
          {total === null || total === undefined ? "-" : priceFormat(total)}
        </span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%" }}>
      <Row gutter={[12, 24]}>
        <Col span={24}>
          <ITitle
            level={1}
            title="Chi tiết sản phẩm"
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        <Col span={24}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-end",
            }}
          >
            <IButton
              icon={ISvg.NAME.WRITE}
              style={{ marginLeft: 15 }}
              loading={isLoading}
              title="Chỉnh sửa"
              color={colors.main}
              onClick={() => {
                history.push("/product/edit/" + id);
              }}
            />
          </div>
        </Col>
        <Col span={24}>
          <div
            style={{
              height: "100%",
            }}
          >
            <Row gutter={[25, 0]} style={{ height: "100%" }} type="flex">
              <Col span={8}>
                <div
                  style={{
                    padding: 30,
                    background: colors.white,
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                    height: 650,
                  }}
                >
                  {isLoading ? (
                    <div>
                      <Skeleton
                        loading={true}
                        active
                        paragraph={{ rows: 16 }}
                      />
                    </div>
                  ) : (
                    <Row gutter={[12, 20]}>
                      <Col span={24}>
                        <ITitle
                          level={3}
                          title="Thông tin sản phẩm"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                      </Col>

                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <ITitle
                              level={4}
                              title={"Tên sản phẩm"}
                              style={{
                                fontWeight: 600,
                              }}
                            />
                            <span
                              style={{
                                wordBreak: "break-all",
                                wordWrap: "break-word",
                              }}
                            >
                              {!dataDetail.name ? "-" : dataDetail.name}
                            </span>
                          </Col>
                          <Col span={12}>
                            <ITitle
                              level={4}
                              title={"Mã sản phẩm"}
                              style={{
                                fontWeight: 600,
                              }}
                            />
                            <span
                              style={{
                                wordBreak: "break-all",
                                wordWrap: "break-word",
                              }}
                            >
                              {!dataDetail.productCode
                                ? "-"
                                : dataDetail.productCode}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 12]}>
                          <Col span={12}>
                            <ITitle
                              level={4}
                              title={"Loại xe"}
                              style={{
                                fontWeight: 600,
                              }}
                            />
                            <span style={{ wordWrap: "break-word" }}>
                              {!dataDetail.typeParentName
                                ? "-"
                                : dataDetail.typeParentName}
                            </span>
                          </Col>
                          <Col span={12}>
                            <ITitle
                              level={4}
                              title={"Dòng xe"}
                              style={{
                                fontWeight: 600,
                              }}
                            />
                            <span style={{ wordWrap: "break-word" }}>
                              {!dataDetail.typeName ? "-" : dataDetail.typeName}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[12, 0]}>
                          <Col span={12}>
                            <ITitle
                              level={4}
                              title={"Thương hiệu"}
                              style={{
                                fontWeight: 600,
                              }}
                            />
                            <span style={{ wordWrap: "break-word" }}>
                              {!dataDetail.categoryName
                                ? "-"
                                : dataDetail.categoryName}
                            </span>
                          </Col>
                          <Col span={12}>
                            <ITitle
                              level={4}
                              title={"Thương hiệu"}
                              style={{
                                fontWeight: 600,
                              }}
                            />
                            <span style={{ wordWrap: "break-word" }}>
                              {!dataDetail.brandName
                                ? "-"
                                : dataDetail.brandName}
                            </span>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={12}>
                        <ITitle
                          level={4}
                          title={"Trạng thái"}
                          style={{
                            fontWeight: 600,
                          }}
                        />
                        <span style={{ wordWrap: "break-word" }}>
                          {!dataDetail.statusName ? "-" : dataDetail.statusName}
                        </span>
                      </Col>
                      <Col span={24}>
                        <ITitle
                          level={3}
                          title="Thông số kỹ thuật"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                        <div style={{ marginTop: 10 }}>
                          {dataDetail.specification ? (
                            <a
                              href={`${dataDetail.specification_url}${dataDetail.specification}`}
                              target="_blank"
                              download={dataDetail.specification}
                              style={{
                                textDecoration: "underline",
                                color: colors.blackChart,
                                cursor: "pointer",
                              }}
                            >
                              {dataDetail.specification}
                            </a>
                          ) : (
                            <span>Chưa cập nhật</span>
                          )}
                        </div>
                      </Col>
                    </Row>
                  )}
                </div>
              </Col>
              <Col span={16}>
                <div
                  style={{
                    padding: 30,
                    background: colors.white,
                    height: "100%",
                    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  {isLoading ? (
                    <div>
                      <Skeleton
                        loading={true}
                        active
                        paragraph={{ rows: 16 }}
                      />
                    </div>
                  ) : (
                    <Row gutter={[12, 20]}>
                      <Col>
                        <ITitle
                          level={3}
                          title="Phiên bản sản phẩm"
                          style={{
                            fontWeight: "bold",
                          }}
                        />
                      </Col>

                      <Col span={24}>
                        <ITableNotFooter
                          data={dataDetail.listVersion}
                          columns={columns}
                          hidePage={true}
                          backgroundWhite={true}
                          bordered
                          hidePage={true}
                          size="small"
                          pagination={false}
                          style={{
                            width: "100%",
                            border: "1px solid rgba(122, 123, 123, 0.3)",
                            borderRadius: 4,
                          }}
                          scroll={{
                            y: 550,
                            x: dataDetail.listVersion.length === 0 ? 0 : 1400,
                          }}
                          locale={{
                            emptyText: (
                              <Empty description="Không có phiên bản" />
                            ),
                          }}
                          onRow={(record, rowIndex) => {
                            return {
                              onClick: () => {
                                history.push(
                                  `/product/detail/${id}/version/${record.id}/${rowIndex}`
                                );
                              },
                            };
                          }}
                        ></ITableNotFooter>
                      </Col>
                    </Row>
                  )}
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default DetailProductPage;
