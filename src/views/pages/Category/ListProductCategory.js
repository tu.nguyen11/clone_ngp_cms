import {
  MinusCircleTwoTone,
  MinusOutlined,
  PlusCircleTwoTone,
  PlusOutlined,
} from "@ant-design/icons";

import React, { useState, useEffect } from "react";

import { Row, Col, Tooltip, message, Modal, Table } from "antd";

import { useHistory } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";

import styled from "styled-components";

import {
  ITitle,
  ISearch,
  ISvg,
  ISelect,
  ITable,
  IButton,
} from "../../components";
import { colors, images } from "../../../assets";
import { APIService } from "../../../services";
import { priceFormat } from "../../../utils";
import { CLEAR_REDUX_PRODUCT } from "../../store/reducers";
import { StyledITitle } from "../../components/common/Font/font";

const IStyledTable = styled(Table)`
  .ant-table-row {
    cursor: pointer;
  }
  & .ant-table-tbody > tr > td > span {
    /* display: inline-block; */
    /* display: -webkit-box; */
    /* -webkit-line-clamp: 1;
    -webkit-box-orient: vertical; */
    overflow: hidden;
    text-overflow: ellipsis;
  }
  .ant-table-column-title {
    font-weight: bold;
  }
  .ant-table-thead {
    background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};
  }
  & .ant-table-thead > tr > th {
    background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 19px;
    border-bottom: ${(props) =>
      props.bordered === true
        ? "0px solid rgba(122, 123, 123, 0.3)"
        : "1px solid rgba(122, 123, 123, 0.3)"};
  }
  .ant-table-row-expand-icon {
    border: none;
    background-color: transparent;
    width: 25px;
    height: 25px;
    position: relative;
  }
  .ant-table-row-collapsed::after {
    font-size: 20px;
    color: ${colors.main};
  }
  .ant-table-row-expanded::after {
    color: ${colors.main};
    font-size: 20px;
  }
  & .ant-table.ant-table-bordered thead > tr > th {
      border-right: 1px solid rgba(122, 123, 123, 0.3) !important;
      border-bottom: ${(props) =>
        props.bordered === false
          ? "0px solid rgba(122, 123, 123, 0.3)"
          : "1px solid rgba(122, 123, 123, 0.3)"};
    }

    & .ant-table.ant-table-bordered thead > tr > th:last-child {
      border-right: 0px solid rgba(122, 123, 123, 0.3) !important;
    }

    & .ant-table-tbody > tr > td {
      background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};

      border-bottom: ${(props) =>
        props.bordered === false
          ? "0px solid rgba(122, 123, 123, 0.3) !important"
          : "1px solid rgba(122, 123, 123, 0.3) !important"};
    }

    & .ant-table.ant-table-bordered tbody > tr > td {
      border-right: ${(props) =>
        props.bordered === false
          ? "0px solid rgba(122, 123, 123, 0.3) !important"
          : "1px solid rgba(122, 123, 123, 0.3) !important"};
    }

    & .ant-table.ant-table-bordered tbody > tr > td:last-child {
      border-right: 0px solid rgba(122, 123, 123, 0.3) !important;
    }
    .ant-table-fixed-header .ant-table-scroll .ant-table-header {
      overflow-x:scroll !important;
      overflow-y:hidden !important;
    }

    background: ${(props) => (props.backgroundWhite ? "#fff" : "#f5f3f6")};
    // border-bottom: 1px solid rgba(122, 123, 123, 0.3);
    font-size: clamp(8px, 4vw, 14px);
    font-style: normal;
    font-weight: normal;
    color: #22232b;
    line-height: 20px;
  }
`;

function ListProductCategory(props) {
  const history = useHistory();
  const dispatch = useDispatch();

  const [filter, setFilter] = useState({
    group_id: 0,
    page: 1,
    status: 2,
    key: "",
    category_id: 0,
    type: 0,
  });

  const [dataTable, setDataTable] = useState({
    listProduct: [],
    total: 1,
    size: 10,
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const [loadingGroupProduct2, setLoadingGroupProduct2] = useState(false);
  const [loadingTable, setLoadingTable] = useState(true);
  const [isModal, setIsModal] = useState(false);
  const [checkReview, setCheckReview] = useState(1);

  const [dataReview, setDataReview] = useState([
    {
      name: "Hiển thị sản phẩm đã chọn",
      active: true,
      type: 1,
    },
    {
      name: "Ẩn sản phẩm đã chọn",
      active: false,
      type: 2,
    },
  ]);

  const _fetchAPIListProductsCategory = async (obj) => {
    try {
      const data = await APIService._getListProductCategory(obj);
      console.log("Data", data);
      let removeDuplicateKeyArray = data.list.map((v) => ({
        ...v,
        key: v.key + "_" + v.type,
      }));
      // data.product.map((item, index) => {
      //   item.stt = (filter.page - 1) * 10 + index + 1;
      //   return item;
      // });

      setDataTable({
        listProduct: removeDuplicateKeyArray,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log(error);
      setLoadingTable(false);
    }
  };

  const postUpdateStatusProduct = async (obj) => {
    try {
      const data = await APIService._postUpdateStatusProduct();
      setIsModal(false);
      setSelectedRowKeys([]);
      await _fetchAPIListProductsCategory();
      message.success("Duyệt thành công");
    } catch (err) {
      message.error(err);
    }
  };

  useEffect(() => {
    _fetchAPIListProductsCategory(filter);
  }, [filter]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const columns = [
    // {
    //   title: "STT",
    //   dataIndex: "stt",
    //   key: "stt",
    //   align: "center",
    //   width: 60,
    // },

    {
      title: "Loại xe",
      dataIndex: "name",
      key: "name",
      render: (name) => (
        <Tooltip title={name}>
          <span>{!name ? "-" : name}</span>
        </Tooltip>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      align: "center",
      render: (status) => (status === 1 ? <span>Hiện</span> : <span>Ẩn</span>),
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
                Danh sách danh mục sản phẩm
              </StyledITitle>
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo tên thương hiệu">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo tên thương hiệu"
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilter({
                        ...filter,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                          alt=""
                        />
                      </div>
                    }
                  />
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div style={{ margin: "26px 0px" }}>
            <Row gutter={[0, 50]}>
              <Col span={24}>
                <Row>
                  <Col span={24}>
                    <div className="flex justify-end">
                      <IButton
                        icon={ISvg.NAME.ARROWUP}
                        title={"Tạo mới"}
                        color={colors.main}
                        styleHeight={{
                          width: 120,
                          marginLeft: 15,
                        }}
                        onClick={() => {
                          dispatch(CLEAR_REDUX_PRODUCT());
                          history.push("/category/create/0");
                        }}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <IStyledTable
                    columns={columns}
                    dataSource={dataTable.listProduct}
                    // expandable={{
                    //   expandedRowRender: (record) => (
                    //     <p style={{ margin: 0 }}>{record.type}</p>
                    //   ),
                    //   expandIcon: ({ expanded, onExpand, record }) =>
                    //     expanded ? (
                    //       <MinusCircleTwoTone
                    //         onClick={(e) => onExpand(record, e)}
                    //       />
                    //     ) : (
                    //       <PlusCircleTwoTone
                    //         onClick={(e) => onExpand(record, e)}
                    //       />
                    //     ),
                    //   rowExpandable: (record) => record.type - 1,
                    // }}
                    style={{ width: "100%" }}
                    size="large"
                    loading={loadingTable}
                    pagination={false}
                    scroll={{
                      x: "calc(100vw - 100)",
                      y: "calc(100vh - 368px)",
                    }}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          history.push(
                            `/category/detail/${record.type}/${
                              record.key.split("_")[0]
                            }`
                          );
                        }, // click row
                      };
                    }}
                    indentSize={140}
                    expandIcon={({ expanded, record, onExpand }) => {
                      if (!record.children || !record.children.length)
                        return (
                          <span
                            style={{
                              marginRight: 40,
                            }}
                          />
                        );

                      return !expanded ? (
                        <PlusOutlined
                          width={35}
                          height={35}
                          style={{
                            marginRight: 20,
                            color: colors.main,
                            fontSize: 20,
                          }}
                          onClick={(e) => onExpand(record, e)}
                        />
                      ) : (
                        <MinusOutlined
                          width={35}
                          height={35}
                          style={{
                            marginRight: 20,
                            color: colors.main,
                            fontSize: 20,
                          }}
                          onClick={(e) => onExpand(record, e)}
                        />
                      );
                    }}
                    onChangePage={(page) => {
                      setLoadingTable(true);
                      setFilter({ ...filter, page: page });
                    }}
                  ></IStyledTable>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default ListProductCategory;
