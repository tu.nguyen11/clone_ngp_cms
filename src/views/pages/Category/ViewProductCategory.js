import React, {useState, useEffect} from "react";

import {Row, Form, Col, DatePicker, message, Radio, Skeleton} from "antd";

import {useHistory, useParams} from "react-router-dom";

import locale from "antd/es/date-picker/locale/vi_VN";

import moment from "moment";

import styled from "styled-components";

import {
  ITitle,
  IButton,
  ISvg,
  IInput,
  ISelect,
  ISelectCity,
  IUpload,
} from "../../components";
import {colors} from "../../../assets";
import {ImageType} from "../../../constant";
import {APIService} from "../../../services";
import {IDatePickerFrom, IInputText} from "../../components/common";
import {
  _getAPIListCounty,
  _getAPIListWard,
} from "../../containers/hookcustom/ApiCustomHook";
import {StyledForm} from "../../components/styled/form/styledForm";

const StyledRadio = styled(Radio)`
  margin-bottom: 20px;
  .ant-radio-inner {
    border-color: ${colors.main};
  }
  .ant-radio-checked .ant-radio-inner {
    background-color: ${colors.main};
  }
  .ant-radio-checked .ant-radio-inner::after {
    background-color: #fff;
  }
  .ant-radio-checked .ant-radio-inner {
    border-color: ${colors.main};
  }
  span {
    font-weight: 500;
    font-size: 18;
  }
`;

function CreateProductCategory(props) {
  const {viewType, typeId, id} = useParams();
  const history = useHistory();

  const [state, setState] = useState({
    type: typeId ? Number(typeId) : 1,
    status: 0,
    product_type_parent_id: 0,
    product_type_id: 0,
    name: "",
    image_path: null,
    image_url: null,
    url: "",
  });
  const [categoryList, setCategoryList] = useState([]);
  const [categoryListChildren, setCategoryListChildren] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (viewType === "detail") {
      console.log("Use eeff");
      _getCategoryDetail();
      return;
    }
  }, []);
  useEffect(() => {
    if (typeId && id) {
      _getCategoryDetail();
    }
  }, [typeId, id]);
  useEffect(() => {
    if (state.product_type_parent_id) {
      getListSubCategory(state.product_type_parent_id);
      return;
    }
  }, [state.product_type_parent_id, state.type]);
  useEffect(() => {
    if (!categoryList || !categoryList.length) {
      getListProductType();
    }
    if (state.type !== 1 && viewType !== "detail") {
      setCategoryListChildren([]);
      setState({
        ...state,
        // type: typeId,
        status: 0,
        product_type_parent_id: 0,
        product_type_id: 0,
        cate: 0,
      });
    }
  }, [state.type]);
  useEffect(() => {
    console.log("state change ne", state);
  }, [state]);

  const _getCategoryDetail = async () => {
    try {
      setIsLoading(true);
      let obj = {id: id, type: typeId};
      const data = await APIService._getDetailProductCategory(obj);
      console.log("State detail");
      setState({
        ...state,
        ...data,
        type: typeId * 1,
        image_path: data.image,
        image_url: data.image_url
      });
      setIsLoading(false);
    } catch (err) {
      setIsLoading(false);
      console.log("Get detail err", err);
    }
  };
  const getListProductType = async () => {
    try {
      const {categories} = await APIService._getListBrandCategory(0);
      const newData = categories.map((item) => {
        const key = item.id;
        const value = item.name;
        return {key, value};
      });
      setCategoryList(newData);
      console.log("new Data ne", newData);
    } catch (err) {
      console.log(err);
    }
  };
  const getListSubCategory = async (parentId = 0) => {
    try {
      const data = await APIService._getListSubCategpry(parentId);
      let sub_category = data.categories.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      console.log("Data ne category", data);
      setCategoryListChildren(sub_category);
      // setLoadingGroupProduct2(false);
    } catch (err) {
      console.log(err);
      // setLoadingGroupProduct2(false);
    }
  };

  const _handleChange = (e) => {
    if (e.target.name !== "type") {
      if (e.target.name === "product_type_parent_id") {
        setState({
          ...state,
          [e.target.name]: e.target.value,
          product_type_id: null,
        });
      } else {
        setState({...state, [e.target.name]: e.target.value, image_url: e.target.image_url});
      }

      return;
    }
    setState({
      ...state,
      type: e.target.value,
      status: 0,
      product_type_parent_id: 0,
      product_type_id: 0,
      name: "",
      image_url: e.target.image_url
    });
  };
  const showError = () => {
    let x = state.type;
    console.log("Type", state.type);
    switch (x) {
      case 1: {
        if (!state.name) {
          message.warning("Vui lòng nhập tên loại xe");
          return false;
        }
        if (!state.image_path) {
          message.warning("Vui lòng thêm ảnh");
          return false;
        }
        return true;
      }
      case 2: {
        if (!state.name) {
          message.warning("Vui lòng nhập tên dòng xe");
          return false;
        }
        if (!state.product_type_parent_id) {
          message.warning("Vui lòng chọn loại xe");
          return false;
        }
        if (!state.image_path) {
          message.warning("Vui lòng thêm ảnh");
          return false;
        }
        return true;
      }
      case 3: {
        console.log("Vaof 3 ne", state.type);
        if (!state.name) {
          message.warning("Vui lòng nhập tên thương hiệu");
          return false;
        }
        if (!state.product_type_parent_id) {
          message.warning("Vui lòng chọn loại xe");
          return false;
        }
        if (!state.product_type_id) {
          message.warning("Vui lòng chọn dòng xe");
          return false;
        }

        return true;
      }
      default: {
        return true;
      }
    }
  };

  const onSubmit = (e) => {
    console.log("State submit", state);
    let flag = showError();
    if (!flag) return;
    if (viewType === "edit") {
      postUpdateCategory();
      return;
    }
    postAddNewCategory();
  };
  const postAddNewCategory = async () => {
    try {
      await APIService._postAddNewProductCategory(state);
      message.success("Thêm danh mục thành công");
      history.goBack();
    } catch (err) {
      console.log("Add categort err", err);
    }
  };
  const postUpdateCategory = async () => {
    try {
      let params = {...state};
      if (Number(typeId) === 1) {
        params.product_type_parent_id = Number(id);
      }
      if (Number(typeId) === 2) {
        params.product_type_id = Number(id);
      }
      if (Number(typeId) === 3) {
        params.cate_id = Number(id);
      }
      let img = `${state.image_path}`;
      img = img.replace(state.image_url, "");
      params.image_path = img;
      await APIService._postUpdateProductCategory(params);
      message.success("Chỉnh sửa danh mục thành công");
      history.goBack();
      await _getCategoryDetail();
    } catch (err) {
      console.log("Add categort err", err);
    }
  };

  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        paddingBottom: 20,
      }}
    >
      <Row gutter={[0, 30]} style={{height: "100%"}}>
        <Col>
          <ITitle
            level={1}
            title={
              viewType === "edit"
                ? "Chỉnh sửa danh mục sản phẩm"
                : viewType === "detail"
                ? "Chi tiết danh mục sản phẩm"
                : "Tạo mới danh mục sản phẩm"
            }
            style={{
              color: colors.mainDark,
              fontWeight: "bold",
              marginTop: 14,
            }}
          />
        </Col>
        <Col span={24}>
          <Row>
            <Col
              span={24}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              {viewType === "edit" ? (
                <React.Fragment>
                  <IButton
                    icon={ISvg.NAME.SAVE}
                    title={"Lưu"}
                    styleHeight={{
                      width: 120,
                    }}
                    color={colors.main}
                    onClick={onSubmit}
                  />
                  <IButton
                    icon={ISvg.NAME.CROSS}
                    title={"Huỷ"}
                    styleHeight={{
                      width: 120,
                      marginLeft: 20,
                    }}
                    color={colors.red}
                    onClick={() => {
                      history.goBack();
                    }}
                  />
                </React.Fragment>
              ) : viewType === "create" ? (
                <React.Fragment>
                  {" "}
                  <IButton
                    icon={ISvg.NAME.SAVE}
                    title={"Lưu"}
                    styleHeight={{
                      width: 120,
                    }}
                    color={colors.main}
                    onClick={onSubmit}
                  />
                  <IButton
                    icon={ISvg.NAME.CROSS}
                    title={"Huỷ"}
                    styleHeight={{
                      width: 120,
                      marginLeft: 20,
                    }}
                    color={colors.red}
                    onClick={() => {
                      history.goBack();
                    }}
                  />
                </React.Fragment>
              ) : (
                <IButton
                  // icon={ISvg.NAME.CROSS}
                  title={"Chỉnh sửa"}
                  styleHeight={{
                    width: 120,
                    marginLeft: 20,
                  }}
                  color={colors.main}
                  onClick={() => {
                    history.push(`/category/edit/${typeId}/${id}`);
                  }}
                />
              )}
            </Col>
          </Row>
        </Col>
        <Col style={{height: "100%"}} span={24}>
          <Row>
            <Col
              className="box-shadow"
              span={4}
              style={{minHeight: 500, padding: 36}}
            >
              <Skeleton loading={isLoading}>
                <React.Fragment>
                  <Row type="flex" style={{flexDirection: "column"}}>
                    <Col>
                      <ITitle
                        title="Cây danh mục"
                        level={2}
                        style={{fontWeight: "bold"}}
                      />
                    </Col>
                    <Col>
                      {viewType === "edit" || viewType === "create" ? (
                        <Radio.Group
                          disabled={viewType === "edit"}
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            marginTop: 20,
                          }}
                          size="large"
                          value={state.type}
                          name="type"
                          onChange={(e) => {
                            console.log("Change e ne", e);
                            _handleChange(e);
                          }}
                        >
                          <StyledRadio value={1}>Loại xe</StyledRadio>
                          <StyledRadio value={2}>Dòng xe</StyledRadio>
                          <StyledRadio value={3}>Thương hiệu</StyledRadio>
                        </Radio.Group>
                      ) : (
                        <span>
                          {state.type === 1
                            ? "Loại xe"
                            : state.type === 2
                              ? "Dòng xe"
                              : "Thương hiệu"}
                        </span>
                      )}
                    </Col>
                  </Row>
                </React.Fragment>
              </Skeleton>
            </Col>
            <Col
              className="box-shadow"
              span={8}
              style={{
                minHeight: 500,
                padding: 36,
                marginLeft: 20,
                marginRight: 20,
              }}
            >
              <Skeleton loading={isLoading}>
                <ITitle
                  title="Thông tin danh mục sản phẩm"
                  level={2}
                  style={{fontWeight: "bold"}}
                />
                {state.type === 1 ? (
                  <React.Fragment>
                    <Row
                      style={{
                        margin: "20px 0",
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      <ITitle
                        title="Tên loại xe"
                        level={4}
                        style={{fontWeight: "bold", marginBottom: 10}}
                      />
                      {viewType === "edit" || viewType === "create" ? (
                        <IInputText
                          name="name"
                          key="name"
                          onChange={_handleChange}
                          placeholder="Nhập tên loại xe"
                          value={state.name ? state.name : null}
                        />
                      ) : (
                        <span>{state.name}</span>
                      )}
                    </Row>
                  </React.Fragment>
                ) : state.type === 2 ? (
                  <React.Fragment>
                    <Row style={{margin: "20px 0"}}>
                      <ITitle
                        title="Tên dòng xe"
                        level={4}
                        style={{fontWeight: "bold", marginBottom: 10}}
                      />
                      {viewType === "edit" || viewType === "create" ? (
                        <IInputText
                          name="name"
                          key="name"
                          onChange={_handleChange}
                          placeholder="Nhập tên dòng xe"
                          value={state.name ? state.name : null}
                        />
                      ) : (
                        <span>{state.name}</span>
                      )}
                    </Row>
                    <Row style={{margin: "20px 0"}}>
                      <ITitle
                        title="Loại xe"
                        level={4}
                        style={{fontWeight: "bold", marginBottom: 10}}
                      />
                      {viewType === "edit" || viewType === "create" ? (
                        <ISelect
                          value={
                            state.product_type_parent_id
                              ? state.product_type_parent_id
                              : undefined
                          }
                          select
                          isBackground={false}
                          name="product_type_parent_id"
                          key="product_type_parent_id"
                          onChange={(value) => {
                            _handleChange({
                              target: {
                                name: "product_type_parent_id",
                                value,
                              },
                            });
                          }}
                          data={categoryList}
                          placeholder="Chọn loại xe"
                        />
                      ) : (
                        <span>{state.product_type_parent_name}</span>
                      )}
                    </Row>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <Row style={{margin: "20px 0"}}>
                      <ITitle
                        title="Tên thương hiệu"
                        level={4}
                        style={{fontWeight: "bold", marginBottom: 10}}
                      />
                      {viewType === "edit" || viewType === "create" ? (
                        <IInputText
                          name="name"
                          key="name"
                          value={state.name ? state.name : null}
                          onChange={_handleChange}
                          placeholder="Nhập tên thương hiệu"
                        />
                      ) : (
                        <span>{state.name}</span>
                      )}
                    </Row>
                    <Row style={{margin: "20px 0"}}>
                      <ITitle
                        title="Loại xe"
                        level={4}
                        style={{fontWeight: "bold", marginBottom: 10}}
                      />
                      {viewType === "edit" || viewType === "create" ? (
                        <ISelect
                          value={
                            state.product_type_parent_id
                              ? state.product_type_parent_id
                              : undefined
                          }
                          isBackground={false}
                          select
                          name="product_type_parent_id"
                          key="product_type_parent_id"
                          onChange={(value) =>
                            _handleChange({
                              target: {name: "product_type_parent_id", value},
                            })
                          }
                          data={categoryList}
                          placeholder="Chọn loại xe"
                        />
                      ) : (
                        <span>{state.product_type_parent_name}</span>
                      )}
                    </Row>
                    <Row style={{margin: "20px 0"}}>
                      <ITitle
                        title="Dòng xe"
                        level={4}
                        style={{fontWeight: "bold", marginBottom: 10}}
                      />
                      {viewType === "edit" || viewType === "create" ? (
                        <ISelect
                          isBackground={false}
                          select
                          name="product_type_id"
                          key="product_type_id"
                          onChange={(value) =>
                            _handleChange({
                              target: {name: "product_type_id", value},
                            })
                          }
                          data={categoryListChildren}
                          placeholder="Chọn dòng xe"
                          value={
                            state.product_type_id
                              ? state.product_type_id
                              : undefined
                          }
                        />
                      ) : (
                        <span>{state.product_type_name}</span>
                      )}
                    </Row>
                  </React.Fragment>
                )}

                <Row style={{display: "flex", flexDirection: "column"}}>
                  <ITitle
                    title="Trạng thái"
                    level={4}
                    style={{fontWeight: "bold"}}
                  />
                  {viewType === "edit" || viewType === "create" ? (
                    <Radio.Group
                      style={{
                        display: "flex",
                        marginTop: 10,
                      }}
                      size="large"
                      name="status"
                      value={state.status}
                      onChange={(e) => {
                        _handleChange(e);
                      }}
                    >
                      <StyledRadio value={0}>Ẩn</StyledRadio>
                      <StyledRadio value={1}>Hiện</StyledRadio>
                    </Radio.Group>
                  ) : (
                    <span style={{marginTop: 10}}>
                      {state.status === 0 ? "Ẩn" : "Hiện"}
                    </span>
                  )}
                </Row>
              </Skeleton>
            </Col>
            {state.type !== 3 ? (
              <Col
                xxl={4}
                xl={6}
                lg={7}
                md={8}
                sm={10}
                style={{minHeight: 500, padding: 36}}
                className="box-shadow"
              >
                <Skeleton loading={isLoading}>
                  <Row>
                    <Col
                      span={24}
                      style={{display: "flex", flexDirection: "column"}}
                    >
                      <ITitle
                        title="Hình ảnh sản phẩm"
                        level={2}
                        style={{fontWeight: "bold", marginBottom: 20}}
                      />
                      {viewType === "create" ? (
                        <IUpload
                          url={state.image_url}
                          name={state.image_path}
                          type={ImageType.PRODUCT_TYPE}
                          callback={(images, image_url) => {
                            _handleChange({
                              target: {
                                value: images[0],
                                name: "image_path",
                                image_url
                              },
                            })
                          }
                          }
                        />
                      ) : viewType === "edit" ? (
                        <IUpload
                          url={state.image_url}
                          name={state.image_path}
                          type={ImageType.PRODUCT_TYPE}
                          callback={(images, image_url) =>
                            _handleChange({
                              target: {
                                value: images[0],
                                name: "image_path",
                                image_url
                              },
                            })
                          }
                        />
                      ) : (
                        <img
                          alt="picture"
                          src={
                            state.image_url +
                            state.image_path
                          }
                          width={180}
                          height={180}
                        />
                      )}
                    </Col>
                  </Row>
                </Skeleton>
              </Col>
            ) : null}
          </Row>
        </Col>
      </Row>
    </div>
  );
}

const createProductCategory = Form.create({
  name: "CreateProductCategory",
})(CreateProductCategory);

export default createProductCategory;
