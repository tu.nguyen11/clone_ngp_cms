import { Col, message, Row, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { colors, images } from "../../../assets";
import { APIService } from "../../../services";
import {
  IButton,
  IDatePicker,
  ISearch,
  ISelect,
  ISvg,
  ITable,
  ITitle,
} from "../../components";
import { StyledITitle } from "../../components/common/Font/font";
import { useHistory, useParams } from "react-router";
import { priceFormat } from "../../../utils";
import FormatterDay from "../../../utils/FormatterDay";
const NewDate = new Date();
const DateStart = new Date();
let startValue = DateStart.setMonth(DateStart.getMonth() - 1);

export default function HistoryUpdateDebitPage() {
  const paramsAgency = useParams();
  const [loadingTable, setLoadingTable] = useState(true);
  const [dataTable, setDataTable] = useState({});
  const [filterTable, setFilterTable] = useState({
    user_agency_id: paramsAgency.id,
    startValue: startValue,
    endValue: NewDate.setHours(23, 59, 59, 999),
    page: 1,
  });

  useEffect(() => {
    _fetchAPIHistoryUpdateDebit();
  }, [filterTable]);

  const _fetchAPIHistoryUpdateDebit = async () => {
    try {
      const data = await APIService._getListOrderDebit(
        filterTable.user_agency_id,
        filterTable.startValue,
        filterTable.endValue,
        filterTable.page
      );
      data.response.map(
        (item, index) => (item.stt = (filterTable.page - 1) * 10 + index + 1)
      );
      setDataTable(data);
      setLoadingTable(false);
      console.log(data);
    } catch (error) {
      setLoadingTable(false);
    }
  };
  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Thời gian cập nhật",
      dataIndex: "thoiGianCapNhat",
      key: "statthoiGianXacNhanus_name",
      render: (thoiGianCapNhat) => (
        <span>
          {FormatterDay.dateFormatWithString(
            thoiGianCapNhat,
            "#DD#/#MM#/#YYYY# - #hhh#:#mm#"
          )}
        </span>
      ),
    },
    {
      title: "Hạn mức tín dụng ",
      dataIndex: "hanMucTinDung",
      key: "hanMucTinDung",
      render: (hanMucTinDung) => <span>{priceFormat(hanMucTinDung, "")}</span>,
    },
    // {
    //   title: "Hạn mức khả dụng",
    //   dataIndex: "hanMucKhaDung",
    //   key: "hanMucKhaDung",
    //   render: (hanMucKhaDung) => (
    //     <span>
    //       {!hanMucKhaDung || hanMucKhaDung < 0
    //         ? 0
    //         : priceFormat(hanMucKhaDung, "")}
    //     </span>
    //   ),
    // },

    {
      title: "Công nợ cuối kỳ ",
      dataIndex: "congNoCuoiKy",
      key: "congNoCuoiKy",
      render: (congNoCuoiKy) => <span>{priceFormat(congNoCuoiKy, "")}</span>,
    },
    {
      title: "Hạn mức gối đầu",
      dataIndex: "hanMucGoiDau",
      key: "hanMucGoiDau",
      render: (hanMucGoiDau) => <span>{priceFormat(hanMucGoiDau, "")}</span>,
    },
    {
      title: "Hạn nợ",
      dataIndex: "hanNo",
      key: "hanNo",
      render: (hanNo) => (
        <span>
          {hanNo === null || hanNo === undefined ? "" : `${hanNo} Ngày`}
        </span>
      ),
    },
  ];

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Lịch sử cập nhật công nợ
          </StyledITitle>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <div className="flex justify-end" style={{ alignItems: "center" }}>
              <ISvg
                name={ISvg.NAME.EXPERIMENT}
                with={16}
                height={16}
                fill={colors.icon.default}
              />
              <ITitle
                title="Từ ngày"
                level={4}
                style={{
                  marginLeft: 25,
                  marginRight: 25,
                }}
              />
              <IDatePicker
                isBackground
                from={filterTable.startValue}
                to={filterTable.endValue}
                onChange={(date, dateString) => {
                  if (date.length <= 1) {
                    return;
                  }

                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    startValue: date[0]._d.setHours(0, 0, 0),
                    endValue: date[1]._d.setHours(23, 59, 59),
                    page: 1,
                  });
                }}
                style={{
                  background: colors.white,
                  borderWidth: 1,
                  borderColor: colors.line,
                  borderStyle: "solid",
                }}
              />
            </div>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={dataTable.response}
                style={{ width: "100%" }}
                defaultCurrent={1}
                sizeItem={dataTable.size}
                rowKey="id"
                indexPage={filterTable.page}
                maxpage={dataTable.total_record}
                loading={loadingTable}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilterTable({ ...filterTable, page: page });
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}
