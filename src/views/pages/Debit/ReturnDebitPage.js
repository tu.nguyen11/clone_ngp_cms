import React, { useEffect, useState, useRef } from "react";
import {
  IButton,
  IInput,
  ISearch,
  ISelect,
  ISvg,
  ITable,
  ITitle,
} from "../../components";
import { colors, images } from "../../../assets";
import { Row, Col, Tooltip, Modal, Radio, AutoComplete, message } from "antd";
import RadioGroup from "antd/lib/radio/group";
import { APIService } from "../../../services";
import { priceFormat } from "../../../utils";

function ReturnDebitPage(props) {
  const typeTimeRef = useRef(null);
  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
      render: (stt) => <span>{stt}</span>,
    },
    {
      title: "Tên đại lý",
      dataIndex: "shop_name",
      key: "shop_name",
      render: (shop_name) =>
        !shop_name ? (
          "-"
        ) : (
          <Tooltip title={shop_name}>
            <span>{shop_name}</span>
          </Tooltip>
        ),
    },
    {
      title: "Mã đại lý",
      dataIndex: "code",
      key: "code",
      render: (code) =>
        !code ? (
          "-"
        ) : (
          <Tooltip title={code}>
            <span>{code}</span>
          </Tooltip>
        ),
    },

    {
      title: "Cấp bậc",
      dataIndex: "membership_name",
      key: "membership_name",
      render: (membership_name) => (
        <span>{!membership_name ? "-" : membership_name}</span>
      ),
    },
    {
      title: "Hoàn trả công nợ",
      dataIndex: "money",
      key: "money",
      render: (money) => <span>{priceFormat(money)}</span>,
    },
    {
      title: "Công nợ cuối kỳ",
      dataIndex: "debt_final",
      key: "debt_final",
      render: (debt_final) => <span>{priceFormat(debt_final)}</span>,
    },
    {
      title: "Lý do",
      dataIndex: "reason",
      key: "reason",
      render: (reason) =>
        !reason ? (
          "-"
        ) : (
          <Tooltip title={reason}>
            <div
              style={{
                wordWrap: "break-word",
                wordBreak: "break-word",
                overflow: "hidden",
                maxWidth: 200,
              }}
              className="ellipsis-text"
            >
              <ITitle level={4} title={reason} />
            </div>
          </Tooltip>
        ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      render: (status_name) => <span>{!status_name ? "-" : status_name}</span>,
    },
  ];

  const listStatus = [
    {
      key: 2,
      value: "Tất cả",
    },
    {
      key: 0,
      value: "Chờ xác nhận",
    },
    {
      key: 1,
      value: "Đã xác nhận",
    },
    {
      key: -1,
      value: "Hủy",
    },
  ];

  // Api list debt
  const [filterDataDebt, setFilterDataDebt] = useState({
    membership_id: 0,
    status: 2,
    key: "",
    page: 1,
  });

  const [loadingButton, setLoadingButton] = useState(false);

  const [dataTableDebt, setDataTableDebt] = useState({
    listDebt: [],
    size: 10,
    total: 0,
  });

  const _getListDataReturnDebt = async (filter) => {
    try {
      const data = await APIService._getListReturnDebt(
        filter.membership_id,
        filter.status,
        filter.key,
        filter.page
      );
      data.return_purcharsed.map((item, index) => {
        item.stt = (filterDataDebt.page - 1) * 10 + index + 1;
        item.rowTable = JSON.stringify({
          status: item.status,
          id: item.id,
        });
        item.disabled = item.status === -1 || item.status === 1 ? true : false;
      });

      setDataTableDebt({
        listDebt: data.return_purcharsed,
        size: data.size,
        total: data.total,
      });
      setLoadingTable(false);
    } catch (error) {
      console.log("err", error);
      setLoadingTable(false);
    }
  };

  const _postConfirmReturnDebt = async (arr) => {
    try {
      const data = await APIService._postConfirmReturnDebt(arr);
      setSelectedRowKeys([]);
      setLoadingButton(false);
      setModalDebt(false);
      await _getListDataReturnDebt(filterDataDebt);
      message.success("Xác nhận giao dịch thành công");
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
      setModalDebt(false);
    }
  };

  const _postRejectReturnDebt = async (arr) => {
    try {
      const data = await APIService._postRejectReturnDebt(arr);
      setSelectedRowKeys([]);
      setLoadingButton(false);
      setModalDebt(false);
      await _getListDataReturnDebt(filterDataDebt);
      message.success("Hủy giao dịch thành công");
    } catch (error) {
      console.log(error);
      setLoadingButton(false);
      setModalDebt(false);
    }
  };

  const [moneyAgencyReturn, setMoneyAgencyReturn] = useState({
    money: 0,
    reason: "",
  });

  const [agencID, setAgencyID] = useState("");

  const _postDataReturnDebt = async () => {
    let monney = Number(moneyAgencyReturn.money.split(",").join(""));
    let reason = moneyAgencyReturn.reason;
    if (monney <= 0 || !monney || !agencID || !reason) {
      message.warning("Vui lòng nhập đầy đủ thông tin");
      return;
    }

    const data = await APIService._postReturnDebt({
      user_id: agencID,
      monney: monney,
      reason,
    });
    message.success("Tạo giao dịch hoàn trả công nợ thành công");
    await _getListDataReturnDebt(filterDataDebt);
  };

  useEffect(() => {
    _getListDataReturnDebt(filterDataDebt);
  }, [filterDataDebt]);

  const [visibleModal, setVisbleModal] = useState(false);
  const [modalDebt, setModalDebt] = useState(false);

  const showModal = () => {
    setVisbleModal(true);
  };

  const cancelDebt = (e) => {
    setModalDebt(false);
  };
  const handleChangeRadio = (e) => {
    setValue(e.target.value);
  };

  const [value, setValue] = useState(0);

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys) => {
      console.log(selectedRowKeys);
      setSelectedRowKeys(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      disabled: record.disabled,
    }),
  };

  const [loadingTable, setLoadingTable] = useState(true);
  const { Option } = AutoComplete;

  function removeVietnameseTones(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    // Remove extra spaces
    // Bỏ các khoảng trắng liền nhau
    str = str.replace(/ + /g, " ");
    str = str.trim();
    // Remove punctuations
    // Bỏ dấu câu, kí tự đặc biệt
    str = str.replace(
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
      " "
    );
    return str;
  }
  const [dataLevel, setDataLevel] = useState([]);

  const _getDataLevel = async () => {
    try {
      const data = await APIService._getListLevel();
      const newData = data.membership.map((item) => {
        const key = item.id;
        const value = item.name;
        return { key, value };
      });
      newData.unshift({
        key: 0,
        value: "Tất cả",
      });
      setDataLevel(newData);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    _getDataLevel();
  }, []);

  const [listS1, setListS1] = useState([]);
  const [filterListS1, setFilterListS1] = useState({
    status: 2,
    page: 1,
    city_id: 0,
    agency_id: -1,
    keySearch: "",
    membership_id: 0,
  });

  const _getListAgency1 = async () => {
    if (filterListS1.page === 1) {
      setListS1([]);
    }
    const data = await APIService._getListAgencyS1Droplist(
      filterListS1.page,
      filterListS1.keySearch
    );
    const newData = data.useragency.map((item) => {
      const id = item.id;
      const value = item.dms_code.toLowerCase();
      const text = item.dms_code;
      return { id, value, text };
    });
    setListS1(newData);
  };

  useEffect(() => {
    _getListAgency1();
  }, [filterListS1]);

  const [dataDetail, setDataDetail] = useState({
    user_agency: {},
    image_url: "",
    loading: false,
  });

  const _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailAgencyS1(idAgency);
      setDataDetail(data);
    } catch (error) {
      console.log(error);
    }
  };

  const { user_agency } = dataDetail;

  const renderAutoOption = (option, search) => {
    console.log("listS1", listS1);
    const renderOption = option.map((item) => {
      let keySearch = removeVietnameseTones(search);
      let itemText = removeVietnameseTones(item.text);

      let index = itemText.toLowerCase().indexOf(keySearch.toLowerCase());
      if (index > -1) {
        let text1 = item.text.slice(0, index);
        let text2 = item.text.slice(index, index + keySearch.length);
        let text3 = item.text.slice(index + keySearch.length);
        return (
          <Option key={item.id} text={item.text}>
            <span>{text1}</span>
            <span>{text2}</span>
            <span>{text3}</span>
          </Option>
        );
      } else {
        return (
          <Option key={item.id} text={item.text}>
            <span>{item.text}</span>
          </Option>
        );
      }
    });
    return renderOption;
  };

  return (
    <div>
      <Row gutter={[0, 30]}>
        <Col>
          <Row>
            <Col span={12}>
              <ITitle
                title="Hoàn trả công nợ ĐLC1"
                style={{
                  color: colors.mainDark,
                  fontSize: 22,
                  fontWeight: "bold",
                }}
              />
            </Col>
            <Col span={12}>
              <div className="flex justify-end">
                <Tooltip title="Tìm kiếm theo mã, tên đại lý">
                  <ISearch
                    className="cursor"
                    placeholder="Tìm kiếm theo mã, tên đại lý"
                    icon={
                      <div
                        style={{
                          display: "flex",
                          width: 42,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <img
                          src={images.icSearch}
                          style={{ width: 16, height: 16 }}
                        />
                      </div>
                    }
                    onPressEnter={(e) => {
                      setLoadingTable(true);
                      setFilterDataDebt({
                        ...filterDataDebt,
                        key: e.target.value,
                        page: 1,
                      });
                    }}
                  ></ISearch>
                </Tooltip>
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={8}>
              <div style={{ display: "flex" }}>
                <div style={{ width: 190 }}>
                  <ISelect
                    defaultValue="Cấp bậc"
                    data={dataLevel}
                    select={true}
                    onChange={(value) => {
                      setFilterDataDebt({
                        ...filterDataDebt,
                        membership_id: value,
                      });
                    }}
                  />
                </div>
                <div style={{ width: 190, marginLeft: 20 }}>
                  <ISelect
                    defaultValue="Trạng thái"
                    data={listStatus}
                    select={true}
                    onChange={(value) => {
                      setFilterDataDebt({
                        ...filterDataDebt,
                        status: value,
                      });
                    }}
                  />
                </div>
              </div>
            </Col>
            <Col span={8}></Col>
            <Col span={8}>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <IButton
                  title="Duyệt công nợ"
                  color={colors.mainDark}
                  onClick={() => {
                    if (selectedRowKeys.length === 0) {
                      message.warning("Vui lòng chọn giao dịch!");
                      return;
                    }
                    setModalDebt(true);
                  }}
                />
                <IButton
                  title="Tạo mới"
                  color={colors.mainDark}
                  icon={ISvg.NAME.ARROWUP}
                  style={{ marginLeft: 20 }}
                  onClick={showModal}
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <ITable
            columns={columns}
            data={dataTableDebt.listDebt}
            scroll={{ x: 0 }}
            rowSelection={rowSelection}
            sizeItem={dataTableDebt.size}
            loading={loadingTable}
            indexPage={filterDataDebt.page}
            maxpage={dataTableDebt.total}
            rowKey="rowTable"
            onChangePage={(page) => {
              setFilterDataDebt({
                ...filterDataDebt,
                page: page,
              });
            }}
          />
        </Col>
      </Row>

      <Modal visible={visibleModal} closable={false} footer={null}>
        <Row gutter={[0, 30]}>
          <Col>
            <ITitle
              title="GIAO DỊCH THANH TOÁN ĐLC1"
              style={{
                color: colors.mainDark,
                display: "flex",
                justifyContent: "center",
                fontWeight: "bold",
              }}
            />
          </Col>
          <Col>
            <Row gutter={[0, 20]}>
              <Col>
                <span>Mã đại lý</span>

                <AutoComplete
                  dataSource={renderAutoOption(listS1, filterListS1.keySearch)}
                  optionLabelProp="text"
                  onSelect={(idAgency) => {
                    setAgencyID(idAgency);
                    // setMoneyAgencyReturn({""})
                    _getAPIDetailAgency(idAgency);

                    setFilterListS1({
                      ...filterListS1,
                      keySearch: "",
                      page: 1,
                    });
                  }}
                  placeholder="Vui lòng nhập mã đại lý"
                  onSearch={(value) => {
                    if (typeTimeRef.current) {
                      clearTimeout(typeTimeRef.current);
                    }

                    typeTimeRef.current = setTimeout(() => {
                      setFilterListS1({
                        ...filterListS1,
                        keySearch: value,
                        page: 1,
                      });
                    }, 1500);
                  }}
                />
              </Col>
              <Col>
                <Row>
                  <div style={{ display: "flex" }}>
                    <Col span={12}>
                      <div style={{ display: "flex", flexDirection: "column" }}>
                        <span>Tên đại lý</span>
                        <span
                          style={{
                            color: colors.text.black,
                            fontSize: 15,
                          }}
                        >
                          {!user_agency.shop_name ? "-" : user_agency.shop_name}
                        </span>
                      </div>
                    </Col>
                    <Col span={12}>
                      <div style={{ display: "flex", flexDirection: "column" }}>
                        <span>Số điện thoại</span>
                        <span
                          style={{
                            color: colors.text.black,
                            fontSize: 15,
                          }}
                        >
                          {!user_agency.phone ? "-" : user_agency.phone}
                        </span>
                      </div>
                    </Col>
                  </div>
                </Row>
              </Col>

              <Col>
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <span>Địa chỉ</span>
                  <span
                    style={{
                      color: colors.text.black,
                      fontSize: 15,
                    }}
                  >
                    {!user_agency.address
                      ? "-"
                      : user_agency.address +
                        ", " +
                        user_agency.ward_name +
                        ", " +
                        user_agency.district_name +
                        ", " +
                        user_agency.cities_name}
                  </span>
                </div>
              </Col>
              <Col>
                <span>Giao dịch thanh toán</span>
                <IInput
                  placeholder="Nhập số tiền giao dịch"
                  inputpadding={0}
                  onChange={(e) => {
                    let price = e.target.value;
                    setMoneyAgencyReturn({
                      ...moneyAgencyReturn,
                      money: price,
                    });
                  }}
                  value={moneyAgencyReturn.money
                    .toString()
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                />
              </Col>
              <Col>
                <span>Lý do cập nhật</span>
                <IInput
                  placeholder="Nhập lý do giao dịch thanh toán"
                  inputpadding={0}
                  onChange={(e) => {
                    let reason = e.target.value;
                    setMoneyAgencyReturn({
                      ...moneyAgencyReturn,
                      reason: reason,
                    });
                  }}
                  value={moneyAgencyReturn.reason}
                />
              </Col>
            </Row>
          </Col>
          <Col>
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                position: "absolute",
                marginTop: -82,
                bottom: -73,
                right: -22,
              }}
            >
              <IButton
                title="Huỷ bỏ"
                icon={ISvg.NAME.CROSS}
                color={colors.red}
                onClick={() => {
                  setVisbleModal(false);
                }}
              />
              <IButton
                title="Xác nhận"
                color={colors.mainDark}
                style={{ marginLeft: 15 }}
                onClick={() => {
                  if (agencID === "") {
                    message.warning("Vui lòng chọn đại lý");
                    return;
                  }
                  if (
                    moneyAgencyReturn.money === 0 ||
                    moneyAgencyReturn.money === ""
                  ) {
                    message.warning("Vui lòng nhập số tiền hoàn trả công nợ");
                    return;
                  }
                  if (moneyAgencyReturn.reason === "") {
                    message.warning("Vui lòng nhập lý do");
                    return;
                  }
                  _postDataReturnDebt();
                  setVisbleModal(false);
                }}
              />
            </div>
          </Col>
        </Row>
      </Modal>
      <Modal visible={modalDebt} closable={false} footer={null} width={320}>
        <Row gutter={[0, 15]}>
          <Col>
            <ITitle
              title="Duyệt công nợ"
              style={{
                color: colors.mainDark,
                fontSize: 22,
                display: "flex",
                justifyContent: "center",
                fontWeight: "bold",
              }}
            ></ITitle>
          </Col>
          <Col>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <RadioGroup value={value} onChange={(e) => handleChangeRadio(e)}>
                <Col>
                  <Radio value={1}>Xác nhận công nợ</Radio>
                </Col>
                <Col>
                  <Radio value={2}>Huỷ công nợ</Radio>
                </Col>
              </RadioGroup>
            </div>
          </Col>
        </Row>
        <Row
          gutter={[17, 0]}
          style={{
            position: "absolute",
            bottom: -60,
            left: 0,
          }}
        >
          <Col span={12}>
            <div style={{ width: 153 }}>
              <IButton
                color={colors.red}
                title="Hủy bỏ"
                onClick={cancelDebt}
                icon={ISvg.NAME.CROSS}
              />
            </div>
          </Col>
          <Col span={12}>
            <div style={{ width: 153 }}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                icon={ISvg.NAME.CHECKMARK}
                loading={loadingButton}
                onClick={() => {
                  setLoadingButton(true);
                  let arrSelect = selectedRowKeys.map((item) => {
                    let itemParse = JSON.parse(item);
                    return itemParse.id;
                  });

                  // const obj = {
                  //   id: arrSelect,
                  // };

                  value === 1
                    ? _postConfirmReturnDebt(arrSelect)
                    : _postRejectReturnDebt(arrSelect);
                  setValue(0);
                }}
              />
            </div>
          </Col>
        </Row>
      </Modal>
    </div>
  );
}

export default ReturnDebitPage;
