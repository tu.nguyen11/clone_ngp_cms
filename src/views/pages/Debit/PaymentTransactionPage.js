import { AutoComplete, Col, message, Modal, Row, Tooltip } from "antd";
import React, { useEffect, useRef, useState } from "react";
import { colors, images } from "../../../assets";
import { APIService } from "../../../services";
import {
  IButton,
  IInput,
  ISearch,
  ISelect,
  ISvg,
  ITable,
  ITableHtml,
  ITitle,
} from "../../components";
import { StyledITitle } from "../../components/common/Font/font";
import { useHistory } from "react-router";
import { priceFormat } from "../../../utils";
import FormatterDay from "../../../utils/FormatterDay";
import ITransfer from "../../components/styled/modalTransfer/ITransfer";
import { IInputText } from "../../components/common";
import styled from "styled-components";

export default function PaymentTransactionPage(props) {
  const typeTimeRef = useRef(null);
  const { Option } = AutoComplete;

  const [loadingTable, setLoadingTable] = useState(true);
  const [dataTable, setDataTable] = useState({});
  const [filterTable, setFilterTable] = useState({
    level_id: 0,
    status: 0,
    page: 1,
    search: "",
  });
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [dataConfirm, setDataConfirm] = useState([]);
  const [dataDefaut, setDataDefault] = useState([]);
  const [dataListS1, setDataListS1] = useState([]);
  const [showPopupConfirm, setShowPopupConfirm] = useState(false);
  const [showPopupConfirm2, setShowPopupConfirm2] = useState(false);
  const [showPopupConfirmReject, setShowPopupConfirmReject] = useState(false);
  const [render, setRender] = useState(false);
  const [showPopupCreate, setShowPopupCreate] = useState(false);
  const [agencID, setAgencyID] = useState("");
  const [moneyAgencyConfirm, setMoneyAgencyConfirm] = useState("");
  const [filterListS1, setFilterListS1] = useState({
    status: 2,
    page: 1,
    city_id: 0,
    // district_id: 0,
    agency_id: -1,
    keySearch: "",
    membership_id: 0,
  });
  const [dataDetail, setDataDetail] = useState({
    user_agency: {},
    image_url: "",
    loading: false,
  });
  const [dataLevel, setDataLevel] = useState([]);
  const [isCreate, setIsCreate] = useState(0);
  const [loadingButton, setLoadingButton] = useState(false);

  const listStatus = [
    {
      value: "Tất cả",
      key: 0,
    },
    {
      value: "Chờ xác nhận",
      key: 1,
    },
    {
      value: "Đã xác nhận",
      key: 2,
    },
    {
      value: "Từ chối",
      key: 3,
    },
  ];

  const _fetchAPIListPaymentTransaction = async (filter) => {
    try {
      const data = await APIService._getListPaymentTransaction(
        filter.level_id,
        filter.status,
        filter.page,
        filter.search
      );
      data.data.map((item, index) => {
        item.stt = (filterTable.page - 1) * 10 + index + 1;

        item.disabled =
          item.trangThai == "Hủy" || item.trangThai == "Đã xác nhận"
            ? true
            : false;
      });
      setDataTable(data);
      setLoadingTable(false);
      console.log(data);
    } catch (error) {
      setLoadingTable(false);
    }
  };

  const _getAPIListLevel = async () => {
    try {
      const data = await APIService._getListLevel();
      const dataNew = data.membership.map((item, index) => {
        const key = item.id;
        const value = item.name;
        return {
          key,
          value,
        };
      });
      dataNew.unshift({
        value: "Tất cả",
        key: 0,
      });
      setDataLevel(dataNew);
    } catch (error) {}
  };

  const _postAPIRejectPayment = async () => {
    try {
      setLoadingTable(true);
      const array = dataConfirm.map((item) => {
        const obj = {
          id: item.id,
          money: 0,
        };
        return obj;
      });
      await APIService._postRejectPayment(array);
      //   selectedRowKeys([]);
      setLoadingTable(false);
      setShowPopupConfirm(false);
      message.success("Từ chối giao dịch thành công");
    } catch (error) {}
  };

  useEffect(() => {
    _getAPIListLevel();
    _getAPIDataconfirm();
  }, []);

  useEffect(() => {
    _getAPIListAgency();
  }, [filterListS1]);

  useEffect(() => {
    _fetchAPIListPaymentTransaction(filterTable);
  }, [filterTable]);

  const _getAPIListAgency = async (search) => {
    if (filterListS1.page == 1) {
      setDataListS1([]);
    }
    const data = await APIService._getListAgencyS1Droplist(
      filterListS1.page,
      filterListS1.keySearch
    );
    const dataNew = data.useragency.map((item) => {
      const id = item.id;
      const value = item.dms_code.toLowerCase();
      const text = item.dms_code;
      return { id, value, text };
    });
    setDataListS1(dataNew);
  };

  const _postAPIConfirmPayment = async () => {
    try {
      if (dataConfirm.length == 0) {
        message.warning("Không có dữ liệu");
        return;
      }
      setLoadingButton(true);
      let array = [];
      array = dataConfirm.map((item) => {
        const obj = {
          id: item.id,
          money: item.xacNhanThanhToan,
        };
        return obj;
      });
      const data = await APIService._postConfirmPayment(array);
      setSelectedRowKeys([]);
      setDataConfirm([]);
      await _fetchAPIListPaymentTransaction(filterTable);
      setLoadingButton(false);
      message.success("Xác nhận thanh toán thành công");
    } catch (error) {
      setLoadingButton(false);
    }
  };

  const _postAPICreatePayment = async () => {
    try {
      let money = Number(moneyAgencyConfirm.split(",").join(""));
      if (money <= 0 || !money || !agencID) {
        message.warning("Vui lòng nhập đầy đủ thông tin !");
        return;
      }
      setLoadingButton(true);
      const data = await APIService._postCreatePayment(agencID, money);

      message.success("Xác nhận thành công");
      setDataDetail({
        user_agency: {},
        image_url: "",
        loading: false,
      });
      setAgencyID("");
      setMoneyAgencyConfirm("");
      setShowPopupCreate(false);
      _getAPIDataconfirm(filterListS1);
      _fetchAPIListPaymentTransaction(filterTable);
      setLoadingButton(false);
    } catch (error) {
      setLoadingButton(false);
    }
  };

  const _getItemById = () => {
    let array = [];
    dataDefaut.forEach((item, index) => {
      selectedRowKeys.forEach((id, index) => {
        if (item.id == id) {
          array.push(item);
        }
      });
    });
    setDataConfirm(array);
  };

  const _getAPIDataconfirm = async () => {
    try {
      const data = await APIService._getListPaymentTransaction(
        filterTable.level_id,
        1, // status cho xac nhan
        0,
        filterTable.search
      );
      data.data.map((item, index) => (item.stt = index + 1));
      setDataConfirm(data.data);
      setDataDefault(data.data);
    } catch (error) {}
  };

  const _getAPIDetailAgency = async (idAgency) => {
    try {
      const data = await APIService._getDetailAgencyS1(idAgency);
      setDataDetail(data);
    } catch (error) {
      console.log(error);
    }
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      align: "center",
      width: 80,
    },
    {
      title: "Tên đại lý",
      dataIndex: "tenDaiLy",
      key: "tenDaiLy",
      render: (tenDaiLy) => <span>{tenDaiLy}</span>,
    },
    {
      title: "Mã đại lý",
      dataIndex: "maDaiLy",
      key: "maDaiLy",
      render: (maDaiLy) => <span>{maDaiLy}</span>,
    },
    {
      title: "Cấp bậc",
      dataIndex: "capBac",
      key: "capBac",
      render: (capBac) => (
        <span style={{ textTransform: "capitalize" }}>{capBac}</span>
      ),
    },
    {
      title: "Hạn mức tín dụng ",
      dataIndex: "hanMucTinDung",
      key: "hanMucTinDung",
      render: (hanMucTinDung) => <span>{priceFormat(hanMucTinDung, "")}</span>,
    },
    {
      title: "Hạn mức khả dụng",
      dataIndex: "hanMucKhaDung",
      key: "hanMucKhaDung",
      render: (hanMucKhaDung) => <span>{priceFormat(hanMucKhaDung, "")}</span>,
    },

    {
      title: "Công nợ cuối kỳ",
      dataIndex: "congNoCuoiKy",
      key: "congNoCuoiKy",
      render: (congNoCuoiKy) => <span>{priceFormat(congNoCuoiKy, "")}</span>,
    },
    {
      title: "Đại lý thanh toán",
      dataIndex: "daiLyThanhToan",
      key: "daiLyThanhToan",
      render: (daiLyThanhToan) => (
        <span>{priceFormat(daiLyThanhToan, "")}</span>
      ),
    },

    {
      title: "Xác nhận thanh toán",
      dataIndex: "xacNhanThanhToan",
      key: "xacNhanThanhToan",
      render: (xacNhanThanhToan) => (
        <span>{priceFormat(xacNhanThanhToan, "")}</span>
      ),
    },
    {
      title: "Thời gian xác nhận",
      key: "statthoiGianXacNhanus_name",
      render: (item) => {
        if (item.trangThai == "Đã xác nhận") {
          return (
            <span>
              {FormatterDay.dateFormatWithString(
                item.thoiGianXacNhan,
                "#DD#/#MM#/#YYYY# - #hhh#:#mm#"
              )}
            </span>
          );
        }
      },
    },
    {
      title: "Trạng thái",
      dataIndex: "trangThai",
      key: "trangThai",
      render: (trangThai) => <span>{trangThai}</span>,
    },
  ];

  // const rowSelection = {
  //     selectedRowKeys,
  //     onChange: (selectedRowKeys) => {
  //         console.log(selectedRowKeys);
  //         setSelectedRowKeys(selectedRowKeys);
  //     },
  //     getCheckboxProps: record => ({
  //         disabled: record.disabled,
  //     }),
  // }

  function removeVietnameseTones(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    // Remove extra spaces
    // Bỏ các khoảng trắng liền nhau
    str = str.replace(/ + /g, " ");
    str = str.trim();
    // Remove punctuations
    // Bỏ dấu câu, kí tự đặc biệt
    str = str.replace(
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
      " "
    );
    return str;
  }

  const renderOptionAutoComplete = (option, search) => {
    const _renderOption = option.map((item) => {
      let keySearch = removeVietnameseTones(search);
      let itemText = removeVietnameseTones(item.text);
      let index = itemText.toLowerCase().indexOf(keySearch.toLowerCase());

      if (index > -1) {
        let text1 = item.text.slice(0, index);
        let text2 = item.text.slice(index, index + keySearch.length);
        let text3 = item.text.slice(index + keySearch.length);

        return (
          <Option key={item.id} text={item.text}>
            <span>{text1}</span>
            <span style={{ fontWeight: 600, color: colors.main }}>{text2}</span>
            <span>{text3}</span>
          </Option>
        );
      } else {
        return (
          <Option key={item.id} text={item.text}>
            <span>{item.text}</span>
          </Option>
        );
      }
    });

    return _renderOption;
  };

  const _renderPopupCreate = () => {
    const { user_agency } = dataDetail;
    return (
      <Modal
        visible={showPopupCreate}
        width={645}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 30 }}
        centered
      >
        <div
          style={{ width: "100%", display: "flex", justifyContent: "center" }}
        >
          <ITitle
            title={"TẠO MỚI GIAO DỊCH THANH TOÁN ĐLC1"}
            level={3}
            style={{ color: colors.main, fontWeight: "bold" }}
          />
        </div>
        <div
          style={{
            width: "100%",
            paddingLeft: 15,
            paddingRight: 15,
            marginTop: 15,
          }}
        >
          <Col>
            <span
              style={{
                color: colors.text.black,
                fontSize: 15,
                fontWeight: 500,
              }}
            >
              Mã đại lý
            </span>
          </Col>
          <Col>
            <AutoComplete
              dataSource={renderOptionAutoComplete(
                dataListS1,
                filterListS1.keySearch
              )}
              style={{
                marginTop: 6,
              }}
              optionLabelProp="text"
              placeholder=""
              onSelect={(idAgency) => {
                setAgencyID(idAgency);
                setMoneyAgencyConfirm("");
                _getAPIDetailAgency(idAgency);
                setFilterListS1({
                  ...filterListS1,
                  keySearch: "",
                  page: 1,
                });
              }}
              onSearch={(data) => {
                if (typeTimeRef.current) {
                  clearTimeout(typeTimeRef.current);
                }
                typeTimeRef.current = setTimeout(() => {
                  setFilterListS1({
                    ...filterListS1,
                    keySearch: data,
                    page: 1,
                  });
                }, 1500);
              }}
            />
          </Col>

          <Row style={{ paddingTop: 15 }}>
            <Col span={12}>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span
                  style={{
                    color: colors.text.black,
                    fontSize: 15,
                    fontWeight: 500,
                  }}
                >
                  Tên đại lý
                </span>
                <span
                  style={{
                    color: colors.text.black,
                    fontSize: 15,
                  }}
                >
                  {" "}
                  {!user_agency.shop_name ? "-" : user_agency.shop_name}
                </span>
              </div>
            </Col>
            <Col span={12}>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span
                  style={{
                    color: colors.text.black,
                    fontSize: 15,
                    fontWeight: 500,
                  }}
                >
                  Số điện thoại
                </span>
                <span
                  style={{
                    color: colors.text.black,
                    fontSize: 15,
                  }}
                >
                  {!user_agency.phone ? "-" : user_agency.phone}
                </span>
              </div>
            </Col>
          </Row>
          <Col style={{ paddingTop: 15 }}>
            <span
              style={{
                color: colors.text.black,
                fontSize: 15,
                fontWeight: 500,
              }}
            >
              Địa chỉ
            </span>
          </Col>
          <Col>
            <span
              style={{
                color: colors.text.black,
                fontSize: 15,
              }}
            >
              {" "}
              {!user_agency.address
                ? "-"
                : user_agency.address +
                  ", " +
                  user_agency.ward_name +
                  ", " +
                  user_agency.district_name +
                  ", " +
                  user_agency.cities_name}
            </span>
          </Col>
          <Col style={{ paddingTop: 15 }}>
            <span
              style={{
                color: colors.text.black,
                fontSize: 15,
                fontWeight: 500,
              }}
            >
              Đại lý thanh toán
            </span>
          </Col>
          <Col>
            <IInput
              style={{
                color: "#000",
                borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
                height: 30,
              }}
              onChange={(e) => {
                let price = e.target.value;
                setMoneyAgencyConfirm(price);
              }}
              value={moneyAgencyConfirm
                .toString()
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
            />
          </Col>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "absolute",
            right: 0,
            bottom: -60,
          }}
        >
          <IButton
            title={"Hủy bỏ"}
            icon={ISvg.NAME.CROSS}
            color={colors.oranges}
            styleHeight={{
              width: 160,
            }}
            onClick={() => {
              setShowPopupCreate(false);
              //   setDataDetail({
              //     user_agency: {},
              //     image_url: "",
              //     loading: false,
              //   });
            }}
          />{" "}
          <IButton
            title={"Xác nhận"}
            color={colors.main}
            styleHeight={{
              width: 160,
              marginLeft: 25,
            }}
            onClick={async () => {
              if (Object.keys(dataDetail.user_agency).length === 0) {
                message.error("Vui lòng chọn đại lý!");
                return;
              }

              if (moneyAgencyConfirm === 0 || moneyAgencyConfirm === "") {
                message.error("Vui lòng nhập số tiền đại lý thanh toán!");
                return;
              }
              setShowPopupCreate(false);
              setShowPopupConfirm2(true);
            }}
          />
        </div>
      </Modal>
    );
  };

  const _renderPopupConfirmPayment = () => {
    return (
      <Modal
        visible={showPopupConfirm}
        width={1045}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 30 }}
        centered
      >
        <Row>
          <Col span={23}>
            <ITitle
              title={"XÁC NHẬN GIAO DỊCH THANH TOÁN ĐLC1"}
              level={3}
              style={{ color: colors.main, fontWeight: "bold" }}
            />
          </Col>
          <Col span={1}>
            <div className="flex justify-end">
              <div
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  background: "#FCEFED",
                  alignItems: "center",
                  justifyContent: "center",
                  display: "flex",
                }}
                className="cursor scale"
                onClick={() => {
                  setShowPopupConfirm(false);
                }}
              >
                <ISvg
                  name={ISvg.NAME.CROSS}
                  width={7}
                  height={7}
                  fill={colors.oranges}
                />
              </div>
            </div>
          </Col>
        </Row>
        <div
          style={{
            width: "100%",
            border: "1px solid rgba(122, 123, 123, 0.5)",
            paddingTop: 10,
            paddingLeft: 15,
            paddingRight: 15,
            marginTop: 15,
          }}
        >
          <Row style={{ paddingBottom: 10 }}>
            <Col span={2}>
              <span
                style={{
                  color: colors.text.black,
                  fontSize: 16,
                  fontWeight: 500,
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                STT
              </span>
            </Col>
            <Col span={5}>
              <span
                style={{
                  color: colors.text.black,
                  fontSize: 16,
                  fontWeight: 500,
                }}
              >
                Mã đại lý
              </span>
            </Col>
            <Col span={7}>
              <span
                style={{
                  color: colors.text.black,
                  fontSize: 16,
                  fontWeight: 500,
                }}
              >
                Tên đại lý
              </span>
            </Col>
            <Col span={5}>
              <span
                style={{
                  color: colors.text.black,
                  fontSize: 16,
                  fontWeight: 500,
                }}
              >
                Đại lý thanh toán
              </span>
            </Col>
            <Col span={5}>
              <div style={{ textAlign: "center" }}>
                <span
                  style={{
                    color: colors.text.black,
                    fontSize: 16,
                    fontWeight: 500,
                  }}
                >
                  Xác nhận thanh toán
                </span>
              </div>
            </Col>
            {/* <Col span={1}>
              <div
                style={{
                  width: 20,
                  height: 20,
                }}
              ></div>
            </Col> */}
          </Row>
          <div
            style={
              {
                //   maxHeight: 300,
                //   overflowY: "scroll",
                //   overflowX: "hidden",
              }
            }
          >
            {dataConfirm.map((item, index) => {
              return _renderItems(item, index);
            })}
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "absolute",
            right: 0,
            bottom: -60,
          }}
        >
          <IButton
            title={"Từ chối"}
            icon={ISvg.NAME.CROSS}
            color={colors.oranges}
            styleHeight={{
              width: 160,
            }}
            onClick={async () => {
              setShowPopupConfirmReject(true);
              setShowPopupConfirm(false);
            }}
          />{" "}
          <IButton
            title={"Xác nhận"}
            color={colors.main}
            styleHeight={{
              width: 160,
              marginLeft: 25,
            }}
            onClick={async () => {
              if (dataConfirm[0].xacNhanThanhToan <= 0) {
                message.error("Vui lòng nhập số tiền xác nhận thanh toán");
                return;
              }
              setShowPopupConfirm(false);
              setShowPopupConfirm2(true);
            }}
          />
        </div>
      </Modal>
    );
  };

  const _renderItems = (item, index) => {
    return (
      <Row
        gutter={[5, 0]}
        style={{
          borderTop: "1px solid rgba(122, 123, 123, 0.5)",
          paddingTop: 20,
          paddingBottom: 10,
        }}
      >
        <Col span={2} style={{ justifyContent: "center", display: "flex" }}>
          <span
            style={{
              color: colors.text.black,
              fontSize: 15,
            }}
          >
            {item.stt}
          </span>
        </Col>
        <Col span={5}>
          <span
            style={{
              color: colors.text.black,
              fontSize: 15,
            }}
          >
            {item.maDaiLy}
          </span>
        </Col>
        <Col span={7}>
          <span
            style={{
              color: colors.text.black,
              fontSize: 15,
            }}
          >
            {item.tenDaiLy}
          </span>
        </Col>
        <Col span={5}>
          <span
            style={{
              color: colors.text.black,
              fontSize: 15,
            }}
          >
            {priceFormat(item.daiLyThanhToan)}
          </span>
        </Col>
        <Col span={5}>
          <IInput
            style={{
              color: "#000",
              borderBottom: "1px solid rgba(122, 123, 123, 0.5)",
              textAlign: "center",
              height: 30,
            }}
            onChange={(e) => {
              let money = Number(e.target.value.split(",").join(""));
              item.xacNhanThanhToan = money;

              setRender(!render);
            }}
            value={item.xacNhanThanhToan
              .toString()
              .replace(/\D/g, "")
              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
          />
        </Col>
        {/* <Col span={1}>
          <div
            style={{
              width: 20,
              height: 20,
              borderRadius: 10,
              background: "#FCEFED",
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
            }}
            className="cursor scale"
            onClick={() => {
              if (dataConfirm.length <= 1) {
                return;
              }
              let dataNew = [...dataConfirm];
              dataNew.splice(index, 1);
              setDataConfirm(dataNew);
            }}
          >
            <ISvg
              name={ISvg.NAME.CROSS}
              width={7}
              height={7}
              fill={colors.oranges}
            />
          </div>
        </Col>
       */}
      </Row>
    );
  };

  return (
    <div style={{ width: "100%", padding: "18px 0px 0px 0px" }}>
      <Row>
        <Col span={12}>
          <StyledITitle style={{ color: colors.main, fontSize: 22 }}>
            Giao dịch thanh toán ĐLC1
          </StyledITitle>
        </Col>
        <Col span={12}>
          <div className="flex justify-end">
            <Tooltip title="Tìm kiếm ...">
              <ISearch
                className="cursor"
                placeholder="Tìm kiếm ..."
                onPressEnter={(e) => {
                  setLoadingTable(true);
                  setFilterTable({
                    ...filterTable,
                    search: e.target.value,
                    page: 1,
                  });
                }}
                icon={
                  <div
                    style={{
                      display: "flex",
                      width: 42,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      src={images.icSearch}
                      style={{ width: 16, height: 16 }}
                      alt=""
                    />
                  </div>
                }
              />
            </Tooltip>
          </div>
        </Col>
      </Row>
      <div style={{ margin: "26px 0px" }}>
        <Row gutter={[0, 50]}>
          <Col span={24}>
            <Row>
              <Col span={12}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <div style={{ width: 200, marginRight: 20 }}>
                    <ISelect
                      defaultValue="Cấp bậc"
                      data={dataLevel}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          level_id: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>

                  <div style={{ width: 200 }}>
                    <ISelect
                      defaultValue="Trạng thái"
                      data={listStatus}
                      select={true}
                      onChange={(value) => {
                        setLoadingTable(true);
                        setFilterTable({
                          ...filterTable,
                          status: value,
                          page: 1,
                        });
                      }}
                    />
                  </div>
                </div>
              </Col>
              <Col span={12}>
                <div className="flex justify-end">
                  {/* <IButton
                    icon={ISvg.NAME.CROSS}
                    title="Từ chối"
                    color={colors.oranges}
                    onClick={async () => {
                      if (selectedRowKeys.length == 0) {
                        message.warning(
                          "Vui lòng chọn giao dịch để từ chối...!"
                        );
                        return;
                      }
                      await _postAPIRejectPayment();
                      _fetchAPIListPaymentTransaction(filterTable);
                    }}
                  /> */}
                  <IButton
                    icon={ISvg.NAME.ARROWUP}
                    title={"Tạo mới"}
                    color={colors.main}
                    styleHeight={{
                      width: 120,
                      marginLeft: 25,
                    }}
                    onClick={() => {
                      setIsCreate(1);
                      setShowPopupCreate(true);
                    }}
                  />
                  {/* <IButton
                    title={"Xác nhận thanh toán"}
                    color={colors.main}
                    styleHeight={{
                      width: 197,
                      marginLeft: 25,
                    }}
                    onClick={async () => {
                      if (selectedRowKeys.length != 0) {
                        _getItemById();
                      } else {
                        setDataConfirm(dataDefaut);
                      }
                      setShowPopupConfirm(true);
                    }}
                  /> */}
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <ITable
                columns={columns}
                data={dataTable.data}
                style={{ width: "100%" }}
                defaultCurrent={1}
                sizeItem={dataTable.size}
                rowKey="id"
                indexPage={filterTable.page}
                maxpage={dataTable.total_record}
                loading={loadingTable}
                // rowSelection={rowSelection}

                onRow={(record, rowIndex) => {
                  return {
                    onClick: (event) => {
                      const id = Number(
                        event.currentTarget.attributes[1].ownerElement.dataset
                          .rowKey
                      );

                      let array = [];
                      dataDefaut.forEach((item, index) => {
                        if (item.id === id) {
                          array.push(item);
                        }
                      });

                      if (array.length === 0) {
                        message.error("Vui lòng chọn giao dịch chờ xác nhận!");
                        return;
                      }
                      setDataConfirm(array);
                      setIsCreate(2);
                      setShowPopupConfirm(true);
                    }, // click row
                  };
                }}
                onChangePage={(page) => {
                  setLoadingTable(true);
                  setFilterTable({ ...filterTable, page: page });
                }}
              ></ITable>
            </Row>
          </Col>
        </Row>
      </div>
      {_renderPopupConfirmPayment()}
      {_renderPopupCreate()}
      <Modal
        visible={showPopupConfirm2}
        minWidth={400}
        maxWidth={600}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 30 }}
        centered
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Xác nhận thanh toán
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: "100%",
                  marginTop: 5,
                  textAlign: "center",
                  fontWeight: 600,
                  color: "#000",
                }}
              >
                <div>
                  Bạn vừa xác nhận đại lý{" "}
                  {isCreate === 2
                    ? dataConfirm.length === 0
                      ? "-"
                      : dataConfirm[0].tenDaiLy
                    : !dataDetail.user_agency.shop_name
                    ? "-"
                    : dataDetail.user_agency.shop_name}{" "}
                  thanh toán{" "}
                  {isCreate === 2
                    ? dataConfirm.length === 0
                      ? 0
                      : priceFormat(dataConfirm[0].xacNhanThanhToan)
                    : priceFormat(
                        moneyAgencyConfirm === ""
                          ? 0
                          : Number(moneyAgencyConfirm.split(",").join(""))
                      )}
                  .
                </div>
                <div>Bấm “Xác nhận” để hoàn tất giao dịch.</div>
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -60,
              right: -1,
              display: "flex",
              justifyContent: "center",
              width: "100%",
            }}
          >
            <div style={{ width: 165, marginRight: 25 }}>
              <IButton
                color={colors.oranges}
                title="Hủy bỏ"
                styleHeight={{ minWidth: 0 }}
                onClick={() => {
                  if (isCreate === 1) {
                    setShowPopupConfirm2(false);
                    setShowPopupCreate(true);
                  } else {
                    setShowPopupConfirm2(false);
                    setShowPopupConfirm(true);
                  }
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{ width: 165 }}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                styleHeight={{ minWidth: 0 }}
                loading={loadingButton}
                icon={ISvg.NAME.CHECKMARK}
                onClick={async () => {
                  if (isCreate === 1) {
                    await _postAPICreatePayment();
                  } else {
                    await _postAPIConfirmPayment();
                  }

                  setShowPopupConfirm2(false);
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        visible={showPopupConfirmReject}
        width={350}
        footer={null}
        closable={false}
        maskClosable={false}
        bodyStyle={{ borderRadius: 0, padding: 30 }}
        centered
      >
        <div
          style={{
            height: "100%",
          }}
        >
          <Row gutter={[0, 10]}>
            <Col span={24}>
              <div style={{ textAlign: "center" }}>
                <StyledITitle style={{ color: colors.main, fontSize: 17 }}>
                  Xác nhận từ chối
                </StyledITitle>
              </div>
            </Col>
            <Col span={24}>
              <div
                style={{
                  width: "100%",
                  marginTop: 5,
                  textAlign: "center",
                  fontWeight: 600,
                  color: "#000",
                }}
              >
                <div>Bạn có thực sự muốn từ chối giao dịch.</div>
                <div>Bấm “Xác nhận” để hoàn tất từ chối.</div>
              </div>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              bottom: -60,
              right: -1,
              display: "flex",
              justifyContent: "center",
              width: "100%",
            }}
          >
            <div style={{ width: 165, marginRight: 25 }}>
              <IButton
                color={colors.oranges}
                title="Hủy bỏ"
                styleHeight={{ minWidth: 0 }}
                onClick={() => {
                  setShowPopupConfirmReject(false);
                  setShowPopupConfirm(true);
                }}
                icon={ISvg.NAME.CROSS}
              />
            </div>

            <div style={{ width: 165 }}>
              <IButton
                color={colors.main}
                title="Xác nhận"
                styleHeight={{ minWidth: 0 }}
                icon={ISvg.NAME.CHECKMARK}
                onClick={async () => {
                  await _postAPIRejectPayment();
                  await _fetchAPIListPaymentTransaction(filterTable);

                  setShowPopupConfirmReject(false);
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
