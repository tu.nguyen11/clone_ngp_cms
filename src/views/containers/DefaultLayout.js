import React, { Component } from "react";

import { Link, Route, Suspense, Switch, Redirect } from "react-router-dom";
import { ver } from "./../../assets/version";
import {
  Layout,
  Menu,
  Breadcrumb,
  Icon,
  Avatar,
  Badge,
  Dropdown,
  Button,
  Affix,
  Tooltip,
} from "antd";

import { Row, Col, Container } from "reactstrap";

import { object } from "prop-types";

import { ISvg, ITitle } from "../components";
import { images, colors } from "../../assets";
import { HistoryService, UserService } from "../../services";
import _menu from "../../views/containers/_menu";

import "./DefaultLayout.css";
import menus from "./_menu";
import _routes from "./_routes";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: true,
      hover: false,
      current: -1,
      activeChildren: -1, // để -1 default để không đụng vị trí index của children trong menu
      menu: _menu,
    };
    this.onSelectMenu = this.onSelectMenu.bind(this);
    this.handleClickMenu = this.handleClickMenu.bind(this);
  }

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  handleClickMenu = (e) => {
    let path;
    let objSubMenu = JSON.parse(e.key); // objSuMenu parse string thành object để lấy vị trí index _menu
    let index = objSubMenu.index; // vị trí đạng chọn trong _menu[subMenu]
    let indexDataMenu = this.state.menu[objSubMenu.subMenu]; // indexDataMenu lấy data tại vị trí subMenu của mảng _menu

    if (indexDataMenu.children.length == 0) {
      path = indexDataMenu.path; // lấy path trỏ đến router
      this.setState({
        current: objSubMenu.subMenu,
      });
    } else {
      path = indexDataMenu.children[index].path;
      this.setState({
        current: objSubMenu.subMenu,
        activeChildren: objSubMenu.index,
      });
    }

    this.props.history.push(path);
  };

  menu = (
    <Menu>
      <Menu.Item key="0" onClick={() => UserService._setToken("")}>
        <Link to="/login">Đăng xuất</Link>
      </Menu.Item>
    </Menu>
  );

  onSelectMenu = (obj) => {
    let key = Number(obj.key);
    this.state.arrayMenu.map((item, index) => {
      if (item.index == key) {
        let active = !item.active;
        item.active = active;
        this.setState({
          arrayMenu: this.state.arrayMenu,
        });
        return;
      } else {
        item.active = false;
        this.setState({
          arrayMenu: this.state.arrayMenu,
        });
        return;
      }
    });
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  toggleHover = (index) => {
    let hover = !this.state.arrayMenu[index].hover;
    this.state.arrayMenu[index].hover = hover;
    this.setState({ arrayMenu: this.state.arrayMenu });
  };

  render() {
    const version = ver();
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Header className="box-shadow no_print">
          {/* <div>
            <img
              src={images.logo}
              style={{
                width: 50,
                height: 45,
                objectFit: "contain",
                marginBottom: 6
              }}
            />
          </div>
          <div style={{ flex: 1 }}></div>

          <div className="d-flex align-items-center">
            <ISvg name={ISvg.NAME.USER} width={32} height={32} fill={"white"} />
            <h6 style={{ marginLeft: 24, marginBottom: 0, marginRight: 45 }}>
              ADMIN
            </h6>
          </div> */}
          <Col style={{}} className="m-0 p-0 ">
            <Row>
              <Col lg={10} className="m-0 p-2 ">
                {/* <Tooltip title="Nhấn vào để vào trang HomeDashboard."> */}
                <img
                  src={images.logo}
                  style={{
                    width: 70,
                    objectFit: "contain",
                  }}
                  onClick={() => this.props.history.push("/order/request/list")}
                  className="m-0 p-0 cursor"
                />
                {/* </Tooltip> */}
              </Col>
              <Col
                lg={2}
                style={{ display: "flex", justifyContent: "flex-end" }}
                className="m-0 p-0"
              >
                <Dropdown trigger={["click"]} overlay={this.menu}>
                  <div className="d-flex align-items-center cursor">
                    <ISvg
                      name={ISvg.NAME.USER}
                      width={32}
                      height={32}
                      fill={"white"}
                    />

                    <h6
                      style={{ marginLeft: 10, marginRight: 25 }}
                      onClick={() => {
                        // UserService._setToken("");
                        // this.props.history.replace("/login");
                      }}
                    >
                      {localStorage.getItem("user_name_login")}
                    </h6>
                  </div>
                </Dropdown>
              </Col>
            </Row>
          </Col>
        </Header>

        <Layout style={{ marginTop: 64, background: colors.background }}>
          <Sider
            collapsible
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
            width={301}
            className="box-shadow no_print"
            trigger={null}
          >
            {/* <Affix offsetTop={64}> */}
            <div
              style={{
                width: "100%",
                height: 80,
              }}
              className="cursor"
            >
              <div
                style={{
                  width: 80,
                  height: 80,
                  //position: "absolute",
                  float: this.state.collapsed ? "left" : "right",
                  // alignSelf: "flex-end",
                  // right: 0,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  background: this.state.collapsed ? colors.main : colors.white,
                }}
                onClick={this.toggle}
              >
                <ISvg
                  name={ISvg.NAME.MENU}
                  fill={!this.state.collapsed ? colors.main : colors.white}
                  width={24}
                  height={24}
                />
              </div>
            </div>

            <Menu
              theme="light"
              // defaultSelectedKeys={["1"]}
              selectedKeys={[this.state.current]}
              mode="inline"
              style={{ marginTop: 15, borderRight: "0px !important" }}
              onSelect={this.handleClickMenu}
            >
              {this.state.menu.map((item, index) => {
                const active = index === this.state.current ? true : false;
                return item.children.length === 0 ? (
                  <Menu.Item
                    key={JSON.stringify({
                      index: item.index,
                      subMenu: item.subMenu,
                    })}
                    style={{ marginTop: 25 }}
                  >
                    <Row>
                      <Col xs="auto">
                        <ISvg
                          name={item.name}
                          width={20}
                          height={20}
                          fill={active ? colors.main : colors.icon.default}
                        />
                      </Col>
                      <Col>
                        <ITitle
                          level={3}
                          style={{
                            fontWeight: 500,
                            color: active ? colors.main : colors.icon.default,
                          }}
                          title={item.value}
                        />
                      </Col>
                    </Row>
                  </Menu.Item>
                ) : (
                  <SubMenu
                    style={{ marginTop: 25 }}
                    key={item.subMenu}
                    title={
                      <Row>
                        <Col xs="auto">
                          <ISvg
                            name={item.name}
                            width={20}
                            height={20}
                            fill={active ? colors.main : colors.icon.default}
                          />
                        </Col>
                        <Col>
                          <ITitle
                            level={3}
                            style={{
                              fontWeight: 500,
                              color: active ? colors.main : colors.icon.default,
                            }}
                            title={item.value}
                          />
                        </Col>
                      </Row>
                    }
                  >
                    {item.children.map((itemChildren, indexChildren) => {
                      const active1 =
                        indexChildren === this.state.activeChildren &&
                        itemChildren.subMenu == this.state.current
                          ? true
                          : false;

                      return (
                        <Menu.Item
                          key={JSON.stringify({
                            index: itemChildren.index,
                            subMenu: itemChildren.subMenu,
                          })}
                        >
                          <Row>
                            <Col className="ml-4">
                              <ITitle
                                level={3}
                                style={{
                                  color: active1
                                    ? colors.main
                                    : colors.icon.default,
                                }}
                                title={itemChildren.value}
                              />
                            </Col>
                          </Row>
                        </Menu.Item>
                      );
                    })}
                  </SubMenu>
                );
              })}
            </Menu>
            {/*{!this.state.collapsed && (*/}
            {/*    <ITitle*/}
            {/*        level={4}*/}
            {/*        style={{*/}
            {/*          color: colors.icon.default,*/}
            {/*          position:'absolute',left:10,bottom:10*/}
            {/*        }}*/}
            {/*        title={"Version: " +version}*/}
            {/*    />*/}
            {/*)}*/}
          </Sider>

          <Content style={{}}>
            <Switch>
              {_routes.map((route, idx) => {
                return route.component ? (
                  <Route
                    key={idx}
                    path={route.path}
                    breadcrumbName={route.name}
                    exact={true}
                    name={route.name}
                    render={(props) => (
                      <Container
                        style={{
                          background: colors.background,
                          padding: route.padding ? "0px" : "24px 36px",
                          height: "100%",
                        }}
                        fluid
                      >
                        {/* {route.back ? (
                          <a onClick={() => this.props.history.goBack()}>
                            <ISvg
                              name={ISvg.NAME.ARROWTHINLEFT}
                              width={9}
                              height={7}
                              fill={colors.icon.default}
                            />
                            <ITitle
                              title="Back"
                              level="5"
                              style={{ marginLeft: 8 }}
                            />
                          </a>
                        ) : null} */}

                        <route.component {...props} />
                      </Container>
                    )}
                  />
                ) : null;
              })}
              <Redirect from="/" to="/login" />
            </Switch>
            {/* <Footer style={{ textAlign: "center" }}>
              CMS Design ©2019 Created by IPI Corp
            </Footer> */}
          </Content>
        </Layout>
      </Layout>
    );
  }

  _renderItemMenu = (item, index) => {
    const { name = "", icon = "", path = "", children } = item;
    if (children && children.length > 0) {
      return (
        <SubMenu
          key={path}
          title={
            <span>
              <Icon type={icon} />
              <span>{name}</span>
            </span>
          }
        >
          {children.map(this._renderItemMenu)}
        </SubMenu>
      );
    }
    return (
      <Menu.Item
        key={path}
        onClick={() => {
          this.props.history.push(path);
        }}
      >
        <Icon type={icon} />
        <span>{name}</span>
      </Menu.Item>
    );
  };
  _renderItemBreadcrumb(item, params, routes, paths) {
    const last = routes.indexOf(item) === routes.length - 1;
    return last ? (
      <span>{item.name}</span>
    ) : (
      //   <Breadcrumb.Item>{route.name}</Breadcrumb.Item>
      <Link to={item.path}>{item.name}</Link>
    );
  }
}

export default DefaultLayout;
