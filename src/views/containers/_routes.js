import HomePage from "../pages/HomePage";
import LoginPage from "../pages/LoginPage";
import CreateBannerPage from "../pages/Banner/CreateBannerPage";
import DetailBannerPage from "../pages/Banner/DetailBannerPage";
import ListBannerPage from "../pages/Banner/ListBannerPage";
import ListBrandPage from "../pages/Brand/ListBrandPage";
import ProductCategoryListPage from "../pages/Category/ListProductCategory";
import ViewProductCategoryPage from "../pages/Category/ViewProductCategory";
import BusinessDashboard from "../pages/Dashboard/BusinessDashboard";
import DetailSalesDashboard from "../pages/Dashboard/DetailSalesDashboard";
import HomeDashboard from "../pages/Dashboard/HomeDashboard";
import Test from "../pages/Dashboard/Test";
import DetailOrderDebitPage from "../pages/Debit/DetailOrderDebitPage";
import HistoryUpdateDebitPage from "../pages/Debit/HistoryUpdateDebitPage";
import PaymentTransactionPage from "../pages/Debit/PaymentTransactionPage";
import CreateEvent from "../pages/Event/CreateEvent";
import CreateEventDescription from "../pages/Event/CreateEventDescription";
import DetailEventPage from "../pages/Event/DetailEventPage";
import EditEventPage from "../pages/Event/EditEventPage";
import ListEventPage from "../pages/Event/ListEventPage";
import StatisticalEventPage from "../pages/Event/StatisticalEventPage";
import ImportDebitPage from "../pages/Import/ImportDebitPage";
import ListKpi from '../pages/KpiManagement/ListKpiManagement'

// an
import ReturnDebitPage from "../pages/Debit/ReturnDebitPage";
import CommentPage from "../pages/News/CommentPage";
import CreateNews from "../pages/News/CreateNews";
import CreateNewsCategories from "../pages/News/CreateNewsCategories";
import DetailNews from "../pages/News/DetailNews";
import DetailNewsCategories from "../pages/News/DetailNewsCategories";
import NewCategoriesPage from "../pages/News/NewCategoriesPage";
import NewsPage from "../pages/News/NewsPage";
import NewsPrioritizePage from "../pages/News/NewsPrioritizePage";
import createNotification from "../pages/Notification/CMS/CreateNotification";
import DetailNotification from "../pages/Notification/CMS/DetailNotification";
import NotificationPage from "../pages/Notification/CMS/NotificationPage";
import DetailOrder from "../pages/Order/DetailOrder";
import DetailOrderCoordinatorS1 from "../pages/Order/DetailOrderCoordinatorS1";
import DetailOrderS1 from "../pages/Order/DetailOrderS1";
import DetailOrderS3 from "../pages/Order/DetailOrderS3";
import DetailOrderTemporary from "../pages/Order/DetailOrderTemporary";
import OrderDLC1Page from "../pages/Order/OrderDLC1Page";
import OrderDLC3Page from "../pages/Order/OrderDLC3Page";
import OrderPage from "../pages/Order/OrderPage";
import OrderTemporaryDLC1Page from "../pages/Order/OrderTemporaryDLC1Page";
import PrintOrderPage from "../pages/Order/PrintOrderPage";
import CreateProduct from "../pages/Product/CreateProduct";
import DetailProduct from "../pages/Product/DetailProduct";
import editProduct from "../pages/Product/EditProduct";
import ProductPage from "../pages/Product/ProductPage";
import ReportGeneralPage from "../pages/Report/ReportGeneralPage";
import createRouter from "../pages/Route/CreateRouter";
import DetailRoute from "../pages/Route/DetailRoute";
import ListRoute from "../pages/Route/ListRoute";
import createSalesman from "../pages/Salesman/CreateSalesman";
import DetailSalesman from "../pages/Salesman/DetailSalesman";
import ListSalesmanPage from "../pages/Salesman/ListSalesmanPage";
import CreateShipperPage from "../pages/Shipper/CreateShipperPage";
import DetailShipperPage from "../pages/Shipper/DetailShipperPage";
import editShipperPage from "../pages/Shipper/EditShipperPage";
import HistoryShipperPage from "../pages/Shipper/HistoryShipperPage";
import ListShipperPage from "../pages/Shipper/ListShipperPage";
import CreateCouponInputPage from "../pages/Store/CreateCouponInputPage";
import CreateCouponOnputPage from "../pages/Store/CreateCouponOutputPage";
import CreateCouponPage from "../pages/Store/CreateCouponPage";
import CreateStore from "../pages/Store/CreateStore";
import DetailCouponOutputPage from "../pages/Store/DetailCouponOutputPage";
import DetailCouponPage from "../pages/Store/DetailCouponPage";
import DetailStore from "../pages/Store/DetailStore";
import EditStore from "../pages/Store/EditStore";
import ListInventoryPage from "../pages/Store/ListInventoryPage";
import ListShipperAssign from "../pages/Store/ListShipperAssign";
import ListStoreInputPage from "../pages/Store/ListStoreInputPage";
import ListStoreOuputPage from "../pages/Store/ListStoreOuputPage";
import ListStorePage from "../pages/Store/ListStorePage";
import PrintCouponPage from "../pages/Store/PrintCouponPage";
import CreateWarehousePage from "../pages/Warehouse/CreateWarehousePage";
import DetailWarehousePage from "../pages/Warehouse/DetailWarehousePage";
import ListWarehousePage from "../pages/Warehouse/ListWarehousePage";
import AgencyPage from "../pages/Agency/AgenCyS1/AgencyPage";
import CreateAgency from "../pages/Agency/AgenCyS1/CreateAgency";
import DetailAgency from "../pages/Agency/AgenCyS1/DetailAgency";
import DetailAgencyS1Page from "../pages/Agency/AgenCyS1/DetailAgentcyS1Page";
import EditAgency from "../pages/Agency/AgenCyS1/EditAgency";
import AgencyS2Page from "../pages/Agency/AgencyS2/AgencyS2Page";
import createAgencyS2 from "../pages/Agency/AgencyS2/CreateAgencyS2";
import DetailAgencyS2 from "../pages/Agency/AgencyS2/DetailAgencyS2";
import DetailDebtAgencyS2 from "../pages/Agency/AgencyS2/DetailDebtAgencyS2";
import BranchPage from "../pages/Agency/Branch/BranchPage";
import DetailBranch from "../pages/Agency/Branch/DetailBranch";
import editBranchPage from "../pages/Agency/Branch/EditBranchPage";
import DetailDistribution from "../pages/Agency/Distribution/DetailDistribution";
import DistributionPage from "../pages/Agency/Distribution/DistributionPage";
import createDistributionPage from "../pages/Agency/Distribution/EditDistributionPage";
import editDistributionPage from "../pages/Agency/Distribution/EditDistributionPage";
import DetailEndUsersPage from "../pages/Agency/EndUser/DetailEndUsersPage";
import EditEndUserPage from "../pages/Agency/EndUser/EditEndUserPage";
import ListEndUsersPage from "../pages/Agency/EndUser/ListEndUsersPage";
import CreateEventFilterAgency from "../pages/Event/EventAgency/CreateEventFilter";
import CreateEventDesgin from "../pages/Event/EventFilter/CreateEventDesgin";
import CreateEventFilter from "../pages/Event/EventFilter/CreateEventFilter";
import DetailSalesPolicyEventGiftPage from "../pages/Event/EventGift/DetailSalesPolicyEventGiftPage";
import HistoryStatisticalEventGiftPage from "../pages/Event/EventGift/HistoryStatisticalEventGiftPage";
import ListEventGiftPage from "../pages/Event/EventGift/ListEventGiftPage";
import StatisticalEventGiftPage from "../pages/Event/EventGift/StatisticalEventGiftPage";
import CreateEventAgencyFilterImmediate from "../pages/Event/Immediate/CreateEventAgencyFilterImmediate";
import CreateEvenDesginImmediate from "../pages/Event/Immediate/CreateEventDesginImmediate";
import CreateEventFilterImmediate from "../pages/Event/Immediate/CreateEventFilterImmediate";
import CreateEventImmediate from "../pages/Event/Immediate/CreateEventImmediate";
import CreateEventImmediateDescription from "../pages/Event/Immediate/CreateEventImmediateDescription";
import DetailEventImmediatePage from "../pages/Event/Immediate/DetailEventImmediatePage";
import ListEventImmediatePage from "../pages/Event/Immediate/ListEventImmediatePage";
import StatisticalEventImmediatePage from "../pages/Event/Immediate/StatisticalEventImmediatePage";
import DetailNPPProductPage from "../pages/Product/NPP/DetailNPPProductPage";
import ListNPPProductPage from "../pages/Product/NPP/ListNPPProductPage";
import CreateProductPage from "../pages/Product/NewProduct/CreateProductPage";
import DetailProductPage from "../pages/Product/NewProduct/DetailProductPage";
import DetailProductVersion from "../pages/Product/NewProduct/DetailProductVersion";
import EditVersionProduct from "../pages/Product/NewProduct/EditVersionProduct";
import ListProductPage from "../pages/Product/NewProduct/ListProductPage";
import ListBestSaleProductPage from "../pages/Product/BestSale/ListBestSaleProductPage";
import ListHotProductPage from "../pages/Product/Hot/ListHotProductPage";
import CreateTypeProduct from "../pages/Product/type/CreateTypeProduct";
import DetailTypeProduct from "../pages/Product/type/DetailTypeProduct";
import editTypeProduct from "../pages/Product/type/EditTypeProduct";
import ProductTypePage from "../pages/Product/type/ProductTypePage";
import ActionPage from "../pages/Route/Action/ActionPage";
import DetailActionPage from "../pages/Route/Action/DetailActionPage";
import DetailSellRoute from "../pages/Route/Sell/DetailSellRoute";
import ListSellRoute from "../pages/Route/Sell/ListSellRoute";
import DetailSeriveRoute from "../pages/Route/Service/DetailSeriveRoute";
import ListServiceRoute from "../pages/Route/Service/ListServiceRoute";
import ReportInputStock from "../pages/Warehouse/Report/ReportInputStock";
import ReportInventory from "../pages/Warehouse/Report/ReportInventory";
import ReportReleaseStock from "../pages/Warehouse/Report/ReportReleaseStock";
import DetailStockReceipt from "../pages/Warehouse/Stock/DetailStockReceipt";
import ListStockReceipt from "../pages/Warehouse/Stock/ListStockReceipt";
import CreateEventGift from "../pages/Event/EventGift/Create/CreateEventGift";
import CreateEventGiftDescription from "../pages/Event/EventGift/Create/CreateEventGiftDescription";

import ListProductByObject from "../pages/Product/ProductsByObject/ListProductByObject";
import CreateProductByObject from "../pages/Product/ProductsByObject/CreateProductByObject";
import CreateEventGiftDesignCondition from "../pages/Event/EventGift/Create/CreateEventGiftDesignCondition";
import CreateEventGiftFilterAgencyC1 from "../pages/Event/EventGift/Create/CreateEventGiftFilterAgencyC1";
import CreateEventGiftFilterCondition from "../pages/Event/EventGift/Create/CreateEventGiftFilterCondition";
import ListEventOfAgencyS1 from "../pages/Agency/AgenCyS1/ListEventOfAgencyS1";
import DetailNotificationNew from "../pages/Notification/CMS/DetailNotificationNew";
import HistoryInventory from "../pages/Warehouse/Report/HistoryInventory";

import ListDecentralization from "../pages/Decentralization/ADMIN/ListDecentralization.jsx";
import DetailDecentralization from "../pages/Decentralization/ADMIN/DetailDecentralization.jsx";
import CreateDecentralization from "../pages/Decentralization/ADMIN/CreateDecentralization.jsx";
import ListDecentralizationUser from "../pages/Decentralization/USER/ListDecentralizationUser.jsx";
import DetailDecentralizationUser from "../pages/Decentralization/USER/DetailDecentralizationUser.jsx";
import CreateDecentralizationUser from "../pages/Decentralization/USER/CreateDecentralizationUser.jsx";
import Page404 from "../pages/Page404";

import ListHideEventOfAgencyS1 from "../pages/Agency/AgenCyS1/ListHideEventOfAgencyS1";
import ListHideProductByObject from "../pages/HideByObject/ListHideProductByObject";
import ListHideCateByObject from "../pages/HideByObject/ListHideCateByObject";
import ListHideSubCateByObject from "../pages/HideByObject/ListHideSubCateByObject";
import ListHideBrandByObject from "../pages/HideByObject/ListHideBrandByObject";
import ListHideEventByObject from "../pages/HideByObject/ListHideEventByObject";
import CreateHideByObject from "../pages/HideByObject/CreateHideByObject";

import CreateEventAgencyFilterImmediateNew from "../pages/Event/ImmediateNew/CreateEventAgencyFilterImmediateNew";
import CreateEventConditionAttachImmediateNew from "../pages/Event/ImmediateNew/CreateEventConditionAttachImmediateNew";
import CreateEventDesignImmediateNew from "../pages/Event/ImmediateNew/CreateEventDesignImmediateNew";
import CreateEventFilterImmediateNew from "../pages/Event/ImmediateNew/CreateEventFilterImmediateNew";
import CreateEventImmediateDescriptionNew from "../pages/Event/ImmediateNew/CreateEventImmediateDescriptionNew";
import CreateEventImmediateNew from "../pages/Event/ImmediateNew/CreateEventImmediateNew";
import CreateEventImmediateNewChildren from "../pages/Event/ImmediateNew/CreateEventImmediateNewChildren";
import DetailEventImmediateNew from "../pages/Event/ImmediateNew/DetailEventImmediateNew";
import DecentralizationAdminByAgencyC1 from "../pages/Decentralization/AGENCYC1/DecentralizationAdminByAgencyC1";
import DecentralizationPKDByAgencyC1 from "../pages/Decentralization/AGENCYC1/DecentralizationPKDByAgencyC1";
import CreateDecentralizationByAgencyC1 from "../pages/Decentralization/AGENCYC1/CreateDecentralizationByAgencyC1";
import DetailDecentralizationByAgencyC1 from "../pages/Decentralization/AGENCYC1/DetailDecentralizationByAgencyC1";
import ListProductPurchasedAgencyC1 from "../pages/Agency/AgenCyS1/ListProductPurchasedAgencyC1";
import ListHideOrShowProductAgencyC1 from "../pages/Agency/AgenCyS1/ListHideOrShowProductAgencyC1";
import ListSetupNotificationApp from "../pages/Notification/App/ListSetupNotificationApp";
import CreateActionPage from "../pages/Route/Action/CreateActionPage";
import CreateServiceRoute from "../pages/Route/Service/CreateServiceRoute";
import CreateSalesManSell from "../pages/Route/Sell/CreateSalesManSell";
import ListReleaseReceipt from "../pages/Warehouse/Release/ListReleaseReceipt";
import DetailReleaseReceipt from "../pages/Warehouse/Release/DetailReleaseReceipt";
import CreateStockReceipt from "../pages/Warehouse/Stock/CreateStockReceipt";

import ListOrderRequest from "../pages/Order/OrderRequest/ListOrderRequest";
import DetailOrderRequest from "../pages/Order/OrderRequest/DetailOrderRequest";

import ListCustomer from "../pages/Customer/ListCustomer";
import CreateCustomer from "../pages/Customer/CreateCustomer";
import DetailCustomer from "../pages/Customer/DetailCustomer";
import ListHistorySurvey from "../pages/Customer/ListHistorySurvey";

import ListOfContracts from "../pages/Contracts/ListOfContracts";
import DetailOfContract from "../pages/Contracts/DetailOfContract";
import EditInfomationOfContracts from "../pages/Contracts/EditInfomationOfContracts";
import FormContract from "../pages/Contracts/FormContract";
import ListSurvey from "../pages/Survey/ListSurvey";
import DetailSurvey from "../pages/Survey/DetailSurvey";
import CreateSurvey from "../pages/Survey/CreatesSurvey";
import DetailKpi from "../pages/KpiManagement/DetailKpiManagement";
import CreateKpi from "../pages/KpiManagement/CreateKpiManagement";
import ListPaymentType from "../pages/PaymentType/ListPaymentType";
import DetailTypeOfPayment from "../pages/PaymentType/DetailTypeOfPayment";
import EditTypeOfPayment from "../pages/PaymentType/EditTypeOfPayment";
import DetailTradingSchedule from "../pages/Contracts/TradingSchedule/DeatailTrading";
import EditTradingSchedule from "../pages/Contracts/TradingSchedule/EditTradingSchedule";

const routerPath = {
  // khách hàng
  listCustomer: "/customer/list",
  createCustomer: "/customer/:type/:id",
  detailCustomer: "/detail/customer/:id",
  listHistorySurvey: "/detail/customer/:id/survey/list",

  // đơn yêu cầu
  listOrderRequest: "/order/request/list",
  detailOrderRequest: "/order/request/detail/:id",

  decentralizationAdminByAgencyC1: "/decentralization/user/admin/s1/list",
  decentralizationPKDByAgencyC1: "/decentralization/user/agency/s1/list",
  createDecentralizationByAgencyC1:
    "/decentralization/user/:type_user/s1/:type/:id",
  detailDecentralizationByAgencyC1:
    "/decentralization/user/:type_user/s1/detail/:id",
  listSetupNotificationApp: "/notification/app/list",
  listProductPurchasedAgencyC1: "/agency/c1/product/purchased/list/:id",
  listHideOrShowProductAgencyC1: "/agency/c1/product/hideOrShow/list/:id",
  // ẩn hiện theo đối tượng
  listHideProductByObject: "/hide/product/list",
  listHideCateByObject: "/hide/cate/list",
  listHideSubCateByObject: "/hide/sub/cate/list",
  listHideBrandByObject: "/hide/brand/list",
  listHideEventByObject: "/hide/event/list",
  createHideByObject: "/hide/:type/create",
  // Event trả ngay mới
  createEventImmediateNew: "/create/event/immediate/:id",
  createEventFilterImmediateNew: "/create/event/immediate/filter/:id",
  createEventAgencyFilterImmediateNew:
    "/create/event/immediate/filter/agency/:level/:id",
  createEventDesignImmediateNew: "/create/event/immediate/design/:id",
  createEventConditionAttachImmediateNew:
    "/create/event/immediate/condition/attach/:id",
  createEventImmediateDescriptionNew: "/create/event/immediate/description/:id",
  detailEventImmediateNew: "/detail/event/immediate/:id",
  createEventImmediateNewChildren: "/create/event/immediate/children/:id",

  // user phân quyền
  list_decentralization_user: "/decentralization/user/:type/list",
  detail_decentralization_user:
    "/account/decentralization/user/:type/detail/:id",
  create_decentralization_user: "/decentralization/user/:type/:type_page/:id",
  create_condition_event: "/event_create/condition/create/:id",
  create_condition_event_combo: "/event_create/combo/condition/create/:id",

  // phân quyền danh sách
  list_decentralization: "/decentralization/:type/list",

  // chi tiết phân quyền
  detail_decentralization_admin: "/decentralization/:type/detail/:id",
  create_decentralization_admin: "/group/decentralization/:type/:type_page/:id",

  detailNotificationNew: "/notification/detail/:id",

  listEventOfAgencyS1: "/agency/c1/event/list/:id",
  // Products by Object
  listProductByObject: "/product/object/list",
  createProductByObject: "/product/object/create",

  //PRODUCT CATEGORY
  viewProductCategory: "/category/:viewType/:typeId/:id",
  createProductCategory: "/category/:viewType/:flag",
  listProductCategory: "/category/list",

  // BRAND
  listBrandPage: "/brand/list",

  //EVENT GIFT
  createEventGift: "/event/gift/:type/:id",
  createEventGiftFilterCondition: "/event/gift/filter/:type/:id",
  createEventGiftFilterAgencyC1: "/event/gift/agencyS1/:type/:id",
  createEventGiftDesignCondition: "/event/gift/design/condition/:type/:id",
  listEventGiftPage: "/event/gift/list",
  createEventGiftDescription: "/event/gift/description/:type/:id",

  //NEW PRODUCT PAGES
  listProductPage: "/product/list",
  createProductPage: "/product/:type/:id",
  editVersionProduct: "/product/:type/:id/version/:idVersion/:index",
  detailProductPage: "/product/detail/:id",
  detailProductVersion: "/product/detail/:id/version/:idVersion/:index",
  //END USERS
  listEndUsersPage: "/end/user/list",
  detailEndUsersPage: "/end/user/detail/:id",
  editEndUserPage: "/end/user/edit/:id",
  //EVENT ACCUMULATION
  CreateEventFiler: "/eventCreate/filter/:id",
  CreateEventDesgin: "/eventCreate/filter/desgin/:id",
  CreateEventFilterAgency: "/eventCreate/filter_agency/:level/:id",
  // CreateEventDesginAgency: '/eventCreate/filter_agency/desgin/:id',
  CreateEventDescription: "/eventCreate/description/:type/:id",
  StatisticalEventPage: "/statisticalEventPage/:id",
  DetailEeventPage: "/detailEventPage/:type/:id",
  detailDebtAgencyS2: "/agencyS2/debt/detail/:id",

  // EVENT IMMEEDIATE
  listEventImmediate: "/event/immediate/list",
  createEventImmediate: "/event/immediate/create/:id",
  DetailAgencyS1Page: "/agency/detail/:id",
  createEventFilterImmediate: "/event/immediate/create/filter/:id",
  createEvenDesginImmediate: "/event/immediate/create/desgin/:id",
  statisticalEventImmediatePage: "/event/immediate/statistical",
  detailEventImmediatePage: "/event/immediate/detail/:type/:id",
  createEventAgencyFilterImmediate:
    "/event/immediate/create/filter/agency/:level/:id",
  createEventImmediateDescription: "/event/immediate/description/:type/:id",
  statisticalEventGiftPage: "/event/eventGift/statisticalEventGiftPage/:id",
  historyStatisticalEventGiftPage:
    "/event/eventGift/historyStatisticalEventGiftPage/:id",
  detailSalesPolicyEventGiftPage:
    "/event/eventGift/detailSalesPolicyEventGiftPage/:id",

  // ORDER
  listOrderS1: "/orderPageS1",
  detailOrderS1: "/orderPageS1/detail/:id",
  detailOrderCoordinatorS1:
    "/orderPageS1/coordinator/detail/:id/:order_code/:order_codeS2/:length_S2",
  listOrderS3: "/orderPageS3",
  detailOrderS3: "/orderPageS3/detail/:order_code",

  //Route
  salesmanSell: "/route/sell/list",
  detailSalesManSell: "/detail/route/sell/:id",
  action: "/route/action",
  detailAction: "/route/action/detail/:id",
  listSeriveRoute: "/route/serviceSalesman",
  detailServiceSalesman: "/serviceSalesman/detail/:id",
  createActionPage: "/route/action/:type/:id",
  createServiceRoute: "/route/service/:type/:id",
  createServiceSalesman: "/serviceSalesman/:type/:id",
  createSalesManSell: "/route/sell/:type/:id",

  //Best sale Product
  listBestSaleProductPage: "/products/best/sale/list",

  //Hot product
  listHotProductPage: "/products/hot/list",

  //NPP Product
  listNPPProductPage: "/products/npp/list",
  detailNPPProductPage: "/product/npp/detail/:id",

  //Banner
  listBannerPage: "/banner/list",
  detailBannerPage: "/banner/detail/:id",
  createBannerPage: "/banner/create/:id",

  //WAREHOUSE
  listWarehousePage: "/warehouse/list",
  createWarehousePage: "/warehouse/:type/:id",
  detailWarehousePage: "/warehouse/detail/:id",
  listStockReceipt: "/stock/receipt/list",
  detailStockReceipt: "/detail/stock/receipt/:id",
  listReleaseReceipt: "/release/receipt/list",
  detailReleaseReceipt: "/release/receipt/detail/:id",
  createStockReceipt: "/stock/receipt/:type/:id",

  //WAREHOUSE REPORT
  reportInputStock: "/warehouse/report/input/list",
  reportReleaseStock: "/warehouse/report/release/list",
  reportInventory: "/warehouse/report/inventory/list",
  historyInventory: "/warehouse/history/inventory/list/:id",
  //Payment
  paymentTransactionPage: "/payment/list",

  //Debit
  importDebitPage: "/debit/import",
  historyUpdateDebit: "/debit/history/:id",
  detailOrderDebit: "/debit/detailOrderDebit/:id",

  // an
  returnDebitPage: "/debit/returnDebitPage",

  //Order Temporary DLC1 and Detail
  orderTemporaryDLC1Page: "/orderTemporaryPageS1",
  detailOrderTemporary: "/orderTemporaryPageS1/detail/:id",

  listHideEventOfAgencyS1: "/agency/c1/event/hide/list/:id",

  listContracts: "/contract/list",
  detailOfContract: "/contract/detail/:id",
  editOfContract: "/contract/edit/:id",
  formContract: "/contract/form/:id",
  tradingSchedule: '/contract/tradingSchedule/detail/:id',
  editTradingSchedule: '/contract/tradingSchedule/edit/:id',
  //Survey
  listSurvey: "/survey/list",
  detailSurvey: "/survey/detail/:id",
  editSurvey: "/survey/:type/:id",

  // Chi tieu
  listKpiManagement: '/kpimanagement/list',
  detailKpiManagement: "/kpimanagement/detail/:id",
  createKpiManagement: '/kpimanagement/:type/:id',
  // Loai hinh thanh toan
  listTypeOfPayment: '/typeofpayment/list',
  detailTypeOfPayment: '/typeofpayment/detail/:id',
  editTypeOfPayment: '/typeofpayment/:type/:id'
};

const routes = [
  {
    name: "FormContract",
    path: routerPath.formContract,
    component: FormContract,
    exact: true,
  },
  {
    name: "ListHistorySurvey",
    path: routerPath.listHistorySurvey,
    component: ListHistorySurvey,
    exact: true,
  },
  {
    name: "DetailCustomer",
    path: routerPath.detailCustomer,
    component: DetailCustomer,
    exact: true,
  },
  {
    name: "CreateCustomer",
    path: routerPath.createCustomer,
    component: CreateCustomer,
    exact: true,
  },
  {
    name: "ListCustomer",
    path: routerPath.listCustomer,
    component: ListCustomer,
    exact: true,
  },
  {
    name: "DetailOrderRequest",
    path: routerPath.detailOrderRequest,
    component: DetailOrderRequest,
    exact: true,
  },
  {
    name: "ListOrderRequest",
    path: routerPath.listOrderRequest,
    component: ListOrderRequest,
    exact: true,
  },
  {
    name: "CreateStockReceipt",
    path: routerPath.createStockReceipt,
    component: CreateStockReceipt,
    exact: true,
  },
  {
    name: "DetailReleaseReceipt",
    path: routerPath.detailReleaseReceipt,
    component: DetailReleaseReceipt,
    exact: true,
  },
  {
    name: "ListReleaseReceipt",
    path: routerPath.listReleaseReceipt,
    component: ListReleaseReceipt,
    exact: true,
  },
  {
    name: "CreateSalesManSell",
    path: routerPath.createSalesManSell,
    component: CreateSalesManSell,
    exact: true,
  },
  {
    name: "CreateServiceRoute",
    path: routerPath.createServiceRoute,
    component: CreateServiceRoute,
    back: false,
  },
  {
    name: "CreateActionPage",
    path: routerPath.createActionPage,
    component: CreateActionPage,
    back: false,
  },
  {
    name: "DecentralizationPKDByAgencyC1",
    path: routerPath.decentralizationPKDByAgencyC1,
    component: DecentralizationPKDByAgencyC1,
    back: false,
  },
  {
    name: "DetailDecentralizationByAgencyC1",
    path: routerPath.detailDecentralizationByAgencyC1,
    component: DetailDecentralizationByAgencyC1,
    back: false,
  },
  {
    name: "CreateDecentralizationByAgencyC1",
    path: routerPath.createDecentralizationByAgencyC1,
    component: CreateDecentralizationByAgencyC1,
    back: false,
  },
  {
    name: "DecentralizationAdminByAgencyC1",
    path: routerPath.decentralizationAdminByAgencyC1,
    component: DecentralizationAdminByAgencyC1,
    back: false,
  },
  {
    name: "ListHotProductPage",
    path: routerPath.listHotProductPage,
    component: ListHotProductPage,
    back: false,
  },
  {
    name: "ListSetupNotificationApp",
    path: routerPath.listSetupNotificationApp,
    component: ListSetupNotificationApp,
    back: false,
  },
  {
    name: "ListHideOrShowProductAgencyC1",
    path: routerPath.listHideOrShowProductAgencyC1,
    component: ListHideOrShowProductAgencyC1,
    back: false,
  },
  {
    name: "ListProductPurchasedAgencyC1",
    path: routerPath.listProductPurchasedAgencyC1,
    component: ListProductPurchasedAgencyC1,
    back: false,
  },
  // trả ngay
  {
    name: "CreateEventImmediateNewChildren",
    path: routerPath.createEventImmediateNewChildren,
    component: CreateEventImmediateNewChildren,
    back: false,
  },
  {
    name: "DetailEventImmediateNew",
    path: routerPath.detailEventImmediateNew,
    component: DetailEventImmediateNew,
    back: false,
  },
  {
    name: "CreateEventImmediateDescriptionNew",
    path: routerPath.createEventImmediateDescriptionNew,
    component: CreateEventImmediateDescriptionNew,
    back: false,
  },
  {
    name: "CreateEventConditionAttachImmediateNew",
    path: routerPath.createEventConditionAttachImmediateNew,
    component: CreateEventConditionAttachImmediateNew,
    back: false,
  },
  {
    name: "CreateEventDesignImmediateNew",
    path: routerPath.createEventDesignImmediateNew,
    component: CreateEventDesignImmediateNew,
    back: false,
  },
  {
    name: "CreateEventAgencyFilterImmediateNew",
    path: routerPath.createEventAgencyFilterImmediateNew,
    component: CreateEventAgencyFilterImmediateNew,
    back: false,
  },
  {
    name: "CreateEventFilterImmediateNew",
    path: routerPath.createEventFilterImmediateNew,
    component: CreateEventFilterImmediateNew,
    back: false,
  },
  {
    name: "CreateEventImmediateNew",
    path: routerPath.createEventImmediateNew,
    component: CreateEventImmediateNew,
    back: false,
  },
  //

  {
    name: "CreateHideByObject",
    path: routerPath.createHideByObject,
    component: CreateHideByObject,
    back: false,
  },
  {
    name: "ListHideEventByObject",
    path: routerPath.listHideEventByObject,
    component: ListHideEventByObject,
    back: false,
  },
  {
    name: "ListHideBrandByObject",
    path: routerPath.listHideBrandByObject,
    component: ListHideBrandByObject,
    back: false,
  },
  {
    name: "ListHideSubCateByObject",
    path: routerPath.listHideSubCateByObject,
    component: ListHideSubCateByObject,
    back: false,
  },
  {
    name: "ListHideCateByObject",
    path: routerPath.listHideCateByObject,
    component: ListHideCateByObject,
    back: false,
  },
  {
    name: "ListHideProductByObject",
    path: routerPath.listHideProductByObject,
    component: ListHideProductByObject,
    back: false,
  },
  {
    name: "HistoryInventory",
    path: routerPath.historyInventory,
    component: HistoryInventory,
    back: false,
  },
  {
    name: "404 Page",
    path: "/404",
    component: Page404,
    back: false,
  },
  {
    name: "DetailNotificationNew",
    path: routerPath.detailNotificationNew,
    component: DetailNotificationNew,
    exact: true,
  },
  {
    name: "ListEventOfAgencyS1",
    path: routerPath.listEventOfAgencyS1,
    component: ListEventOfAgencyS1,
    exact: true,
  },
  // CATEGORY
  {
    name: "CreateProductCategory",
    path: routerPath.createProductCategory,
    component: ViewProductCategoryPage,
    exact: true,
  },
  {
    name: "ViewProductCategory",
    path: routerPath.viewProductCategory,
    component: ViewProductCategoryPage,
    exact: true,
  },
  {
    name: "ListProductCategory",
    path: routerPath.listProductCategory,
    component: ProductCategoryListPage,
    exact: false,
  },
  //   name: 'CreateEventDesginAgency',
  //   path: routerPath.CreateEventDesginAgency,
  //   component: CreateEventDesginAgency,
  //   back: false
  // },

  //Route
  {
    name: "CreateProductByObject",
    path: routerPath.createProductByObject,
    component: CreateProductByObject,
    exact: true,
  },
  {
    name: "ListProductByObject",
    path: routerPath.listProductByObject,
    component: ListProductByObject,
    exact: true,
  },
  {
    name: "ListBrandPage",
    path: routerPath.listBrandPage,
    component: ListBrandPage,
    exact: true,
  },
  {
    name: "CreateEventGiftDescription",
    path: routerPath.createEventGiftDescription,
    component: CreateEventGiftDescription,
    exact: true,
  },
  {
    name: "ListEventGiftPage",
    path: routerPath.listEventGiftPage,
    component: ListEventGiftPage,
    exact: true,
  },
  {
    name: "CreateEventGiftDesignCondition",
    path: routerPath.createEventGiftDesignCondition,
    component: CreateEventGiftDesignCondition,
    exact: true,
  },
  {
    name: "CreateEventGiftFilterAgencyC1",
    path: routerPath.createEventGiftFilterAgencyC1,
    component: CreateEventGiftFilterAgencyC1,
    exact: true,
  },
  {
    name: "CreateEventGiftFilterCondition",
    path: routerPath.createEventGiftFilterCondition,
    component: CreateEventGiftFilterCondition,
    exact: true,
  },
  {
    name: "CreateEventGift",
    path: routerPath.createEventGift,
    component: CreateEventGift,
    exact: true,
  },
  {
    name: "DetailProductVersion",
    path: routerPath.detailProductVersion,
    component: DetailProductVersion,
    exact: true,
  },
  {
    name: "DetailProductPage",
    path: routerPath.detailProductPage,
    component: DetailProductPage,
    exact: true,
  },
  {
    name: "EditVersionProduct",
    path: routerPath.editVersionProduct,
    component: EditVersionProduct,
    exact: true,
  },
  {
    name: "CreateProductPage",
    path: routerPath.createProductPage,
    component: CreateProductPage,
    exact: true,
  },
  {
    name: "ListProductPage",
    path: routerPath.listProductPage,
    component: ListProductPage,
    exact: true,
  },
  {
    name: "CreateEventImmediateDescription",
    path: routerPath.createEventImmediateDescription,
    component: CreateEventImmediateDescription,
    exact: true,
  },
  {
    name: "CreateEventAgencyFilterImmediate",
    path: routerPath.createEventAgencyFilterImmediate,
    component: CreateEventAgencyFilterImmediate,
    exact: true,
  },
  {
    name: "CreateEvenDesginImmediate",
    path: routerPath.createEvenDesginImmediate,
    component: CreateEvenDesginImmediate,
    exact: true,
  },
  {
    name: "CreateEventFilterImmediate",
    path: routerPath.createEventFilterImmediate,
    component: CreateEventFilterImmediate,
    exact: true,
  },
  {
    name: "CreateEventImmediate",
    path: routerPath.createEventImmediate,
    component: CreateEventImmediate,
    exact: true,
  },
  {
    name: "ListEventImmediatePage",
    path: routerPath.listEventImmediate,
    component: ListEventImmediatePage,
    exact: true,
  },
  {
    name: "DetailCoordinatorOrderS1",
    path: routerPath.detailOrderCoordinatorS1,
    component: DetailOrderCoordinatorS1,
    padding: true,
    exact: true,
  },
  {
    name: "DetailAction",
    path: routerPath.detailAction,
    component: DetailActionPage,
    exact: true,
  },
  {
    name: "DetailOrderS3",
    path: routerPath.detailOrderS3,
    component: DetailOrderS3,
    padding: true,
    exact: true,
  },
  {
    name: "OrderDLC3Page",
    path: routerPath.listOrderS3,
    component: OrderDLC3Page,
    exact: true,
  },
  {
    name: "SalesmanDetailSell",
    path: routerPath.detailServiceSalesman,
    component: DetailSeriveRoute,
    exact: true,
  },
  {
    name: "ListServiceRoute",
    path: routerPath.listSeriveRoute,
    component: ListServiceRoute,
    exact: true,
  },
  {
    name: "SalesmanDetailSell",
    path: routerPath.detailSalesManSell,
    component: DetailSellRoute,
    exact: true,
  },
  {
    name: "DetailOrderS1",
    path: routerPath.detailOrderS1,
    component: DetailOrderS1,
    padding: true,
    exact: true,
  },
  {
    name: "SellSalesman",
    path: routerPath.salesmanSell,
    component: ListSellRoute,
    back: true,
  },

  {
    name: "OrderListS2",
    path: routerPath.listOrderS1,
    component: OrderDLC1Page,
    back: true,
  },
  {
    name: "Detail Event",
    path: routerPath.DetailEeventPage,
    component: DetailEventPage,
    back: true,
  },
  {
    name: "StatisticalEventPage",
    path: routerPath.StatisticalEventPage,
    component: StatisticalEventPage,
    back: false,
  },
  {
    name: "CreateEventFilterAgency",
    path: routerPath.CreateEventFilterAgency,
    component: CreateEventFilterAgency,
    back: false,
  },
  {
    name: "CreateEventDescription",
    path: routerPath.CreateEventDescription,
    component: CreateEventDescription,
    back: false,
  },
  {
    name: "CreateEventDesgin",
    path: routerPath.CreateEventDesgin,
    component: CreateEventDesgin,
    back: false,
  },
  {
    name: "CreateEventFiler",
    path: routerPath.CreateEventFiler,
    component: CreateEventFilter,
    back: false,
  },
  {
    name: "OrderPage",
    path: "/orderPage",
    component: OrderPage,
    back: false,
  },
  {
    name: "Login",
    path: "/login",
    component: LoginPage,
    back: false,
  },
  {
    name: "DetailOrder",
    path: "/detailOrder/:id",
    component: DetailOrder,
    padding: true,

    back: true,
  },
  {
    name: "ProductPage",
    path: "/productPage",
    component: ProductPage,
    back: false,
  },
  {
    name: "ProductDetail",
    path: "/productDetail/:id",
    component: DetailProduct,
    back: true,
  },
  {
    name: "ProductCreate",
    path: "/productCreate",
    component: CreateProduct,
    back: true,
  },
  {
    name: "ProductEdit",
    path: "/productEdit/:id",
    component: editProduct,
    back: true,
  },
  {
    name: "TypeProductPage",
    path: "/typeProductPage",
    component: ProductTypePage,
    back: false,
  },
  {
    name: "DetailTypeProduct",
    path: "/detailTypeProduct/:id",
    component: DetailTypeProduct,
    back: true,
  },
  {
    name: "CreateTypeProduct",
    path: "/productCreateType",
    component: CreateTypeProduct,
    back: true,
  },
  {
    name: "EditTypeProduct",
    path: "/productEditType/:id",
    component: editTypeProduct,
    back: true,
  },
  {
    name: "AgencyPage",
    path: "/agencyPage",
    component: AgencyPage,
    back: false,
  },
  {
    name: "DetailAgency",
    path: "/detailAgency/:id",
    component: DetailAgency,
    back: true,
  },
  {
    name: "AgencyCreate",
    path: "/agencyS1/:type/:id",
    component: CreateAgency,
    back: true,
  },
  {
    name: "AgencyEdit",
    path: "/agencyEdit/:id",
    component: EditAgency,
    back: true,
  },
  {
    name: "StoreList",
    path: "/listStorePage",
    component: ListStorePage,
    back: false,
  },
  {
    name: "StoreList",
    path: "/storeDetail/:id",
    component: DetailStore,
    back: true,
  },
  {
    name: "StoreEdit",
    path: "/storeEdit/:id",
    component: EditStore,
    back: true,
  },
  {
    name: "StoreCreate",
    path: "/storeCreate",
    component: CreateStore,
    back: true,
  },
  {
    name: "StoreInputList",
    path: "/listStoreInputPage",
    component: ListStoreInputPage,
    back: false,
  },
  {
    name: "StoreOutputList",
    path: "/listStoreOutputPage",
    component: ListStoreOuputPage,
    back: false,
  },
  {
    name: "InventoryList",
    path: "/listInventoryPage",
    component: ListInventoryPage,
    back: false,
  },
  {
    name: "ShipperAssignList",
    path: "/listShipperAssignPage",
    component: ListShipperAssign,
    back: false,
  },
  {
    name: "DetailCoupon",
    path: "/DetailCouponPage/:id/:type",
    component: DetailCouponPage,
    back: false,
  },

  {
    name: "List Shipper",
    path: "/shipperPage",
    component: ListShipperPage,
    back: false,
  },
  {
    name: "Detail Shipper",
    path: "/detailShipperPage/:id",
    component: DetailShipperPage,
    back: false,
  },
  {
    name: "Create Shipper",
    path: "/createShipperPage",
    component: CreateShipperPage,
    back: false,
  },
  {
    name: "History Shipper",
    path: "/historyShipperPage/:id",
    component: HistoryShipperPage,
    back: true,
  },
  {
    name: "DetailCouponOutput",
    path: "/DetailCouponOutputPage",
    component: DetailCouponOutputPage,
    back: true,
  },
  {
    name: "CreateCouponInput",
    path: "/CreateCouponInputPage/:type",
    component: CreateCouponInputPage,
    back: true,
  },
  {
    name: "CreateCouponInput",
    path: "/CreateCouponInputPage",
    component: CreateCouponInputPage,
    back: true,
  },
  {
    name: "CreateCouponOnput",
    path: "/CreateCouponOnputPage",
    component: CreateCouponOnputPage,
    back: true,
  },
  {
    name: "List Shipper",
    path: "/shipperPage",
    component: ListShipperPage,
    back: false,
  },
  {
    name: "Detail Shipper",
    path: "/detailShipperPage/:id",
    component: DetailShipperPage,
    back: false,
  },
  {
    name: "Create Shipper",
    path: "/createShipperPage/",
    component: CreateShipperPage,
    back: false,
  },
  {
    name: "Edit Shipper",
    path: "/editShipperPage/:id",
    component: editShipperPage,
    back: false,
  },
  {
    name: "Edit Shipper",
    path: "/editShipperPage/:id",
    component: editShipperPage,
    back: false,
  },
  {
    name: "History Shipper",
    path: "/historyShipperPage/:id",
    component: HistoryShipperPage,
    back: false,
  },
  {
    name: "List Event",
    path: "/listEventPage",
    component: ListEventPage,
    back: false,
  },

  {
    name: "EventCreate",
    path: "/eventCreate/:type/:id",
    component: CreateEvent,
    back: true,
  },
  {
    name: "Edit Event",
    path: "/editEventPage/:id",
    component: EditEventPage,
    back: true,
  },
  {
    name: "SalesmanList",
    path: "/salesman/list",
    component: ListSalesmanPage,
    back: false,
  },
  {
    name: "SalesmanDetail",
    path: "/salesman/detail/:id",
    component: DetailSalesman,
    back: false,
  },
  {
    name: "CreateSalesman",
    path: "/salesman/:type/:id",
    component: createSalesman,
    back: false,
  },
  {
    name: "routeList",
    path: "/routePage",
    component: ListRoute,
    back: false,
  },
  {
    name: "RouteDetail",
    path: "/detailRoute/:id",
    component: DetailRoute,
    back: false,
  },
  {
    name: "CreateRouter",
    path: "/createRouter/:type/:id",
    component: createRouter,
    back: false,
  },
  {
    name: "HomeDashboard",
    path: "/homeDashboard",
    component: HomeDashboard,
    back: false,
  },

  {
    name: "DetailSalesDashboard",
    path: "/detailSalesDashboard",
    component: DetailSalesDashboard,
    back: false,
  },
  {
    name: "NewPage",
    path: "/newsPage",
    component: NewsPage,
    back: false,
  },
  {
    name: "DetailNews",
    path: "/newsDetail/:id",
    component: DetailNews,
    back: false,
  },
  {
    name: "CreateNews",
    path: "/news/:type/:id",
    component: CreateNews,
    back: false,
  },
  {
    name: "ListComment",
    path: "/new/:id/comment",
    component: CommentPage,
    back: false,
  },
  {
    name: "ListNewsCategories",
    path: "/new/categories/:type/page",
    component: NewCategoriesPage,
    back: false,
  },
  {
    name: "DetaiNewsCategories",
    path: "/new/categories/detail/:id",
    component: DetailNewsCategories,
    back: false,
  },
  {
    name: "CreateNewsCategories",
    path: "/new/:type/categories/:id",
    component: CreateNewsCategories,
    back: false,
  },
  {
    name: "NewsPrioritize",
    path: "/prioritize/news/page",
    component: NewsPrioritizePage,
    back: false,
  },
  {
    name: "NotificationPage",
    path: "/notificationPage",
    component: NotificationPage,
    back: false,
  },
  {
    name: "DetailNotification",
    path: "/notification/:id",
    component: DetailNotification,
    back: false,
  },
  {
    name: "CreateNotification",
    path: "/notifications/:type/:id/:typeNoti",
    component: createNotification,
    back: false,
  },
  {
    name: "Agency S2",
    path: "/agencyPageS2",
    component: AgencyS2Page,
    back: false,
  },
  {
    name: "DetailAgencyS2",
    path: "/detailAgencyS2/:id",
    component: DetailAgencyS2,
    back: false,
  },
  {
    name: "CreateAgencyS2",
    path: "/agencyS2/:type/:id",
    component: createAgencyS2,
    back: false,
  },
  {
    name: "BranchName",
    path: "/branchpage",
    component: BranchPage,
    back: false,
  },
  {
    name: "Detailbranch",
    path: "/detailbranch/:id",
    component: DetailBranch,
    back: false,
  },
  {
    name: "DistributionPage",
    path: "/distributionPage",
    component: DistributionPage,
    back: false,
  },
  {
    name: "DistributionPage",
    path: "/distributionPage",
    component: DistributionPage,
    back: false,
  },
  {
    name: "DetailDistribution",
    path: "/detailDistribution/:id",
    component: DetailDistribution,
    back: false,
  },
  {
    name: "EditDistributionPage",
    path: "/editDistributionPage/:type",
    component: editDistributionPage,
    back: false,
  },
  {
    name: "EditBranchPage",
    path: "/editBranchPage/:type",
    component: editBranchPage,
    back: false,
  },
  {
    name: "Test",
    path: "/Test",
    component: Test,
    back: false,
  },
  {
    name: "report",
    path: "/reportGeneralPage",
    component: ReportGeneralPage,
    back: false,
  },
  {
    name: "DetailAgencyS1Page",
    path: routerPath.DetailAgencyS1Page,
    component: DetailAgencyS1Page,
    back: false,
  },
  {
    name: "ActionPage",
    path: routerPath.action,
    component: ActionPage,
    back: false,
  },
  {
    name: "StatisticalEventImmediatePage",
    path: routerPath.statisticalEventImmediatePage,
    component: StatisticalEventImmediatePage,
    back: false,
  },
  {
    name: "DetailEventImmediatePage",
    path: routerPath.detailEventImmediatePage,
    component: DetailEventImmediatePage,
    back: false,
  },
  {
    name: "DetailDebtAgencyS2",
    path: routerPath.detailDebtAgencyS2,
    component: DetailDebtAgencyS2,
    back: false,
  },
  {
    name: "ListBestSaleProductPage",
    path: routerPath.listBestSaleProductPage,
    component: ListBestSaleProductPage,
    back: false,
  },
  {
    name: "ListNPPProductPage",
    path: routerPath.listNPPProductPage,
    component: ListNPPProductPage,
    back: false,
  },
  {
    name: "DetailNPPProductPage",
    path: routerPath.detailNPPProductPage,
    component: DetailNPPProductPage,
    back: false,
  },
  {
    name: "ListEndUsersPage",
    path: routerPath.listEndUsersPage,
    component: ListEndUsersPage,
    back: false,
  },
  {
    name: "DetailEndUsersPage",
    path: routerPath.detailEndUsersPage,
    component: DetailEndUsersPage,
    back: false,
  },
  {
    name: "EditEndUserPage",
    path: routerPath.editEndUserPage,
    component: EditEndUserPage,
    back: false,
  },
  {
    name: "ListBannerPage",
    path: routerPath.listBannerPage,
    component: ListBannerPage,
    back: false,
  },
  {
    name: "DetailBannerPage",
    path: routerPath.detailBannerPage,
    component: DetailBannerPage,
    back: false,
  },
  {
    name: "CreateBannerPage",
    path: routerPath.createBannerPage,
    component: CreateBannerPage,
    back: false,
  },
  {
    name: "ListWarehousePage",
    path: routerPath.listWarehousePage,
    component: ListWarehousePage,
    back: false,
  },
  {
    name: "DetailWarehousePage",
    path: routerPath.detailWarehousePage,
    component: DetailWarehousePage,
    back: false,
  },
  {
    name: "ReportInputStock",
    path: routerPath.reportInputStock,
    component: ReportInputStock,
    back: false,
  },
  {
    name: "ReportReleaseStock",
    path: routerPath.reportReleaseStock,
    component: ReportReleaseStock,
    back: false,
  },
  {
    name: "ReportInventory",
    path: routerPath.reportInventory,
    component: ReportInventory,
    back: false,
  },
  {
    name: "ListStockReceipt",
    path: routerPath.listStockReceipt,
    component: ListStockReceipt,
    back: false,
  },
  {
    name: "DetailStockReceipt",
    path: routerPath.detailStockReceipt,
    component: DetailStockReceipt,
    back: false,
    exact: true,
  },
  {
    name: "CreateWarehousePage",
    path: routerPath.createWarehousePage,
    component: CreateWarehousePage,
    back: false,
  },
  {
    name: "PaymentTransactionPage",
    path: routerPath.paymentTransactionPage,
    component: PaymentTransactionPage,
    back: false,
  },
  {
    name: "ImportPaymentPage",
    path: routerPath.importDebitPage,
    component: ImportDebitPage,
    back: false,
  },
  // an

  {
    name: "returnDebitPage",
    path: routerPath.returnDebitPage,
    component: ReturnDebitPage,
    back: false,
  },

  {
    name: "HistoryUpdateDebit",
    path: routerPath.historyUpdateDebit,
    component: HistoryUpdateDebitPage,
    back: false,
  },
  {
    name: "DetailOrderDebit",
    path: routerPath.detailOrderDebit,
    component: DetailOrderDebitPage,
    back: false,
  },

  {
    name: "OrderTemporaryDLC1Page",
    path: routerPath.orderTemporaryDLC1Page,
    component: OrderTemporaryDLC1Page,
    back: false,
  },
  {
    name: "DetailOrderTemporary",
    path: routerPath.detailOrderTemporary,
    component: DetailOrderTemporary,
    exact: true,
    back: false,
  },
  {
    name: "StatisticalEventGiftPage",
    path: routerPath.statisticalEventGiftPage,
    component: StatisticalEventGiftPage,
    exact: true,
  },
  {
    name: "HistoryStatisticalEventGiftPage",
    path: routerPath.historyStatisticalEventGiftPage,
    component: HistoryStatisticalEventGiftPage,
    exact: true,
  },
  {
    name: "DetailSalesPolicyEventGiftPage",
    path: routerPath.detailSalesPolicyEventGiftPage,
    component: DetailSalesPolicyEventGiftPage,
    exact: true,
  },
  //phân quyền
  {
    name: "CreateDecentralizationUser",
    path: routerPath.create_decentralization_user,
    component: CreateDecentralizationUser,
    back: false,
  },

  {
    name: "DetailDecentralizationUser",
    path: routerPath.detail_decentralization_user,
    component: DetailDecentralizationUser,
    back: false,
  },
  {
    name: "ListDecentralizationUser",
    path: routerPath.list_decentralization_user,
    component: ListDecentralizationUser,
    back: false,
  },

  {
    name: "CreateDecentralizationADMIN",
    path: routerPath.create_decentralization_admin,
    component: CreateDecentralization,
    back: false,
  },

  {
    name: "DetailDecentralizationADMIN",
    path: routerPath.detail_decentralization_admin,
    component: DetailDecentralization,
    back: false,
  },
  {
    name: "ListDecentralizationTTPP",
    path: routerPath.list_decentralization,
    component: ListDecentralization,
    back: false,
  },
  {
    name: "ListDecentralizationChiNhanh",
    path: routerPath.list_decentralization,
    component: ListDecentralization,
    back: false,
  },
  {
    name: "ListDecentralizationAdmin",
    path: routerPath.list_decentralization,
    component: ListDecentralization,
    back: false,
  },
  {
    name: "ListHideEventOfAgencyS1",
    path: routerPath.listHideEventOfAgencyS1,
    component: ListHideEventOfAgencyS1,
    back: false,
  },
  {
    name: "ListOfContracts",
    path: routerPath.listContracts,
    component: ListOfContracts,
    back: false,
  },
  {
    name: "DetailOfContract",
    path: routerPath.detailOfContract,
    component: DetailOfContract,
    back: false,
  },
  {
    name: "EditInfomationOfContracts",
    path: routerPath.editOfContract,
    component: EditInfomationOfContracts,
    back: false,
  },
  {
    name: "ListOfSurvey",
    path: routerPath.listSurvey,
    component: ListSurvey,
    back: false,
  },
  {
    name: "DetailOfSurvey",
    path: routerPath.detailSurvey,
    component: DetailSurvey,
    back: false,
  },
  {
    name: "CreateSurvey",
    path: routerPath.editSurvey,
    component: CreateSurvey,
    back: false,
  },

  {
    name: "ListKpiManagement",
    path: routerPath.listKpiManagement,
    component: ListKpi,
    back: false,
  },
  {
    name: "DetailKpiManagement",
    path: routerPath.detailKpiManagement,
    component: DetailKpi,
    back: false,
  },
  {
    name: "CreateKpiManagement",
    path: routerPath.createKpiManagement,
    component: CreateKpi,
    back: false,
  },
  {
    name: "ListTypeOfPayment",
    path: routerPath.listTypeOfPayment,
    component: ListPaymentType,
    back: false,
  },
  {
    name: "DetailTypeOfPayment",
    path: routerPath.detailTypeOfPayment,
    component: DetailTypeOfPayment,
    back: false,
  },
  {
    name: "EditTypeOfPayment",
    path: routerPath.editTypeOfPayment,
    component: EditTypeOfPayment,
    back: false,
  },
  {
    name: "DetailTradingSchedule",
    path: routerPath.tradingSchedule,
    component: DetailTradingSchedule,
    back: false
  },
  {
    name: "EditTradingSchedule",
    path: routerPath.editTradingSchedule,
    component: EditTradingSchedule,
    back: false
  },

];

export default routes;
