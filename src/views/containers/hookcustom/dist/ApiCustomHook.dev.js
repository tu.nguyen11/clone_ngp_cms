"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._getAPIListCounty = void 0;

var _require = require("../../../services"),
    APIService = _require.APIService;

var _getAPIListCounty = function _getAPIListCounty(city_id, search, calback) {
  var data, dataNew;
  return regeneratorRuntime.async(function _getAPIListCounty$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap(APIService._getListCounty(city_id, search));

        case 3:
          data = _context.sent;
          dataNew = data.district.map(function (item, index) {
            var key = item.id;
            var value = item.name;
            return {
              key: key,
              value: value
            };
          });
          calback(dataNew);
          _context.next = 11;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          console.log(_context.t0);

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 8]]);
};

exports._getAPIListCounty = _getAPIListCounty;