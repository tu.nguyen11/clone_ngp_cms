const { APIService } = require("../../../services");

const _getAPIListCounty = async (city_id, search, calback) => {
  try {
    const data = await APIService._getListCounty(city_id, search);
    const dataNew = data.district.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    calback(dataNew);
  } catch (err) {
    console.log(err);
  }
};
const _getAPIListWard = async (district_id, search, calback) => {
  try {
    const data = await APIService._getListWard(district_id, search);
    const dataNew = data.wards.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    calback(dataNew);
  } catch (err) {
    console.log(err);
  }
};
const _getAPIListAgencyS1 = async (search, calback) => {
  try {
    const data = await APIService._getListAgencyS1Name(search);
    const dataNew = data.suppliers_name.map((item, index) => {
      const key = item.id;
      const value = item.name;
      return {
        key,
        value,
      };
    });
    calback(dataNew);
  } catch (err) {
    console.log(err);
  }
};

export { _getAPIListCounty, _getAPIListWard, _getAPIListAgencyS1 };
