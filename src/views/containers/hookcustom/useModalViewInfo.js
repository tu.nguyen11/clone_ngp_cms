import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { SearchOutlined } from '@ant-design/icons'
import { Input } from 'antd'
import { Col, Row, List, Empty } from 'antd'
import { APIService } from '../../../services'
import { StyledITileHeading } from '../../components/common/Font/font'
import { colors } from '../../../assets'
import FormatterDay from '../../../utils/FormatterDay'

import { ITableHtml, ISvg } from '../../components'

const { Search } = Input

const StyledSearchCustom = styled(Search)`
  height: 42px;
  padding-right: 6px;
  width: 100%;
  border: 0px !important;
  border-bottom: 1px solid #7a7b7b !important;
  .ant-input-suffix {
    opacity: 0 !important;
  }
  .ant-input:focus {
    box-shadow: none !important;
  }
  -webkit-box-shadow: 0 0 0 0 white !important;
`

let headerTable = [
  {
    name: 'STT',
    align: 'center'
  },
  {
    name: 'Hình ảnh',
    align: 'left'
  },
  {
    name: 'Tên sản phẩm',
    align: 'left'
  },
  {
    name: 'Quy cách 1',
    align: 'center'
  },
  {
    name: 'Quy cách 2',
    align: 'center'
  }
]

let timer = null

export default function UseModalViewInfo (
  title,
  callbackClose = () => {},
  id_order,
  order_codeS2
) {
  const headerTableProduct = headerTable => {
    return (
      <tr className='tr-table'>
        {headerTable.map((item, index) => (
          <th className={'th-table'} style={{ textAlign: item.align }}>
            {item.name}
          </th>
        ))}
      </tr>
    )
  }

  const bodyTableProduct = contentTable => {
    return contentTable.length === 0 ? (
      <tr className='tr-table' style={{ textAlign: 'center' }}>
        <td colSpan='5' style={{ padding: '24px 0px' }}>
          <Empty description='Không có dữ liệu' />
        </td>
      </tr>
    ) : (
      contentTable.map((item, index) => (
        <tr className='tr-table'>
          <td className='td-table' style={{ textAlign: 'center' }}>
            {index + 1}
          </td>
          <td className='td-table'>
            <img
              src={item.image_url + item.image_thumbnail}
              style={{
                width: 40,
                height: 40
              }}
              alt='sản phẩm'
            />
          </td>
          <td className='td-table'>{item.name}</td>
          <td className='td-table' style={{ textAlign: 'center' }}>
            {item.max_quantity}
          </td>
          <td className='td-table' style={{ textAlign: 'center' }}>
            {item.min_quantity}
          </td>
        </tr>
      ))
    )
  }

  const [dataList, setDataList] = useState([])

  const [filter, setFilter] = useState({
    id: id_order,
    search: ''
  })
  const [isLoading, setIsLoading] = useState(true)

  const [dataDetailS2, setDataDetailS2] = useState({ list_product: [] })

  const [isLoadingContent, setLoadingContent] = useState(true)

  const [code_orderS2, setCode_OrderS2] = useState(order_codeS2)

  const [isCheckIndex, setCheckIndex] = useState(0)

  const fetchData = async filter => {
    try {
      const data = await APIService._getAllCodeOrderS2(filter.id, filter.search)
      setDataList(data.order_s2)
      setIsLoading(false)
    } catch (err) {
      setIsLoading(false)
      console.log(err)
    }
  }

  const fetchDataDetailS2 = async code_orderS2 => {
    try {
      const data = await APIService._getDetailCodeOrderS2(code_orderS2)
      setDataDetailS2(data.detail_s2)
      setLoadingContent(false)
    } catch (err) {
      setLoadingContent(false)
      console.log(err)
    }
  }

  useEffect(() => {
    fetchData(filter)
  }, [filter])

  useEffect(() => {
    if (code_orderS2 === '0') {
      return
    }
    fetchDataDetailS2(code_orderS2)
  }, [code_orderS2])

  return (
    <div style={{ width: '100%', height: '100%' }}>
      <div
        style={{
          background: '#FCEFED',
          width: 20,
          height: 20,
          borderRadius: 10,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          position: 'absolute',
          top: -8,
          right: -8
        }}
        onClick={() => callbackClose()}
        className='cursor-push remove'
      >
        <ISvg name={ISvg.NAME.CROSS} width={8} fill='#E35F4B' />
      </div>
      <Row gutter={[0, 16]}>
        <Col span={24}>
          <StyledITileHeading minFont='10px' maxFont='16px'>
            {title}
          </StyledITileHeading>
        </Col>
        <Col span={24}>
          <div style={{ width: '100%', height: '100%' }}>
            <Row gutter={[26, 0]}>
              <Col span={8}>
                <div style={{ width: '100%', height: '100%' }}>
                  <Row gutter={[0, 12]}>
                    <Col span={24} style={{ width: '100%' }}>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          alignItems: 'center',
                          width: '100%'
                        }}
                      >
                        {/* <Space size={16}> */}
                        <SearchOutlined
                          style={{
                            fontSize: 20,
                            color: colors.svg_default
                          }}
                        />
                        <StyledSearchCustom
                          style={{ width: 260 }}
                          placeholder='nhập từ khóa tìm kiếm'
                          suffix={() => {
                            return null
                          }}
                          prefix={() => {
                            return null
                          }}
                          onChange={e => {
                            let keysearch = e.target.value
                            if (timer) {
                              clearTimeout(timer)
                            }
                            timer = setTimeout(() => {
                              setIsLoading(true)
                              setFilter({ ...filter, search: keysearch })
                            }, 500)
                          }}
                          onSearch={value => {
                            setIsLoading(true)
                            setFilter({ ...filter, search: value })
                          }}
                        />
                        {/* </Space> */}
                      </div>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          height: 392,
                          overflow: 'auto',
                          border: '1px solid rgba(122, 123, 123, 0.5)',
                          paddingLeft: 18,
                          paddingRight: 18
                        }}
                      >
                        <List
                          dataSource={dataList}
                          bordered={false}
                          loading={isLoading}
                          renderItem={(item, index) => (
                            <List.Item
                              className='cursor-push'
                              onClick={() => {
                                setLoadingContent(true)
                                setCode_OrderS2(item.order_code_child)
                                setCheckIndex(index)
                              }}
                            >
                              <span
                                id={
                                  isCheckIndex === index ? 'font_name' : 'white'
                                }
                              >
                                {item.order_code_child}
                              </span>
                            </List.Item>
                          )}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col span={16}>
                <div>
                  <Row gutter={[0, 6]}>
                    <Col span={24}>
                      <Row gutter={[0, 18]}>
                        <Col span={24}>
                          <Row gutter={[12, 0]}>
                            <Col span={6}>
                              <p style={{ fontWeight: 500 }}>Tên S2</p>
                              <span>
                                {dataDetailS2.name_s2 === '' ||
                                typeof dataDetailS2.name_s2 === 'undefined'
                                  ? '-'
                                  : dataDetailS2.name_s2}
                              </span>
                            </Col>
                            <Col span={6}>
                              <p style={{ fontWeight: 500 }}>Ngày đặt</p>
                              <span>
                                {dataDetailS2.order_date === '' ||
                                typeof dataDetailS2.order_date === 'undefined'
                                  ? '-'
                                  : FormatterDay.dateFormatWithString(
                                      dataDetailS2.order_date,
                                      '#DD#/#MM#/#YYYY#'
                                    )}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[12, 0]}>
                            <Col span={6}>
                              <p style={{ fontWeight: 500 }}>Mã S2</p>
                              <span>
                                {dataDetailS2.s2_code === '' ||
                                typeof dataDetailS2.s2_code === 'undefined'
                                  ? '-'
                                  : dataDetailS2.s2_code}
                              </span>
                            </Col>
                            <Col span={6}>
                              <p style={{ fontWeight: 500 }}>Người đặt</p>
                              <span>
                                {dataDetailS2.order_man === '' ||
                                typeof dataDetailS2.s2_code === 'undefined'
                                  ? '-'
                                  : dataDetailS2.order_man}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={24}>
                      <div
                        style={{
                          height: 500,
                          border: '1px solid rgba(122, 123, 123, 0.5)'
                        }}
                      >
                        <ITableHtml
                          childrenBody={bodyTableProduct(
                            dataDetailS2.list_product
                          )}
                          childrenHeader={headerTableProduct(headerTable)}
                          isBorder={false}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  )
}
