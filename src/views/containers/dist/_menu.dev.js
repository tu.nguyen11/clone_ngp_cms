"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _components = require("../components");

var _ref;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = [{
  name: _components.ISvg.NAME.ORDER,
  opacity: 0.5,
  value: 'Đơn hàng',
  index: 0,
  subMenu: 0,
  children: [{
    value: 'Danh sách đơn hàng S2 lên S1',
    path: '/orderPage',
    index: 0,
    subMenu: 0
  }]
}, {
  name: _components.ISvg.NAME.TAG,
  opacity: 0.5,
  value: 'Sản phẩm',
  subMenu: 1,
  children: [{
    value: 'Danh sách sản phẩm',
    path: '/productPage',
    index: 0,
    subMenu: 1
  }, {
    value: 'Phân loại sản phẩm',
    path: '/typeProductPage',
    index: 1,
    subMenu: 1
  }]
}, // {
//   index: 2,
//   name: ISvg.NAME.ARCHIVE,
//   opacity: 0.5,
//   hover: false,
//   value: "Quản lý kho",
//   active: false,
//   children: [
//     {
//       value: "Danh sách kho",
//       path: "/listStorePage",
//       index: 0,
//       subMenu: 2,
//     },
//     {
//       value: "Nhập kho",
//       path: "/listStoreInputPage",
//       index: 1,
//       subMenu: 2,
//     },
//     {
//       value: "Xuất kho",
//       path: "/listStoreOutputPage",
//       index: 2,
//       subMenu: 2,
//     },
//     {
//       value: "Tồn kho",
//       path: "/listInventoryPage",
//       index: 3,
//       subMenu: 2,
//     },
//     {
//       value: "Gán Shipper",
//       path: "/listShipperAssignPage",
//       index: 4,
//       subMenu: 2,
//     },
//   ],
// },
{
  name: _components.ISvg.NAME.STORE,
  opacity: 0.5,
  hover: false,
  value: 'Đại lý',
  subMenu: 2,
  index: 2,
  children: [{
    value: 'Đại lý cấp S1',
    path: '/agencyPage',
    index: 0,
    subMenu: 2
  }, {
    value: 'Đại lý cấp S2',
    path: '/agencyPageS2',
    index: 1,
    subMenu: 2
  }, {
    value: 'Chi nhánh',
    path: '/branchpage',
    index: 2,
    subMenu: 2
  }, {
    value: 'Trung tâm phân phối',
    path: '/distributionPage',
    index: 3,
    subMenu: 2
  }]
}, {
  name: _components.ISvg.NAME.BROADCAST,
  opacity: 0.5,
  hover: false,
  value: 'Chương Trình',
  subMenu: 3,
  index: 3,
  children: [{
    value: 'Chương trình tích lũy',
    path: '/listEventPage',
    index: 0,
    subMenu: 3
  } // {
  //   value: "Chương trình trả ngay",
  //   path: "/listEventPage",
  //   index: 1,
  //   subMenu: 3,
  // },
  ]
}, {
  name: _components.ISvg.NAME.SHIPPER,
  opacity: 0.5,
  hover: false,
  value: 'Shipper',
  subMenu: 4,
  index: 4,
  path: '/shipperPage',
  children: []
}, {
  name: _components.ISvg.NAME.SALEMAN,
  hover: true,
  value: 'Nhân viên bán hàng',
  subMenu: 5,
  index: 5,
  path: '/salesmanPage',
  children: []
}, {
  index: 6,
  name: _components.ISvg.NAME.LOCATION,
  hover: true,
  value: 'Tuyến',
  subMenu: 6,
  path: '/routePage',
  children: [// {
    //   value: "Quản lý tuyến",
    //   path: "/routePage",
    //   index: 6,
    //   subMenu: 6
    // }
  ]
}, (_ref = {
  index: 7,
  name: _components.ISvg.NAME.Wifi,
  hover: true,
  value: 'Tin tức',
  subMenu: 7
}, _defineProperty(_ref, "index", 7), _defineProperty(_ref, "children", [{
  value: 'Danh sách tin tức',
  path: '/newsPage',
  index: 0,
  subMenu: 7
}, {
  value: 'Tin tức ưu tiên',
  path: '/prioritize/news/page',
  index: 1,
  subMenu: 7
}, {
  value: 'Chuyên mục bài viết',
  path: '/new/categories/NEWS/page',
  index: 2,
  subMenu: 7
}, {
  value: 'Chuyên mục video',
  path: '/new/categories/VIDEO/page',
  index: 3,
  subMenu: 7
}]), _ref), {
  name: _components.ISvg.NAME.BELL,
  opacity: 0.5,
  hover: false,
  value: 'Thông báo',
  subMenu: 8,
  index: 8,
  path: '/notificationPage',
  children: []
}, {
  name: _components.ISvg.NAME.Dashboard,
  opacity: 0.5,
  hover: false,
  value: 'Dashboard',
  subMenu: 9,
  index: 9,
  path: '/homeDashboard',
  children: []
}];
exports["default"] = _default;