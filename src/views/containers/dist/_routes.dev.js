"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _LoginPage = _interopRequireDefault(require("../pages/LoginPage"));

var _HomePage = _interopRequireDefault(require("../pages/HomePage"));

var _OrderPage = _interopRequireDefault(require("../pages/Order/OrderPage"));

var _DetailOrder = _interopRequireDefault(require("../pages/Order/DetailOrder"));

var _ProductPage = _interopRequireDefault(require("../pages/Product/ProductPage"));

var _DetailProduct = _interopRequireDefault(require("../pages/Product/DetailProduct"));

var _CreateProduct = _interopRequireDefault(require("../pages/Product/CreateProduct"));

var _ProductTypePage = _interopRequireDefault(require("../pages/Product/type/ProductTypePage"));

var _DetailTypeProduct = _interopRequireDefault(require("../pages/Product/type/DetailTypeProduct"));

var _CreateTypeProduct = _interopRequireDefault(require("../pages/Product/type/CreateTypeProduct"));

var _DetailAgency = _interopRequireDefault(require("../pages/Agency/AgenCyS1/DetailAgency"));

var _CreateAgency = _interopRequireDefault(require("../pages/Agency/AgenCyS1/CreateAgency"));

var _EditAgency = _interopRequireDefault(require("../pages/Agency/AgenCyS1/EditAgency"));

var _EditProduct = _interopRequireDefault(require("../pages/Product/EditProduct"));

var _EditTypeProduct = _interopRequireDefault(require("../pages/Product/type/EditTypeProduct"));

var _PrintOrderPage = _interopRequireDefault(require("../pages/Order/PrintOrderPage"));

var _ListShipperPage = _interopRequireDefault(require("../pages/Shipper/ListShipperPage"));

var _DetailShipperPage = _interopRequireDefault(require("../pages/Shipper/DetailShipperPage"));

var _CreateShipperPage = _interopRequireDefault(require("../pages/Shipper/CreateShipperPage"));

var _HistoryShipperPage = _interopRequireDefault(require("../pages/Shipper/HistoryShipperPage"));

var _ListStorePage = _interopRequireDefault(require("../pages/Store/ListStorePage"));

var _ListStoreInputPage = _interopRequireDefault(require("../pages/Store/ListStoreInputPage"));

var _ListShipperAssign = _interopRequireDefault(require("../pages/Store/ListShipperAssign"));

var _ListInventoryPage = _interopRequireDefault(require("../pages/Store/ListInventoryPage"));

var _ListStoreOuputPage = _interopRequireDefault(require("../pages/Store/ListStoreOuputPage"));

var _DetailCouponPage = _interopRequireDefault(require("../pages/Store/DetailCouponPage"));

var _DetailCouponOutputPage = _interopRequireDefault(require("../pages/Store/DetailCouponOutputPage"));

var _CreateCouponInputPage = _interopRequireDefault(require("../pages/Store/CreateCouponInputPage"));

var _CreateCouponPage = _interopRequireDefault(require("../pages/Store/CreateCouponPage"));

var _CreateCouponOutputPage = _interopRequireDefault(require("../pages/Store/CreateCouponOutputPage"));

var _DetailStore = _interopRequireDefault(require("../pages/Store/DetailStore"));

var _EditStore = _interopRequireDefault(require("../pages/Store/EditStore"));

var _CreateStore = _interopRequireDefault(require("../pages/Store/CreateStore"));

var _EditShipperPage = _interopRequireDefault(require("../pages/Shipper/EditShipperPage"));

var _PrintCouponPage = _interopRequireDefault(require("../pages/Store/PrintCouponPage"));

var _ListEventPage = _interopRequireDefault(require("../pages/Event/ListEventPage"));

var _CreateEvent = _interopRequireDefault(require("../pages/Event/CreateEvent"));

var _DetailEventPage = _interopRequireDefault(require("../pages/Event/DetailEventPage"));

var _EditEventPage = _interopRequireDefault(require("../pages/Event/EditEventPage"));

var _ListSalesmanPage = _interopRequireDefault(require("../pages/Salesman/ListSalesmanPage"));

var _DetailSalesman = _interopRequireDefault(require("../pages/Salesman/DetailSalesman"));

var _CreateSalesman = _interopRequireDefault(require("../pages/Salesman/CreateSalesman"));

var _ListRoute = _interopRequireDefault(require("../pages/Route/ListRoute"));

var _DetailRoute = _interopRequireDefault(require("../pages/Route/DetailRoute"));

var _CreateRouter = _interopRequireDefault(require("../pages/Route/CreateRouter"));

var _HomeDashboard = _interopRequireDefault(require("../pages/Dashboard/HomeDashboard"));

var _DetailSalesDashboard = _interopRequireDefault(require("../pages/Dashboard/DetailSalesDashboard"));

var _BusinessDashboard = _interopRequireDefault(require("../pages/Dashboard/BusinessDashboard"));

var _NewsPage = _interopRequireDefault(require("../pages/News/NewsPage"));

var _DetailNews = _interopRequireDefault(require("../pages/News/DetailNews"));

var _CreateNews = _interopRequireDefault(require("../pages/News/CreateNews"));

var _CommentPage = _interopRequireDefault(require("../pages/News/CommentPage"));

var _NewCategoriesPage = _interopRequireDefault(require("../pages/News/NewCategoriesPage"));

var _DetailNewsCategories = _interopRequireDefault(require("../pages/News/DetailNewsCategories"));

var _CreateNewsCategories = _interopRequireDefault(require("../pages/News/CreateNewsCategories"));

var _NewsPrioritizePage = _interopRequireDefault(require("../pages/News/NewsPrioritizePage"));

var _NotificationPage = _interopRequireDefault(require("../pages/Notification/NotificationPage"));

var _DetailNotification = _interopRequireDefault(require("../pages/Notification/DetailNotification"));

var _CreateNotification = _interopRequireDefault(require("../pages/Notification/CreateNotification"));

var _AgencyPage = _interopRequireDefault(require("../pages/Agency/AgenCyS1/AgencyPage"));

var _AgencyS2Page = _interopRequireDefault(require("../pages/Agency/AgencyS2/AgencyS2Page"));

var _DetailAgencyS = _interopRequireDefault(require("../pages/Agency/AgencyS2/DetailAgencyS2"));

var _CreateAgencyS = _interopRequireDefault(require("../pages/Agency/AgencyS2/CreateAgencyS2"));

var _BranchPage = _interopRequireDefault(require("../pages/Agency/Branch/BranchPage"));

var _DetailBranch = _interopRequireDefault(require("../pages/Agency/Branch/DetailBranch"));

var _DistributionPage = _interopRequireDefault(require("../pages/Agency/Distribution/DistributionPage"));

var _DetailDistribution = _interopRequireDefault(require("../pages/Agency/Distribution/DetailDistribution"));

var _EditDistributionPage = _interopRequireDefault(require("../pages/Agency/Distribution/EditDistributionPage"));

var _EditBranchPage = _interopRequireDefault(require("../pages/Agency/Branch/EditBranchPage"));

var _Test = _interopRequireDefault(require("../pages/Dashboard/Test"));

var _CreateEventFilter = _interopRequireDefault(require("../pages/Event/EventFilter/CreateEventFilter"));

var _CreateEventDesgin = _interopRequireDefault(require("../pages/Event/EventFilter/CreateEventDesgin"));

var _CreateEventFilter2 = _interopRequireDefault(require("../pages/Event/EventAgency/CreateEventFilter"));

var _StatisticalEventPage = _interopRequireDefault(require("../pages/Event/StatisticalEventPage"));

var _CreateEventDescription = _interopRequireDefault(require("../pages/Event/CreateEventDescription"));

var _ReportGeneralPage = _interopRequireDefault(require("../pages/Report/ReportGeneralPage"));

var _DetailAgentcyS1Page = _interopRequireDefault(require("../pages/Agency/AgenCyS1/DetailAgentcyS1Page"));

var _OrderDLC1Page = _interopRequireDefault(require("../pages/Order/OrderDLC1Page"));

var _ListSellRoute = _interopRequireDefault(require("../pages/Route/Sell/ListSellRoute"));

var _DetailOrderS = _interopRequireDefault(require("../pages/Order/DetailOrderS1"));

var _DetailSellRoute = _interopRequireDefault(require("../pages/Route/Sell/DetailSellRoute"));

var _ListServiceRoute = _interopRequireDefault(require("../pages/Route/Service/ListServiceRoute"));

var _DetailSeriveRoute = _interopRequireDefault(require("../pages/Route/Service/DetailSeriveRoute"));

var _OrderDLC3Page = _interopRequireDefault(require("../pages/Order/OrderDLC3Page"));

var _DetailOrderS2 = _interopRequireDefault(require("../pages/Order/DetailOrderS3"));

var _ActionPage = _interopRequireDefault(require("../pages/Route/Sell/ActionPage"));

var _DetailActionPage = _interopRequireDefault(require("../pages/Route/Sell/DetailActionPage"));

var _DetailOrderCoordinatorS = _interopRequireDefault(require("../pages/Order/DetailOrderCoordinatorS1"));

var _ListEventImmediatePage = _interopRequireDefault(require("../pages/Event/Immediate/ListEventImmediatePage"));

var _CreateEventImmediate = _interopRequireDefault(require("../pages/Event/Immediate/CreateEventImmediate"));

var _CreateEventFilterImmediate = _interopRequireDefault(require("../pages/Event/Immediate/CreateEventFilterImmediate"));

var _CreateEventDesginImmediate = _interopRequireDefault(require("../pages/Event/Immediate/CreateEventDesginImmediate"));

var _StatisticalEventImmediatePage = _interopRequireDefault(require("../pages/Event/Immediate/StatisticalEventImmediatePage"));

var _DetailEventImmediatePage = _interopRequireDefault(require("../pages/Event/Immediate/DetailEventImmediatePage"));

var _CreateEventAgencyFilterImmediate = _interopRequireDefault(require("../pages/Event/Immediate/CreateEventAgencyFilterImmediate"));

var _CreateEventImmediateDescription = _interopRequireDefault(require("../pages/Event/Immediate/CreateEventImmediateDescription"));

var _DetailDebtAgencyS = _interopRequireDefault(require("../pages/Agency/AgencyS2/DetailDebtAgencyS2"));

var _ListHotProductPage = _interopRequireDefault(require("../pages/Product/hot/ListHotProductPage"));

var _ListNPPProductPage = _interopRequireDefault(require("../pages/Product/NPP/ListNPPProductPage"));

var _DetailNPPProductPage = _interopRequireDefault(require("../pages/Product/NPP/DetailNPPProductPage"));

var _ListEndUsersPage = _interopRequireDefault(require("../pages/Agency/EndUser/ListEndUsersPage"));

var _DetailEndUsersPage = _interopRequireDefault(require("../pages/Agency/EndUser/DetailEndUsersPage"));

var _EditEndUserPage = _interopRequireDefault(require("../pages/Agency/EndUser/EditEndUserPage"));

var _ListBannerPage = _interopRequireDefault(require("../pages/Banner/ListBannerPage"));

var _DetailBannerPage = _interopRequireDefault(require("../pages/Banner/DetailBannerPage"));

var _CreateBannerPage = _interopRequireDefault(require("../pages/Banner/CreateBannerPage"));

var _ListProductPage = _interopRequireDefault(require("../pages/Product/NewProduct/ListProductPage"));

var _CreateProductPage = _interopRequireDefault(require("../pages/Product/NewProduct/CreateProductPage"));

var _EditVersionProduct = _interopRequireDefault(require("../pages/Product/NewProduct/EditVersionProduct"));

var _DetailProductPage = _interopRequireDefault(require("../pages/Product/NewProduct/DetailProductPage"));

var _DetailProductVersion = _interopRequireDefault(require("../pages/Product/NewProduct/DetailProductVersion"));

var _ListWarehousePage = _interopRequireDefault(require("../pages/Warehouse/ListWarehousePage"));

var _CreateWarehousePage = _interopRequireDefault(require("../pages/Warehouse/CreateWarehousePage"));

var _DetailWarehousePage = _interopRequireDefault(require("../pages/Warehouse/DetailWarehousePage"));

var _ReportInputStock = _interopRequireDefault(require("../pages/Warehouse/Report/ReportInputStock"));

var _ReportReleaseStock = _interopRequireDefault(require("../pages/Warehouse/Report/ReportReleaseStock"));

var _ReportInventory = _interopRequireDefault(require("../pages/Warehouse/Report/ReportInventory"));

var _ListStockReceipt = _interopRequireDefault(require("../pages/Warehouse/Stock/ListStockReceipt"));

var _DetailStockReceipt = _interopRequireDefault(require("../pages/Warehouse/Stock/DetailStockReceipt"));

var _PaymentTransactionPage = _interopRequireDefault(require("../pages/Debit/PaymentTransactionPage"));

var _ImportDebitPage = _interopRequireDefault(require("../pages/Import/ImportDebitPage"));

var _HistoryUpdateDebitPage = _interopRequireDefault(require("../pages/Debit/HistoryUpdateDebitPage"));

var _DetailOrderDebitPage = _interopRequireDefault(require("../pages/Debit/DetailOrderDebitPage"));

var _OrderTemporaryDLC1Page = _interopRequireDefault(require("../pages/Order/OrderTemporaryDLC1Page"));

var _DetailOrderTemporary = _interopRequireDefault(require("../pages/Order/DetailOrderTemporary"));

var _CreateEventGift = _interopRequireDefault(require("../pages/Event/EventGift/Create/CreateEventGift"));

var _CreateEventGiftFilterCondition = _interopRequireDefault(require("../pages/Event/EventGift/Create/CreateEventGiftFilterCondition"));

var _CreateEventGiftFilterAgencyC = _interopRequireDefault(require("../pages/Event/EventGift/Create/CreateEventGiftFilterAgencyC1"));

var _CreateEventGiftDesignCondition = _interopRequireDefault(require("../pages/Event/EventGift/Create/CreateEventGiftDesignCondition"));

var _ListEventGiftPage = _interopRequireDefault(require("../pages/Event/EventGift/ListEventGiftPage"));

var _CreateEventGiftDescription = _interopRequireDefault(require("../pages/Event/EventGift/Create/CreateEventGiftDescription"));

var _StatisticalEventGiftPage = _interopRequireDefault(require("../pages/Event/EventGift/StatisticalEventGiftPage"));

var _HistoryStatisticalEventGiftPage = _interopRequireDefault(require("../pages/Event/EventGift/HistoryStatisticalEventGiftPage"));

var _DetailSalesPolicyEventGiftPage = _interopRequireDefault(require("../pages/Event/EventGift/DetailSalesPolicyEventGiftPage"));

var _ListBrandPage = _interopRequireDefault(require("../pages/Brand/ListBrandPage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import CreateEventDesginAgency from '../pages/Event/EventAgency/CreateEventDesgin'
var routerPath = {
  // BRAND
  listBrandPage: "/brand/list",
  //EVENT GIFT
  createEventGift: "/event/gift/:type/:id",
  createEventGiftFilterCondition: "/event/gift/filter/:type/:id",
  createEventGiftFilterAgencyC1: "/event/gift/agencyS1/:type/:id",
  createEventGiftDesignCondition: "/event/gift/design/condition/:type/:id",
  listEventGiftPage: "/event/gift/list",
  createEventGiftDescription: "/event/gift/description/:type/:id",
  //NEW PRODUCT PAGES
  listProductPage: "/product/list",
  createProductPage: "/product/:type/:id",
  editVersionProduct: "/product/:type/:id/version/:idVersion/:index",
  detailProductPage: "/product/detail/:id",
  detailProductVersion: "/product/detail/:id/version/:idVersion/:index",
  //END USERS
  listEndUsersPage: "/end/user/list",
  detailEndUsersPage: "/end/user/detail/:id",
  editEndUserPage: "/end/user/edit/:id",
  //EVENT ACCUMULATION
  CreateEventFiler: "/eventCreate/filter/:id",
  CreateEventDesgin: "/eventCreate/filter/desgin/:id",
  CreateEventFilterAgency: "/eventCreate/filter_agency/:level/:id",
  // CreateEventDesginAgency: '/eventCreate/filter_agency/desgin/:id',
  CreateEventDescription: "/eventCreate/description/:type/:id",
  StatisticalEventPage: "/statisticalEventPage/:id",
  DetailEeventPage: "/detailEventPage/:type/:id",
  detailDebtAgencyS2: "/agencyS2/debt/detail/:id",
  // EVENT IMMEEDIATE
  listEventImmediate: "/event/immediate/list",
  createEventImmediate: "/event/immediate/create/:id",
  DetailAgencyS1Page: "/agency/detail/:id",
  createEventFilterImmediate: "/event/immediate/create/filter/:id",
  createEvenDesginImmediate: "/event/immediate/create/desgin/:id",
  statisticalEventImmediatePage: "/event/immediate/statistical",
  detailEventImmediatePage: "/event/immediate/detail/:type/:id",
  createEventAgencyFilterImmediate: "/event/immediate/create/filter/agency/:level/:id",
  createEventImmediateDescription: "/event/immediate/description/:type/:id",
  statisticalEventGiftPage: "/event/eventGift/statisticalEventGiftPage/:id",
  historyStatisticalEventGiftPage: "/event/eventGift/historyStatisticalEventGiftPage/:id",
  detailSalesPolicyEventGiftPage: "/event/eventGift/detailSalesPolicyEventGiftPage/:id",
  // ORDER
  listOrderS1: "/orderPageS1",
  detailOrderS1: "/orderPageS1/detail/:id",
  detailOrderCoordinatorS1: "/orderPageS1/coordinator/detail/:id/:order_code/:order_codeS2/:length_S2",
  listOrderS3: "/orderPageS3",
  detailOrderS3: "/orderPageS3/detail/:order_code",
  //Route
  salesmanSell: "/route/sellSalesman",
  detailSalesManSell: "/sellSalesman/detail/:id",
  action: "/route/action",
  detailAction: "/route/action/detail/:id",
  listSeriveRoute: "/route/serviceSalesman",
  detailServiceSalesman: "/serviceSalesman/detail/:id",
  //Hot Product
  listHotProductPage: "/products/hot/list",
  //NPP Product
  listNPPProductPage: "/products/npp/list",
  detailNPPProductPage: "/product/npp/detail/:id",
  //Banner
  listBannerPage: "/banner/list",
  detailBannerPage: "/banner/detail/:id",
  createBannerPage: "/banner/create/:id",
  //WAREHOUSE
  listWarehousePage: "/warehouse/list",
  createWarehousePage: "/warehouse/:type/:id",
  detailWarehousePage: "/warehouse/detail/:id",
  listStockReceipt: "/stock/receipt/list",
  detailStockReceipt: "/stock/receipt/detail/:id",
  //WAREHOUSE REPORT
  reportInputStock: "/warehouse/report/input/list",
  reportReleaseStock: "/warehouse/report/release/list",
  reportInventory: "/warehouse/report/inventory/list",
  //Payment
  paymentTransactionPage: "/payment/list",
  //Debit
  importDebitPage: "/debit/import",
  historyUpdateDebit: "/debit/history/:id",
  detailOrderDebit: "/debit/detailOrderDebit/:id",
  //Order Temporary DLC1 and Detail
  orderTemporaryDLC1Page: "/orderTemporaryPageS1",
  detailOrderTemporary: "/orderTemporaryPageS1/detail/:id"
};
var routes = [// {
//   name: 'CreateEventDesginAgency',
//   path: routerPath.CreateEventDesginAgency,
//   component: CreateEventDesginAgency,
//   back: false
// },
//Route ListBrandPage
{
  name: "ListBrandPage",
  path: routerPath.listBrandPage,
  component: _ListBrandPage["default"],
  exact: true
}, {
  name: "CreateEventGiftDescription",
  path: routerPath.createEventGiftDescription,
  component: _CreateEventGiftDescription["default"],
  exact: true
}, {
  name: "ListEventGiftPage",
  path: routerPath.listEventGiftPage,
  component: _ListEventGiftPage["default"],
  exact: true
}, {
  name: "CreateEventGiftDesignCondition",
  path: routerPath.createEventGiftDesignCondition,
  component: _CreateEventGiftDesignCondition["default"],
  exact: true
}, {
  name: "CreateEventGiftFilterAgencyC1",
  path: routerPath.createEventGiftFilterAgencyC1,
  component: _CreateEventGiftFilterAgencyC["default"],
  exact: true
}, {
  name: "CreateEventGiftFilterCondition",
  path: routerPath.createEventGiftFilterCondition,
  component: _CreateEventGiftFilterCondition["default"],
  exact: true
}, {
  name: "CreateEventGift",
  path: routerPath.createEventGift,
  component: _CreateEventGift["default"],
  exact: true
}, {
  name: "DetailProductVersion",
  path: routerPath.detailProductVersion,
  component: _DetailProductVersion["default"],
  exact: true
}, {
  name: "DetailProductPage",
  path: routerPath.detailProductPage,
  component: _DetailProductPage["default"],
  exact: true
}, {
  name: "EditVersionProduct",
  path: routerPath.editVersionProduct,
  component: _EditVersionProduct["default"],
  exact: true
}, {
  name: "CreateProductPage",
  path: routerPath.createProductPage,
  component: _CreateProductPage["default"],
  exact: true
}, {
  name: "ListProductPage",
  path: routerPath.listProductPage,
  component: _ListProductPage["default"],
  exact: true
}, {
  name: "CreateEventImmediateDescription",
  path: routerPath.createEventImmediateDescription,
  component: _CreateEventImmediateDescription["default"],
  exact: true
}, {
  name: "CreateEventAgencyFilterImmediate",
  path: routerPath.createEventAgencyFilterImmediate,
  component: _CreateEventAgencyFilterImmediate["default"],
  exact: true
}, {
  name: "CreateEvenDesginImmediate",
  path: routerPath.createEvenDesginImmediate,
  component: _CreateEventDesginImmediate["default"],
  exact: true
}, {
  name: "CreateEventFilterImmediate",
  path: routerPath.createEventFilterImmediate,
  component: _CreateEventFilterImmediate["default"],
  exact: true
}, {
  name: "CreateEventImmediate",
  path: routerPath.createEventImmediate,
  component: _CreateEventImmediate["default"],
  exact: true
}, {
  name: "ListEventImmediatePage",
  path: routerPath.listEventImmediate,
  component: _ListEventImmediatePage["default"],
  exact: true
}, {
  name: "DetailCoordinatorOrderS1",
  path: routerPath.detailOrderCoordinatorS1,
  component: _DetailOrderCoordinatorS["default"],
  padding: true,
  exact: true
}, {
  name: "DetailAction",
  path: routerPath.detailAction,
  component: _DetailActionPage["default"],
  exact: true
}, {
  name: "DetailOrderS3",
  path: routerPath.detailOrderS3,
  component: _DetailOrderS2["default"],
  padding: true,
  exact: true
}, {
  name: "OrderDLC3Page",
  path: routerPath.listOrderS3,
  component: _OrderDLC3Page["default"],
  exact: true
}, {
  name: "SalesmanDetailSell",
  path: routerPath.detailServiceSalesman,
  component: _DetailSeriveRoute["default"],
  exact: true
}, {
  name: "ListServiceRoute",
  path: routerPath.listSeriveRoute,
  component: _ListServiceRoute["default"],
  exact: true
}, {
  name: "SalesmanDetailSell",
  path: routerPath.detailSalesManSell,
  component: _DetailSellRoute["default"],
  exact: true
}, {
  name: "DetailOrderS1",
  path: routerPath.detailOrderS1,
  component: _DetailOrderS["default"],
  padding: true,
  exact: true
}, {
  name: "SellSalesman",
  path: routerPath.salesmanSell,
  component: _ListSellRoute["default"],
  back: true
}, {
  name: "OrderListS2",
  path: routerPath.listOrderS1,
  component: _OrderDLC1Page["default"],
  back: true
}, {
  name: "Detail Event",
  path: routerPath.DetailEeventPage,
  component: _DetailEventPage["default"],
  back: true
}, {
  name: "StatisticalEventPage",
  path: routerPath.StatisticalEventPage,
  component: _StatisticalEventPage["default"],
  back: false
}, {
  name: "CreateEventFilterAgency",
  path: routerPath.CreateEventFilterAgency,
  component: _CreateEventFilter2["default"],
  back: false
}, {
  name: "CreateEventDescription",
  path: routerPath.CreateEventDescription,
  component: _CreateEventDescription["default"],
  back: false
}, {
  name: "CreateEventDesgin",
  path: routerPath.CreateEventDesgin,
  component: _CreateEventDesgin["default"],
  back: false
}, {
  name: "CreateEventFiler",
  path: routerPath.CreateEventFiler,
  component: _CreateEventFilter["default"],
  back: false
}, {
  name: "OrderPage",
  path: "/orderPage",
  component: _OrderPage["default"],
  back: false
}, {
  name: "Login",
  path: "/login",
  component: _LoginPage["default"],
  back: false
}, {
  name: "DetailOrder",
  path: "/detailOrder/:id",
  component: _DetailOrder["default"],
  padding: true,
  back: true
}, {
  name: "ProductPage",
  path: "/productPage",
  component: _ProductPage["default"],
  back: false
}, {
  name: "ProductDetail",
  path: "/productDetail/:id",
  component: _DetailProduct["default"],
  back: true
}, {
  name: "ProductCreate",
  path: "/productCreate",
  component: _CreateProduct["default"],
  back: true
}, {
  name: "ProductEdit",
  path: "/productEdit/:id",
  component: _EditProduct["default"],
  back: true
}, {
  name: "TypeProductPage",
  path: "/typeProductPage",
  component: _ProductTypePage["default"],
  back: false
}, {
  name: "DetailTypeProduct",
  path: "/detailTypeProduct/:id",
  component: _DetailTypeProduct["default"],
  back: true
}, {
  name: "CreateTypeProduct",
  path: "/productCreateType",
  component: _CreateTypeProduct["default"],
  back: true
}, {
  name: "EditTypeProduct",
  path: "/productEditType/:id",
  component: _EditTypeProduct["default"],
  back: true
}, {
  name: "AgencyPage",
  path: "/agencyPage",
  component: _AgencyPage["default"],
  back: false
}, {
  name: "DetailAgency",
  path: "/detailAgency/:id",
  component: _DetailAgency["default"],
  back: true
}, {
  name: "AgencyCreate",
  path: "/agencyS1/:type/:id",
  component: _CreateAgency["default"],
  back: true
}, {
  name: "AgencyEdit",
  path: "/agencyEdit/:id",
  component: _EditAgency["default"],
  back: true
}, {
  name: "StoreList",
  path: "/listStorePage",
  component: _ListStorePage["default"],
  back: false
}, {
  name: "StoreList",
  path: "/storeDetail/:id",
  component: _DetailStore["default"],
  back: true
}, {
  name: "StoreEdit",
  path: "/storeEdit/:id",
  component: _EditStore["default"],
  back: true
}, {
  name: "StoreCreate",
  path: "/storeCreate",
  component: _CreateStore["default"],
  back: true
}, {
  name: "StoreInputList",
  path: "/listStoreInputPage",
  component: _ListStoreInputPage["default"],
  back: false
}, {
  name: "StoreOutputList",
  path: "/listStoreOutputPage",
  component: _ListStoreOuputPage["default"],
  back: false
}, {
  name: "InventoryList",
  path: "/listInventoryPage",
  component: _ListInventoryPage["default"],
  back: false
}, {
  name: "ShipperAssignList",
  path: "/listShipperAssignPage",
  component: _ListShipperAssign["default"],
  back: false
}, {
  name: "DetailCoupon",
  path: "/DetailCouponPage/:id/:type",
  component: _DetailCouponPage["default"],
  back: false
}, {
  name: "List Shipper",
  path: "/shipperPage",
  component: _ListShipperPage["default"],
  back: false
}, {
  name: "Detail Shipper",
  path: "/detailShipperPage/:id",
  component: _DetailShipperPage["default"],
  back: false
}, {
  name: "Create Shipper",
  path: "/createShipperPage",
  component: _CreateShipperPage["default"],
  back: false
}, {
  name: "History Shipper",
  path: "/historyShipperPage/:id",
  component: _HistoryShipperPage["default"],
  back: true
}, {
  name: "DetailCouponOutput",
  path: "/DetailCouponOutputPage",
  component: _DetailCouponOutputPage["default"],
  back: true
}, {
  name: "CreateCouponInput",
  path: "/CreateCouponInputPage/:type",
  component: _CreateCouponInputPage["default"],
  back: true
}, {
  name: "CreateCouponInput",
  path: "/CreateCouponInputPage",
  component: _CreateCouponInputPage["default"],
  back: true
}, {
  name: "CreateCouponOnput",
  path: "/CreateCouponOnputPage",
  component: _CreateCouponOutputPage["default"],
  back: true
}, {
  name: "List Shipper",
  path: "/shipperPage",
  component: _ListShipperPage["default"],
  back: false
}, {
  name: "Detail Shipper",
  path: "/detailShipperPage/:id",
  component: _DetailShipperPage["default"],
  back: false
}, {
  name: "Create Shipper",
  path: "/createShipperPage/",
  component: _CreateShipperPage["default"],
  back: false
}, {
  name: "Edit Shipper",
  path: "/editShipperPage/:id",
  component: _EditShipperPage["default"],
  back: false
}, {
  name: "Edit Shipper",
  path: "/editShipperPage/:id",
  component: _EditShipperPage["default"],
  back: false
}, {
  name: "History Shipper",
  path: "/historyShipperPage/:id",
  component: _HistoryShipperPage["default"],
  back: false
}, {
  name: "List Event",
  path: "/listEventPage",
  component: _ListEventPage["default"],
  back: false
}, {
  name: "EventCreate",
  path: "/eventCreate/:type/:id",
  component: _CreateEvent["default"],
  back: true
}, {
  name: "Edit Event",
  path: "/editEventPage/:id",
  component: _EditEventPage["default"],
  back: true
}, {
  name: "SalesmanList",
  path: "/salesmanPage",
  component: _ListSalesmanPage["default"],
  back: false
}, {
  name: "SalesmanDetail",
  path: "/detailSalesman/:id",
  component: _DetailSalesman["default"],
  back: false
}, {
  name: "CreateSalesman",
  path: "/createSalesman/:type/:id",
  component: _CreateSalesman["default"],
  back: false
}, {
  name: "routeList",
  path: "/routePage",
  component: _ListRoute["default"],
  back: false
}, {
  name: "RouteDetail",
  path: "/detailRoute/:id",
  component: _DetailRoute["default"],
  back: false
}, {
  name: "CreateRouter",
  path: "/createRouter/:type/:id",
  component: _CreateRouter["default"],
  back: false
}, {
  name: "HomeDashboard",
  path: "/homeDashboard",
  component: _HomeDashboard["default"],
  back: false
}, {
  name: "DetailSalesDashboard",
  path: "/detailSalesDashboard",
  component: _DetailSalesDashboard["default"],
  back: false
}, {
  name: "NewPage",
  path: "/newsPage",
  component: _NewsPage["default"],
  back: false
}, {
  name: "DetailNews",
  path: "/newsDetail/:id",
  component: _DetailNews["default"],
  back: false
}, {
  name: "CreateNews",
  path: "/news/:type/:id",
  component: _CreateNews["default"],
  back: false
}, {
  name: "ListComment",
  path: "/new/:id/comment",
  component: _CommentPage["default"],
  back: false
}, {
  name: "ListNewsCategories",
  path: "/new/categories/:type/page",
  component: _NewCategoriesPage["default"],
  back: false
}, {
  name: "DetaiNewsCategories",
  path: "/new/categories/detail/:id",
  component: _DetailNewsCategories["default"],
  back: false
}, {
  name: "CreateNewsCategories",
  path: "/new/:type/categories/:id",
  component: _CreateNewsCategories["default"],
  back: false
}, {
  name: "NewsPrioritize",
  path: "/prioritize/news/page",
  component: _NewsPrioritizePage["default"],
  back: false
}, {
  name: "NotificationPage",
  path: "/notificationPage",
  component: _NotificationPage["default"],
  back: false
}, {
  name: "DetailNotification",
  path: "/notification/:id",
  component: _DetailNotification["default"],
  back: false
}, {
  name: "CreateNotification",
  path: "/notifications/:type/:id",
  component: _CreateNotification["default"],
  back: false
}, {
  name: "Agency S2",
  path: "/agencyPageS2",
  component: _AgencyS2Page["default"],
  back: false
}, {
  name: "DetailAgencyS2",
  path: "/detailAgencyS2/:id",
  component: _DetailAgencyS["default"],
  back: false
}, {
  name: "CreateAgencyS2",
  path: "/agencyS2/:type/:id",
  component: _CreateAgencyS["default"],
  back: false
}, {
  name: "BranchName",
  path: "/branchpage",
  component: _BranchPage["default"],
  back: false
}, {
  name: "Detailbranch",
  path: "/detailbranch/:id",
  component: _DetailBranch["default"],
  back: false
}, {
  name: "DistributionPage",
  path: "/distributionPage",
  component: _DistributionPage["default"],
  back: false
}, {
  name: "DistributionPage",
  path: "/distributionPage",
  component: _DistributionPage["default"],
  back: false
}, {
  name: "DetailDistribution",
  path: "/detailDistribution/:id",
  component: _DetailDistribution["default"],
  back: false
}, {
  name: "EditDistributionPage",
  path: "/editDistributionPage/:type",
  component: _EditDistributionPage["default"],
  back: false
}, {
  name: "EditBranchPage",
  path: "/editBranchPage/:type",
  component: _EditBranchPage["default"],
  back: false
}, {
  name: "Test",
  path: "/Test",
  component: _Test["default"],
  back: false
}, {
  name: "report",
  path: "/reportGeneralPage",
  component: _ReportGeneralPage["default"],
  back: false
}, {
  name: "DetailAgencyS1Page",
  path: routerPath.DetailAgencyS1Page,
  component: _DetailAgentcyS1Page["default"],
  back: false
}, {
  name: "ActionPage",
  path: routerPath.action,
  component: _ActionPage["default"],
  back: false
}, {
  name: "StatisticalEventImmediatePage",
  path: routerPath.statisticalEventImmediatePage,
  component: _StatisticalEventImmediatePage["default"],
  back: false
}, {
  name: "DetailEventImmediatePage",
  path: routerPath.detailEventImmediatePage,
  component: _DetailEventImmediatePage["default"],
  back: false
}, {
  name: "DetailDebtAgencyS2",
  path: routerPath.detailDebtAgencyS2,
  component: _DetailDebtAgencyS["default"],
  back: false
}, {
  name: "ListHotProductPage",
  path: routerPath.listHotProductPage,
  component: _ListHotProductPage["default"],
  back: false
}, {
  name: "ListNPPProductPage",
  path: routerPath.listNPPProductPage,
  component: _ListNPPProductPage["default"],
  back: false
}, {
  name: "DetailNPPProductPage",
  path: routerPath.detailNPPProductPage,
  component: _DetailNPPProductPage["default"],
  back: false
}, {
  name: "ListEndUsersPage",
  path: routerPath.listEndUsersPage,
  component: _ListEndUsersPage["default"],
  back: false
}, {
  name: "DetailEndUsersPage",
  path: routerPath.detailEndUsersPage,
  component: _DetailEndUsersPage["default"],
  back: false
}, {
  name: "EditEndUserPage",
  path: routerPath.editEndUserPage,
  component: _EditEndUserPage["default"],
  back: false
}, {
  name: "ListBannerPage",
  path: routerPath.listBannerPage,
  component: _ListBannerPage["default"],
  back: false
}, {
  name: "DetailBannerPage",
  path: routerPath.detailBannerPage,
  component: _DetailBannerPage["default"],
  back: false
}, {
  name: "CreateBannerPage",
  path: routerPath.createBannerPage,
  component: _CreateBannerPage["default"],
  back: false
}, {
  name: "ListWarehousePage",
  path: routerPath.listWarehousePage,
  component: _ListWarehousePage["default"],
  back: false
}, {
  name: "DetailWarehousePage",
  path: routerPath.detailWarehousePage,
  component: _DetailWarehousePage["default"],
  back: false
}, {
  name: "ReportInputStock",
  path: routerPath.reportInputStock,
  component: _ReportInputStock["default"],
  back: false
}, {
  name: "ReportReleaseStock",
  path: routerPath.reportReleaseStock,
  component: _ReportReleaseStock["default"],
  back: false
}, {
  name: "ReportInventory",
  path: routerPath.reportInventory,
  component: _ReportInventory["default"],
  back: false
}, {
  name: "ListStockReceipt",
  path: routerPath.listStockReceipt,
  component: _ListStockReceipt["default"],
  back: false
}, {
  name: "DetailStockReceipt",
  path: routerPath.detailStockReceipt,
  component: _DetailStockReceipt["default"],
  back: false
}, {
  name: "CreateWarehousePage",
  path: routerPath.createWarehousePage,
  component: _CreateWarehousePage["default"],
  back: false
}, {
  name: "PaymentTransactionPage",
  path: routerPath.paymentTransactionPage,
  component: _PaymentTransactionPage["default"],
  back: false
}, {
  name: "ImportPaymentPage",
  path: routerPath.importDebitPage,
  component: _ImportDebitPage["default"],
  back: false
}, {
  name: "HistoryUpdateDebit",
  path: routerPath.historyUpdateDebit,
  component: _HistoryUpdateDebitPage["default"],
  back: false
}, {
  name: "DetailOrderDebit",
  path: routerPath.detailOrderDebit,
  component: _DetailOrderDebitPage["default"],
  back: false
}, {
  name: "OrderTemporaryDLC1Page",
  path: routerPath.orderTemporaryDLC1Page,
  component: _OrderTemporaryDLC1Page["default"],
  back: false
}, {
  name: "DetailOrderTemporary",
  path: routerPath.detailOrderTemporary,
  component: _DetailOrderTemporary["default"],
  exact: true,
  back: false
}, {
  name: "StatisticalEventGiftPage",
  path: routerPath.statisticalEventGiftPage,
  component: _StatisticalEventGiftPage["default"],
  exact: true
}, {
  name: "HistoryStatisticalEventGiftPage",
  path: routerPath.historyStatisticalEventGiftPage,
  component: _HistoryStatisticalEventGiftPage["default"],
  exact: true
}, {
  name: "DetailSalesPolicyEventGiftPage",
  path: routerPath.detailSalesPolicyEventGiftPage,
  component: _DetailSalesPolicyEventGiftPage["default"],
  exact: true
}];
var _default = routes;
exports["default"] = _default;