"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.UPDATE_KEY_REWARD = exports.UPDATE_TREE_KEYDEFAULT = exports.ASSIGNED_KEY = exports.REMOVE_TREE = exports.UPDATE_DATA_PRODUCT = exports.CREATE_DATA_PRODUCT = exports.CREATE_KEY_DEGSIN_IMMEDIATE = exports.CREATE_DATE_EVENT_FILTER_IMMEDIATE = exports.CREATE_DATE_EVENT_IMMEDIATE = exports.UPDATE_TREE_PRIORITY = exports.CHECK_VALIDATE = exports.RESET_MODAL = exports.UPDATE_KEY_CATE = exports.UPDATE_DESGIN_EVENT_DISCOUNT = exports.CANCEL_TREE = exports.ASSIGNED_DATA_REDUX_DETAIL = exports.UPDATE_TREE = exports.CLEAR_REDUX_IMMEDIATE = exports.CLEAR_REDUX = exports.SAVE_TREE = exports.SHOW_TREE = exports.ADD_TREE = exports.UPDATE_ACTIVELISTAGENCY_MODAL = exports.CANCEL_MODAL = exports.DEAFUAT_MODAL = exports.REST_MODAL = exports.ARRAY_MODAL = exports.CONFIRM_MODAL = exports.SAVE_MODAL = exports.UPDATE_ACTIVELIST_MODAL = exports.REMOVE_MODAL = exports.ADD_MODAL = exports.UPDATE_DESGIN_EVENT = exports.REMOVE_DESGIN_EVENT = exports.ADD_DESGIN_EVENT = exports.CREATE_DATE_EVENT_FILTER = exports.CREATE_DATE_EVENT = exports.CREATE_PRODUCT_VERSION = exports.CLEAR_REDUX_PRODUCT = exports.CREATE_PRODUCT_INFO = exports.CHECK_PRODUCT_DEFAULT = exports.CREATE_PRODUCT_VERSION_INFO = exports.ADD_SPECIFICATION_INFO = exports.REMOVE_SPECIFICATION_INFO = exports.CREATE_PRODUCT_SPECIFICATION_INFO = exports.CLEAR_REDUX_PRODUCT_VERSION = exports.ASSIGNED_DATA_PRODUCT_REDUX_DETAIL = exports.ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL = exports.SORT_LIST_VERSION = exports.CHECK_NULL_VALUE_LIST_SPECIFICATION = exports.ADD_ITEM_SPEC_INFO = exports.ASSIGN_MIN_MAX_UNIT = exports.CREATE_EVENT_GIFT_FILTER = exports.CREATE_INFO_EVENT_GIFT = exports.CLEAR_REDUX_EVENT_GIFT = exports.ADD_EVENT_GIFT_LEVEL = exports.REMOVE_EVENT_GIFT_LEVEL = exports.UPDATE_KEY_CATE_EVENT_GIFT = exports.UPDATE_DESIGN_EVENT_GIFT = exports.CHECK_VALIDATE_EVENT_GIFT = exports.SAVE_TREE_NEW = exports.CANCEL_TREE_NEW = exports.REMOVE_PRODUCT = exports.REMOVE_PRODUCT_CONFIRM = exports.ASSIGN_LIST_KEY_CLONE = exports.ADD_TREE_KEY_CLONE = exports.UPDATE_ID_COUNT_SAVE_ITEM = exports.ADD_PRODUCT_GROUP_LEVEL = exports.SAVE_TREE_GROUP = exports.SAVE_TREE_GROUP_OVER = exports.CLOSE_MODAL_TREE = exports.ASSIGNED_TREE_CLICK_GROUP = exports.ADD_DESIGN_EVENT_LEVEL_GROUP = exports.REMOVE_PRODUCT_GROUP = exports.REMOVE_LEVEL = exports.CHANGE_CONDITION_APPLY = exports.CLEAR_REDUX_PRODUCTS_BY_OBJECT = exports.CREATE_PRODUCT_BY_OBJECT_INFO = exports.CHANGE_PRICE_PRODUCT_BY_OBJECT = exports.CHECK_VALIDATE_PRICE_BY_OBJECT = exports.REMOVE_PRODUCT_BY_OBJECT_CONFIRM = exports.REMOVE_PRODUCT_BY_OBJECT = void 0;

var _toolkit = require("@reduxjs/toolkit");

var _services = require("../../services");

var _reducerEvent = require("../reducer/event/reducerEvent");

var _reducerEventImmediate = require("../reducer/event/reducerEventImmediate");

var _reducerEventGift = require("../reducer/event/reducerEventGift");

var _reducerModal = require("../reducer/modal/reducerModal");

var _reducerTree = require("../reducer/modal/reducerTree");

var _reducerVersionProduct = require("../reducer/product/reducerVersionProduct");

var _reducerProductsByObject = require("../reducer/product/reducerProductsByObject");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialStore = {
  event: _reducerEvent.initialEvent,
  eventImmediate: _reducerEventImmediate.initialEventImmediate,
  eventGift: _reducerEventGift.initialEventGift,
  modalS1: _reducerModal.initialModalS1,
  modalS2: _reducerModal.initialModalS2,
  treeProduct: _reducerTree.initialTreeProduct,
  treeProductEventGiftGroup: _reducerTree.initialTreeProductEventGiftGroup,
  treeProductEventGiftGroup2: _reducerTree.initialTreeProductEventGiftGroup2,
  dataDetail: {},
  treeProductIImmediate: _reducerTree.initialTreeProductImmediate,
  treeProductPresentIImmediate: _reducerTree.initialTreeProductPresentImmediate,
  dataProduct: [],
  dataProductPresent: [],
  product: _reducerVersionProduct.initialProduct,
  productsByObject: _reducerProductsByObject.initialProductsByObject
};
var initialProductDefaults = {
  product: _reducerVersionProduct.initialProduct
};
var initialStoreDeafuat = {
  event: _reducerEvent.initialEvent,
  modalS1: _reducerModal.initialModalS1,
  modalS2: _reducerModal.initialModalS2,
  treeProduct: _reducerTree.initialTreeProduct,
  dataProduct: []
};
var initialStoreDeafuatIMMEDIATE = {
  eventImmediate: _reducerEventImmediate.initialEventImmediate,
  modalS1: _reducerModal.initialModalS1,
  modalS2: _reducerModal.initialModalS2,
  treeProductIImmediate: _reducerTree.initialTreeProductImmediate,
  treeProductPresentIImmediate: _reducerTree.initialTreeProductPresentImmediate,
  dataProduct: [],
  dataProductPresent: []
};
var initialStoreEventGift = {
  eventGift: _reducerEventGift.initialEventGift,
  modalS1: _reducerModal.initialModalS1,
  treeProduct: _reducerTree.initialTreeProduct,
  treeProductEventGiftGroup: _reducerTree.initialTreeProductEventGiftGroup,
  treeProductEventGiftGroup2: _reducerTree.initialTreeProductEventGiftGroup2,
  dataProduct: []
};
var initialStoreProductsByObject = {
  productsByObject: _reducerProductsByObject.initialProductsByObject,
  modalS1: _reducerModal.initialModalS1,
  treeProduct: _reducerTree.initialTreeProduct
}; // const _fetchAPIDetailEvent = async (id) => {
//   try {
//     const data = await APIService._getDetailEvent(id);
//     data.event_response.list_cate_type.map(
//       (item) => (item.children = item.lst_product)
//     );
//   } catch (error) {
//     console.log(error);
//   }
// };

var _fetchAPIDetailEvent = (0, _toolkit.createAsyncThunk)("event/detail", function _callee(eventID, thunkAPI) {
  var data;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap(_services.APIService._getDetailEvent(eventID));

        case 3:
          data = _context.sent;
          return _context.abrupt("return", data.event_response);

        case 7:
          _context.prev = 7;
          _context.t0 = _context["catch"](0);
          console.log(_context.t0);

        case 10:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 7]]);
});

var rootReducers = (0, _toolkit.createSlice)({
  name: "store",
  initialState: initialStore,
  reducers: {
    CREATE_DATA_PRODUCT: function CREATE_DATA_PRODUCT(state, action) {
      state[action.payload.key] = action.payload.value;
      return state;
    },
    UPDATE_DATA_PRODUCT: function UPDATE_DATA_PRODUCT(state, action) {
      state[action.payload.keyRoot][action.payload.keyLocationRoot][action.payload.keyObject][action.payload.idx][action.payload.keyValue] = action.payload.value;
      return state;
    },
    CREATE_DATE_EVENT: function CREATE_DATE_EVENT(state, action) {
      return _reducerEvent.reducerEvent.CREATE_DATE_EVENT(state, action);
    },
    CREATE_DATE_EVENT_FILTER: function CREATE_DATE_EVENT_FILTER(state, action) {
      return _reducerEvent.reducerEvent.CREATE_DATE_EVENT_FILTER(state, action);
    },
    ADD_DESGIN_EVENT: function ADD_DESGIN_EVENT(state, action) {
      return _reducerEvent.reducerEvent.ADD_DESGIN_EVENT(state, action);
    },
    REMOVE_DESGIN_EVENT: function REMOVE_DESGIN_EVENT(state, action) {
      return _reducerEvent.reducerEvent.REMOVE_DESGIN_EVENT(state, action);
    },
    UPDATE_DESGIN_EVENT: function UPDATE_DESGIN_EVENT(state, action) {
      return _reducerEvent.reducerEvent.UPDATE_DESGIN_EVENT(state, action);
    },
    UPDATE_DESGIN_EVENT_DISCOUNT: function UPDATE_DESGIN_EVENT_DISCOUNT(state, action) {
      return _reducerEvent.reducerEvent.UPDATE_DESGIN_EVENT_DISCOUNT(state, action);
    },
    ADD_MODAL: function ADD_MODAL(state, action) {
      return _reducerModal.reducerModal.ADD_MODAL(state, action);
    },
    REMOVE_MODAL: function REMOVE_MODAL(state, action) {
      return _reducerModal.reducerModal.REMOVE_MODAL(state, action);
    },
    RESET_MODAL: function RESET_MODAL(state, action) {
      return _reducerModal.reducerModal.RESET_MODAL(state, action);
    },
    UPDATE_KEY_REWARD: function UPDATE_KEY_REWARD(state, action) {
      return _reducerEvent.reducerEvent.UPDATE_KEY_REWARD(state, action);
    },
    UPDATE_ACTIVELIST_MODAL: function UPDATE_ACTIVELIST_MODAL(state, action) {
      return _reducerModal.reducerModal.UPDATE_ACTIVELIST_MODAL(state, action);
    },
    SAVE_MODAL: function SAVE_MODAL(state, action) {
      return _reducerModal.reducerModal.SAVE_MODAL(state, action);
    },
    CONFIRM_MODAL: function CONFIRM_MODAL(state, action) {
      return _reducerModal.reducerModal.CONFIRM_MODAL(state, action);
    },
    ARRAY_MODAL: function ARRAY_MODAL(state, action) {
      return _reducerModal.reducerModal.ARRAY_MODAL(state, action);
    },
    REST_MODAL: function REST_MODAL(state, action) {
      return _reducerModal.reducerModal.REST_MODAL(state, action);
    },
    DEAFUAT_MODAL: function DEAFUAT_MODAL(state, action) {
      return _reducerModal.reducerModal.DEAFUAT_MODAL(state, action);
    },
    CANCEL_MODAL: function CANCEL_MODAL(state, action) {
      return _reducerModal.reducerModal.CANCEL_MODAL(state, action);
    },
    UPDATE_ACTIVELISTAGENCY_MODAL: function UPDATE_ACTIVELISTAGENCY_MODAL(state, action) {
      return _reducerModal.reducerModal.UPDATE_ACTIVELISTAGENCY_MODAL(state, action);
    },
    ADD_TREE: function ADD_TREE(state, action) {
      return _reducerTree.reducerTreeProduct.ADD_TREE(state, action);
    },
    SHOW_TREE: function SHOW_TREE(state, action) {
      return _reducerTree.reducerTreeProduct.SHOW_TREE(state, action);
    },
    SAVE_TREE: function SAVE_TREE(state, action) {
      return _reducerTree.reducerTreeProduct.SAVE_TREE(state, action);
    },
    UPDATE_TREE: function UPDATE_TREE(state, action) {
      return _reducerTree.reducerTreeProduct.UPDATE_TREE(state, action);
    },
    REMOVE_TREE: function REMOVE_TREE(state, action) {
      return _reducerTree.reducerTreeProduct.REMOVE_TREE(state, action);
    },
    UPDATE_TREE_KEYDEFAULT: function UPDATE_TREE_KEYDEFAULT(state, action) {
      return _reducerTree.reducerTreeProduct.UPDATE_TREE_KEYDEFAULT(state, action);
    },
    ASSIGNED_KEY: function ASSIGNED_KEY(state, action) {
      return _reducerTree.reducerTreeProduct.ASSIGNED_KEY(state, action);
    },
    CANCEL_TREE: function CANCEL_TREE(state, action) {
      return _reducerTree.reducerTreeProduct.CANCEL_TREE(state, action);
    },
    CLEAR_REDUX: function CLEAR_REDUX(state, action) {
      state = _objectSpread({}, state, {}, initialStoreDeafuat);
      return state;
    },
    CLEAR_REDUX_IMMEDIATE: function CLEAR_REDUX_IMMEDIATE(state, action) {
      state = _objectSpread({}, state, {}, initialStoreDeafuatIMMEDIATE);
      return state;
    },
    UPDATE_KEY_CATE: function UPDATE_KEY_CATE(state, action) {
      return _reducerEvent.reducerEvent.UPDATE_KEY_CATE(state, action);
    },
    ASSIGNED_DATA_REDUX_DETAIL: function ASSIGNED_DATA_REDUX_DETAIL(state, action) {
      state.eventGift.recommended_time = action.payload.suggest_date;
      state.eventGift.type_way = action.payload.type_join === 1 ? 2 : action.payload.type_join === 3 ? 1 : 3;
      state.eventGift.type_event_status = action.payload.status;
      state.eventGift.event_design.key_type_product = action.payload.group_id;
      state.eventGift.conditions_apply = action.payload.type_condition_apply;
      var arrCity = action.payload.lst_city.map(function (item) {
        return item.id;
      });
      action.payload.lst_city.map(function (item) {
        item.parent = item.parent_id;
        item.key = item.id;
      });
      var arrayRegion = action.payload.lst_region.map(function (item) {
        return item.id;
      });
      var arrayMembership = action.payload.lst_membership.map(function (item) {
        return item.id;
      });
      state.eventGift.event_filter = {
        arrayRegion: arrayRegion,
        arrayCity: arrCity,
        arrayCityClone: action.payload.lst_city,
        id_agency: action.payload.level_id,
        membership: arrayMembership
      };

      if (action.payload.type_condition_apply === 2) {
        (function () {
          var reward = [];
          var count = 0;
          action.payload.list_condition_product.forEach(function (item) {
            if (item.han_muc > count) {
              count = item.han_muc;
            }
          });

          var _loop = function _loop(i) {
            reward.push([]);
            action.payload.list_condition_product.forEach(function (item) {
              if (item.han_muc === i) {
                reward[i - 1].push({
                  number_to: item.condition_min,
                  discount: item.discount,
                  valid_number_to: false,
                  valid_discount: false
                });
              }
            });
          };

          for (var i = 1; i <= count; i++) {
            _loop(i);
          }

          state.eventGift.event_design.reward = reward;
        })();
      } else {
        (function () {
          var rewardQuantity = [];
          var count = 0;
          action.payload.list_condition_product.forEach(function (item) {
            if (item.han_muc > count) {
              count = item.han_muc;
            }
          });

          var _loop2 = function _loop2(i) {
            rewardQuantity.push([]);
            action.payload.list_condition_product.forEach(function (item) {
              if (item.han_muc === i) {
                rewardQuantity[i - 1].push({
                  number_to: item.condition_min,
                  discount: item.discount,
                  conversion_rate: item.quy_doi_ty_le,
                  valid_number_to: false,
                  valid_discount: false
                });
              }
            });
          };

          for (var i = 1; i <= count; i++) {
            _loop2(i);
          }

          state.eventGift.event_design.rewardQuantity = rewardQuantity;
        })();
      }

      var listArrListTreeConfirmClone = [];
      var listArrDisabled = [];
      action.payload.list_cate_type.forEach(function (item) {
        var treeProduct = item.event_group_product;
        listArrListTreeConfirmClone.push({
          treeProduct: treeProduct
        });
        item.event_group_product.forEach(function (itemLv1) {
          itemLv1.children.forEach(function (itemLv2) {
            itemLv2.children.forEach(function (itemLv3) {
              listArrDisabled.push(itemLv3.tree_id);
            });
          });
        });
      });

      if (action.payload.group_id === 1) {
        state.treeProductEventGiftGroup.listArrListTreeConfirm = listArrListTreeConfirmClone;
      } else {
        state.treeProductEventGiftGroup2.listArrListTreeConfirm = listArrListTreeConfirmClone;
        state.treeProductEventGiftGroup2.listArrDisabled = listArrDisabled;
        state.treeProductEventGiftGroup2.listKeyCloneDisabled = listArrDisabled;
      }

      state.eventGift.content_event = action.payload.description;
      state.eventGift.code_event = action.payload.code;
      state.eventGift.img_event = action.payload.image;
      state.eventGift.name_event = action.payload.name;
      state.eventGift.IsEdit = true;
      state.eventGift.URl_img = action.payload.image_url;
      var application = action.payload.list_user_join.map(function (item) {
        var shop_name = item.user_agency_name;
        var dms_code = item.user_agency_code;
        var level = item.level;
        var address = item.address;
        var id = item.id;
        var agency_name = item.agency_name;
        return {
          shop_name: shop_name,
          dms_code: dms_code,
          level: level,
          address: address,
          agency_name: agency_name,
          id: id
        };
      });
      state.modalS1.listS1Show = action.payload.type_join === 1 ? application : [];
      state.modalS1.listS1Add = action.payload.type_join === 1 ? application : [];
      state.modalS1.listS1AddClone = action.payload.type_join === 1 ? application : [];
      return state;
    },
    CHECK_VALIDATE: function CHECK_VALIDATE(state, action) {
      return _reducerEvent.reducerEvent.CHECK_VALIDATE(state, action);
    },
    UPDATE_TREE_PRIORITY: function UPDATE_TREE_PRIORITY(state, action) {
      return _reducerTree.reducerTreeProduct.UPDATE_TREE_PRIORITY(state, action);
    },
    // EventImmediate
    CREATE_DATE_EVENT_IMMEDIATE: function CREATE_DATE_EVENT_IMMEDIATE(state, action) {
      return _reducerEventImmediate.reducerEventImmediate.CREATE_DATE_EVENT_IMMEDIATE(state, action);
    },
    CREATE_DATE_EVENT_FILTER_IMMEDIATE: function CREATE_DATE_EVENT_FILTER_IMMEDIATE(state, action) {
      return _reducerEventImmediate.reducerEventImmediate.CREATE_DATE_EVENT_FILTER_IMMEDIATE(state, action);
    },
    CREATE_KEY_DEGSIN_IMMEDIATE: function CREATE_KEY_DEGSIN_IMMEDIATE(state, action) {
      return _reducerEventImmediate.reducerEventImmediate.CREATE_KEY_DEGSIN_IMMEDIATE(state, action);
    },
    // PRODUCT
    CREATE_PRODUCT_INFO: function CREATE_PRODUCT_INFO(state, action) {
      return _reducerVersionProduct.reducerProduct.CREATE_PRODUCT_INFO(state, action);
    },
    CLEAR_REDUX_PRODUCT: function CLEAR_REDUX_PRODUCT(state, action) {
      state = _objectSpread({}, state, {}, initialProductDefaults);
      return state;
    },
    // CLEAR_REDUX_PRODUCT_VERSION: (state, action) => {
    //   state = { ...state, ...initialProductDefaults };
    //   return state;
    // },
    CREATE_PRODUCT_VERSION: function CREATE_PRODUCT_VERSION(state, action) {
      _reducerVersionProduct.reducerProduct.CREATE_PRODUCT_VERSION(state, action);
    },
    CHECK_PRODUCT_DEFAULT: function CHECK_PRODUCT_DEFAULT(state, action) {
      _reducerVersionProduct.reducerProduct.CHECK_PRODUCT_DEFAULT(state, action);
    },
    CREATE_PRODUCT_VERSION_INFO: function CREATE_PRODUCT_VERSION_INFO(state, action) {
      _reducerVersionProduct.reducerProduct.CREATE_PRODUCT_VERSION_INFO(state, action);
    },
    ADD_SPECIFICATION_INFO: function ADD_SPECIFICATION_INFO(state, action) {
      _reducerVersionProduct.reducerProduct.ADD_SPECIFICATION_INFO(state, action);
    },
    REMOVE_SPECIFICATION_INFO: function REMOVE_SPECIFICATION_INFO(state, action) {
      _reducerVersionProduct.reducerProduct.REMOVE_SPECIFICATION_INFO(state, action);
    },
    CREATE_PRODUCT_SPECIFICATION_INFO: function CREATE_PRODUCT_SPECIFICATION_INFO(state, action) {
      _reducerVersionProduct.reducerProduct.CREATE_PRODUCT_SPECIFICATION_INFO(state, action);
    },
    CLEAR_REDUX_PRODUCT_VERSION: function CLEAR_REDUX_PRODUCT_VERSION(state, action) {
      _reducerVersionProduct.reducerProduct.CLEAR_REDUX_PRODUCT_VERSION(state, action);
    },
    ASSIGNED_DATA_PRODUCT_REDUX_DETAIL: function ASSIGNED_DATA_PRODUCT_REDUX_DETAIL(state, action) {
      state.product.productCode = !action.payload.productCode ? "" : action.payload.productCode;
      state.product.productName = !action.payload.name ? "" : action.payload.name;
      state.product.productNameOther = !action.payload.shortName ? "" : action.payload.shortName;
      state.product.different_name = !action.payload.different_name ? "" : action.payload.different_name;
      state.product.productType = action.payload.groupId;
      state.product.cate = action.payload.typeParentId;
      state.product.brand = action.payload.typeId;
      state.product.newBrand = action.payload.categoryId;
      state.product.outStandingBrand = action.payload.brandId;
      var arrVersion = action.payload.listVersion.map(function (itemVersion) {
        var id = itemVersion.id;
        var active = itemVersion.isDefault;
        var colorID = itemVersion.colorId;
        var colorName = !itemVersion.colorName ? "" : itemVersion.colorName;
        var attributeValue = !itemVersion.valueUnit ? "" : itemVersion.valueUnit;
        var attribute = itemVersion.unitId;
        var attributeName = itemVersion.unitName;
        var productVersionCode = !itemVersion.versionCode ? "" : itemVersion.versionCode;
        var showProductVersion = itemVersion.status;
        var characteristics = itemVersion.characteristics;
        var minUnitValue = itemVersion.attribute_min;
        var price = itemVersion.price_min;
        var urlImages = action.payload.image_url;
        var arrayImages = !itemVersion.image ? [] : [itemVersion.image];
        var min_purchase = !itemVersion.quantity_buy_min ? 0 : itemVersion.quantity_buy_min;
        return {
          id: id,
          active: active,
          colorID: colorID,
          colorName: colorName,
          attributeName: attributeName,
          attributeValue: attributeValue,
          attribute: attribute,
          productVersionCode: productVersionCode,
          showProductVersion: showProductVersion,
          characteristics: characteristics,
          minUnitValue: minUnitValue,
          price: price,
          min_purchase: min_purchase,
          urlImages: urlImages,
          arrayImages: arrayImages
        };
      });
      state.product.arrProductVersion = arrVersion;
      return state;
    },
    ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL: function ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL(state, action) {
      var specificationsInfo = action.payload.value.listSpecification.map(function (itemSpec) {
        var id = itemSpec.id;
        var name = !itemSpec.name ? "" : itemSpec.name;
        var description = !itemSpec.content ? "" : itemSpec.content;
        return {
          id: id,
          name: name,
          description: description
        };
      });
      var obj = {
        productVersionQuantityTime: action.payload.value.warrantyPeriodId,
        productVersionDescription: !action.payload.value.description ? "" : action.payload.value.description,
        productVersionAccessories: !action.payload.value.element ? "" : action.payload.value.element,
        maxUnit: !action.payload.value.attributeIdMax ? "" : action.payload.value.attributeIdMax,
        percent: !action.payload.value.attributeRatio ? "" : action.payload.value.attributeRatio,
        arrayImages: !action.payload.value.image ? [] : action.payload.value.image,
        specificationsInfo: specificationsInfo,
        minUnit: action.payload.value.attributeIdMin,
        min_purchase: action.payload.value.quantity_buy_min
      };

      var newObj = _objectSpread({}, state.product.arrProductVersion[action.payload.idx], {}, obj);

      state.product.arrProductVersion[action.payload.idx] = newObj;
      return state;
    },
    SORT_LIST_VERSION: function SORT_LIST_VERSION(state, action) {
      _reducerVersionProduct.reducerProduct.SORT_LIST_VERSION(state, action);
    },
    CHECK_NULL_VALUE_LIST_SPECIFICATION: function CHECK_NULL_VALUE_LIST_SPECIFICATION(state, action) {
      _reducerVersionProduct.reducerProduct.CHECK_NULL_VALUE_LIST_SPECIFICATION(state, action);
    },
    ADD_ITEM_SPEC_INFO: function ADD_ITEM_SPEC_INFO(state, action) {
      _reducerVersionProduct.reducerProduct.ADD_ITEM_SPEC_INFO(state, action);
    },
    ASSIGN_MIN_MAX_UNIT: function ASSIGN_MIN_MAX_UNIT(state, action) {
      _reducerVersionProduct.reducerProduct.ASSIGN_MIN_MAX_UNIT(state, action);
    },
    CREATE_INFO_EVENT_GIFT: function CREATE_INFO_EVENT_GIFT(state, action) {
      _reducerEventGift.reducerEventGift.CREATE_INFO_EVENT_GIFT(state, action);
    },
    CREATE_EVENT_GIFT_FILTER: function CREATE_EVENT_GIFT_FILTER(state, action) {
      _reducerEventGift.reducerEventGift.CREATE_EVENT_GIFT_FILTER(state, action);
    },
    CLEAR_REDUX_EVENT_GIFT: function CLEAR_REDUX_EVENT_GIFT(state, action) {
      state = _objectSpread({}, state, {}, initialStoreEventGift);
      return state;
    },
    ADD_EVENT_GIFT_LEVEL: function ADD_EVENT_GIFT_LEVEL(state, action) {
      _reducerEventGift.reducerEventGift.ADD_EVENT_GIFT_LEVEL(state, action);

      return state;
    },
    REMOVE_EVENT_GIFT_LEVEL: function REMOVE_EVENT_GIFT_LEVEL(state, action) {
      _reducerEventGift.reducerEventGift.REMOVE_EVENT_GIFT_LEVEL(state, action);

      return state;
    },
    UPDATE_KEY_CATE_EVENT_GIFT: function UPDATE_KEY_CATE_EVENT_GIFT(state, action) {
      _reducerEventGift.reducerEventGift.UPDATE_KEY_CATE_EVENT_GIFT(state, action);

      return state;
    },
    UPDATE_DESIGN_EVENT_GIFT: function UPDATE_DESIGN_EVENT_GIFT(state, action) {
      _reducerEventGift.reducerEventGift.UPDATE_DESIGN_EVENT_GIFT(state, action);

      return state;
    },
    CHECK_VALIDATE_EVENT_GIFT: function CHECK_VALIDATE_EVENT_GIFT(state, action) {
      _reducerEventGift.reducerEventGift.CHECK_VALIDATE_EVENT_GIFT(state, action);

      return state;
    },
    SAVE_TREE_NEW: function SAVE_TREE_NEW(state, action) {
      _reducerTree.reducerTreeProduct.SAVE_TREE_NEW(state, action);

      return state;
    },
    CANCEL_TREE_NEW: function CANCEL_TREE_NEW(state, action) {
      _reducerTree.reducerTreeProduct.CANCEL_TREE_NEW(state, action);

      return state;
    },
    REMOVE_PRODUCT: function REMOVE_PRODUCT(state, action) {
      _reducerTree.reducerTreeProduct.REMOVE_PRODUCT(state, action);

      return state;
    },
    REMOVE_PRODUCT_CONFIRM: function REMOVE_PRODUCT_CONFIRM(state, action) {
      _reducerTree.reducerTreeProduct.REMOVE_PRODUCT_CONFIRM(state, action);

      return state;
    },
    ASSIGN_LIST_KEY_CLONE: function ASSIGN_LIST_KEY_CLONE(state, action) {
      _reducerTree.reducerTreeProduct.ASSIGN_LIST_KEY_CLONE(state, action);

      return state;
    },
    ADD_TREE_KEY_CLONE: function ADD_TREE_KEY_CLONE(state, action) {
      _reducerTree.reducerTreeProduct.ADD_TREE_KEY_CLONE(state, action);

      return state;
    },
    UPDATE_ID_COUNT_SAVE_ITEM: function UPDATE_ID_COUNT_SAVE_ITEM(state, action) {
      _reducerEventGift.reducerEventGift.UPDATE_ID_COUNT_SAVE_ITEM(state, action);

      return state;
    },
    ADD_PRODUCT_GROUP_LEVEL: function ADD_PRODUCT_GROUP_LEVEL(state, action) {
      _reducerEventGift.reducerEventGift.ADD_PRODUCT_GROUP_LEVEL(state, action);

      return state;
    },
    SAVE_TREE_GROUP: function SAVE_TREE_GROUP(state, action) {
      _reducerTree.reducerTreeProduct.SAVE_TREE_GROUP(state, action);

      return state;
    },
    SAVE_TREE_GROUP_OVER: function SAVE_TREE_GROUP_OVER(state, action) {
      _reducerTree.reducerTreeProduct.SAVE_TREE_GROUP_OVER(state, action);

      return state;
    },
    CLOSE_MODAL_TREE: function CLOSE_MODAL_TREE(state, action) {
      _reducerTree.reducerTreeProduct.CLOSE_MODAL_TREE(state, action);

      return state;
    },
    ASSIGNED_TREE_CLICK_GROUP: function ASSIGNED_TREE_CLICK_GROUP(state, action) {
      _reducerTree.reducerTreeProduct.ASSIGNED_TREE_CLICK_GROUP(state, action);

      return state;
    },
    ADD_DESIGN_EVENT_LEVEL_GROUP: function ADD_DESIGN_EVENT_LEVEL_GROUP(state, action) {
      _reducerEventGift.reducerEventGift.ADD_DESIGN_EVENT_LEVEL_GROUP(state, action);

      return state;
    },
    REMOVE_PRODUCT_GROUP: function REMOVE_PRODUCT_GROUP(state, action) {
      _reducerTree.reducerTreeProduct.REMOVE_PRODUCT_GROUP(state, action);

      return state;
    },
    REMOVE_LEVEL: function REMOVE_LEVEL(state, action) {
      _reducerEventGift.reducerEventGift.REMOVE_LEVEL(state, action);

      return state;
    },
    CHANGE_CONDITION_APPLY: function CHANGE_CONDITION_APPLY(state, action) {
      _reducerEventGift.reducerEventGift.CHANGE_CONDITION_APPLY(state, action);

      return state;
    },
    CLEAR_REDUX_PRODUCTS_BY_OBJECT: function CLEAR_REDUX_PRODUCTS_BY_OBJECT(state, action) {
      state = _objectSpread({}, state, {}, initialStoreProductsByObject);
      return state;
    },
    CREATE_PRODUCT_BY_OBJECT_INFO: function CREATE_PRODUCT_BY_OBJECT_INFO(state, action) {
      _reducerProductsByObject.reducerProductsByObject.CREATE_PRODUCT_BY_OBJECT_INFO(state, action);

      return state;
    },
    CHANGE_PRICE_PRODUCT_BY_OBJECT: function CHANGE_PRICE_PRODUCT_BY_OBJECT(state, action) {
      _reducerProductsByObject.reducerProductsByObject.CHANGE_PRICE_PRODUCT_BY_OBJECT(state, action);

      return state;
    },
    CHECK_VALIDATE_PRICE_BY_OBJECT: function CHECK_VALIDATE_PRICE_BY_OBJECT(state, action) {
      _reducerProductsByObject.reducerProductsByObject.CHECK_VALIDATE_PRICE_BY_OBJECT(state, action);

      return state;
    },
    REMOVE_PRODUCT_BY_OBJECT_CONFIRM: function REMOVE_PRODUCT_BY_OBJECT_CONFIRM(state, action) {
      _reducerTree.reducerTreeProduct.REMOVE_PRODUCT_BY_OBJECT_CONFIRM(state, action);

      return state;
    },
    REMOVE_PRODUCT_BY_OBJECT: function REMOVE_PRODUCT_BY_OBJECT(state, action) {
      _reducerTree.reducerTreeProduct.REMOVE_PRODUCT_BY_OBJECT(state, action);

      return state;
    }
  },
  extraReducers: _defineProperty({}, _fetchAPIDetailEvent.fulfilled, function (state, action) {
    state.dataDetail = action.payload;
  })
});
var reducer = rootReducers.reducer,
    actions = rootReducers.actions;
var REMOVE_PRODUCT_BY_OBJECT = actions.REMOVE_PRODUCT_BY_OBJECT,
    REMOVE_PRODUCT_BY_OBJECT_CONFIRM = actions.REMOVE_PRODUCT_BY_OBJECT_CONFIRM,
    CHECK_VALIDATE_PRICE_BY_OBJECT = actions.CHECK_VALIDATE_PRICE_BY_OBJECT,
    CHANGE_PRICE_PRODUCT_BY_OBJECT = actions.CHANGE_PRICE_PRODUCT_BY_OBJECT,
    CREATE_PRODUCT_BY_OBJECT_INFO = actions.CREATE_PRODUCT_BY_OBJECT_INFO,
    CLEAR_REDUX_PRODUCTS_BY_OBJECT = actions.CLEAR_REDUX_PRODUCTS_BY_OBJECT,
    CHANGE_CONDITION_APPLY = actions.CHANGE_CONDITION_APPLY,
    REMOVE_LEVEL = actions.REMOVE_LEVEL,
    REMOVE_PRODUCT_GROUP = actions.REMOVE_PRODUCT_GROUP,
    ADD_DESIGN_EVENT_LEVEL_GROUP = actions.ADD_DESIGN_EVENT_LEVEL_GROUP,
    ASSIGNED_TREE_CLICK_GROUP = actions.ASSIGNED_TREE_CLICK_GROUP,
    CLOSE_MODAL_TREE = actions.CLOSE_MODAL_TREE,
    SAVE_TREE_GROUP_OVER = actions.SAVE_TREE_GROUP_OVER,
    SAVE_TREE_GROUP = actions.SAVE_TREE_GROUP,
    ADD_PRODUCT_GROUP_LEVEL = actions.ADD_PRODUCT_GROUP_LEVEL,
    UPDATE_ID_COUNT_SAVE_ITEM = actions.UPDATE_ID_COUNT_SAVE_ITEM,
    ADD_TREE_KEY_CLONE = actions.ADD_TREE_KEY_CLONE,
    ASSIGN_LIST_KEY_CLONE = actions.ASSIGN_LIST_KEY_CLONE,
    REMOVE_PRODUCT_CONFIRM = actions.REMOVE_PRODUCT_CONFIRM,
    REMOVE_PRODUCT = actions.REMOVE_PRODUCT,
    CANCEL_TREE_NEW = actions.CANCEL_TREE_NEW,
    SAVE_TREE_NEW = actions.SAVE_TREE_NEW,
    CHECK_VALIDATE_EVENT_GIFT = actions.CHECK_VALIDATE_EVENT_GIFT,
    UPDATE_DESIGN_EVENT_GIFT = actions.UPDATE_DESIGN_EVENT_GIFT,
    UPDATE_KEY_CATE_EVENT_GIFT = actions.UPDATE_KEY_CATE_EVENT_GIFT,
    REMOVE_EVENT_GIFT_LEVEL = actions.REMOVE_EVENT_GIFT_LEVEL,
    ADD_EVENT_GIFT_LEVEL = actions.ADD_EVENT_GIFT_LEVEL,
    CLEAR_REDUX_EVENT_GIFT = actions.CLEAR_REDUX_EVENT_GIFT,
    CREATE_INFO_EVENT_GIFT = actions.CREATE_INFO_EVENT_GIFT,
    CREATE_EVENT_GIFT_FILTER = actions.CREATE_EVENT_GIFT_FILTER,
    ASSIGN_MIN_MAX_UNIT = actions.ASSIGN_MIN_MAX_UNIT,
    ADD_ITEM_SPEC_INFO = actions.ADD_ITEM_SPEC_INFO,
    CHECK_NULL_VALUE_LIST_SPECIFICATION = actions.CHECK_NULL_VALUE_LIST_SPECIFICATION,
    SORT_LIST_VERSION = actions.SORT_LIST_VERSION,
    ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL = actions.ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL,
    ASSIGNED_DATA_PRODUCT_REDUX_DETAIL = actions.ASSIGNED_DATA_PRODUCT_REDUX_DETAIL,
    CLEAR_REDUX_PRODUCT_VERSION = actions.CLEAR_REDUX_PRODUCT_VERSION,
    CREATE_PRODUCT_SPECIFICATION_INFO = actions.CREATE_PRODUCT_SPECIFICATION_INFO,
    REMOVE_SPECIFICATION_INFO = actions.REMOVE_SPECIFICATION_INFO,
    ADD_SPECIFICATION_INFO = actions.ADD_SPECIFICATION_INFO,
    CREATE_PRODUCT_VERSION_INFO = actions.CREATE_PRODUCT_VERSION_INFO,
    CHECK_PRODUCT_DEFAULT = actions.CHECK_PRODUCT_DEFAULT,
    CREATE_PRODUCT_INFO = actions.CREATE_PRODUCT_INFO,
    CLEAR_REDUX_PRODUCT = actions.CLEAR_REDUX_PRODUCT,
    CREATE_PRODUCT_VERSION = actions.CREATE_PRODUCT_VERSION,
    CREATE_DATE_EVENT = actions.CREATE_DATE_EVENT,
    CREATE_DATE_EVENT_FILTER = actions.CREATE_DATE_EVENT_FILTER,
    ADD_DESGIN_EVENT = actions.ADD_DESGIN_EVENT,
    REMOVE_DESGIN_EVENT = actions.REMOVE_DESGIN_EVENT,
    UPDATE_DESGIN_EVENT = actions.UPDATE_DESGIN_EVENT,
    ADD_MODAL = actions.ADD_MODAL,
    REMOVE_MODAL = actions.REMOVE_MODAL,
    UPDATE_ACTIVELIST_MODAL = actions.UPDATE_ACTIVELIST_MODAL,
    SAVE_MODAL = actions.SAVE_MODAL,
    CONFIRM_MODAL = actions.CONFIRM_MODAL,
    ARRAY_MODAL = actions.ARRAY_MODAL,
    REST_MODAL = actions.REST_MODAL,
    DEAFUAT_MODAL = actions.DEAFUAT_MODAL,
    CANCEL_MODAL = actions.CANCEL_MODAL,
    UPDATE_ACTIVELISTAGENCY_MODAL = actions.UPDATE_ACTIVELISTAGENCY_MODAL,
    ADD_TREE = actions.ADD_TREE,
    SHOW_TREE = actions.SHOW_TREE,
    SAVE_TREE = actions.SAVE_TREE,
    CLEAR_REDUX = actions.CLEAR_REDUX,
    CLEAR_REDUX_IMMEDIATE = actions.CLEAR_REDUX_IMMEDIATE,
    UPDATE_TREE = actions.UPDATE_TREE,
    ASSIGNED_DATA_REDUX_DETAIL = actions.ASSIGNED_DATA_REDUX_DETAIL,
    CANCEL_TREE = actions.CANCEL_TREE,
    UPDATE_DESGIN_EVENT_DISCOUNT = actions.UPDATE_DESGIN_EVENT_DISCOUNT,
    UPDATE_KEY_CATE = actions.UPDATE_KEY_CATE,
    RESET_MODAL = actions.RESET_MODAL,
    CHECK_VALIDATE = actions.CHECK_VALIDATE,
    UPDATE_TREE_PRIORITY = actions.UPDATE_TREE_PRIORITY,
    CREATE_DATE_EVENT_IMMEDIATE = actions.CREATE_DATE_EVENT_IMMEDIATE,
    CREATE_DATE_EVENT_FILTER_IMMEDIATE = actions.CREATE_DATE_EVENT_FILTER_IMMEDIATE,
    CREATE_KEY_DEGSIN_IMMEDIATE = actions.CREATE_KEY_DEGSIN_IMMEDIATE,
    CREATE_DATA_PRODUCT = actions.CREATE_DATA_PRODUCT,
    UPDATE_DATA_PRODUCT = actions.UPDATE_DATA_PRODUCT,
    REMOVE_TREE = actions.REMOVE_TREE,
    ASSIGNED_KEY = actions.ASSIGNED_KEY,
    UPDATE_TREE_KEYDEFAULT = actions.UPDATE_TREE_KEYDEFAULT,
    UPDATE_KEY_REWARD = actions.UPDATE_KEY_REWARD;
exports.UPDATE_KEY_REWARD = UPDATE_KEY_REWARD;
exports.UPDATE_TREE_KEYDEFAULT = UPDATE_TREE_KEYDEFAULT;
exports.ASSIGNED_KEY = ASSIGNED_KEY;
exports.REMOVE_TREE = REMOVE_TREE;
exports.UPDATE_DATA_PRODUCT = UPDATE_DATA_PRODUCT;
exports.CREATE_DATA_PRODUCT = CREATE_DATA_PRODUCT;
exports.CREATE_KEY_DEGSIN_IMMEDIATE = CREATE_KEY_DEGSIN_IMMEDIATE;
exports.CREATE_DATE_EVENT_FILTER_IMMEDIATE = CREATE_DATE_EVENT_FILTER_IMMEDIATE;
exports.CREATE_DATE_EVENT_IMMEDIATE = CREATE_DATE_EVENT_IMMEDIATE;
exports.UPDATE_TREE_PRIORITY = UPDATE_TREE_PRIORITY;
exports.CHECK_VALIDATE = CHECK_VALIDATE;
exports.RESET_MODAL = RESET_MODAL;
exports.UPDATE_KEY_CATE = UPDATE_KEY_CATE;
exports.UPDATE_DESGIN_EVENT_DISCOUNT = UPDATE_DESGIN_EVENT_DISCOUNT;
exports.CANCEL_TREE = CANCEL_TREE;
exports.ASSIGNED_DATA_REDUX_DETAIL = ASSIGNED_DATA_REDUX_DETAIL;
exports.UPDATE_TREE = UPDATE_TREE;
exports.CLEAR_REDUX_IMMEDIATE = CLEAR_REDUX_IMMEDIATE;
exports.CLEAR_REDUX = CLEAR_REDUX;
exports.SAVE_TREE = SAVE_TREE;
exports.SHOW_TREE = SHOW_TREE;
exports.ADD_TREE = ADD_TREE;
exports.UPDATE_ACTIVELISTAGENCY_MODAL = UPDATE_ACTIVELISTAGENCY_MODAL;
exports.CANCEL_MODAL = CANCEL_MODAL;
exports.DEAFUAT_MODAL = DEAFUAT_MODAL;
exports.REST_MODAL = REST_MODAL;
exports.ARRAY_MODAL = ARRAY_MODAL;
exports.CONFIRM_MODAL = CONFIRM_MODAL;
exports.SAVE_MODAL = SAVE_MODAL;
exports.UPDATE_ACTIVELIST_MODAL = UPDATE_ACTIVELIST_MODAL;
exports.REMOVE_MODAL = REMOVE_MODAL;
exports.ADD_MODAL = ADD_MODAL;
exports.UPDATE_DESGIN_EVENT = UPDATE_DESGIN_EVENT;
exports.REMOVE_DESGIN_EVENT = REMOVE_DESGIN_EVENT;
exports.ADD_DESGIN_EVENT = ADD_DESGIN_EVENT;
exports.CREATE_DATE_EVENT_FILTER = CREATE_DATE_EVENT_FILTER;
exports.CREATE_DATE_EVENT = CREATE_DATE_EVENT;
exports.CREATE_PRODUCT_VERSION = CREATE_PRODUCT_VERSION;
exports.CLEAR_REDUX_PRODUCT = CLEAR_REDUX_PRODUCT;
exports.CREATE_PRODUCT_INFO = CREATE_PRODUCT_INFO;
exports.CHECK_PRODUCT_DEFAULT = CHECK_PRODUCT_DEFAULT;
exports.CREATE_PRODUCT_VERSION_INFO = CREATE_PRODUCT_VERSION_INFO;
exports.ADD_SPECIFICATION_INFO = ADD_SPECIFICATION_INFO;
exports.REMOVE_SPECIFICATION_INFO = REMOVE_SPECIFICATION_INFO;
exports.CREATE_PRODUCT_SPECIFICATION_INFO = CREATE_PRODUCT_SPECIFICATION_INFO;
exports.CLEAR_REDUX_PRODUCT_VERSION = CLEAR_REDUX_PRODUCT_VERSION;
exports.ASSIGNED_DATA_PRODUCT_REDUX_DETAIL = ASSIGNED_DATA_PRODUCT_REDUX_DETAIL;
exports.ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL = ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL;
exports.SORT_LIST_VERSION = SORT_LIST_VERSION;
exports.CHECK_NULL_VALUE_LIST_SPECIFICATION = CHECK_NULL_VALUE_LIST_SPECIFICATION;
exports.ADD_ITEM_SPEC_INFO = ADD_ITEM_SPEC_INFO;
exports.ASSIGN_MIN_MAX_UNIT = ASSIGN_MIN_MAX_UNIT;
exports.CREATE_EVENT_GIFT_FILTER = CREATE_EVENT_GIFT_FILTER;
exports.CREATE_INFO_EVENT_GIFT = CREATE_INFO_EVENT_GIFT;
exports.CLEAR_REDUX_EVENT_GIFT = CLEAR_REDUX_EVENT_GIFT;
exports.ADD_EVENT_GIFT_LEVEL = ADD_EVENT_GIFT_LEVEL;
exports.REMOVE_EVENT_GIFT_LEVEL = REMOVE_EVENT_GIFT_LEVEL;
exports.UPDATE_KEY_CATE_EVENT_GIFT = UPDATE_KEY_CATE_EVENT_GIFT;
exports.UPDATE_DESIGN_EVENT_GIFT = UPDATE_DESIGN_EVENT_GIFT;
exports.CHECK_VALIDATE_EVENT_GIFT = CHECK_VALIDATE_EVENT_GIFT;
exports.SAVE_TREE_NEW = SAVE_TREE_NEW;
exports.CANCEL_TREE_NEW = CANCEL_TREE_NEW;
exports.REMOVE_PRODUCT = REMOVE_PRODUCT;
exports.REMOVE_PRODUCT_CONFIRM = REMOVE_PRODUCT_CONFIRM;
exports.ASSIGN_LIST_KEY_CLONE = ASSIGN_LIST_KEY_CLONE;
exports.ADD_TREE_KEY_CLONE = ADD_TREE_KEY_CLONE;
exports.UPDATE_ID_COUNT_SAVE_ITEM = UPDATE_ID_COUNT_SAVE_ITEM;
exports.ADD_PRODUCT_GROUP_LEVEL = ADD_PRODUCT_GROUP_LEVEL;
exports.SAVE_TREE_GROUP = SAVE_TREE_GROUP;
exports.SAVE_TREE_GROUP_OVER = SAVE_TREE_GROUP_OVER;
exports.CLOSE_MODAL_TREE = CLOSE_MODAL_TREE;
exports.ASSIGNED_TREE_CLICK_GROUP = ASSIGNED_TREE_CLICK_GROUP;
exports.ADD_DESIGN_EVENT_LEVEL_GROUP = ADD_DESIGN_EVENT_LEVEL_GROUP;
exports.REMOVE_PRODUCT_GROUP = REMOVE_PRODUCT_GROUP;
exports.REMOVE_LEVEL = REMOVE_LEVEL;
exports.CHANGE_CONDITION_APPLY = CHANGE_CONDITION_APPLY;
exports.CLEAR_REDUX_PRODUCTS_BY_OBJECT = CLEAR_REDUX_PRODUCTS_BY_OBJECT;
exports.CREATE_PRODUCT_BY_OBJECT_INFO = CREATE_PRODUCT_BY_OBJECT_INFO;
exports.CHANGE_PRICE_PRODUCT_BY_OBJECT = CHANGE_PRICE_PRODUCT_BY_OBJECT;
exports.CHECK_VALIDATE_PRICE_BY_OBJECT = CHECK_VALIDATE_PRICE_BY_OBJECT;
exports.REMOVE_PRODUCT_BY_OBJECT_CONFIRM = REMOVE_PRODUCT_BY_OBJECT_CONFIRM;
exports.REMOVE_PRODUCT_BY_OBJECT = REMOVE_PRODUCT_BY_OBJECT;
var _default = reducer;
exports["default"] = _default;