import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { APIService } from "../../services";
import { initialEvent, reducerEvent } from "../reducer/event/reducerEvent";
import {
  initialEventImmediate,
  reducerEventImmediate,
} from "../reducer/event/reducerEventImmediate";

import {
  initialEventImmediateNew,
  reducerEventImmediateNew,
} from "../reducer/event/reducerEventImmediateNew";

import {
  initialEventGift,
  reducerEventGift,
} from "../reducer/event/reducerEventGift";
import {
  initialModalS1,
  reducerModal,
  initialModalS2,
  initialBusiness,
} from "../reducer/modal/reducerModal";
import {
  initialTreeProduct,
  reducerTreeProduct,
  initialTreeProductImmediate,
  initialTreeProductPresentImmediate,
  initialTreeProductEventGiftGroup,
  initialTreeProductEventGiftGroup2,
  initialTreeProductImmediateGiftGroup,
  initialTreeProductImmediateConditionAttach,
  initialTreeProductImmediateConditionAttachGroup,
  initialTreeProductPresentImmediateDiscount,
  initialTreeProductComboGroup,
  initialTreeProductConditionPage,
  initialTreeProductConditionGroup,
} from "../reducer/modal/reducerTree";
import {
  initialCity,
  initialDistrict,
  initialWard,
  reducerModalThunk,
} from "../reducer/modal/reducerModalThunk";

import {
  initialProduct,
  reducerProduct,
} from "../reducer/product/reducerVersionProduct";

import {
  initialProductsByObject,
  reducerProductsByObject,
} from "../reducer/product/reducerProductsByObject";

import {
  Add_locality,
  Array_locality,
  Confirm_locality,
  Save_locality,
  Select_frequency_id,
  Reset_locality,
  Deafuat_listLocality,
  Remove_locality,
  Update_ActiveList,
  Cancel_locality,
  Not_checkAll,
} from "../pages/Route/Service/reducerServiceRoute";

import {
  Add_S2,
  Array_S2,
  Select_frequency_id_sell,
  Save_S2,
  Confirm_S2,
  Reset_S2,
  Deafuat_S2,
  Remove_S2,
  Update_ActiveListSell,
  Cancel_S2,
} from "../pages/Route/Sell/reducersSellSalesman";

import { DefineKeyEvent } from "../../utils/DefineKey";
import {initListTargetKpi,reducerKpiTarget} from '../reducer/modal/reducerKpiManagement'
const initialValueCondition = {
  treeProductCondition: initialTreeProductConditionPage,
  treeProductConditionGroup: initialTreeProductConditionGroup,
  condition_type: DefineKeyEvent.conditon_nguyen_don,
  sum_sales_order: 0,
  condition_order_style: DefineKeyEvent.tung_san_pham,
  eventCombo: {
    event_design: {
      count_save_group: 1,
      id_count_save_item: 0,
      reward_condition: [],
    },
  },
};

const initialDataArrayCondition = [initialValueCondition];

const initialStore = {
  event: initialEvent,
  eventImmediate: initialEventImmediate,
  eventImmediateNew: initialEventImmediateNew,
  eventGift: initialEventGift,
  modalS1: initialModalS1,
  modalS2: initialModalS2,
  treeProduct: initialTreeProduct,
  treeProductEventGiftGroup: initialTreeProductEventGiftGroup,
  treeProductEventGiftGroup2: initialTreeProductEventGiftGroup2,
  dataDetail: {},
  treeProductIImmediate: initialTreeProductImmediate,
  treeProductPresentIImmediate: initialTreeProductPresentImmediate,
  treeProductPresentIImmediateConditionAttach:
    initialTreeProductImmediateConditionAttach,
  treeProductImmediateConditionAttachGroup:
    initialTreeProductImmediateConditionAttachGroup,
  treeProductImmediateGiftGroup: initialTreeProductImmediateGiftGroup,
  treeProductPresentIImmediateDiscount:
    initialTreeProductPresentImmediateDiscount,
  dataProduct: [],
  dataProduct2: [],
  dataProductPresent: [],
  dataProductImmediatePresent: [],
  dataProductImmediateConditionAttach: [],
  product: initialProduct,
  productsByObject: initialProductsByObject,
  listLocalityAddClone: [],
  listLocalityAdd: [],
  listLocalityShow: [],
  listLocality: [],
  listLocalityClone: [],
  listS2AddClone: [],
  listS2Add: [],
  listS2Show: [],
  listS2: [],
  listS2Clone: [],
  listS2Before: [],
  listTargetKpi:initListTargetKpi
};
const initialStoreDefaultImmediateNew = {
  eventImmediateNew: initialEventImmediateNew,
  modalS1: initialModalS1,
  modalS2: initialModalS2,
  modalBusiness: initialBusiness,
  modalCity: initialCity,
  modalDistrict: initialDistrict,
  modalWard: initialWard,
  array_businessRegion: [],
  treeProductIImmediate: initialTreeProductImmediate,
  treeProductPresentIImmediate: initialTreeProductPresentImmediate,
  treeProductImmediateGiftGroup: initialTreeProductImmediateGiftGroup,
  treeProductPresentIImmediateConditionAttach:
    initialTreeProductImmediateConditionAttach,
  treeProductImmediateConditionAttachGroup:
    initialTreeProductImmediateConditionAttachGroup,
  treeProductPresentIImmediateDiscount:
    initialTreeProductPresentImmediateDiscount,
  dataProduct: [],
  dataProductImmediatePresent: [],
  dataProductImmediateConditionAttach: [],
  dataProductPresent: [],
  treeProductComboGroup: initialTreeProductComboGroup,
  treeProductCondition: initialTreeProductConditionPage,
  treeProductConditionGroup: initialTreeProductConditionGroup,
  dataArrayCondition: [...initialDataArrayCondition],
  idxDataArrayCondition: 0,
};

const initialProductDefaults = {
  product: initialProduct,
};

const initialStoreDeafuat = {
  event: initialEvent,
  modalS1: initialModalS1,
  modalS2: initialModalS2,
  treeProduct: initialTreeProduct,
  dataProduct: [],
};

const initialStoreDeafuatIMMEDIATE = {
  eventImmediate: initialEventImmediate,
  modalS1: initialModalS1,
  modalS2: initialModalS2,
  treeProductIImmediate: initialTreeProductImmediate,
  treeProductPresentIImmediate: initialTreeProductPresentImmediate,
  dataProduct: [],
  dataProductPresent: [],
};

const initialStoreEventGift = {
  eventGift: initialEventGift,
  modalS1: initialModalS1,
  treeProduct: initialTreeProduct,
  treeProductEventGiftGroup: initialTreeProductEventGiftGroup,
  treeProductEventGiftGroup2: initialTreeProductEventGiftGroup2,
  dataProduct: [],
  dataProduct2: [],
};

const initialStoreProductsByObject = {
  productsByObject: initialProductsByObject,
  modalS1: initialModalS1,
  treeProduct: initialTreeProduct,
};

const initialStoreDefaultLocality = {
  listLocalityAddClone: [],
  listLocalityAdd: [],
  listLocalityShow: [],
  listLocality: [],
  listLocalityClone: [],
};

// const _fetchAPIDetailEvent = async (id) => {
//   try {
//     const data = await APIService._getDetailEvent(id);
//     data.event_response.list_cate_type.map(
//       (item) => (item.children = item.lst_product)
//     );
//   } catch (error) {
//     console.log(error);
//   }
// };

const _fetchAPIDetailEvent = createAsyncThunk(
  "event/detail",
  async (eventID, thunkAPI) => {
    try {
      const data = await APIService._getDetailEvent(eventID);
      return data.event_response;
    } catch (error) {
      console.log(error);
    }
  }
);

export const _fetchAPIDetailEventParent = createAsyncThunk(
  "event/parent/detail",
  async (id) => {
    try {
      const data = await APIService._getDetailEventComboGroup(id);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
);

export const _fetchAPIDetailEventImmediateParent = createAsyncThunk(
  "event/immediate/parent/detail",
  async (id) => {
    try {
      const data = await APIService._getDetailEventImmediate(id);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
);

export const _fetchAPIListBuusinessRegion = createAsyncThunk(
  "event/region/list",
  async (filter) => {
    try {
      const data = await APIService._getListAgencyByListBussinessRegion(
        filter.key_word,
        filter.list_region
      );
      return data;
    } catch (error) {
      console.log(error);
    }
  }
);

export const _fetchAPIListCity = createAsyncThunk(
  "event/city/list",
  async (key) => {
    try {
      const data = await APIService._getListCities(key);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
);

export const _fetchAPIListDistrict = createAsyncThunk(
  "event/district/list",
  async ({ city_id, search }) => {
    try {
      const data = await APIService._getListCountyAll(city_id, search);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
);

export const _fetchAPIListWard = createAsyncThunk(
  "event/ward/list",
  async ({ district_id, search }) => {
    try {
      const data = await APIService._getListWardAll(district_id, search);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
);

export const _fetchAPIListBusinessRegion = createAsyncThunk(
  "event/businesss/list",
  async (search) => {
    try {
      const data = await APIService._getAllAgencyGetRegion(search);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
);

const rootReducers = createSlice({
  name: "store",
  initialState: initialStore,
  reducers: {
    CREATE_DATA_PRODUCT: (state, action) => {
      state[action.payload.key] = action.payload.value;
      return state;
    },
    UPDATE_DATA_PRODUCT: (state, action) => {
      state[action.payload.keyRoot][action.payload.keyLocationRoot][
        action.payload.keyObject
      ][action.payload.idx][action.payload.keyValue] = action.payload.value;
      return state;
    },
    CREATE_DATE_EVENT: (state, action) =>
      reducerEvent.CREATE_DATE_EVENT(state, action),
    CREATE_DATE_EVENT_FILTER: (state, action) =>
      reducerEvent.CREATE_DATE_EVENT_FILTER(state, action),
    ADD_DESGIN_EVENT: (state, action) =>
      reducerEvent.ADD_DESGIN_EVENT(state, action),
    REMOVE_DESGIN_EVENT: (state, action) =>
      reducerEvent.REMOVE_DESGIN_EVENT(state, action),
    UPDATE_DESGIN_EVENT: (state, action) =>
      reducerEvent.UPDATE_DESGIN_EVENT(state, action),
    UPDATE_DESGIN_EVENT_DISCOUNT: (state, action) =>
      reducerEvent.UPDATE_DESGIN_EVENT_DISCOUNT(state, action),
    ADD_MODAL: (state, action) => reducerModal.ADD_MODAL(state, action),
    REMOVE_MODAL: (state, action) => reducerModal.REMOVE_MODAL(state, action),
    RESET_MODAL: (state, action) => reducerModal.RESET_MODAL(state, action),
    UPDATE_KEY_REWARD: (state, action) =>
      reducerEvent.UPDATE_KEY_REWARD(state, action),

    UPDATE_ACTIVELIST_MODAL: (state, action) =>
      reducerModal.UPDATE_ACTIVELIST_MODAL(state, action),
    SAVE_MODAL: (state, action) => reducerModal.SAVE_MODAL(state, action),
    CONFIRM_MODAL: (state, action) => reducerModal.CONFIRM_MODAL(state, action),
    ARRAY_MODAL: (state, action) => reducerModal.ARRAY_MODAL(state, action),
    REST_MODAL: (state, action) => reducerModal.REST_MODAL(state, action),
    DEAFUAT_MODAL: (state, action) => reducerModal.DEAFUAT_MODAL(state, action),
    CANCEL_MODAL: (state, action) => reducerModal.CANCEL_MODAL(state, action),
    UPDATE_ACTIVELISTAGENCY_MODAL: (state, action) =>
      reducerModal.UPDATE_ACTIVELISTAGENCY_MODAL(state, action),
    ADD_TREE: (state, action) => reducerTreeProduct.ADD_TREE(state, action),
    SHOW_TREE: (state, action) => reducerTreeProduct.SHOW_TREE(state, action),
    SAVE_TREE: (state, action) => reducerTreeProduct.SAVE_TREE(state, action),
    UPDATE_TREE: (state, action) =>
      reducerTreeProduct.UPDATE_TREE(state, action),
    REMOVE_TREE: (state, action) =>
      reducerTreeProduct.REMOVE_TREE(state, action),
    UPDATE_TREE_KEYDEFAULT: (state, action) =>
      reducerTreeProduct.UPDATE_TREE_KEYDEFAULT(state, action),
    ASSIGNED_KEY: (state, action) =>
      reducerTreeProduct.ASSIGNED_KEY(state, action),
    CANCEL_TREE: (state, action) =>
      reducerTreeProduct.CANCEL_TREE(state, action),
    CLEAR_REDUX: (state, action) => {
      state = { ...state, ...initialStoreDeafuat };
      return state;
    },
    CLEAR_REDUX_IMMEDIATE: (state, action) => {
      state = { ...state, ...initialStoreDeafuatIMMEDIATE };
      return state;
    },
    UPDATE_KEY_CATE: (state, action) =>
      reducerEvent.UPDATE_KEY_CATE(state, action),
    ASSIGNED_DATA_REDUX_DETAIL: (state, action) => {
      state.eventGift.recommended_time = action.payload.suggest_date;
      state.eventGift.type_way =
        action.payload.type_join === 1
          ? 2
          : action.payload.type_join === 3
          ? 1
          : 3;
      state.eventGift.type_event_status = action.payload.status;
      state.eventGift.event_design.key_type_product = action.payload.group_id;
      state.eventGift.conditions_apply = action.payload.type_condition_apply;

      const arrCity = action.payload.lst_city.map((item) => {
        return item.id;
      });

      action.payload.lst_city.map((item) => {
        item.parent = item.parent_id;
        item.key = item.id;
      });

      const arrayRegion = action.payload.lst_region.map((item) => {
        return item.id;
      });

      const arrayMembership = action.payload.lst_membership.map((item) => {
        return item.id;
      });

      state.eventGift.event_filter = {
        arrayRegion: arrayRegion,
        arrayCity: arrCity,
        arrayCityClone: action.payload.lst_city,
        id_agency: action.payload.level_id,
        membership: arrayMembership,
      };

      if (action.payload.type_condition_apply === 2) {
        let reward = [];
        let count = 0;
        action.payload.list_condition_product.forEach((item) => {
          if (item.han_muc > count) {
            count = item.han_muc;
          }
        });

        for (let i = 1; i <= count; i++) {
          reward.push([]);
          action.payload.list_condition_product.forEach((item) => {
            if (item.han_muc === i) {
              reward[i - 1].push({
                number_to: item.condition_min,
                discount: item.discount,
                valid_number_to: false,
                valid_discount: false,
              });
            }
          });
        }

        state.eventGift.event_design.reward = reward;
      } else {
        let rewardQuantity = [];
        let count = 0;
        action.payload.list_condition_product.forEach((item) => {
          if (item.han_muc > count) {
            count = item.han_muc;
          }
        });

        for (let i = 1; i <= count; i++) {
          rewardQuantity.push([]);
          action.payload.list_condition_product.forEach((item) => {
            if (item.han_muc === i) {
              rewardQuantity[i - 1].push({
                number_to: item.condition_min,
                discount: item.discount,
                conversion_rate: item.quy_doi_ty_le,
                valid_number_to: false,
                valid_discount: false,
              });
            }
          });
        }

        state.eventGift.event_design.rewardQuantity = rewardQuantity;
      }

      let listArrListTreeConfirmClone = [];
      let listArrDisabled = [];
      action.payload.list_cate_type.forEach((item) => {
        let treeProduct = item.event_group_product;
        listArrListTreeConfirmClone.push({ treeProduct: treeProduct });
        item.event_group_product.forEach((itemLv1) => {
          itemLv1.children.forEach((itemLv2) => {
            itemLv2.children.forEach((itemLv3) => {
              listArrDisabled.push(itemLv3.tree_id);
            });
          });
        });
      });
      if (action.payload.group_id === 1) {
        state.treeProductEventGiftGroup.listArrListTreeConfirm =
          listArrListTreeConfirmClone;
        state.treeProductEventGiftGroup.listArrDisabled = listArrDisabled;
        state.treeProductEventGiftGroup.listKeyCloneDisabled = listArrDisabled;
      } else {
        state.treeProductEventGiftGroup2.listArrListTreeConfirm =
          listArrListTreeConfirmClone;
        state.treeProductEventGiftGroup2.listArrDisabled = listArrDisabled;
        state.treeProductEventGiftGroup2.listKeyCloneDisabled = listArrDisabled;
      }

      state.eventGift.content_event = action.payload.description;
      state.eventGift.code_event = action.payload.code;
      state.eventGift.img_event = action.payload.image;
      state.eventGift.name_event = action.payload.name;
      state.eventGift.show_name_event =
        action.payload.is_show_name === 1 ? true : false;
      state.eventGift.IsEdit = true;
      state.eventGift.URl_img = action.payload.image_url;

      const application = action.payload.list_user_join.map((item) => {
        const shop_name = item.user_agency_name;
        const dms_code = item.user_agency_code;
        const level = item.level;
        const address = item.address;
        const id = item.id;
        const agency_name = item.agency_name;
        return { shop_name, dms_code, level, address, agency_name, id };
      });

      state.modalS1.listS1Show =
        action.payload.type_join === 1 ? application : [];

      state.modalS1.listS1Add =
        action.payload.type_join === 1 ? application : [];

      state.modalS1.listS1AddClone =
        action.payload.type_join === 1 ? application : [];

      return state;
    },

    CHECK_VALIDATE: (state, action) =>
      reducerEvent.CHECK_VALIDATE(state, action),

    UPDATE_TREE_PRIORITY: (state, action) =>
      reducerTreeProduct.UPDATE_TREE_PRIORITY(state, action),
    // EventImmediate

    CREATE_DATE_EVENT_IMMEDIATE: (state, action) =>
      reducerEventImmediate.CREATE_DATE_EVENT_IMMEDIATE(state, action),
    CREATE_DATE_EVENT_FILTER_IMMEDIATE: (state, action) =>
      reducerEventImmediate.CREATE_DATE_EVENT_FILTER_IMMEDIATE(state, action),
    CREATE_KEY_DEGSIN_IMMEDIATE: (state, action) =>
      reducerEventImmediate.CREATE_KEY_DEGSIN_IMMEDIATE(state, action),

    // PRODUCT
    CREATE_PRODUCT_INFO: (state, action) =>
      reducerProduct.CREATE_PRODUCT_INFO(state, action),
    CLEAR_REDUX_PRODUCT: (state, action) => {
      state = { ...state, ...initialProductDefaults };
      return state;
    },
    // CLEAR_REDUX_PRODUCT_VERSION: (state, action) => {
    //   state = { ...state, ...initialProductDefaults };
    //   return state;
    // },
    CREATE_PRODUCT_VERSION: (state, action) => {
      reducerProduct.CREATE_PRODUCT_VERSION(state, action);
    },
    CHECK_PRODUCT_DEFAULT: (state, action) => {
      reducerProduct.CHECK_PRODUCT_DEFAULT(state, action);
    },
    CREATE_PRODUCT_VERSION_INFO: (state, action) => {
      reducerProduct.CREATE_PRODUCT_VERSION_INFO(state, action);
    },
    ADD_SPECIFICATION_INFO: (state, action) => {
      reducerProduct.ADD_SPECIFICATION_INFO(state, action);
    },
    REMOVE_SPECIFICATION_INFO: (state, action) => {
      reducerProduct.REMOVE_SPECIFICATION_INFO(state, action);
    },
    CREATE_PRODUCT_SPECIFICATION_INFO: (state, action) => {
      reducerProduct.CREATE_PRODUCT_SPECIFICATION_INFO(state, action);
    },
    CLEAR_REDUX_PRODUCT_VERSION: (state, action) => {
      reducerProduct.CLEAR_REDUX_PRODUCT_VERSION(state, action);
    },
    ASSIGNED_DATA_PRODUCT_REDUX_DETAIL: (state, action) => {
      state.product.productCode = !action.payload.productCode
        ? ""
        : action.payload.productCode;
      state.product.productName = !action.payload.name
        ? ""
        : action.payload.name;
      state.product.cate = action.payload.typeParentId;
      state.product.brand = action.payload.typeId;
      state.product.newBrand = action.payload.categoryId;
      state.product.outStandingBrand = action.payload.brandId;
      state.product.pdf_url = action.payload.specification_url;
      state.product.pdf_name = action.payload.specification;

      let arrVersion = action.payload.listVersion.map((itemVersion) => {
        const id = itemVersion.id;
        const colorID = itemVersion.colorId;
        const colorName = !itemVersion.colorName ? "" : itemVersion.colorName;
        const year = !itemVersion.year_of_manufacture
          ? ""
          : itemVersion.year_of_manufacture;
        const barrelType = !itemVersion.characteristics
          ? ""
          : itemVersion.characteristics;
        const productVersionCode = !itemVersion.versionCode
          ? ""
          : itemVersion.versionCode;
        const publishedPrice = itemVersion.price;
        const boxPrice = itemVersion.price_package;
        const registrationPrice = itemVersion.price_register;
        const totalPrice = itemVersion.total;
        const price_promotion = itemVersion.price_promotion;
        return {
          id,
          colorID,
          colorName,
          productVersionCode,
          year,
          barrelType,
          publishedPrice,
          boxPrice,
          registrationPrice,
          totalPrice,
          price_promotion
        };
      });

      state.product.arrProductVersion = arrVersion;
      return state;
    },

    ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL: (state, action) => {
      let obj = {
        productVersionDescription: !action.payload.value.note
          ? ""
          : action.payload.value.note,
        urlImages: !action.payload.value.image_url
          ? []
          : action.payload.value.image_url,
        arrayImages: !action.payload.value.image
          ? []
          : action.payload.value.image,
      };

      const newObj = {
        ...state.product.arrProductVersion[action.payload.idx],
        ...obj,
      };

      state.product.arrProductVersion[action.payload.idx] = newObj;

      return state;
    },
    SORT_LIST_VERSION: (state, action) => {
      reducerProduct.SORT_LIST_VERSION(state, action);
    },
    CHECK_NULL_VALUE_LIST_SPECIFICATION: (state, action) => {
      reducerProduct.CHECK_NULL_VALUE_LIST_SPECIFICATION(state, action);
    },
    ADD_ITEM_SPEC_INFO: (state, action) => {
      reducerProduct.ADD_ITEM_SPEC_INFO(state, action);
    },
    ASSIGN_MIN_MAX_UNIT: (state, action) => {
      reducerProduct.ASSIGN_MIN_MAX_UNIT(state, action);
    },
    CREATE_INFO_EVENT_GIFT: (state, action) => {
      reducerEventGift.CREATE_INFO_EVENT_GIFT(state, action);
    },
    CREATE_EVENT_GIFT_FILTER: (state, action) => {
      reducerEventGift.CREATE_EVENT_GIFT_FILTER(state, action);
    },
    CLEAR_REDUX_EVENT_GIFT: (state, action) => {
      state = { ...state, ...initialStoreEventGift };
      return state;
    },
    ADD_EVENT_GIFT_LEVEL: (state, action) => {
      reducerEventGift.ADD_EVENT_GIFT_LEVEL(state, action);
      return state;
    },
    REMOVE_EVENT_GIFT_LEVEL: (state, action) => {
      reducerEventGift.REMOVE_EVENT_GIFT_LEVEL(state, action);
      return state;
    },
    UPDATE_KEY_CATE_EVENT_GIFT: (state, action) => {
      reducerEventGift.UPDATE_KEY_CATE_EVENT_GIFT(state, action);
      return state;
    },
    UPDATE_DESIGN_EVENT_GIFT: (state, action) => {
      reducerEventGift.UPDATE_DESIGN_EVENT_GIFT(state, action);
      return state;
    },
    CHECK_VALIDATE_EVENT_GIFT: (state, action) => {
      reducerEventGift.CHECK_VALIDATE_EVENT_GIFT(state, action);
      return state;
    },
    SAVE_TREE_NEW: (state, action) => {
      reducerTreeProduct.SAVE_TREE_NEW(state, action);
      return state;
    },
    CANCEL_TREE_NEW: (state, action) => {
      reducerTreeProduct.CANCEL_TREE_NEW(state, action);
      return state;
    },
    REMOVE_PRODUCT: (state, action) => {
      reducerTreeProduct.REMOVE_PRODUCT(state, action);
      return state;
    },
    REMOVE_PRODUCT_CONFIRM: (state, action) => {
      reducerTreeProduct.REMOVE_PRODUCT_CONFIRM(state, action);
      return state;
    },
    ASSIGN_LIST_KEY_CLONE: (state, action) => {
      reducerTreeProduct.ASSIGN_LIST_KEY_CLONE(state, action);
      return state;
    },
    ADD_TREE_KEY_CLONE: (state, action) => {
      reducerTreeProduct.ADD_TREE_KEY_CLONE(state, action);
      return state;
    },
    UPDATE_ID_COUNT_SAVE_ITEM: (state, action) => {
      reducerEventGift.UPDATE_ID_COUNT_SAVE_ITEM(state, action);
      return state;
    },

    UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE: (state, action) => {
      reducerEventImmediateNew.UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE(
        state,
        action
      );
      return state;
    },

    ADD_PRODUCT_GROUP_LEVEL: (state, action) => {
      reducerEventGift.ADD_PRODUCT_GROUP_LEVEL(state, action);
      return state;
    },
    SAVE_TREE_GROUP: (state, action) => {
      reducerTreeProduct.SAVE_TREE_GROUP(state, action);
      return state;
    },
    SAVE_TREE_GROUP_OVER: (state, action) => {
      reducerTreeProduct.SAVE_TREE_GROUP_OVER(state, action);
      return state;
    },
    CLOSE_MODAL_TREE: (state, action) => {
      reducerTreeProduct.CLOSE_MODAL_TREE(state, action);
      return state;
    },
    ASSIGNED_TREE_CLICK_GROUP: (state, action) => {
      reducerTreeProduct.ASSIGNED_TREE_CLICK_GROUP(state, action);
      return state;
    },
    ADD_DESIGN_EVENT_LEVEL_GROUP: (state, action) => {
      reducerEventGift.ADD_DESIGN_EVENT_LEVEL_GROUP(state, action);
      return state;
    },
    REMOVE_PRODUCT_GROUP: (state, action) => {
      reducerTreeProduct.REMOVE_PRODUCT_GROUP(state, action);
      return state;
    },
    REMOVE_LEVEL: (state, action) => {
      reducerEventGift.REMOVE_LEVEL(state, action);
      return state;
    },
    CHANGE_CONDITION_APPLY: (state, action) => {
      reducerEventGift.CHANGE_CONDITION_APPLY(state, action);
      return state;
    },
    CLEAR_REDUX_PRODUCTS_BY_OBJECT: (state, action) => {
      state = { ...state, ...initialStoreProductsByObject };
      return state;
    },
    CREATE_PRODUCT_BY_OBJECT_INFO: (state, action) => {
      reducerProductsByObject.CREATE_PRODUCT_BY_OBJECT_INFO(state, action);
      return state;
    },
    CHANGE_PRICE_PRODUCT_BY_OBJECT: (state, action) => {
      reducerProductsByObject.CHANGE_PRICE_PRODUCT_BY_OBJECT(state, action);
      return state;
    },
    CHECK_VALIDATE_PRICE_BY_OBJECT: (state, action) => {
      reducerProductsByObject.CHECK_VALIDATE_PRICE_BY_OBJECT(state, action);
      return state;
    },
    REMOVE_PRODUCT_BY_OBJECT_CONFIRM: (state, action) => {
      reducerTreeProduct.REMOVE_PRODUCT_BY_OBJECT_CONFIRM(state, action);
      return state;
    },
    REMOVE_PRODUCT_BY_OBJECT: (state, action) => {
      reducerTreeProduct.REMOVE_PRODUCT_BY_OBJECT(state, action);
      return state;
    },
    ADD_TREE_CLONE_NOT_DISABLE: (state, action) => {
      reducerTreeProduct.ADD_TREE_CLONE_NOT_DISABLE(state, action);
      return state;
    },
    ADD_CONTENT_PRODUCT: (state, action) => {
      reducerTreeProduct.ADD_CONTENT_PRODUCT(state, action);
      return state;
    },

    // trả ngay
    ADD_BONUS_LEVEL_EVENT_IMMEDIATE_NEW: (state, action) =>
      reducerEventImmediateNew.ADD_BONUS_LEVEL_EVENT_IMMEDIATE_NEW(
        state,
        action
      ),
    ADD_GIFT_LEVEL_EVENT_IMMEDIATE_NEW: (state, action) =>
      reducerEventImmediateNew.ADD_GIFT_LEVEL_EVENT_IMMEDIATE_NEW(
        state,
        action
      ),
    ASSIGNED_TREE_CLICK_GROUP_CONDITION_ARRAY: (state, aciton) => {
      const listKeyNew = aciton.payload.data[0].children.map((item) => item.id);
      listKeyNew.forEach((item, index) => {
        const idx =
          state.dataArrayCondition[
            aciton.payload.idxx
          ].treeProductConditionGroup.listArrDisabled.indexOf(item);
        state.dataArrayCondition[
          aciton.payload.idxx
        ].treeProductConditionGroup.listArrDisabled.splice(idx, 1);
      });

      state.dataArrayCondition[
        aciton.payload.idxx
      ].treeProductConditionGroup.listKeyCloneDisabled = [...listKeyNew];

      state.dataArrayCondition[
        aciton.payload.idxx
      ].treeProductConditionGroup.listTreeClone = [...aciton.payload.data];
      state.dataArrayCondition[
        aciton.payload.idxx
      ].treeProductConditionGroup.listTreeShow = [...aciton.payload.data];
      state.dataArrayCondition[
        aciton.payload.idxx
      ].treeProductConditionGroup.listKeyClone = [...listKeyNew];
      state.dataArrayCondition[
        aciton.payload.idxx
      ].treeProductConditionGroup.listArrDisabled = [
        ...state.dataArrayCondition[aciton.payload.idxx]
          .treeProductConditionGroup.listArrDisabled,
      ];
    },
    ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE: (state, action) =>
      reducerTreeProduct.ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE(state, action),
    CLEAR_MODAL_ALL_KD: (state, action) => {
      reducerModalThunk.CLEAR_MODAL_ALL_KD(state, action);
    },
    CLEAR_REDUX_IMMEDIATE_NEW: (state, action) => {
      state = { ...state, ...initialStoreDefaultImmediateNew };
      return state;
    },
    CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW: (state, action) =>
      reducerEventImmediateNew.CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW(
        state,
        action
      ),
    CREATE_DATE_EVENT_IMMEDIATE_NEW: (state, action) =>
      reducerEventImmediateNew.CREATE_DATE_EVENT_IMMEDIATE_NEW(state, action),
    CREATE_KEY_DEGSIN_IMMEDIATE_NEW: (state, action) =>
      reducerEventImmediateNew.CREATE_KEY_DEGSIN_IMMEDIATE_NEW(state, action),
    ONCHANGE_CONDITION_EVENT_GROUP_COMBO_ARRAY: (state, action) => {
      state.eventImmediateNew.arr_condition_attach_group[
        action.payload.idx
      ].salesOrQuantityGroup = action.payload.value;
      return state;
    },

    REMOVE_BONUS_LEVEL: (state, action) =>
      reducerEventImmediateNew.REMOVE_BONUS_LEVEL(state, action),
    REMOVE_COFIRM_MODAL_THUNK: (state, action) => {
      reducerModalThunk.REMOVE_COFIRM_MODAL_THUNK(state, action);
    },
    REMOVE_CONDITION_COMBO: (state, action) => {
      state.dataArrayCondition.splice(action.payload, 1);
      state.idxDataArrayCondition = 0;
    },
    REMOVE_GIFT_LEVEL: (state, action) =>
      reducerEventImmediateNew.REMOVE_GIFT_LEVEL(state, action),
    UPDATE_CONDITION_COMBO: (state, action) => {
      state.dataArrayCondition[action.payload.idx][action.payload.key] =
        action.payload.value;
    },
    UPDATE_GIFT_GROUP_PRODUCT: (state, action) =>
      reducerEventImmediateNew.UPDATE_GIFT_GROUP_PRODUCT(state, action),
    UPDATE_IDX_ARRAY_COMBO_DESIGN_CONDITION: (state, action) => {
      state.idxDataArrayCondition = action.payload;
    },
    UPDATE_KEY_EVENT_COMBO_DESIGN_CONDITION_ARRAY: (state, action) => {
      state.dataArrayCondition[action.payload.idx].eventCombo.event_design[
        action.payload.key
      ] = action.payload.value;
      return state;
    },
    UPDATE_REWARD_PERFORMANCE: (state, action) =>
      reducerEventImmediateNew.UPDATE_REWARD_PERFORMANCE(state, action),
    UPDATE_REWARD_PERFORMANCE_PRODUCT: (state, action) =>
      reducerEventImmediateNew.UPDATE_REWARD_PERFORMANCE_PRODUCT(state, action),
    ASSIGNED_EVENT_INFO_DETAIL: (state, action) =>
      reducerEventImmediateNew.ASSIGNED_EVENT_INFO_DETAIL(state, action),
    ADD_ALL_MODAL_THUNK: (state, action) => {
      reducerModalThunk.ADD_ALL_MODAL_THUNK(state, action);
    },
    ADD_MODAL_THUNK: (state, action) => {
      reducerModalThunk.ADD_MODAL_THUNK(state, action);
    },
    COFIRM_MODAL_THUNK: (state, action) => {
      reducerModalThunk.COFIRM_MODAL_THUNK(state, action);
    },
    SHOW_MODAL_THUNK: (state, action) => {
      reducerModalThunk.SHOW_MODAL_THUNK(state, action);
    },
    REMOVE_MODAL_THUNK: (state, action) => {
      reducerModalThunk.REMOVE_MODAL_THUNK(state, action);
    },
    REMOVE_ALL_MODAL_THUNK: (state, action) => {
      reducerModalThunk.REMOVE_ALL_MODAL_THUNK(state, action);
    },
    CLOSE_MODAL_COMBO_CONDITION: (state, action) =>
      reducerTreeProduct.CLOSE_MODAL_COMBO_CONDITION(state, action),
    SAVE_TREE_GROUP_COMBO_CONDITION: (state, action) =>
      reducerTreeProduct.SAVE_TREE_GROUP_COMBO_CONDITION(state, action),
    SAVE_TREE_GROUP_COMBO_OVER_CONDITION: (state, action) => {
      reducerTreeProduct.SAVE_TREE_GROUP_COMBO_OVER_CONDITION(state, action);
    },
    ADD_ARR_PRODUCT_GIFT_GROUP: (state, action) =>
      reducerEventImmediateNew.ADD_ARR_PRODUCT_GIFT_GROUP(state, action),
    CLOSE_MODAL_COMBO: (state, action) =>
      reducerTreeProduct.CLOSE_MODAL_COMBO(state, action),
    SAVE_TREE_GIFT_GROUP_IMMEDIATE: (state, action) =>
      reducerTreeProduct.SAVE_TREE_GIFT_GROUP_IMMEDIATE(state, action),
    SAVE_TREE_GIFT_GROUP_IMMEDIATE_OVER: (state, action) =>
      reducerTreeProduct.SAVE_TREE_GIFT_GROUP_IMMEDIATE_OVER(state, action),
    ADD_FIRST_BONUS_LEVEL_EVENT_IMMEDIATE_NEW: (state, action) =>
      reducerEventImmediateNew.ADD_FIRST_BONUS_LEVEL_EVENT_IMMEDIATE_NEW(
        state,
        action
      ),
    ADD_BONUS_LEVEL_ATTACH_EVENT_IMMEDIATE_NEW: (state, action) =>
      reducerEventImmediateNew.ADD_BONUS_LEVEL_ATTACH_EVENT_IMMEDIATE_NEW(
        state,
        action
      ),
    UPDATE_ARR_ATTACH: (state, action) =>
      reducerEventImmediateNew.UPDATE_ARR_ATTACH(state, action),
    ADD_PRODUCT_GROUP_LEVEL_EVENT_IMMEDIATE: (state, action) =>
      reducerEventImmediateNew.ADD_PRODUCT_GROUP_LEVEL_EVENT_IMMEDIATE(
        state,
        action
      ),
    ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE_CONDITION_ATTACH: (state, action) => {
      reducerTreeProduct.ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE_CONDITION_ATTACH(
        state,
        action
      );
    },
    REMOVE_PRODUCT_GROUP_IMMEDIATE: (state, action) => {
      reducerTreeProduct.REMOVE_PRODUCT_GROUP_IMMEDIATE(state, action);
    },
    SAVE_TREE_GROUP_NEW: (state, action) => {
      reducerTreeProduct.SAVE_TREE_GROUP_NEW(state, action);
    },
    SAVE_TREE_GROUP_OVER_NEW: (state, action) => {
      reducerTreeProduct.SAVE_TREE_GROUP_OVER_NEW(state, action);
    },
    CLOSE_MODAL_TREE_NEW: (state, action) => {
      reducerTreeProduct.CLOSE_MODAL_TREE_NEW(state, action);
    },

    ADD_CONTENT_PRODUCT_NEW: (state, action) => {
      reducerTreeProduct.ADD_CONTENT_PRODUCT_NEW(state, action);
    },
    CLEAR_REDUX_LOCALITY: (state, action) => {
      state = { ...state, ...initialStoreDefaultLocality };
      return state;
    },
    ARRAY_LOCALITY: (state, action) => Array_locality(state, action),
    ADD_LOCALITY: (state, action) => Add_locality(state, action),
    SELECT_FREQUENCY_ID: (state, action) => Select_frequency_id(state, action),
    SAVE_LOCALITY: (state, action) => Save_locality(state, action),
    CONFIRM_LOCALITY: (state, action) => Confirm_locality(state, action),
    RESET_LOCALITY: (state, action) => Reset_locality(state, action),
    DEAFUAR_LISTLOCALITY: (state, action) =>
      Deafuat_listLocality(state, action),
    REMOVE_LOCALITY: (state, action) => Remove_locality(state, action),
    UPDATE_ACTIVELIST: (state, action) => Update_ActiveList(state, action),
    CANCEL_LOCALITY: (state, action) => Cancel_locality(state, action),
    NOT_CHECK_ALL: (state, action) => Not_checkAll(state, action),

    //Tuyến bán hàng
    ARRAY_S2: (state, action) => Array_S2(state, action),
    ADD_S2: (state, action) => Add_S2(state, action),
    SELECT_FREQUENCY_ID_SELL: (state, action) =>
      Select_frequency_id_sell(state, action),
    SAVE_S2: (state, action) => Save_S2(state, action),
    CONFIRM_S2: (state, action) => Confirm_S2(state, action),
    RESET_S2: (state, action) => Reset_S2(state, action),
    DEAFUAR_S2: (state, action) => Deafuat_S2(state, action),
    REMOVE_S2: (state, action) => Remove_S2(state, action),
    UPDATE_ACTIVELISTSELL: (state, action) =>
      Update_ActiveListSell(state, action),
    CANCEL_S2: (state, action) => Cancel_S2(state, action),
    CHANGE_ITEM: (state, action) => {
      reducerTreeProduct.CHANGE_ITEM(state, action);
    },
    ADD_ARR_CONFIRMED: (state, action) => {
      reducerTreeProduct.ADD_ARR_CONFIRMED(state, action);
    },
    ADD_NEW_OBJ_FROM_SERVER: (state, action) => {
      reducerTreeProduct.ADD_NEW_OBJ_FROM_SERVER(state, action);
    },
    ADD_OBJ_UPDATE_CONFIRMED: (state, action) => {
      reducerTreeProduct.ADD_OBJ_UPDATE_CONFIRMED(state, action);
    },
    REMOVE_OBJ_UNCHECK: (state, action) => {
      reducerTreeProduct.REMOVE_OBJ_UNCHECK(state, action);
    },
    CANCEL_ALL_TREE: (state, action) => {
      state = { ...state, treeProduct: initialTreeProduct };
      return state;
    },
    REMOVE_PRODUCT_CONFIRM_NEW: (state, action) => {
      reducerTreeProduct.REMOVE_PRODUCT_CONFIRM_NEW(state, action);
    },
    ASSIGNED_DATA_EDIT: (state, action) => {
      let dataProduct = JSON.parse(JSON.stringify(state.dataProduct));
      let listProduct = action.payload.listProduct;

      let objUpdateConfirm = {};
      listProduct.forEach((item) => {
        objUpdateConfirm[item.product_id] = {
          quantity: item.quantity,
          attributesId: item.attribute_id,
          attribute_ratio: item.attribute_ratio,
          quantity_min: item.quantity_min,
          frame_numbers: item.frame_numbers,
          seri_numbers: item.seri_numbers
        };
      });

      let listTree = [];
      dataProduct.forEach((itemLv1) => {
        let arrChildLv1 = [];
        itemLv1.children.forEach((itemLv2) => {
          let arrChildLv2 = [];
          itemLv2.children.forEach((itemLv3) => {
            let idxProduct = listProduct.findIndex(
              (item) => item.product_id === itemLv3.id
            );
            if (idxProduct !== -1) {
              arrChildLv2.push(itemLv3);
            }
          });

          if (arrChildLv2.length > 0) {
            arrChildLv1.push({
              ...itemLv2,
              children: arrChildLv2,
            });
          }
        });

        if (arrChildLv1.length > 0) {
          listTree.push({
            ...itemLv1,
            children: arrChildLv1,
          });
        }
      });

      let listKey = [];
      let arrConfirmTmp = [];
      listTree.forEach((itemLv1) => {
        itemLv1.children.forEach((itemLv2) => {
          itemLv2.children.forEach((itemLv3, indexLv3) => {
            arrConfirmTmp.push(itemLv3);
            listKey.push(itemLv3.tree_id);
          });
        });
      });

      let arrConfirm = arrConfirmTmp.map((item, index) => {
        item.stt = index + 1;
        return item;
      });


      state.treeProduct.listTreeClone = listTree;
      state.treeProduct.listTreeShow = listTree;
      state.treeProduct.listConfirm = listTree;
      state.treeProduct.listKeyClone = listKey;
      state.treeProduct.listKeyConfirm = listKey;
      state.treeProduct.arrConfirm = arrConfirm;
      state.treeProduct.objUpdateConfirm = objUpdateConfirm;
      state.treeProduct.objUpdateShow = objUpdateConfirm;
      return state;
    },
    ASSIGN_LIST_TARGET_KPI : (state,action) => {
      reducerKpiTarget.ASSIGN_LIST_TARGET_KPI(state,action)
    },
    ADD_LIST_CLONE_CHOOSE:(state,action)=>{
      reducerKpiTarget.ADD_LIST_CLONE_CHOOSE(state,action)
    },
    ADD_LIST_SHOW_CHOOSE : (state,action)=>{
      reducerKpiTarget.ADD_LIST_SHOW_CHOOSE(state,action)
    },
    EDIT_COUNT : (state,action)=>{
      reducerKpiTarget.EDIT_COUNT(state,action)
    },
    SAVE_LIST_APPROVE:(state,action)=>{
      reducerKpiTarget.SAVE_LIST_APPROVE(state,action)
    },
    CANCEL_LIST_KPI_MODAL :(state,action)=>{
      reducerKpiTarget.CANCEL_LIST_KPI_MODAL(state,action)
    },
    REMOVE_ITEM_SHOW: (state,action)=>{
      reducerKpiTarget.REMOVE_ITEM_SHOW(state,action)
    },
    REMOVE_ITEM_APPROVE:(state,action)=>{
      reducerKpiTarget.REMOVE_ITEM_APPROVE(state,action)
    },
    ASSIGNED_DATA_TARGET_KPI_EDIT:(state,action)=>{
      reducerKpiTarget.ASSIGNED_DATA_TARGET_KPI_EDIT(state,action)
    },
    CANCEL_LIST_KPI_MODAL_ALL:(state,action)=>{
      state = { ...state, listTargetKpi:initListTargetKpi };
      return state;
    }
  },
  extraReducers: {
    [_fetchAPIDetailEventImmediateParent.pending]: (state, action) => {
      state.eventImmediateNew.loading = true;
    },

    [_fetchAPIDetailEventImmediateParent.fulfilled]: (state, action) => {
      const data = action.payload;
      state.eventImmediateNew.dataDetail = { ...data };
      state.eventImmediateNew.loading = false;

      state.eventImmediateNew.is_evet_parent = 2;
      state.eventImmediateNew.from_time_apply = data.event_response.start_date;
      state.eventImmediateNew.to_time_apply = data.event_response.end_date;
      state.eventImmediateNew.attribute_promotion =
        data.event_response.product_atrtibute_type;

      state.eventImmediateNew.type_way =
        data.event_response.lst_city.length > 0 ? 3 : 1;
      state.eventImmediateNew.event_filter.arrayCity =
        data.event_response.lst_city.map((item) => item.id);
      // state.eventImmediateNew.event_filter.arrayRegion =
      //   data.event_response.lst_district.map((item) => item.id);
      state.eventImmediateNew.event_filter.id_agency =
        data.event_response.level_id;
      state.eventImmediateNew.event_degsin.key_cate =
        data.event_response.company_id;
      state.eventImmediateNew.condition_promotion =
        data.event_response.application_type_id;
      state.eventImmediateNew.gift_promotion =
        data.event_response.reward_type_id;
      state.eventImmediateNew.content_event = data.event_response.description;
      state.eventImmediateNew.img_event = data.event_response.image;
      state.modalS1.listS1Show =
        data.event_response.type_entry_condition === 2
          ? data.event_response.list_application_object.map((item) => item.id)
          : [];
      state.modalS2.listS2Show =
        data.event_response.type_entry_condition === 3
          ? data.event_response.list_application_object.map((item) => item.id)
          : [];

      state.eventImmediateNew.name_event = data.event_response.name;
      state.eventImmediateNew.pdf_name = data.event_response.pdf_name;
      state.eventImmediateNew.code_event = data.event_response.code;
      state.eventImmediateNew.pdf_url = data.pdf_url;
      state.eventImmediateNew.URl_img = data.image_url;
      return state;
    },
    [_fetchAPIDetailEvent.fulfilled]: (state, action) => {
      state.dataDetail = action.payload;
    },
    [_fetchAPIListWard.pending]: (state, action) => {
      state.modalWard.loading = true;
    },
    [_fetchAPIListWard.fulfilled]: (state, action) => {
      state.modalWard.listWard = action.payload.district;
      if (!action.meta.arg.search) {
        let listCityConfirm = JSON.parse(
          JSON.stringify(state.modalWard.listWardShow)
        );
        const spreaded = [];
        action.payload.district.forEach((item) => {
          const idx = listCityConfirm.map((el) => el.id).indexOf(item.id);
          if (idx >= 0) {
            spreaded.push(item);
          }
        });
        state.modalWard.listWardAdd = [...spreaded];
        state.modalWard.listWardConfirm = [...spreaded];
        state.modalWard.listWardShow = [...spreaded];
      }
      state.modalWard.loading = false;
    },
    [_fetchAPIListDistrict.pending]: (state, action) => {
      state.modalDistrict.loading = true;
    },
    [_fetchAPIListDistrict.fulfilled]: (state, action) => {
      state.modalDistrict.listDistrict = action.payload.district;

      if (!action.meta.arg.search) {
        let listCityConfirm = JSON.parse(
          JSON.stringify(state.modalDistrict.listDistrictShow)
        );
        const spreaded = [];
        action.payload.district.forEach((item) => {
          const idx = listCityConfirm.map((el) => el.id).indexOf(item.id);
          if (idx >= 0) {
            spreaded.push(item);
          }
        });
        state.modalDistrict.listDistrictsAdd = [...spreaded];
        state.modalDistrict.listDistrictConfirm = [...spreaded];
        state.modalDistrict.listDistrictShow = [...spreaded];
      }
      state.modalDistrict.loading = false;
    },
    [_fetchAPIListCity.pending]: (state, action) => {
      state.modalCity.loading = true;
    },
    [_fetchAPIListCity.fulfilled]: (state, action) => {
      state.modalCity.listCity = action.payload.cities;
      if (!action.meta.arg) {
        let listCityConfirm = JSON.parse(
          JSON.stringify(state.modalCity.listCityShow)
        );
        const spreaded = [];
        action.payload.cities.forEach((item) => {
          const idx = listCityConfirm.map((el) => el.id).indexOf(item.id);
          if (idx >= 0) {
            spreaded.push(item);
          }
        });
        state.modalCity.listCityAdd = [...spreaded];
        state.modalCity.listCityShow = [...spreaded];
        state.modalCity.listCityConfirm = [...spreaded];
      }

      state.modalCity.loading = false;
    },
    [_fetchAPIListBusinessRegion.fulfilled]: (state, action) => {
      action.payload.businessRegion.map((item) => {
        item.key = item.id;
        item.value = item.name;
      });
      state.array_businessRegion = action.payload.businessRegion;
    },
    [_fetchAPIListBuusinessRegion.pending]: (state, action) => {
      state.modalBusiness.loading = true;
    },
    [_fetchAPIListBuusinessRegion.fulfilled]: (state, action) => {
      state.modalBusiness.listBusiness = action.payload.list_agency;
      if (!action.meta.arg.key_word) {
        let listBusinessConfirm = JSON.parse(
          JSON.stringify(state.modalBusiness.listBusinessShow)
        );
        const spreaded = [];
        action.payload.list_agency.forEach((item) => {
          const idx = listBusinessConfirm
            .map((el) => el.agency_id)
            .indexOf(item.agency_id);
          if (idx >= 0) {
            spreaded.push(item);
          }
        });
        state.modalBusiness.listBusinessAdd = [...spreaded];
        state.modalBusiness.listBusinessShow = [...spreaded];
        state.modalBusiness.listBusinessConfirm = [...spreaded];
      }
      state.modalBusiness.loading = false;
    },
  },
});
const { reducer, actions } = rootReducers;
export const {
  ASSIGNED_DATA_EDIT,
  REMOVE_PRODUCT_CONFIRM_NEW,
  CANCEL_ALL_TREE,
  REMOVE_OBJ_UNCHECK,
  ADD_OBJ_UPDATE_CONFIRMED,
  ADD_NEW_OBJ_FROM_SERVER,
  ADD_ARR_CONFIRMED,
  CHANGE_ITEM,
  ARRAY_S2,
  ADD_S2,
  SELECT_FREQUENCY_ID_SELL,
  SAVE_S2,
  CONFIRM_S2,
  RESET_S2,
  DEAFUAR_S2,
  REMOVE_S2,
  UPDATE_ACTIVELISTSELL,
  CANCEL_S2,
  CLEAR_REDUX_LOCALITY,
  ARRAY_LOCALITY,
  ADD_LOCALITY,
  SELECT_FREQUENCY_ID,
  SAVE_LOCALITY,
  CONFIRM_LOCALITY,
  RESET_LOCALITY,
  DEAFUAR_LISTLOCALITY,
  REMOVE_LOCALITY,
  UPDATE_ACTIVELIST,
  CANCEL_LOCALITY,
  NOT_CHECK_ALL,
  //trả ngay
  ADD_CONTENT_PRODUCT_NEW,
  SAVE_TREE_GROUP_NEW,
  SAVE_TREE_GROUP_OVER_NEW,
  CLOSE_MODAL_TREE_NEW,
  REMOVE_PRODUCT_GROUP_IMMEDIATE,
  ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE_CONDITION_ATTACH,
  ADD_PRODUCT_GROUP_LEVEL_EVENT_IMMEDIATE,
  UPDATE_ARR_ATTACH,
  ADD_BONUS_LEVEL_ATTACH_EVENT_IMMEDIATE_NEW,
  UPDATE_ID_COUNT_SAVE_ITEM_IMMEDIATE,
  ADD_FIRST_BONUS_LEVEL_EVENT_IMMEDIATE_NEW,
  ADD_BONUS_LEVEL_EVENT_IMMEDIATE_NEW,
  ADD_GIFT_LEVEL_EVENT_IMMEDIATE_NEW,
  ASSIGNED_TREE_CLICK_GROUP_CONDITION_ARRAY,
  ASSIGNED_TREE_CLICK_GROUP_IMMEDIATE,
  CLEAR_MODAL_ALL_KD,
  CLEAR_REDUX_IMMEDIATE_NEW,
  CREATE_DATE_EVENT_FILTER_IMMEDIATE_NEW,
  CREATE_DATE_EVENT_IMMEDIATE_NEW,
  CREATE_KEY_DEGSIN_IMMEDIATE_NEW,
  ONCHANGE_CONDITION_EVENT_GROUP_COMBO_ARRAY,
  REMOVE_BONUS_LEVEL,
  REMOVE_COFIRM_MODAL_THUNK,
  REMOVE_CONDITION_COMBO,
  REMOVE_GIFT_LEVEL,
  UPDATE_CONDITION_COMBO,
  UPDATE_GIFT_GROUP_PRODUCT,
  UPDATE_IDX_ARRAY_COMBO_DESIGN_CONDITION,
  UPDATE_KEY_EVENT_COMBO_DESIGN_CONDITION_ARRAY,
  UPDATE_REWARD_PERFORMANCE,
  UPDATE_REWARD_PERFORMANCE_PRODUCT,
  ASSIGNED_EVENT_INFO_DETAIL,
  ADD_ALL_MODAL_THUNK,
  ADD_MODAL_THUNK,
  COFIRM_MODAL_THUNK,
  SHOW_MODAL_THUNK,
  REMOVE_MODAL_THUNK,
  REMOVE_ALL_MODAL_THUNK,
  CLOSE_MODAL_COMBO_CONDITION,
  SAVE_TREE_GROUP_COMBO_CONDITION,
  SAVE_TREE_GROUP_COMBO_OVER_CONDITION,
  ADD_ARR_PRODUCT_GIFT_GROUP,
  CLOSE_MODAL_COMBO,
  SAVE_TREE_GIFT_GROUP_IMMEDIATE,
  SAVE_TREE_GIFT_GROUP_IMMEDIATE_OVER,
  // trả ngay
  ADD_CONTENT_PRODUCT,
  ADD_TREE_CLONE_NOT_DISABLE,
  REMOVE_PRODUCT_BY_OBJECT,
  REMOVE_PRODUCT_BY_OBJECT_CONFIRM,
  CHECK_VALIDATE_PRICE_BY_OBJECT,
  CHANGE_PRICE_PRODUCT_BY_OBJECT,
  CREATE_PRODUCT_BY_OBJECT_INFO,
  CLEAR_REDUX_PRODUCTS_BY_OBJECT,
  CHANGE_CONDITION_APPLY,
  REMOVE_LEVEL,
  REMOVE_PRODUCT_GROUP,
  ADD_DESIGN_EVENT_LEVEL_GROUP,
  ASSIGNED_TREE_CLICK_GROUP,
  CLOSE_MODAL_TREE,
  SAVE_TREE_GROUP_OVER,
  SAVE_TREE_GROUP,
  ADD_PRODUCT_GROUP_LEVEL,
  UPDATE_ID_COUNT_SAVE_ITEM,
  ADD_TREE_KEY_CLONE,
  ASSIGN_LIST_KEY_CLONE,
  REMOVE_PRODUCT_CONFIRM,
  REMOVE_PRODUCT,
  CANCEL_TREE_NEW,
  SAVE_TREE_NEW,
  CHECK_VALIDATE_EVENT_GIFT,
  UPDATE_DESIGN_EVENT_GIFT,
  UPDATE_KEY_CATE_EVENT_GIFT,
  REMOVE_EVENT_GIFT_LEVEL,
  ADD_EVENT_GIFT_LEVEL,
  CLEAR_REDUX_EVENT_GIFT,
  CREATE_INFO_EVENT_GIFT,
  CREATE_EVENT_GIFT_FILTER,
  ASSIGN_MIN_MAX_UNIT,
  ADD_ITEM_SPEC_INFO,
  CHECK_NULL_VALUE_LIST_SPECIFICATION,
  SORT_LIST_VERSION,
  ASSIGNED_DATA_PRODUCT_VERSION_REDUX_DETAIL,
  ASSIGNED_DATA_PRODUCT_REDUX_DETAIL,
  CLEAR_REDUX_PRODUCT_VERSION,
  CREATE_PRODUCT_SPECIFICATION_INFO,
  REMOVE_SPECIFICATION_INFO,
  ADD_SPECIFICATION_INFO,
  CREATE_PRODUCT_VERSION_INFO,
  CHECK_PRODUCT_DEFAULT,
  CREATE_PRODUCT_INFO,
  CLEAR_REDUX_PRODUCT,
  CREATE_PRODUCT_VERSION,
  CREATE_DATE_EVENT,
  CREATE_DATE_EVENT_FILTER,
  ADD_DESGIN_EVENT,
  REMOVE_DESGIN_EVENT,
  UPDATE_DESGIN_EVENT,
  ADD_MODAL,
  REMOVE_MODAL,
  UPDATE_ACTIVELIST_MODAL,
  SAVE_MODAL,
  CONFIRM_MODAL,
  ARRAY_MODAL,
  REST_MODAL,
  DEAFUAT_MODAL,
  CANCEL_MODAL,
  UPDATE_ACTIVELISTAGENCY_MODAL,
  ADD_TREE,
  SHOW_TREE,
  SAVE_TREE,
  CLEAR_REDUX,
  CLEAR_REDUX_IMMEDIATE,
  UPDATE_TREE,
  ASSIGNED_DATA_REDUX_DETAIL,
  CANCEL_TREE,
  UPDATE_DESGIN_EVENT_DISCOUNT,
  UPDATE_KEY_CATE,
  RESET_MODAL,
  CHECK_VALIDATE,
  UPDATE_TREE_PRIORITY,
  CREATE_DATE_EVENT_IMMEDIATE,
  CREATE_DATE_EVENT_FILTER_IMMEDIATE,
  CREATE_KEY_DEGSIN_IMMEDIATE,
  CREATE_DATA_PRODUCT,
  UPDATE_DATA_PRODUCT,
  REMOVE_TREE,
  ASSIGNED_KEY,
  UPDATE_TREE_KEYDEFAULT,
  UPDATE_KEY_REWARD,
  ASSIGN_LIST_TARGET_KPI,
  ADD_LIST_CLONE_CHOOSE,
  ADD_LIST_SHOW_CHOOSE,
  EDIT_COUNT,
  SAVE_LIST_APPROVE,
  CANCEL_LIST_KPI_MODAL,
  REMOVE_ITEM_SHOW,
  REMOVE_ITEM_APPROVE,
  ASSIGNED_DATA_TARGET_KPI_EDIT,
  CANCEL_LIST_KPI_MODAL_ALL
} = actions;
export default reducer;
