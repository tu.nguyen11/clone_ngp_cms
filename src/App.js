import React from "react";
import "./App.css";
import "./views/components/style.css";
import { Route, Switch, Router, BrowserRouter } from "react-router-dom";
import { HistoryService } from "./services";
import PrintOrderPage from "./views/pages/Order/PrintOrderPage";
import { IVinhHoang } from "./views/components";
import PrintCouponPage from "./views/pages/Store/PrintCouponPage";
//Pages
const loading = () => (
  <div className="animated fadeIn pt-3 text-center">
    <IVinhHoang />
  </div>
);
const DefaultLayout = React.lazy(() =>
  import("./views/containers/DefaultLayout")
);
const LoginPage = React.lazy(() => import("./views/pages/LoginPage"));
const Page404 = React.lazy(() => import("./views/pages/Page404"));
const OrderPage = React.lazy(() => import("./views/pages/Order/OrderPage"));
const DetailNewsApp = React.lazy(() =>
  import("./views/pages/NewsApp/DetailNewsApp")
);
const Test = React.lazy(() => import("./views/pages/Dashboard/Test"));

function App() {
  return (
    <BrowserRouter>
      <React.Suspense fallback={loading()}>
        <Router history={HistoryService}>
          <Switch>
            <Route
              path="/login"
              name="Login Page"
              render={(props) => <LoginPage {...props} />}
            />
            <Route
              path="/order"
              name="Order Page"
              exact={true}
              render={(props) => <OrderPage {...props} />}
            />
            {/* <Route
              path="/detailorder"
              name="Order Page"
              render={props => <DetailOrder {...props} />}
            /> */}
            {/* <Route
              path='/404'
              name='404 Page'
              render={props => <Page404 {...props} />}
            /> */}
            <Route
              path="/printOrder/:id"
              name="Order Page"
              render={(props) => <PrintOrderPage {...props} />}
            />
            <Route
              path="/printInvoice/:type"
              name="Print Invoice Page"
              render={(props) => <PrintCouponPage {...props} />}
            />
            <Route
              path="/loctroi/:type/:id"
              name="DetailNewsApp"
              render={(props) => <DetailNewsApp {...props} />}
            />
            <Route
              path="/"
              name="Home"
              render={(props) => <DefaultLayout {...props} />}
            />
            <Route
              path="/Test"
              name="Home"
              render={(props) => <Test {...props} />}
            />
          </Switch>
        </Router>
      </React.Suspense>
    </BrowserRouter>
  );
}

export default App;
